#ifndef _TITANIUM_ARRAY_SET_SORT_H_
#define _TITANIUM_ARRAY_SET_SORT_H_


#include "AST.h"
#include "CtType.h"
#include "SortingPredicate.h"

class TitaniumArrayTypeNode;


struct ArraySetSort : public SortingPredicate< const TitaniumArrayTypeNode * > {
  bool operator () ( const TitaniumArrayTypeNode *a,
		     const TitaniumArrayTypeNode *b ) const
    {
      // intentionally ignore top-level local qualifications
      // DOB: use a reverse sort so dependencies on lower arities added during
      // array instantiation in instantiateArrays() will get picked up correctly
      return a->tiArity() > b->tiArity()
	|| (a->tiArity() == b->tiArity() &&
	    a->elementType()->cType() > b->elementType()->cType());
    }
};


#endif // !_TITANIUM_ARRAY_SET_SORT_H_
