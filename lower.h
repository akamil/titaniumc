#ifndef _INCLUDE_TITANIUM_LOWER_H_
#define _INCLUDE_TITANIUM_LOWER_H_


#include "AST.h"
#include <vector>

BlockNode *EncapsulateInits(vector<TreeNode *>);
ExprNode *defaultInitializer(TypeNode *t,
			     const SourcePosn &pos = NoSourcePosition);
string immutableDefaultInitializerStr(TypeNode *t);
bool checkAliasing(TreeNode *n);
void checkAndFixAliasing(TreeNode *n, const char *s = 0);

void AddStmts(TreeNode *insertPt, llist<TreeNode *> *newStmts);

// in code-alloc.cc
extern const string callNoArgConstructor(TypeNode *t);

void resetIFtemp();

ObjectNode *MakeTemporary(TreeNode *expr, TreeNode *&declStmt,
			  TreeNode *&assnStmt, const string *tempname = NULL);

#endif // !_INCLUDE_TITANIUM_LOWER_H_
