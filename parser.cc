/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for GLR parsing with Bison,
   Copyright (C) 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* This is the parser code for GLR (Generalized LR) parser. */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "glr.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     ABSTRACT = 258,
     ASSERT = 259,
     BOOLEAN = 260,
     BREAK = 261,
     BROADCAST = 262,
     BYTE = 263,
     CASE = 264,
     CATCH = 265,
     CHAR = 266,
     CLASS = 267,
     CONTINUE = 268,
     DEFAULT = 269,
     DO = 270,
     DOUBLE = 271,
     ELSE = 272,
     EXTENDS = 273,
     FINAL = 274,
     FINALLY = 275,
     FLOAT = 276,
     FOR = 277,
     FOREACH = 278,
     IF = 279,
     IMPLEMENTS = 280,
     IMPORT = 281,
     INSTANCEOF = 282,
     INT = 283,
     INTERFACE = 284,
     IMMUTABLE = 285,
     INLINE = 286,
     LOCAL = 287,
     LONG = 288,
     NATIVE = 289,
     NEW = 290,
     NULL_VAL = 291,
     OPERATOR = 292,
     OVERLAP = 293,
     _PACKAGE = 294,
     PRIVATE = 295,
     PROTECTED = 296,
     PUBLIC = 297,
     POLYSHARED = 298,
     NONSHARED = 299,
     PARTITION = 300,
     RETURN = 301,
     SHORT = 302,
     STATIC = 303,
     SUPER = 304,
     SWITCH = 305,
     SYNCHRONIZED = 306,
     STRICTFP = 307,
     SINGLE = 308,
     SGLOBAL = 309,
     TEMPLATE = 310,
     THIS = 311,
     THROW = 312,
     THROWS = 313,
     TRANSIENT = 314,
     TRY = 315,
     VOID = 316,
     VOLATILE = 317,
     WHILE = 318,
     TRUE_LITERAL = 319,
     FALSE_LITERAL = 320,
     IDENTIFIER = 321,
     INT_LITERAL = 322,
     LONG_LITERAL = 323,
     FLOAT_LITERAL = 324,
     DOUBLE_LITERAL = 325,
     CHARACTER_LITERAL = 326,
     STRING_LITERAL = 327,
     CAND = 328,
     COR = 329,
     EQ = 330,
     NE = 331,
     LE = 332,
     GE = 333,
     LSHIFTL = 334,
     ASHIFTR = 335,
     LSHIFTR = 336,
     PLUS_ASG = 337,
     MINUS_ASG = 338,
     MULT_ASG = 339,
     DIV_ASG = 340,
     REM_ASG = 341,
     LSHIFTL_ASG = 342,
     ASHIFTR_ASG = 343,
     LSHIFTR_ASG = 344,
     AND_ASG = 345,
     XOR_ASG = 346,
     OR_ASG = 347,
     PLUSPLUS = 348,
     MINUSMINUS = 349,
     GUARDS = 350
   };
#endif
/* Tokens.  */
#define ABSTRACT 258
#define ASSERT 259
#define BOOLEAN 260
#define BREAK 261
#define BROADCAST 262
#define BYTE 263
#define CASE 264
#define CATCH 265
#define CHAR 266
#define CLASS 267
#define CONTINUE 268
#define DEFAULT 269
#define DO 270
#define DOUBLE 271
#define ELSE 272
#define EXTENDS 273
#define FINAL 274
#define FINALLY 275
#define FLOAT 276
#define FOR 277
#define FOREACH 278
#define IF 279
#define IMPLEMENTS 280
#define IMPORT 281
#define INSTANCEOF 282
#define INT 283
#define INTERFACE 284
#define IMMUTABLE 285
#define INLINE 286
#define LOCAL 287
#define LONG 288
#define NATIVE 289
#define NEW 290
#define NULL_VAL 291
#define OPERATOR 292
#define OVERLAP 293
#define _PACKAGE 294
#define PRIVATE 295
#define PROTECTED 296
#define PUBLIC 297
#define POLYSHARED 298
#define NONSHARED 299
#define PARTITION 300
#define RETURN 301
#define SHORT 302
#define STATIC 303
#define SUPER 304
#define SWITCH 305
#define SYNCHRONIZED 306
#define STRICTFP 307
#define SINGLE 308
#define SGLOBAL 309
#define TEMPLATE 310
#define THIS 311
#define THROW 312
#define THROWS 313
#define TRANSIENT 314
#define TRY 315
#define VOID 316
#define VOLATILE 317
#define WHILE 318
#define TRUE_LITERAL 319
#define FALSE_LITERAL 320
#define IDENTIFIER 321
#define INT_LITERAL 322
#define LONG_LITERAL 323
#define FLOAT_LITERAL 324
#define DOUBLE_LITERAL 325
#define CHARACTER_LITERAL 326
#define STRING_LITERAL 327
#define CAND 328
#define COR 329
#define EQ 330
#define NE 331
#define LE 332
#define GE 333
#define LSHIFTL 334
#define ASHIFTR 335
#define LSHIFTR 336
#define PLUS_ASG 337
#define MINUS_ASG 338
#define MULT_ASG 339
#define DIV_ASG 340
#define REM_ASG 341
#define LSHIFTL_ASG 342
#define ASHIFTR_ASG 343
#define LSHIFTR_ASG 344
#define AND_ASG 345
#define XOR_ASG 346
#define OR_ASG 347
#define PLUSPLUS 348
#define MINUSMINUS 349
#define GUARDS 350




/* Copy the first part of user declarations.  */
#line 138 "parser.yy"

#include <cstdlib>
#include <stdarg.h>
#include "AST.h"
#include "errors.h"
#include "parse.h"
#include "tokens.h"
#include "code-util.h"
#include "pseudocode.h"
#include "lex-string.h"

#define YYDEBUG 1

#ifdef HAVE_ALLOCA
# define YYSTACK_USE_ALLOCA 1
# if HAVE_ALLOCA_H
#  include <alloca.h>
# else
   extern "C" void *alloca(size_t size); 
# endif // HAVE_ALLOCA_H
#if defined(__GNUC__) && !defined(alloca)
  #define alloca __builtin_alloca
#endif
#endif // HAVE_ALLOCA

static void yyerror(const char* msg);

static TreeNode *newOperator(char *op, SourcePosn p);
/* Routines to check the validity of modifier flags.  Each routine */
/* takes a logical `or' of flag values and a source position to which */
/* to refer error messages. */
static void checkFieldModifiers (Common::Modifiers flags, SourcePosn posn);
static void checkConstantFieldModifiers (Common::Modifiers flags, 
					 SourcePosn posn);
static void checkMethodModifiers (Common::Modifiers flags, SourcePosn posn);
static void checkConstructorModifiers (Common::Modifiers flags, 
				       SourcePosn posn);
static void checkMethodSignatureModifiers (Common::Modifiers flags, 
				       	   SourcePosn posn);
static void checkInterfaceModifiers(Common::Modifiers flags, 
				    SourcePosn posn);
static void checkNestedTypeModifiers(Common::Modifiers flags, SourcePosn posn);
static void checkLocalTypeModifiers(Common::Modifiers flags, SourcePosn posn);
static const string *isTitaniumTypeName(TypeNode *t); 
static TypeNode *buildTemplateInstanceType(TreeNode *tn, llist<TreeNode*> *args); 
static TreeNode *buildTemplateQualifiedName(TypeNode *tmpl, TreeNode *name);
static TreeNode *addSynchronized(Common::Modifiers modifiers, TreeNode *body);


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 190 "parser.yy"
typedef union YYSTYPE {
    SimpTerminalInfo SimpTerminal;  	/* Terminal symbols with only position. */
    TerminalInfo Terminal;		/* Terminal symbols with ASCII string data. */
    StrTerminalInfo StrTerminal;	/* Terminal symbols with Unicode string data. */
    CharTerminalInfo CharTerminal;	/* Terminal symbols with Unicode char data. */
    long Int;				/* Simple integer values */
    bool Bool;
    CompileUnitNode* CompileUnit;
    TreeNode* Tree;
    TypeNode* Type;
    CatchNode* Catch;
    llist<TreeNode*>* TreeList;
    llist<TypeNode*>* TypeList;
    llist<CatchNode*>* CatchList;
    Common::Modifiers Modifiers;
    llist<DeclaratorTuple>* DeclaratorList;
    TryNode *Try;
    TypeDeclNode *TypeDecl;
} YYSTYPE;
/* Line 181 of glr.c.  */
#line 331 "y.tab.c"
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

#if ! defined (YYLTYPE) && ! defined (YYLTYPE_IS_DECLARED)
typedef struct YYLTYPE
{

  char yydummy;

} YYLTYPE;
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif

/* Default (constant) value used for initialization for null
   right-hand sides.  Unlike the standard yacc.c template,
   here we set the default value of $$ to a zeroed-out value.
   Since the default value is undefined, this behavior is
   technically correct. */
static YYSTYPE yyval_default;

/* Copy the second part of user declarations.  */


/* Line 217 of glr.c.  */
#line 358 "y.tab.c"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#ifndef YYFREE
# define YYFREE free
#endif
#ifndef YYMALLOC
# define YYMALLOC malloc
#endif
#ifndef YYREALLOC
# define YYREALLOC realloc
#endif

#define YYSIZEMAX ((size_t) -1)

#ifdef __cplusplus
   typedef bool yybool;
#else
   typedef unsigned char yybool;
#endif
#define yytrue 1
#define yyfalse 0

#ifndef YYSETJMP
# include <setjmp.h>
# define YYJMP_BUF jmp_buf
# define YYSETJMP(env) setjmp (env)
# define YYLONGJMP(env, val) longjmp (env, val)
#endif

/*-----------------.
| GCC extensions.  |
`-----------------*/

#ifndef __attribute__
/* This feature is available in gcc versions 2.5 and later.  */
# if (!defined (__GNUC__) || __GNUC__ < 2 \
      || (__GNUC__ == 2 && __GNUC_MINOR__ < 5) || __STRICT_ANSI__)
#  define __attribute__(Spec) /* empty */
# endif
#endif


#ifdef __cplusplus
# define YYOPTIONAL_LOC(Name) /* empty */
#else
# define YYOPTIONAL_LOC(Name) Name __attribute__ ((__unused__))
#endif

#ifndef YYASSERT
# define YYASSERT(condition) ((void) ((condition) || (abort (), 0)))
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  11
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   3746

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  121
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  178
/* YYNRULES -- Number of rules. */
#define YYNRULES  475
/* YYNRULES -- Number of states. */
#define YYNSTATES  830
/* YYMAXRHS -- Maximum number of symbols on right-hand side of rule. */
#define YYMAXRHS 10
/* YYMAXLEFT -- Maximum number of symbols to the left of a handle
   accessed by $0, $-1, etc., in any rule. */
#define YYMAXLEFT 0

/* YYTRANSLATE(X) -- Bison symbol number corresponding to X.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   350

#define YYTRANSLATE(YYX)						\
  ((YYX <= 0) ? YYEOF :							\
   (unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    86,     2,     2,     2,    97,    94,     2,
      73,    74,    92,    90,    79,    91,    80,    93,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    89,    81,
      85,    83,    84,    88,    82,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    77,     2,    78,    96,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    75,    95,    76,    87,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short int yyprhs[] =
{
       0,     0,     3,     5,     7,     9,    11,    13,    15,    17,
      19,    21,    23,    25,    27,    30,    34,    37,    41,    44,
      46,    48,    50,    52,    54,    57,    60,    62,    65,    69,
      74,    80,    81,    84,    86,    88,    90,    92,    94,    96,
      98,   100,   102,   104,   106,   108,   112,   116,   117,   118,
     121,   122,   125,   128,   130,   132,   134,   136,   138,   140,
     142,   146,   152,   158,   165,   167,   168,   170,   173,   175,
     177,   179,   181,   183,   186,   187,   190,   191,   195,   197,
     198,   200,   203,   205,   207,   209,   211,   213,   215,   217,
     219,   221,   228,   234,   239,   241,   242,   244,   247,   249,
     251,   253,   255,   257,   259,   261,   263,   265,   267,   269,
     271,   273,   275,   277,   279,   281,   283,   285,   289,   292,
     297,   299,   301,   312,   323,   325,   327,   329,   330,   333,
     340,   342,   343,   345,   349,   354,   356,   357,   359,   360,
     363,   365,   369,   371,   373,   384,   394,   396,   398,   404,
     410,   416,   423,   426,   428,   434,   436,   437,   440,   444,
     445,   448,   450,   452,   454,   456,   461,   471,   481,   483,
     485,   491,   493,   497,   500,   503,   507,   512,   514,   518,
     520,   524,   527,   531,   536,   538,   542,   545,   549,   554,
     556,   560,   563,   565,   567,   569,   573,   578,   581,   583,
     587,   589,   591,   595,   597,   598,   600,   603,   605,   607,
     609,   616,   621,   625,   627,   629,   631,   633,   635,   637,
     639,   641,   643,   645,   647,   651,   657,   661,   664,   666,
     668,   670,   672,   674,   676,   678,   684,   692,   698,   702,
     703,   707,   709,   711,   714,   718,   721,   727,   735,   744,
     746,   749,   751,   753,   754,   756,   757,   759,   763,   770,
     772,   773,   777,   782,   784,   788,   792,   796,   800,   805,
     807,   808,   814,   817,   820,   824,   827,   829,   832,   838,
     841,   846,   852,   853,   858,   860,   862,   864,   866,   868,
     870,   872,   875,   879,   883,   887,   891,   893,   895,   897,
     901,   905,   908,   911,   913,   917,   921,   923,   925,   927,
     930,   933,   936,   939,   942,   945,   948,   951,   954,   957,
     960,   963,   966,   969,   972,   975,   978,   981,   984,   987,
     990,   993,   996,   999,  1002,  1005,  1008,  1011,  1014,  1017,
    1020,  1023,  1026,  1029,  1033,  1038,  1043,  1045,  1048,  1051,
    1055,  1058,  1062,  1066,  1072,  1076,  1079,  1081,  1086,  1091,
    1093,  1094,  1096,  1100,  1103,  1106,  1110,  1115,  1121,  1128,
    1136,  1143,  1151,  1155,  1156,  1158,  1161,  1163,  1166,  1168,
    1169,  1172,  1176,  1180,  1185,  1187,  1189,  1191,  1193,  1195,
    1197,  1200,  1203,  1205,  1207,  1209,  1211,  1213,  1215,  1218,
    1221,  1223,  1225,  1227,  1230,  1233,  1235,  1237,  1239,  1241,
    1244,  1247,  1249,  1254,  1259,  1263,  1267,  1271,  1273,  1277,
    1281,  1283,  1287,  1293,  1295,  1296,  1298,  1302,  1306,  1310,
    1314,  1318,  1322,  1326,  1330,  1334,  1338,  1342,  1346,  1352,
    1356,  1360,  1364,  1366,  1368,  1370,  1372,  1374,  1378,  1382,
    1386,  1390,  1394,  1398,  1402,  1406,  1410,  1414,  1418,  1422,
    1426,  1430,  1434,  1438,  1442,  1444,  1449,  1451,  1455,  1459,
    1462,  1466,  1470,  1473,  1476,  1479
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const short int yyrhs[] =
{
     122,     0,    -1,   137,    -1,    67,    -1,    68,    -1,    69,
      -1,    70,    -1,    64,    -1,    65,    -1,    71,    -1,    72,
      -1,   248,    -1,   126,    -1,   126,    -1,   136,   134,    -1,
     136,   134,   132,    -1,   198,   134,    -1,   198,   134,   132,
      -1,   128,   134,    -1,   136,    -1,   129,    -1,   198,    -1,
     250,    -1,   131,    -1,   198,    80,    -1,   130,   250,    -1,
     133,    -1,   133,   132,    -1,    77,    78,   134,    -1,    77,
     256,    78,   134,    -1,    77,   291,    66,    78,   134,    -1,
      -1,   134,   135,    -1,    53,    -1,    32,    -1,    44,    -1,
      43,    -1,     5,    -1,    11,    -1,     8,    -1,    47,    -1,
      28,    -1,    21,    -1,    33,    -1,    16,    -1,   138,   139,
     140,    -1,    39,   250,    81,    -1,    -1,    -1,   139,   143,
      -1,    -1,   141,   140,    -1,    81,   140,    -1,   142,    -1,
     195,    -1,   147,    -1,   186,    -1,   144,    -1,   145,    -1,
     146,    -1,    26,   250,    81,    -1,    26,   250,    80,    92,
      81,    -1,    26,   250,    83,   250,    81,    -1,   148,    12,
     251,   151,   152,   153,    -1,   149,    -1,    -1,   150,    -1,
     149,   150,    -1,     3,    -1,    19,    -1,    42,    -1,    30,
      -1,    52,    -1,    18,   124,    -1,    -1,    25,   178,    -1,
      -1,    75,   154,    76,    -1,   155,    -1,    -1,   156,    -1,
     155,   156,    -1,   160,    -1,   167,    -1,   180,    -1,   184,
      -1,   185,    -1,   157,    -1,    81,    -1,   158,    -1,   159,
      -1,   161,    12,   251,   151,   152,   153,    -1,   161,    29,
     251,   187,   189,    -1,   161,   124,   164,    81,    -1,   162,
      -1,    -1,   163,    -1,   162,   163,    -1,    42,    -1,    41,
      -1,    40,    -1,    48,    -1,    19,    -1,    53,    -1,    54,
      -1,     3,    -1,    34,    -1,    51,    -1,    59,    -1,    52,
      -1,    62,    -1,    32,    -1,    44,    -1,    43,    -1,    31,
      -1,    30,    -1,   165,    -1,   164,    79,   165,    -1,   251,
     268,    -1,   251,   268,    83,   166,    -1,   291,    -1,   207,
      -1,   161,   124,   168,    73,   172,    74,   268,   176,   170,
     179,    -1,   161,   169,   168,    73,   172,    74,   268,   176,
     170,   179,    -1,   251,    -1,   254,    -1,    61,    -1,    -1,
     171,   170,    -1,    38,    73,   251,    79,   251,    74,    -1,
     173,    -1,    -1,   174,    -1,   174,    79,   173,    -1,   175,
     124,   251,   268,    -1,    19,    -1,    -1,   177,    -1,    -1,
      58,   178,    -1,   124,    -1,   124,    79,   178,    -1,   210,
      -1,    81,    -1,   161,   251,    73,   172,    74,   176,    75,
     181,   211,    76,    -1,   161,   251,    73,   172,    74,   176,
      75,   211,    76,    -1,   182,    -1,   183,    -1,    56,    73,
     261,    74,    81,    -1,    49,    73,   261,    74,    81,    -1,
     258,    73,   261,    74,    81,    -1,   257,    49,    73,   261,
      74,    81,    -1,    48,   210,    -1,   210,    -1,   148,    29,
     251,   187,   189,    -1,   188,    -1,    -1,    18,   178,    -1,
      75,   190,    76,    -1,    -1,   191,   190,    -1,   192,    -1,
     193,    -1,   157,    -1,    81,    -1,   161,   124,   164,    81,
      -1,   161,   124,   168,    73,   172,    74,   268,   176,    81,
      -1,   161,   169,   168,    73,   172,    74,   268,   176,    81,
      -1,    55,    -1,    82,    -1,   194,    85,   196,    84,   142,
      -1,   197,    -1,   197,    79,   196,    -1,    12,   251,    -1,
     124,   251,    -1,   250,    85,   200,    -1,   194,   250,    85,
     200,    -1,   206,    -1,   199,    79,   206,    -1,   201,    -1,
     199,    79,   201,    -1,   206,    84,    -1,   250,    85,   202,
      -1,   194,   250,    85,   202,    -1,   203,    -1,   199,    79,
     203,    -1,   206,   105,    -1,   250,    85,   204,    -1,   194,
     250,    85,   204,    -1,   205,    -1,   199,    79,   205,    -1,
     206,   106,    -1,   125,    -1,   293,    -1,   248,    -1,    75,
     208,    76,    -1,    75,   208,    79,    76,    -1,    75,    76,
      -1,   209,    -1,   208,    79,   209,    -1,   291,    -1,   207,
      -1,    75,   211,    76,    -1,   212,    -1,    -1,   213,    -1,
     212,   213,    -1,   215,    -1,   216,    -1,   214,    -1,   148,
      12,   251,   151,   152,   153,    -1,    19,   124,   164,    81,
      -1,   124,   164,    81,    -1,   217,    -1,   218,    -1,   219,
      -1,   220,    -1,   222,    -1,   227,    -1,   236,    -1,   238,
      -1,   210,    -1,   243,    -1,    81,    -1,     4,   291,    81,
      -1,     4,   291,    89,   291,    81,    -1,   251,    89,   216,
      -1,   221,    81,    -1,   295,    -1,   278,    -1,   279,    -1,
     273,    -1,   274,    -1,   260,    -1,   264,    -1,    24,    73,
     291,    74,   216,    -1,    24,    73,   291,    74,   216,    17,
     216,    -1,    50,    73,   291,    74,   223,    -1,    75,   224,
      76,    -1,    -1,   225,   212,   224,    -1,   225,    -1,   226,
      -1,   226,   225,    -1,     9,   296,    89,    -1,    14,    89,
      -1,    63,    73,   291,    74,   216,    -1,    15,   216,    63,
      73,   291,    74,    81,    -1,    22,    73,   228,   290,    81,
     229,    74,   216,    -1,   232,    -1,   230,    81,    -1,   215,
      -1,   231,    -1,    -1,   231,    -1,    -1,   221,    -1,   221,
      79,   231,    -1,    23,    73,   234,    74,   233,   216,    -1,
      31,    -1,    -1,   251,   235,   291,    -1,   198,   251,   235,
     291,    -1,    66,    -1,     6,   237,    81,    -1,    13,   237,
      81,    -1,    46,   290,    81,    -1,    57,   291,    81,    -1,
      53,    57,   291,    81,    -1,   251,    -1,    -1,    51,    73,
     291,    74,   216,    -1,   239,   242,    -1,   239,   240,    -1,
     239,   240,   242,    -1,    60,   210,    -1,   241,    -1,   241,
     240,    -1,    10,    73,   174,    74,   210,    -1,    20,   210,
      -1,    45,    75,   244,    76,    -1,    45,   251,    75,   244,
      76,    -1,    -1,   291,   120,   216,   244,    -1,   248,    -1,
     246,    -1,   264,    -1,   247,    -1,   123,    -1,    36,    -1,
      56,    -1,   252,    56,    -1,   131,    80,    56,    -1,   198,
      80,    56,    -1,    73,   291,    74,    -1,    73,   248,    74,
      -1,   255,    -1,   259,    -1,   260,    -1,   136,    80,    12,
      -1,    61,    80,    12,    -1,   250,   134,    -1,   131,   134,
      -1,   249,    -1,   250,   134,   132,    -1,   131,   134,   132,
      -1,   251,    -1,   253,    -1,    66,    -1,   250,    80,    -1,
     252,    66,    -1,   252,    12,    -1,   252,   254,    -1,    37,
      86,    -1,    37,    87,    -1,    37,    85,    -1,    37,    84,
      -1,    37,   102,    -1,    37,   103,    -1,    37,   100,    -1,
      37,   101,    -1,    37,    90,    -1,    37,    91,    -1,    37,
      92,    -1,    37,    93,    -1,    37,    94,    -1,    37,    95,
      -1,    37,    96,    -1,    37,    97,    -1,    37,   104,    -1,
      37,   105,    -1,    37,   106,    -1,    37,   107,    -1,    37,
     108,    -1,    37,   109,    -1,    37,   110,    -1,    37,   111,
      -1,    37,   112,    -1,    37,   113,    -1,    37,   114,    -1,
      37,   115,    -1,    37,   116,    -1,    37,   117,    -1,    37,
      77,    78,    -1,    37,    77,    78,    83,    -1,   247,    77,
     256,    78,    -1,   262,    -1,   246,    80,    -1,   252,    49,
      -1,   131,    80,    49,    -1,   257,   168,    -1,    49,    80,
     168,    -1,   258,    80,   168,    -1,   198,    80,    49,    80,
     168,    -1,   249,    80,   168,    -1,   130,    12,    -1,   254,
      -1,   259,    73,   261,    74,    -1,   248,    73,   261,    74,
      -1,   262,    -1,    -1,   291,    -1,   291,    79,   262,    -1,
     257,    35,    -1,   252,    35,    -1,   131,    80,    35,    -1,
      35,   265,   127,   266,    -1,    35,   265,   127,   266,   207,
      -1,    35,   265,   127,    73,   261,    74,    -1,    35,   265,
     127,    73,   261,    74,   153,    -1,   263,   265,   127,    73,
     261,    74,    -1,   263,   265,   127,    73,   261,    74,   153,
      -1,    73,   291,    74,    -1,    -1,   267,    -1,   267,   266,
      -1,   133,    -1,   285,   134,    -1,   269,    -1,    -1,    77,
      78,    -1,    77,    78,    32,    -1,   269,    77,    78,    -1,
     269,    77,    78,    32,    -1,   245,    -1,   272,    -1,   246,
      -1,   272,    -1,   273,    -1,   274,    -1,   270,   118,    -1,
     270,   119,    -1,   280,    -1,   277,    -1,   281,    -1,   277,
      -1,   278,    -1,   279,    -1,    90,   275,    -1,    91,   275,
      -1,   284,    -1,   285,    -1,   297,    -1,   118,   275,    -1,
     119,   275,    -1,   270,    -1,   282,    -1,   271,    -1,   282,
      -1,    87,   275,    -1,    86,   275,    -1,   283,    -1,    73,
     124,    74,   275,    -1,    73,   248,    74,   280,    -1,    77,
     262,    78,    -1,    77,   286,    78,    -1,    77,   288,    78,
      -1,   287,    -1,   287,    79,   286,    -1,   291,    89,   291,
      -1,   289,    -1,   289,    79,   288,    -1,   291,    89,   291,
      89,   291,    -1,   291,    -1,    -1,   292,    -1,   291,    85,
     291,    -1,   291,    84,   291,    -1,   291,   102,   291,    -1,
     291,   103,   291,    -1,   291,    27,   124,    -1,   291,   100,
     291,    -1,   291,   101,   291,    -1,   291,    94,   291,    -1,
     291,    95,   291,    -1,   291,    96,   291,    -1,   291,    98,
     291,    -1,   291,    99,   291,    -1,   291,    88,   291,    89,
     291,    -1,   291,   104,   291,    -1,   291,   106,   291,    -1,
     291,   105,   291,    -1,   295,    -1,   275,    -1,   294,    -1,
     276,    -1,   294,    -1,   292,    92,   292,    -1,   292,    93,
     292,    -1,   292,    97,   292,    -1,   292,    90,   292,    -1,
     292,    91,   292,    -1,   275,    83,   291,    -1,   275,   109,
     291,    -1,   275,   110,   291,    -1,   275,   111,   291,    -1,
     275,   107,   291,    -1,   275,   108,   291,    -1,   275,   112,
     291,    -1,   275,   114,   291,    -1,   275,   113,   291,    -1,
     275,   115,   291,    -1,   275,   116,   291,    -1,   275,   117,
     291,    -1,   291,    -1,     7,   291,   298,   275,    -1,    66,
      -1,    26,     1,    81,    -1,    39,     1,    81,    -1,     1,
     153,    -1,    75,     1,    76,    -1,    75,     1,    76,    -1,
       1,    81,    -1,     1,   210,    -1,     1,    81,    -1,     1,
     153,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short int yyrline[] =
{
       0,   325,   325,   331,   333,   335,   337,   339,   341,   343,
     345,   353,   354,   358,   362,   363,   366,   367,   373,   377,
     378,   379,   383,   385,   390,   395,   400,   401,   405,   406,
     408,   421,   422,   431,   432,   433,   434,   438,   440,   442,
     444,   446,   448,   450,   452,   461,   472,   475,   480,   481,
     488,   489,   496,   501,   503,   508,   510,   518,   519,   520,
     524,   533,   538,   550,   558,   560,   564,   565,   574,   576,
     578,   580,   582,   591,   594,   601,   604,   612,   617,   619,
     623,   624,   632,   633,   635,   637,   639,   641,   643,   651,
     653,   658,   664,   674,   697,   699,   703,   704,   713,   715,
     717,   720,   722,   724,   726,   729,   731,   733,   736,   738,
     741,   743,   745,   747,   749,   752,   761,   762,   767,   769,
     777,   778,   784,   790,   800,   801,   805,   810,   811,   815,
     825,   827,   831,   833,   838,   843,   844,   851,   853,   857,
     862,   864,   872,   873,   881,   888,   904,   905,   909,   914,
     916,   918,   926,   931,   939,   949,   951,   955,   962,   968,
     969,   974,   975,   977,   979,   984,  1000,  1007,  1020,  1021,
    1025,  1030,  1032,  1037,  1039,  1044,  1047,  1052,  1054,  1059,
    1061,  1066,  1068,  1071,  1076,  1078,  1083,  1085,  1088,  1093,
    1095,  1100,  1105,  1107,  1109,  1118,  1120,  1122,  1129,  1131,
    1136,  1137,  1146,  1151,  1153,  1157,  1159,  1164,  1166,  1168,
    1176,  1186,  1188,  1195,  1196,  1197,  1198,  1199,  1200,  1201,
    1202,  1203,  1204,  1210,  1216,  1225,  1232,  1240,  1245,  1247,
    1249,  1251,  1253,  1255,  1257,  1265,  1267,  1269,  1274,  1279,
    1280,  1282,  1287,  1289,  1294,  1296,  1304,  1306,  1308,  1310,
    1314,  1316,  1321,  1323,  1327,  1329,  1333,  1335,  1342,  1347,
    1348,  1352,  1354,  1369,  1380,  1382,  1384,  1386,  1388,  1394,
    1396,  1403,  1406,  1408,  1410,  1415,  1420,  1422,  1427,  1432,
    1439,  1441,  1446,  1447,  1459,  1461,  1465,  1466,  1470,  1471,
    1473,  1475,  1477,  1479,  1481,  1483,  1485,  1486,  1488,  1489,
    1507,  1521,  1522,  1523,  1527,  1528,  1532,  1533,  1537,  1544,
    1549,  1552,  1554,  1559,  1560,  1561,  1562,  1563,  1564,  1565,
    1566,  1567,  1568,  1569,  1570,  1571,  1572,  1573,  1574,  1575,
    1576,  1577,  1578,  1579,  1580,  1581,  1582,  1583,  1584,  1585,
    1586,  1587,  1588,  1589,  1590,  1596,  1601,  1610,  1615,  1617,
    1623,  1625,  1627,  1629,  1631,  1634,  1636,  1644,  1646,  1651,
    1653,  1657,  1659,  1667,  1669,  1671,  1676,  1678,  1681,  1683,
    1686,  1688,  1694,  1697,  1701,  1703,  1708,  1710,  1715,  1717,
    1721,  1723,  1725,  1727,  1735,  1736,  1740,  1741,  1745,  1746,
    1750,  1755,  1763,  1764,  1768,  1769,  1773,  1774,  1775,  1777,
    1779,  1780,  1781,  1785,  1790,  1795,  1796,  1800,  1801,  1805,
    1807,  1809,  1813,  1815,  1826,  1830,  1831,  1835,  1836,  1840,
    1846,  1847,  1851,  1859,  1861,  1865,  1866,  1868,  1870,  1872,
    1874,  1876,  1878,  1880,  1882,  1884,  1886,  1888,  1890,  1892,
    1894,  1896,  1898,  1902,  1903,  1907,  1908,  1912,  1914,  1916,
    1918,  1920,  1928,  1930,  1932,  1934,  1936,  1938,  1940,  1942,
    1944,  1946,  1948,  1950,  1958,  1962,  1967,  1978,  1983,  1988,
    1993,  1998,  2003,  2005,  2010,  2015
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "ABSTRACT", "ASSERT", "BOOLEAN", "BREAK",
  "BROADCAST", "BYTE", "CASE", "CATCH", "CHAR", "CLASS", "CONTINUE",
  "DEFAULT", "DO", "DOUBLE", "ELSE", "EXTENDS", "FINAL", "FINALLY",
  "FLOAT", "FOR", "FOREACH", "IF", "IMPLEMENTS", "IMPORT", "INSTANCEOF",
  "INT", "INTERFACE", "IMMUTABLE", "INLINE", "LOCAL", "LONG", "NATIVE",
  "NEW", "NULL_VAL", "OPERATOR", "OVERLAP", "_PACKAGE", "PRIVATE",
  "PROTECTED", "PUBLIC", "POLYSHARED", "NONSHARED", "PARTITION", "RETURN",
  "SHORT", "STATIC", "SUPER", "SWITCH", "SYNCHRONIZED", "STRICTFP",
  "SINGLE", "SGLOBAL", "TEMPLATE", "THIS", "THROW", "THROWS", "TRANSIENT",
  "TRY", "VOID", "VOLATILE", "WHILE", "TRUE_LITERAL", "FALSE_LITERAL",
  "IDENTIFIER", "INT_LITERAL", "LONG_LITERAL", "FLOAT_LITERAL",
  "DOUBLE_LITERAL", "CHARACTER_LITERAL", "STRING_LITERAL", "'('", "')'",
  "'{'", "'}'", "'['", "']'", "','", "'.'", "';'", "'@'", "'='", "'>'",
  "'<'", "'!'", "'~'", "'?'", "':'", "'+'", "'-'", "'*'", "'/'", "'&'",
  "'|'", "'^'", "'%'", "CAND", "COR", "EQ", "NE", "LE", "GE", "LSHIFTL",
  "ASHIFTR", "LSHIFTR", "PLUS_ASG", "MINUS_ASG", "MULT_ASG", "DIV_ASG",
  "REM_ASG", "LSHIFTL_ASG", "ASHIFTR_ASG", "LSHIFTR_ASG", "AND_ASG",
  "XOR_ASG", "OR_ASG", "PLUSPLUS", "MINUSMINUS", "GUARDS", "$accept",
  "Start", "Literal", "Type", "NoNameType", "OtherType",
  "QualifiedBaseType", "BaseType", "TypeName", "TemplateQualifierStart",
  "TemplateQualifiedName", "ArraySpecifiers", "ArraySpecifier",
  "QualifiersOpt", "Qualifier", "PrimitiveType", "CompilationUnit",
  "PackageDeclarationOpt", "ImportStatementsOpt", "TypeDeclarationsOpt",
  "TypeDeclaration", "BasicTypeDeclaration", "ImportStatement",
  "TypeImportStatement", "TypeImportOnDemandStatement", "TypeDefStatement",
  "ClassDeclaration", "ClassModifiersOpt", "ClassModifiers",
  "ClassModifier", "SuperOpt", "InterfacesOpt", "ClassBody",
  "FieldDeclarationsOpt", "FieldDeclarations", "FieldDeclaration",
  "NestedTypeDeclaration", "NestedClassDeclaration",
  "NestedInterfaceDeclaration", "FieldVariableDeclaration",
  "FieldModifiersOpt", "FieldModifiers", "FieldModifier",
  "VariableDeclarators", "VariableDeclarator", "VariableInitializer",
  "MethodDeclaration", "MethodName", "Void", "OverlapsOpt", "OverlapDecl",
  "ParameterListOpt", "ParameterList", "Parameter", "OptionalFinal",
  "ThrowsOpt", "Throws", "TypeNameList", "MethodBody",
  "ConstructorDeclaration", "ConstructorCallStatement",
  "ExplicitThisConstructorCallStatement",
  "ExplicitSuperConstructorCallStatement", "StaticInitializer",
  "InstanceInitializer", "InterfaceDeclaration", "ExtendsInterfacesOpt",
  "ExtendsInterfaces", "InterfaceBody", "InterfaceMemberDeclarationsOpt",
  "InterfaceMemberDeclaration", "ConstantFieldDeclaration",
  "MethodSignatureDeclaration", "TemplateIntro", "TemplateDeclaration",
  "TemplateFormalList", "TemplateFormal", "TemplateInstance",
  "TemplateActualList", "TemplateActualList1", "TemplateActual1",
  "TemplateActualList2", "TemplateActual2", "TemplateActualList3",
  "TemplateActual3", "TemplateActual", "ArrayInitializer",
  "ElementInitializers", "Element", "Block", "BlockStatementsOpt",
  "BlockStatements", "BlockStatement", "LocalClassDeclarationStatement",
  "LocalVariableDeclarationStatement", "Statement", "EmptyStatement",
  "AssertStatement", "LabeledStatement", "ExpressionStatement",
  "StatementExpression", "SelectionStatement", "SwitchBlock",
  "SwitchBlockStatementsOpt", "SwitchLabels", "SwitchLabel",
  "IterationStatement", "ForInit", "ForUpdateOpt",
  "StatementExpressionsOpt", "StatementExpressions", "ForEachStatement",
  "OptionalInline", "ForEachClauses", "SpecialIn", "JumpStatement",
  "LabelOpt", "GuardingStatement", "TryBlock", "Catches", "Catch",
  "Finally", "PartitionStatement", "PartitionClauses", "PrimaryExpression",
  "NotJustName", "ComplexPrimary", "ArrayName", "FullArrayName", "Name",
  "SimpleName", "QualifiedNameStart", "QualifiedName", "OperatorName",
  "ArrayAccess", "IndexExpression", "FieldAccessStart",
  "QualifiedSuperStart", "FieldAccess", "MethodCall", "ArgumentListOpt",
  "ArgumentList", "QualifiedAllocationStart", "AllocationExpression",
  "RegionOpt", "DimExprs", "DimExpr", "DimsOpt", "Dims",
  "PostfixExpression", "NoNamePostfixExpression", "OtherPostfixExpression",
  "PostIncrement", "PostDecrement", "UnaryExpression",
  "NoNameUnaryExpression", "OtherUnaryExpression", "PreIncrement",
  "PreDecrement", "RestrictedUnaryExpression",
  "NoNameRestrictedUnaryExpression", "OtherRestrictedPostfixExpression",
  "CastExpression", "PointLiteral", "DomainLiteral", "DomainPairs",
  "DomainPair", "DomainTriples", "DomainTriple", "ExpressionOpt",
  "Expression", "Expression1", "NoNameExpression1", "BinaryExpression1",
  "Assignment", "ConstantExpression", "BroadcastExpression", "SpecialFrom", 0
};
#endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned short int yyr1[] =
{
       0,   121,   122,   123,   123,   123,   123,   123,   123,   123,
     123,   124,   124,   125,   126,   126,   126,   126,   127,   128,
     128,   128,   129,   129,   130,   131,   132,   132,   133,   133,
     133,   134,   134,   135,   135,   135,   135,   136,   136,   136,
     136,   136,   136,   136,   136,   137,   138,   138,   139,   139,
     140,   140,   140,   141,   141,   142,   142,   143,   143,   143,
     144,   145,   146,   147,   148,   148,   149,   149,   150,   150,
     150,   150,   150,   151,   151,   152,   152,   153,   154,   154,
     155,   155,   156,   156,   156,   156,   156,   156,   156,   157,
     157,   158,   159,   160,   161,   161,   162,   162,   163,   163,
     163,   163,   163,   163,   163,   163,   163,   163,   163,   163,
     163,   163,   163,   163,   163,   163,   164,   164,   165,   165,
     166,   166,   167,   167,   168,   168,   169,   170,   170,   171,
     172,   172,   173,   173,   174,   175,   175,   176,   176,   177,
     178,   178,   179,   179,   180,   180,   181,   181,   182,   183,
     183,   183,   184,   185,   186,   187,   187,   188,   189,   190,
     190,   191,   191,   191,   191,   192,   193,   193,   194,   194,
     195,   196,   196,   197,   197,   198,   198,   199,   199,   200,
     200,   201,   201,   201,   202,   202,   203,   203,   203,   204,
     204,   205,   206,   206,   206,   207,   207,   207,   208,   208,
     209,   209,   210,   211,   211,   212,   212,   213,   213,   213,
     214,   215,   215,   216,   216,   216,   216,   216,   216,   216,
     216,   216,   216,   217,   218,   218,   219,   220,   221,   221,
     221,   221,   221,   221,   221,   222,   222,   222,   223,   224,
     224,   224,   225,   225,   226,   226,   227,   227,   227,   227,
     228,   228,   229,   229,   230,   230,   231,   231,   232,   233,
     233,   234,   234,   235,   236,   236,   236,   236,   236,   237,
     237,   238,   238,   238,   238,   239,   240,   240,   241,   242,
     243,   243,   244,   244,   245,   245,   246,   246,   247,   247,
     247,   247,   247,   247,   247,   247,   247,   247,   247,   247,
     247,   248,   248,   248,   249,   249,   250,   250,   251,   252,
     253,   253,   253,   254,   254,   254,   254,   254,   254,   254,
     254,   254,   254,   254,   254,   254,   254,   254,   254,   254,
     254,   254,   254,   254,   254,   254,   254,   254,   254,   254,
     254,   254,   254,   254,   254,   255,   256,   257,   258,   258,
     259,   259,   259,   259,   259,   259,   259,   260,   260,   261,
     261,   262,   262,   263,   263,   263,   264,   264,   264,   264,
     264,   264,   265,   265,   266,   266,   267,   267,   268,   268,
     269,   269,   269,   269,   270,   270,   271,   271,   272,   272,
     273,   274,   275,   275,   276,   276,   277,   277,   277,   277,
     277,   277,   277,   278,   279,   280,   280,   281,   281,   282,
     282,   282,   283,   283,   284,   285,   285,   286,   286,   287,
     288,   288,   289,   290,   290,   291,   291,   291,   291,   291,
     291,   291,   291,   291,   291,   291,   291,   291,   291,   291,
     291,   291,   291,   292,   292,   293,   293,   294,   294,   294,
     294,   294,   295,   295,   295,   295,   295,   295,   295,   295,
     295,   295,   295,   295,   296,   297,   298,   143,   138,   157,
     153,   210,   216,   216,   156,   141
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     2,     3,     2,     3,     2,     1,
       1,     1,     1,     1,     2,     2,     1,     2,     3,     4,
       5,     0,     2,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     3,     3,     0,     0,     2,
       0,     2,     2,     1,     1,     1,     1,     1,     1,     1,
       3,     5,     5,     6,     1,     0,     1,     2,     1,     1,
       1,     1,     1,     2,     0,     2,     0,     3,     1,     0,
       1,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     6,     5,     4,     1,     0,     1,     2,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     3,     2,     4,
       1,     1,    10,    10,     1,     1,     1,     0,     2,     6,
       1,     0,     1,     3,     4,     1,     0,     1,     0,     2,
       1,     3,     1,     1,    10,     9,     1,     1,     5,     5,
       5,     6,     2,     1,     5,     1,     0,     2,     3,     0,
       2,     1,     1,     1,     1,     4,     9,     9,     1,     1,
       5,     1,     3,     2,     2,     3,     4,     1,     3,     1,
       3,     2,     3,     4,     1,     3,     2,     3,     4,     1,
       3,     2,     1,     1,     1,     3,     4,     2,     1,     3,
       1,     1,     3,     1,     0,     1,     2,     1,     1,     1,
       6,     4,     3,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     3,     5,     3,     2,     1,     1,
       1,     1,     1,     1,     1,     5,     7,     5,     3,     0,
       3,     1,     1,     2,     3,     2,     5,     7,     8,     1,
       2,     1,     1,     0,     1,     0,     1,     3,     6,     1,
       0,     3,     4,     1,     3,     3,     3,     3,     4,     1,
       0,     5,     2,     2,     3,     2,     1,     2,     5,     2,
       4,     5,     0,     4,     1,     1,     1,     1,     1,     1,
       1,     2,     3,     3,     3,     3,     1,     1,     1,     3,
       3,     2,     2,     1,     3,     3,     1,     1,     1,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     3,     4,     4,     1,     2,     2,     3,
       2,     3,     3,     5,     3,     2,     1,     4,     4,     1,
       0,     1,     3,     2,     2,     3,     4,     5,     6,     7,
       6,     7,     3,     0,     1,     2,     1,     2,     1,     0,
       2,     3,     3,     4,     1,     1,     1,     1,     1,     1,
       2,     2,     1,     1,     1,     1,     1,     1,     2,     2,
       1,     1,     1,     2,     2,     1,     1,     1,     1,     2,
       2,     1,     4,     4,     3,     3,     3,     1,     3,     3,
       1,     3,     5,     1,     0,     1,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     5,     3,
       3,     3,     1,     1,     1,     1,     1,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     1,     4,     1,     3,     3,     2,
       3,     3,     2,     2,     2,     2
};

/* YYDPREC[RULE-NUM] -- Dynamic precedence of rule #RULE-NUM (0 if none). */
static const unsigned char yydprec[] =
{
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0
};

/* YYMERGER[RULE-NUM] -- Index of merging function for rule #RULE-NUM. */
static const unsigned char yymerger[] =
{
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0
};

/* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
   doesn't specify something else to do.  Zero means the default is an
   error.  */
static const unsigned short int yydefact[] =
{
      47,     0,     0,     2,    48,     0,   308,     0,   306,     0,
     307,     1,     0,   468,   309,    46,   311,     0,   310,   312,
       0,    68,    69,     0,    71,    70,    72,   168,     0,   169,
      45,     0,    53,    49,    57,    58,    59,    55,     0,    64,
      66,    56,     0,    54,     0,   316,   315,   313,   314,   321,
     322,   323,   324,   325,   326,   327,   328,   319,   320,   317,
     318,   329,   330,   331,   332,   333,   334,   335,   336,   337,
     338,   339,   340,   341,   342,     0,   475,     0,     0,    52,
      51,     0,     0,    67,     0,   343,     0,   105,   102,   115,
     114,   111,   106,   100,    99,    98,   113,   112,   101,   107,
     109,   103,   104,   108,   110,     0,    88,     0,     0,    80,
      87,    89,    90,    82,     0,    94,    96,    83,    84,    85,
      86,   153,   467,   309,    60,     0,    74,   156,    37,    39,
      38,     0,    44,    42,    41,    43,    40,     0,    12,     0,
      31,    31,     0,     0,   171,    31,    11,   303,    31,   344,
     470,   474,   469,   152,     0,     0,   270,     0,   270,     0,
      69,     0,     0,     0,   373,   289,     0,   424,     0,     0,
       0,     0,   290,     0,     0,     0,     0,     7,     8,     3,
       4,     5,     6,     9,    10,     0,     0,   223,     0,     0,
       0,     0,     0,     0,   288,     0,     0,    31,    31,     0,
      31,   221,     0,     0,   205,   209,   207,   208,   213,   214,
     215,   216,     0,   217,   218,   249,   219,   220,     0,   222,
     384,   285,   287,   284,   303,   306,     0,   356,   296,     0,
       0,   297,   298,   373,   286,   405,   385,   388,   389,     0,
     393,   396,   397,   392,   406,   411,   400,   401,   228,   402,
      77,     0,    81,     0,     0,   126,     0,     0,   306,   101,
      97,     0,     0,     0,    76,     0,     0,   155,   173,   174,
      25,   302,    14,     0,    65,     0,    24,    16,     0,   301,
     471,   472,   473,     0,     0,   284,   298,   286,   388,   389,
     443,   396,   397,     0,   425,   444,   442,     0,   269,     0,
       0,     0,     0,     0,   255,     0,     0,     0,     0,   282,
       0,     0,   423,     0,     0,     0,     0,     0,   275,     0,
       0,     0,   284,     0,     0,     0,   417,     0,   420,   361,
     410,   409,   398,   399,   403,   404,     0,   116,   379,   355,
       0,     0,     0,    24,   202,   206,   227,     0,     0,   273,
     276,   272,   347,     0,   360,     0,     0,   364,   348,   291,
     363,   350,   124,   125,     0,   360,     0,   390,   391,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    74,   156,     0,     0,   379,     0,   136,    61,    62,
      73,     0,     0,   140,   157,     0,   154,    34,    36,    35,
      33,     0,   305,    26,    32,    15,     0,   170,   172,    17,
     192,    13,     0,     0,   175,   179,   177,   285,   284,    31,
     407,   385,   443,   445,   393,   394,   406,     0,   193,   444,
     304,     0,   224,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   264,   466,     0,   265,     0,     0,
       0,   251,   256,   424,     0,   254,     0,     0,     0,   306,
       0,     0,     0,    31,    20,    23,    19,    21,    22,     0,
       0,   282,   266,   351,     0,     0,     0,   267,   300,     0,
       0,   295,   294,   414,   415,     0,   416,     0,     0,     0,
       0,   212,     0,   118,   378,   365,   349,   292,   299,    74,
       0,   293,   136,   279,   274,   277,     0,   346,   361,     0,
     359,   354,   226,   352,     0,     0,   452,   456,   457,   453,
     454,   455,   458,   460,   459,   461,   462,   463,    76,     0,
      93,   136,   136,   135,     0,   130,   132,     0,    75,    63,
       0,     0,   164,   163,     0,     0,     0,   161,   162,    31,
       0,   361,    27,   176,     0,     0,   181,     0,   430,   427,
     426,     0,     0,   433,   434,   435,   436,   437,   431,   432,
     428,   429,   439,   441,   440,   450,   451,   447,   448,   449,
     465,     0,   211,     0,     0,   250,     0,   260,   263,     0,
       0,   372,   360,     0,   376,   366,   374,    31,    18,   280,
       0,     0,     0,     0,   268,     0,   412,   413,   418,     0,
     421,     0,   362,   419,   117,   380,     0,     0,    76,     0,
       0,   345,   358,   357,   360,     0,    92,     0,     0,   138,
     136,     0,   141,     0,     0,   158,   160,    28,    31,     0,
       0,   180,   178,     0,     0,   182,   184,   177,    31,     0,
     225,     0,   257,   253,     0,   259,     0,   261,   235,     0,
     361,     0,   367,   375,   377,   282,   281,   239,   237,   271,
     246,     0,     0,     0,   381,   119,   121,   120,   382,     0,
     353,     0,     0,    91,   379,   379,     0,     0,   137,   133,
     379,     0,     0,     0,    29,    31,   183,     0,     0,   186,
       0,   438,     0,     0,   252,   262,   258,     0,   368,   197,
     201,     0,   198,   200,   283,     0,     0,     0,     0,   242,
     419,     0,   422,   383,   210,   278,   370,   138,   138,   139,
       0,   134,   165,   136,   136,    30,     0,   185,   178,     0,
     187,   189,   177,   247,     0,   236,   369,   195,     0,   464,
       0,   245,   238,     0,   243,   371,   127,   127,     0,   290,
       0,   146,   147,     0,     0,     0,     0,     0,   188,     0,
     191,   248,   196,   199,   244,   240,     0,     0,   127,     0,
     360,   360,     0,   145,     0,   360,   379,   379,   190,   178,
       0,   143,   122,   142,   128,   123,     0,     0,   144,   360,
       0,   138,   138,     0,     0,     0,     0,     0,     0,     0,
       0,   149,   148,     0,   150,   166,   167,     0,   151,   129
};

/* YYPDEFGOTO[NTERM-NUM]. */
static const short int yydefgoto[] =
{
      -1,     2,   194,   195,   410,   138,   472,   473,   474,   196,
     197,   402,   403,   279,   404,   283,     3,     4,    12,    30,
      31,    32,    33,    34,    35,    36,    37,   199,    39,    40,
     264,   392,   152,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   336,   337,   685,   117,   361,   257,   787,
     788,   544,   545,   546,   547,   697,   698,   394,   802,   118,
     770,   771,   772,   119,   120,    41,   266,   267,   396,   555,
     556,   557,   558,   142,    43,   143,   144,   284,   413,   414,
     415,   655,   656,   750,   751,   416,   720,   721,   722,   201,
     202,   203,   204,   205,   206,   207,   208,   209,   210,   211,
     212,   213,   678,   727,   728,   729,   214,   463,   713,   464,
     465,   215,   666,   467,   599,   216,   297,   217,   218,   349,
     350,   351,   219,   479,   220,   221,   222,   285,   224,   148,
       8,   226,    10,   227,   228,   560,   229,   230,   231,   286,
     519,   520,   233,   287,   308,   605,   606,   503,   504,   235,
     420,   236,   288,   289,   290,   423,   240,   291,   292,   243,
     425,   244,   245,   246,   247,   325,   326,   327,   328,   311,
     518,   294,   428,   295,   296,   760,   249,   456
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -670
static const short int yypact[] =
{
      44,    49,   106,  -670,  -670,    47,  -670,   -20,  -670,    53,
    -670,  -670,  1084,  -670,  -670,  -670,  -670,  3629,  -670,  -670,
      28,  -670,  -670,    54,  -670,  -670,  -670,  -670,  1296,  -670,
    -670,  1296,  -670,  -670,  -670,  -670,  -670,  -670,   162,    75,
    -670,  -670,    55,  -670,    97,  -670,  -670,  -670,  -670,  -670,
    -670,  -670,  -670,  -670,  -670,  -670,  -670,  -670,  -670,  -670,
    -670,  -670,  -670,  -670,  -670,  -670,  -670,  -670,  -670,  -670,
    -670,  -670,  -670,  -670,  -670,  2811,  -670,   194,   333,  -670,
    -670,   134,   134,  -670,   913,   189,   274,  -670,  -670,  -670,
    -670,  -670,  -670,  -670,  -670,  -670,  -670,  -670,   209,  -670,
    -670,  -670,  -670,  -670,  -670,  1743,  -670,   226,  2893,  -670,
    -670,  -670,  -670,  -670,  3605,  3650,  -670,  -670,  -670,  -670,
    -670,  -670,  -670,   234,  -670,   134,   345,   371,  -670,  -670,
    -670,   134,  -670,  -670,  -670,  -670,  -670,   134,  -670,   134,
    -670,  -670,   134,   306,   318,   340,  -670,  -670,   147,  -670,
    -670,  -670,  -670,  -670,   334,  2665,   134,  2665,   134,  2199,
    3607,   350,   364,   365,   373,  -670,   135,  2665,   347,   374,
     377,   349,  -670,  2665,   209,   372,   378,  -670,  -670,  -670,
    -670,  -670,  -670,  -670,  -670,  2665,  2665,  -670,  2665,  2665,
    2665,  2665,  2665,  2665,  -670,   134,    36,   375,   379,   445,
     381,  -670,   386,  1835,  -670,  -670,  -670,  -670,  -670,  -670,
    -670,  -670,   382,  -670,  -670,  -670,  -670,  -670,   197,  -670,
    -670,   384,   393,    22,   388,   383,   254,  -670,  -670,    56,
     395,   398,   255,   373,   288,   129,  -670,   292,   302,  1595,
    -670,   303,   320,  -670,  -670,  -670,  -670,  -670,  -670,  -670,
    -670,   244,  -670,   134,   134,  -670,    64,    64,   400,  -670,
    -670,   397,   257,  3607,   451,  3607,   404,  -670,  -670,  -670,
     402,   161,   161,   177,    75,   913,  -670,   161,  2665,   161,
    -670,  -670,  -670,   379,   381,   411,  -670,  -670,  -670,  -670,
    1595,  -670,  -670,  1588,   231,  -670,  -670,   406,  -670,  3031,
     408,   272,   422,   134,  2432,   103,  2665,  2665,  3607,  2665,
     415,   412,  3480,    64,  2665,  2665,  2665,  3057,  -670,   487,
    2665,   426,   278,  3090,   423,   425,   429,   427,   430,  2965,
    -670,  -670,  -670,  -670,  -670,  -670,   149,  -670,   434,  -670,
      51,   492,   134,   202,  -670,  -670,  -670,   441,   209,   496,
     508,  -670,  -670,  2665,  2665,    64,  2199,  -670,  -670,  -670,
    -670,  -670,  -670,  -670,    64,  2665,  3607,  -670,  -670,  2665,
    2665,  2665,  2665,  2665,  2665,  2665,  2665,  2665,  2665,  2665,
    2665,   345,   371,   293,   446,   228,   450,    25,  -670,  -670,
    -670,  3607,    28,   448,  -670,  2975,  -670,  -670,  -670,  -670,
    -670,  2505,  -670,   452,  -670,  -670,  2665,  -670,  -670,  -670,
    -670,  -670,   134,   449,  -670,  -670,   453,   119,    58,   191,
    -670,    83,  -670,  -670,   137,  -670,   180,   231,  -670,   190,
    -670,  3607,  -670,  2665,  2665,  2665,  2665,  2665,  2665,  2665,
    2665,  2665,  2665,  2665,  2665,  2665,  2665,  2665,  2665,  2665,
    2665,  2665,  2665,  2665,  -670,  -670,  2665,  -670,   457,   354,
    3607,  -670,   454,  2665,   459,  -670,   134,   458,   300,   469,
    3113,  3146,   235,  -670,  -670,  -670,  -670,   340,   300,   462,
    1363,  2665,  -670,  -670,  3179,  3202,  3228,  -670,  -670,  3261,
    2665,  2738,  -670,  -670,  -670,  2665,  -670,  2665,  2665,  2665,
     134,  -670,   456,   460,   465,  -670,  -670,  -670,  -670,   345,
     464,  -670,   526,  -670,  -670,  -670,   470,  -670,  3290,   472,
    -670,  -670,  -670,  -670,   475,   478,  3480,  3480,  3480,  3480,
    3480,  3480,  3480,  3480,  3480,  3480,  3480,  3480,   451,   404,
    -670,    25,    25,  -670,   479,  -670,   480,  3607,  -670,  -670,
    3607,    28,  -670,  -670,  3605,   482,  2975,  -670,  -670,  -670,
     483,  3006,  -670,  -670,   313,  2665,  -670,  2665,  -670,   335,
     335,  3313,  3341,  1300,  2297,  2226,  3503,  1096,    41,    41,
     335,   335,  -670,  -670,  -670,    84,    84,  -670,  -670,  -670,
    -670,  2665,  -670,  2665,   490,  -670,   469,   529,  -670,  2665,
    2199,  -670,  2665,  2505,  -670,   488,   491,  -670,   201,  -670,
    2199,   498,   497,  2199,  -670,  2199,  -670,  -670,  -670,  3372,
    -670,  3396,  -670,  3421,  -670,   543,  2592,   499,   451,    64,
     502,  -670,  -670,  -670,  2665,    28,  -670,   505,   506,   527,
     526,   134,  -670,    64,    64,  -670,  -670,   201,  -670,   511,
    2665,  -670,   453,   134,   507,  -670,  -670,    27,   337,  2665,
    -670,  3455,  -670,  2665,  2665,  -670,  2199,  3480,   573,   517,
    1447,  2286,  -670,  -670,   201,  2665,  -670,   326,  -670,  -670,
    -670,  2665,  2665,  2665,  -670,  -670,  -670,  3480,   563,    28,
    -670,   209,   523,  -670,   434,   434,  3607,   521,  -670,  -670,
     434,   363,   528,   530,   201,  -670,  -670,   344,  2665,  -670,
    2665,  3480,   524,   534,  -670,  3480,  -670,  2199,    28,  -670,
    -670,   139,  -670,  3480,  -670,  2665,   515,   533,  1927,   326,
    3480,  3421,  3480,  -670,  -670,  -670,    28,   527,   527,  -670,
    2019,  -670,  -670,    25,    25,   201,  2665,  -670,    27,   535,
    -670,  -670,    -9,  -670,  2199,  -670,  -670,  -670,  2359,  3480,
     531,  -670,  -670,  1556,  -670,  -670,   574,   574,   200,   540,
    2111,  -670,  -670,   539,    32,   214,   547,   551,  -670,  2665,
    -670,  -670,  -670,  -670,  -670,  -670,   556,   289,   574,   289,
    2665,  2665,   554,  -670,   558,  2665,   434,   434,  -670,    -9,
     134,  -670,  -670,  -670,  -670,  -670,   559,   560,  -670,  2665,
     561,   527,   527,   553,   557,   564,   566,   565,   567,   569,
     134,  -670,  -670,   570,  -670,  -670,  -670,   568,  -670,  -670
};

/* YYPGOTO[NTERM-NUM].  */
static const short int yypgoto[] =
{
    -670,  -670,  -670,   687,  -670,  -254,   275,  -670,  -670,   -73,
      34,  -225,  -447,  -127,  -670,    52,  -670,  -670,  -670,   376,
    -670,   385,  -670,  -670,  -670,  -670,  -670,    14,  -670,   608,
    -344,  -489,   -17,  -670,  -670,   545,  -360,  -670,  -670,  -670,
    -359,  -670,   542,  -252,   155,  -670,  -670,  -241,   108,  -632,
    -670,  -465,    20,   157,  -670,  -658,  -670,  -363,  -121,  -670,
    -670,  -670,  -670,  -670,  -670,  -670,   291,  -670,   131,   115,
    -670,  -670,  -670,   -11,  -670,   399,  -670,   -52,  -537,  -385,
    -559,  -612,  -669,   -71,  -102,  -538,  -455,  -670,   -80,   -65,
    -666,   -49,  -198,  -670,   380,   -93,  -670,  -670,  -670,  -670,
    -295,  -670,  -670,   -81,   -46,  -670,  -670,  -670,  -670,  -670,
    -547,  -670,  -670,  -670,    89,  -670,   532,  -670,  -670,   336,
    -670,   338,  -670,  -469,  -670,  -222,  -670,   156,   673,    -1,
     -74,   468,  -670,    -7,  -670,   339,   -47,   -36,  -670,   141,
    -342,  -167,  -670,   227,   481,    85,  -670,  -636,  -670,  -670,
    -670,   100,   307,   391,   593,  -670,   322,   522,   653,   217,
    -670,   438,  -670,  -670,  -438,   210,  -670,   215,  -670,   250,
    1059,   525,  -670,   461,   654,  -670,  -670,  -670
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -447
static const short int yytable[] =
{
       7,    42,    19,    76,   383,   345,   651,   126,   127,   462,
     121,   139,   611,   271,   272,   384,   386,    42,   277,   324,
      42,   563,    78,   524,   411,   604,    38,   652,   548,   657,
     654,   225,   145,   153,   607,   553,   554,   538,   706,   747,
     258,   139,    38,   121,   543,    38,   662,   405,   339,   635,
       5,   459,   409,   200,   430,    77,   417,   268,   737,   738,
      14,    15,   145,   269,   741,    16,   302,   360,   431,    17,
     271,   272,   483,   277,   773,   566,   637,   638,    21,   766,
     767,   794,   298,     1,   298,   225,   505,   139,   -11,   282,
      17,   360,   310,    17,    22,   354,   709,   780,     6,  -131,
     506,    17,     6,    75,   792,    24,    11,   507,   145,   318,
     747,   566,   657,   654,   521,     6,   714,    25,   140,    18,
       6,   338,     6,   523,   262,   433,   434,    26,    13,   225,
       6,   354,   709,   200,   706,   789,   141,  -194,   270,   689,
      84,   273,  -194,   444,   445,   446,   447,   448,   140,   651,
     672,   200,   411,   818,   819,   362,   804,   198,    27,   604,
     811,   812,  -387,  -194,  -194,   628,   141,  -387,   607,     6,
     748,   686,   752,   749,    81,    85,   451,   452,   562,   381,
     382,   453,   385,   362,   417,    29,   517,   642,  -387,  -387,
     139,    82,   139,   397,   140,   270,   553,   554,  -386,   352,
       6,     6,   139,  -386,   398,   399,   724,   347,   752,   749,
     309,   145,   141,   145,   400,   757,  -395,   348,   758,    19,
     651,  -395,   363,   145,  -386,  -386,   200,    14,   500,   338,
     501,   469,   278,   397,   517,   139,   282,   198,   401,   362,
     146,   799,  -395,  -395,   398,   399,   232,   367,   368,   363,
     363,   510,   200,   466,   400,   198,   477,    14,   511,  -408,
     669,   223,   406,   522,  -408,   563,    16,   412,   509,  -446,
     146,    14,   149,   790,  -446,   122,   567,   419,   776,   777,
     313,   362,   225,   513,   105,  -408,  -408,   795,    38,   357,
     362,    17,   692,   139,   364,  -446,  -446,   140,   462,   140,
     232,  -124,   250,   358,   468,   502,   363,   478,   602,   140,
     359,   411,   603,   411,   477,   141,   146,   141,   139,    75,
      18,   449,   450,   451,   452,   151,   261,   141,   453,  -233,
     198,   622,   234,   739,  -233,   725,  -233,    14,   389,   145,
     726,   322,   475,   417,   232,   417,   608,   105,   363,    75,
     150,   354,   491,   281,   200,   151,   198,   363,   139,   223,
     476,   563,  -234,   263,   105,   478,  -231,  -234,   462,  -234,
     801,  -231,   500,  -231,   540,   549,  -232,  -229,   421,   145,
      14,  -232,  -229,  -232,  -229,   278,   234,   139,   690,   265,
     274,   701,   596,    14,  -230,   412,   411,   275,   650,  -230,
     475,  -230,   702,   703,    79,   419,   316,    80,   145,   105,
     280,   564,   237,   123,   124,   281,   125,    14,   476,   146,
     276,   146,   710,   304,    14,   140,   338,   313,   417,   746,
     234,   146,   647,   500,   418,   592,   517,   305,   306,   446,
     447,   448,   500,   141,   742,   232,   307,   314,   806,   807,
     315,   320,   319,   810,   411,   340,   411,   342,   198,   341,
     223,   343,   344,   346,   352,   140,   237,   816,   355,     9,
     353,   365,   356,   387,   139,   364,   391,   139,   388,   395,
     674,   139,    14,   141,   354,   458,   417,   454,   417,   457,
     481,     9,   411,   482,   140,   145,   238,   232,   145,   488,
     490,   493,   145,   494,   508,   496,   421,   668,   495,   497,
     237,   502,   141,   200,   512,   200,   348,   675,   347,   541,
     679,   704,   680,   542,   417,   411,   225,   550,   565,   401,
     591,   234,   597,   593,   625,   598,   225,   566,   609,   225,
     595,   225,   627,   626,   629,   543,   632,   146,   631,   633,
     238,   634,     9,   639,   412,   362,   653,   417,   645,   640,
     665,   648,   418,   671,   419,   345,   658,   700,   603,   385,
     362,   663,   677,   716,   676,   684,   691,   688,   745,   694,
     695,   140,     9,   234,   140,   696,   708,   146,   140,   705,
     717,   718,   225,     9,   238,   733,   740,   736,   200,   141,
     424,   743,   141,   744,   761,   753,   141,     9,   754,   762,
       9,   237,   786,   791,   779,   793,   146,   198,   693,   198,
     784,   796,   363,   139,   755,   797,   735,   241,     9,   800,
     808,   809,   820,   814,   815,   817,   363,   363,   821,   653,
     823,   525,   829,   225,   145,   822,   824,    83,   825,   658,
     826,   828,   707,   252,   225,   624,   200,   260,   200,   407,
     699,   781,   644,   237,     9,   421,   225,   421,   805,   630,
     636,   646,   734,   539,   408,   778,   200,   798,   783,   763,
     225,   241,   785,   764,   461,   664,   515,   514,   200,   225,
     300,   673,   516,   774,   200,   238,   225,   653,   239,   653,
     362,   756,   198,   146,   775,   618,   146,   658,   617,   658,
     146,   200,   620,   594,   366,     0,   426,     0,   200,   765,
       0,   418,   803,   418,   803,   241,   813,   200,   424,     0,
     140,     9,     0,     9,   232,   653,     0,     0,     0,   429,
       0,   232,     0,     9,     0,   658,   827,   238,   141,     0,
     421,   232,   239,     0,   232,     0,   232,   147,   242,   248,
     198,     0,   198,     0,     0,     0,     0,   363,   653,     0,
       0,   137,     0,     9,     0,     0,     9,     0,   658,     0,
     198,   330,   331,   332,   333,   334,   335,   147,     0,     0,
       0,     0,   198,     0,     0,     0,   239,     0,   198,     0,
       0,   256,     0,   427,   232,     0,   418,   232,   421,     0,
     421,     0,   242,   248,     0,   198,     0,     0,     0,     0,
     234,     0,   198,     0,     0,     0,   241,   234,     0,     0,
       0,   198,     0,   147,     9,     0,     0,   234,     0,     0,
     234,     0,   234,     0,   426,     0,   421,   303,     0,     0,
       0,     0,   146,     0,     0,     0,   242,   248,   232,     9,
       0,     0,     0,     0,   418,     0,   418,   429,     0,   232,
       0,   422,   321,     0,     0,     0,     0,     0,   241,   421,
       9,   232,     0,     0,   223,     0,     0,   424,     0,   424,
     234,     0,     0,   234,     0,   232,   223,   239,     0,     9,
     237,     0,   418,     0,   232,     0,     0,   237,     0,     0,
       0,   232,     0,     0,     0,     0,     0,   237,   128,   223,
     237,   129,   237,     0,   130,   131,   223,     0,     9,   132,
       0,   427,     0,     0,   133,   418,   147,     0,   147,     0,
       0,   134,     0,     0,   234,     0,   135,     0,   147,   239,
     390,     0,   393,     0,     0,   234,     0,   242,   248,     0,
     136,     0,   137,     0,     0,     0,     0,   234,    27,     0,
     237,     0,   424,   237,   585,   586,   587,   588,   589,     6,
       0,   234,     0,     0,   238,     0,     0,     0,     0,     0,
     234,   238,     0,     0,     0,    29,     0,   234,     0,   422,
       0,   238,     0,   426,   238,   426,   238,     0,     0,   242,
     248,     0,     0,     0,     0,     9,     0,     0,     9,     0,
       0,     0,     9,     0,   237,     0,   429,     0,   429,     0,
     424,     0,   424,     0,     0,   237,     0,     0,     0,     0,
       0,     0,   422,   422,   422,   422,   422,   237,     0,   590,
       0,     0,     0,     0,   238,     0,     0,   238,     0,     0,
       0,   237,     0,     0,   147,     0,     0,     0,   424,     0,
     237,     0,     0,     0,     0,     0,     0,   237,   393,     0,
       0,     0,     0,   616,   -50,    20,     0,    21,   426,     0,
     427,     0,   427,     0,     0,     0,   -65,     0,     0,     0,
       0,   424,     0,    22,   147,     0,     0,     0,   238,     0,
      23,   429,     0,   -65,    24,   241,     0,     0,   568,   238,
       0,     9,   241,   431,     0,     0,    25,     0,     0,     0,
       0,   238,   241,   147,     0,   241,    26,   241,     0,    27,
       0,     0,     0,     0,     0,   238,   426,   303,   426,     0,
       0,     0,     0,     0,   238,     0,     0,     0,   422,     0,
     422,   238,     0,     0,     9,    28,    29,     0,     0,   429,
       0,   429,     0,     0,     0,   427,     0,     0,     0,     0,
     433,   434,     0,     0,   426,   241,   239,     0,   241,     0,
     437,   438,   439,   239,   440,     0,   442,   443,   444,   445,
     446,   447,   448,   239,     0,     0,   239,   429,   239,     0,
       0,     0,     0,     0,   293,     0,   299,   426,     0,     0,
     147,     0,     0,   147,     0,     0,   312,   147,     0,     0,
       0,     0,   317,   427,   641,   427,     0,   393,     0,   241,
     429,   643,     0,   422,   323,   329,   242,   248,     0,     0,
     241,     0,     0,   242,   248,     0,   239,     0,     0,   239,
       0,     0,   241,   242,   248,     0,   242,   248,   242,   248,
       0,   427,     0,     0,     0,     0,   241,     0,     0,     0,
       0,     0,     0,     0,     0,   241,     0,     0,     0,     0,
       0,     0,   241,     0,     0,     0,   -50,    20,     0,    21,
       0,   422,     0,   422,   427,     0,     0,     0,   -65,     0,
     239,     0,     0,     0,     0,    22,   242,   248,     0,   242,
     248,   239,     0,     0,     0,   -65,    24,   431,     0,     0,
       0,     0,     0,   239,     0,     0,     0,     0,    25,   422,
       0,     0,     0,     0,     0,     0,     0,   239,    26,     0,
       0,    27,     0,     0,     0,     0,   239,     0,     0,     0,
       0,     0,     0,   239,     0,   470,   471,     0,   480,   147,
     242,   248,   422,   484,   485,   486,     0,    28,    29,   489,
       0,   242,   248,   393,   433,   434,     0,     0,     0,     0,
     431,     0,     0,   242,   248,     0,     0,     0,     0,     0,
     442,   443,   444,   445,   446,   447,   448,   242,   248,     0,
       0,     0,     0,     0,     0,     0,   242,   248,     0,     0,
       0,     0,     0,   242,   248,     0,     0,     0,   526,   527,
     528,   529,   530,   531,   532,   533,   534,   535,   536,   537,
       0,     0,     0,     0,     0,     0,     0,   433,   434,     0,
       0,   435,     0,     0,     0,     0,     0,   437,   438,   439,
     561,   440,   441,   442,   443,   444,   445,   446,   447,   448,
       0,     0,     0,     0,   431,     0,     0,     0,     0,     0,
       0,     0,     0,   610,     0,     0,     0,     0,     0,     0,
       0,     0,   569,   570,   571,   572,   573,   574,   575,   576,
     577,   578,   579,   580,   581,   582,   583,   584,     0,     0,
       0,     0,     0,   649,     0,     0,     0,     0,     0,     0,
       0,     0,   312,     0,     0,     0,   498,     0,     0,     0,
       0,   433,   434,     0,     0,   435,   499,     0,     0,     0,
     480,   437,   438,   439,     0,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   619,     0,   621,   301,   623,    21,
     155,   128,   156,   157,   129,   725,     0,   130,   -65,   158,
     726,   159,   132,     0,     0,   160,     0,   133,   161,   162,
     163,     0,     0,     0,   134,     0,    24,     0,     0,   135,
       0,   164,   165,    17,     0,     0,     0,     0,    25,     0,
       0,   166,   167,   136,     0,   168,   169,   170,    26,   171,
       0,    27,   172,   173,     0,   431,   174,   175,     0,   176,
     177,   178,     6,   179,   180,   181,   182,   183,   184,   185,
       0,   105,  -239,   186,     0,     0,     0,   187,    29,     0,
       0,     0,   188,   189,     0,     0,   190,   191,     0,     0,
     661,     0,     0,     0,     0,     0,     0,     0,   667,     0,
       0,     0,   670,     0,     0,     0,     0,     0,     0,   432,
       0,     0,   433,   434,   192,   193,   435,   436,   369,     0,
       0,     0,   437,   438,   439,   687,   440,   441,   442,   443,
     444,   445,   446,   447,   448,     0,     0,     0,     0,     0,
       0,     0,   370,   371,   372,   373,   374,   375,   376,   377,
     378,   379,   380,     0,     0,     0,     0,     0,   711,     0,
       0,     0,     0,   715,     0,     0,     0,     0,     0,     0,
     723,     0,     0,     0,   480,     0,     0,     0,     0,     0,
     730,   731,   732,     0,   154,     0,    21,   155,   128,   156,
     157,   129,     0,     0,   130,   -65,   158,     0,   159,   132,
       0,     0,   160,     0,   133,   161,   162,   163,     0,     0,
       0,   134,     0,    24,     0,     0,   135,     0,   164,   165,
      17,     0,     0,     0,   759,    25,     0,     0,   166,   167,
     136,     0,   168,   169,   170,    26,   171,     0,    27,   172,
     173,     0,     0,   174,   175,     0,   176,   177,   178,     6,
     179,   180,   181,   182,   183,   184,   185,   723,   105,  -204,
     186,     0,     0,     0,   187,    29,     0,     0,     0,   188,
     189,     0,     0,   190,   191,     0,   301,     0,    21,   155,
     128,   156,   157,   129,     0,     0,   130,   -65,   158,     0,
     159,   132,     0,     0,   160,     0,   133,   161,   162,   163,
       0,   192,   193,   134,     0,    24,     0,     0,   135,     0,
     164,   165,    17,     0,     0,     0,     0,    25,     0,     0,
     166,   167,   136,     0,   168,   169,   170,    26,   171,     0,
      27,   172,   173,     0,     0,   174,   175,     0,   176,   177,
     178,     6,   179,   180,   181,   182,   183,   184,   185,     0,
     105,  -203,   186,     0,     0,     0,   187,    29,     0,     0,
       0,   188,   189,     0,     0,   190,   191,     0,   301,     0,
      21,   155,   128,   156,   157,   129,     0,     0,   130,   -65,
     158,     0,   159,   132,     0,     0,   160,     0,   133,   161,
     162,   163,     0,   192,   193,   134,     0,    24,     0,     0,
     135,     0,   164,   165,    17,     0,     0,     0,     0,    25,
       0,     0,   166,   167,   136,     0,   168,   169,   170,    26,
     171,     0,    27,   172,   173,     0,     0,   174,   175,     0,
     176,   177,   178,     6,   179,   180,   181,   182,   183,   184,
     185,     0,   105,  -241,   186,     0,     0,     0,   187,    29,
       0,     0,     0,   188,   189,     0,     0,   190,   191,     0,
     301,     0,    21,   155,   128,   156,   157,   129,     0,     0,
     130,   -65,   158,     0,   159,   132,     0,     0,   160,     0,
     133,   161,   162,   163,     0,   192,   193,   134,     0,    24,
       0,     0,   135,     0,   164,   165,    17,     0,     0,     0,
       0,    25,     0,     0,   166,   167,   136,     0,   768,   169,
     170,    26,   171,     0,    27,   769,   173,     0,     0,   174,
     175,     0,   176,   177,   178,     6,   179,   180,   181,   182,
     183,   184,   185,     0,   105,  -204,   186,     0,     0,     0,
     187,    29,     0,     0,     0,   188,   189,     0,     0,   190,
     191,     0,   301,     0,    21,   155,   128,   156,   157,   129,
       0,     0,   130,   -65,   158,     0,   159,   132,     0,     0,
     160,     0,   133,   161,   162,   163,     0,   192,   193,   134,
       0,    24,     0,     0,   135,     0,   164,   165,    17,     0,
       0,     0,     0,    25,     0,     0,   166,   167,   136,     0,
     168,   169,   170,    26,   171,     0,    27,   172,   173,     0,
       0,   174,   175,     0,   176,   177,   178,     6,   179,   180,
     181,   182,   183,   184,   185,     0,   105,  -204,   186,     0,
       0,     0,   187,    29,     0,     0,     0,   188,   189,     0,
     301,   190,   191,   155,   128,   156,   157,   129,     0,     0,
     130,     0,   158,     0,   159,   132,     0,     0,     0,     0,
     133,   161,   162,   163,     0,     0,     0,   134,     0,   192,
     193,     0,   135,     0,   164,   165,    17,     0,     0,     0,
       0,     0,     0,     0,   166,   167,   136,     0,   168,   169,
     170,     0,   171,   431,    27,   172,   173,     0,     0,   174,
     175,     0,   176,   177,   178,     6,   179,   180,   181,   182,
     183,   184,   185,     0,   105,     0,   186,     0,     0,     0,
     187,    29,     0,     0,     0,   188,   189,     0,     0,   190,
     191,   128,     0,   157,   129,     0,     0,   130,     0,     0,
       0,     0,   132,     0,     0,     0,     0,   133,     0,     0,
     433,   434,     0,     0,   134,     0,     0,   192,   193,   135,
     437,   164,   165,    17,   431,     0,   442,   443,   444,   445,
     446,   447,   448,   136,     0,   168,     0,     0,     0,     0,
       0,    27,   172,     0,     0,     0,     0,   175,     0,     0,
     177,   178,     6,   179,   180,   181,   182,   183,   184,   185,
       0,   671,   719,   186,   128,     0,   157,   129,    29,     0,
     130,     0,   188,   189,     0,   132,   190,   191,     0,     0,
     133,   433,   434,     0,     0,     0,     0,   134,     0,     0,
       0,   437,   135,   439,   164,   165,    17,   442,   443,   444,
     445,   446,   447,   448,   192,   193,   136,     0,   168,     0,
       0,     0,     0,     0,    27,   172,     0,     0,     0,     0,
     175,     0,     0,   177,   178,     6,   179,   180,   181,   182,
     183,   184,   185,     0,   671,   782,   186,   128,     0,   157,
     129,    29,     0,   130,     0,   188,   189,     0,   132,   190,
     191,   460,     0,   133,     0,     0,     0,     0,     0,     0,
     134,     0,     0,     0,     0,   135,     0,   164,   165,    17,
       0,     0,     0,     0,     0,     0,     0,   192,   193,   136,
       0,   168,     0,     0,     0,     0,     0,    27,   172,     0,
       0,     0,     0,   175,     0,     0,   177,   178,     6,   179,
     180,   181,   182,   183,   184,   185,     0,     0,     0,   186,
     128,     0,   157,   129,    29,     0,   130,     0,   188,   189,
       0,   132,   190,   191,     0,     0,   133,     0,     0,     0,
       0,     0,     0,   134,     0,     0,     0,     0,   135,     0,
     164,   165,    17,     0,     0,     0,     0,     0,     0,     0,
     192,   193,   136,     0,   168,     0,     0,     0,     0,     0,
      27,   172,     0,     0,     0,     0,   175,     0,     0,   177,
     178,     6,   179,   180,   181,   182,   183,   184,   185,     0,
       0,     0,   186,   559,     0,     0,     0,    29,     0,     0,
       0,   188,   189,     0,     0,   190,   191,   128,     0,   157,
     129,     0,     0,   130,     0,     0,     0,     0,   132,     0,
       0,     0,     0,   133,     0,     0,     0,     0,     0,     0,
     134,     0,     0,   192,   193,   135,     0,   164,   165,    17,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   136,
       0,   168,     0,     0,     0,     0,     0,    27,   172,     0,
       0,     0,     0,   175,     0,     0,   177,   178,     6,   179,
     180,   181,   182,   183,   184,   185,     0,   671,     0,   186,
     128,     0,   157,   129,    29,     0,   130,     0,   188,   189,
       0,   132,   190,   191,     0,     0,   133,     0,     0,     0,
       0,     0,     0,   134,     0,     0,     0,     0,   135,     0,
     164,   165,    17,     0,     0,     0,     0,     0,     0,     0,
     192,   193,   136,     0,   168,     0,     0,     0,     0,     0,
      27,   172,     0,     0,     0,     0,   175,     0,     0,   177,
     178,     6,   179,   180,   181,   182,   183,   184,   185,     0,
       0,     0,   186,   128,     0,     0,   129,    29,     0,   130,
       0,   188,   189,     0,   132,   190,   191,     0,     0,   133,
       0,     0,     0,     0,     0,     0,   134,     0,     0,     0,
       0,   135,     0,   164,   165,    17,     0,     0,     0,     0,
       0,     0,     0,   192,   193,   136,     0,   168,     0,     0,
       0,     0,     0,    27,   172,     0,     0,     0,     0,   175,
       0,     0,   177,   178,     6,   179,   180,   181,   182,   183,
     184,   185,    86,     0,    87,     0,   -95,     0,     0,   -95,
      29,     0,   -95,   -95,   188,   189,     0,   -95,     0,     0,
      88,     0,   -95,     0,     0,     0,     0,     0,     0,   -95,
     -95,    89,    90,    91,   -95,    92,     0,     0,     0,     0,
       0,    93,    94,    95,    96,    97,     0,     0,   -95,    98,
       0,     0,    99,   100,   101,   102,   -95,     0,     0,     0,
     103,     0,   -95,   104,     0,     0,     0,   -95,     0,     0,
       0,     0,     0,     0,     0,     0,   105,   -79,     0,     0,
       0,     0,   106,   -95,   251,     0,    87,     0,   -95,     0,
       0,   -95,     0,     0,   -95,   -95,     0,     0,     0,   -95,
       0,     0,    88,     0,   -95,     0,     0,     0,     0,     0,
       0,   -95,   -95,    89,    90,    91,   -95,    92,     0,     0,
       0,     0,     0,    93,    94,    95,    96,    97,     0,     0,
     -95,    98,     0,     0,    99,   100,   101,   102,   -95,     0,
       0,     0,   103,     0,   -95,   104,     0,     0,     0,   -95,
       0,     0,     0,     0,     0,     0,     0,     0,   105,   -78,
       0,     0,     0,     0,   106,   -95,   551,     0,    87,     0,
     -95,     0,     0,   -95,     0,     0,   -95,   -95,     0,     0,
       0,   -95,   431,     0,    88,     0,   -95,     0,     0,     0,
       0,     0,     0,   -95,   -95,    89,    90,    91,   -95,    92,
       0,     0,     0,     0,     0,    93,    94,    95,    96,    97,
       0,     0,   -95,   259,     0,     0,    99,   100,   101,   102,
     -95,     0,     0,   431,   103,     0,   -95,   104,     0,     0,
       0,   -95,     0,     0,   498,     0,     0,     0,     0,   433,
     434,  -159,     0,   435,   499,     0,   552,   -95,   431,   437,
     438,   439,     0,   440,   441,   442,   443,   444,   445,   446,
     447,   448,   649,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   431,   498,     0,     0,     0,     0,
     433,   434,     0,     0,   435,     0,     0,   455,     0,     0,
     437,   438,   439,     0,   440,   441,   442,   443,   444,   445,
     446,   447,   448,     0,     0,   433,   434,   431,     0,   435,
       0,     0,     0,     0,     0,   437,   438,   439,     0,   440,
     441,   442,   443,   444,   445,   446,   447,   448,   487,     0,
     431,   433,   434,     0,     0,   435,     0,     0,     0,     0,
       0,   437,   438,   439,     0,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   492,     0,     0,     0,     0,     0,
       0,     0,     0,   431,   433,   434,     0,     0,   435,     0,
       0,     0,     0,     0,   437,   438,   439,   600,   440,   441,
     442,   443,   444,   445,   446,   447,   448,   433,   434,     0,
       0,   435,     0,     0,     0,     0,   431,   437,   438,   439,
       0,   440,   441,   442,   443,   444,   445,   446,   447,   448,
     601,     0,     0,     0,     0,     0,     0,     0,     0,   431,
     433,   434,     0,     0,   435,     0,     0,     0,     0,     0,
     437,   438,   439,     0,   440,   441,   442,   443,   444,   445,
     446,   447,   448,   612,     0,   431,     0,     0,     0,     0,
       0,     0,     0,   433,   434,     0,     0,   435,     0,     0,
       0,     0,     0,   437,   438,   439,   613,   440,   441,   442,
     443,   444,   445,   446,   447,   448,   433,   434,   431,     0,
     435,     0,     0,     0,     0,     0,   437,   438,   439,     0,
     440,   441,   442,   443,   444,   445,   446,   447,   448,   614,
       0,     0,   433,   434,     0,     0,   435,   431,     0,     0,
       0,     0,   437,   438,   439,     0,   440,   441,   442,   443,
     444,   445,   446,   447,   448,   615,     0,     0,     0,     0,
     431,     0,     0,     0,     0,   433,   434,     0,     0,   435,
       0,     0,     0,     0,     0,   437,   438,   439,     0,   440,
     441,   442,   443,   444,   445,   446,   447,   448,   431,   498,
       0,     0,     0,     0,   433,   434,     0,     0,   435,     0,
       0,     0,     0,     0,   437,   438,   439,     0,   440,   441,
     442,   443,   444,   445,   446,   447,   448,   433,   434,   431,
       0,   435,   659,     0,     0,     0,     0,   437,   438,   439,
       0,   440,   441,   442,   443,   444,   445,   446,   447,   448,
       0,     0,   660,   431,     0,   433,   434,     0,     0,   435,
       0,     0,     0,     0,     0,   437,   438,   439,     0,   440,
     441,   442,   443,   444,   445,   446,   447,   448,   431,     0,
       0,     0,     0,     0,     0,     0,   433,   434,     0,     0,
     435,   681,     0,     0,     0,     0,   437,   438,   439,     0,
     440,   441,   442,   443,   444,   445,   446,   447,   448,     0,
     433,   434,   431,     0,   435,   682,     0,     0,     0,     0,
     437,   438,   439,     0,   440,   441,   442,   443,   444,   445,
     446,   447,   448,     0,     0,   433,   434,   431,     0,   435,
     683,     0,     0,     0,     0,   437,   438,   439,     0,   440,
     441,   442,   443,   444,   445,   446,   447,   448,     0,   712,
     431,     0,     0,     0,     0,     0,     0,     0,     0,   433,
     434,     0,     0,   435,     0,     0,     0,     0,     0,   437,
     438,   439,     0,   440,   441,   442,   443,   444,   445,   446,
     447,   448,     0,     0,   433,   434,     0,     0,   435,     0,
       0,     0,     0,     0,   437,   438,   439,     0,   440,   441,
     442,   443,   444,   445,   446,   447,   448,   433,   434,     0,
       0,     0,     0,     0,     0,     0,     0,   437,   438,   439,
       0,     0,     0,   442,   443,   444,   445,   446,   447,   448,
     128,     0,   128,   129,     0,   129,   130,   253,   130,     0,
       0,   132,     0,   132,     0,     0,   133,     0,   133,     0,
       0,     0,     0,   134,   254,   134,     0,     0,   135,     0,
     135,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   136,    87,   136,     0,     0,     0,     0,     0,
      27,     0,    27,     0,     0,     0,   255,     0,     0,    88,
       0,     6,     0,     6,     0,     0,     0,     0,     0,     0,
      89,    90,    91,     0,    92,     0,     0,    29,     0,    29,
      93,    94,    95,    96,    97,     0,     0,     0,   259,     0,
       0,    99,   100,   101,   102,     0,    44,     0,     0,   103,
       0,     0,   104,    45,    46,    47,    48,     0,     0,    49,
      50,    51,    52,    53,    54,    55,    56,     0,     0,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74
};

/* YYCONFLP[YYPACT[STATE-NUM]] -- Pointer into YYCONFL of start of
   list of conflicting reductions corresponding to action entry for
   state STATE-NUM in yytable.  0 means no conflicts.  The list in
   yyconfl is terminated by a rule number of 0.  */
static const unsigned char yyconflp[] =
{
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     1,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     3,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0
};

/* YYCONFL[I] -- lists of conflicting rule numbers, each terminated by
   0, pointed into by YYCONFLP.  */
static const short int yyconfl[] =
{
       0,    31,     0,    25,     0
};

static const short int yycheck[] =
{
       1,    12,     9,    20,   256,   203,   565,    81,    82,   304,
      75,    84,   481,   140,   141,   256,   257,    28,   145,   186,
      31,   406,    23,   365,   278,   472,    12,   565,   391,   567,
     567,   105,    84,    98,   472,   395,   395,   381,   650,   708,
     114,   114,    28,   108,    19,    31,   593,   272,    12,   538,
       1,   303,   277,   105,   279,     1,   278,   131,   694,   695,
      80,    81,   114,   137,   700,    12,   159,    35,    27,    37,
     197,   198,   313,   200,   740,    84,   541,   542,     3,   737,
     738,    49,   156,    39,   158,   159,    35,   160,    66,   154,
      37,    35,   166,    37,    19,    73,   105,   106,    66,    74,
      49,    37,    66,    75,   770,    30,     0,    56,   160,   174,
     779,    84,   650,   650,   355,    66,   663,    42,    84,    66,
      66,   195,    66,   364,   125,    84,    85,    52,    81,   203,
      66,    73,   105,   185,   746,   767,    84,    79,   139,   628,
      85,   142,    84,   102,   103,   104,   105,   106,   114,   708,
     605,   203,   406,   811,   812,   229,   788,   105,    55,   606,
     796,   797,    79,   105,   106,   509,   114,    84,   606,    66,
     708,   626,   710,   710,    12,    78,    92,    93,   403,   253,
     254,    97,   256,   257,   406,    82,   353,   550,   105,   106,
     263,    29,   265,    32,   160,   196,   556,   556,    79,    80,
      66,    66,   275,    84,    43,    44,   675,    10,   746,   746,
      75,   263,   160,   265,    53,    76,    79,    20,    79,   226,
     779,    84,   229,   275,   105,   106,   278,    80,    79,   303,
      81,   305,    85,    32,   401,   308,   301,   185,    77,   313,
      84,   779,   105,   106,    43,    44,   105,   118,   119,   256,
     257,    49,   304,   305,    53,   203,   308,    80,    56,    79,
     602,   105,    85,   356,    84,   650,    12,   278,   342,    79,
     114,    80,    83,    73,    84,    81,    85,   278,   743,   744,
      80,   355,   356,   348,    75,   105,   106,    73,   274,    35,
     364,    37,   634,   366,    80,   105,   106,   263,   593,   265,
     159,    73,    76,    49,   305,    77,   313,   308,    73,   275,
      56,   565,    77,   567,   366,   263,   160,   265,   391,    75,
      66,    90,    91,    92,    93,    81,    92,   275,    97,    74,
     278,   498,   105,   696,    79,     9,    81,    80,    81,   391,
      14,   185,   308,   565,   203,   567,   473,    75,   355,    75,
      76,    73,    74,    81,   406,    81,   304,   364,   431,   203,
     308,   746,    74,    18,    75,   366,    74,    79,   663,    81,
      81,    79,    79,    81,    81,   392,    74,    74,   278,   431,
      80,    79,    79,    81,    81,    85,   159,   460,   629,    18,
      84,   643,   466,    80,    74,   406,   650,    79,    85,    79,
     366,    81,   643,   644,    28,   406,    57,    31,   460,    75,
      76,   412,   105,    80,    81,    81,    83,    80,   366,   263,
      80,   265,    85,    73,    80,   391,   500,    80,   650,    85,
     203,   275,   559,    79,   278,    81,   603,    73,    73,   104,
     105,   106,    79,   391,    81,   304,    73,    73,   790,   791,
      73,    73,    80,   795,   708,    80,   710,    12,   406,    80,
     304,    80,    76,    81,    80,   431,   159,   809,    80,     1,
      77,    73,    89,    73,   547,    80,    25,   550,    81,    75,
     607,   554,    80,   431,    73,    63,   708,    81,   710,    81,
      75,    23,   746,    81,   460,   547,   105,   356,   550,    12,
      74,    78,   554,    78,    12,    78,   406,   600,    79,    79,
     203,    77,   460,   565,    73,   567,    20,   610,    10,    73,
     613,   648,   615,    73,   746,   779,   600,    79,    79,    77,
      73,   304,    74,    79,    78,    66,   610,    84,    76,   613,
      81,   615,    77,    83,    80,    19,    74,   391,    78,    74,
     159,    73,    84,    74,   565,   629,   567,   779,    76,    79,
      31,    78,   406,    75,   565,   763,   567,   641,    77,   643,
     644,    81,    75,   666,    76,    32,    74,    78,   705,    74,
      74,   547,   114,   356,   550,    58,    79,   431,   554,    78,
      17,    74,   666,   125,   203,    32,    75,    74,   650,   547,
     278,    73,   550,    73,    89,    81,   554,   139,    74,    76,
     142,   304,    38,    73,    79,    76,   460,   565,   635,   567,
      89,    74,   629,   696,   717,    74,   691,   105,   160,    73,
      76,    73,    79,    74,    74,    74,   643,   644,    81,   650,
      74,   366,    74,   717,   696,    81,    81,    39,    81,   650,
      81,    81,   653,   108,   728,   500,   708,   115,   710,   274,
     640,   754,   554,   356,   196,   565,   740,   567,   789,   512,
     539,   556,   689,   382,   275,   746,   728,   779,   758,   728,
     754,   159,   763,   729,   304,   596,   350,   349,   740,   763,
     158,   606,   353,   740,   746,   304,   770,   708,   105,   710,
     774,   718,   650,   547,   740,   495,   550,   708,   491,   710,
     554,   763,   497,   463,   233,    -1,   278,    -1,   770,   736,
      -1,   565,   787,   567,   789,   203,   800,   779,   406,    -1,
     696,   263,    -1,   265,   593,   746,    -1,    -1,    -1,   278,
      -1,   600,    -1,   275,    -1,   746,   820,   356,   696,    -1,
     650,   610,   159,    -1,   613,    -1,   615,    84,   105,   105,
     708,    -1,   710,    -1,    -1,    -1,    -1,   774,   779,    -1,
      -1,    84,    -1,   305,    -1,    -1,   308,    -1,   779,    -1,
     728,   188,   189,   190,   191,   192,   193,   114,    -1,    -1,
      -1,    -1,   740,    -1,    -1,    -1,   203,    -1,   746,    -1,
      -1,   114,    -1,   278,   663,    -1,   650,   666,   708,    -1,
     710,    -1,   159,   159,    -1,   763,    -1,    -1,    -1,    -1,
     593,    -1,   770,    -1,    -1,    -1,   304,   600,    -1,    -1,
      -1,   779,    -1,   160,   366,    -1,    -1,   610,    -1,    -1,
     613,    -1,   615,    -1,   406,    -1,   746,   160,    -1,    -1,
      -1,    -1,   696,    -1,    -1,    -1,   203,   203,   717,   391,
      -1,    -1,    -1,    -1,   708,    -1,   710,   406,    -1,   728,
      -1,   278,   185,    -1,    -1,    -1,    -1,    -1,   356,   779,
     412,   740,    -1,    -1,   728,    -1,    -1,   565,    -1,   567,
     663,    -1,    -1,   666,    -1,   754,   740,   304,    -1,   431,
     593,    -1,   746,    -1,   763,    -1,    -1,   600,    -1,    -1,
      -1,   770,    -1,    -1,    -1,    -1,    -1,   610,     5,   763,
     613,     8,   615,    -1,    11,    12,   770,    -1,   460,    16,
      -1,   406,    -1,    -1,    21,   779,   263,    -1,   265,    -1,
      -1,    28,    -1,    -1,   717,    -1,    33,    -1,   275,   356,
     263,    -1,   265,    -1,    -1,   728,    -1,   304,   304,    -1,
      47,    -1,   275,    -1,    -1,    -1,    -1,   740,    55,    -1,
     663,    -1,   650,   666,   449,   450,   451,   452,   453,    66,
      -1,   754,    -1,    -1,   593,    -1,    -1,    -1,    -1,    -1,
     763,   600,    -1,    -1,    -1,    82,    -1,   770,    -1,   406,
      -1,   610,    -1,   565,   613,   567,   615,    -1,    -1,   356,
     356,    -1,    -1,    -1,    -1,   547,    -1,    -1,   550,    -1,
      -1,    -1,   554,    -1,   717,    -1,   565,    -1,   567,    -1,
     708,    -1,   710,    -1,    -1,   728,    -1,    -1,    -1,    -1,
      -1,    -1,   449,   450,   451,   452,   453,   740,    -1,   456,
      -1,    -1,    -1,    -1,   663,    -1,    -1,   666,    -1,    -1,
      -1,   754,    -1,    -1,   391,    -1,    -1,    -1,   746,    -1,
     763,    -1,    -1,    -1,    -1,    -1,    -1,   770,   391,    -1,
      -1,    -1,    -1,   490,     0,     1,    -1,     3,   650,    -1,
     565,    -1,   567,    -1,    -1,    -1,    12,    -1,    -1,    -1,
      -1,   779,    -1,    19,   431,    -1,    -1,    -1,   717,    -1,
      26,   650,    -1,    29,    30,   593,    -1,    -1,   431,   728,
      -1,   653,   600,    27,    -1,    -1,    42,    -1,    -1,    -1,
      -1,   740,   610,   460,    -1,   613,    52,   615,    -1,    55,
      -1,    -1,    -1,    -1,    -1,   754,   708,   460,   710,    -1,
      -1,    -1,    -1,    -1,   763,    -1,    -1,    -1,   565,    -1,
     567,   770,    -1,    -1,   696,    81,    82,    -1,    -1,   708,
      -1,   710,    -1,    -1,    -1,   650,    -1,    -1,    -1,    -1,
      84,    85,    -1,    -1,   746,   663,   593,    -1,   666,    -1,
      94,    95,    96,   600,    98,    -1,   100,   101,   102,   103,
     104,   105,   106,   610,    -1,    -1,   613,   746,   615,    -1,
      -1,    -1,    -1,    -1,   155,    -1,   157,   779,    -1,    -1,
     547,    -1,    -1,   550,    -1,    -1,   167,   554,    -1,    -1,
      -1,    -1,   173,   708,   547,   710,    -1,   550,    -1,   717,
     779,   554,    -1,   650,   185,   186,   593,   593,    -1,    -1,
     728,    -1,    -1,   600,   600,    -1,   663,    -1,    -1,   666,
      -1,    -1,   740,   610,   610,    -1,   613,   613,   615,   615,
      -1,   746,    -1,    -1,    -1,    -1,   754,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   763,    -1,    -1,    -1,    -1,
      -1,    -1,   770,    -1,    -1,    -1,     0,     1,    -1,     3,
      -1,   708,    -1,   710,   779,    -1,    -1,    -1,    12,    -1,
     717,    -1,    -1,    -1,    -1,    19,   663,   663,    -1,   666,
     666,   728,    -1,    -1,    -1,    29,    30,    27,    -1,    -1,
      -1,    -1,    -1,   740,    -1,    -1,    -1,    -1,    42,   746,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   754,    52,    -1,
      -1,    55,    -1,    -1,    -1,    -1,   763,    -1,    -1,    -1,
      -1,    -1,    -1,   770,    -1,   306,   307,    -1,   309,   696,
     717,   717,   779,   314,   315,   316,    -1,    81,    82,   320,
      -1,   728,   728,   696,    84,    85,    -1,    -1,    -1,    -1,
      27,    -1,    -1,   740,   740,    -1,    -1,    -1,    -1,    -1,
     100,   101,   102,   103,   104,   105,   106,   754,   754,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   763,   763,    -1,    -1,
      -1,    -1,    -1,   770,   770,    -1,    -1,    -1,   369,   370,
     371,   372,   373,   374,   375,   376,   377,   378,   379,   380,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    84,    85,    -1,
      -1,    88,    -1,    -1,    -1,    -1,    -1,    94,    95,    96,
     401,    98,    99,   100,   101,   102,   103,   104,   105,   106,
      -1,    -1,    -1,    -1,    27,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   120,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   433,   434,   435,   436,   437,   438,   439,   440,
     441,   442,   443,   444,   445,   446,   447,   448,    -1,    -1,
      -1,    -1,    -1,    66,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   463,    -1,    -1,    -1,    79,    -1,    -1,    -1,
      -1,    84,    85,    -1,    -1,    88,    89,    -1,    -1,    -1,
     481,    94,    95,    96,    -1,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   495,    -1,   497,     1,   499,     3,
       4,     5,     6,     7,     8,     9,    -1,    11,    12,    13,
      14,    15,    16,    -1,    -1,    19,    -1,    21,    22,    23,
      24,    -1,    -1,    -1,    28,    -1,    30,    -1,    -1,    33,
      -1,    35,    36,    37,    -1,    -1,    -1,    -1,    42,    -1,
      -1,    45,    46,    47,    -1,    49,    50,    51,    52,    53,
      -1,    55,    56,    57,    -1,    27,    60,    61,    -1,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      -1,    75,    76,    77,    -1,    -1,    -1,    81,    82,    -1,
      -1,    -1,    86,    87,    -1,    -1,    90,    91,    -1,    -1,
     591,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   599,    -1,
      -1,    -1,   603,    -1,    -1,    -1,    -1,    -1,    -1,    81,
      -1,    -1,    84,    85,   118,   119,    88,    89,    83,    -1,
      -1,    -1,    94,    95,    96,   626,    98,    99,   100,   101,
     102,   103,   104,   105,   106,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,    -1,    -1,    -1,    -1,    -1,   659,    -1,
      -1,    -1,    -1,   664,    -1,    -1,    -1,    -1,    -1,    -1,
     671,    -1,    -1,    -1,   675,    -1,    -1,    -1,    -1,    -1,
     681,   682,   683,    -1,     1,    -1,     3,     4,     5,     6,
       7,     8,    -1,    -1,    11,    12,    13,    -1,    15,    16,
      -1,    -1,    19,    -1,    21,    22,    23,    24,    -1,    -1,
      -1,    28,    -1,    30,    -1,    -1,    33,    -1,    35,    36,
      37,    -1,    -1,    -1,   725,    42,    -1,    -1,    45,    46,
      47,    -1,    49,    50,    51,    52,    53,    -1,    55,    56,
      57,    -1,    -1,    60,    61,    -1,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,   758,    75,    76,
      77,    -1,    -1,    -1,    81,    82,    -1,    -1,    -1,    86,
      87,    -1,    -1,    90,    91,    -1,     1,    -1,     3,     4,
       5,     6,     7,     8,    -1,    -1,    11,    12,    13,    -1,
      15,    16,    -1,    -1,    19,    -1,    21,    22,    23,    24,
      -1,   118,   119,    28,    -1,    30,    -1,    -1,    33,    -1,
      35,    36,    37,    -1,    -1,    -1,    -1,    42,    -1,    -1,
      45,    46,    47,    -1,    49,    50,    51,    52,    53,    -1,
      55,    56,    57,    -1,    -1,    60,    61,    -1,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    -1,
      75,    76,    77,    -1,    -1,    -1,    81,    82,    -1,    -1,
      -1,    86,    87,    -1,    -1,    90,    91,    -1,     1,    -1,
       3,     4,     5,     6,     7,     8,    -1,    -1,    11,    12,
      13,    -1,    15,    16,    -1,    -1,    19,    -1,    21,    22,
      23,    24,    -1,   118,   119,    28,    -1,    30,    -1,    -1,
      33,    -1,    35,    36,    37,    -1,    -1,    -1,    -1,    42,
      -1,    -1,    45,    46,    47,    -1,    49,    50,    51,    52,
      53,    -1,    55,    56,    57,    -1,    -1,    60,    61,    -1,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    -1,    75,    76,    77,    -1,    -1,    -1,    81,    82,
      -1,    -1,    -1,    86,    87,    -1,    -1,    90,    91,    -1,
       1,    -1,     3,     4,     5,     6,     7,     8,    -1,    -1,
      11,    12,    13,    -1,    15,    16,    -1,    -1,    19,    -1,
      21,    22,    23,    24,    -1,   118,   119,    28,    -1,    30,
      -1,    -1,    33,    -1,    35,    36,    37,    -1,    -1,    -1,
      -1,    42,    -1,    -1,    45,    46,    47,    -1,    49,    50,
      51,    52,    53,    -1,    55,    56,    57,    -1,    -1,    60,
      61,    -1,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    -1,    75,    76,    77,    -1,    -1,    -1,
      81,    82,    -1,    -1,    -1,    86,    87,    -1,    -1,    90,
      91,    -1,     1,    -1,     3,     4,     5,     6,     7,     8,
      -1,    -1,    11,    12,    13,    -1,    15,    16,    -1,    -1,
      19,    -1,    21,    22,    23,    24,    -1,   118,   119,    28,
      -1,    30,    -1,    -1,    33,    -1,    35,    36,    37,    -1,
      -1,    -1,    -1,    42,    -1,    -1,    45,    46,    47,    -1,
      49,    50,    51,    52,    53,    -1,    55,    56,    57,    -1,
      -1,    60,    61,    -1,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    -1,    75,    76,    77,    -1,
      -1,    -1,    81,    82,    -1,    -1,    -1,    86,    87,    -1,
       1,    90,    91,     4,     5,     6,     7,     8,    -1,    -1,
      11,    -1,    13,    -1,    15,    16,    -1,    -1,    -1,    -1,
      21,    22,    23,    24,    -1,    -1,    -1,    28,    -1,   118,
     119,    -1,    33,    -1,    35,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,    50,
      51,    -1,    53,    27,    55,    56,    57,    -1,    -1,    60,
      61,    -1,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    -1,    75,    -1,    77,    -1,    -1,    -1,
      81,    82,    -1,    -1,    -1,    86,    87,    -1,    -1,    90,
      91,     5,    -1,     7,     8,    -1,    -1,    11,    -1,    -1,
      -1,    -1,    16,    -1,    -1,    -1,    -1,    21,    -1,    -1,
      84,    85,    -1,    -1,    28,    -1,    -1,   118,   119,    33,
      94,    35,    36,    37,    27,    -1,   100,   101,   102,   103,
     104,   105,   106,    47,    -1,    49,    -1,    -1,    -1,    -1,
      -1,    55,    56,    -1,    -1,    -1,    -1,    61,    -1,    -1,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      -1,    75,    76,    77,     5,    -1,     7,     8,    82,    -1,
      11,    -1,    86,    87,    -1,    16,    90,    91,    -1,    -1,
      21,    84,    85,    -1,    -1,    -1,    -1,    28,    -1,    -1,
      -1,    94,    33,    96,    35,    36,    37,   100,   101,   102,
     103,   104,   105,   106,   118,   119,    47,    -1,    49,    -1,
      -1,    -1,    -1,    -1,    55,    56,    -1,    -1,    -1,    -1,
      61,    -1,    -1,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    -1,    75,    76,    77,     5,    -1,     7,
       8,    82,    -1,    11,    -1,    86,    87,    -1,    16,    90,
      91,    19,    -1,    21,    -1,    -1,    -1,    -1,    -1,    -1,
      28,    -1,    -1,    -1,    -1,    33,    -1,    35,    36,    37,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   118,   119,    47,
      -1,    49,    -1,    -1,    -1,    -1,    -1,    55,    56,    -1,
      -1,    -1,    -1,    61,    -1,    -1,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    -1,    -1,    -1,    77,
       5,    -1,     7,     8,    82,    -1,    11,    -1,    86,    87,
      -1,    16,    90,    91,    -1,    -1,    21,    -1,    -1,    -1,
      -1,    -1,    -1,    28,    -1,    -1,    -1,    -1,    33,    -1,
      35,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     118,   119,    47,    -1,    49,    -1,    -1,    -1,    -1,    -1,
      55,    56,    -1,    -1,    -1,    -1,    61,    -1,    -1,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    -1,
      -1,    -1,    77,    78,    -1,    -1,    -1,    82,    -1,    -1,
      -1,    86,    87,    -1,    -1,    90,    91,     5,    -1,     7,
       8,    -1,    -1,    11,    -1,    -1,    -1,    -1,    16,    -1,
      -1,    -1,    -1,    21,    -1,    -1,    -1,    -1,    -1,    -1,
      28,    -1,    -1,   118,   119,    33,    -1,    35,    36,    37,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    47,
      -1,    49,    -1,    -1,    -1,    -1,    -1,    55,    56,    -1,
      -1,    -1,    -1,    61,    -1,    -1,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    -1,    75,    -1,    77,
       5,    -1,     7,     8,    82,    -1,    11,    -1,    86,    87,
      -1,    16,    90,    91,    -1,    -1,    21,    -1,    -1,    -1,
      -1,    -1,    -1,    28,    -1,    -1,    -1,    -1,    33,    -1,
      35,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     118,   119,    47,    -1,    49,    -1,    -1,    -1,    -1,    -1,
      55,    56,    -1,    -1,    -1,    -1,    61,    -1,    -1,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    -1,
      -1,    -1,    77,     5,    -1,    -1,     8,    82,    -1,    11,
      -1,    86,    87,    -1,    16,    90,    91,    -1,    -1,    21,
      -1,    -1,    -1,    -1,    -1,    -1,    28,    -1,    -1,    -1,
      -1,    33,    -1,    35,    36,    37,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   118,   119,    47,    -1,    49,    -1,    -1,
      -1,    -1,    -1,    55,    56,    -1,    -1,    -1,    -1,    61,
      -1,    -1,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,     1,    -1,     3,    -1,     5,    -1,    -1,     8,
      82,    -1,    11,    12,    86,    87,    -1,    16,    -1,    -1,
      19,    -1,    21,    -1,    -1,    -1,    -1,    -1,    -1,    28,
      29,    30,    31,    32,    33,    34,    -1,    -1,    -1,    -1,
      -1,    40,    41,    42,    43,    44,    -1,    -1,    47,    48,
      -1,    -1,    51,    52,    53,    54,    55,    -1,    -1,    -1,
      59,    -1,    61,    62,    -1,    -1,    -1,    66,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    75,    76,    -1,    -1,
      -1,    -1,    81,    82,     1,    -1,     3,    -1,     5,    -1,
      -1,     8,    -1,    -1,    11,    12,    -1,    -1,    -1,    16,
      -1,    -1,    19,    -1,    21,    -1,    -1,    -1,    -1,    -1,
      -1,    28,    29,    30,    31,    32,    33,    34,    -1,    -1,
      -1,    -1,    -1,    40,    41,    42,    43,    44,    -1,    -1,
      47,    48,    -1,    -1,    51,    52,    53,    54,    55,    -1,
      -1,    -1,    59,    -1,    61,    62,    -1,    -1,    -1,    66,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    75,    76,
      -1,    -1,    -1,    -1,    81,    82,     1,    -1,     3,    -1,
       5,    -1,    -1,     8,    -1,    -1,    11,    12,    -1,    -1,
      -1,    16,    27,    -1,    19,    -1,    21,    -1,    -1,    -1,
      -1,    -1,    -1,    28,    29,    30,    31,    32,    33,    34,
      -1,    -1,    -1,    -1,    -1,    40,    41,    42,    43,    44,
      -1,    -1,    47,    48,    -1,    -1,    51,    52,    53,    54,
      55,    -1,    -1,    27,    59,    -1,    61,    62,    -1,    -1,
      -1,    66,    -1,    -1,    79,    -1,    -1,    -1,    -1,    84,
      85,    76,    -1,    88,    89,    -1,    81,    82,    27,    94,
      95,    96,    -1,    98,    99,   100,   101,   102,   103,   104,
     105,   106,    66,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    27,    79,    -1,    -1,    -1,    -1,
      84,    85,    -1,    -1,    88,    -1,    -1,    66,    -1,    -1,
      94,    95,    96,    -1,    98,    99,   100,   101,   102,   103,
     104,   105,   106,    -1,    -1,    84,    85,    27,    -1,    88,
      -1,    -1,    -1,    -1,    -1,    94,    95,    96,    -1,    98,
      99,   100,   101,   102,   103,   104,   105,   106,    81,    -1,
      27,    84,    85,    -1,    -1,    88,    -1,    -1,    -1,    -1,
      -1,    94,    95,    96,    -1,    98,    99,   100,   101,   102,
     103,   104,   105,   106,    74,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    27,    84,    85,    -1,    -1,    88,    -1,
      -1,    -1,    -1,    -1,    94,    95,    96,    74,    98,    99,
     100,   101,   102,   103,   104,   105,   106,    84,    85,    -1,
      -1,    88,    -1,    -1,    -1,    -1,    27,    94,    95,    96,
      -1,    98,    99,   100,   101,   102,   103,   104,   105,   106,
      74,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    27,
      84,    85,    -1,    -1,    88,    -1,    -1,    -1,    -1,    -1,
      94,    95,    96,    -1,    98,    99,   100,   101,   102,   103,
     104,   105,   106,    74,    -1,    27,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    84,    85,    -1,    -1,    88,    -1,    -1,
      -1,    -1,    -1,    94,    95,    96,    74,    98,    99,   100,
     101,   102,   103,   104,   105,   106,    84,    85,    27,    -1,
      88,    -1,    -1,    -1,    -1,    -1,    94,    95,    96,    -1,
      98,    99,   100,   101,   102,   103,   104,   105,   106,    81,
      -1,    -1,    84,    85,    -1,    -1,    88,    27,    -1,    -1,
      -1,    -1,    94,    95,    96,    -1,    98,    99,   100,   101,
     102,   103,   104,   105,   106,    74,    -1,    -1,    -1,    -1,
      27,    -1,    -1,    -1,    -1,    84,    85,    -1,    -1,    88,
      -1,    -1,    -1,    -1,    -1,    94,    95,    96,    -1,    98,
      99,   100,   101,   102,   103,   104,   105,   106,    27,    79,
      -1,    -1,    -1,    -1,    84,    85,    -1,    -1,    88,    -1,
      -1,    -1,    -1,    -1,    94,    95,    96,    -1,    98,    99,
     100,   101,   102,   103,   104,   105,   106,    84,    85,    27,
      -1,    88,    89,    -1,    -1,    -1,    -1,    94,    95,    96,
      -1,    98,    99,   100,   101,   102,   103,   104,   105,   106,
      -1,    -1,    81,    27,    -1,    84,    85,    -1,    -1,    88,
      -1,    -1,    -1,    -1,    -1,    94,    95,    96,    -1,    98,
      99,   100,   101,   102,   103,   104,   105,   106,    27,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    84,    85,    -1,    -1,
      88,    89,    -1,    -1,    -1,    -1,    94,    95,    96,    -1,
      98,    99,   100,   101,   102,   103,   104,   105,   106,    -1,
      84,    85,    27,    -1,    88,    89,    -1,    -1,    -1,    -1,
      94,    95,    96,    -1,    98,    99,   100,   101,   102,   103,
     104,   105,   106,    -1,    -1,    84,    85,    27,    -1,    88,
      89,    -1,    -1,    -1,    -1,    94,    95,    96,    -1,    98,
      99,   100,   101,   102,   103,   104,   105,   106,    -1,    74,
      27,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    84,
      85,    -1,    -1,    88,    -1,    -1,    -1,    -1,    -1,    94,
      95,    96,    -1,    98,    99,   100,   101,   102,   103,   104,
     105,   106,    -1,    -1,    84,    85,    -1,    -1,    88,    -1,
      -1,    -1,    -1,    -1,    94,    95,    96,    -1,    98,    99,
     100,   101,   102,   103,   104,   105,   106,    84,    85,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    94,    95,    96,
      -1,    -1,    -1,   100,   101,   102,   103,   104,   105,   106,
       5,    -1,     5,     8,    -1,     8,    11,    12,    11,    -1,
      -1,    16,    -1,    16,    -1,    -1,    21,    -1,    21,    -1,
      -1,    -1,    -1,    28,    29,    28,    -1,    -1,    33,    -1,
      33,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    47,     3,    47,    -1,    -1,    -1,    -1,    -1,
      55,    -1,    55,    -1,    -1,    -1,    61,    -1,    -1,    19,
      -1,    66,    -1,    66,    -1,    -1,    -1,    -1,    -1,    -1,
      30,    31,    32,    -1,    34,    -1,    -1,    82,    -1,    82,
      40,    41,    42,    43,    44,    -1,    -1,    -1,    48,    -1,
      -1,    51,    52,    53,    54,    -1,    77,    -1,    -1,    59,
      -1,    -1,    62,    84,    85,    86,    87,    -1,    -1,    90,
      91,    92,    93,    94,    95,    96,    97,    -1,    -1,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned short int yystos[] =
{
       0,    39,   122,   137,   138,     1,    66,   250,   251,   252,
     253,     0,   139,    81,    80,    81,    12,    37,    66,   254,
       1,     3,    19,    26,    30,    42,    52,    55,    81,    82,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   186,   194,   195,    77,    84,    85,    86,    87,    90,
      91,    92,    93,    94,    95,    96,    97,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,    75,   153,     1,   250,   140,
     140,    12,    29,   150,    85,    78,     1,     3,    19,    30,
      31,    32,    34,    40,    41,    42,    43,    44,    48,    51,
      52,    53,    54,    59,    62,    75,    81,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   167,   180,   184,
     185,   210,    81,    80,    81,    83,   251,   251,     5,     8,
      11,    12,    16,    21,    28,    33,    47,   124,   126,   130,
     131,   136,   194,   196,   197,   198,   248,   249,   250,    83,
      76,    81,   153,   210,     1,     4,     6,     7,    13,    15,
      19,    22,    23,    24,    35,    36,    45,    46,    49,    50,
      51,    53,    56,    57,    60,    61,    63,    64,    65,    67,
      68,    69,    70,    71,    72,    73,    77,    81,    86,    87,
      90,    91,   118,   119,   123,   124,   130,   131,   136,   148,
     198,   210,   211,   212,   213,   214,   215,   216,   217,   218,
     219,   220,   221,   222,   227,   232,   236,   238,   239,   243,
     245,   246,   247,   248,   249,   251,   252,   254,   255,   257,
     258,   259,   260,   263,   264,   270,   272,   273,   274,   275,
     277,   278,   279,   280,   282,   283,   284,   285,   295,   297,
      76,     1,   156,    12,    29,    61,   124,   169,   251,    48,
     163,    92,   250,    18,   151,    18,   187,   188,   251,   251,
     250,   134,   134,   250,    84,    79,    80,   134,    85,   134,
      76,    81,   210,   136,   198,   248,   260,   264,   273,   274,
     275,   278,   279,   291,   292,   294,   295,   237,   251,   291,
     237,     1,   216,   124,    73,    73,    73,    73,   265,    75,
     251,   290,   291,    80,    73,    73,    57,   291,   210,    80,
      73,   124,   248,   291,   262,   286,   287,   288,   289,   291,
     275,   275,   275,   275,   275,   275,   164,   165,   251,    12,
      80,    80,    12,    80,    76,   213,    81,    10,    20,   240,
     241,   242,    80,    77,    73,    80,    89,    35,    49,    56,
      35,   168,   251,   254,    80,    73,   265,   118,   119,    83,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   251,   251,   164,   168,   251,   168,    73,    81,    81,
     124,    25,   152,   124,   178,    75,   189,    32,    43,    44,
      53,    77,   132,   133,   135,   132,    85,   142,   196,   132,
     125,   126,   194,   199,   200,   201,   206,   246,   248,   250,
     271,   272,   275,   276,   277,   281,   282,   292,   293,   294,
     132,    27,    81,    84,    85,    88,    89,    94,    95,    96,
      98,    99,   100,   101,   102,   103,   104,   105,   106,    90,
      91,    92,    93,    97,    81,    66,   298,    81,    63,   164,
      19,   215,   221,   228,   230,   231,   198,   234,   250,   251,
     291,   291,   127,   128,   129,   131,   136,   198,   250,   244,
     291,    75,    81,   168,   291,   291,   291,    81,    12,   291,
      74,    74,    74,    78,    78,    79,    78,    79,    79,    89,
      79,    81,    77,   268,   269,    35,    49,    56,    12,   251,
      49,    56,    73,   210,   242,   240,   256,   262,   291,   261,
     262,   168,   216,   168,   261,   127,   291,   291,   291,   291,
     291,   291,   291,   291,   291,   291,   291,   291,   151,   187,
      81,    73,    73,    19,   172,   173,   174,   175,   178,   153,
      79,     1,    81,   157,   161,   190,   191,   192,   193,    78,
     256,   291,   132,   200,   250,    79,    84,    85,   124,   291,
     291,   291,   291,   291,   291,   291,   291,   291,   291,   291,
     291,   291,   291,   291,   291,   292,   292,   292,   292,   292,
     275,    73,    81,    79,   290,    81,   251,    74,    66,   235,
      74,    74,    73,    77,   133,   266,   267,   285,   134,    76,
     120,   244,    74,    74,    81,    74,   275,   280,   286,   291,
     288,   291,   262,   291,   165,    78,    83,    77,   151,    80,
     174,    78,    74,    74,    73,   152,   189,   172,   172,    74,
      79,   124,   178,   124,   169,    76,   190,   134,    78,    66,
      85,   201,   206,   194,   199,   202,   203,   206,   250,    89,
      81,   291,   231,    81,   235,    31,   233,   291,   216,   261,
     291,    75,   207,   266,   134,   216,    76,    75,   223,   216,
     216,    89,    89,    89,    32,   166,   207,   291,    78,   152,
     168,    74,   261,   153,    74,    74,    58,   176,   177,   173,
     251,   164,   168,   168,   134,    78,   202,   250,    79,   105,
      85,   291,    74,   229,   231,   291,   216,    17,    74,    76,
     207,   208,   209,   291,   244,     9,    14,   224,   225,   226,
     291,   291,   291,    32,   153,   210,    74,   268,   268,   178,
      75,   268,    81,    73,    73,   134,    85,   203,   206,   199,
     204,   205,   206,    81,    74,   216,   153,    76,    79,   291,
     296,    89,    76,   212,   225,   153,   176,   176,    49,    56,
     181,   182,   183,   211,   257,   258,   172,   172,   204,    79,
     106,   216,    76,   209,    89,   224,    38,   170,   171,   170,
      73,    73,   211,    76,    49,    73,    74,    74,   205,   206,
      73,    81,   179,   210,   170,   179,   261,   261,    76,    73,
     261,   268,   268,   251,    74,    74,   261,    74,   176,   176,
      79,    81,    81,    74,    81,    81,    81,   251,    81,    74
};


/* Prevent warning if -Wmissing-prototypes.  */
int yyparse (void);

/* Error token number */
#define YYTERROR 1

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */


#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N) ((void) 0)
#endif


#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */
#define YYLEX yylex ()

YYSTYPE yylval;

YYLTYPE yylloc;

int yynerrs;
int yychar;

static const int YYEOF = 0;
static const int YYEMPTY = -2;

typedef enum { yyok, yyaccept, yyabort, yyerr } YYRESULTTAG;

#define YYCHK(YYE)							     \
   do { YYRESULTTAG yyflag = YYE; if (yyflag != yyok) return yyflag; }	     \
   while (0)

#if YYDEBUG

#if ! defined (YYFPRINTF)
#  define YYFPRINTF fprintf
#endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep)
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);


# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr,					\
                  Type, Value);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;

#else /* !YYDEBUG */

# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)

#endif /* !YYDEBUG */

/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYMAXDEPTH * sizeof (GLRStackItem)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif

/* Minimum number of free items on the stack allowed after an
   allocation.  This is to allow allocation and initialization
   to be completed by functions that call yyexpandGLRStack before the
   stack is expanded, thus insuring that all necessary pointers get
   properly redirected to new data. */
#define YYHEADROOM 2

#ifndef YYSTACKEXPANDABLE
# if (! defined (__cplusplus) \
      || (defined (YYSTYPE_IS_TRIVIAL) && YYSTYPE_IS_TRIVIAL))
#  define YYSTACKEXPANDABLE 1
# else
#  define YYSTACKEXPANDABLE 0
# endif
#endif

#if YYERROR_VERBOSE

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static size_t
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      size_t yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return strlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

#endif /* !YYERROR_VERBOSE */

/** State numbers, as in LALR(1) machine */
typedef int yyStateNum;

/** Rule numbers, as in LALR(1) machine */
typedef int yyRuleNum;

/** Grammar symbol */
typedef short int yySymbol;

/** Item references, as in LALR(1) machine */
typedef short int yyItemNum;

typedef struct yyGLRState yyGLRState;
typedef struct yySemanticOption yySemanticOption;
typedef union yyGLRStackItem yyGLRStackItem;
typedef struct yyGLRStack yyGLRStack;
typedef struct yyGLRStateSet yyGLRStateSet;

struct yyGLRState {
  /** Type tag: always true. */
  yybool yyisState;
  /** Type tag for yysemantics. If true, yysval applies, otherwise
   *  yyfirstVal applies. */
  yybool yyresolved;
  /** Number of corresponding LALR(1) machine state. */
  yyStateNum yylrState;
  /** Preceding state in this stack */
  yyGLRState* yypred;
  /** Source position of the first token produced by my symbol */
  size_t yyposn;
  union {
    /** First in a chain of alternative reductions producing the
     *  non-terminal corresponding to this state, threaded through
     *  yynext. */
    yySemanticOption* yyfirstVal;
    /** Semantic value for this state. */
    YYSTYPE yysval;
  } yysemantics;
  /** Source location for this state. */
  YYLTYPE yyloc;
};

struct yyGLRStateSet {
  yyGLRState** yystates;
  size_t yysize, yycapacity;
};

struct yySemanticOption {
  /** Type tag: always false. */
  yybool yyisState;
  /** Rule number for this reduction */
  yyRuleNum yyrule;
  /** The last RHS state in the list of states to be reduced. */
  yyGLRState* yystate;
  /** Next sibling in chain of options. To facilitate merging,
   *  options are chained in decreasing order by address. */
  yySemanticOption* yynext;
};

/** Type of the items in the GLR stack. The yyisState field
 *  indicates which item of the union is valid. */
union yyGLRStackItem {
  yyGLRState yystate;
  yySemanticOption yyoption;
};

struct yyGLRStack {
  int yyerrState;


  yySymbol* yytokenp;
  YYJMP_BUF yyexception_buffer;
  yyGLRStackItem* yyitems;
  yyGLRStackItem* yynextFree;
  size_t yyspaceLeft;
  yyGLRState* yysplitPoint;
  yyGLRState* yylastDeleted;
  yyGLRStateSet yytops;
};

static void yyexpandGLRStack (yyGLRStack* yystack);

static void yyFail (yyGLRStack* yystack, const char* yymsg)
  __attribute__ ((__noreturn__));
static void
yyFail (yyGLRStack* yystack, const char* yymsg)
{
  if (yymsg != NULL)
    yyerror (yymsg);
  YYLONGJMP (yystack->yyexception_buffer, 1);
}

static void yyMemoryExhausted (yyGLRStack* yystack)
  __attribute__ ((__noreturn__));
static void
yyMemoryExhausted (yyGLRStack* yystack)
{
  YYLONGJMP (yystack->yyexception_buffer, 2);
}

#if YYDEBUG || YYERROR_VERBOSE
/** A printable representation of TOKEN.  */
static inline const char*
yytokenName (yySymbol yytoken)
{
  if (yytoken == YYEMPTY)
    return "";

  return yytname[yytoken];
}
#endif

/** Fill in YYVSP[YYLOW1 .. YYLOW0-1] from the chain of states starting
 *  at YYVSP[YYLOW0].yystate.yypred.  Leaves YYVSP[YYLOW1].yystate.yypred
 *  containing the pointer to the next state in the chain. Assumes
 *  YYLOW1 < YYLOW0.  */
static void yyfillin (yyGLRStackItem *, int, int) __attribute__ ((__unused__));
static void
yyfillin (yyGLRStackItem *yyvsp, int yylow0, int yylow1)
{
  yyGLRState* s;
  int i;
  s = yyvsp[yylow0].yystate.yypred;
  for (i = yylow0-1; i >= yylow1; i -= 1)
    {
      YYASSERT (s->yyresolved);
      yyvsp[i].yystate.yyresolved = yytrue;
      yyvsp[i].yystate.yysemantics.yysval = s->yysemantics.yysval;
      yyvsp[i].yystate.yyloc = s->yyloc;
      s = yyvsp[i].yystate.yypred = s->yypred;
    }
}

/* Do nothing if YYNORMAL or if *YYLOW <= YYLOW1.  Otherwise, fill in
   YYVSP[YYLOW1 .. *YYLOW-1] as in yyfillin and set *YYLOW = YYLOW1.
   For convenience, always return YYLOW1.  */
static inline int yyfill (yyGLRStackItem *, int *, int, yybool)
     __attribute__ ((__unused__));
static inline int
yyfill (yyGLRStackItem *yyvsp, int *yylow, int yylow1, yybool yynormal)
{
  if (!yynormal && yylow1 < *yylow)
    {
      yyfillin (yyvsp, *yylow, yylow1);
      *yylow = yylow1;
    }
  return yylow1;
}

/** Perform user action for rule number YYN, with RHS length YYRHSLEN,
 *  and top stack item YYVSP.  YYLVALP points to place to put semantic
 *  value ($$), and yylocp points to place for location information
 *  (@$). Returns yyok for normal return, yyaccept for YYACCEPT,
 *  yyerr for YYERROR, yyabort for YYABORT. */
static YYRESULTTAG
yyuserAction (yyRuleNum yyn, int yyrhslen, yyGLRStackItem* yyvsp,
	      YYSTYPE* yyvalp,
	      YYLTYPE* YYOPTIONAL_LOC (yylocp),
	      yyGLRStack* yystack
              )
{
  yybool yynormal __attribute__ ((__unused__)) =
    (yystack->yysplitPoint == NULL);
  int yylow;

# undef yyerrok
# define yyerrok (yystack->yyerrState = 0)
# undef YYACCEPT
# define YYACCEPT return yyaccept
# undef YYABORT
# define YYABORT return yyabort
# undef YYERROR
# define YYERROR return yyerrok, yyerr
# undef YYRECOVERING
# define YYRECOVERING (yystack->yyerrState != 0)
# undef yyclearin
# define yyclearin (yychar = *(yystack->yytokenp) = YYEMPTY)
# undef YYFILL
# define YYFILL(N) yyfill (yyvsp, &yylow, N, yynormal)
# undef YYBACKUP
# define YYBACKUP(Token, Value)						     \
  return yyerror (YY_("syntax error: cannot back up")),     \
	 yyerrok, yyerr

  yylow = 1;
  if (yyrhslen == 0)
    *yyvalp = yyval_default;
  else
    *yyvalp = yyvsp[YYFILL (1-yyrhslen)].yystate.yysemantics.yysval;
  YYLLOC_DEFAULT (*yylocp, yyvsp - yyrhslen, yyrhslen);

  switch (yyn)
    {
        case 2:
#line 325 "parser.yy"
    { compileAST ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.CompileUnit)); }
    break;

  case 3:
#line 332 "parser.yy"
    { ((*yyvalp).Tree) = new PrimitiveLitNode (intLiteral(*(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).val), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).posn); }
    break;

  case 4:
#line 334 "parser.yy"
    { ((*yyvalp).Tree) = new PrimitiveLitNode (longLiteral(*(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).val), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).posn); }
    break;

  case 5:
#line 336 "parser.yy"
    { ((*yyvalp).Tree) = new PrimitiveLitNode (floatLiteral(*(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).val), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).posn); }
    break;

  case 6:
#line 338 "parser.yy"
    { ((*yyvalp).Tree) = new PrimitiveLitNode (doubleLiteral(*(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).val), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).posn); }
    break;

  case 7:
#line 340 "parser.yy"
    { ((*yyvalp).Tree) = new PrimitiveLitNode (Literal(true), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 8:
#line 342 "parser.yy"
    { ((*yyvalp).Tree) = new PrimitiveLitNode (Literal(false), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 9:
#line 344 "parser.yy"
    { ((*yyvalp).Tree) = new PrimitiveLitNode (Literal((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.CharTerminal).val), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.CharTerminal).posn); }
    break;

  case 10:
#line 346 "parser.yy"
    { ((*yyvalp).Tree) = new StringLitNode (*(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.StrTerminal).val, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.StrTerminal).posn); }
    break;

  case 11:
#line 353 "parser.yy"
    { ((*yyvalp).Type) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)->asType(); }
    break;

  case 14:
#line 362 "parser.yy"
    { ((*yyvalp).Type) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Type); (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Type)->modifiers((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers)); }
    break;

  case 15:
#line 364 "parser.yy"
    { (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type)->modifiers((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Modifiers));
		  ((*yyvalp).Type) = arraySpecifiers2Type((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type)); }
    break;

  case 16:
#line 366 "parser.yy"
    { ((*yyvalp).Type) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Type); (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Type)->modifiers((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers)); }
    break;

  case 17:
#line 368 "parser.yy"
    { (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type)->modifiers((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Modifiers));
		  ((*yyvalp).Type) = arraySpecifiers2Type((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type)); }
    break;

  case 18:
#line 373 "parser.yy"
    { ((*yyvalp).Type) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Type); ((*yyvalp).Type)->modifiers((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers)); }
    break;

  case 22:
#line 384 "parser.yy"
    { ((*yyvalp).Type) = new TypeNameNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 23:
#line 386 "parser.yy"
    { ((*yyvalp).Type) = new TypeNameNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 24:
#line 391 "parser.yy"
    { ((*yyvalp).Type) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Type); }
    break;

  case 25:
#line 396 "parser.yy"
    { ((*yyvalp).Tree) = buildTemplateQualifiedName((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 26:
#line 400 "parser.yy"
    { ((*yyvalp).TreeList) = cons((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 27:
#line 401 "parser.yy"
    { ((*yyvalp).TreeList) = cons((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 28:
#line 405 "parser.yy"
    { ((*yyvalp).Tree) = new EmptyArrayNode((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 29:
#line 407 "parser.yy"
    { ((*yyvalp).Tree) = new ExpressionArrayNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers)); }
    break;

  case 30:
#line 409 "parser.yy"
    { 
		  if (*(((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Terminal).val != "d")
		    {
		      Error((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Terminal).posn) << "Unexpected identifier '" << *(((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Terminal).val << "'" << endl;
		      ((*yyvalp).Tree) = new ExpressionArrayNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers));
		    }
		  else
		    ((*yyvalp).Tree) = new TitaniumArrayNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers));
		}
    break;

  case 31:
#line 421 "parser.yy"
    { ((*yyvalp).Modifiers) = (Common::Modifiers)0; }
    break;

  case 32:
#line 423 "parser.yy"
    {
		  ((*yyvalp).Modifiers) = (Common::Modifiers) ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Modifiers) | (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers));
		  if (((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Modifiers) & (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers)) != 0)
		      Error (lexerPosition()) << "repeated modifier" << endl;
	        }
    break;

  case 33:
#line 431 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Single; }
    break;

  case 34:
#line 432 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Local; }
    break;

  case 35:
#line 433 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::NonsharedQ; }
    break;

  case 36:
#line 434 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::PolysharedQ; }
    break;

  case 37:
#line 439 "parser.yy"
    { ((*yyvalp).Type) = new BoolTypeNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 38:
#line 441 "parser.yy"
    { ((*yyvalp).Type) = new CharTypeNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 39:
#line 443 "parser.yy"
    { ((*yyvalp).Type) = new ByteTypeNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 40:
#line 445 "parser.yy"
    { ((*yyvalp).Type) = new ShortTypeNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 41:
#line 447 "parser.yy"
    { ((*yyvalp).Type) = new IntTypeNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 42:
#line 449 "parser.yy"
    { ((*yyvalp).Type) = new FloatTypeNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 43:
#line 451 "parser.yy"
    { ((*yyvalp).Type) = new LongTypeNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 44:
#line 453 "parser.yy"
    { ((*yyvalp).Type) = new DoubleTypeNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 45:
#line 462 "parser.yy"
    { 
		   llist<TreeNode*> *types = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList);
	           SourcePosn pos = lexerPosition();
		   pos.posn = 0;
		   ((*yyvalp).CompileUnit) = new CompileUnitNode (NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), 
					     new TreeListNode (types), NULL, pos); 
		}
    break;

  case 46:
#line 473 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree); }
    break;

  case 47:
#line 475 "parser.yy"
    { ((*yyvalp).Tree) = TreeNode::omitted; }
    break;

  case 48:
#line 480 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 49:
#line 482 "parser.yy"
    { ((*yyvalp).TreeList) = extend ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), cons((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree))); }
    break;

  case 50:
#line 488 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 51:
#line 490 "parser.yy"
    {
		  if ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree)->absent())
		    ((*yyvalp).TreeList) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList);
		  else
		    ((*yyvalp).TreeList) = cons (static_cast<TreeNode*>((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree)), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList));
		}
    break;

  case 52:
#line 497 "parser.yy"
    { ((*yyvalp).TreeList) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList); }
    break;

  case 53:
#line 502 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TypeDecl); }
    break;

  case 54:
#line 504 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 55:
#line 509 "parser.yy"
    { ((*yyvalp).TypeDecl) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TypeDecl); }
    break;

  case 56:
#line 511 "parser.yy"
    { ((*yyvalp).TypeDecl) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TypeDecl); }
    break;

  case 60:
#line 525 "parser.yy"
    { ((*yyvalp).Tree) = new ImportNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 61:
#line 534 "parser.yy"
    { ((*yyvalp).Tree) = new ImportOnDemandNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 62:
#line 539 "parser.yy"
    { Error (lexerPosition()) << "typedefs unimplemented, use macro instead" << endl;
		  ((*yyvalp).Tree) = new ImportNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 63:
#line 551 "parser.yy"
    { ((*yyvalp).TypeDecl) = new ClassDeclNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TypeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList), NULL, TreeNode::omitted, (NameNode *) (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), false, TreeNode::omitted, NULL, 0, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 65:
#line 560 "parser.yy"
    { ((*yyvalp).Modifiers) = (Common::Modifiers) 0; }
    break;

  case 67:
#line 566 "parser.yy"
    { ((*yyvalp).Modifiers) = (Common::Modifiers) ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Modifiers) | (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers)); 
		  if (((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Modifiers) & (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers)) != 0)
		      Error (lexerPosition()) << "repeated modifier" << endl;
	      }
    break;

  case 68:
#line 575 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Abstract; }
    break;

  case 69:
#line 577 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Final; }
    break;

  case 70:
#line 579 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Public; }
    break;

  case 71:
#line 581 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Immutable; }
    break;

  case 72:
#line 583 "parser.yy"
    {   Warning (lexerPosition(),"strictfp") << "strictfp modifier is currently ignored." << endl; 
                    ((*yyvalp).Modifiers) = TreeNode::Strictfp; }
    break;

  case 73:
#line 592 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Type); }
    break;

  case 74:
#line 594 "parser.yy"
    { ((*yyvalp).Tree) = TreeNode::omitted; }
    break;

  case 75:
#line 602 "parser.yy"
    { ((*yyvalp).TypeList) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TypeList); }
    break;

  case 76:
#line 604 "parser.yy"
    { ((*yyvalp).TypeList) = NULL; }
    break;

  case 77:
#line 613 "parser.yy"
    { ((*yyvalp).TreeList) = cons (TreeNode::omitted, cons (TreeNode::omitted, cons(TreeNode::omitted, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList)))); }
    break;

  case 79:
#line 619 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 81:
#line 625 "parser.yy"
    { ((*yyvalp).TreeList) = extend ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 83:
#line 634 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 84:
#line 636 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 85:
#line 638 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 86:
#line 640 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 87:
#line 642 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 88:
#line 644 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 89:
#line 652 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 90:
#line 654 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 91:
#line 659 "parser.yy"
    {   checkNestedTypeModifiers((((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.SimpTerminal).posn);
		    ((*yyvalp).Tree) = static_cast<TreeNode *>(new ClassDeclNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TypeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList), NULL, TreeNode::omitted, (NameNode *) (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), false, TreeNode::omitted, NULL, 0, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.SimpTerminal).posn)); }
    break;

  case 92:
#line 666 "parser.yy"
    {   checkNestedTypeModifiers((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.SimpTerminal).posn);
		    ((*yyvalp).Tree) = static_cast<TreeNode *>(new InterfaceDeclNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TypeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList), NULL, (NameNode *) (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), false, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.SimpTerminal).posn)); }
    break;

  case 93:
#line 675 "parser.yy"
    { checkFieldModifiers ((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type)->position());
	       ((*yyvalp).TreeList) = NULL;
	       foreach (decl, llist<DeclaratorTuple>, *(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.DeclaratorList)) {
		   ((*yyvalp).TreeList) = cons (static_cast<TreeNode *>(new FieldDeclNode(makeArrayType((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type), 
										      (*decl).dims),
									(*decl).name,
									(((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Modifiers),
									(*decl).initExpr)),
			      ((*yyvalp).TreeList));
	       }
	       free_all ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.DeclaratorList));
	   }
    break;

  case 95:
#line 699 "parser.yy"
    { ((*yyvalp).Modifiers) = (Common::Modifiers) 0; }
    break;

  case 97:
#line 705 "parser.yy"
    { ((*yyvalp).Modifiers) = (Common::Modifiers) ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Modifiers) | (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers));
		  if (((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Modifiers) & (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers)) != 0)
		      Error (lexerPosition()) << "repeated modifier" << endl;
	      }
    break;

  case 98:
#line 714 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Public; }
    break;

  case 99:
#line 716 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Protected; }
    break;

  case 100:
#line 718 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Private; }
    break;

  case 101:
#line 721 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Static; }
    break;

  case 102:
#line 723 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Final; }
    break;

  case 103:
#line 725 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Sglobal; }
    break;

  case 104:
#line 727 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Sglobal; }
    break;

  case 105:
#line 730 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Abstract; }
    break;

  case 106:
#line 732 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Native; }
    break;

  case 107:
#line 734 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Synchronized; }
    break;

  case 108:
#line 737 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Transient; }
    break;

  case 109:
#line 739 "parser.yy"
    {   Warning (lexerPosition(),"strictfp") << "strictfp modifier is currently ignored." << endl; 
 	          ((*yyvalp).Modifiers) = TreeNode::Strictfp; }
    break;

  case 110:
#line 742 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Volatile; }
    break;

  case 111:
#line 744 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Local; }
    break;

  case 112:
#line 746 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::NonsharedQ; }
    break;

  case 113:
#line 748 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::PolysharedQ; }
    break;

  case 114:
#line 750 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Inline; }
    break;

  case 115:
#line 753 "parser.yy"
    { ((*yyvalp).Modifiers) = TreeNode::Immutable; }
    break;

  case 117:
#line 763 "parser.yy"
    { ((*yyvalp).DeclaratorList) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.DeclaratorList); (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.DeclaratorList)->tail() = (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.DeclaratorList); }
    break;

  case 118:
#line 768 "parser.yy"
    { ((*yyvalp).DeclaratorList) = cons(DeclaratorTuple((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Int), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), TreeNode::omitted)); }
    break;

  case 119:
#line 770 "parser.yy"
    { ((*yyvalp).DeclaratorList) = cons(DeclaratorTuple((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Int), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree))); }
    break;

  case 122:
#line 786 "parser.yy"
    { checkMethodModifiers((((yyGLRStackItem const *)yyvsp)[YYFILL (-9)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.Tree)->position());
	    TreeNode *body = addSynchronized((((yyGLRStackItem const *)yyvsp)[YYFILL (-9)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree));
	    ((*yyvalp).Tree) = new MethodDeclNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-9)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.TreeList), makeArrayType((((yyGLRStackItem const *)yyvsp)[YYFILL (-8)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Int)), makeArrayType((((yyGLRStackItem const *)yyvsp)[YYFILL (-8)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Int)), (((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TypeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), body); 
	    }
    break;

  case 123:
#line 792 "parser.yy"
    { checkMethodModifiers((((yyGLRStackItem const *)yyvsp)[YYFILL (-9)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.Tree)->position());
	    TreeNode *body = addSynchronized((((yyGLRStackItem const *)yyvsp)[YYFILL (-9)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree));
	    ((*yyvalp).Tree) = new MethodDeclNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-9)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.TreeList), makeArrayType((((yyGLRStackItem const *)yyvsp)[YYFILL (-8)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Int)), makeArrayType((((yyGLRStackItem const *)yyvsp)[YYFILL (-8)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Int)), (((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TypeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), body); 
	    if ((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Int)) Error((((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.Tree)->position()) << "cannot return array of void" << endl;
	    }
    break;

  case 126:
#line 806 "parser.yy"
    { ((*yyvalp).Type) = new VoidTypeNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 127:
#line 810 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 128:
#line 811 "parser.yy"
    { ((*yyvalp).TreeList) = cons((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 129:
#line 816 "parser.yy"
    { ((*yyvalp).Tree) = new OverlapNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 131:
#line 827 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 132:
#line 832 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 133:
#line 834 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 134:
#line 839 "parser.yy"
    { ((*yyvalp).Tree) = new ParameterNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Bool), makeArrayType ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Int)), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 135:
#line 843 "parser.yy"
    { ((*yyvalp).Bool) = true; }
    break;

  case 136:
#line 844 "parser.yy"
    { ((*yyvalp).Bool) = false; }
    break;

  case 138:
#line 853 "parser.yy"
    { ((*yyvalp).TypeList) = NULL; }
    break;

  case 139:
#line 858 "parser.yy"
    { ((*yyvalp).TypeList) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TypeList); }
    break;

  case 140:
#line 863 "parser.yy"
    { ((*yyvalp).TypeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Type)); }
    break;

  case 141:
#line 865 "parser.yy"
    { ((*yyvalp).TypeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TypeList)); }
    break;

  case 143:
#line 874 "parser.yy"
    { ((*yyvalp).Tree) = TreeNode::omitted; }
    break;

  case 144:
#line 883 "parser.yy"
    { checkConstructorModifiers ((((yyGLRStackItem const *)yyvsp)[YYFILL (-9)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-8)].yystate.yysemantics.yysval.Tree)->position());
	      ((*yyvalp).Tree) = new ConstructorDeclNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-9)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (-8)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.TypeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), 
					    new BlockNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn),
					    TreeNode::omitted);
	    }
    break;

  case 145:
#line 890 "parser.yy"
    { checkConstructorModifiers ((((yyGLRStackItem const *)yyvsp)[YYFILL (-8)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.Tree)->position ());
	      ((*yyvalp).Tree) = new ConstructorDeclNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-8)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.TypeList),
					    new SuperConstructorCallNode (Common::CompilerGenerated, TreeNode::omitted, NULL, NULL,
									  (((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.Tree)->position()),
					    new BlockNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn),
					    TreeNode::omitted);
	  }
    break;

  case 148:
#line 910 "parser.yy"
    { ((*yyvalp).Tree) = new ThisConstructorCallNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList), NULL, false, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 149:
#line 915 "parser.yy"
    { ((*yyvalp).Tree) = new SuperConstructorCallNode (Common::None, TreeNode::omitted, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 150:
#line 917 "parser.yy"
    { ((*yyvalp).Tree) = new SuperConstructorCallNode (Common::None, new ObjectNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Tree)->position()), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 151:
#line 919 "parser.yy"
    { ((*yyvalp).Tree) = new SuperConstructorCallNode (Common::None, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 152:
#line 927 "parser.yy"
    { ((*yyvalp).Tree) = new StaticInitNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 153:
#line 932 "parser.yy"
    { ((*yyvalp).Tree) = new InstanceInitNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 154:
#line 941 "parser.yy"
    { checkInterfaceModifiers((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.SimpTerminal).posn);
	        ((*yyvalp).TypeDecl) = new InterfaceDeclNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TypeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList), NULL, (NameNode *) (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), false); }
    break;

  case 156:
#line 951 "parser.yy"
    { ((*yyvalp).TypeList) = NULL; }
    break;

  case 157:
#line 956 "parser.yy"
    { ((*yyvalp).TypeList) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TypeList); }
    break;

  case 158:
#line 963 "parser.yy"
    { ((*yyvalp).TreeList) = cons(TreeNode::omitted, cons(TreeNode::omitted, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList))); }
    break;

  case 159:
#line 968 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 160:
#line 970 "parser.yy"
    { ((*yyvalp).TreeList) = extend ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 162:
#line 976 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 163:
#line 978 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 164:
#line 980 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 165:
#line 985 "parser.yy"
    { checkConstantFieldModifiers ((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type)->position());
	       ((*yyvalp).TreeList) = NULL;
	       foreach (decl, llist<DeclaratorTuple>, *(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.DeclaratorList)) {
		   ((*yyvalp).TreeList) = cons (static_cast<TreeNode*>(new FieldDeclNode (makeArrayType ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type), 
										       (*decl).dims),
									(*decl).name,
									(((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Modifiers),
									(*decl).initExpr)),
			      ((*yyvalp).TreeList));
	       }
	       free_all ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.DeclaratorList));
	   }
    break;

  case 166:
#line 1002 "parser.yy"
    { checkMethodSignatureModifiers ((((yyGLRStackItem const *)yyvsp)[YYFILL (-8)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.Type)->position());
	      ((*yyvalp).Tree) = new MethodSignatureNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-8)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.TreeList), 
					    makeArrayType ((((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Int)),
					    makeArrayType ((((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Int)),
					    (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TypeList)); }
    break;

  case 167:
#line 1009 "parser.yy"
    { checkMethodSignatureModifiers ((((yyGLRStackItem const *)yyvsp)[YYFILL (-8)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.Type)->position());
	      ((*yyvalp).Tree) = new MethodSignatureNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-8)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.TreeList), 
					    makeArrayType ((((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Int)),
					    makeArrayType ((((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Int)),
					    (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TypeList)); }
    break;

  case 170:
#line 1026 "parser.yy"
    { ((*yyvalp).Tree) = new TemplateDeclNode( (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TypeDecl), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList), 0 ); }
    break;

  case 171:
#line 1031 "parser.yy"
    { ((*yyvalp).TreeList) = cons( (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree) ); }
    break;

  case 172:
#line 1033 "parser.yy"
    { ((*yyvalp).TreeList) = cons( (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList) ); }
    break;

  case 173:
#line 1038 "parser.yy"
    { ((*yyvalp).Tree) = new TemplateTypeParamNode( (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree) ); }
    break;

  case 174:
#line 1040 "parser.yy"
    { ((*yyvalp).Tree) = new TemplateConstParamNode( (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree) ); }
    break;

  case 175:
#line 1045 "parser.yy"
    { ((*yyvalp).Type) = buildTemplateInstanceType((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 176:
#line 1048 "parser.yy"
    { ((*yyvalp).Type) = buildTemplateInstanceType((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 177:
#line 1053 "parser.yy"
    { ((*yyvalp).TreeList) = cons( (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree) ); }
    break;

  case 178:
#line 1055 "parser.yy"
    { ((*yyvalp).TreeList) = extend( (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList), cons((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)) ); }
    break;

  case 179:
#line 1060 "parser.yy"
    { ((*yyvalp).TreeList) = cons( (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree) ); }
    break;

  case 180:
#line 1062 "parser.yy"
    { ((*yyvalp).TreeList) = extend( (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList), cons((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)) ); }
    break;

  case 181:
#line 1067 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree); }
    break;

  case 182:
#line 1069 "parser.yy"
    { ((*yyvalp).Tree) = buildTemplateInstanceType((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 183:
#line 1072 "parser.yy"
    { ((*yyvalp).Tree) = buildTemplateInstanceType((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 184:
#line 1077 "parser.yy"
    { ((*yyvalp).TreeList) = cons( (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree) ); }
    break;

  case 185:
#line 1079 "parser.yy"
    { ((*yyvalp).TreeList) = extend( (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList), cons((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)) ); }
    break;

  case 186:
#line 1084 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree); }
    break;

  case 187:
#line 1086 "parser.yy"
    { ((*yyvalp).Tree) = buildTemplateInstanceType((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 188:
#line 1089 "parser.yy"
    { ((*yyvalp).Tree) = buildTemplateInstanceType((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 189:
#line 1094 "parser.yy"
    { ((*yyvalp).TreeList) = cons( (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree) ); }
    break;

  case 190:
#line 1096 "parser.yy"
    { ((*yyvalp).TreeList) = extend( (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList), cons((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)) ); }
    break;

  case 191:
#line 1101 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree); }
    break;

  case 192:
#line 1106 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Type); }
    break;

  case 193:
#line 1108 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 194:
#line 1110 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 195:
#line 1119 "parser.yy"
    { ((*yyvalp).Tree) = new ArrayInitNode (dreverse ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList)), TreeNode::omitted, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 196:
#line 1121 "parser.yy"
    { ((*yyvalp).Tree) = new ArrayInitNode (dreverse ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList)), TreeNode::omitted, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 197:
#line 1123 "parser.yy"
    { ((*yyvalp).Tree) = new ArrayInitNode (NULL, TreeNode::omitted, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 198:
#line 1130 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 199:
#line 1132 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 202:
#line 1147 "parser.yy"
    { ((*yyvalp).Tree) = new BlockNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 204:
#line 1153 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 205:
#line 1158 "parser.yy"
    { ((*yyvalp).TreeList) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList); }
    break;

  case 206:
#line 1160 "parser.yy"
    { ((*yyvalp).TreeList) = extend ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 207:
#line 1165 "parser.yy"
    { ((*yyvalp).TreeList) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList); }
    break;

  case 208:
#line 1167 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 209:
#line 1169 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 210:
#line 1177 "parser.yy"
    {   checkLocalTypeModifiers((((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.SimpTerminal).posn);
		    ((*yyvalp).Tree) = new ClassDeclNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TypeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList), NULL, TreeNode::omitted, (NameNode *) (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), false, TreeNode::omitted, NULL, 0, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 211:
#line 1187 "parser.yy"
    { ((*yyvalp).TreeList) = makeVarDeclNodes(true, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.DeclaratorList)); }
    break;

  case 212:
#line 1189 "parser.yy"
    { ((*yyvalp).TreeList) = makeVarDeclNodes(false, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.DeclaratorList)); }
    break;

  case 223:
#line 1211 "parser.yy"
    { ((*yyvalp).Tree) = new EmptyStmtNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 224:
#line 1217 "parser.yy"
    { /* Java spec says a missing Expr2 produces an empty message, 
                     but that sucks - so add a default message that's actually useful */
                  string msg = string(" at ") + (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.SimpTerminal).posn.asString() + ": " + pseudocode((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree));
                  ((*yyvalp).Tree) = new AssertNode ( (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), 
                       new StringLitNode (
                        string16( convertString((char*)msg.c_str(), msg.length(), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.SimpTerminal).posn) ), 
                        (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.SimpTerminal).posn) ); 
                }
    break;

  case 225:
#line 1226 "parser.yy"
    { ((*yyvalp).Tree) = new AssertNode ( (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree) ); }
    break;

  case 226:
#line 1233 "parser.yy"
    { ((*yyvalp).Tree) = new LabeledStmtNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 227:
#line 1241 "parser.yy"
    { ((*yyvalp).Tree) = new ExpressionStmtNode( (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree) ); }
    break;

  case 228:
#line 1246 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 229:
#line 1248 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 230:
#line 1250 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 231:
#line 1252 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 232:
#line 1254 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 233:
#line 1256 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 234:
#line 1258 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 235:
#line 1266 "parser.yy"
    { ((*yyvalp).Tree) = new IfStmtNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree), TreeNode::omitted); }
    break;

  case 236:
#line 1268 "parser.yy"
    { ((*yyvalp).Tree) = new IfStmtNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 237:
#line 1270 "parser.yy"
    { ((*yyvalp).Tree) = new SwitchNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 238:
#line 1275 "parser.yy"
    { ((*yyvalp).TreeList) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList); }
    break;

  case 239:
#line 1279 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 240:
#line 1281 "parser.yy"
    { ((*yyvalp).TreeList) = cons (static_cast<TreeNode*>(new SwitchBranchNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList))), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 241:
#line 1283 "parser.yy"
    { ((*yyvalp).TreeList) = cons (static_cast<TreeNode*>(new SwitchBranchNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList), cons (static_cast<TreeNode*>(new EmptyStmtNode ()))))); }
    break;

  case 242:
#line 1288 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 243:
#line 1290 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 244:
#line 1295 "parser.yy"
    { ((*yyvalp).Tree) = new CaseNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 245:
#line 1297 "parser.yy"
    { ((*yyvalp).Tree) = new CaseNode (TreeNode::omitted, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 246:
#line 1305 "parser.yy"
    { ((*yyvalp).Tree) = new WhileNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 247:
#line 1307 "parser.yy"
    { ((*yyvalp).Tree) = new DoNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 248:
#line 1309 "parser.yy"
    { ((*yyvalp).Tree) = new ForNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 250:
#line 1315 "parser.yy"
    { ((*yyvalp).TreeList) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList); }
    break;

  case 251:
#line 1317 "parser.yy"
    { ((*yyvalp).TreeList) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList); }
    break;

  case 253:
#line 1323 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 255:
#line 1329 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 256:
#line 1334 "parser.yy"
    { ((*yyvalp).TreeList) = cons (static_cast<TreeNode *>(new ExpressionStmtNode((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)))); }
    break;

  case 257:
#line 1336 "parser.yy"
    { ((*yyvalp).TreeList) = cons (static_cast<TreeNode *>(new ExpressionStmtNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree))), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 258:
#line 1343 "parser.yy"
    { ((*yyvalp).Tree) = new ForEachStmtNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Bool), (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 259:
#line 1347 "parser.yy"
    { ((*yyvalp).Bool) = true; }
    break;

  case 260:
#line 1348 "parser.yy"
    { ((*yyvalp).Bool) = false; }
    break;

  case 261:
#line 1353 "parser.yy"
    { ((*yyvalp).TreeList) = cons(static_cast<TreeNode *>(new ForEachPairNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree), TreeNode::omitted, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree)->position()))); }
    break;

  case 262:
#line 1355 "parser.yy"
    { TreeNode *pttype = (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Type);
                  if (!((TypeNode*)pttype)->isPointType()) {
		    Error(pttype->position()) << "optional point type declaration in a foreach must be Point<N>" << endl;
	            pttype = TreeNode::omitted;
                  }
                  ((*yyvalp).TreeList) = cons(static_cast<TreeNode *>(new ForEachPairNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree), pttype, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Type)->position()))); }
    break;

  case 263:
#line 1370 "parser.yy"
    { if (*(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).val == "within") 
		    Warning(lexerPosition(),"deprecated-foreach-within") << "deprecated use of 'within' in foreach - expected 'in'" << endl;
                  else if (*(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).val != "in")
	      	    Error(lexerPosition()) << "bad keyword in foreach - expected 'in'" << endl;
	  	}
    break;

  case 264:
#line 1381 "parser.yy"
    { ((*yyvalp).Tree) = new BreakNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), NULL, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 265:
#line 1383 "parser.yy"
    { ((*yyvalp).Tree) = new ContinueNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), NULL, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 266:
#line 1385 "parser.yy"
    { ((*yyvalp).Tree) = new ReturnNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 267:
#line 1387 "parser.yy"
    { ((*yyvalp).Tree) = new ThrowNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), false, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 268:
#line 1389 "parser.yy"
    { ((*yyvalp).Tree) = new ThrowNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), true, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 270:
#line 1396 "parser.yy"
    { ((*yyvalp).Tree) = TreeNode::omitted; }
    break;

  case 271:
#line 1404 "parser.yy"
    { ((*yyvalp).Tree) = new SynchronizedNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 272:
#line 1407 "parser.yy"
    { ((*yyvalp).Tree) = new TryStmtNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Try), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 273:
#line 1409 "parser.yy"
    { ((*yyvalp).Tree) = new TryStmtNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Try), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.CatchList), TreeNode::omitted); }
    break;

  case 274:
#line 1411 "parser.yy"
    { ((*yyvalp).Tree) = new TryStmtNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Try), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.CatchList), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 275:
#line 1416 "parser.yy"
    { ((*yyvalp).Try) = new TryNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 276:
#line 1421 "parser.yy"
    { ((*yyvalp).CatchList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Catch)); }
    break;

  case 277:
#line 1423 "parser.yy"
    { ((*yyvalp).CatchList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Catch), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.CatchList)); }
    break;

  case 278:
#line 1428 "parser.yy"
    { ((*yyvalp).Catch) = new CatchNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree), true, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 279:
#line 1433 "parser.yy"
    { ((*yyvalp).Tree) = new FinallyNode( (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree) ); }
    break;

  case 280:
#line 1440 "parser.yy"
    { ((*yyvalp).Tree) = new PartitionStmtNode(TreeNode::omitted, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 281:
#line 1442 "parser.yy"
    { ((*yyvalp).Tree) = new PartitionStmtNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 282:
#line 1446 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 283:
#line 1448 "parser.yy"
    { ((*yyvalp).TreeList) = cons(static_cast<TreeNode *>(new PartitionClauseNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree))),
					    (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 284:
#line 1460 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)->asExpr(); }
    break;

  case 289:
#line 1472 "parser.yy"
    { ((*yyvalp).Tree) = new NullPntrNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 290:
#line 1474 "parser.yy"
    { ((*yyvalp).Tree) = new ThisNode (TreeNode::omitted, NULL, Common::None, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 291:
#line 1476 "parser.yy"
    { ((*yyvalp).Tree) = new ThisNode (new TypeNameNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree)), NULL, Common::None, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 292:
#line 1478 "parser.yy"
    { ((*yyvalp).Tree) = new ThisNode (new TypeNameNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree)), NULL, Common::None, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 293:
#line 1480 "parser.yy"
    { ((*yyvalp).Tree) = new ThisNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type), NULL, Common::None, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 294:
#line 1482 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree); }
    break;

  case 295:
#line 1484 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree)->asExpr(); }
    break;

  case 297:
#line 1487 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 299:
#line 1490 "parser.yy"
    { string s = "java.lang.";
		  switch ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type)->signature()[0]) {
                    case 'Z': s += "Boolean"; break;
                    case 'C': s += "Character"; break;
                    case 'B': s += "Byte"; break;
                    case 'S': s += "Short"; break;
                    case 'I': s += "Integer"; break;
                    case 'J': s += "Long"; break;
                    case 'F': s += "Float"; break;
                    case 'D': s += "Double"; break;
		    default: abort();
		  }
		  ((*yyvalp).Tree) = new TypeFieldAccessNode ( 
                               new TypeNameNode(buildName(s.c_str(), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.SimpTerminal).posn)), 
                                                buildName("TYPE", (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.SimpTerminal).posn),
                                                (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.SimpTerminal).posn); 
                }
    break;

  case 300:
#line 1508 "parser.yy"
    { 
                  ((*yyvalp).Tree) = new TypeFieldAccessNode ( 
                               new TypeNameNode(buildName("java.lang.Void", (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.SimpTerminal).posn)), 
                                                buildName("TYPE", (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.SimpTerminal).posn),
                                                (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.SimpTerminal).posn); 
 		}
    break;

  case 301:
#line 1521 "parser.yy"
    { ((*yyvalp).Tree) = new ArrayNameNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers), NULL); }
    break;

  case 302:
#line 1522 "parser.yy"
    { ((*yyvalp).Tree) = new ArrayNameNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers), NULL); }
    break;

  case 304:
#line 1527 "parser.yy"
    { ((*yyvalp).Tree) = new ArrayNameNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 305:
#line 1528 "parser.yy"
    { ((*yyvalp).Tree) = new ArrayNameNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Modifiers), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 308:
#line 1538 "parser.yy"
    { ((*yyvalp).Tree) =
		    new NameNode (TreeNode::omitted, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).val, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).posn); }
    break;

  case 309:
#line 1545 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree); }
    break;

  case 310:
#line 1550 "parser.yy"
    { ((*yyvalp).Tree) = new NameNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).val, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).posn); }
    break;

  case 311:
#line 1553 "parser.yy"
    { ((*yyvalp).Tree) = new NameNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), intern("class"), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 312:
#line 1555 "parser.yy"
    { ((*yyvalp).Tree) = new NameNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)->ident(), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)->position()); }
    break;

  case 313:
#line 1559 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("!", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 314:
#line 1560 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("~", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 315:
#line 1561 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("<", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 316:
#line 1562 "parser.yy"
    { ((*yyvalp).Tree) = newOperator(">", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 317:
#line 1563 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("<=", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 318:
#line 1564 "parser.yy"
    { ((*yyvalp).Tree) = newOperator(">=", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 319:
#line 1565 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("==", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 320:
#line 1566 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("!=", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 321:
#line 1567 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("+", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 322:
#line 1568 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("-", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 323:
#line 1569 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("*", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 324:
#line 1570 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("/", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 325:
#line 1571 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("&", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 326:
#line 1572 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("|", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 327:
#line 1573 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("^", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 328:
#line 1574 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("%", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 329:
#line 1575 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("<<", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 330:
#line 1576 "parser.yy"
    { ((*yyvalp).Tree) = newOperator(">>", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 331:
#line 1577 "parser.yy"
    { ((*yyvalp).Tree) = newOperator(">>>", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 332:
#line 1578 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("+=", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 333:
#line 1579 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("-=", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 334:
#line 1580 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("*=", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 335:
#line 1581 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("/=", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 336:
#line 1582 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("%=", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 337:
#line 1583 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("<<=", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 338:
#line 1584 "parser.yy"
    { ((*yyvalp).Tree) = newOperator(">>=", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 339:
#line 1585 "parser.yy"
    { ((*yyvalp).Tree) = newOperator(">>>=", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 340:
#line 1586 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("&=", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 341:
#line 1587 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("^=", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 342:
#line 1588 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("|=", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 343:
#line 1589 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("[]", (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 344:
#line 1590 "parser.yy"
    { ((*yyvalp).Tree) = newOperator("[]=", (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 345:
#line 1597 "parser.yy"
    { ((*yyvalp).Tree) = new ArrayAccessNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 346:
#line 1601 "parser.yy"
    { ((*yyvalp).Tree) = new PointNode((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 347:
#line 1611 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree); }
    break;

  case 348:
#line 1616 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree); }
    break;

  case 349:
#line 1618 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree); }
    break;

  case 350:
#line 1624 "parser.yy"
    { ((*yyvalp).Tree) = new ObjectFieldAccessNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 351:
#line 1626 "parser.yy"
    { ((*yyvalp).Tree) = new SuperFieldAccessNode (TreeNode::omitted, NULL, Common::None, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 352:
#line 1628 "parser.yy"
    { ((*yyvalp).Tree) = new SuperFieldAccessNode (new TypeNameNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree)), NULL, Common::None, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 353:
#line 1630 "parser.yy"
    { ((*yyvalp).Tree) = new SuperFieldAccessNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Type), NULL, Common::None, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 354:
#line 1632 "parser.yy"
    { ((*yyvalp).Tree) = new ObjectFieldAccessNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree)->asExpr(), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 355:
#line 1635 "parser.yy"
    { ((*yyvalp).Tree) = new TypeFieldAccessNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Type), buildName("class", (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.SimpTerminal).posn)); }
    break;

  case 356:
#line 1637 "parser.yy"
    { ((*yyvalp).Tree) = new ThisFieldAccessNode (NULL, Common::None, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 357:
#line 1645 "parser.yy"
    { ((*yyvalp).Tree) = new MethodCallNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 358:
#line 1647 "parser.yy"
    { ((*yyvalp).Tree) = new MethodCallNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree)->asExpr(), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 360:
#line 1653 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 361:
#line 1658 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 362:
#line 1660 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 363:
#line 1668 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree); }
    break;

  case 364:
#line 1670 "parser.yy"
    { ((*yyvalp).Tree) = new ObjectNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree)->position()); }
    break;

  case 365:
#line 1672 "parser.yy"
    { ((*yyvalp).Tree) = new ObjectNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree)->position()); }
    break;

  case 366:
#line 1677 "parser.yy"
    { ((*yyvalp).Tree) = new AllocateArrayNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList), TreeNode::omitted); }
    break;

  case 367:
#line 1680 "parser.yy"
    { ((*yyvalp).Tree) = new AllocateArrayNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 368:
#line 1682 "parser.yy"
    { ((*yyvalp).Tree) = new AllocateNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), NULL, TreeNode::omitted, TreeNode::omitted); }
    break;

  case 369:
#line 1684 "parser.yy"
    { llist<TreeNode*>* body = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList);
		  ((*yyvalp).Tree) = new AllocateNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList), NULL, TreeNode::omitted, new TreeListNode(body, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Type)->position())); }
    break;

  case 370:
#line 1687 "parser.yy"
    { ((*yyvalp).Tree) = new AllocateNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.Tree), TreeNode::omitted); }
    break;

  case 371:
#line 1689 "parser.yy"
    { llist<TreeNode*>* body = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList);
		  ((*yyvalp).Tree) = new AllocateNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Type), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.TreeList), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.Tree), new TreeListNode(body, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Type)->position())); }
    break;

  case 372:
#line 1695 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree); }
    break;

  case 373:
#line 1697 "parser.yy"
    { ((*yyvalp).Tree) = TreeNode::omitted; }
    break;

  case 374:
#line 1702 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 375:
#line 1704 "parser.yy"
    { ((*yyvalp).TreeList) = cons ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 376:
#line 1709 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 377:
#line 1711 "parser.yy"
    { ((*yyvalp).Tree) = new AllocateArrayDimensionNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Modifiers)); }
    break;

  case 379:
#line 1717 "parser.yy"
    { ((*yyvalp).Int) = 0; }
    break;

  case 380:
#line 1722 "parser.yy"
    { ((*yyvalp).Int) = 1; }
    break;

  case 381:
#line 1724 "parser.yy"
    { ((*yyvalp).Int) = 1; }
    break;

  case 382:
#line 1726 "parser.yy"
    { ((*yyvalp).Int) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Int) + 1; }
    break;

  case 383:
#line 1728 "parser.yy"
    { ((*yyvalp).Int) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.Int) + 1; }
    break;

  case 390:
#line 1751 "parser.yy"
    { ((*yyvalp).Tree) = new PostIncrNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 391:
#line 1756 "parser.yy"
    { ((*yyvalp).Tree) = new PostDecrNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 398:
#line 1776 "parser.yy"
    { ((*yyvalp).Tree) = new UnaryPlusNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 399:
#line 1778 "parser.yy"
    { ((*yyvalp).Tree) = new UnaryMinusNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.SimpTerminal).posn); }
    break;

  case 403:
#line 1786 "parser.yy"
    { ((*yyvalp).Tree) = new PreIncrNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 404:
#line 1791 "parser.yy"
    { ((*yyvalp).Tree) = new PreDecrNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 409:
#line 1806 "parser.yy"
    { ((*yyvalp).Tree) = new ComplementNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 410:
#line 1808 "parser.yy"
    { ((*yyvalp).Tree) = new NotNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 412:
#line 1814 "parser.yy"
    { ((*yyvalp).Tree) = new CastNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Type)); }
    break;

  case 413:
#line 1816 "parser.yy"
    { ((*yyvalp).Tree) = new CastNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree)->asType()); }
    break;

  case 414:
#line 1826 "parser.yy"
    { ((*yyvalp).Tree) = new PointNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 415:
#line 1830 "parser.yy"
    { ((*yyvalp).Tree) = new DomainNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 416:
#line 1831 "parser.yy"
    { ((*yyvalp).Tree) = new DomainNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 417:
#line 1835 "parser.yy"
    { ((*yyvalp).TreeList) = cons((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 418:
#line 1836 "parser.yy"
    { ((*yyvalp).TreeList) = cons((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 419:
#line 1841 "parser.yy"
    { llist<TreeNode*>* children = cons((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), cons((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)));
		  ((*yyvalp).Tree) = new TreeListNode(children); }
    break;

  case 420:
#line 1846 "parser.yy"
    { ((*yyvalp).TreeList) = cons((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 421:
#line 1847 "parser.yy"
    { ((*yyvalp).TreeList) = cons((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.TreeList)); }
    break;

  case 422:
#line 1852 "parser.yy"
    { llist<TreeNode*>* children = cons((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Tree), cons((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), cons((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree))));
		  ((*yyvalp).Tree) = new TreeListNode(children); }
    break;

  case 424:
#line 1861 "parser.yy"
    { ((*yyvalp).Tree) = TreeNode::omitted; }
    break;

  case 426:
#line 1867 "parser.yy"
    { ((*yyvalp).Tree) = new LTNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 427:
#line 1869 "parser.yy"
    { ((*yyvalp).Tree) = new GTNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 428:
#line 1871 "parser.yy"
    { ((*yyvalp).Tree) = new LENode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 429:
#line 1873 "parser.yy"
    { ((*yyvalp).Tree) = new GENode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 430:
#line 1875 "parser.yy"
    { ((*yyvalp).Tree) = new InstanceOfNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Type)); }
    break;

  case 431:
#line 1877 "parser.yy"
    { ((*yyvalp).Tree) = new EQNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 432:
#line 1879 "parser.yy"
    { ((*yyvalp).Tree) = new NENode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 433:
#line 1881 "parser.yy"
    { ((*yyvalp).Tree) = new BitAndNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 434:
#line 1883 "parser.yy"
    { ((*yyvalp).Tree) = new BitOrNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 435:
#line 1885 "parser.yy"
    { ((*yyvalp).Tree) = new BitXorNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 436:
#line 1887 "parser.yy"
    { ((*yyvalp).Tree) = new CandNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 437:
#line 1889 "parser.yy"
    { ((*yyvalp).Tree) = new CorNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 438:
#line 1891 "parser.yy"
    { ((*yyvalp).Tree) = new IfExprNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 439:
#line 1893 "parser.yy"
    { ((*yyvalp).Tree) = new LeftShiftLogNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 440:
#line 1895 "parser.yy"
    { ((*yyvalp).Tree) = new RightShiftLogNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 441:
#line 1897 "parser.yy"
    { ((*yyvalp).Tree) = new RightShiftArithNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 447:
#line 1913 "parser.yy"
    { ((*yyvalp).Tree) = new MultNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 448:
#line 1915 "parser.yy"
    { ((*yyvalp).Tree) = new DivNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 449:
#line 1917 "parser.yy"
    { ((*yyvalp).Tree) = new RemNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 450:
#line 1919 "parser.yy"
    { ((*yyvalp).Tree) = new PlusNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 451:
#line 1921 "parser.yy"
    { ((*yyvalp).Tree) = new MinusNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 452:
#line 1929 "parser.yy"
    { ((*yyvalp).Tree) = new AssignNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 453:
#line 1931 "parser.yy"
    { ((*yyvalp).Tree) = new MultAssignNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 454:
#line 1933 "parser.yy"
    { ((*yyvalp).Tree) = new DivAssignNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 455:
#line 1935 "parser.yy"
    { ((*yyvalp).Tree) = new RemAssignNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 456:
#line 1937 "parser.yy"
    { ((*yyvalp).Tree) = new PlusAssignNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 457:
#line 1939 "parser.yy"
    { ((*yyvalp).Tree) = new MinusAssignNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 458:
#line 1941 "parser.yy"
    { ((*yyvalp).Tree) = new LeftShiftLogAssignNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 459:
#line 1943 "parser.yy"
    { ((*yyvalp).Tree) = new RightShiftLogAssignNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 460:
#line 1945 "parser.yy"
    { ((*yyvalp).Tree) = new RightShiftArithAssignNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 461:
#line 1947 "parser.yy"
    { ((*yyvalp).Tree) = new BitAndAssignNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 462:
#line 1949 "parser.yy"
    { ((*yyvalp).Tree) = new BitXorAssignNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 463:
#line 1951 "parser.yy"
    { ((*yyvalp).Tree) = new BitOrAssignNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 465:
#line 1963 "parser.yy"
    { ((*yyvalp).Tree) = new BroadcastNode ((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.Tree), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree)); }
    break;

  case 466:
#line 1968 "parser.yy"
    { if (*(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Terminal).val != "from")
                  Error(lexerPosition()) << "bad keyword in broadcast - expected 'from'" << endl;
              }
    break;

  case 467:
#line 1979 "parser.yy"
    { ((*yyvalp).Tree) = TreeNode::omitted; }
    break;

  case 468:
#line 1984 "parser.yy"
    { ((*yyvalp).Tree) = TreeNode::omitted; }
    break;

  case 469:
#line 1989 "parser.yy"
    { ((*yyvalp).Tree) = TreeNode::omitted; }
    break;

  case 470:
#line 1994 "parser.yy"
    { ((*yyvalp).TreeList) = cons ( TreeNode::omitted ); }
    break;

  case 471:
#line 1999 "parser.yy"
    { ((*yyvalp).Tree) = TreeNode::omitted; }
    break;

  case 472:
#line 2004 "parser.yy"
    { ((*yyvalp).Tree) = new EmptyStmtNode; }
    break;

  case 473:
#line 2006 "parser.yy"
    { ((*yyvalp).Tree) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.Tree); }
    break;

  case 474:
#line 2011 "parser.yy"
    { ((*yyvalp).TreeList) = NULL; }
    break;

  case 475:
#line 2016 "parser.yy"
    { ((*yyvalp).Tree) = TreeNode::omitted; }
    break;


      default: break;
    }

  return yyok;
# undef yyerrok
# undef YYABORT
# undef YYACCEPT
# undef YYERROR
# undef YYBACKUP
# undef yyclearin
# undef YYRECOVERING
/* Line 872 of glr.c.  */
#line 5007 "y.tab.c"
}


static void
yyuserMerge (int yyn, YYSTYPE* yy0, YYSTYPE* yy1)
{
  /* `Use' the arguments.  */
  (void) yy0;
  (void) yy1;

  switch (yyn)
    {
      
      default: break;
    }
}

			      /* Bison grammar-table manipulation.  */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}

/** Number of symbols composing the right hand side of rule #RULE.  */
static inline int
yyrhsLength (yyRuleNum yyrule)
{
  return yyr2[yyrule];
}

static void
yydestroyGLRState (char const *yymsg, yyGLRState *yys)
{
  if (yys->yyresolved)
    yydestruct (yymsg, yystos[yys->yylrState],
		&yys->yysemantics.yysval);
  else
    {
#if YYDEBUG
      if (yydebug)
	{
	  YYFPRINTF (stderr, "%s unresolved ", yymsg);
	  yysymprint (stderr, yystos[yys->yylrState],
		      &yys->yysemantics.yysval);
	  YYFPRINTF (stderr, "\n");
	}
#endif

      if (yys->yysemantics.yyfirstVal)
        {
          yySemanticOption *yyoption = yys->yysemantics.yyfirstVal;
          yyGLRState *yyrh;
          int yyn;
          for (yyrh = yyoption->yystate, yyn = yyrhsLength (yyoption->yyrule);
               yyn > 0;
               yyrh = yyrh->yypred, yyn -= 1)
            yydestroyGLRState (yymsg, yyrh);
        }
    }
}

/** Left-hand-side symbol for rule #RULE. */
static inline yySymbol
yylhsNonterm (yyRuleNum yyrule)
{
  return yyr1[yyrule];
}

#define yyis_pact_ninf(yystate) \
  ((yystate) == YYPACT_NINF)

/** True iff LR state STATE has only a default reduction (regardless
 *  of token). */
static inline yybool
yyisDefaultedState (yyStateNum yystate)
{
  return yyis_pact_ninf (yypact[yystate]);
}

/** The default reduction for STATE, assuming it has one. */
static inline yyRuleNum
yydefaultAction (yyStateNum yystate)
{
  return yydefact[yystate];
}

#define yyis_table_ninf(yytable_value) \
  0

/** Set *YYACTION to the action to take in YYSTATE on seeing YYTOKEN.
 *  Result R means
 *    R < 0:  Reduce on rule -R.
 *    R = 0:  Error.
 *    R > 0:  Shift to state R.
 *  Set *CONFLICTS to a pointer into yyconfl to 0-terminated list of
 *  conflicting reductions.
 */
static inline void
yygetLRActions (yyStateNum yystate, int yytoken,
	        int* yyaction, const short int** yyconflicts)
{
  int yyindex = yypact[yystate] + yytoken;
  if (yyindex < 0 || YYLAST < yyindex || yycheck[yyindex] != yytoken)
    {
      *yyaction = -yydefact[yystate];
      *yyconflicts = yyconfl;
    }
  else if (! yyis_table_ninf (yytable[yyindex]))
    {
      *yyaction = yytable[yyindex];
      *yyconflicts = yyconfl + yyconflp[yyindex];
    }
  else
    {
      *yyaction = 0;
      *yyconflicts = yyconfl + yyconflp[yyindex];
    }
}

static inline yyStateNum
yyLRgotoState (yyStateNum yystate, yySymbol yylhs)
{
  int yyr;
  yyr = yypgoto[yylhs - YYNTOKENS] + yystate;
  if (0 <= yyr && yyr <= YYLAST && yycheck[yyr] == yystate)
    return yytable[yyr];
  else
    return yydefgoto[yylhs - YYNTOKENS];
}

static inline yybool
yyisShiftAction (int yyaction)
{
  return 0 < yyaction;
}

static inline yybool
yyisErrorAction (int yyaction)
{
  return yyaction == 0;
}

				/* GLRStates */

static void
yyaddDeferredAction (yyGLRStack* yystack, yyGLRState* yystate,
		     yyGLRState* rhs, yyRuleNum yyrule)
{
  yySemanticOption* yynewItem;
  yynewItem = &yystack->yynextFree->yyoption;
  yystack->yyspaceLeft -= 1;
  yystack->yynextFree += 1;
  yynewItem->yyisState = yyfalse;
  yynewItem->yystate = rhs;
  yynewItem->yyrule = yyrule;
  yynewItem->yynext = yystate->yysemantics.yyfirstVal;
  yystate->yysemantics.yyfirstVal = yynewItem;
  if (yystack->yyspaceLeft < YYHEADROOM)
    yyexpandGLRStack (yystack);
}

				/* GLRStacks */

/** Initialize SET to a singleton set containing an empty stack. */
static yybool
yyinitStateSet (yyGLRStateSet* yyset)
{
  yyset->yysize = 1;
  yyset->yycapacity = 16;
  yyset->yystates = (yyGLRState**) YYMALLOC (16 * sizeof yyset->yystates[0]);
  if (! yyset->yystates)
    return yyfalse;
  yyset->yystates[0] = NULL;
  return yytrue;
}

static void yyfreeStateSet (yyGLRStateSet* yyset)
{
  YYFREE (yyset->yystates);
}

/** Initialize STACK to a single empty stack, with total maximum
 *  capacity for all stacks of SIZE. */
static yybool
yyinitGLRStack (yyGLRStack* yystack, size_t yysize)
{
  yystack->yyerrState = 0;
  yynerrs = 0;
  yystack->yyspaceLeft = yysize;
  yystack->yyitems =
    (yyGLRStackItem*) YYMALLOC (yysize * sizeof yystack->yynextFree[0]);
  if (!yystack->yyitems)
    return yyfalse;
  yystack->yynextFree = yystack->yyitems;
  yystack->yysplitPoint = NULL;
  yystack->yylastDeleted = NULL;
  return yyinitStateSet (&yystack->yytops);
}

#define YYRELOC(YYFROMITEMS,YYTOITEMS,YYX,YYTYPE) \
  &((YYTOITEMS) - ((YYFROMITEMS) - (yyGLRStackItem*) (YYX)))->YYTYPE

/** If STACK is expandable, extend it.  WARNING: Pointers into the
    stack from outside should be considered invalid after this call.
    We always expand when there are 1 or fewer items left AFTER an
    allocation, so that we can avoid having external pointers exist
    across an allocation. */
static void
yyexpandGLRStack (yyGLRStack* yystack)
{
#if YYSTACKEXPANDABLE
  yyGLRStackItem* yynewItems;
  yyGLRStackItem* yyp0, *yyp1;
  size_t yysize, yynewSize;
  size_t yyn;
  yysize = yystack->yynextFree - yystack->yyitems;
  if (YYMAXDEPTH <= yysize)
    yyMemoryExhausted (yystack);
  yynewSize = 2*yysize;
  if (YYMAXDEPTH < yynewSize)
    yynewSize = YYMAXDEPTH;
  yynewItems = (yyGLRStackItem*) YYMALLOC (yynewSize * sizeof yynewItems[0]);
  if (! yynewItems)
    yyMemoryExhausted (yystack);
  for (yyp0 = yystack->yyitems, yyp1 = yynewItems, yyn = yysize;
       0 < yyn;
       yyn -= 1, yyp0 += 1, yyp1 += 1)
    {
      *yyp1 = *yyp0;
      if (*(yybool *) yyp0)
	{
	  yyGLRState* yys0 = &yyp0->yystate;
	  yyGLRState* yys1 = &yyp1->yystate;
	  if (yys0->yypred != NULL)
	    yys1->yypred =
	      YYRELOC (yyp0, yyp1, yys0->yypred, yystate);
	  if (! yys0->yyresolved && yys0->yysemantics.yyfirstVal != NULL)
	    yys1->yysemantics.yyfirstVal =
	      YYRELOC(yyp0, yyp1, yys0->yysemantics.yyfirstVal, yyoption);
	}
      else
	{
	  yySemanticOption* yyv0 = &yyp0->yyoption;
	  yySemanticOption* yyv1 = &yyp1->yyoption;
	  if (yyv0->yystate != NULL)
	    yyv1->yystate = YYRELOC (yyp0, yyp1, yyv0->yystate, yystate);
	  if (yyv0->yynext != NULL)
	    yyv1->yynext = YYRELOC (yyp0, yyp1, yyv0->yynext, yyoption);
	}
    }
  if (yystack->yysplitPoint != NULL)
    yystack->yysplitPoint = YYRELOC (yystack->yyitems, yynewItems,
				 yystack->yysplitPoint, yystate);

  for (yyn = 0; yyn < yystack->yytops.yysize; yyn += 1)
    if (yystack->yytops.yystates[yyn] != NULL)
      yystack->yytops.yystates[yyn] =
	YYRELOC (yystack->yyitems, yynewItems,
		 yystack->yytops.yystates[yyn], yystate);
  YYFREE (yystack->yyitems);
  yystack->yyitems = yynewItems;
  yystack->yynextFree = yynewItems + yysize;
  yystack->yyspaceLeft = yynewSize - yysize;

#else
  yyMemoryExhausted (yystack);
#endif
}

static void
yyfreeGLRStack (yyGLRStack* yystack)
{
  YYFREE (yystack->yyitems);
  yyfreeStateSet (&yystack->yytops);
}

/** Assuming that S is a GLRState somewhere on STACK, update the
 *  splitpoint of STACK, if needed, so that it is at least as deep as
 *  S. */
static inline void
yyupdateSplit (yyGLRStack* yystack, yyGLRState* yys)
{
  if (yystack->yysplitPoint != NULL && yystack->yysplitPoint > yys)
    yystack->yysplitPoint = yys;
}

/** Invalidate stack #K in STACK. */
static inline void
yymarkStackDeleted (yyGLRStack* yystack, size_t yyk)
{
  if (yystack->yytops.yystates[yyk] != NULL)
    yystack->yylastDeleted = yystack->yytops.yystates[yyk];
  yystack->yytops.yystates[yyk] = NULL;
}

/** Undelete the last stack that was marked as deleted.  Can only be
    done once after a deletion, and only when all other stacks have
    been deleted. */
static void
yyundeleteLastStack (yyGLRStack* yystack)
{
  if (yystack->yylastDeleted == NULL || yystack->yytops.yysize != 0)
    return;
  yystack->yytops.yystates[0] = yystack->yylastDeleted;
  yystack->yytops.yysize = 1;
  YYDPRINTF ((stderr, "Restoring last deleted stack as stack #0.\n"));
  yystack->yylastDeleted = NULL;
}

static inline void
yyremoveDeletes (yyGLRStack* yystack)
{
  size_t yyi, yyj;
  yyi = yyj = 0;
  while (yyj < yystack->yytops.yysize)
    {
      if (yystack->yytops.yystates[yyi] == NULL)
	{
	  if (yyi == yyj)
	    {
	      YYDPRINTF ((stderr, "Removing dead stacks.\n"));
	    }
	  yystack->yytops.yysize -= 1;
	}
      else
	{
	  yystack->yytops.yystates[yyj] = yystack->yytops.yystates[yyi];
	  if (yyj != yyi)
	    {
	      YYDPRINTF ((stderr, "Rename stack %lu -> %lu.\n",
			  (unsigned long int) yyi, (unsigned long int) yyj));
	    }
	  yyj += 1;
	}
      yyi += 1;
    }
}

/** Shift to a new state on stack #K of STACK, corresponding to LR state
 * LRSTATE, at input position POSN, with (resolved) semantic value SVAL. */
static inline void
yyglrShift (yyGLRStack* yystack, size_t yyk, yyStateNum yylrState,
	    size_t yyposn,
	    YYSTYPE yysval, YYLTYPE* yylocp)
{
  yyGLRStackItem* yynewItem;

  yynewItem = yystack->yynextFree;
  yystack->yynextFree += 1;
  yystack->yyspaceLeft -= 1;
  yynewItem->yystate.yyisState = yytrue;
  yynewItem->yystate.yylrState = yylrState;
  yynewItem->yystate.yyposn = yyposn;
  yynewItem->yystate.yyresolved = yytrue;
  yynewItem->yystate.yypred = yystack->yytops.yystates[yyk];
  yystack->yytops.yystates[yyk] = &yynewItem->yystate;
  yynewItem->yystate.yysemantics.yysval = yysval;
  yynewItem->yystate.yyloc = *yylocp;
  if (yystack->yyspaceLeft < YYHEADROOM)
    yyexpandGLRStack (yystack);
}

/** Shift stack #K of YYSTACK, to a new state corresponding to LR
 *  state YYLRSTATE, at input position YYPOSN, with the (unresolved)
 *  semantic value of YYRHS under the action for YYRULE. */
static inline void
yyglrShiftDefer (yyGLRStack* yystack, size_t yyk, yyStateNum yylrState,
		 size_t yyposn, yyGLRState* rhs, yyRuleNum yyrule)
{
  yyGLRStackItem* yynewItem;

  yynewItem = yystack->yynextFree;
  yynewItem->yystate.yyisState = yytrue;
  yynewItem->yystate.yylrState = yylrState;
  yynewItem->yystate.yyposn = yyposn;
  yynewItem->yystate.yyresolved = yyfalse;
  yynewItem->yystate.yypred = yystack->yytops.yystates[yyk];
  yynewItem->yystate.yysemantics.yyfirstVal = NULL;
  yystack->yytops.yystates[yyk] = &yynewItem->yystate;
  yystack->yynextFree += 1;
  yystack->yyspaceLeft -= 1;
  yyaddDeferredAction (yystack, &yynewItem->yystate, rhs, yyrule);
}

/** Pop the symbols consumed by reduction #RULE from the top of stack
 *  #K of STACK, and perform the appropriate semantic action on their
 *  semantic values.  Assumes that all ambiguities in semantic values
 *  have been previously resolved. Set *VALP to the resulting value,
 *  and *LOCP to the computed location (if any).  Return value is as
 *  for userAction. */
static inline YYRESULTTAG
yydoAction (yyGLRStack* yystack, size_t yyk, yyRuleNum yyrule,
	    YYSTYPE* yyvalp, YYLTYPE* yylocp)
{
  int yynrhs = yyrhsLength (yyrule);

  if (yystack->yysplitPoint == NULL)
    {
      /* Standard special case: single stack. */
      yyGLRStackItem* rhs = (yyGLRStackItem*) yystack->yytops.yystates[yyk];
      YYASSERT (yyk == 0);
      yystack->yynextFree -= yynrhs;
      yystack->yyspaceLeft += yynrhs;
      yystack->yytops.yystates[0] = & yystack->yynextFree[-1].yystate;
      return yyuserAction (yyrule, yynrhs, rhs,
			   yyvalp, yylocp, yystack);
    }
  else
    {
      int yyi;
      yyGLRState* yys;
      yyGLRStackItem yyrhsVals[YYMAXRHS + YYMAXLEFT + 1];
      yys = yyrhsVals[YYMAXRHS + YYMAXLEFT].yystate.yypred
	= yystack->yytops.yystates[yyk];
      for (yyi = 0; yyi < yynrhs; yyi += 1)
	{
	  yys = yys->yypred;
	  YYASSERT (yys);
	}
      yyupdateSplit (yystack, yys);
      yystack->yytops.yystates[yyk] = yys;
      return yyuserAction (yyrule, yynrhs, yyrhsVals + YYMAXRHS + YYMAXLEFT - 1,
			   yyvalp, yylocp, yystack);
    }
}

#if !YYDEBUG
# define YY_REDUCE_PRINT(K, Rule)
#else
# define YY_REDUCE_PRINT(K, Rule)	\
do {					\
  if (yydebug)				\
    yy_reduce_print (K, Rule);		\
} while (0)

/*----------------------------------------------------------.
| Report that the RULE is going to be reduced on stack #K.  |
`----------------------------------------------------------*/

static inline void
yy_reduce_print (size_t yyk, yyRuleNum yyrule)
{
  int yyi;
  YYFPRINTF (stderr, "Reducing stack %lu by rule %d (line %lu), ",
	     (unsigned long int) yyk, yyrule - 1,
	     (unsigned long int) yyrline[yyrule]);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytokenName (yyrhs[yyi]));
  YYFPRINTF (stderr, "-> %s\n", yytokenName (yyr1[yyrule]));
}
#endif

/** Pop items off stack #K of STACK according to grammar rule RULE,
 *  and push back on the resulting nonterminal symbol.  Perform the
 *  semantic action associated with RULE and store its value with the
 *  newly pushed state, if FORCEEVAL or if STACK is currently
 *  unambiguous.  Otherwise, store the deferred semantic action with
 *  the new state.  If the new state would have an identical input
 *  position, LR state, and predecessor to an existing state on the stack,
 *  it is identified with that existing state, eliminating stack #K from
 *  the STACK. In this case, the (necessarily deferred) semantic value is
 *  added to the options for the existing state's semantic value.
 */
static inline YYRESULTTAG
yyglrReduce (yyGLRStack* yystack, size_t yyk, yyRuleNum yyrule,
             yybool yyforceEval)
{
  size_t yyposn = yystack->yytops.yystates[yyk]->yyposn;

  if (yyforceEval || yystack->yysplitPoint == NULL)
    {
      YYSTYPE yysval;
      YYLTYPE yyloc;

      YY_REDUCE_PRINT (yyk, yyrule);
      YYCHK (yydoAction (yystack, yyk, yyrule, &yysval, &yyloc));
      yyglrShift (yystack, yyk,
		  yyLRgotoState (yystack->yytops.yystates[yyk]->yylrState,
				 yylhsNonterm (yyrule)),
		  yyposn, yysval, &yyloc);
    }
  else
    {
      size_t yyi;
      int yyn;
      yyGLRState* yys, *yys0 = yystack->yytops.yystates[yyk];
      yyStateNum yynewLRState;

      for (yys = yystack->yytops.yystates[yyk], yyn = yyrhsLength (yyrule);
	   0 < yyn; yyn -= 1)
	{
	  yys = yys->yypred;
	  YYASSERT (yys);
	}
      yyupdateSplit (yystack, yys);
      yynewLRState = yyLRgotoState (yys->yylrState, yylhsNonterm (yyrule));
      YYDPRINTF ((stderr,
		  "Reduced stack %lu by rule #%d; action deferred. Now in state %d.\n",
		  (unsigned long int) yyk, yyrule - 1, yynewLRState));
      for (yyi = 0; yyi < yystack->yytops.yysize; yyi += 1)
	if (yyi != yyk && yystack->yytops.yystates[yyi] != NULL)
	  {
	    yyGLRState* yyp, *yysplit = yystack->yysplitPoint;
	    yyp = yystack->yytops.yystates[yyi];
	    while (yyp != yys && yyp != yysplit && yyp->yyposn >= yyposn)
	      {
		if (yyp->yylrState == yynewLRState && yyp->yypred == yys)
		  {
		    yyaddDeferredAction (yystack, yyp, yys0, yyrule);
		    yymarkStackDeleted (yystack, yyk);
		    YYDPRINTF ((stderr, "Merging stack %lu into stack %lu.\n",
				(unsigned long int) yyk,
				(unsigned long int) yyi));
		    return yyok;
		  }
		yyp = yyp->yypred;
	      }
	  }
      yystack->yytops.yystates[yyk] = yys;
      yyglrShiftDefer (yystack, yyk, yynewLRState, yyposn, yys0, yyrule);
    }
  return yyok;
}

static size_t
yysplitStack (yyGLRStack* yystack, size_t yyk)
{
  if (yystack->yysplitPoint == NULL)
    {
      YYASSERT (yyk == 0);
      yystack->yysplitPoint = yystack->yytops.yystates[yyk];
    }
  if (yystack->yytops.yysize >= yystack->yytops.yycapacity)
    {
      yyGLRState** yynewStates;
      if (! ((yystack->yytops.yycapacity
	      <= (YYSIZEMAX / (2 * sizeof yynewStates[0])))
	     && (yynewStates =
		 (yyGLRState**) YYREALLOC (yystack->yytops.yystates,
					   ((yystack->yytops.yycapacity *= 2)
					    * sizeof yynewStates[0])))))
	yyMemoryExhausted (yystack);
      yystack->yytops.yystates = yynewStates;
    }
  yystack->yytops.yystates[yystack->yytops.yysize]
    = yystack->yytops.yystates[yyk];
  yystack->yytops.yysize += 1;
  return yystack->yytops.yysize-1;
}

/** True iff Y0 and Y1 represent identical options at the top level.
 *  That is, they represent the same rule applied to RHS symbols
 *  that produce the same terminal symbols. */
static yybool
yyidenticalOptions (yySemanticOption* yyy0, yySemanticOption* yyy1)
{
  if (yyy0->yyrule == yyy1->yyrule)
    {
      yyGLRState *yys0, *yys1;
      int yyn;
      for (yys0 = yyy0->yystate, yys1 = yyy1->yystate,
	   yyn = yyrhsLength (yyy0->yyrule);
	   yyn > 0;
	   yys0 = yys0->yypred, yys1 = yys1->yypred, yyn -= 1)
	if (yys0->yyposn != yys1->yyposn)
	  return yyfalse;
      return yytrue;
    }
  else
    return yyfalse;
}

/** Assuming identicalOptions (Y0,Y1), destructively merge the
 *  alternative semantic values for the RHS-symbols of Y1 and Y0. */
static void
yymergeOptionSets (yySemanticOption* yyy0, yySemanticOption* yyy1)
{
  yyGLRState *yys0, *yys1;
  int yyn;
  for (yys0 = yyy0->yystate, yys1 = yyy1->yystate,
       yyn = yyrhsLength (yyy0->yyrule);
       yyn > 0;
       yys0 = yys0->yypred, yys1 = yys1->yypred, yyn -= 1)
    {
      if (yys0 == yys1)
	break;
      else if (yys0->yyresolved)
	{
	  yys1->yyresolved = yytrue;
	  yys1->yysemantics.yysval = yys0->yysemantics.yysval;
	}
      else if (yys1->yyresolved)
	{
	  yys0->yyresolved = yytrue;
	  yys0->yysemantics.yysval = yys1->yysemantics.yysval;
	}
      else
	{
	  yySemanticOption** yyz0p;
	  yySemanticOption* yyz1;
	  yyz0p = &yys0->yysemantics.yyfirstVal;
	  yyz1 = yys1->yysemantics.yyfirstVal;
	  while (yytrue)
	    {
	      if (yyz1 == *yyz0p || yyz1 == NULL)
		break;
	      else if (*yyz0p == NULL)
		{
		  *yyz0p = yyz1;
		  break;
		}
	      else if (*yyz0p < yyz1)
		{
		  yySemanticOption* yyz = *yyz0p;
		  *yyz0p = yyz1;
		  yyz1 = yyz1->yynext;
		  (*yyz0p)->yynext = yyz;
		}
	      yyz0p = &(*yyz0p)->yynext;
	    }
	  yys1->yysemantics.yyfirstVal = yys0->yysemantics.yyfirstVal;
	}
    }
}

/** Y0 and Y1 represent two possible actions to take in a given
 *  parsing state; return 0 if no combination is possible,
 *  1 if user-mergeable, 2 if Y0 is preferred, 3 if Y1 is preferred. */
static int
yypreference (yySemanticOption* y0, yySemanticOption* y1)
{
  yyRuleNum r0 = y0->yyrule, r1 = y1->yyrule;
  int p0 = yydprec[r0], p1 = yydprec[r1];

  if (p0 == p1)
    {
      if (yymerger[r0] == 0 || yymerger[r0] != yymerger[r1])
	return 0;
      else
	return 1;
    }
  if (p0 == 0 || p1 == 0)
    return 0;
  if (p0 < p1)
    return 3;
  if (p1 < p0)
    return 2;
  return 0;
}

static YYRESULTTAG yyresolveValue (yySemanticOption* yyoptionList,
				   yyGLRStack* yystack, YYSTYPE* yyvalp,
				   YYLTYPE* yylocp);

static YYRESULTTAG
yyresolveStates (yyGLRState* yys, int yyn, yyGLRStack* yystack)
{
  YYRESULTTAG yyflag;
  if (0 < yyn)
    {
      YYASSERT (yys->yypred);
      yyflag = yyresolveStates (yys->yypred, yyn-1, yystack);
      if (yyflag != yyok)
	return yyflag;
      if (! yys->yyresolved)
	{
	  yyflag = yyresolveValue (yys->yysemantics.yyfirstVal, yystack,
				   &yys->yysemantics.yysval, &yys->yyloc
				  );
	  if (yyflag != yyok)
	    return yyflag;
	  yys->yyresolved = yytrue;
	}
    }
  return yyok;
}

static YYRESULTTAG
yyresolveAction (yySemanticOption* yyopt, yyGLRStack* yystack,
	         YYSTYPE* yyvalp, YYLTYPE* yylocp)
{
  yyGLRStackItem yyrhsVals[YYMAXRHS + YYMAXLEFT + 1];
  int yynrhs;

  yynrhs = yyrhsLength (yyopt->yyrule);
  YYCHK (yyresolveStates (yyopt->yystate, yynrhs, yystack));
  yyrhsVals[YYMAXRHS + YYMAXLEFT].yystate.yypred = yyopt->yystate;
  return yyuserAction (yyopt->yyrule, yynrhs,
		       yyrhsVals + YYMAXRHS + YYMAXLEFT - 1,
		       yyvalp, yylocp, yystack);
}

#if YYDEBUG
static void
yyreportTree (yySemanticOption* yyx, int yyindent)
{
  int yynrhs = yyrhsLength (yyx->yyrule);
  int yyi;
  yyGLRState* yys;
  yyGLRState* yystates[YYMAXRHS];
  yyGLRState yyleftmost_state;

  for (yyi = yynrhs, yys = yyx->yystate; 0 < yyi; yyi -= 1, yys = yys->yypred)
    yystates[yyi] = yys;
  if (yys == NULL)
    {
      yyleftmost_state.yyposn = 0;
      yystates[0] = &yyleftmost_state;
    }
  else
    yystates[0] = yys;

  if (yyx->yystate->yyposn < yys->yyposn + 1)
    YYFPRINTF (stderr, "%*s%s -> <Rule %d, empty>\n",
	       yyindent, "", yytokenName (yylhsNonterm (yyx->yyrule)),
	       yyx->yyrule);
  else
    YYFPRINTF (stderr, "%*s%s -> <Rule %d, tokens %lu .. %lu>\n",
	       yyindent, "", yytokenName (yylhsNonterm (yyx->yyrule)),
	       yyx->yyrule, (unsigned long int) (yys->yyposn + 1),
	       (unsigned long int) yyx->yystate->yyposn);
  for (yyi = 1; yyi <= yynrhs; yyi += 1)
    {
      if (yystates[yyi]->yyresolved)
	{
	  if (yystates[yyi-1]->yyposn+1 > yystates[yyi]->yyposn)
	    YYFPRINTF (stderr, "%*s%s <empty>\n", yyindent+2, "",
		       yytokenName (yyrhs[yyprhs[yyx->yyrule]+yyi-1]));
	  else
	    YYFPRINTF (stderr, "%*s%s <tokens %lu .. %lu>\n", yyindent+2, "",
		       yytokenName (yyrhs[yyprhs[yyx->yyrule]+yyi-1]),
		       (unsigned long int) (yystates[yyi - 1]->yyposn + 1),
		       (unsigned long int) yystates[yyi]->yyposn);
	}
      else
	yyreportTree (yystates[yyi]->yysemantics.yyfirstVal, yyindent+2);
    }
}
#endif

static void yyreportAmbiguity (yySemanticOption* yyx0, yySemanticOption* yyx1,
			       yyGLRStack* yystack)
  __attribute__ ((__noreturn__));
static void
yyreportAmbiguity (yySemanticOption* yyx0, yySemanticOption* yyx1,
		   yyGLRStack* yystack)
{
  /* `Unused' warnings.  */
  (void) yyx0;
  (void) yyx1;

#if YYDEBUG
  YYFPRINTF (stderr, "Ambiguity detected.\n");
  YYFPRINTF (stderr, "Option 1,\n");
  yyreportTree (yyx0, 2);
  YYFPRINTF (stderr, "\nOption 2,\n");
  yyreportTree (yyx1, 2);
  YYFPRINTF (stderr, "\n");
#endif
  yyFail (yystack, YY_("syntax is ambiguous"));
}


/** Resolve the ambiguity represented by OPTIONLIST, perform the indicated
 *  actions, and return the result. */
static YYRESULTTAG
yyresolveValue (yySemanticOption* yyoptionList, yyGLRStack* yystack,
		YYSTYPE* yyvalp, YYLTYPE* yylocp)
{
  yySemanticOption* yybest;
  yySemanticOption** yypp;
  yybool yymerge;

  yybest = yyoptionList;
  yymerge = yyfalse;
  for (yypp = &yyoptionList->yynext; *yypp != NULL; )
    {
      yySemanticOption* yyp = *yypp;

      if (yyidenticalOptions (yybest, yyp))
	{
	  yymergeOptionSets (yybest, yyp);
	  *yypp = yyp->yynext;
	}
      else
	{
	  switch (yypreference (yybest, yyp))
	    {
	    case 0:
	      yyreportAmbiguity (yybest, yyp, yystack);
	      break;
	    case 1:
	      yymerge = yytrue;
	      break;
	    case 2:
	      break;
	    case 3:
	      yybest = yyp;
	      yymerge = yyfalse;
	      break;
	    default:
	      /* This cannot happen so it is not worth a YYASSERT (yyfalse),
	         but some compilers complain if the default case is
		 omitted.  */
	      break;
	    }
	  yypp = &yyp->yynext;
	}
    }

  if (yymerge)
    {
      yySemanticOption* yyp;
      int yyprec = yydprec[yybest->yyrule];
      YYCHK (yyresolveAction (yybest, yystack, yyvalp, yylocp));
      for (yyp = yybest->yynext; yyp != NULL; yyp = yyp->yynext)
	{
	  if (yyprec == yydprec[yyp->yyrule])
	    {
	      YYSTYPE yyval1;
	      YYLTYPE yydummy;
	      YYCHK (yyresolveAction (yyp, yystack, &yyval1, &yydummy));
	      yyuserMerge (yymerger[yyp->yyrule], yyvalp, &yyval1);
	    }
	}
      return yyok;
    }
  else
    return yyresolveAction (yybest, yystack, yyvalp, yylocp);
}

static YYRESULTTAG
yyresolveStack (yyGLRStack* yystack)
{
  if (yystack->yysplitPoint != NULL)
    {
      yyGLRState* yys;
      int yyn;

      for (yyn = 0, yys = yystack->yytops.yystates[0];
	   yys != yystack->yysplitPoint;
	   yys = yys->yypred, yyn += 1)
	continue;
      YYCHK (yyresolveStates (yystack->yytops.yystates[0], yyn, yystack
			     ));
    }
  return yyok;
}

static void
yycompressStack (yyGLRStack* yystack)
{
  yyGLRState* yyp, *yyq, *yyr;

  if (yystack->yytops.yysize != 1 || yystack->yysplitPoint == NULL)
    return;

  for (yyp = yystack->yytops.yystates[0], yyq = yyp->yypred, yyr = NULL;
       yyp != yystack->yysplitPoint;
       yyr = yyp, yyp = yyq, yyq = yyp->yypred)
    yyp->yypred = yyr;

  yystack->yyspaceLeft += yystack->yynextFree - yystack->yyitems;
  yystack->yynextFree = ((yyGLRStackItem*) yystack->yysplitPoint) + 1;
  yystack->yyspaceLeft -= yystack->yynextFree - yystack->yyitems;
  yystack->yysplitPoint = NULL;
  yystack->yylastDeleted = NULL;

  while (yyr != NULL)
    {
      yystack->yynextFree->yystate = *yyr;
      yyr = yyr->yypred;
      yystack->yynextFree->yystate.yypred = & yystack->yynextFree[-1].yystate;
      yystack->yytops.yystates[0] = &yystack->yynextFree->yystate;
      yystack->yynextFree += 1;
      yystack->yyspaceLeft -= 1;
    }
}

static YYRESULTTAG
yyprocessOneStack (yyGLRStack* yystack, size_t yyk,
	           size_t yyposn, YYSTYPE* yylvalp, YYLTYPE* yyllocp
		  )
{
  int yyaction;
  const short int* yyconflicts;
  yyRuleNum yyrule;
  yySymbol* const yytokenp = yystack->yytokenp;

  while (yystack->yytops.yystates[yyk] != NULL)
    {
      yyStateNum yystate = yystack->yytops.yystates[yyk]->yylrState;
      YYDPRINTF ((stderr, "Stack %lu Entering state %d\n",
		  (unsigned long int) yyk, yystate));

      YYASSERT (yystate != YYFINAL);

      if (yyisDefaultedState (yystate))
	{
	  yyrule = yydefaultAction (yystate);
	  if (yyrule == 0)
	    {
	      YYDPRINTF ((stderr, "Stack %lu dies.\n",
			  (unsigned long int) yyk));
	      yymarkStackDeleted (yystack, yyk);
	      return yyok;
	    }
	  YYCHK (yyglrReduce (yystack, yyk, yyrule, yyfalse));
	}
      else
	{
	  if (*yytokenp == YYEMPTY)
	    {
	      YYDPRINTF ((stderr, "Reading a token: "));
	      yychar = YYLEX;
	      *yytokenp = YYTRANSLATE (yychar);
	      YY_SYMBOL_PRINT ("Next token is", *yytokenp, yylvalp, yyllocp);
	    }
	  yygetLRActions (yystate, *yytokenp, &yyaction, &yyconflicts);

	  while (*yyconflicts != 0)
	    {
	      size_t yynewStack = yysplitStack (yystack, yyk);
	      YYDPRINTF ((stderr, "Splitting off stack %lu from %lu.\n",
			  (unsigned long int) yynewStack,
			  (unsigned long int) yyk));
	      YYCHK (yyglrReduce (yystack, yynewStack,
				  *yyconflicts, yyfalse));
	      YYCHK (yyprocessOneStack (yystack, yynewStack, yyposn,
					yylvalp, yyllocp));
	      yyconflicts += 1;
	    }

	  if (yyisShiftAction (yyaction))
	    {
	      YYDPRINTF ((stderr, "On stack %lu, ", (unsigned long int) yyk));
	      YY_SYMBOL_PRINT ("shifting", *yytokenp, yylvalp, yyllocp);
	      yyglrShift (yystack, yyk, yyaction, yyposn+1,
			  *yylvalp, yyllocp);
	      YYDPRINTF ((stderr, "Stack %lu now in state #%d\n",
			  (unsigned long int) yyk,
			  yystack->yytops.yystates[yyk]->yylrState));
	      break;
	    }
	  else if (yyisErrorAction (yyaction))
	    {
	      YYDPRINTF ((stderr, "Stack %lu dies.\n",
			  (unsigned long int) yyk));
	      yymarkStackDeleted (yystack, yyk);
	      break;
	    }
	  else
	    YYCHK (yyglrReduce (yystack, yyk, -yyaction, yyfalse));
	}
    }
  return yyok;
}

static void
yyreportSyntaxError (yyGLRStack* yystack,
		     YYSTYPE* yylvalp, YYLTYPE* yyllocp)
{
  /* `Unused' warnings. */
  (void) yylvalp;
  (void) yyllocp;

  if (yystack->yyerrState == 0)
    {
#if YYERROR_VERBOSE
      yySymbol* const yytokenp = yystack->yytokenp;
      int yyn;
      yyn = yypact[yystack->yytops.yystates[0]->yylrState];
      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  size_t yysize0 = yytnamerr (NULL, yytokenName (*yytokenp));
	  size_t yysize = yysize0;
	  size_t yysize1;
	  yybool yysize_overflow = yyfalse;
	  char* yymsg = NULL;
	  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
	  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
	  int yyx;
	  char *yyfmt;
	  char const *yyf;
	  static char const yyunexpected[] = "syntax error, unexpected %s";
	  static char const yyexpecting[] = ", expecting %s";
	  static char const yyor[] = " or %s";
	  char yyformat[sizeof yyunexpected
			+ sizeof yyexpecting - 1
			+ ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
			   * (sizeof yyor - 1))];
	  char const *yyprefix = yyexpecting;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 1;

	  yyarg[0] = yytokenName (*yytokenp);
	  yyfmt = yystpcpy (yyformat, yyunexpected);

	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
		  {
		    yycount = 1;
		    yysize = yysize0;
		    yyformat[sizeof yyunexpected - 1] = '\0';
		    break;
		  }
		yyarg[yycount++] = yytokenName (yyx);
		yysize1 = yysize + yytnamerr (NULL, yytokenName (yyx));
		yysize_overflow |= yysize1 < yysize;
		yysize = yysize1;
		yyfmt = yystpcpy (yyfmt, yyprefix);
		yyprefix = yyor;
	      }

	  yyf = YY_(yyformat);
	  yysize1 = yysize + strlen (yyf);
	  yysize_overflow |= yysize1 < yysize;
	  yysize = yysize1;

	  if (!yysize_overflow)
	    yymsg = (char *) YYMALLOC (yysize);

	  if (yymsg)
	    {
	      char *yyp = yymsg;
	      int yyi = 0;
	      while ((*yyp = *yyf))
		{
		  if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		    {
		      yyp += yytnamerr (yyp, yyarg[yyi++]);
		      yyf += 2;
		    }
		  else
		    {
		      yyp++;
		      yyf++;
		    }
		}
	      yyerror (yymsg);
	      YYFREE (yymsg);
	    }
	  else
	    {
	      yyerror (YY_("syntax error"));
	      yyMemoryExhausted (yystack);
	    }
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror (YY_("syntax error"));
      yynerrs += 1;
    }
}

/* Recover from a syntax error on YYSTACK, assuming that YYTOKENP,
   YYLVALP, and YYLLOCP point to the syntactic category, semantic
   value, and location of the look-ahead.  */
static void
yyrecoverSyntaxError (yyGLRStack* yystack,
		      YYSTYPE* yylvalp,
		      YYLTYPE* YYOPTIONAL_LOC (yyllocp)
		      )
{
  yySymbol* const yytokenp = yystack->yytokenp;
  size_t yyk;
  int yyj;

  if (yystack->yyerrState == 3)
    /* We just shifted the error token and (perhaps) took some
       reductions.  Skip tokens until we can proceed.  */
    while (yytrue)
      {
	if (*yytokenp == YYEOF)
	  yyFail (yystack, NULL);
	if (*yytokenp != YYEMPTY)
	  {
	    yydestruct ("Error: discarding",
			*yytokenp, yylvalp);
	  }
	YYDPRINTF ((stderr, "Reading a token: "));
	yychar = YYLEX;
	*yytokenp = YYTRANSLATE (yychar);
	YY_SYMBOL_PRINT ("Next token is", *yytokenp, yylvalp, yyllocp);
	yyj = yypact[yystack->yytops.yystates[0]->yylrState];
	if (yyis_pact_ninf (yyj))
	  return;
	yyj += *yytokenp;
	if (yyj < 0 || YYLAST < yyj || yycheck[yyj] != *yytokenp)
	  {
	    if (yydefact[yystack->yytops.yystates[0]->yylrState] != 0)
	      return;
	  }
	else if (yytable[yyj] != 0 && ! yyis_table_ninf (yytable[yyj]))
	  return;
      }

  /* Reduce to one stack.  */
  for (yyk = 0; yyk < yystack->yytops.yysize; yyk += 1)
    if (yystack->yytops.yystates[yyk] != NULL)
      break;
  if (yyk >= yystack->yytops.yysize)
    yyFail (yystack, NULL);
  for (yyk += 1; yyk < yystack->yytops.yysize; yyk += 1)
    yymarkStackDeleted (yystack, yyk);
  yyremoveDeletes (yystack);
  yycompressStack (yystack);

  /* Now pop stack until we find a state that shifts the error token. */
  yystack->yyerrState = 3;
  while (yystack->yytops.yystates[0] != NULL)
    {
      yyGLRState *yys = yystack->yytops.yystates[0];
      yyj = yypact[yys->yylrState];
      if (! yyis_pact_ninf (yyj))
	{
	  yyj += YYTERROR;
	  if (0 <= yyj && yyj <= YYLAST && yycheck[yyj] == YYTERROR
	      && yyisShiftAction (yytable[yyj]))
	    {
	      /* Shift the error token having adjusted its location.  */
	      YYLTYPE yyerrloc;
	      YY_SYMBOL_PRINT ("Shifting", yystos[yytable[yyj]],
			       yylvalp, &yyerrloc);
	      yyglrShift (yystack, 0, yytable[yyj],
			  yys->yyposn, *yylvalp, &yyerrloc);
	      yys = yystack->yytops.yystates[0];
	      break;
	    }
	}

      yydestroyGLRState ("Error: popping", yys);
      yystack->yytops.yystates[0] = yys->yypred;
      yystack->yynextFree -= 1;
      yystack->yyspaceLeft += 1;
    }
  if (yystack->yytops.yystates[0] == NULL)
    yyFail (yystack, NULL);
}

#define YYCHK1(YYE)							     \
  do {									     \
    switch (YYE) {							     \
    case yyok:								     \
      break;								     \
    case yyabort:							     \
      goto yyabortlab;							     \
    case yyaccept:							     \
      goto yyacceptlab;							     \
    case yyerr:								     \
      goto yyuser_error;						     \
    default:								     \
      goto yybuglab;							     \
    }									     \
  } while (0)


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
  int yyresult;
  yySymbol yytoken;
  yyGLRStack yystack;
  size_t yyposn;


  YYSTYPE* const yylvalp = &yylval;
  YYLTYPE* const yyllocp = &yylloc;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yytoken = YYEMPTY;
  yylval = yyval_default;


  if (! yyinitGLRStack (&yystack, YYINITDEPTH))
    goto yyexhaustedlab;
  switch (YYSETJMP (yystack.yyexception_buffer))
    {
    case 0: break;
    case 1: goto yyabortlab;
    case 2: goto yyexhaustedlab;
    default: goto yybuglab;
    }
  yystack.yytokenp = &yytoken;
  yyglrShift (&yystack, 0, 0, 0, yylval, &yylloc);
  yyposn = 0;

  while (yytrue)
    {
      /* For efficiency, we have two loops, the first of which is
	 specialized to deterministic operation (single stack, no
	 potential ambiguity).  */
      /* Standard mode */
      while (yytrue)
	{
	  yyRuleNum yyrule;
	  int yyaction;
	  const short int* yyconflicts;

	  yyStateNum yystate = yystack.yytops.yystates[0]->yylrState;
          YYDPRINTF ((stderr, "Entering state %d\n", yystate));
	  if (yystate == YYFINAL)
	    goto yyacceptlab;
	  if (yyisDefaultedState (yystate))
	    {
	      yyrule = yydefaultAction (yystate);
	      if (yyrule == 0)
		{

		  yyreportSyntaxError (&yystack, yylvalp, yyllocp);
		  goto yyuser_error;
		}
	      YYCHK1 (yyglrReduce (&yystack, 0, yyrule, yytrue));
	    }
	  else
	    {
	      if (yytoken == YYEMPTY)
		{
		  YYDPRINTF ((stderr, "Reading a token: "));
		  yychar = YYLEX;
		  yytoken = YYTRANSLATE (yychar);
                  YY_SYMBOL_PRINT ("Next token is", yytoken, yylvalp, yyllocp);
		}
	      yygetLRActions (yystate, yytoken, &yyaction, &yyconflicts);
	      if (*yyconflicts != 0)
		break;
	      if (yyisShiftAction (yyaction))
		{
		  YY_SYMBOL_PRINT ("Shifting", yytoken, yylvalp, yyllocp);
		  if (yytoken != YYEOF)
		    yytoken = YYEMPTY;
		  yyposn += 1;
		  yyglrShift (&yystack, 0, yyaction, yyposn, yylval, yyllocp);
		  if (0 < yystack.yyerrState)
		    yystack.yyerrState -= 1;
		}
	      else if (yyisErrorAction (yyaction))
		{

		  yyreportSyntaxError (&yystack, yylvalp, yyllocp);
		  goto yyuser_error;
		}
	      else
		YYCHK1 (yyglrReduce (&yystack, 0, -yyaction, yytrue));
	    }
	}

      while (yytrue)
	{
	  size_t yys;
	  size_t yyn = yystack.yytops.yysize;
	  for (yys = 0; yys < yyn; yys += 1)
	    YYCHK1 (yyprocessOneStack (&yystack, yys, yyposn,
				       yylvalp, yyllocp));
	  yytoken = YYEMPTY;
	  yyposn += 1;
	  yyremoveDeletes (&yystack);
	  if (yystack.yytops.yysize == 0)
	    {
	      yyundeleteLastStack (&yystack);
	      if (yystack.yytops.yysize == 0)
		yyFail (&yystack, YY_("syntax error"));
	      YYCHK1 (yyresolveStack (&yystack));
	      YYDPRINTF ((stderr, "Returning to deterministic operation.\n"));

	      yyreportSyntaxError (&yystack, yylvalp, yyllocp);
	      goto yyuser_error;
	    }
	  else if (yystack.yytops.yysize == 1)
	    {
	      YYCHK1 (yyresolveStack (&yystack));
	      YYDPRINTF ((stderr, "Returning to deterministic operation.\n"));
	      yycompressStack (&yystack);
	      break;
	    }
	}
      continue;
    yyuser_error:
      yyrecoverSyntaxError (&yystack, yylvalp, yyllocp);
      yyposn = yystack.yytops.yystates[0]->yyposn;
    }

 yyacceptlab:
  yyresult = 0;
  goto yyreturn;

 yybuglab:
  YYASSERT (yyfalse);
  /* Fall through.  */

 yyabortlab:
  yyresult = 1;
  goto yyreturn;

 yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */

 yyreturn:
  if (yytoken != YYEOF && yytoken != YYEMPTY)
    yydestruct ("Cleanup: discarding lookahead",
                yytoken, yylvalp);

  /* If the stack is well-formed, pop the stack until it is empty,
     destroying its entries as we go.  But free the stack regardless
     of whether it is well-formed.  */
  if (yystack.yyitems)
    {
      yyGLRState** yystates = yystack.yytops.yystates;
      if (yystates)
	while (yystates[0])
	  {
	    yyGLRState *yys = yystates[0];
	    yydestroyGLRState ("Cleanup: popping", yys);
	    yystates[0] = yys->yypred;
	    yystack.yynextFree -= 1;
	    yystack.yyspaceLeft += 1;
	  }
      yyfreeGLRStack (&yystack);
    }

  return yyresult;
}

/* DEBUGGING ONLY */
#ifdef YYDEBUG
static void yypstack (yyGLRStack* yystack, size_t yyk)
  __attribute__ ((__unused__));
static void yypdumpstack (yyGLRStack* yystack) __attribute__ ((__unused__));

static void
yy_yypstack (yyGLRState* yys)
{
  if (yys->yypred)
    {
      yy_yypstack (yys->yypred);
      fprintf (stderr, " -> ");
    }
  fprintf (stderr, "%d@%lu", yys->yylrState, (unsigned long int) yys->yyposn);
}

static void
yypstates (yyGLRState* yyst)
{
  if (yyst == NULL)
    fprintf (stderr, "<null>");
  else
    yy_yypstack (yyst);
  fprintf (stderr, "\n");
}

static void
yypstack (yyGLRStack* yystack, size_t yyk)
{
  yypstates (yystack->yytops.yystates[yyk]);
}

#define YYINDEX(YYX)							     \
    ((YYX) == NULL ? -1 : (yyGLRStackItem*) (YYX) - yystack->yyitems)


static void
yypdumpstack (yyGLRStack* yystack)
{
  yyGLRStackItem* yyp;
  size_t yyi;
  for (yyp = yystack->yyitems; yyp < yystack->yynextFree; yyp += 1)
    {
      fprintf (stderr, "%3lu. ", (unsigned long int) (yyp - yystack->yyitems));
      if (*(yybool *) yyp)
	{
	  fprintf (stderr, "Res: %d, LR State: %d, posn: %lu, pred: %ld",
		   yyp->yystate.yyresolved, yyp->yystate.yylrState,
		   (unsigned long int) yyp->yystate.yyposn,
		   (long int) YYINDEX (yyp->yystate.yypred));
	  if (! yyp->yystate.yyresolved)
	    fprintf (stderr, ", firstVal: %ld",
		     (long int) YYINDEX (yyp->yystate.yysemantics.yyfirstVal));
	}
      else
	{
	  fprintf (stderr, "Option. rule: %d, state: %ld, next: %ld",
		   yyp->yyoption.yyrule,
		   (long int) YYINDEX (yyp->yyoption.yystate),
		   (long int) YYINDEX (yyp->yyoption.yynext));
	}
      fprintf (stderr, "\n");
    }
  fprintf (stderr, "Tops:");
  for (yyi = 0; yyi < yystack->yytops.yysize; yyi += 1)
    fprintf (stderr, "%lu: %ld; ", (unsigned long int) yyi,
	     (long int) YYINDEX (yystack->yytops.yystates[yyi]));
  fprintf (stderr, "\n");
}
#endif


#line 2020 "parser.yy"


static void yyerror(const char* msg)
{
  Error(yylval.SimpTerminal.posn) << msg << endl;
}

static TreeNode *newOperator(char *op, SourcePosn p)
{
  return new NameNode(TreeNode::omitted, intern(op), NULL, p);
}

/* Check that only the modifiers in ... are in FLAGS, reporting an */
/* error at POSN if not. */
static void checkModifiers(SourcePosn posn, Common::Modifiers flags, ...)
{
  unsigned long flag;
  va_list ap;

  va_start (ap, flags);
  while (true) {
    flag = va_arg (ap, unsigned long);
    if (flag == 0)
      break;
    flags = (Common::Modifiers)((unsigned long)flags & ~flag);
  }
  va_end (ap);

  if ((unsigned long) flags != 0)
    Error(posn) << "illegal modifiers: " << stringifyModifiers(flags) << endl;
}

static void checkFieldModifiers(Common::Modifiers flags, 
				SourcePosn posn)
{
  checkModifiers(posn, flags, 
		 TreeNode::Public, TreeNode::Protected, TreeNode::Private,
		 TreeNode::Static, TreeNode::Final,
		 TreeNode::Transient, TreeNode::Volatile,
		 (Common::Modifiers) 0);
}

static void checkConstantFieldModifiers(Common::Modifiers flags, 
					SourcePosn posn)
{
  checkModifiers(posn, flags, 
		 TreeNode::Public, TreeNode::Static, TreeNode::Final,
		 (Common::Modifiers) 0);
}

static void checkMethodModifiers(Common::Modifiers flags, 
				 SourcePosn posn)
{
  checkModifiers(posn, flags, 
		 TreeNode::Public, TreeNode::Protected, TreeNode::Private,
		 TreeNode::Static, TreeNode::Final,
		 TreeNode::Abstract, TreeNode::Native, 
		 TreeNode::Sglobal, TreeNode::Local,
		 TreeNode::NonsharedQ, TreeNode::PolysharedQ,
		 TreeNode::Synchronized, TreeNode::Inline,
		 TreeNode::Strictfp,
		 (Common::Modifiers) 0);
}

static void checkConstructorModifiers(Common::Modifiers flags, 
				      SourcePosn posn)
{
  checkModifiers(posn, flags, 
		 TreeNode::Public, TreeNode::Protected, TreeNode::Private,
		 TreeNode::NonsharedQ, TreeNode::PolysharedQ,
		 TreeNode::Sglobal, TreeNode::Inline,
		 (Common::Modifiers) 0);
}

static void checkMethodSignatureModifiers(Common::Modifiers flags, 
					  SourcePosn posn)
{
  checkModifiers(posn, flags, 
		 TreeNode::Public, TreeNode::Abstract,
		 TreeNode::Sglobal, TreeNode::Local,
		 TreeNode::NonsharedQ, TreeNode::PolysharedQ,
		 (Common::Modifiers) 0);
}

static void checkInterfaceModifiers(Common::Modifiers flags, 
				    SourcePosn posn)
{
  checkModifiers(posn, flags, 
		 TreeNode::Public, TreeNode::Abstract, 
		 TreeNode::Strictfp,
		 (Common::Modifiers) 0);
}

static void checkNestedTypeModifiers(Common::Modifiers flags, 
				     SourcePosn posn)
{
  checkModifiers(posn, flags,
		 TreeNode::Public, TreeNode::Protected, TreeNode::Private,
		 TreeNode::Static, TreeNode::Final, TreeNode::Abstract, 
		 TreeNode::Immutable, TreeNode::Strictfp,
		 (Common::Modifiers) 0);
}

static void checkLocalTypeModifiers(Common::Modifiers flags, 
				     SourcePosn posn)
{
  checkModifiers(posn, flags,
		 TreeNode::Final, TreeNode::Abstract, TreeNode::Strictfp,
		 (Common::Modifiers) 0);
}

static const string *isTitaniumTypeName(TypeNode *t) {
  if (isTypeNameNode(t) && 
      isNameNode(t->name()) &&
      t->name()->qualifier()->absent()) {
    const string *id = t->name()->ident();
    if (*id == "Point" || 
        *id == "RectDomain" ||
        *id == "Domain") return id; 
  }
  return NULL;
}

static TypeNode *buildTemplateInstanceType(TreeNode *tn, llist<TreeNode*> *args) {
  TypeNode *t = new TypeNameNode(tn);
  const string *tname = isTitaniumTypeName(t);
  if (tname) {
    TreeNode *arg = TreeNode::omitted;
    if (args->size() != 1) {
      Error(t->position()) << *tname << " requires exactly one argument" << endl;
    } else if (args->front()->isTypeNode()) {
      Error(t->position()) << "Bad argument to " << *tname << endl;
    } else arg = args->front();

    if (isArrayNameNode(arg)) arg = arg->asExpr();

    if (*tname == "Point") return new PointTypeNode(arg, t->position());
    else if (*tname == "RectDomain") return new RectDomainTypeNode(arg, t->position());
    else { assert(*tname == "Domain"); return new DomainTypeNode(arg, t->position()); }
  } else /* not a Titanium type - a regular template */
     return new TemplateInstanceTypeNode( t, args, 0 );
}

static TreeNode *buildTemplateQualifiedName(TypeNode *tmpl, TreeNode *name)
{
  if (name->qualifier()->absent())
    return new NameNode(new TemplateNameNode(TreeNode::omitted, tmpl, tmpl->position()), 
                        name->ident(), NULL, name->position());
  else 
    return new NameNode(buildTemplateQualifiedName(tmpl, name->qualifier()), 
                        name->ident(), NULL, name->position());
}

static TreeNode *addSynchronized(Common::Modifiers modifiers, TreeNode *body)
{
  if ((modifiers & TreeNode::Synchronized) != 0 && !body->absent()) {
    TreeNode *syncObject;
    if ((modifiers & TreeNode::Static) != 0) {
      syncObject = TreeNode::omitted;
    }
    else {
      syncObject = new ThisNode(TreeNode::omitted, NULL, Common::None, body->position());
    }
    body = new SynchronizedNode(syncObject, body);
  }
  return body;
}

