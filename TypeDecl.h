#ifndef _TYPE_DECL_H_
#define _TYPE_DECL_H_

#include <list>
#include <string>
#include "BadSubfield.h"
#include "decls.h"

class TypeNode;
class CtType;


class TypeDecl : public Decl {
public:
  bool isType() const;
  TypeNode *asType() const;
  
  virtual bool allFieldsShared() { return true; }
  virtual void collectEmbeddedLocals( list< BadSubfield > &, const Subfield * = 0 ) = 0;
  virtual bool containsEmbeddedLocals() = 0;

  const CtType &cDescriptorType() const;
  const CtType &cType() const;

protected:
  TypeDecl( const string *, TypeNode * );

private:
  TypeNode * const _asType;
  mutable const CtType *_cType;
  mutable const CtType *_cDesc;
};


#endif // !_TYPE_DECL_H_


// Local Variables:
// c-file-style: "gnu"
// End:
