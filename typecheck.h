#ifndef _INCLUDE_TITANIUM_TYPECHECK_H_
#define _INCLUDE_TITANIUM_TYPECHECK_H_

#include "llist.h"

class ClassDecl;
class ConstructorDeclNode;
class MethodDeclNode;


class TreeNode::TypeCheckContext
{
public:
  TypeCheckContext() : currentMethod(NULL), currentConstructor(NULL),
    allowedExceptions(NULL), dtype(NULL) { }
  TypeCheckContext(TypeCheckContext *old) { *this = *old; }
  MethodDeclNode *currentMethod;
  ConstructorDeclNode *currentConstructor;
  llist<TypeNode *> *allowedExceptions;
  TypeNode *dtype;
  bool inStatic;
  bool inInitializer;
  Decl *currentClass;
};


#endif // !_INCLUDE_TITANIUM_TYPECHECK_H_
