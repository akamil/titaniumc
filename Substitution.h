#ifndef _SUBSTITUTION_H_
#define _SUBSTITUTION_H_


#include "llist.h"

class Decl;
class TreeNode;


// A substitution of d by with
struct Substitution
{
  Decl *d;
  TreeNode *with;
  Substitution(Decl *_d, TreeNode *_with) { d = _d; with = _with; }
};


typedef llist<Substitution *> Subst;


#endif // !_SUBSTITUTION_H_
