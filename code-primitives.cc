#include "AST.h"


const char *BoolTypeNode::cPrimitiveTypeName() const
{
  return "jboolean";
}


const char *ByteTypeNode::cPrimitiveTypeName() const
{
  return "jbyte";
}


const char *CharTypeNode::cPrimitiveTypeName() const
{
  return "jchar";
}


const char *DoubleTypeNode::cPrimitiveTypeName() const
{
  return "jdouble";
}


const char *FloatTypeNode::cPrimitiveTypeName() const
{
  return "jfloat";
}


const char *IntTypeNode::cPrimitiveTypeName() const
{
  return "jint";
}


const char *LongTypeNode::cPrimitiveTypeName() const
{
  return "jlong";
}


const char *NullTypeNode::cPrimitiveTypeName() const
{
  return "void";
}


const char *ShortTypeNode::cPrimitiveTypeName() const
{
  return "jshort";
}


const char *VoidTypeNode::cPrimitiveTypeName() const
{
  return "void";
}
