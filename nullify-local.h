#ifndef _include_nullify_local_h_
#define _include_nullify_local_h_


class CfCode;
class FieldDecl;

void fieldNullifyLocal( FieldDecl &, CfCode & );


#endif // !_include_nullify_local_h_
