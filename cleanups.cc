#include "AST.h"


void ExprNode::collectCleanups()
{
  // short circuit the descent: no break, continue,
  // or return can possibly appear below here
}


void TemplateDeclNode::collectCleanups()
{
}


void TreeNode::collectCleanups()
{
  foriter (subtree, allChildren(), ChildIter)
    (*subtree)->collectCleanups();
}


////////////////////////////////////////////////////////////////////////


void BreakNode::collectCleanups()
{
  assert( !_cleanups );
  unwindTree( _cleanups, destination() );
}


void ContinueNode::collectCleanups()
{
  assert( !_cleanups );
  unwindTree( _cleanups, destination() );
}


void ReturnNode::collectCleanups()
{
  // an unwind destination of "0" designates the
  // enclosing method or constructor body
  
  assert( !_cleanups );
  unwindTree( _cleanups, 0 );
}
