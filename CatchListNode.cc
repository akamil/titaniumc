#include "AST.h"
#include "CodeContext.h"


void CatchListNode::codeGen( CodeContext &context )
{
    if (arity()) {
	context << "else {" << endCline
		<< "    static const ti_ExceptionHandler handler_table[] = {" << endCline;
	
	foriter (handler, allChildren(), ChildIter)
	    (*handler)->emitCatchTable( context );

	context << "      {0,0}" << endCline
		<< "    };" << endCline
		<< "    const int handler = find_handler( &local_exnstate, handler_table );" << endCline
		<< endCline
		<< "    local_exnstate.handling = 1;" << endCline
		<< "    switch (handler) {" << endCline;
	
	for (int handler = 0; handler < arity(); ++handler) {
	    context << "    case " << handler << ":" << endCline;
	    child( handler )->codeGen( context );
	    context << "      break;" << endCline;
	}
	context << "    }" << endCline
		<< "    if (handler != NO_HANDLER) globalize(local_exnstate.exn_instance, (instance_header *) 0);" << endCline
		<< "}" << endCline;
    }
}
