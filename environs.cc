/* environs.cc: Implementations of environments */

/* Copyright (C) 1996, Paul N. Hilfinger.  All Rights Reserved.		*/

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include "utils.h"
#include "AST.h"
#include "compiler.h"
#include "decls.h"


				/* Class EnvironIter */

#define member EnvironIter::

static vector<EnvironEntry*> dummyVect;

member EnvironIter()
    : _next (NULL), here (dummyVect.end()), 
      end (dummyVect.end()), name (NULL)
{ }

void member next()
{
    assert (here != end);
    ++here;
    skip();
}

void member skip()
{
    while (true) {
	while (here == end && _next) {
	    here = _next->decls->begin();
	    end = _next->decls->end();
	    _next = _next->next;
	}
	if (here == end)
	    return;
	while (here != end &&
	       !((mask == -1 || (mask & (*here)->decl()->category()) != 0) &&
		 (name == NULL || name == (*here)->name())))
	    ++here;
	if (here != end)
	    return;
    }
}

member EnvironIter (Environ* next0,
		    vector<EnvironEntry*>::const_iterator begin0,
		    vector<EnvironEntry*>::const_iterator end0,
		    const string* name0, int mask0)
    : _next (next0), here (begin0), end (end0), name (name0), mask (mask0)
{
    skip();
}

#undef member

				/* Class Environ */

#define member Environ::

member Environ (Environ* nestedIn)
    : decls(new vector<EnvironEntry*>()), next (nestedIn)
{}

member Environ (Environ *nestedIn, Environ *linkedTo)
    : decls(linkedTo->decls), next(nestedIn)
{}

void member copy(Environ *from)
{
  *decls = *from->decls;
}

Decl* member lookup (const string* name, int mask) const
{
    bool dummy;
    return lookup (name, dummy, mask);
}

Decl* member lookup (const string* name, bool& more, int mask) const
{
    Decl* d;
    const Environ* e;

    e = this;
    do {
	d = e->lookupProper (name, more, mask);
	e = e->next;
    } while (d == NULL && e != NULL);
	
    return d;
}

Decl* member lookupProper (const string* name, int mask) const
{
    bool dummy;
    return lookupProper (name, dummy, mask);
}

Decl* member lookupProper (const string* name, bool& more, int mask) const
{
    EnvironIter iter (NULL, decls->begin(), decls->end(), name, mask);

    if (iter.isDone())
	return NULL;
    else {
	Decl* result = &(*iter);
	iter.next();
	more = !iter.isDone();
	return result;
    }
}
    
EnvironIter member lookupFirst (const string* name, int mask) const
{
    EnvironIter result;
    result = EnvironIter (next, decls->begin(), decls->end(), name, mask);
    result._next = NULL;
    return result;
}

EnvironIter member lookupFirstProper (const string* name, int mask) const
{
    EnvironIter result;
    result = EnvironIter (NULL, decls->begin(), decls->end(), name, mask);
    result._next = NULL;
    return result;
}

EnvironIter member allDecls(int mask) const
{
    return EnvironIter (next, decls->begin(), decls->end(), NULL, mask);
}


EnvironIter member allProperDecls(int mask) const
{
    return EnvironIter (NULL, decls->begin(), decls->end(), NULL, mask);
}

void member add (Decl* decl)
{
    add(decl->name(), decl);
}

void member add (const string* name, Decl* decl)
{
    decls->push_back (new EnvironEntry(name, decl));

    if (DEBUG) {
	cout << "adding entry " << decls->size() - 1
	     << " to environment " << this
	     << ": " << decl->errorName() << endl;
    }
}


void member print (ostream& os) const
{
    printProper(os);
    if (parent())
	parent()->print(os);
}

void member printProper (ostream& os) const
{
    os << "[self: " << this << endl
       << " next: " << next << endl
       << ' ' << decls->size() << " entries: (";
    foriter (decl, allProperDecls(), EnvironIter) {
	os << endl << "  ";
	decl->print (os);
    }
    os << ")]" << endl;
}

#undef member

				/* Class EnvironEntry */

#define member EnvironEntry::

member EnvironEntry()
    : _name (NULL), _decl(NULL)
{ }

member EnvironEntry (const string *name0, Decl* decl0)
    : _name (name0), _decl (decl0)
{ }

member EnvironEntry (Decl* decl0)
    : _name (decl0->name()), _decl (decl0)
{ }

#undef member

void db_print_environ (Environ* env)
{
    env->print (cerr);
}

bool moreThanOne (EnvironIter env)
{
    if (env.isDone())
	return false;
    env.next();
    return ! env.isDone();
}

