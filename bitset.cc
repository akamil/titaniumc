#include <iostream>
#include "using-std.h"
#include "bitset.h"

int Bitset::intLogSize = 0;
int Bitset::intSize = 0;

void Bitset::un(Bitset *b) {
  assert(b->_size == _size);

  int *otherset = b->array;
  int *myset = array;
  int realSize = (_size >> intLogSize) + 1;
  int *term = &(b->array[realSize]);
  while (otherset != term) {
    *myset = *myset | *otherset;
    otherset++;
    myset++;
  }
}

void Bitset::subtract(Bitset *b) {
  assert(b->_size == _size);

  int *otherset = b->array;
  int *myset = array;
  int realSize = (_size >> intLogSize) + 1;
  int *term = &(b->array[realSize]);
  while (otherset != term) {
    *myset = *myset & ~(*otherset);
    otherset++;
    myset++;
  }
}

bool Bitset::equal(Bitset *b) {
  assert(b->_size == _size);

  int *otherset = b->array;
  int *myset = array;
  int realSize = (_size >> intLogSize) + 1;
  int *term = &(b->array[realSize]);
  while (otherset != term) {
    if (*otherset != *myset) {
      return false;
    }
    otherset++;
    myset++;
  }
  return true;
}

bool Bitset::disjoint(Bitset *b) {
  assert(b->_size == _size);

  int *otherset = b->array;
  int *myset = array;
  int realSize = (_size >> intLogSize) + 1;
  int *term = &(b->array[realSize]);
  while (otherset != term) {
    if (((*myset) & (*otherset)) != 0)
      return false;
    otherset++;
    myset++;
  }
  return true;
}


void Bitset::print(void) {
  for (int index = 0; index < _size; index++) {
    if (test(index)) {
      cout << "1";
    }
    else {
      cout << "0";
    }
  }
  cout << endl;
}
