/* -*- c++ -*- */
#ifndef _PRAGMA_H_
#define _PRAGMA_H_

/* pragma.h: Definitions related to pragmas */

treeSet *find_subtrees_not_with_pragma(Pragma::Request r, const TreeNode *t);
bool underPragma(Pragma::Request r, const TreeNode *t);

static inline PragmaNode *pragma_no_optimize(TreeNode *t)
{
  Pragma::Request r = (Pragma::Request)
    (Pragma::noOpt | Pragma::noDefs | Pragma::noUAE | Pragma::noCFG);
  return new PragmaNode(t, r, t->position());
}

#endif
