#ifndef _INCLUDE_TITANIUM_DELIMIT_H_
#define _INCLUDE_TITANIUM_DELIMIT_H_

#include <string>

template< class InputIterator > inline
const string delimit( InputIterator begin, InputIterator end, const char delimiter[] = ", " )
{
    string result;
    bool first = true;
    
    //while (std::operator!=(begin, end)) {
    while (!(begin == end)) {
	if (!first) result += delimiter;
	result += *begin++;
	first = false;
    }

    return result;
}


template< class Container > inline
const string delimit( const Container &items, const char delimiter[] = ", " )
{
    return delimit( items.begin(), items.end(), delimiter );
}


#endif
