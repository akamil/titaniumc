#include <iostream>
#include "WithDef.h"


WithDef::WithDef( ostream &out, const char macro[], const string &value )
  : out( out ),
    macro( macro )
{
  out << "#define " << macro << ' ' << value << '\n';
}


WithDef::~WithDef()
{
  out << "#undef " << macro << '\n';
}
