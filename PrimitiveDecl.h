#ifndef _INCLUDE_PRIMITIVEDECL_H_
#define _INCLUDE_PRIMITIVEDECL_H_

#include "TypeDecl.h"
#include "parse.h"


// This is used to hush a G++ warning.
class SuppressWarning;


class PrimitiveDecl : public TypeDecl {
public:
  Category category() const { return Primitive; }
  const char * thisClassName() const { return "PrimitiveDecl"; }
  string errorName();

  void collectEmbeddedLocals( list< BadSubfield > &, const Subfield * );
  bool containsEmbeddedLocals();

  void includeSelf( CfCode & );

  static PrimitiveDecl VoidDecl;
  static PrimitiveDecl NullDecl;
  static PrimitiveDecl BoolDecl;
  static PrimitiveDecl CharDecl;
  static PrimitiveDecl ByteDecl;
  static PrimitiveDecl ShortDecl;
  static PrimitiveDecl IntDecl;
  static PrimitiveDecl LongDecl;
  static PrimitiveDecl FloatDecl;
  static PrimitiveDecl DoubleDecl;

protected:
  void dumpAttributes( ostream & ) const;

private:
  PrimitiveDecl( TypeNode * );

  friend class SuppressWarning;
};


#endif
