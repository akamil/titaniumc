#include "AST.h"
#include "osstream.h"
//#include <strstream>


string TreeNode::mangle()
{
  undefined( "mangle" );
  return string();
}


////////////////////////////////////////////////////////////////////////


string NullPntrNode::mangle()
{
  static const string null( "null" );
  return string("v4vnull") + type()->mangle();
}


string PrimitiveLitNode::mangle()
{
  string asString = literal().asString();

  /* DOB: PR575: be sure we mangle floating-point values to legal and unique C identifiers */
  string::size_type pos;
  while ((pos=asString.find('.')) != asString.npos) 
    asString.replace(pos,1,string("_"));
  while ((pos=asString.find('+')) != asString.npos) 
    asString.replace(pos,1,string("P"));
  while ((pos=asString.find('-')) != asString.npos) 
    asString.replace(pos,1,string("M"));
  while ((pos=asString.find('e')) != asString.npos) 
    asString.replace(pos,1,string("E"));

  return string("v") + int2string(asString.length()) + 
         string("v") + asString + type()->mangle();
}


string StringLitNode::mangle()
{
  ostringstream formatter;
  const string16 value = text();
  formatter << 'v' << int2string(value.length()) <<
      'v' << value << type()->mangle();
  return formatter.str();
}


string TreeListNode::mangle()
{
  string result( int2string( arity() ) );
  
  foriter (child, allChildren(), ChildIter)
    result += (*child)->mangle();

  return result;
}


string TypeNode::mangle()
{
  return cType();
}


// Local Variables:
// c-file-style: "gnu"
// End:
