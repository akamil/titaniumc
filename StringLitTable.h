#ifndef _include_StringSet_h_
#define _include_StringSet_h_
#include <map>
#include "string16.h"

class CodeContext;
class CtLocal;

#if defined(__xlC__) || defined(__MTA__)
/* bug workaround for xlC STL bug */
#define _SLT_CONST_ 
#else
#define _SLT_CONST_ const 
#endif

class StringLitTable : public map< _SLT_CONST_ string16, unsigned, less< _SLT_CONST_ string16 > >
{
public:
  StringLitTable();
  size_t operator[]( const string16 & );
  
  void predeclare( ostream & ) const;
  void declare( ostream & ) const;
  void internAll( CodeContext & ) const;

  static const char name[];

private:
  static const char tableName[];
  static const CtLocal *stringType;
};


#endif // !_include_StringSet_h_
