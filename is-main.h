#ifndef _include_is_main_h_
#define _include_is_main_h_

#include "MethodSet.h"


extern MethodSet mainMethods;

void checkOnlyOneMain();


#endif // !_include_is_main_h_
