#include "AST.h"
#include "CodeContext.h"


void ReturnNode::codeGen( CodeContext &context )
{
  // cleanups should have been extracted by lowering phase
  assert( !cleanups() );
  
  const string value( expr()->absent()
		      ? string()
		      : expr()->emitExpression( context ) );
  
  context << "return " << value << ';' << endCline;
}
