#include "AST.h"
#include "CodeContext.h"


void ExpressionStmtNode::emitStatement( CodeContext &context )
{
  const string computation = expr()->emitExpression( context );
  
  if (!computation.empty())
    context << computation << ';' << endCline;
}
