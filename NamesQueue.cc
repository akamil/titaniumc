#include "AST.h"
#include "NameContext.h"
#include "NamesQueue.h"
#include "code-util.h"

NamesQueue unresolvedNames;


void NamesQueue::resolve()
{
  NamesQueue postponedUnits;
  
  while (!empty())
    {
      CompileUnitNode &unit = *front();
      pop_front();

      compile_status(2,string("name resolution: ") + *(unit.ident()));      
      bool postponed = false;
      TreeNode::NameContext context( postponed );
      unit.resolveName( context );
      
      if (postponed)
	postponedUnits.push_back( &unit );
    }
  
  splice( end(), postponedUnits );
}


// Local Variables:
// c-file-style: "gnu"
// End:
