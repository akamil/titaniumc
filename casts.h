#ifndef _INCLUDE_TITANIUM_CASTS_H_
#define _INCLUDE_TITANIUM_CASTS_H_


#include <string>

class ClassDecl;
class CodeContext;


const string getClassInfo( CodeContext &, ClassDecl &,
			   const string &, const bool );


#endif // !_INCLUDE_TITANIUM_CASTS_H_
