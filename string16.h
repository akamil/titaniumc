#ifndef _INCLUDE_TITANIUM_STRING16_H_
#define _INCLUDE_TITANIUM_STRING16_H_

#include <string>
#include "ti_config.h"
#include "literal.h"
#include "uint16-traits.h"
#include "using-std.h"

typedef basic_string< uint16 > string16;

// For human-readable presentations, use the "<<" operator instead.
// These representations may be fed into a C compiler, but are not as
// good for human viewing.

void printEscaped( ostream &, uint16 );
void printEscaped( ostream &, const string16 & );

ostream &operator<<( ostream &, const string16 & );

#endif
