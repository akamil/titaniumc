(defun insert-field (field)
  (insert "  sink << '\\n';   indent( sink, depth + 1 );   "
	  (format (if (assq field *children*)
		      "%s()->dump( sink, depth + 1 )"
		    "::dump( sink, depth + 1, %s() )")
		  (basic-field-name field))
	  ";\n"))

(defun defnode* (name parents fields methods states)
  (when (is-subclass-p name 'TypeNode)
    (push 'modifiers fields))
  (when fields
    (insert (format "void %s::dump( ostream &sink, unsigned depth ) const\n" name)
	    "{\n"
	    "  dumpPrefix( sink, depth );\n")
    (mapc 'insert-field fields)
    (insert "  dumpSuffix( sink, depth );\n"
	    "}\n"
	    ?\n)))
