#ifndef _TI_STRINGSET_H_
#define _TI_STRINGSET_H_

#include <algorithm>
#include <set>
#include <string>

typedef set< string, less<string> > stringset;
typedef stringset StringSet;

/* Non-destructive. */ 
static inline stringset stringset_union(const stringset x, const stringset y)
{
  stringset result;
  set_union(x.begin(), x.end(), y.begin(), y.end(),
            inserter(result, result.begin()),
            less<string>());
  return result;
}

/* Add all elements of y to x */
static inline void stringset_merge(stringset x, const stringset y)
{
  for (stringset::const_iterator i = y.begin(); i != y.end(); i++)
    x.insert(*i);
}

/* Non-destructive. */ 
static inline stringset stringset_intersection(const stringset x,
					       const stringset y)
{
  stringset result;
  set_intersection(x.begin(), x.end(), y.begin(), y.end(),
		   inserter(result, result.begin()),
		   less<string>());
  return result;
}

#endif // !_TI_STRINGSET_H_
