#include "AST.h"
#include "decls.h"


bool TypeNode::isAtomic() const
{
  // conservative default
  return false;
}

static bool isAtomic(TypeNode const *t) {
  ClassDecl *cd = dynamic_cast<ClassDecl*>(t->decl());
  switch (cd->kind()) {
  case Common::ClassKind:
  case Common::InterfaceKind:
    return false;
    
  case Common::ImmutableKind:
    // Immutables can be considered atomic if all of their constituent
    // fields are atomic.  Ooh, recursion!  

    foriter (field, cd->environ()->allProperDecls(), EnvironIter)
      if (field->category() == Decl::Field &&
	  !(field->modifiers() & Common::Static) &&
	  (field->container() == cd)) {
	if (field->hasType()) {
	  if (!field->type()->isAtomic()) {
	    return false;
	  }
	} else if (field->isType()) {
	  if (!field->asType()->isAtomic()) {
	    return false;
	  }
	}
      }

    return true;

  default:
    t->fatal( "unable to check atomicity of " + cd->errorName() );
    return false;
  }
}

bool TypeNameNode::isAtomic() const
{ return ::isAtomic(this); }

bool TemplateInstanceTypeNode::isAtomic() const
{ return ::isAtomic(this); }

bool PrimitiveTypeNode::isAtomic() const
{
  return true;
}


bool PointTypeNode::isAtomic() const
{
  return true;
}


bool RectDomainTypeNode::isAtomic() const
{
  return true;
}
