#ifndef _INCLUDE_STRING_ERASE_H_
#define _INCLUDE_STRING_ERASE_H_

#include <string>
#include "ti_config.h"


// We need a portable way to delete one character from the middle of a
// string.  Older G++ libraries may have basic_string::remove(), while
// ANSI standard libraries have basic_string::erase().

inline string &stringErase( string &target,
			    string::size_type position,
			    string::size_type count = string::npos )
{
#ifdef HAVE_STRING_ERASE
  return target.erase(  position, count );
#else
  return target.remove( position, count );
#endif
}


#endif
