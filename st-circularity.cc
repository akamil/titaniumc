#include "AST.h"
#include "compiler.h"


bool TreeNode::verifyCircularity() const
{
  undefined( "verifyCircularity" );
  return false;
}


bool CompileUnitNode::verifyCircularity() const
{
  bool change = false;

  foriter (type, types()->allChildren(), ChildIter)
    change |= (*type)->verifyCircularity();

  return change;
}


bool TypeDeclNode::verifyCircularity() const
{
  bool change = false;

  // Type is valid if all parents are valid
  ClassDecl &d = *decl();
  
  if (!d.valid())
    if (!d.hasSuperClass() || d.superClass()->valid())
      {
	bool interfacesok = true;

	foreach (extends, llist<Decl *>, *d.interfaces())
	  if (!(*extends)->valid())
	    {
	      interfacesok = false;
	      break;
	    }
	if (interfacesok)
	  {
	    change = true;
	    d.valid(true);
	  }
      }
  
  return change;
}


bool TemplateDeclNode::verifyCircularity() const
{
  return false;
}


////////////////////////////////////////////////////////////////////////


void TreeNode::reportCircularity() const
{
  undefined( "reportCircularity" );
}


void CompileUnitNode::reportCircularity() const
{
  foriter (type, types()->allChildren(), ChildIter)
    (*type)->reportCircularity();
}


void TemplateDeclNode::reportCircularity() const
{
}


void TypeDeclNode::reportCircularity() const
{
  ClassDecl &d = *decl();
  
  if (!d.valid())
    error() << d.errorName() << " in circular definition" << endl;
}


////////////////////////////////////////////////////////////////////////


void verifyCircularity()
{
  // Verify that class and interface defs are not circular
  ObjectDecl->valid( true );

  bool change;
  do 
    {
      change = false;

      foreach (file, llist<CompileUnitNode *>, *allFiles)
	change |= (*file)->verifyCircularity();
    }
  while (change);

  // Report all invalid types
  foreach (file, llist<CompileUnitNode *>, *allFiles)
    (*file)->reportCircularity();
}
