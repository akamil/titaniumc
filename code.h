#ifndef _INCLUDE_TITANIUM_CODE_H_
#define _INCLUDE_TITANIUM_CODE_H_

#define __SINST__
#include <string>
#include "using-std.h"

class Decl;
class FieldDecl;
class MethodDecl;
class TreeNode;
class CodeContext;

enum AssignKind { assignNormal, assignLocal, assignGlobal };
enum AssignType { assignNothing, assignUnknown, assignBoolean, assignByte, 
		  assignChar, assignDouble,
		  assignFloat, assignInt, assignLong,
		  assignShort, assignLP, assignGP, assignBulk };

extern string cFieldName(Decl *);
extern string cMethodTypeString(const MethodDecl &);

extern bool codeGen_libs;
extern bool codeGen_main;
extern string codeGen_outdir;

extern bool compilerLocComments;
#define compilerLoc()							\
  (compilerLocComments ?						\
       string("/*") + __FILE__ + ':' + int2string(__LINE__) + "*/" :	\
       string(""))

// located in aux-code.cc
extern const string locComment(TreeNode *t);

// located in code-stmt.cc
extern CodeContext * enclosingDeclContainer(TreeNode *t);

// located in code-titan.cc
extern const string int2string(int);
extern const string long2hexstring(long);

// defines to help to set points
#define SET_POINT_INDEX_START(sep, arity) \
	string("m3setIIm") sep MANGLE_POINT_TYPE(sep, arity) sep "("
#define SET_POINT_INDEX_END(sep, index, value) \
	(index) sep ',' sep value sep ')'

#define NEW_POINT_1I_START(sep, arity) \
	string("m3allIm") sep MANGLE_POINT_TYPE(sep, arity) sep "("
#define NEW_POINT_1I_END() \
	')'

// defines to help create domains

#define MANGLE_POINT_TYPE(sep, arity) \
        string("T") sep int2string(string(arity).length() + 7) sep "tiPoint" sep (arity) sep "7domains2ti"
#define MANGLE_TI_DOMAINS_POINT_TYPE      MANGLE_POINT_TYPE
#define MANGLE_TI_DOMAINS_POINT_ARG MANGLE_TI_DOMAINS_POINT_TYPE

#define MANGLE_RECTDOMAIN_TYPE(sep, arity) \
        string("T") sep int2string(string(arity).length() + 12) sep "tiRectDomain" sep (arity) sep "7domains2ti"
#define MANGLE_RECTDOMAIN_CONSTRUCTOR(sep, arity) \
        string("m") sep int2string(string(arity).length() + 12) sep "tiRectDomain" sep (arity)
#define MANGLE_TI_DOMAINS_RECTDOMAIN_TYPE MANGLE_RECTDOMAIN_TYPE
#define MANGLE_TI_DOMAINS_RECTDOMAIN_ARG MANGLE_TI_DOMAINS_RECTDOMAIN_TYPE

#define MANGLE_DOMAIN_TYPE_UNBOXED(sep, arity) \
        string("T") sep int2string(string(arity).length() + 8) sep "tiDomain" sep (arity) sep "7domains2ti"
#define MANGLE_DOMAIN_TYPE(sep, arity) \
        string("P") sep MANGLE_DOMAIN_TYPE_UNBOXED(sep, arity)
#define MANGLE_TI_DOMAINS_DOMAIN_TYPE     MANGLE_DOMAIN_TYPE
#define MANGLE_MRAD_TYPE_UNBOXED(sep, arity) \
        string("T") sep int2string(string(arity).length() + 18) sep "tiMultiRectADomain" sep (arity) sep "7domains2ti"
#define MANGLE_MRAD_CONSTRUCTOR(sep, arity) \
        string("m") sep int2string(string(arity).length() + 18) sep "tiMultiRectADomain" sep (arity)
#define MANGLE_MRAD_TYPE(sep, arity) \
        string("P") sep MANGLE_MRAD_TYPE_UNBOXED(sep, arity)
#define MANGLE_TI_DOMAINS_MRAD_TYPE     MANGLE_MRAD_TYPE

#define POINT_PLUS_ALL_ONE(sep, arity, arg) \
        string("o2PL") sep MANGLE_POINT_TYPE(sep, arity) sep "m" sep MANGLE_POINT_TYPE(sep, arity) \
        sep "(" sep (arg) sep ", " sep MANGLE_TI_DOMAINS_POINT_UNIT(sep, arity) sep "())"

#define NEW_RECTDOMAIN_3PT_MAX(sep, arity, minpt, maxpt, stridept) \
        MANGLE_RECTDOMAIN_CONSTRUCTOR(sep, arity) \
        sep MANGLE_POINT_TYPE(sep, arity) sep MANGLE_POINT_TYPE(sep, arity) sep MANGLE_POINT_TYPE(sep, arity) \
        sep "c" sep MANGLE_RECTDOMAIN_TYPE(sep, arity) \
        sep "(" sep minpt sep ", (" sep POINT_PLUS_ALL_ONE(sep,arity,maxpt) sep "), " sep stridept sep ")"

#define NEW_RECTDOMAIN_2PT_MAX(sep, arity, minpt, maxpt) \
        MANGLE_RECTDOMAIN_CONSTRUCTOR(sep, arity) \
        sep MANGLE_POINT_TYPE(sep, arity) sep MANGLE_POINT_TYPE(sep, arity) \
        sep "c" sep MANGLE_RECTDOMAIN_TYPE(sep, arity) \
        sep "(" sep minpt sep ", (" sep POINT_PLUS_ALL_ONE(sep,arity,maxpt) sep "))"

#define NEW_RECTDOMAIN_3PT_UPB(sep, arity, minpt, upbpt, stridept) \
        MANGLE_RECTDOMAIN_CONSTRUCTOR(sep, arity) \
        sep MANGLE_POINT_TYPE(sep, arity) sep MANGLE_POINT_TYPE(sep, arity) sep MANGLE_POINT_TYPE(sep, arity) \
        sep "c" sep MANGLE_RECTDOMAIN_TYPE(sep, arity) \
        sep "(" sep minpt sep ", " sep upbpt sep ", " sep stridept sep ")"

#define NEW_RECTDOMAIN_2PT_UPB(sep, arity, minpt, upbpt) \
        MANGLE_RECTDOMAIN_CONSTRUCTOR(sep, arity) \
        sep MANGLE_POINT_TYPE(sep, arity) sep MANGLE_POINT_TYPE(sep, arity) \
        sep "c" sep MANGLE_RECTDOMAIN_TYPE(sep, arity) \
        sep "(" sep minpt sep ", " sep upbpt sep ")"

#define NEW_RECTDOMAIN_2PT_NONEMPTY_UPB(sep, arity, minpt, upbpt) \
        MANGLE_RECTDOMAIN_CONSTRUCTOR(sep, arity) \
        sep MANGLE_POINT_TYPE(sep, arity) sep MANGLE_POINT_TYPE(sep, arity) \
        sep MANGLE_BOOLEAN_ARG(sep) \
        sep "c" sep MANGLE_RECTDOMAIN_TYPE(sep, arity) \
        sep "(" sep minpt sep ", " sep upbpt sep ", (jboolean)0)"

#define NEW_RECTDOMAIN_DOMAIN_START(sep, arity) \
	MANGLE_RECTDOMAIN_CONSTRUCTOR(sep, arity) \
        sep MANGLE_DOMAIN_TYPE(sep, arity) sep "c" sep MANGLE_RECTDOMAIN_TYPE(sep, arity) sep "("
#define NEW_RECTDOMAIN_DOMAIN_END() \
	')'

#define NEW_RECTDOMAIN_EMPTY(sep, arity) \
        string("mi5emptym") sep MANGLE_RECTDOMAIN_TYPE(sep,arity) sep "()"

// defines for mangling

#define MANGLE_QID(sep, s) string(int2string(string(s).length())) sep (s)
#define MANGLE_METH_HEAD(sep, namespace, s) string("m") sep namespace sep MANGLE_QID(sep, s)
#define MANGLE_OP_HEAD(sep, s) string("o") sep MANGLE_QID(sep, s)
#define MANGLE_FIELD(sep, s, typestr) string("f") sep MANGLE_QID(sep, s) sep typestr
#define MANGLE_STACK_VAR(sep, s) string("a") sep MANGLE_QID(sep, s)
#define MANGLE_TEMP(sep, s) string("t") sep MANGLE_QID(sep, s)
#define MANGLE_THIS_VAR(sep) string("a4this") /* MANGLE_STACK_VAR(sep, "this") */
#define MANGLE_CLASS_INIT(sep, typestr) string("mi5initcm") sep typestr
//#define MANGLE_CLASS_INST(sep, typestr) string("mi5initim") sep typestr // DOB: no longer used
#define MANGLE_CLASS_DESC(sep, typestr) string("fi5cdesc") sep typestr
#define MANGLE_CLASS_DTYP(sep, typestr) string("ti5cdescm") sep typestr
#define MANGLE_CLASS_INTF(sep, typestr) string("fi4intf") sep typestr
#define MANGLE_CLASS_EQUALS(sep, typestr) string("mi6equalsm") sep typestr
#define MANGLE_CLASS_NEQUALS(sep, typestr) string("mi7nequalsm") sep typestr
#define MANGLE_CLASS_NULLLOC(sep, typestr) string("mi7nulllocm") sep typestr
#define MANGLE_CLASS_BUILDST(sep, typestr) string("mi7buildstm") sep typestr
#define MANGLE_CLASS_CHECKPOINT(sep, typestr) string("mi10checkpointm") sep typestr
#define MANGLE_CLASS_RESTORE(sep, typestr) string("mi7restorem") sep typestr
#define MANGLE_CLASS_ARRAY_CHECKPOINT(sep, typestr) string("mi16array_checkpointm") sep typestr
#define MANGLE_CLASS_ARRAY_RESTORE(sep, typestr) string("mi13array_restorem") sep typestr
#define MANGLE_CLASS_FINALIZER(sep, typestr) string("mi8finalize") sep typestr

#define MANGLE_INNER_CLASS_MARKER "C"
#define MANGLE_METHOD_NAME_END_MARKER "m"
#define MANGLE_CONSTRUCTOR_NAME_END_MARKER "c"
#define MANGLE_TYPE_MARKER "T"
#define MANGLE_TYPE_LOCAL_MARKER "L"
#define MANGLE_TYPE_ILOCAL_MARKER "L"
#define MANGLE_TYPE_GLOBAL_MARKER "P"
#define MANGLE_TYPE_SHARED_MARKER ""
#define MANGLE_TYPE_NONSHARED_MARKER "n"
#define MANGLE_TYPE_POLYSHARED_MARKER "p"
#define MANGLE_JAVA_ARRAY_TYPE_MARKER "A"
#define MANGLE_TI_ARRAY_TYPE_MARKER "B"
#define MANGLE_TI_POINT_TYPE_MARKER "P"
#define MANGLE_TI_RECTDOMAIN_TYPE_MARKER "R"
#define MANGLE_TI_DOMAIN_TYPE_MARKER "G"
#define MANGLE_LOCAL_NAMESPACE_MARKER "l"
#define MANGLE_LQI_NAMESPACE_MARKER "m"
#define MANGLE_LQI_FORMAL_MARKER "M"
#define MANGLE_NONSHARED_NAMESPACE_MARKER "n"
#define MANGLE_POLYSHARED_NAMESPACE_MARKER "p"
#define MANGLE_SHARING_INFERENCE_FORMAL_MARKER "s"
#define MANGLE_GENERIC_MARKER "G"

#define MANGLE_TI_LANG_THROWCLASSCASTEXCEPTION "m23throwClassCastExceptionmT11NativeUtils4lang2ti()" /* ti.lang.NativeUtils.throwClassCastException() */


#define MANGLE_TI_DOMAINS_RECTDOMAIN_RAWDISPATCH(sep, arity, methodprefix, args) \
        string("") sep methodprefix sep args sep "m" sep MANGLE_RECTDOMAIN_TYPE(sep, arity) 

#define MANGLE_TI_DOMAINS_RECTDOMAIN_DISPATCH(sep, arity, fname, args) \
        MANGLE_TI_DOMAINS_RECTDOMAIN_RAWDISPATCH(sep, arity, string("m") sep int2string(string(fname).length()) sep fname, args)

#define MANGLE_TI_DOMAINS_DOMAIN_RAWDISPATCH(sep, arity, methodprefix, args) \
        string("") sep methodprefix sep args sep "m" sep MANGLE_MRAD_TYPE_UNBOXED(sep, arity)

#define MANGLE_TI_DOMAINS_DOMAIN_DISPATCH(sep, arity, fname, args) \
        MANGLE_TI_DOMAINS_DOMAIN_RAWDISPATCH(sep, arity, string("m") sep int2string(string(fname).length()) sep fname, args)

#define MANGLE_TI_DOMAINS_DOMAIN_CONSTRUCTORDISPATCH(sep, arity, methodprefix, args) \
        string("") sep methodprefix sep args sep "c" sep MANGLE_MRAD_TYPE_UNBOXED(sep, arity) 

#define MANGLE_TI_DOMAINS_DOMAIN_STATICDISPATCH(sep, arity, fname, args) \
        string("m") sep int2string(string(fname).length()) sep fname sep args sep "m" \
        sep MANGLE_DOMAIN_TYPE_UNBOXED(sep, arity) 

#define MANGLE_TI_DOMAINS_MRAD_STATICDISPATCH(sep, arity, fname, args) \
        string("m") sep int2string(string(fname).length()) sep fname sep args sep "m" \
        sep MANGLE_MRAD_TYPE_UNBOXED(sep, arity) 

#define NEW_DOMAIN_EMPTY(sep, arity) \
        MANGLE_TI_DOMAINS_MRAD_STATICDISPATCH(sep, arity, "emptyDomain", "") sep "()"

#define NEW_DOMAIN_CAST_RECTDOMAIN(sep, arity) \
        MANGLE_TI_DOMAINS_MRAD_STATICDISPATCH(sep, arity, "toDomain", MANGLE_RECTDOMAIN_TYPE(sep, arity)) 

#define MANGLE_INT_ARG(sep) "I"
#define MANGLE_BOOLEAN_ARG(sep) "Z"

#define MANGLE_TI_DOMAINS_POINT_RAWDISPATCH(sep, arity, methodprefix, args) \
        string("") sep methodprefix sep args \
        sep "m" sep MANGLE_TI_DOMAINS_POINT_ARG(sep, arity)

#define MANGLE_TI_DOMAINS_POINT_DISPATCH(sep, arity, fname, args) \
        MANGLE_TI_DOMAINS_POINT_RAWDISPATCH(sep, arity, string("m") sep int2string(string(fname).length()) sep fname, args)

#define MANGLE_TI_DOMAINS_RECTDOMAIN_FIELD_ACCESS(sep, field, arity) \
        MANGLE_FIELD(sep, field, MANGLE_RECTDOMAIN_TYPE(sep, arity))
#define MANGLE_TI_DOMAINS_MRAD_FIELD_ACCESS(sep, field, arity) \
        MANGLE_FIELD(sep, field, MANGLE_MRAD_TYPE_UNBOXED(sep, arity))

#define MANGLE_TI_DOMAINS_POINT_GET(sep, arity) \
        string("m3getIm") sep MANGLE_POINT_TYPE(sep,arity) sep "("
#define MANGLE_TI_DOMAINS_POINT_CONSTRUCT(sep, arity) \
        string("mi9constructm") sep MANGLE_POINT_TYPE(sep,arity) sep "("
#define MANGLE_TI_DOMAINS_POINT_EMPTY(sep, arity) \
        string("mi5emptym") sep MANGLE_POINT_TYPE(sep,arity)
#define MANGLE_TI_DOMAINS_POINT_UNIT(sep, arity) \
        string("mi4unitm") sep MANGLE_POINT_TYPE(sep,arity)
#define MANGLE_TI_DOMAINS_POINT_NEGUNIT(sep, arity) \
        string("mi7negunitm") sep MANGLE_POINT_TYPE(sep,arity)

#endif
