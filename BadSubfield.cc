#include "BadSubfield.h"
#include "Subfield.h"


BadSubfield::BadSubfield( const Subfield &subfield )
  : source( subfield.source() ),
    name( subfield.name() )
{
}
