#ifndef INCLUDE_INSTANCEPOOL_H
#define INCLUDE_INSTANCEPOOL_H

#include <cstddef>
#include <list>
#include "BufferPool.h"
#include "using-std.h"


/***********************************************************************
 *
 *  InstancePool extends BufferPool with support for calling
 *  destructors when buffers are no longer in use.  See BufferPool.h
 *  for documentation on behavior common to both classes.
 *
 *  When an instance pool is reset, the destructor will be called once
 *  for each active buffer that is being placed back on the free list.
 *  Note, however, that destructors are not called when a single
 *  buffer is released using InstancePool::release().  That method
 *  should only be called from overloaded delete operators; the C++
 *  compiler will ensure that the destructor is called immediately
 *  thereafter.
 *
 *  Resetting an instance pool requires time linear in the number of
 *  active buffers.  Contrast with resetting a buffer pool, which
 *  requires only constant time.
 *
 *  An instance pool is reset before being destroyed.  Thus,
 *  destructors will be called for all buffers that are active at the
 *  time an instance pool is destroyed.
 *
 **********************************************************************/


template< class T > class InstancePool : public BufferPool< sizeof( T ) > {
protected:
  typedef BufferPool< sizeof( T ) > super;
public:
  void reset();
  ~InstancePool();
};


template< class T > void InstancePool< T >::reset()
{
  for (typename super::List::const_iterator chaff = super::used.begin(); 
       chaff != super::used.end(); ++chaff)
    reinterpret_cast< T * >( *chaff )->~T();
  
  BufferPool< sizeof( T ) >::reset();
}


template< class T > InstancePool< T >::~InstancePool()
{
  reset();
}


#endif // !INCLUDE_INSTANCEPOOL_H
