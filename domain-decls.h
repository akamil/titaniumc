#ifndef _TITANIUM_DOMAIN_DECLS_H_
#define _TITANIUM_DOMAIN_DECLS_H_


class ClassDecl;

// !!! HACK miyamoto: to always instantiate domains

extern ClassDecl* PointNDecl[];
extern ClassDecl* DomainNDecl[];
extern ClassDecl* MultiRectADomainNDecl[];
extern ClassDecl* RectDomainNDecl[];


#endif // !_TITANIUM_DOMAIN_DECLS_H_
