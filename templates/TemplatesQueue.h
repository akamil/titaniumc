#ifndef INCLUDE_TEMPLATES_QUEUE_H
#define INCLUDE_TEMPLATES_QUEUE_H

#include <list>

class CompileUnitNode;


struct TemplatesQueue : public list< CompileUnitNode * >
{
  bool resolve( bool );
};


extern TemplatesQueue unresolvedTemplates;


#endif // INCLUDE_TEMPLATES_QUEUE_H


// Local Variables:
// c-file-style: "gnu"
// End:
