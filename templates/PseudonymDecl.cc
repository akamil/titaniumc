#include "../AST.h"
#include "PseudonymDecl.h"


PseudonymDecl::PseudonymDecl( const string *name, TreeNode &referent )
  : Decl( name ),
    referent( referent )
{
}


string PseudonymDecl::errorName()
{
  return "pseudonym " + *name();
}


void PseudonymDecl::dumpAttributes( ostream &sink ) const
{
  sink << ' ' << &referent;
}


////////////////////////////////////////////////////////////////////////


PseudonymDecl *TreeNode::bindPseudonym( TreeNode &referent ) const
{
  undefined( "bindPseudonym" );
  return 0;
}


// Local Variables:
// c-file-style: "gnu"
// End:
