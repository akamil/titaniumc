#ifndef INCLUDE_TEMPLATE_CHECK_CONTEXT_H
#define INCLUDE_TEMPLATE_CHECK_CONTEXT_H


struct TemplateCheckContext
{
  TemplateCheckContext( bool );

  const bool force;
  bool valid;
  bool ready;
};


inline TemplateCheckContext::TemplateCheckContext( bool force )
  : force( force ),
    valid( true ),
    ready( true )
{
}


#endif // !INCLUDE_TEMPLATE_CHECK_CONTEXT_H


// Local Variables:
// c-file-style: "gnu"
// End:
