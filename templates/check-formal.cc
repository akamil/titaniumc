#include "../AST.h"
#include "../NameContext.h"
#include "TemplatesQueue.h"

// The logic for checking formals could be extended to report the
// locations of all uses of a multiply-used formal.  But that seems
// like overkill given the small number of formals by which we expect
// any individual template to be parameterized.


void TreeNode::checkTemplateFormal( IdentSet &,
				    IdentSet & ) const
{
  undefined( "checkTemplateFormal" );
}


void TemplateConstParamNode::checkTemplateFormal( IdentSet &values,
						  IdentSet & ) const
{
  const string * const id = simpName()->ident();
  if (!dtype()->isPrimitive())
    error() << "template formal parameter \""
	    << *id << "\" should be preceded by \"class,\" \"char,\""
      " \"byte,\" \"short,\" \"int,\" \"long,\""
      " \"float,\" \"double,\" or \"boolean\"" << endl;
  else if (!values.insert( id ).second)
    error() << "template formal value parameter \""
	    << *id << "\" appears multiple times" << endl;
}


void TemplateTypeParamNode::checkTemplateFormal( IdentSet &,
						 IdentSet &types ) const
{
  const string * const id = simpName()->ident();
  if (!types.insert( id ).second)
    error() << "template formal type parameter \""
	    << *id << "\" appears multiple times" << endl;
}


// Local Variables:
// c-file-style: "gnu"
// End:
