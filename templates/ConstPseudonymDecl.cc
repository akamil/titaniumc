#include "../AST.h"
#include "ConstPseudonymDecl.h"


ConstPseudonymDecl::ConstPseudonymDecl( const string *name, LitNode &referent )
  : PseudonymDecl( name, referent )
{
}


string ConstPseudonymDecl::errorName()
{
  return "constant " + PseudonymDecl::errorName();
}


const char *ConstPseudonymDecl::thisClassName() const
{
  return "ConstPseudonymDecl";
}


ConstPseudonymDecl::Category ConstPseudonymDecl::category() const
{
  return ConstPseudonym;
}


////////////////////////////////////////////////////////////////////////


PseudonymDecl *TemplateConstParamNode::bindPseudonym( TreeNode &referent ) const
{
  assert( referent.isLitNode() );
  return new ConstPseudonymDecl( simpName()->ident(), static_cast< LitNode & >( referent ) );
}


// Local Variables:
// c-file-style: "gnu"
// End:
