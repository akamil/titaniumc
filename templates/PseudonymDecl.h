#ifndef INCLUDE_TEMPLATES_PSEUDONYM_DECL_H
#define INCLUDE_TEMPLATES_PSEUDONYM_DECL_H

#include "../decls.h"


class PseudonymDecl : public Decl {
public:
  PseudonymDecl( const string *, TreeNode &referent );
  string errorName();
  
  TreeNode &referent;
  
protected:
  void dumpAttributes( ostream & ) const;
};


#endif // !INCLUDE_TEMPLATES_PSEUDONYM_DECL_H


// Local Variables:
// c-file-style: "gnu"
// End:
