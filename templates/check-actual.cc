#include <algorithm>
#include "../AST.h"
#include "TemplateCheckContext.h"


void TreeNode::checkTemplateActual( TemplateCheckContext &,
				    TreeNode & ) const
{
  undefined( "checkTemplateActual" );
}


void TemplateTypeParamNode::checkTemplateActual( TemplateCheckContext &context,
						 TreeNode &actual ) const
{
  assert( actual.isTypeNode() );
  
  if (!actual.decl())
    {
      postpone( "template instantiation", "unresolved type argument" );
      context.ready = false;
    }
}


void TemplateConstParamNode::checkTemplateActual( TemplateCheckContext &context,
						  TreeNode &actual ) const
{
  assert( actual.isExprNode() );
  static_cast< ExprNode & >( actual ).checkTemplateActual( context, *simpName()->ident(), *dtype() );
}


////////////////////////////////////////////////////////////////////////


void ExprNode::checkTemplateActual( TemplateCheckContext &context,
				    const string &name,
				    TypeNode &dtype )
{
  postpone( "template instantiation", "unfolded constant argument" );
  context.ready = false;
  
  if (context.force)
    error() << "template parameter " << name
	    << " is not a constant expression of type "
	    << dtype.typeName() 
	    << " or is in a circularity" << endl;
}


void LitNode::checkTemplateActual( TemplateCheckContext &,
				   const string &,
				   TypeNode & )
{
}


// Local Variables:
// c-file-style: "gnu"
// End:
