#include "../AST.h"
#include "TypePseudonymDecl.h"


TypePseudonymDecl::TypePseudonymDecl( const string *name, TypeNode &referent )
  : PseudonymDecl( name, referent )
{
}


string TypePseudonymDecl::errorName()
{
  return "type " + PseudonymDecl::errorName();
}


const char *TypePseudonymDecl::thisClassName() const
{
  return "TypePseudonymDecl";
}


TypePseudonymDecl::Category TypePseudonymDecl::category() const
{
  return TypePseudonym;
}


////////////////////////////////////////////////////////////////////////


PseudonymDecl *TemplateTypeParamNode::bindPseudonym( TreeNode &referent ) const
{
  assert( referent.isTypeNode() );
  return new TypePseudonymDecl( simpName()->ident(), static_cast< TypeNode & >( referent ) );
}


// Local Variables:
// c-file-style: "gnu"
// End:
