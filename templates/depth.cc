#include <algorithm>
#include "../AST.h"
#include "../tclimits.h"


int maxTemplateDepth = 17;


int TreeNode::templateDepth() const
{
  undefined( "templateDepth" );
  return 0;
}


int TypeNode::templateDepth() const
{
  return 0;
}


int ArrayTypeNode::templateDepth() const
{
  return elementType()->templateDepth();
}


int TypeNameNode::templateDepth() const
{
  ClassDecl &classDecl = static_cast< ClassDecl & >( *decl() );
  assert( classDecl.category() & (Decl::Class | Decl::Interface) );
  return decl()->templateDepth;
}


int TemplateInstanceTypeNode::templateDepth() const
{
  return decl()->templateDepth;
}


int LitNode::templateDepth() const
{
  return 0;
}


// Local Variables:
// c-file-style: "gnu"
// End:
