#ifndef INCLUDE_TEMPLATE_CONTEXT_H
#define INCLUDE_TEMPLATE_CONTEXT_H


class TemplatesQueue;


struct TemplateContext
{
  TemplateContext( bool &, bool * );

  bool &progress;
  bool *postponed;
};


inline TemplateContext::TemplateContext( bool &progress, bool *postponed )
  : progress( progress ),
    postponed( postponed )
{
}


#endif // !INCLUDE_TEMPLATE_CONTEXT_H


// Local Variables:
// c-file-style: "gnu"
// End:
