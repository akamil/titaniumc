#include "../AST.h"
#include "TemplateContext.h"
#include "TemplatesQueue.h"
#include "../code-util.h"

TemplatesQueue unresolvedTemplates;


bool TemplatesQueue::resolve( bool force )
{
  TemplatesQueue postponedUnits;
  bool progress = false;
  
  while (!empty())
    {
      CompileUnitNode &unit = *front();
      pop_front();

      compile_status(2,string("template resolution: ") + *(unit.ident()));  

      bool postponed = false;
      TemplateContext context( progress, force ? 0 : &postponed );
      unit.resolveTemplate( context );

      if (postponed)
	postponedUnits.push_back( &unit );
    }

  splice( end(), postponedUnits );
  return progress;
}


// Local Variables:
// c-file-style: "gnu"
// End:
