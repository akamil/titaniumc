#ifndef INCLUDE_TEMPLATES_TYPE_PSEUDONYM_DECL_H
#define INCLUDE_TEMPLATES_TYPE_PSEUDONYM_DECL_H

#include "PseudonymDecl.h"


class TypePseudonymDecl : public PseudonymDecl {
public:
  TypePseudonymDecl( const string *, TypeNode & );
  string errorName();
  const char* thisClassName() const;
  Category category() const;
};


#endif // !INCLUDE_TEMPLATES_TYPE_PSEUDONYM_DECL_H


// Local Variables:
// c-file-style: "gnu"
// End:
