#include "../AST.h"


TreeNode *TreeNode::fixTemplateActual( TreeNode & ) const
{
  undefined( "fixTemplateActual" );
  return 0;
}


TreeNode *TemplateTypeParamNode::fixTemplateActual( TreeNode &actual ) const
{
  return actual.fixTemplateActual( *simpName()->ident() );
}


TreeNode *TemplateConstParamNode::fixTemplateActual( TreeNode &actual ) const
{
  return actual.fixTemplateActual( *simpName()->ident(), *dtype() );
}


////////////////////////////////////////////////////////////////////////


TypeNode *TreeNode::fixTemplateActual( const string &name )
{
  error() << "template parameter " << name << " must be a type" << endl;
  return 0;
}


TypeNode *ArrayNameNode::fixTemplateActual( const string & )
{
  return asType();
}


TypeNode *TypeNode::fixTemplateActual( const string & )
{
  return this;
}


////////////////////////////////////////////////////////////////////////


ExprNode *TreeNode::fixTemplateActual( const string &name,
				       TypeNode &dtype )
{
  error() << "template parameter " << name
	  << " must be a constant expression of type "
	  << dtype.typeName() << endl;
  
  return 0;
}


ExprNode *ArrayNameNode::fixTemplateActual( const string &name,
					    TypeNode &dtype )
{
  return asExpr()->fixTemplateActual( name, dtype );
}


ExprNode *ExprNode::fixTemplateActual( const string &name,
				       TypeNode &dtype )
{
  return new CastNode( this, &dtype, position() );
}


// Local Variables:
// c-file-style: "gnu"
// End:
