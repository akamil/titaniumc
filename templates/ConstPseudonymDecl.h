#ifndef INCLUDE_TEMPLATES_CONST_PSEUDONYM_DECL_H
#define INCLUDE_TEMPLATES_CONST_PSEUDONYM_DECL_H

#include "PseudonymDecl.h"

class LitNode;


class ConstPseudonymDecl : public PseudonymDecl {
public:
  ConstPseudonymDecl( const string *, LitNode & );
  string errorName();
  const char* thisClassName() const;
  Category category() const;
};


#endif // !INCLUDE_TEMPLATES_CONST_PSEUDONYM_DECL_H


// Local Variables:
// c-file-style: "gnu"
// End:
