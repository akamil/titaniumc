#ifndef INCLUDE_INSTANTIAIONS_H
#define INCLUDE_INSTANTIAIONS_H

#include <list>
#include "../using-std.h"

class ClassDecl;
class TreeListNode;


class Instantiations : private list< pair< const TreeListNode *, ClassDecl * > >
{
public:
  ClassDecl *&operator [] ( const TreeListNode & );

private:
  ClassDecl **find( const TreeListNode & );
};


#endif // !INCLUDE_INSTANTIAIONS_H


// Local Variables:
// c-file-style: "gnu"
// End:
