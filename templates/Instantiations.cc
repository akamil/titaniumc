#include <algorithm>
#include "../AST.h"
#include "Instantiations.h"


ClassDecl **Instantiations::find( const TreeListNode &key )
{
  for (iterator seek = begin(); seek != end(); ++seek)
    {
      const TreeListNode &candidate = *seek->first;
      assert( key.arity() == candidate.arity() );

      bool matched = true;
      for (int scan = 0; matched && scan < key.arity(); ++scan)
	if (!key.child( scan )->typeIdent( candidate.child( scan ) ))
	  matched = false;

      if (matched)
	return &seek->second;
    }
  
  return 0;
}


ClassDecl *&Instantiations::operator [] ( const TreeListNode &key )
{
  ClassDecl **found = find( key );

  if (found)
    return *found;
  else
    {
      push_front( value_type( &key, 0 ) );
      return begin()->second;
    }
}


// Local Variables:
// c-file-style: "gnu"
// End:
