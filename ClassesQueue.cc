#include "AST.h"
#include "ClassesQueue.h"
#include "ClassContext.h"
#include "code-util.h"

ClassesQueue unresolvedClasses;


void ClassesQueue::resolve()
{
  ClassesQueue postponedUnits;
  ClassesQueue inheritUnits;
  
  while (!empty())
    {
      CompileUnitNode &unit = *front();
      pop_front();

      compile_status(2,string("class resolution: ") + *(unit.ident()));      
      bool postponed = false;
      TreeNode::ClassContext context( postponed );
      unit.resolveClass( &context );
      
      if (postponed)
	postponedUnits.push_back( &unit );
      inheritUnits.push_back( &unit );
    }

  splice( end(), inheritUnits );
  while (!empty())
    {
      CompileUnitNode &unit = *front();
      pop_front();

      compile_status(2,string("inheritance resolution: ") + *(unit.ident()));      
      unit.resolveInheritance( );
    }

  
  splice( end(), postponedUnits );
}


// Local Variables:
// c-file-style: "gnu"
// End:
