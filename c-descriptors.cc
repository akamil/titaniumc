#include "AST.h"
#include "CtObjectDescriptor.h"
#include "ctValueDescriptor.h"
#include "domain-decls.h"
#include "PrimitiveDecl.h"


const CtType &TypeNode::cDescriptorTypeCreate() const
{
  undefined( "cDescriptorTypeCreate" );
  return PrimitiveDecl::VoidDecl.cType();
}


////////////////////////////////////////////////////////////////////////


const CtType &DomainTypeNode::cDescriptorTypeCreate() const
{
  return DomainNDecl[ tiArity() - 1 ]->cDescriptorType();
}


const CtType &PrimitiveTypeNode::cDescriptorTypeCreate() const
{
  return ctValueDescriptor;
}


const CtType &PointTypeNode::cDescriptorTypeCreate() const
{
  return PointNDecl[ tiArity() - 1 ]->cDescriptorType();
}


const CtType &RectDomainTypeNode::cDescriptorTypeCreate() const
{
  return RectDomainNDecl[ tiArity() - 1 ]->cDescriptorType();
}


static const CtType &cDescriptorTypeCreate(ClassDecl *cd) 
{
  switch (cd->kind())
    {
    case Common::ClassKind:
      return *new CtObjectDescriptor( *cd );
    case Common::InterfaceKind:
      return CtObjectDescriptor::interfaceHeader;
    case Common::ImmutableKind:
      return ctValueDescriptor;
    default:
      assert( 0 );
      return PrimitiveDecl::VoidDecl.cType();
    }
}

const CtType &TypeNameNode::cDescriptorTypeCreate() const {
  return ::cDescriptorTypeCreate(decl());
}

const CtType &TemplateInstanceTypeNode::cDescriptorTypeCreate() const {
  return ::cDescriptorTypeCreate(decl());
}

