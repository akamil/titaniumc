#include <string>
#include "AST.h"
#include "PrimitiveDecl.h"
#include "types.h"


#define PRIMITIVE(type)						\
								\
TypeDecl *type ## TypeNode::decl() const			\
{								\
    return &PrimitiveDecl::type ## Decl;			\
}								\
								\
TypeNode *the ## type ## Type = new type ## TypeNode;		\
PrimitiveDecl PrimitiveDecl::type ## Decl(the ## type ## Type)


PRIMITIVE(Void);
PRIMITIVE(Null);
PRIMITIVE(Bool);
PRIMITIVE(Char);
PRIMITIVE(Byte);
PRIMITIVE(Short);
PRIMITIVE(Int);
PRIMITIVE(Long);
PRIMITIVE(Float);
PRIMITIVE(Double);


void PrimitiveDecl::includeSelf( CfCode &os )
{
}


PrimitiveDecl::PrimitiveDecl( TypeNode *asType )
  : TypeDecl( intern( asType->unqualifiedTypeName() ), asType )
{
}


string PrimitiveDecl::errorName()
{
  return "primitive type " + *name();
}
