#include "AST.h"


void TreeNode::unwindSelf( CleanupList & ) const
{
}


////////////////////////////////////////////////////////////////////////


void TryStmtNode::unwindTry( CleanupList &list ) const
{
  // note that statements will be executed in the
  // reverse of the order in which they are pushed

  if (!finally()->absent())
    push( list, finally()->block()->deepClone() );

  push( list, popState.deepClone() );
}


void CatchListNode::unwindSelf( CleanupList &list ) const
{
  static_cast< TryStmtNode * >( parent() )->unwindTry( list );
}


void TryNode::unwindSelf( CleanupList &list ) const
{
  static_cast< TryStmtNode * >( parent() )->unwindTry( list );
}
