#include "AST.h"
#include "CodeContext.h"
#include "StringLitTable.h"
#include "osstream.h"


const string StringLitNode::emitExpression( CodeContext &context )
{
  ostringstream format;
  format << StringLitTable::name << "[MYBOXPROC][" 
	 << context.strings[ text() ] << ']';

  return format.str();
}
