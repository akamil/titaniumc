#ifndef _INCLUDE_TITANIUM_AUX_CODE_H_
#define _INCLUDE_TITANIUM_AUX_CODE_H_


#include "optimize.h"

class CfCode;
class ClassDecl;
class FieldDecl;


typedef void (*FieldPrinter)( FieldDecl &, CfCode & );


void emitStaticFields( ClassDecl &, FieldPrinter, CfCode & );
     
void       staticFieldDecl( FieldDecl &, CfCode & );
void externStaticFieldDecl( FieldDecl &, CfCode & );




template< class Context, class Iterator > inline
void codeGenAll( Context &context, Iterator begin, const Iterator end )
{
    while (begin < end)
	(*begin++)->codeGen( context );
}


template< class Context, class Container > inline
void codeGenAll( Context &context, const Container &container )
{
    codeGenAll( context, container.begin(), container.end() );
}

// These are in the file version.cc and contain compiler version information
extern int tcMajorVersion;
extern int tcMinorVersion;
extern char tcWhatString[];
extern char tcMajorVersionStr[];
extern char tcMinorVersionStr[];

#endif // !_INCLUDE_TITANIUM_AUX_CODE_H_
