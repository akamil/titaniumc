/* -*- c++ -*- */
/* common.h: Assorted Definitions					*/


#ifndef _COMMON_H_
#define _COMMON_H_

#include <iosfwd>
#include <string>
#include "using-std.h"


class Common {
public:
    /* Access and other properties of classes, fields, and methods. A */
    /* logical OR of these values represents a set of properties. */
    /* Example: the value  */
    /*   (Common::Modifiers) (Common::Public | Common::Volatile) */
    /* indicates a volatile public field. */
    enum Modifiers {
        None = 0,
	Public = 1 << 0,
	Private = 1 << 1,
	Protected = 1 << 2,
	Static = 1 << 3,
	Final = 1 << 4,
	Synchronized = 1 << 5,
	Volatile = 1 << 6,
	Transient = 1 << 7,
	Native = 1 << 8,
	Interface = 1 << 9,
	Abstract = 1 << 10,
	Single = 1 << 11,
	Local = 1 << 12,
	LocalInferred = 1 << 13,
	Immutable = 1 << 14,
	Sglobal = 1 << 15,
	Inline = 1 << 16,
	CompilerGenerated = 1 << 17,
	NonsharedQ = 1 << 18,
	PolysharedQ = 1 << 19,
	SharingInferred = 1 << 20,
	Strictfp = 1 << 21,
	MemberType = 1 << 22, // This and the two below are inferred by the
	LocalClass = 1 << 23,  // compiler and used for analysis.
	AnonymousClass = 1 << 24
    };

    /* A Kind is a category of type. */
    enum Kind {
      BoolKind = 1 << 0,
      CharKind = 1 << 1,
      ByteKind = 1 << 2,
      ShortKind = 1 << 3,
      IntKind = 1 << 4,
      LongKind = 1 << 5,
      FloatKind = 1 << 6,
      DoubleKind = 1 << 7,
      NullKind = 1 << 8,	/* The 'type' of null. */
      ClassKind = 1 << 9,
      InterfaceKind = 1 << 10,
      MethodKind = 1 << 11,
      VoidKind = 1 << 12,
      ArrayKind = 1 << 13,
      ArrayInitializerKind = 1 << 14, /* The 'type' of array initializers. */ 
      ImmutableKind = 1 << 15,	/* Immutable classes */
      StringConstantKind = 1 << 16  /* Used internally by constant folding */
    };
};

class Pragma {
public:
    /* A logical OR of these values represents a set of properties. */
    enum Request {
        None = 0,
	noUAE = 1,
	noDefs = 1 << 1,
	noCFG = 1 << 2,
	noOpt = 1 << 3
    };
};

void indent(ostream &os, int depth);
string stringifyModifiers (Common::Modifiers flags);


#endif
