#ifndef _INCLUDE_TITANIUM_CODE_BOUNDS_H_
#define _INCLUDE_TITANIUM_CODE_BOUNDS_H_

#include "StringRect.h"

void generateBoundsAssert(CodeContext &context, const string &condition,
			  const string &arr, TypeNode *arrayType,
			  int dim, ForEachStmtNode *WRTloop,
			  const string &n1, const string &between,
			  string n2 = "");

void generateUnitStrideCheck(CodeContext &context, 
			     ForEachStmtNode *WRTloop,
			     ArrayAccessSet *arrayAccesses,
			     map_int_to_treeSet &provablyUnitStride);

void generateBoundsChecks(CodeContext &context, 
			  ForEachStmtNode *WRTloop,
			  const StringRect *curr_rect,
			  ArrayAccessSet *arrayAccesses,
			  map_int_to_treeSet &provablyUnitStride);

#endif // _INCLUDE_TITANIUM_CODE_BOUNDS_H_
