#ifndef _CODE_SETUPAA_H_
#define _CODE_SETUPAA_H_



#include "code-aa.h"

void processMethods(TreeNode *t, void action(TreeNode *t));
void setupValuesBblock(Bblock *b);
bool isInVector(TreeNode *t, llist<TreeNode *> &vec);
void setupMethodStatics(TreeNode *t);
MethodStatics *getMethodStatics(TreeNode *methodCall);
void analyzeMethodWakeup(TreeNode *t);
void setupBblocks();
void topSortMethods();
void IPAnal();
void unsetupAA();




#endif
