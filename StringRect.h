#ifndef _TITANIUM_STRING_RECT_H_
#define _TITANIUM_STRING_RECT_H_

#include "code-foreach.h"

class StringRect {
 private:
  int arity;
  mutable map_int_to_string _lo, _hi;
  
 public:
  StringRect(const string& rect, int a) {
    arity = a;
    for (int i = 0; i < arity; i++) {
      _lo[i] = rectMin(rect, i, a);
      _hi[i] = rectMax(rect, i, a);
    }
  }

  StringRect(const string& l, const string& h) {
    _lo[0] = l;
    _hi[0] = h;
    arity = 1;
  }

  string lo(int dim) const { return _lo[dim]; }
  string hi(int dim) const { return _hi[dim]; }
};

#endif
