#ifndef _INCLUDE_IncludeOnce_h_
#define _INCLUDE_IncludeOnce_h_

#include <iostream>
#include <string>
#include "using-std.h"


class IncludeOnce {
public:
    IncludeOnce( ostream &, const string & );
    ~IncludeOnce();

private:
    void emitHeading( const string & ) const;
    ostream &out;
};


inline IncludeOnce::IncludeOnce( ostream &out, const string &root )
    : out( out )
{
    emitHeading( root );
}


inline IncludeOnce::~IncludeOnce()
{
    out << "\n#endif\n";
}


#endif
