#include <AST.h>


/***********************************************************************
 *
 *  isSharedAccess() is true when the queried node is a field/array
 *  access that would cross a shared reference.  Note that this is not
 *  a function of the type of the field/element itself.  Rather, it
 *  depends on the sharing qualification of the containing
 *  object/array.
 *
 **********************************************************************/


bool TreeNode::isSharedAccess() const
{
  return false;
}


////////////////////////////////////////////////////////////////////////


bool ObjectFieldAccessNode::isSharedAccess() const
{
  const TypeNode &container = *accessedObjectType();
  
  if (container.hasReference())
    return container.sharing() != Nonshared;
  else
    return object()->isSharedAccess();
}


bool ThisFieldAccessNode::isSharedAccess() const
{
  const TypeNode &container = *accessedObjectType();
  
  if (container.hasReference())
    return container.sharing() != Nonshared;
  else
    return false;
}


bool TypeFieldAccessNode::isSharedAccess() const
{
  // If PR #327 ever gets fixed, change this to "true".
  return false;
}


////////////////////////////////////////////////////////////////////////


bool ArrayAccessNode::isSharedAccess() const
{
  return array()->type()->sharing() != Nonshared;
}


bool PointArrayAccessNode::isSharedAccess() const
{
  return array()->isSharedAccess();
}
