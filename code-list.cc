#include "AST.h"
#include "utils.h"


void TreeListNode::emitExpressionList( CodeContext &context, vector< string > &names )
{
    names.reserve( names.size() + arity() );
    
    // Although the caller is probably only evaluating each operand
    // once, the temporaries are needed to guarantee evaluation order.

    foriter (element, allChildren(), ChildIter) {
	TreeNode &expression = **element;
	names.push_back( expression.emitExpression( context ) );
    }
}
