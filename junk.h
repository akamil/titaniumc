#ifndef _INCLUDE_JUNK_h_
#define _INCLUDE_JUNK_h_

void find_junked_arrays(TreeNode *method);

static inline bool is_junk_call(const TreeNode *t)
{
  return (isGridMethod(t, "junk") ||
	  (isExpressionStmtNode(t) && isGridMethod(t->expr(), "junk")));
}

#endif /* _INCLUDE_JUNK_h_ */


