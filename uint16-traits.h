#ifndef _INCLUDE_TITANIUM_UINT16_TRAITS_H_
#define _INCLUDE_TITANIUM_UINT16_TRAITS_H_

#include <string>
#include "ti_config.h"

#ifndef IMPLICIT_CHAR_TRAITS

#ifdef HAVE_TEMPLATE_SPECIALIZATION
template<>
#endif
struct char_traits< uint16 > {
  typedef uint16 char_type;

  static int compare( const char_type *first, const char_type *second, size_t count )
    {
      for (size_t scan = 0; scan < count; ++scan)
	if (!eq( first[ scan ], second[ scan ] ))
	  return lt( first[ scan ], second[ scan ] ) ? -1 : 1;
      
      return 0;
    }

  static char_type *copy( char_type *dest, const char_type *source, size_t count )
    {
      while (count--)
	assign( dest[ count ], source[ count ] );

      return dest;
    }

  static void assign( char_type *dest, size_t count, char_type source )
    {
      while (count--)
	assign( dest[ count ], source );
    }

  static void assign( char_type &dest, char_type source )
    {
      dest = source;
    }

  static bool eq( char_type first, char_type second )
    {
      return first == second;
    }

  static bool lt( char_type first, char_type second )
    {
      return first < second;
    }

  static char_type eos()
    {
      return char_type();
    }
};

#endif // !IMPLICIT_CHAR_TRAITS

#endif // !_INCLUDE_TITANIUM_UINT16_TRAITS_H_
