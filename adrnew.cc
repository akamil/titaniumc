#include <cstdlib>
#define DEBUG_MEMORY_USAGE
#include "debug_memory.h"

extern "C" 
{ 
  void debug_alloced(void);
  char *debug_malloc_adr(unsigned long retadr1, unsigned long retadr2, int size);
}

static int foo = atexit(debug_alloced);

void *operator new(size_t s)
{
  return debug_malloc_adr(RETADR1(), RETADR2(), s);
}

void *operator new[](size_t s)
{
  return debug_malloc_adr(RETADR1(), RETADR2(), s);
}

void *operator new(size_t s, unsigned long retadr)
{
  return debug_malloc_adr(retadr, 0, s);
}

void *operator new[](size_t s, unsigned long retadr)
{
  return debug_malloc_adr(retadr, 0, s);
}
