#ifndef _TITANIUM_MIVE_SET_SORT_H_
#define _TITANIUM_MIVE_SET_SORT_H_


#include "AST.h"
#include "SortingPredicate.h"

static inline bool precedes(Poly *p, Poly *q)
{
  if (p == q)
    return false;
  if (p == NULL)
    return true;
  if (q == NULL)
    return false;

  return (p->asString() < q->asString());
}

static inline bool precedes(Poly **p, Poly **q, int arity)
{
  if (arity == SCALAR)
    return precedes(*p, *q);
  else
    for (int i = 0; i < arity; i++) {
      if (precedes(p[i], q[i]))
	return true;
      if (precedes(q[i], p[i]))
	return false;
    }
  return false;
}

struct MIVESort : public SortingPredicate< const MIVE * > {
  bool operator () ( const MIVE *a,
		     const MIVE *b ) const
    {
      return (a->e < b->e ||
	      a->e == b->e &&
	      (a->arity() < b->arity() ||
	       a->arity() == b->arity() &&
	       precedes(a->p, b->p, a->arity())));
    }
};


#endif // !_TITANIUM_MIVE_SET_SORT_H_
