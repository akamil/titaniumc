#include "AST.h"
#include "CtDescriptor.h"
#include "CtObjectDescriptor.h"
#include "TypeDecl.h"
#include "ctValueDescriptor.h"


TypeDecl::TypeDecl( const string *name, TypeNode *_asType )
  : Decl( name ),
    _asType( _asType ),
    _cType( 0 ),
    _cDesc( 0 )
{
}


////////////////////////////////////////////////////////////////////////


bool TypeDecl::isType() const
{
  return true;
}


TypeNode *TypeDecl::asType() const
{
  return _asType;
}


////////////////////////////////////////////////////////////////////////


const CtType &TypeDecl::cType() const
{
  if (!_cType)
    _cType = &asType()->cTypeCreateUnboxed();

  return *_cType;
}


const CtType &TypeDecl::cDescriptorType() const
{
  if (!_cDesc)
    _cDesc = &asType()->cDescriptorTypeCreate();
  
  return *_cDesc;
}
