#ifndef _buildexpr_h_
#define _buildexpr_h_

#include <string>
#include "CodeContext.h"

void reset_buildexpr();

const string build(const string expr, const CtType &t,
		   CodeContext &context);

const string product(const string &a, const string &b, CodeContext &context);
const string quotient(const string &a, const string &b, CodeContext &context);
const string quotient_with_check(const string &a, const string &b,
				 const string &y, const CtType &t,
				 CodeContext &context);
const string sum(const string &a, const string &b, CodeContext &context);
const string min(llist<string> *l, CodeContext &context);
const string difference(const string &a, const string &b, CodeContext &context);
const string neg(const string &a, CodeContext &context);
const string product(const string &a, const string &b, const CtType &t, CodeContext &context);
const string quotient(const string &a, const string &b, const CtType &t, CodeContext &context);
const string quotient_with_check(const string &a, const string &b,
				 const string &y, CodeContext &context);
const string quotient_with_runtime_unitdivisor_opt(const string &a, const string
 &b, CodeContext &context);
const string sum(const string &a, const string &b, const CtType &t, CodeContext &context);
const string min(llist<string> *l, const CtType &t, CodeContext &context);
const string difference(const string &a, const string &b, const CtType &t, CodeContext &context);
const string neg(const string &a, const CtType &t, CodeContext &context);

#endif // _buildexpr_h_
