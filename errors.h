/* -*- c++ -*- */

/* errors.h: Functions for reporting errors */

#ifndef _ERRORS_H_
#define _ERRORS_H_

#include <iosfwd>
#include "parse.h"

#ifndef _STRINGIFY
#define _STRINGIFY_HELPER(x) #x                                                                    
#define _STRINGIFY(x) _STRINGIFY_HELPER(x)                                                         
#endif                                                                                             

/* Report message at source position N. */
ostream &Message(const SourcePosn N = NoSourcePosition);

/* Report error at source position N. */
ostream &Error(const SourcePosn N = NoSourcePosition);

/* Report warning at source position N, with a warning suppression identifier. */
ostream &Warning(const SourcePosn N = NoSourcePosition, const char *messagename = NULL);

/* The number of times Error has been called. */
extern int NumErrors();

/* report a fatal internal compiler error not associated with a TreeNode
   (errors with associated nodes should use TreeNode::fatal()) */
extern void _fatal_error (const string& msg) __attribute__((noreturn));
#define fatal_error(msg) \
 _fatal_error(string("Internal compiler error at " __FILE__ ":" _STRINGIFY(__LINE__)" :\n") + msg) 

#endif
