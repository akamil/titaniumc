#ifndef _METHODSTATICS_H_
#define _METHODSTATICS_H_

#include "AliasInfos.h"

MethodStatics *lookupStatics(TreeNode *md);
MethodStatics *fullMethodStatics(TreeNode *md);
MethodStatics *emptyMethodStatics(TreeNode *md);
bool SEFOverriders(const Decl *d);
bool pureOverriders(const Decl *d);


class MethodStatics;

typedef struct {
  TreeNode *waiter;
  MethodStatics *ms;
} MethodWaiter;


/* Info about the method. They are all "may" information. */
class MethodStatics {
  
public:

  /* Returns false if this method modifies any value that may be visible
     to the outside world prior to calling this method. */
  bool isSideEffectFree() const;

  MethodStatics() { }

  MethodStatics(TreeNode *m, int _numValues, TreeNodeToIntMap &avm);

  MethodStatics(TreeNode *m, Bitset *rv, Bitset *lv, Bitset *mv);

  // True iff I'm equal to ms in terms of information carried about
  // method. Compare MethodStaticses for the same method only.
  bool equal(MethodStatics *ms);

  // copy of me which includes only the info about the method (just serves
  // as a struct of 4 variables for comparison, that's all)
  MethodStatics *emptyCopy();

  // for overriding methods
  void merge(MethodStatics *ms);

  int numValues() { return leaksValues->size(); }

  /* Print with endline to os. */
  void println(ostream &os);
  

public:

  // Who's my method?
  TreeNode *method;
  
  // The internal values that this MethodDeclNode or ConstructorDeclNodes
  // returns. Values in here that are not leaked (check leaksValues) are
  // basically the same as if they had been directly new'ed in your own
  // method. Some day we can treat these values as fresh in our own
  // method, but that requires a little bit of change..
  Bitset *returnsValues;

  // The values that this MethodDeclNode or ConstructorDeclNodes modifies
  // DIRECTLY. This determines side-effects.
  Bitset *modifiesValues;

  // The values (including parameters) in this MethodDeclNode or
  // ConstructorDeclNode that get saved in a an external object's field in
  // the body of the method or in a called method. Remember to check the
  // AA_THISVALUE, especially for constructors.
  Bitset *leaksValues;

  // Mapping of AST's to values (ints)
  TreeNodeToIntMap ASTValueMap;

  // Guys to call back if I changed. A list of pairs of the form
  // (TreeNode* t, MethodStatics* m), where method m wants a call back if
  // my method statics are changed to be not equal to m
  llist<MethodWaiter *> *callBackList;

  // 1 = IN PROGRESS. 2 = COMPLETELY ANALYZED. 3 = PENDING RESULTS
  int status;

public:
  // <ext> and all args
  Bitset *specialValues;

private:
  Bitset *_myThisValueBitset;
  Bitset *_myNullValueBitset;
  Bitset *_myExtValueBitset;

public:

  // various constants
  
  /* A bitset containing just the one value. */
  Bitset *newNullValueBitset();
  Bitset *newThisValueBitset();
  Bitset *newExtValueBitset();

  /* These return the same one on every call */
  Bitset *myNullValueBitset();
  Bitset *myThisValueBitset();
  Bitset *myExtValueBitset();  
  
};



#endif // _METHODSTATICS_H_
