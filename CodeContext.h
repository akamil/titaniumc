#ifndef _INCLUDE_CODE_CONTEXT_H_
#define _INCLUDE_CODE_CONTEXT_H_


#include <cassert>
#include "LocalVars.h"
#include "osstream.h"

class CfSource;
class File;
class StringLitTable;

extern ostream &endCline( ostream & );


class CodeContext : public ostringstream, public LocalVars {

public:
  explicit CodeContext( CodeContext & );
  explicit CodeContext( CodeContext &, CodeContext & );
  explicit CodeContext( CodeContext &, CodeContext * );
  explicit CodeContext( CodeContext &, ostream & );
  explicit CodeContext( CfSource & );
  ~CodeContext();
  
  void pushNoLineDirectives() { suppress_directives++; }
  void popNoLineDirectives() { suppress_directives--; }
  void setPosition( const SourcePosn & );

  static CodeContext *recover( ostream & );

  void declare( const string &name, const CtType &type);

  StringLitTable &strings;

private:
  ostream &sink;
  CodeContext *parent;
  CodeContext &whereDeclare;
  
  SourcePosn currentPosition;
  SourcePosn startPosition;

  const int level;
  int suppress_directives;
  list<string> chunks;

  void codeGen() { codeGen( sink ); }
  void codeGen( ostream & );
  void handoff_chunks( list<string> &childchunks );

  void emitLineFileDirective( const SourcePosn &, ostream & ) const;
  void emitLineDirective( const SourcePosn &, ostream & ) const;

  friend ostream &endCline( ostream & );
  
  static const int recoverSlot;

  // forbidden
  bool operator ==( const CodeContext & ) const;
};


#endif // !_INCLUDE_CODE_CONTEXT_H_
