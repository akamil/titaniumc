/* UNUSED */

#ifndef CODE_POLY_H
#define CODE_POLY_H

#include "AST.h"

// This class is basically a TreeNode with some extra bits on it to 
// identify it as something interesting like a loop invariant or a
// primary induction variable.

class Poly {
protected:
  TreeNode *_astNode;      // The poly expression itself.
  bool _invariant;
  bool _primary_iv;
  bool _derived_iv;
public:
  Poly(TreeNode *expr, bool inv, bool p_iv, bool d_iv) {
    _astNode = expr;
    _invariant = inv;
    _primary_iv = p_iv;
    _derived_iv = d_iv;
  }
  bool &invariant() { return _invariant; }
  bool &primary_iv() { return _primary_iv; }
  bool &derived_iv() { return _derived_iv; }
  TreeNode *&astNode() { return _astNode; }
};

void polyAnalyze(TreeNode *root);

#endif // CODE_POLY_H
