#ifndef _CODE_AA_H_
#define _CODE_AA_H_


/* Alias Analysis Interface:

   setupAAMethod is called in staticAnalyze in static.cc. After the call,
   each Bblock hanging off the method will have attached non-null
   aliasesIn() and aliasesOut() AliasInfos. An AliasInfos represents all
   the information we know about any values at a particular point in the
   method. The AliasInfos::checkAliases(TreeNode *a, TreeNode *b) member
   function will tell you whether or not the 2 expressions given have the
   same values, at a point in the CFG where the AliasInfos is found. At
   the beginning of each Bblock, aliasesIn()->checkAliases(a, b) will tell
   about a and b at the BEGINNING of the block. To get information in the
   middle of the Bblock, update the AliasInfos by running AST's in the CFG
   successively through AliasInfos::process(TreeNode *t).

   For example, suppose we see this at the beginning of a Bblock: 

   a.i = 5;
   b[q].i = 6;
   foo();
   x = a.i;
   
   Say at the assignment "x = a.i", you'd like to know if a and b[q] might
   have the same value (and thus point to the same object). You take the
   aliasesIn() and run all the AST's corresponding to the previous 3
   statements through the process(...) procedure. Then you give the
   expression for a (an ObjectNode) and the expression for b[q] (an
   ArrayAccessNode) and ask the checkAliases(...) procedure on these 2
   nodes. Or if you want to check if "foo()" can return the same value as
   a's value, put in the MethodCallNode, etc...

   Currently, it only returns NO and MAYBE, and it only possibly returns
   NO for aliasable types (it won't tell you, for example, if 2 integers
   have the same value). methodAliases is a merge of all the aliasesOut
   when everything's finished (this may not mean much) Right now all
   aliasing information for one method goes away when you start analyzing
   another method.

   The output name for this phase is "aa", so -dumpphase aa. 
   
   */

   
#include "MethodStatics.h"
#include "AliasInfos.h"


Bitset *copyBitset(Bitset *b);
bool setupAliasesBblock(Bblock *b);
void finalPassAliasesBblock(Bblock *b);
void analyzeMethodWakeup(TreeNode *t);
void analyzeMethod(TreeNode *t);
Decl *getNameRootDecl(TreeNode *a);
bool isAliasable(TypeNode *t);
bool equalDeclToBitsetMaps(DeclToBitsetMap *d1, DeclToBitsetMap *d2);
void printBitset(Bitset *b, ostream &os);
void purePrintBitset(Bitset *b, ostream &os);
MethodStatics *getMethodCallStatics(TreeNode *methodDecl);
Bitset *allocateNodeGetSubvalues(TreeNode *t, AliasInfos *ai);
Bitset *getAllFormalValuesAndSubvalues(TreeNode *t,
				       AliasInfos *ai);
Bitset *getAllObjectAndFormalValuesAndSubvalues(TreeNode *t,
						AliasInfos *ai);
void analyzeMethod(TreeNode *t);



// only some types are currently handled
bool isHandled(TreeNode *x);



#endif // _CODE_AA_H_
