#include "AST.h"
#include "CtType.h"
#include "LocalVars.h"
#include "decls.h"
#include "compiler.h"


void CatchNode::emitCatchTable( ostream &os ) const
{
    ClassDecl &decl = static_cast< ClassDecl & >(*param()->dtype()->decl());
    assert( decl.category() == Decl::Class );

    // PR824: save some runtime checking overhead for statically safe exception types
    // (those with no unchecked exception subtypes) - only Throwable and Exception are problematic
    int uncheckedAllow = uncheckedAllowed(); 
    if (!uncheckedAllow && 
        &decl != ThrowableDecl &&
        &decl != ExceptionDecl) uncheckedAllow = 1; 

    os <<
	"   { (const struct class_header *)"
	"( &" << decl.cDescriptorName() << " ), "
              << "\"" << position().asString() << "\", "
              << param()->dtype()->isSingle() << ", " // PR824: note whether catch is universal
              << uncheckedAllow << " }"           // and whether it's permitted to catch unchecked exns
	",\n";
}


const string CatchNode::emitUse( LocalVars &context,
				 const ObjectNode & ) const
{
    // We use "param()->dtype()" instead of "caught.type()" because
    // the former is const while the later is not.

    const CtType &type = param()->dtype()->cType();

    context.depend( type );

    // side-effect free
    return "*((" + type + " *) &local_exnstate.exn_instance)";
}
