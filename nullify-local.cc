#include <iostream>
#include "AST.h"
#include "CfCode.h"
#include "FieldDecl.h"
#include "code.h"
#include "nullify-local.h"


void fieldNullifyLocal( FieldDecl &field, CfCode &os )
{
  field.type()->emitNullifyLocal( os, MANGLE_THIS_VAR(+) + "->" + field.cFieldName() );
}


void TypeNode::emitNullifyLocal( ostream &os, const string &target ) const
{
  if (isImmutable())
    os << ((TypeNameNode*)this)->decl()->cNullifyLocalName() << "(&" << target << ");\n";
  else {
    assert( isReference() );
    if (isLocal())
      os << target << " = 0;\n";
  }
}


////////////////////////////////////////////////////////////////////////


void PointTypeNode::emitNullifyLocal( ostream &, const string & ) const
{
}


void PrimitiveTypeNode::emitNullifyLocal( ostream &, const string & ) const
{
}


void RectDomainTypeNode::emitNullifyLocal( ostream &, const string & ) const
{
}


void TitaniumArrayTypeNode::emitNullifyLocal( ostream &os, const string &target ) const
{
  if (isLocal())
    os << target << ".A = 0;\n"
       << target << ".ancestor = 0;\n";
}


void TypeNameNode::emitNullifyLocal( ostream &os, const string &target ) const
{
    TypeNode::emitNullifyLocal( os, target );
}
