#ifndef _TITANIUM_CODE_ASSIGN_H_
#define _TITANIUM_CODE_ASSIGN_H_

#include <string>
#include "code.h"
#include "using-std.h"

class CodeContext;
class TreeNode;
class TypeNode;


const string assignPrefix(AssignKind kind, bool isStatic);
const string assignSuffix(AssignType type, bool isLocal);
const string assignMacro(AssignKind kind, AssignType type, bool isStatic, bool isLocal);
const string makeAssign(const string &lvalue, const string &value,
			const bool isLocal, AssignKind kind, AssignType type, 
			bool isStatic);
void checkPointerEscape(TreeNode *rhs, string valstr, string targetNode,
			bool checkPrivate, string exprstr, CodeContext &context);
AssignKind assignKind(const TypeNode *type);
AssignType assignType(const TypeNode *type);

#endif
