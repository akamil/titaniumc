#include "AST.h"
#include "pseudocode.h"

/* This function is intended to be used from the debugger. */
void pt(const TreeNode *t)
{
  cout << endl << pseudocode(t) << endl;
}

/* This function is intended to be used from the debugger. */
void ptv(const TreeNode *t)
{
  cout << endl << t->oper_name() << ": " << pseudocode(t) << endl;
  t->print(cout, 0);
  // cout << endl;
}

void ptall(const TreeNode *t)
{
  while (t != NULL) {
    pt(t);
    t = t->parent();
  }
}
