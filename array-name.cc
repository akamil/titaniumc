#include <AST.h>


string ArrayTypeNode::typeName() const
{
  string name;
  buildArrayTypeName( name );
  return name;
}


////////////////////////////////////////////////////////////////////////


void TypeNode::buildArrayTypeName( string &name ) const
{
  name = typeName() + ' ' + name;
}


void ArrayTypeNode::buildArrayTypeName( string &name ) const
{
  if (!name.empty() && name[ name.length() - 1 ] != ']')
    name += ' ';

  name += singleName( dimensionTypeName() );
  elementType()->buildArrayTypeName( name );
}


////////////////////////////////////////////////////////////////////////


string JavaArrayTypeNode::dimensionTypeName() const
{
  return "[]";
}


string TitaniumArrayTypeNode::dimensionTypeName() const
{
  return '[' + expr()->typeName() + "d]";
}
