\chapter{Introduction}\label{ref:intro}
The Berkeley UPC compiler is a source to source translator that maps the communication required
for access to remote objects, barriers and locks into run-time layer function calls and macros.
The run-time layer is largely a thin wrapper around calls to the \Gn communication API.
\Gn provides a uniform API to numerous low level networking protocols, including InfiniBand,
Quadrics Elan, Myrinet GM and IBM LAPI.  We note that, apart from UPC, \Gn is used in the
% (MLW) I thought Perry said Matlab*P did not use gasnet?
%implementation of the Titanium language, co-array Fortran, Matlab*P as well as an MPICH2
implementation of the Titanium language, co-array Fortran, as well as an MPICH2
ADI layer.
This document describes a proposed implementation of \Gn over the \Pt API for the Cray XT3
and future systems based on the Cray seastar network processor.

Throughout this document we assume detailed understanding of the UPC language as well as
the Portals and the GASNet API.  
The specifications for these can be obtained at:
\begin{tabbing}
 \hspace{1cm}\= LONGSPACE  \= other stuff \kill
        \> \textbf{UPC}     \> \verb+http://upc.gwu.edu/+\\
        \> \textbf{GASNet}  \> \verb+http://gasnet.cs.berkeley.edu+\\
        \> \textbf{Portals} \> \verb+http://www.cs.sandia.gov/Portals+
\end{tabbing}

In this chapter, we outline how UPC operations are mapped to \Gn operations and how \Pt is implemented
on the Cray XT3.
%We also outline the assumptions and limitations of this current design and suggest several extensions
%to \Pt that would aid in our implementation or improve performance.

\section{UPC over \Gn}
In a single-threaded environment, such as Catamount, each UPC thread will map to a \Gn node.
In a multi-threaded environment, UPC threads may map to threads within a \Gn node.
UPC shared objects will reside in the \Gn Remote Access Region (RAR), a portion of memory on each
node that is allocated at job start-up and pinned for RDMA access.
Access to UPC shared objects is implemented through \Gn memory-to-memory operations that map to 
\Pt \Put and \Get operations.
Shared memory allocation and lock functions are implemented using the \Gn Active Message API.
The active messages issued by the compiler and run-time system have small data payloads.
UPC libraries implemented directly over \Gn may use larger payload active messages.

\section{The Choice of Portals as an Implementation Target}
We choose \Pt as an implementation target for the following practical reasons:
\begin{enumerate}
\item It has a well-defined API and is the communication layer used by the Cray MPI and Shmem
implementations,
and therefore will continue to improve and be supported.
\item It is the only low-level communication API supported.
\item It has all the functionality needed to implement a complete \Gn conduit.
\item The implementation will be easily ported to future Cray systems that may have
different network hardware so long as \Pt is available.
\end{enumerate}
The alternative would be to build a \Gn implementation from the ground up.  
That would involve modifying, or getting Cray to modify and support, changes to the 
network layer and resources on the seastar chip as well as custom kernel modules.
Further, it would require dedicated and privileged access to XT3 hardware.

It is our hope, that we can work within the existing software infrastructure, and, if necessary,
request extensions and modifications to the \Pt implementation for the benefit of {\em GASNet}.
These changes would be driven by demonstrated performance issues as the implementation
progresses.

\section{The Cray XT3 Implementation of \Pt}
The software is separated into modules that provide an {\em api} layer accessible to the
user application ({\em GASNet}), a {\em lib} layer that implements the \Pt semantics either in the 
operating system kernel or on the seastar network processor (or both), and the {\em nal} that
implements the network specific data transfers.
Between these layers are {\em bridge} layers that provide common APIs for different
operating systems and nals.  
The current production implementation is for the Catamount operating system with the seastar nal.
A linux port is underway and various other nal layers have been implemented, including 
a process based nal that allows for debugging of portals code without actual hardware.

Catamount is a small, light weight operating system with restricted functionality, but suitable
for most compute-intensive scientific applications.  
Catamount itself does not provide thread support but one can think of the implementation
as having one Opteron thread executing client and OS code, and one seastar thread
that controls the DMA engines, handles incoming messages and possibly executes 
portions of the \Pt protocol.

In the current implementation over Catamount, the vast majority of \Pt processing
(the {\em lib}) occurs on the host Opteron.  
In particular, the receipt of a new message on the network forces an interrupt to the kernel.
The kernel interrupt handler reads the message header, performs memory descriptor matching (based on 
the \Mbits specified in the portals operation), computes the virtual address range of
the target in memory, translates to physical addresses, and pushes this information back to
the seastar.  The seastar thread uses this information to issue the DMA commands that
implement the data transfer.  
When the message transfer is complete, a second interrupt is generated to notify the
host since \Pt event generation is done in the kernel.
In the case when an event queue has a handler registered, the event handler will
run in user space when the client explicitly polls for events to process.

This implementation is known to introduce additional Opteron software overhead and message latency 
and Cray is investigating the implementing of an {\em accelerated} mode for certain applications.
In accelerated mode, more of the timing critical code (such as message matching, address translation
and event generation) is pushed to the seastar processor.
Unfortunately, the seastar has a small (384 KB) SRAM memory and this resource can quickly
become depleted, depending on what portals objects must be stored there.
In Catamount, large blocks of virtual addresses are allocated as large blocks of physical
memory so the address translation table for Portals memory descriptors would be relatively small.
This may not be the case with Linux, where large blocks of virtual memory are broken into 4KB
pages that may be scattered throughout physical memory.
Regardless, the \Pt event queues would be placed in main memory because they could easily
consume the majority of the limited seastar memory.
%This is particularly important because small queues can be easily over-run if the application
%is busy computing and not processing events in a timely manner.

We note that even if all the \Pt protocol is implemented on the seastar chip, the 
client thread must still poll and process the events on the event queues frequently 
enough to prevent resource depletion.
The \Pt implementation is reliable to the extent that a message
will be delivered exactly once to a remote process.  If, however, there is no space
left in the selected (locally managed) receive buffer, the message is dropped and
a higher-level reliability protocol would be required to prevent a fatal error or hang.

\section{Assumptions and Limitations of this Design}
The current design is only for a \verb+GASNET_SEQ+ (as defined in the \Gn spec) implementation.
Furthermore, this is the only implementation that can be supported on the Catamount operating
system since it does not support a threading model.
\Pt processing does occur in interrupt handlers, but \Gn handler functions will only execute
when the application is polling, not in interrupt handlers.
For this reason, the \Gn {\em handler-safe locks} and {\em no-interrupt sections}
are trivially implemented.
Although the initial design is for a single-threaded polling model, we have been
careful to insure that \Gn handles are managed as thread specific data as required by the specification.  
In a threaded environment, other resources managers, such as the chunk allocator will
require some form of locking.

Our initial design for a Linux port will be single-threaded as well.
However, Linux does support a threading model and it will be possible to implement
an {\em option} that supports a second user-level ``progress'' thread that can handle
at least some of the \Pt event processing.  Such an option would require
the implementation of the atomicity controls detailed in the \Gn specification.
Other extensions, such as UPC threads mapped to Linux threads within a single process on
a multi-core systems may also be possible.

This design does not include a reliability mechanism resulting from resource
depletion.  Two cases can occur:
\begin{enumerate}
\item {\bf Request Receive Buffer Overflow}.  This can occur when active message
request packet arrives at a node and there is insufficient space in the receive 
buffer for message.  Our design attempts to mitigate this problem by using a
double-buffer scheme whereby the first buffer is processed, reset and added
back before the second buffer fills.  Further, in the event the second buffer fills
before the first can be re-cycled, we implement a ``catch-basin'' algorithm
in which the critical metadata of the dropped message is captured in an event queue.
We provide enough information so that the event handler can retrieve the original
message using a \Get operation.  However, even this scheme can fail if the 
catch-basin event queue overflows.
\item {\bf Event Queue Overflow}.  This occurs when events are generated faster
than the implementation can process them.  It is particularly likely to occur
when most or all nodes are communicating with a single node that is busy executing
client application code. 
\end{enumerate}

The current design can detect the event queue overflow and generate a fatal error.
A reliable implementation would either have to use a flow-control system to prevent
the overflow, or a recovery scheme whereby the sender must re-transmit the messages
that do not complete in a reasonable time-frame.
Both solutions have performance implications.  Flow-control solutions tend not to scale
well with the number of processes in the job.
Timeouts and re-transmissions have the problem of sending multiple messages even when
the initial message was processed, but just not acknowledged in a timely manner.
We could possibly implement one of the reliability mechanisms as a run-time option.
We will certainly allow the application to influence the size of various buffer resources
to prevent depletion through various environment variables.

Additionally, this design assumes that a single \Pt memory descriptor can cover
the largest memory region we can allocate.  The current \Pt XT3 implementation
has a 4GB restriction on each memory descriptor.  This restriction will be relaxed
in the second quarter 2006 software release from Cray for generic application, 
when the size of a memory descriptor will exceed 1 terabyte.
{\bf\color{darkred}
Note that Kyle Hubert of Cray says the size of a memory descriptor will be contained in
a 64-bit field but that the offset will be 40-bits.  He also states that this
may conflict with an accelerated-mode implementation.
}
If necessary, we can extend the design such that large pinned regions will be
tiled with multiple memory descriptors.

The design assumes a \Pt maximum transfer size of 2GB, the Cray XT3 limit.  
Transfers larger than this will be broken into multiple transfers.
{\bf\color{darkred}
Note that Kyle Hubert of Cray states that this will become 4GB in the near future.
}

Section 4 of the \Pt 3.3 specification indicates that the \Mbits of an operation are
echoed {\bf without modification} into the event records associated with the operation,
including \verb+ACK+ and \verb+REPLY_END+ operations, that must be reflected back
across the network from the initiating operation.
Our design critically depends on this, as we hide up to 60 bits of metadata in 
the \Mbits field.
In a recent conference call with Don Lee of Cray, we learned that Cray also 
depends on this for their MPI and Shmem implementations.

This implementation will support the \verb+GASNET_SEGMENT_FAST+ 
configuration.  This configuration can limit the size of the \Gn remote access region
(UPC shared objects) but guarantees the region is pinned and registered at job
start-up for efficient RDMA access (via Portals) throughout the job execution.
The size of the segment is dependent on the amount memory the system can pin.
Under Catamount, we expect this to be most of available physical memory.
Under Linux, it may be less due to other, operating system requirements.

If this becomes too restrictive, we will implement the \verb+GASNET_SEGMENT_LARGE+ 
configuration, which would allow for a larger (virtual) segment in which not all 
portions of the remote access region are pinned at all times.  
Such a configuration would use the {\em firehose} component of the
\Gn software suite to dynamically pin, and lazily un-pin memory regions.

A \verb+GASNET_SEGMENT_EVERYTHING+ configuration is necessary for a port of the 
Titanium language, and is beyond the scope of this contract. Our design
decisions are intended to be compatible with future extension to 
\verb+GASNET_SEGMENT_LARGE+ and \verb+GASNET_SEGMENT_EVERYTHING+ support
through the use of the {\em firehose} library.

Finally, we note that we will make use of portions of the existing ``extended-reference''
implementation of the \Gn extended API.  
In particular, {\it Barriers} and the {\it Memset} operations will use
the extended-reference implementation and we will borrow the implementation of
thread-safe {\it gasnet\_handle\_t} objects.

% (MLW: commented out this section.  We do use SEND_END events, hide tons of metadata
%  in match_bits which are guaranteed by the spec to be transported unmolested to the
%  target and reflected back in an ACK.  )

% \section{Limitations of the \Pt Design}

% We see several deficiencies in the \Pt design and request a clarification of 
% the event queue handler semantics:
% \begin{enumerate}
% \item We need to be able to tie events associated with a \Pt operation back to the \Gn operation
% that initiated it.
% We currently do this by hiding a reference to a \Gn handle in the ignored bits of the 
% \Mbits of a \Pt \Put or \Get operation.
% This is a bit of a hack and it would be nice if there were some way to provide a 
% 64-bit user-defined value on each
% \Pt \Put and \Get operation that will be echoed in the corresponding \verb+ptl_event_t+ object.
% The \Pt \Put operation does provide such a feature (the \verb+hdr_data+ argument) but it is not
% available on the \Get operations.  
% {\bf\color{darkred} Note that Cray has verbally stated that the match-bits are not
% and will not be modified by the \Pt implementation so this hack of hiding information in
% the match-bits will satisfy out need. }
% % DOB: irrelevant - if we had hdr_data for portal gets, we could use it for all non-AM put/gets
% %Furthermore, our design already uses \verb+hdr_data+ to implement 
% %the catch-basin scheme for the recovery of dropped active message requests.

% \item The \Pt design does not allow fine grain control over exactly which events will
% be generated on a memory descriptor.  It does allow the disabling of {\em all} start events
% or {\em all} end events (or both).  If, for example, a memory descriptor needs only 
% the \verb+PTL_EVENT_REPLY_END+ event, it will also get the unwanted \verb+PTL_EVENT_SEND_END+
% event on every \Get operation, and similarly for the {\em Put}.
% It might be nice to have finer grain control to reduce the work of event generation and to
% prevent the application from having to allocate event queues twice as large and what would
% otherwise be needed.
% {\bf\color{darkred} Doug Gilmore of Cray states that, even in a possibly accelerated mode
% implementation, the event queue space will be allocated in main memory, removing this
% as a critical constraint.}

% \item The discussion of event queue handlers in the specification is vague.
% It states that if a handler is registered it will be called for each event
% posted to the event queue.  It further states that the \Pt library must call
% the handler before PtlEQGet(), PtlEQWait() or PtlEQPoll() returns with a status
% of \verb+PTL_OK+ or \verb+PTL_EQ_DROPPED+.  Finally, it states that after a successful
% handler run, the event will be removed from the queue.
% What is the definition of a ``successful'' handler run?  The handler function
% specification return type is \verb+void+.  
% It seems that if a handler is registered with a queue, it is run for every
% event posted and the event are then removed from the queue.
% What then is returned to the client who must poll the queue in order to activate
% the handler?
% What is the point of registering a handler if it only activates when the client
% is polling and therefore ready to process the events anyhow?
% {\bf\color{darkred} Note: this question was answered by Doug Gilmore of Cray
% in the conference call on 1/13/2005.}

% \end{enumerate}


%{\color{red} Note to Kathy and Dan:}
%{\it The suggestion of a light-weight RDMA operation that does not require the target host
%processor intervention may not be as useful as we originally thought.
%It would save two target side interrupts but still requre the full overhead on the 
%initiator associated with possibly pinning and pushing the virtual-to-physical address translation
%of the src/dest to the seastar.  Further, the host will have to be involved in posting
%the completion event.  Ron Blackwell seemed to recall that removing the host from
%the portals matching operations only saved about 1 usec.  
%Maybe we need more hard data before requesting such a change.
%}
% save this for later
%Another suggestion is to provide some form of light-weight RDMA operations, possibly even
%outside of the \Pt specification.
%We would use this mechanism for the \Gn remote access region.  A region of memory that
%is allocated and pinned at job startup and persistant through the lifetime of the job.

\section{Performance}
During the development of this design, we implemented several point-to-point communication 
benchmarks over portals to collect latency, bandwidth and software overhead values, as well
as to gain experience writing Portals applications.
We present some of the results here.
The benchmarks were run on the {\it BigBen} XT3 system at the Pittsburgh Supercomputer Center and
on the {\it Jaguar} system at Oak Ridge National Laboratory.
The systems were running generic (non-accelerated) portals with the Catamount compute node
operating system.

\subsection{PingPong}
The pingpong benchmark measures the average round-trip time to send a message of size $N$ 
bytes to a target node and receive an acknowledgement or reply.  We implemented several 
means of providing an acknowledgement, including 
(1) an $N$ byte \Put followed by an $N$ byte \Put reply, 
(2) an $N$ byte \Put followed by a {\it zero} byte \Put reply,
(3) an $N$ byte \Put followed by a \Pt \verb+ACK+ event, and
(4) an $N$ byte Portals \Get operation.
Method (3) corresponds to how a \Gn \Put will be implemented and, of course method (4) 
is how the \Get will be implemented.
In addition, we considered three cases for the initiating memory buffer: 
(A) pre-pinned source buffer, (B) copy through a pre-pinned bounce buffer, and (C) pin and un-pin
the source buffer.  
Case (A) corresponds to {\it putting from} or {\it getting into} the UPC shared memory segment.
Case (B) corresponds to copying small messages through a pre-pinned bounce buffer, which
will be done to remove the cost of pinning and unpinning the source or destination
memory region for a small messages.
Finally, case (C) corresponds to {\it putting from} or {\it getting into} UPC private
memory regions.
By ``pinning'', we refer to the creation of a \Pt memory descriptor to cover
the buffer and ``unpinning'' is the act of destroying ({\it PtlMDUnlink()}) the descriptor.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=6.0in]{PortalsLatency.jpg}
\caption{Portals Round-trip Latency Times}
\label{fig:Latency}
\end{center}
\end{figure}

Figure \ref{fig:Latency} shows the latency for the {\it Put+Ack} and {\it Get} methods
using the three cases of how the source (dest) memory buffer is managed.
We see that the {\it Put+Ack} and {\it Get} times are comparable. 
We also see the Latency jump at 17 bytes.  
Messages of length 16 bytes and less include all the data in the initial network packet.  
In this case the seastar does not have to allocate a CAM slot or program the DMA engine 
and it also can immediately issue the END event without taking a second interrupt at 
either node.  So, it takes about 9 usec to deposit a single byte to a target memory
location and to be notified it has arrived.  As we shall see, the CPU will be busy processing
the data transfer for only about 2 usec of this time.
Although not shown, the typical measure of half round-trip time of a 1-byte \Put followed
by a 1-byte \Put reply is just over 5 usec and jumps to about 8 usec at 17 bytes.

This figure also shows that the cost of dynamically creating and destroying a memory descriptor 
(pin and un-pin) for the source buffer is about 1.3 usec.
Although not shown, this low cost stays flat for even large buffers (we ran with up to 16 MB messages).
Catamount clearly does not have to do much work to translate virtual to physical addresses and
large blocks of contiguous virtual memory map (probably trivially) to large blocks of physical
memory.
This will not be the case under a simple Linux port, where large blocks of virtual memory are 
allocated to non-contiguous 4k blocks of physical memory. 

Finally, Figure \ref{fig:Latency} it shows that for messages of size less than 1KB, it is 
faster to copy the data through a pre-pinned bounce buffer than to dynamically create and 
destroy a memory descriptor for the \Pt \Put operation. 

\subsection{Flood Test}
The Flood benchmark we measure the average time on the origin node required to complete portals 
\Put operations from a pre-pinned send buffer with multiple \Put operations in flight simultaneously.  
That is, we initiate QDepth \Put operations of size $N$ bytes, then wait for a \verb+SEND_END+ event 
and immediately re-issue another \Put.  
In doing so, we try to keep QDepth {\it Puts} in flight at all times.  
We do this for 10000 iterations then compute the average \Put time, and repeat the experiment for 
QDepth values of 1, 2, 4 , 8, and 16.  We then compute the resulting bandwidth.
Figure \ref{fig:Flood} shows that we saturate the outbound bandwidth at \verb+QDepth=4+ and
achieve a peak bandwidth of about 1110 MB/sec.
We also see that for a single stream, we achieve an $N_{1/2}$ somewhere between 4 and 8 KB.
Furthermore, we tested both unidirectional and bidirectional (send in both directions at once) 
bandwidth tests and the results were nearly identical.  

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=6.0in]{FloodBandwidth.jpg}
\caption{Portals Bandwidth based on the Flood Benchmark}
\label{fig:Flood}
\end{center}
\end{figure}

\subsection{Send Overhead}
Finally, we wrote a benchmark that attempts to compute the CPU overhead associated
with sending a message of a given size.
The test computes the average time to perform the following sequence of operations:
(1) An $N$ byte {\it PtlPutRegion()} initiation followed by 
(2) Computing in a tight floating point loop of known time duration, followed by
(3) waiting for the Portals \verb+SEND_END+ event.
We iterate this 10000 times and compute the average time and repeat the test with
varying number of floating point loop iterations in (2).
We are interested in when the average run time starts to increase.  
At this point, the floating point loop is beginning to take longer than what can be 
hidden by overlap with communication.  
By subtracting the average time at this point from the time in the flop loop, we compute 
the time spent by the CPU to issue and complete the \Put operation.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=6.0in]{Ovhd_16_17.jpg}
\caption{Portals Send Overhead for 16 and 17 Byte Messages}
\label{fig:Ovhd}
\end{center}
\end{figure}

Figure \ref{fig:Ovhd} shows the send overhead results for both a 16-byte (brown) and 
17-byte (purple) message.
We see that the send overhead for a 16-byte message is about 2 usec and that there is
little opportunity to overlap computation with communication.
On the other hand, for the 17 byte message, we see the higher overhead due to the processing
of the second interrupt.  We also see a greater opportunity to overlap computation with
communication as the curve flattens out when we perform about 1100 iterations of this
particular floating point loop, which translates to about 6 usec.
Finally, we note that the curves for a 2KB message (not shown) are nearly identical to 
that of a 17 byte message.


\section{Document Overview}
This document presents a relatively detailed design of \Gn over \Pt for the Cray XT3.
We present the data structures and \Pt objects required by the design in Chapter \ref{ref:resources}.
We then present the \Gn Extended API, which includes the memory-to-memory (\Put and \Get)
operations in Chapter \ref{ref:extended}.  Finally, we present the \Gn Core API, and, in particular,
the Active Message implementation in Chapter \ref{ref:core}.
We present the material in this order because the memory-to-memory operations (remote access
to UPC shared objects) are the common case and because the mapping to \Pt is less complicated
than the active message operations.


