\chapter{The \Gn Extended API}\label{ref:extended}

Our description of the \Gn implementation over \Pt will be done in the reverse
order of the \Gn specification document.  
We will first present the Extended API because (1) the memory-to-memory operations
implement off-node access to UPC shared objects, and therefore occur frequently
and (2) they are simpler than the Active Messages of the \Gn Core API.

The Extended API defines a collection of memory-to-memory operations that perform 
\Put, \Get and {\it Memset} data operations on the RAR of a target node.
The operations come in multiple flavors: blocking and non-blocking, aligned data or unaligned (bulk)
data, as well as puts from and gets directly into a host register (when supported by the 
underlying hardware). 
In addition, there are two types of non-blocking operations depending
on how the client wishes to manage the \Gn handles needed to complete or
synchronize the operation.  
Explicit handle operations return a handle to the initiator.
Implicit handle operations rely on the \Gn implementation to manage the handles as a 
group.   
Completion is synchronized on the group, or a well-defined subset of the 
group.  
The client may instantiate an "access region" whereby all implicit non-blocking
operations within the region are managed as a group.  At the end of the region, a single
explicit handle is returned to the client representing all the outstanding implicit operations
in that region.  
Implicit non-blocking operations issued outside of an access region must be
completed as a group using one of the \Gn {\it sync\_nbi} functions.

\Pt imposes no alignment restrictions on the {\it PtlPut[Region]} and {\it PtlGet[Region]}
message data.  If the data happens to be cache-aligned the underlying XT3 transfer operations
may be more efficient.  The unaligned {\it bulk} memory-to-memory \Get operations and the
blocking \Put operations will be implemented identically to the corresponding aligned operations:

\begin{Verbatim}
#define gasnet_get_bulk       gasnet_get
#define gasnet_get_nb_bulk    gasnet_get_nb
#define gasnet_get_nbi_bulk   gasnet_get_nbi

#define gasnet_put_bulk       gasnet_put
\end{Verbatim}

The non-blocking \Put operations have slightly different local completion semantics than their 
{\it bulk} counterparts.  The {\it bulk} non-blocking Puts return immediately to the calling
thread and the source data may not be modified until synchronization.
The {\it non-bulk} non-blocking Puts cannot return until the source data is safe to modify.

Additionally, \Pt cannot access or target the host processor's registers, it can only 
modify the users address space.
Given this, {\it gasnet\_put\_val()} will be implemented as a ``store-to-memory'' operation
followed by a {\it gasnet\_put()}.
Likewise, {\it gasnet\_get\_val()} will be implemented as a {\it gasnet\_get()} followed
by a ``load''.
Similarly, the non-blocking ``value put'' and ``value get'' operations will be implemented
in terms of their non-blocking ``memory put'' and ``memory get'' operation.
These register-based operations will not be discussed further.

The \Gn Extended API also includes a split-phase barrier operation, which will be
presented as the last section in this chapter.

\section{The \Gn Get operations}
The \Gn {\it Get} family of operations fetch \verb+nbytes+ of data from the \verb+src+ address on
the target \verb+node+ and place then into the \verb+dest+ address on the initiating node.
The source address range $[src:src+nbytes-1]$ must lie within the Remote Access Region (RAR) 
of the target node.  The destination address $[dest:dest+nbytes-1]$ may lie entirely within
or entirely outside the RAR of the initiator.  
Because of the \Gn guard pages on the RAR, it will be a fatal error (and probably a programming
or compiler bug) for either the src or dest region to lie partially within and partially
outside either the local or remote RAR.

We shall present the implementation of the explicit handle non-blocking get operation first
then discuss the other operations in this family as variations of it.

\subsection{gasnet\_get\_nb}
\begin{Verbatim}
gasnet_handle_t gasnet_get_nb ( void*          dest,
                                gasnet_node_t  node,
                                void*          src,
                                size_t         nbytes);
\end{Verbatim}

This \Gn operation will be implemented using {\it PtlGetRegion} function.
If the size (\verb+nbytes+) is larger than what can be handled in a single 
\Pt transfer (2GB), multiple non-blocking get calls will initiated using the
\Gn implicit-handle access region functionality.

Given that the completion of this operation will be performed in a
context different from this calling routine, any termination or cleanup operations that
need to be performed will be done by the event handler of the destination memory
descriptor.

In order to issue the \Pt {\it Get} operation, the destination memory region on the initiating 
node must be covered by one a memory descriptors.  If the dest region is in the local
RAR we will use the \verb+RARAM_MD+ memory descriptor.  
If not, and if the data payload (\verb+nbytes+) is smaller than \verb+GASNETE_GETBB_MAX+, 
we can allocate a chunk from the ReqSB and use it as a bounce buffer.
Finally, if \verb+nbytes > GASNETE_GETBB_MAX+ we will define a temporary memory descriptor
to cover the region for the duration of this operation.
Note the definitions:

\begin{Verbatim}
  #define CACHE_SIZE 64        /* 64 bytes for Opteron */
  #define GASNETE_GETBB_MAX    (CHUNK_SIZE - CACHE_SIZE)
\end{Verbatim}

The first step in the implementation is to break up any large transfer into a
sequence of smaller transfers that will fit into a single \Pt \Get operation.
Given that \verb+PTL_MAX_TRANSFER+ is 4GB, this will be a rare event.
Given that the current bandwidth is just over 1100 MB/sec, such a transfer
would take about four seconds per \Pt \Get and the overhead of managing 
additional handles would be insignificant.
\begin{Verbatim}
gasnet_handle_t gasnet_get_nb ( void*          dest,
                                gasnet_node_t  src_node,
                                void*          src,
                                size_t         nbytes)
{
   /* break up large transfers */
   if (nbytes > PTL_MAX_TRANSFER) {
      gasnet_begin_nbi_accessregion()
      while( nbytes > 0) {
         len = MIN(nbytes, PTL_MAX_TRANSFER);
         gasnet_get_nbi(dest,src_node,src,len);
         nbytes -= len;  dest += len;  src += len;
      }
      gh = gasnet_end_nbi_accessregion()
      return(gh);
   }

   /* Data will fit into a single portals transfer, See following discussion... */
}
\end{Verbatim}

\begin{figure}[htbp]
\begin{center}
\includegraphics[height=4in]{GET_fig.jpg}
\caption{Possible data paths used in {gasnet\_get*()} operations}
\label{fig:GET}
\end{center}
\end{figure}

See Figure \ref{fig:GET} for a diagram outlining the data transfer operation of 
the {\it gasnet\_get\_nb} function.
The operation proceeds as follows:
\begin{enumerate}
\item A \verb+gasneti_eop+ object is allocated with handler \verb+gh+.
\item The destination memory descriptor \verb+dest_md+ is selected or created as follows:
  \begin{description}
  \item[INRAR] If the destination memory region is contained in the in the local RAR, then
        \verb+dest_md = RARAM_MD+ and \verb+l_offset+ is $dest-gasneti\_seginfo[gasnet\_mynode()].addr$.

  \item[BB] Otherwise, if \verb+nbytes <= GASNETI_GETBB_MAX+, allocate a Chunk from 
        \verb+ReqSB+ at offset \verb+c_offset+.  Reserve the first Opteron cache line
        of this chunk for metadata and set {\it l\_offset = c\_offset + CACHE\_LEN}.
        Recall, all chunks are allocated on cache-line boundaries.  Store the pointer
        {\it dest} in the first 8 bytes of the metadata.
        This will be used later, when copying the data through the
        bounce buffer to the final destination.

  \item[USERBUF] Otherwise the message is large and not in the local RAR.  Construct
        a temporary memory descriptor (\verb+dest_md+) as specified in Table \ref{tab:TmpPutGet} 
        containing the users destination memory region.  Set \verb+l_offset=0+.
        Note that we could also use the {\it GASNet Firehose} software module to handle this
        for us.
  \end{description}

\item Construct the \verb+match_bits+ as shown for the {\it Get} operation in Table \ref{tab:GNGet}
      with the compact 24-bit representation of \verb+gh+ stored in bits $[31:8]$.
      The least significant nibble contains the least significant nibble of \verb+RAR_BITS+
      (Table \ref{tab:MLE})
      to select the \verb+RAR_MD+ memory descriptor on node {\it src\_node}.
      Note that no events will be generated on the target node.

\item Construct the offset \verb+src_offset+ as {\it src - gasneti\_seginfo[src\_node].addr}.

\item Issue the {\it PtlGetRegion()} call with arguments as shown in Table \ref{tab:GNGET}

\item Return to the calling thread.
\end{enumerate}

\begin{table}[htdp]
\caption{Arguments of {\it PtlGetRegion()} used in {\it gasnet\_get\_nb()}}
\label{tab:GNGET}
\begin{center}
\begin{tabular}{|c|c|p{10cm}|}
\hline
\multicolumn{3}{|c|}{ {\bf Argument of {\it PtlGetRegion()}} } \\
\hline
  {\bf Name}     & {\bf Value}          & {\bf Comment} \\ \hline
  md\_handle     & \verb+dest_MD_h+     & Handle of local memory descriptor \\ \hline
  local\_offset  & \verb+l_offset+      & Offset of destination within \verb+dest_md+ \\ \hline
  length         & $nbytes$             & Length of data payload (in bytes) \\ \hline
  target\_id     & {\it PtlID(src\_node)}    & NID and PID of {\it src\_node} \\ \hline
  pt\_index      & \verb+GN_RAR_PTE+    & Target Portals Table index  \\ \hline
  ac\_index      & \verb+GN_AC+         & Target Portals Access Table index \\ \hline
  match\_bits    & {\it see below}      & Targeting ReqRB on {\it dest} \\ \hline
  remote\_offset & \verb+src_offset+    & ReqRB is locally managed  \\ \hline
\hline
\multicolumn{3}{|c|}{ {\bf match\_bits argument of {\it PtlPutRegion()}} } \\
\hline
  {\bf Name}   &  {\bf Bit Range}  & {\bf Value} \\ \hline
  unused       &  [63:32]          & Not used \\ \hline
  AM\_Flag     &  [31:08]          & Compact representation of \verb+gasnet_handle+ \\ \hline
  Msg\_Type    &  [07:04]          & This is a {\it Get} Message \\ \hline
  m\_bits      &  [03:00]          & Lower 4 bits of \verb+RAR_BITS+ {\it Target MD} \\ \hline 
\end{tabular}
\end{center}
\end{table}

At some point in the future, the message will be delivered into \verb+dest_md+ at offset
\verb+l_offset+.  The event handler associated with \verb+dest_md+ will process the 
\verb+REPLY_END+ event (\verb+ev+) and take the following action:

\begin{enumerate}
  \item extract the compact representation of the \verb+gasnet_handle+ from bytes 
    $[1:3]$ of \verb+event->match_bits+.  Convert to a pointer \verb+gh+.

    \begin{description}
    \item[INRAR] Do nothing.

    \item[BB] Recover \verb+l_offset = ev->offset+.  Recover \verb+c_offset+ by subtracting
      \verb+CACHE_LEN+ from \verb+l_offset+.  Extract the pointer to {\it dest} stored in
      the first 8 bytes and \verb+nbytes = ev->mlength+.
      Construct the pointer $p=ev->ms.start + l\_offset$ and perform {\it memcpy(dest,p,nbytes)}.
      Finally, deallocate the Chunk starting at offset \verb+c_offset+.

    \item[USERBUF] release the \verb+tmp_md+ by calling {\it PtlMDUnlink()}.
    \end{description}
\item Mark the operation complete by setting the \verb+GASNETE_STATE_COMPLETE+ 
  bit of \verb+gh->flags+.  This operation is now ready to by synchronized by the
  initiating thread.

\item Return to calling thread.
\end{enumerate}

\subsection{gasnet\_get}
This is the blocking version of gasnet {\it Get}.  The implementation is trivial now that
\verb+gasent_get_nb+ is defined.  \verb+gasnet_wait_syncnb+ will poll the network, reaping
\Pt events until \verb+gh->flag+ indicates the operation is complete.

\begin{Verbatim}
void gasnet_get ( void*          dest,
                  gasnet_node_t  src_node,
                  void*          src,
                  size_t         nbytes)
{
    gasnet_handle_t gh = gasnet_get_nb(dest,src_node,src,nbytes);
    gasnet_wait_syncnb(gh);
}
\end{Verbatim}

\subsection{gasnet\_get\_nbi}
\begin{Verbatim}
void gasnet_get_nbi ( void*          dest,
                      gasnet_node_t  node,
                      void*          src,
                      size_t         nbytes)
{
    gasnet_handle_t gh = gasnet_get_nb(dest,node,src,nbytes);
    // now link onto the appropriate list of the current iop.
    gh->next = current_iop.get;
    current_iop.get = gh;
}
\end{Verbatim}

\section{\Gn Put Operations}
Similar to the {\it gasnet\_get*()} implementations, we need only discuss 
{\it gasnet\_put\_nb()} and {\it gasnet\_put\_nb\_bulk()} for messages of
size less than or equal to \verb+PTL_MAX_TRANSFER+.
The blocking {\it gasnet\_put()} is implemented in terms of {\it gasnet\_put\_nb()}
by polling and waiting for the handle to complete.
There is no difference (under Portals) to {\it gasnet\_bulk()} and {\it gasnet\_put()}.
The {\it *\_nbi} versions are implemented in terms of the corresponding {\it *\_nb}
operations by linking the \verb+gasnet_handle+ onto the current {\it put} iop list.

The only difference between {\it gasnet\_put\_nb()} and {\it gasnet\_put\_nb\_bulk()}
is that the {\it bulk} version may return to the calling thread as soon as the corresponding
{\it PtlPutRegion()} call returns and the {\it non-bulk} version cannot return until
it is safe for the the client's source data region to be modified.
We will discuss both implementations together.

\begin{Verbatim}
void gasnet_put_nb ( gasnet_node_t  target,
                     void*          dest,
                     void*          src,
                     size_t         nbytes);

void gasnet_put_nb_bulk ( void*          target,
                          gasnet_node_t  node,
                          void*          src,
                          size_t         nbytes);
\end{Verbatim}

The operation proceeds as follows:
\begin{enumerate}
\item A \verb+gasneti_eop+ object is allocated with handler \verb+gh+.

\item The source memory descriptor \verb+src_md+ is selected or created as follows:
  \begin{description}
  \item[INRAR] If the source memory region is contained in the in the local RAR, then
        \verb+src_md = RARAM_MD+ and \verb+l_offset+ is \verb+src-gasneti\_seginfo[gasnet\_mynode()].addr+.

  \item[BB] Otherwise, if \verb+nbytes <= GASNETI_PUTBB_MAX+, allocate a Chunk from 
        \verb+ReqSB+ at offset \verb+l_offset+. Copy {\it nbytes} of data from {\it src}
        to this Chunk. Set \verb+GASNETE_STATE_LOCAL_COMPLETE+ bit of \verb+gh+.

  \item[USERBUF] Otherwise the message is large and not in the local RAR.  Construct
        a temporary memory descriptor (\verb+src_md+) as specified in Table \ref{tab:TmpPutGet} 
        containing the users source memory region.  Set \verb+l_offset=0+.
  \end{description}

\item Construct the \verb+match_bits+ as shown for the {\it Put} operation in Table \ref{tab:Ops}
      with the compact 24-bit representation of \verb+gh+ stored in bits $[31:8]$.
      The least significant byte contains the least significant byte of \verb+RAR_BITS+
      (Table \ref{tab:MLE})
      to select the \verb+RAR_MD+ memory descriptor on the {\it target} node.
      Note that no events will be generated on the target node.

\item Construct the offset \verb+dest_offset+ as {\it dest - gasneti\_seginfo[target].addr}.

\item Issue the {\it PtlPutRegion()} call with arguments as shown in Table \ref{tab:GNPUT}

\item If this is the {\it bulk} version, return to the calling thread.

\item If we used a Chunk of \verb+ReqSB+ as a bounce buffer, the clients source buffer
  can safely be over-written so return to the calling thread.

\item Otherwise, poll the network by calling {\it gasnet\_AMPoll()} until the
  \verb+GASNETE_STATE_LOCAL_COMPLETE+ bit of the \verb+gh->flags+ field is set.
\end{enumerate}

\begin{table}[htdp]
\caption{Arguments of {\it PtlPutRegion()} used in {\it gasnet\_put\_nb()}}
\label{tab:GNPUT}
\begin{center}
\begin{tabular}{|c|c|p{10cm}|}
\hline
\multicolumn{3}{|c|}{ {\bf Argument of {\it PtlPutRegion()}} } \\
\hline
  {\bf Name}     & {\bf Value}          & {\bf Comment} \\ \hline
  md\_handle     & \verb+src_md_h+      & Handle of local \verb+src_md+ memory descriptor \\ \hline
  local\_offset  & \verb+l_offset+      & Offset of destination within \verb+src_md+ \\ \hline
  length         & $nbytes$             & Length of data payload (in bytes) \\ \hline
  ack\_req       & \verb+PTL_ACK_REQ+   & Request Acknowledgement Event \\ \hline
  target\_id     & {\it PtlID(target)}  & NID and PID of {\it target} node \\ \hline
  pt\_index      & \verb+GN_RAR_PTE+    & Target Portals Table index  \\ \hline
  ac\_index      & \verb+GN_AC+         & Target Portals Access Table index \\ \hline
  match\_bits    & {\it see below}      & Targeting ReqRB on {\it dest} \\ \hline
  remote\_offset & \verb+dest_offset+   & Offset of destination in target RAR \\ \hline
  hdr\_data      & 0                    & Not used \\ \hline
\hline
\multicolumn{3}{|c|}{ {\bf match\_bits argument of {\it PtlPutRegion()}} } \\
\hline
  {\bf Name}   &  {\bf Bit Range}  & {\bf Value} \\ \hline
  unused       &  [63:32]          & Not used \\ \hline
  AM\_Flag     &  [31:08]          & Compact representation of \verb+gasnet_handle+ \\ \hline
  Msg\_Type    &  [07:04]          & This is a {\it Put} Message \\ \hline
  m\_bits      &  [03:00]          & Lower 4 bits of \verb+RAR_BITS+ {\it Target MD} \\ \hline 
\end{tabular}
\end{center}
\end{table}

At some point in the future, events associated with this operation will be posted
to the event queue attached to \verb+src_md+.
The cases are:
\begin{description}
\item[INRAR] Here, \verb+src_md == RARAM_md+.  The \verb+SEND_END+ event will cause
  the \verb+GASNETE_STATE_LOCAL_COMPLETE+ bit to be set in the gasnet handle.
  The \verb+ACK+ event will cause the \verb+GASNET_STATE_COMPLETE+ bit to be set
  int the gasnet handle.  The handle is extracted from the compact representation
  stored in bits $[31:8]$ of the event \verb+match_bits+ field.

\item[BB] Here, \verb+src_md == ReqSB_md+.  If the event is:
  \begin{description}
  \item[SEND\_END] If the \verb+Msg_Type+ encoded in bits $[4:7]$ of the \verb+ev->match_bits+
    indicate this is a \verb+GASNETI_MSG_PUT+ (as opposed to an active message)
    Then this chunk is used as a bounce buffer for a \Put operation.
    we can release this chunk back to the \verb+ReqSB+ allocator.
  \item[ACK] This event on \verb+ReqSB_MD+ is only activated in the case of a {\it gasnet\_put*()}
    extract the \verb+gh+ from the \verb+match_bits+ and mark the handle \verb+GASNETE_STATE_COMPLETE+.
  \end{description}  
\item[USERBUF] In this case, \verb+src_md+ is a \verb+tmp_md+.  The \verb+SEND_END+ event
  represents local completion of the put operation, set the handle \verb+GASNETE_STATE_LOCAL_COMPLETE+
  bit of the handle.
  The \verb+ACK+ event represents remote completion, set the \verb+GASNETE_STATE_COMPLETE+
  bit of the handle.
  The handle is extracted from the compact representation
  stored in bits $[31:8]$ of the event \verb+match_bits+ field.
\end{description}

\section{The \Gn Memset Operations}
The \Gn memset operations have the effect of performing a {\it memset(dest,val,nbytes)}
on the {\it target} node.  The destination memory region $[dest:dest+nbytes-1]$ 
must lie entirely in the RAR on the target node.
The prototypes of the operations are:
\begin{Verbatim}
  void gasnet_memset(gasnet_node target, void *dest, int val, size_t nbytes);
  void gasnet_memset_nb(gasnet_node target, void *dest, int val, size_t nbytes);
  void gasnet_memset_nbi(gasnet_node target, void *dest, int val, size_t nbytes);
\end{Verbatim}
Note that synchronization of the {\it nbi} operation is handled as if it were
a {\it Put} operation.

We will use the existing extended-reference implementation based on {\it small} 
active messages for this set of operation.
That is, the initiator issues a short AM to the target with the address range
and value that need to be set.  The AM handler will execute a local POSIX memset call.
The AM reply will signal the operation completion.

One possible optimization in the case of a small region would be to memset
a region of memory on the initiator and {\it Put} it directly to the target
destination.  Receipt of an \verb+ACK+ event would signal completion.

\section{Implicit Access Regions}
The foundations for this section were covered in Section \ref{gasnet_handle}.
The \Gn specification does not allow a client to recursively nest access regions, but
the implementation does allow this, and it is used internally.
The \verb+current_iop+ pointer owned by a thread acts as a stack
of iops.  
A new iop is pushed onto the stack when an access region begins, and is popped
when it ends.

\begin{Verbatim}
void gasnet_begin_nbi_accessregion()
{
    // allocate a new iop and push it onto the stack
    // when allocated, the GASNETE_IS_IOP  flag is set and lists are empty.
    iop = get_new_iop();  
    iop->next = current_iop;
    current_iop = iop;
}

gasnet_handle_t gasnet_end_nbi_accessregion()
{
   iop = current_iop;
   current_iop = iop->next;
   return (gasnet_handle_t)iop;
}
\end{Verbatim}

\section{Synchronization of Non-Blocking Gasnet Operations}
Recall that even an explicit handle can either be a single eop, or an iop with
a list of put eops and get eops.


\subsection{Explicit Handle Synchronization}

We go through the gory details for these operations.
In the {\it wait} call, we wait (and poll) for the completion of each gasnet operation 
associated with the handle.
In the {\it try} call, we go through the list and reap all the complete entries.  
If all are done we mark the handle dead and return \verb+GASNET_OK+.
Otherwise, we return \verb+GASNET_ERR_NOT_READY+ to indicate the there are still
outstanding operations on the handle.

\begin{Verbatim}
void gasnete_wait_eop(gasnete_eop_t *eop)
{
   while ( !(eop->flag & GASNETE_STATE_COMPLETE) ) {
      // process events until it is
      gasnet_AMPoll();
   };
   gasnete_eop_free(eop);
}

void gasnet_wait_syncnb(gasnet_handle_t handle)
{
    while(gasnet_try_syncnb(handle) != GASNET_OK) {
        gasnet_AMPoll();
    }
}

int gasnet_try_syncnb (gasnet_handle_t handle)
{
    if (handle == GASNET_INVALID_HANDLE) return GASNET_OK;
    if (handle->flag & GASNETE_IS_EOP) {
        eop = (gasnete_eop_t*)handle;
        if (eop->flag & GASNETE_STATE_COMPLETE) {
           gasnete_eop_free(eop);
           return GASNET_OK;
        }
        return GASNET_ERR_NOT_READY;
    }
    if (handle->flag & GASNETE_IS_IOP) {
        iop = (gasnete_iop_t*)handle;
        done = true;
        /* Arrange to have oldest eops be at the head of the list
           (by inserting them on the tail) that way we can test
           and remove them in order of oldest to youngest
         */
        foreach (eop on iop->put or iop->get list) {
           if (eop->flag & GASNETE_STATE_COMPLETE) {
               remote eop from list;
               gasnete_eop_free(eop);
           } else {
               done = false;
           }
        }
        if (done) {
           gasnete_iop_free(iop);
           return GASNET_OK;
        }
        return GASNET_ERR_NOT_READY;
    }
    fatal_error();
}
\end{Verbatim}

The synchronization operations on arrays of handles are implemented in the
obvious way, by calling {\it gasnet\_wait\_syncnb} and {\it gasnet\_try\_syncnb}
on the individual element handles.
See the \Gn specification document for completion semantics.

The prototypes are:
\begin{Verbatim}
void gasnet_wait_syncnb_all(gasnet_handle_t *handles, size_t);
int  gasnet_try_syncnb_all(gasnet_handle_t *handles, size_t);
void gasnet_wait_syncnb_some(gasnet_handle_t *handles, size_t);
int  gasnet_try_syncnb_some(gasnet_handle_t *handles, size_t);
\end{Verbatim}

\subsection{Implicit Handle Synchronization}
The implicit handle synchronization functions are used to synchronize the completion of
implicit handle ({\it nbi}) operations that occur outside of an access region.
The operations act in a manner similar to the explicit handle synchronization functions
in the case where the handle corresponds to an iop and will not be reproduced here.

\begin{Verbatim}
void gasnet_wait_syncnbi_gets();
void gasnet_wait_syncnbi_puts();
void gasnet_wait_syncnbi_all();
int  gasnet_try_syncnbi_gets();
int  gasnet_try_syncnbi_puts();
int  gasnet_try_syncnbi_all();
\end{Verbatim}
The implementations will return a fatal error if called when \verb+(current_iop && current_iop->next)+,
indicating an access region has been entered.
The {\it get} versions will only synchronize on the \verb+get+ list of the current->iop.
The {\it put} versions will only synchronize on the \verb+put+ list of the current->iop.
The {\it all} versions will synchronize on both lists.

\section{\Gn Barriers}
The gasnet split-phase barrier operation has prototype:

\begin{Verbatim}
void gasnet_barrier_notify(int id, int flags);
int gasnet_barrier_wait(int id, int flags);
\end{Verbatim}

We will use the active message based implementation of split phase barriers
available in the extended reference implementation.
Both all-to-one and tree-based versions are implemented using Short
active messages.
Plans are underway to implement a more efficient {\it Put} based version.

% During the optimization phase, we will implement an alternate version that does not
% rely on active messages.  Each node would construct a memory descriptor \verb+barr_md+
% as specified in section \ref{resource:barrier}.  The master node would allocate
% an array of \verb+gasnete_barrier_t+ objects and the leaf nodes would allocate
% a single object.
% In the notification phase, each leaf node would {\it PtlPut} their \verb+id+ and
% \verb +flags+ value to the object at index {\it gasnet\_mynode()} of the master node.
% The event {\it Barrier\_EventHandler()} on the master would count the number of 
% \verb+PUT_END+ events on its \verb+barr_md+.
% When the master executes {\it gasnet\_barrier\_wait()}, it would poll until all
% \verb+gasnet_nodes()-1+ leaf nodes check in.  It will then process the 
% \verb+gasnete_barrier+ array to detect for mismatches, then {\it PtlPutRegion()}
% the resulting \verb+id+ and \verb+flags+ value back to each leaf node's \verb+barr_md+.
% When the leaf nodes execute {\it gasnet\_barrier\_wait()}, they will poll until
% the \verb+PUT_END+ event is posted on their \verb+barr_md+.
% Local state will also be maintained to insure only one {\it notify()} and {\it wait()}
% (or successful {\it try()}) operations are executed per barrier phase.
% If necessary, we will morph this implementation into a tree based implementation
% for scalability reasons.


