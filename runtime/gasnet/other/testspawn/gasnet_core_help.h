/*   $Source: runtime/gasnet/other/testspawn/gasnet_core_help.h $
 *     $Date: Wed, 05 Apr 2006 08:59:05 -0700 $
 * $Revision: 1.3 $
 * Description:
 * Copyright 2005, Regents of the University of California
 * Terms of use are as specified in license.txt
 */

#ifndef _IN_GASNET_H
  #error This file is not meant to be included directly- clients should include gasnet.h
#endif

#ifndef _GASNET_CORE_HELP_H
#define _GASNET_CORE_HELP_H

GASNETI_BEGIN_EXTERNC

#include <gasnet_help.h>

GASNETI_END_EXTERNC

#endif
