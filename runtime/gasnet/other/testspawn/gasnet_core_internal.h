/*   $Source: runtime/gasnet/other/testspawn/gasnet_core_internal.h $
 *     $Date: Wed, 02 Mar 2005 07:34:41 -0800 $
 * $Revision: 1.2 $
 * Description: 
 * Copyright 2005, Regents of the University of California
 * Terms of use are as specified in license.txt
 */

#ifndef _GASNET_CORE_INTERNAL_H
#define _GASNET_CORE_INTERNAL_H

#include <gasnet_internal.h>
#include <gasnet_bootstrap_internal.h>

#endif
