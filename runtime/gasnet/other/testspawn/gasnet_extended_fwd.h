/*   $Source: runtime/gasnet/other/testspawn/gasnet_extended_fwd.h $
 *     $Date: Wed, 02 Mar 2005 07:34:41 -0800 $
 * $Revision: 1.2 $
 * Description:
 * Copyright 2005, Regents of the University of California
 * Terms of use are as specified in license.txt
 */

#ifndef _IN_GASNET_H
  #error This file is not meant to be included directly- clients should include gasnet.h
#endif

#ifndef _GASNET_EXTENDED_FWD_H
#define _GASNET_EXTENDED_FWD_H

#define GASNET_EXTENDED_VERSION      1.4
#define GASNET_EXTENDED_VERSION_STR  _STRINGIFY(GASNET_EXTENDED_VERSION)
#define GASNET_EXTENDED_NAME         TESTSPAWN
#define GASNET_EXTENDED_NAME_STR     _STRINGIFY(GASNET_EXTENDED_NAME)

#define GASNET_BEGIN_FUNCTION() do {} while(0)

#endif

