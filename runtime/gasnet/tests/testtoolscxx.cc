/*   $Source: runtime/gasnet/tests/testtoolscxx.cc $
 *     $Date: Tue, 15 Aug 2006 16:43:25 -0700 $
 * $Revision: 1.1 $
 * Description: General GASNet correctness tests in C++
 * Copyright 2002, Dan Bonachea <bonachea@cs.berkeley.edu>
 * Terms of use are as specified in license.txt
 */

#include "testtools.c"
