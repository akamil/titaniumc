/*   $Source: runtime/gasnet/tests/testcxx.cc $
 *     $Date: Sat, 25 Feb 2006 04:37:06 -0800 $
 * $Revision: 1.3 $
 * Description: General GASNet correctness tests in C++
 * Copyright 2002, Dan Bonachea <bonachea@cs.berkeley.edu>
 * Terms of use are as specified in license.txt
 */
#include <gasnet.h>
#include <gasnet_vis.h>
#include <gasnet_coll.h>

#include "testgasnet.c"

