/*   $Source: runtime/gasnet/dcmf-conduit/gasnet_core_help.h $
 *     $Date: Fri, 04 Mar 2011 14:17:56 -0800 $
 * $Revision: 1.1 $
 * Description: GASNet dcmf conduit core Header Helpers (Internal code, not for client use)
 * Copyright 2008, Rajesh Nishtala <rajeshn@cs.berkeley.edu>
 *                 Dan Bonachea <bonachea@cs.berkeley.edu>
 * Terms of use are as specified in license.txt
 */

#ifndef _IN_GASNET_H
  #error This file is not meant to be included directly- clients should include gasnet.h
#endif

#ifndef _GASNET_CORE_HELP_H
#define _GASNET_CORE_HELP_H

GASNETI_BEGIN_EXTERNC

#include <gasnet_help.h>

GASNETI_END_EXTERNC

#endif
