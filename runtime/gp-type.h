#ifndef _INCLUDE_TITANIUM_GP_TYPE_H_
#define _INCLUDE_TITANIUM_GP_TYPE_H_

#include "backend-defines.h"
#include "primitives.h"


#ifdef WIDE_POINTERS

# define GP_type( referent )			\
  struct P ## referent {			\
    Box box;                          \
    referent *addr;				\
  }

#else
# define GP_type( referent )  referent *
#endif


#endif /* !_INCLUDE_TITANIUM_GP_TYPE_H_ */
