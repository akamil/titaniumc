#include <backend-defines.h>
#include <class_header.h>
#include <ti-gc.h>
#include <ti_array_methods.h>
#include <tally-memory.h>

#include "T6Object4lang4java.h"
#include "T10Checkpoint4lang2ti.h"
#include "layout!Pclass_header.h"
#include "layout!PLti5cdescmT6Object4lang4java.h"
#include "layout!PT6Object4lang4java.h"
#include "layout!PT10Checkpoint4lang2ti.h"

void ti_array_checkpoint(void *arr, jint length, jchar size, jchar eleminfo, LT10Checkpoint4lang2ti cp) {
  jbyte *rawbuf = (jbyte *) arr;
  PT10Checkpoint4lang2ti gcp;
  globalize(gcp, cp);

  // registration assumed to already be done

  if (eleminfo == TI_ATOMIC_INDEX)
    return;
  else if (eleminfo == TI_GLOBAL_OBJECT_INDEX) {
    PT6Object4lang4java *buf = (PT6Object4lang4java *) rawbuf;
    Lti5cdescmT6Object4lang4java cinfo;
    int i;
    for (i = 0; i < length; i++, buf++) {
      if (TO_LOCAL(*buf) != NULL) {
	CLASS_INFO_GLOBAL(cinfo, ti5cdescmT6Object4lang4java, *buf);
	cinfo->checkpoint(*buf, gcp, ((jboolean) 1));
      }
    }
  } else if (eleminfo == TI_LOCAL_OBJECT_INDEX) {
    LT6Object4lang4java *buf = (LT6Object4lang4java *) rawbuf;
    PT6Object4lang4java elt;
    Lti5cdescmT6Object4lang4java cinfo;
    int i;
    for (i = 0; i < length; i++, buf++) {
      if (*buf != NULL) {
	CLASS_INFO_LOCAL(cinfo, ti5cdescmT6Object4lang4java, *buf);
	TO_GLOBALB(elt, MYBOX, *buf);
	cinfo->checkpoint(elt, gcp, ((jboolean) 1));
      }
    }
  } else {
    cp_methods *methods = ti_get_checkpoint_methods(eleminfo);
    int i;
    for (i = 0; i < length; i++, rawbuf += size) {
      methods->checkpoint(rawbuf, cp);
    }
  }
}


void ti_array_restore(void *arr, jint length, jchar size, jchar eleminfo, LT10Checkpoint4lang2ti cp) {
  jbyte *rawbuf = (jbyte *) arr;

  if (eleminfo == TI_ATOMIC_INDEX)
    return;
  else if (eleminfo == TI_GLOBAL_OBJECT_INDEX) {
    PT6Object4lang4java *buf = (PT6Object4lang4java *) rawbuf;
    int i;
    for (i = 0; i < length; i++, buf++) {
      *buf = ml16unswizzle_globalPT6Object4lang4javamT10Checkpoint4lang2ti(cp, *buf);
    }
  } else if (eleminfo == TI_LOCAL_OBJECT_INDEX) {
    LT6Object4lang4java *buf = (LT6Object4lang4java *) rawbuf;
    int i;
    for (i = 0; i < length; i++, buf++) {
      *buf = ml15unswizzle_localLT6Object4lang4javamT10Checkpoint4lang2ti(cp, *buf);
    }
  } else {
    cp_methods *methods = ti_get_checkpoint_methods(eleminfo);
    int i;
    for (i = 0; i < length; i++, rawbuf += size) {
      methods->restore(rawbuf, cp);
    }
  }
}

