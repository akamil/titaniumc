#include <stdio.h>
#include <titanium.h>
#include <ti_array_flags.h>

#ifndef TI_ARRAY_CAST_SINGLE_MEMCPY
#define TI_ARRAY_CAST_SINGLE_MEMCPY 1
#endif

TI_INLINE(ti_cast_to_local)
ti_LOCAL_ARRAY ti_cast_to_local(const char *where, const ti_GLOBAL_ARRAY *g) {
#if BOUNDS_CHECKING && defined(MEMORY_DISTRIBUTED)
  if (GASNETT_PREDICT_FALSE(!isLocal(g->A))) {
    fprintf(stderr, "Attempt to cast global array to local "
	    "on processor %d failed at %s\n", MYPROC, where);
    fflush(stderr);
    abort();
  }
#endif

#if !defined(WIDE_POINTERS)
  assert(sizeof(ti_LOCAL_ARRAY) == sizeof(ti_GLOBAL_ARRAY));
  return *(ti_LOCAL_ARRAY*)g; /* no conversion necessary */
#else
 { ti_LOCAL_ARRAY l;
  l.A = TO_LOCAL(g->A);
  l.ancestor = TO_LOCAL(g->ancestor);

  #if TI_ARRAY_CAST_SINGLE_MEMCPY
    memcpy(&(l.domain), &(g->domain), ((char*)(g+1))-((char*)&(g->domain)));
  #else
    l.domain = g->domain;
    memcpy(l.stride,      g->stride,      sizeof(l.stride));
    memcpy(l.base,        g->base,        sizeof(l.base));
    memcpy(l.sideFactors, g->sideFactors, sizeof(l.sideFactors));
    #if DUMPABLE_ARRAYS
      l.dumpfn = g->dumpfn;
    #endif
    #if EXPLICITLY_STORE_ALLOCATION_SITE
      l.where = g->where;
    #endif
    #if EXPLICITLY_STORE_CREATOR 
      l.creator = g->creator;
    #endif
  #endif
  return l;
 }
#endif
}

TI_INLINE(ti_cast_to_global)
ti_GLOBAL_ARRAY ti_cast_to_global(const ti_LOCAL_ARRAY *l, int box) {
#if !defined(WIDE_POINTERS)
  assert(sizeof(ti_LOCAL_ARRAY) == sizeof(ti_GLOBAL_ARRAY));
  return *(ti_GLOBAL_ARRAY*)l; /* no conversion necessary */
#else
  ti_GLOBAL_ARRAY g;

  TO_GLOBALB(g.A, box, l->A);
  TO_GLOBALB(g.ancestor, box, l->ancestor);
  #if TI_ARRAY_CAST_SINGLE_MEMCPY
    memcpy(&(g.domain), &(l->domain), ((char*)(l+1))-((char*)&(l->domain)));
  #else
    g.domain = l->domain;
    memcpy(g.stride,      l->stride,      sizeof(g.stride));
    memcpy(g.base,        l->base,        sizeof(g.base));
    memcpy(g.sideFactors, l->sideFactors, sizeof(g.sideFactors));
    #if DUMPABLE_ARRAYS
      g.dumpfn = l->dumpfn;
    #endif
    #if EXPLICITLY_STORE_ALLOCATION_SITE
      g.where = l->where;
    #endif
    #if EXPLICITLY_STORE_CREATOR 
      g.creator = l->creator;
    #endif
  #endif
  return g;
#endif
}

