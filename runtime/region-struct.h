#ifndef __REGION_STRUCT
#define __REGION_STRUCT

#include "primitives.h"
#include "monitor.h"

struct ti5cdescmT6Region4lang2ti;
struct page;

struct ablock {
  char *base;
  jIntPointer allocfrom;
};

struct allocator {
  struct ablock page;
  struct ablock superpage;
  struct ablock hyperpage;
  struct page *pages;
  struct page *bigpages;
};

typedef struct T6Region4lang2ti {
  struct ti5cdescmT6Region4lang2ti *class_info;
  #ifdef HAVE_MONITORS
    tic_monitor_t monitor;
  #endif
  jIntPointer rc;
  struct allocator normal;
  struct allocator atomic;
  jIntPointer id;
  struct T6Region4lang2ti *previous;
  struct T6Region4lang2ti *next;
  jIntPointer bytes;
  void **finalize_list;
} T6Region4lang2ti;


#endif /* !__REGION_STRUCT */
