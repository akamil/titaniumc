#ifndef _INCLUDE_EXCEPTIONS_H_
#define _INCLUDE_EXCEPTIONS_H_

#include <setjmp.h>
#include "ti_config.h"
#include "instance_header.h"
#include "ti-memory.h"


#define SETJMP setjmp
#define LONGJMP longjmp

struct class_header;
struct instance_header;

#if defined(__INTEL_COMPILER) && defined(__ia64__)
  #define TI_EXNSTATE_ALIGN __declspec(align(32))
#else
  #define TI_EXNSTATE_ALIGN 
#endif

typedef struct ExceptionState {
    /* DOB: jmp_buf is often large and has special alignment constraints
     * placing it later in the struct causes bus errors on IA64 Intel C 7.0
     */
    jmp_buf handler;
    GP_instance exn_instance;
    struct ExceptionState *prior;
    int handling;
    int exn_isuniversal;
} ti_ExceptionState;

#define EXCEPTION_STATE_DECLARE(name) TI_EXNSTATE_ALIGN ti_ExceptionState name

extern TI_EXNSTATE_ALIGN ti_ExceptionState *PERPROC_DEF(exception_state);

#define NO_HANDLER -1

typedef struct {
  const struct class_header *catch_class;
  const char *catch_where;
  uint8_t catch_isuniversal;
  uint8_t catch_uncheckedAllowed;
} ti_ExceptionHandler;

int find_handler( const ti_ExceptionState * const,
		  const ti_ExceptionHandler [] );

/* Toss a non-universal exception */
void toss( const GP_instance exception ) __attribute__((noreturn));
/* Toss a universal exception */
void toss_universal( const GP_instance exception ) __attribute__((noreturn));
/* Re-toss an exception at the end of a catch list to continue outward propagation */
void retoss( ti_ExceptionState * ) __attribute__((noreturn));

#endif
