#ifndef _INCLUDE_MEM_LOCAL_H_
#define _INCLUDE_MEM_LOCAL_H_

#include "fence.h"

struct class_header;


/* Assign coding:
   ASSIGN is used for fields and array references, not for local vars
   [WEAK_][S][PL|PG]ASSIGN_[LOCAL|GLOBAL]:
   S: destination is a static field
   LOCAL: destination is accessed by a local pointer
   GLOBAL: destination is accessed by a global pointer
   PL: value is a local reference
   PG: value is a global reference
   WEAK_: "weak" assignment

   The arguments to assign must be evaluable multiple times.
   */   

#define DEREF_LOCAL(val, ptr)			do { (val) = *(ptr); } while (0)
#define ASSIGN_LOCAL(ptr, val)			do { *(ptr) = (val); } while (0)

#define DEREF_LOCAL_jboolean         DEREF_LOCAL
#define DEREF_LOCAL_jbyte            DEREF_LOCAL
#define DEREF_LOCAL_jchar            DEREF_LOCAL
#define DEREF_LOCAL_jdouble          DEREF_LOCAL
#define DEREF_LOCAL_jfloat           DEREF_LOCAL
#define DEREF_LOCAL_jint             DEREF_LOCAL
#define DEREF_LOCAL_jlong            DEREF_LOCAL
#define DEREF_LOCAL_jshort           DEREF_LOCAL
#define DEREF_LOCAL_lp               DEREF_LOCAL
#define DEREF_LOCAL_gp               DEREF_LOCAL
#define DEREF_LOCAL_bulk             DEREF_LOCAL
#define DEREF_LOCAL_bulk_noptrs      DEREF_LOCAL

#define ASSIGN_LOCAL_jboolean       ASSIGN_LOCAL
#define ASSIGN_LOCAL_jbyte          ASSIGN_LOCAL
#define ASSIGN_LOCAL_jchar          ASSIGN_LOCAL
#define ASSIGN_LOCAL_jdouble        ASSIGN_LOCAL
#define ASSIGN_LOCAL_jfloat         ASSIGN_LOCAL
#define ASSIGN_LOCAL_jint           ASSIGN_LOCAL
#define ASSIGN_LOCAL_jlong          ASSIGN_LOCAL
#define ASSIGN_LOCAL_jshort         ASSIGN_LOCAL
#define ASSIGN_LOCAL_lp             ASSIGN_LOCAL
#define ASSIGN_LOCAL_gp             ASSIGN_LOCAL
#define ASSIGN_LOCAL_bulk           ASSIGN_LOCAL


#ifdef NOMEMCHECKS

#define PLASSIGN_LOCAL		ASSIGN_LOCAL
#define PGASSIGN_LOCAL		ASSIGN_LOCAL
#define SPLASSIGN_LOCAL		ASSIGN_LOCAL
#define SPGASSIGN_LOCAL		ASSIGN_LOCAL

#else

#ifdef ENABLE_RC

#define RC_PLASSIGN_LOCAL(ptr, ptrRegion, val, valRegion) \
    { \
      RegionId oldValRegion; \
      PLSWAP_LOCAL(ptr, val, oldValRegion); \
      if (oldValRegion != valRegion) { \
	  if (oldValRegion != ptrRegion) oldValRegion->rc--; \
	  if (valRegion != ptrRegion) valRegion->rc++; \
	} \
    }

#define RC_PGASSIGN_LOCAL(ptr, ptrRegion, val, valRegion) \
    { \
      RegionId oldValRegion; \
      PGSWAP_LOCAL(ptr, val, oldValRegion); \
      if (oldValRegion != valRegion) { \
	  if (oldValRegion != ptrRegion) oldValRegion->rc--; \
	  if (valRegion != ptrRegion) valRegion->rc++; \
	} \
    }

#define SPLASSIGN_LOCAL(ptr, val) \
  do { \
    RegionId oldValRegion = PL2RegionId(*(ptr)); \
    RegionId valRegion = PL2RegionId(val); \
    if (oldValRegion != valRegion) \
      { \
	oldValRegion->rc--; \
	valRegion->rc++; \
      } \
    ASSIGN_LOCAL(ptr, val); \
  } while (0)

#define SPGASSIGN_LOCAL(ptr, val) \
  do { \
    RegionId oldValRegion = PG2RegionId(*(ptr)); \
    RegionId valRegion = PG2RegionId(val); \
    if (oldValRegion != valRegion) \
      { \
	oldValRegion->rc--; \
	valRegion->rc++; \
      } \
    ASSIGN_LOCAL(ptr, val); \
  } while (0)

#else

#define RC_PLASSIGN_LOCAL(ptr, ptrRegion, val, valRegion) ASSIGN_LOCAL(ptr, val)
#define RC_PGASSIGN_LOCAL(ptr, ptrRegion, val, valRegion) ASSIGN_LOCAL(ptr, val)
#define SPLASSIGN_LOCAL(ptr, val) ASSIGN_LOCAL(ptr, val)
#define SPGASSIGN_LOCAL(ptr, val) ASSIGN_LOCAL(ptr, val)

#endif

#define PLASSIGN_LOCAL(ptr, val)                                                      \
  do {                                                                                \
    void *_val = (void *)(val);                                                       \
    void **_ptr = (void **)(ptr);                                                     \
    if (!SHARED_REGIONID(PL2RegionId(_val))      /* pointing into a private region */ \
        && SHARED_REGIONID(PL2RegionId(_ptr)))   /* from a shared region */           \
      sharingViolation(__current_loc);          /* is a bad thing */                  \
                                                                                      \
    RC_PLASSIGN_LOCAL(_ptr, ptrRegion, _val, valRegion);                              \
  } while (0)

#define PGASSIGN_LOCAL(ptr, val)                                                      \
  do {                                                                                \
    jGPointer _val = CAST_GPTR(val);                                                  \
    jGPointer *_ptr = (jGPointer *)(ptr);                                             \
    if (!SHARED_REGIONID(PG2RegionId(_val))      /* pointing into a private region */ \
        && SHARED_REGIONID(PL2RegionId(_ptr)))   /* from a shared region */           \
      sharingViolation(__current_loc);          /* is a bad thing */                  \
                                                                                      \
    RC_PGASSIGN_LOCAL(_ptr, ptrRegion, _val, valRegion);                              \
  } while (0)

#endif

#define WEAK_ASSIGN_LOCAL			ASSIGN_LOCAL
#define WEAK_PLASSIGN_LOCAL			PLASSIGN_LOCAL
#define WEAK_PGASSIGN_LOCAL			PGASSIGN_LOCAL
#define WEAK_SPLASSIGN_LOCAL			SPLASSIGN_LOCAL
#define WEAK_SPGASSIGN_LOCAL			SPGASSIGN_LOCAL

#define FIELD_ADDR_LOCAL(addr, ptr, fld)	((addr) = &((ptr)->fld))
#define IFACE_DISPATCH_LOCAL(ci, mid)          (((struct class_header *)(ci))->intf_info[mid])
#define INDEX_LOCAL(element, base, offset)	((element) = (base) + (offset))
#define SUM_LOCAL(type, element, base, offset)	((element) = (type)(((char *)(base)) + (offset)))
#define EQUAL_LOCAL(p, q)			((p) == (q))
#define ISNULL_LOCAL(p)			        ((p) == NULL)


#define CLASS_INFO_LOCAL(val, type, obj)	((val) = (obj)->class_info, assert((val) != NULL))


#endif /* !_INCLUDE_MEM_LOCAL_H_ */
