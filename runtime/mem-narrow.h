#ifndef _INCLUDE_MEM_NARROW_H_
#define _INCLUDE_MEM_NARROW_H_


#include <string.h>
#include "mem-local.h"

struct instance_header;


#define DEREF_GLOBAL_jboolean(val, ptr)		DEREF_LOCAL(val, ((jboolean *)(ptr)))
#define DEREF_GLOBAL_jbyte(val, ptr)		DEREF_LOCAL(val, ((jbyte *)(ptr)))
#define DEREF_GLOBAL_jchar(val, ptr)		DEREF_LOCAL(val, ((jchar *)(ptr)))
#define DEREF_GLOBAL_jdouble(val, ptr)		DEREF_LOCAL(val, ((jdouble *)(ptr)))
#define DEREF_GLOBAL_jfloat(val, ptr)		DEREF_LOCAL(val, ((jfloat *)(ptr)))
#define DEREF_GLOBAL_jint(val, ptr)		DEREF_LOCAL(val, ((jint *)(ptr)))
#define DEREF_GLOBAL_jlong(val, ptr)		DEREF_LOCAL(val, ((jlong *)(ptr)))
#define DEREF_GLOBAL_jshort(val, ptr)		DEREF_LOCAL(val, ((jshort *)(ptr)))
#define DEREF_GLOBAL_lp(val, ptr) 	        DEREF_LOCAL(val, ((void **)(ptr)))
#define DEREF_GLOBAL_gp(val, ptr)	        DEREF_LOCAL(*(jGPointer *)(&(val)), ((jGPointer *)(ptr)))
#define DEREF_GLOBAL_bulk(val, ptr)	        memcpy(&(val), ptr, sizeof(val))
#define DEREF_GLOBAL_bulk_noptrs                DEREF_GLOBAL_bulk

#define ASSIGN_GLOBAL_jboolean(gptr, val)	ASSIGN_LOCAL((jboolean *)gptr, val)
#define ASSIGN_GLOBAL_jbyte(gptr, val)		ASSIGN_LOCAL((jbyte *)gptr, val)
#define ASSIGN_GLOBAL_jchar(gptr, val)		ASSIGN_LOCAL((jchar *)gptr, val)
#define ASSIGN_GLOBAL_jdouble(gptr, val)	ASSIGN_LOCAL((jdouble *)gptr, val)
#define ASSIGN_GLOBAL_jfloat(gptr, val)		ASSIGN_LOCAL((jfloat *)gptr, val)
#define ASSIGN_GLOBAL_jint(gptr, val)		ASSIGN_LOCAL((jint *)gptr, val)
#define ASSIGN_GLOBAL_jlong(gptr, val)		ASSIGN_LOCAL((jlong *)gptr, val)
#define ASSIGN_GLOBAL_jshort(gptr, val)		ASSIGN_LOCAL((jshort *)gptr, val)
#define ASSIGN_GLOBAL_lp(gptr, val) 		ASSIGN_LOCAL((void **)gptr, val)
#define ASSIGN_GLOBAL_gp(gptr, val)		ASSIGN_LOCAL(((jGPointer *)TO_LOCAL(gptr)), val)
#define ASSIGN_GLOBAL_bulk(gptr, val)		memcpy(gptr, &val, sizeof(val))

#define WEAK_ASSIGN_GLOBAL_jboolean	WEAK_ASSIGN_LOCAL
#define WEAK_ASSIGN_GLOBAL_jbyte	WEAK_ASSIGN_LOCAL
#define WEAK_ASSIGN_GLOBAL_jchar	WEAK_ASSIGN_LOCAL
#define WEAK_ASSIGN_GLOBAL_jdouble	WEAK_ASSIGN_LOCAL
#define WEAK_ASSIGN_GLOBAL_jfloat	WEAK_ASSIGN_LOCAL
#define WEAK_ASSIGN_GLOBAL_jint		WEAK_ASSIGN_LOCAL
#define WEAK_ASSIGN_GLOBAL_jlong	WEAK_ASSIGN_LOCAL
#define WEAK_ASSIGN_GLOBAL_jshort	WEAK_ASSIGN_LOCAL
#define WEAK_ASSIGN_GLOBAL_lp 		WEAK_ASSIGN_LOCAL
#define WEAK_ASSIGN_GLOBAL_gp		WEAK_ASSIGN_LOCAL
#define WEAK_ASSIGN_GLOBAL_bulk		WEAK_ASSIGN_LOCAL

#define PGASSIGN_GLOBAL_jboolean	PGASSIGN_LOCAL
#define PGASSIGN_GLOBAL_jbyte		PGASSIGN_LOCAL
#define PGASSIGN_GLOBAL_jchar		PGASSIGN_LOCAL
#define PGASSIGN_GLOBAL_jdouble		PGASSIGN_LOCAL
#define PGASSIGN_GLOBAL_jfloat		PGASSIGN_LOCAL
#define PGASSIGN_GLOBAL_jint		PGASSIGN_LOCAL
#define PGASSIGN_GLOBAL_jlong		PGASSIGN_LOCAL
#define PGASSIGN_GLOBAL_jshort		PGASSIGN_LOCAL
#define PGASSIGN_GLOBAL_lp 		PGASSIGN_LOCAL
#define PGASSIGN_GLOBAL_gp		PGASSIGN_LOCAL
#define PGASSIGN_GLOBAL_bulk		PGASSIGN_LOCAL

#define SPLASSIGN_GLOBAL_jboolean	SPLASSIGN_LOCAL
#define SPLASSIGN_GLOBAL_jbyte		SPLASSIGN_LOCAL
#define SPLASSIGN_GLOBAL_jchar		SPLASSIGN_LOCAL
#define SPLASSIGN_GLOBAL_jdouble	SPLASSIGN_LOCAL
#define SPLASSIGN_GLOBAL_jfloat		SPLASSIGN_LOCAL
#define SPLASSIGN_GLOBAL_jint		SPLASSIGN_LOCAL
#define SPLASSIGN_GLOBAL_jlong		SPLASSIGN_LOCAL
#define SPLASSIGN_GLOBAL_jshort		SPLASSIGN_LOCAL
#define SPLASSIGN_GLOBAL_lp 		SPLASSIGN_LOCAL
#define SPLASSIGN_GLOBAL_gp		SPLASSIGN_LOCAL
#define SPLASSIGN_GLOBAL_bulk		SPLASSIGN_LOCAL

#define SPGASSIGN_GLOBAL_jboolean	SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_jbyte		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_jchar		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_jdouble	SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_jfloat		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_jint		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_jlong		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_jshort		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_lp 		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_gp		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_bulk		SPGASSIGN_LOCAL

#define WEAK_PLASSIGN_GLOBAL_jboolean	WEAK_PLASSIGN_LOCAL
#define WEAK_PLASSIGN_GLOBAL_jbyte	WEAK_PLASSIGN_LOCAL
#define WEAK_PLASSIGN_GLOBAL_jchar	WEAK_PLASSIGN_LOCAL
#define WEAK_PLASSIGN_GLOBAL_jdouble	WEAK_PLASSIGN_LOCAL
#define WEAK_PLASSIGN_GLOBAL_jfloat	WEAK_PLASSIGN_LOCAL
#define WEAK_PLASSIGN_GLOBAL_jint	WEAK_PLASSIGN_LOCAL
#define WEAK_PLASSIGN_GLOBAL_jlong	WEAK_PLASSIGN_LOCAL
#define WEAK_PLASSIGN_GLOBAL_jshort	WEAK_PLASSIGN_LOCAL
#define WEAK_PLASSIGN_GLOBAL_lp 	WEAK_PLASSIGN_LOCAL
#define WEAK_PLASSIGN_GLOBAL_gp		WEAK_PLASSIGN_LOCAL
#define WEAK_PLASSIGN_GLOBAL_bulk	WEAK_PLASSIGN_LOCAL


#define WEAK_PGASSIGN_GLOBAL_jboolean	WEAK_PGASSIGN_LOCAL
#define WEAK_PGASSIGN_GLOBAL_jbyte	WEAK_PGASSIGN_LOCAL
#define WEAK_PGASSIGN_GLOBAL_jchar	WEAK_PGASSIGN_LOCAL
#define WEAK_PGASSIGN_GLOBAL_jdouble	WEAK_PGASSIGN_LOCAL
#define WEAK_PGASSIGN_GLOBAL_jfloat	WEAK_PGASSIGN_LOCAL
#define WEAK_PGASSIGN_GLOBAL_jint	WEAK_PGASSIGN_LOCAL
#define WEAK_PGASSIGN_GLOBAL_jlong	WEAK_PGASSIGN_LOCAL
#define WEAK_PGASSIGN_GLOBAL_jshort	WEAK_PGASSIGN_LOCAL
#define WEAK_PGASSIGN_GLOBAL_lp 	WEAK_PGASSIGN_LOCAL
#define WEAK_PGASSIGN_GLOBAL_gp		WEAK_PGASSIGN_LOCAL
#define WEAK_PGASSIGN_GLOBAL_bulk	WEAK_PGASSIGN_LOCAL

#define WEAK_SPLASSIGN_GLOBAL_jboolean	WEAK_SPLASSIGN_LOCAL
#define WEAK_SPLASSIGN_GLOBAL_jbyte	WEAK_SPLASSIGN_LOCAL
#define WEAK_SPLASSIGN_GLOBAL_jchar	WEAK_SPLASSIGN_LOCAL
#define WEAK_SPLASSIGN_GLOBAL_jdouble	WEAK_SPLASSIGN_LOCAL
#define WEAK_SPLASSIGN_GLOBAL_jfloat	WEAK_SPLASSIGN_LOCAL
#define WEAK_SPLASSIGN_GLOBAL_jint	WEAK_SPLASSIGN_LOCAL
#define WEAK_SPLASSIGN_GLOBAL_jlong	WEAK_SPLASSIGN_LOCAL
#define WEAK_SPLASSIGN_GLOBAL_jshort	WEAK_SPLASSIGN_LOCAL
#define WEAK_SPLASSIGN_GLOBAL_lp 	WEAK_SPLASSIGN_LOCAL
#define WEAK_SPLASSIGN_GLOBAL_gp	WEAK_SPLASSIGN_LOCAL
#define WEAK_SPLASSIGN_GLOBAL_bulk	WEAK_SPLASSIGN_LOCAL

#define SPGASSIGN_GLOBAL_jboolean	SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_jbyte		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_jchar		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_jdouble	SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_jfloat		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_jint		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_jlong		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_jshort		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_lp 		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_gp		SPGASSIGN_LOCAL
#define SPGASSIGN_GLOBAL_bulk		SPGASSIGN_LOCAL

 
#define FIELD_ADDR_GLOBAL	FIELD_ADDR_LOCAL
#define IFACE_DISPATCH_GLOBAL	IFACE_DISPATCH_LOCAL
#define INDEX_GLOBAL		INDEX_LOCAL
#define SUM_GLOBAL		SUM_LOCAL
#define EQUAL_GLOBAL		EQUAL_LOCAL
#define ISNULL_GLOBAL		ISNULL_LOCAL

#define CLASS_INFO_GLOBAL	CLASS_INFO_LOCAL


#define TO_GLOBALP(global, proc, local)	globalize(global, local)
#define TO_GLOBALB(global, box,  local)	globalize(global, local)
#define TO_LOCAL(ptr)			(ptr)
#define TO_BOX(ptr)			(0)
#define TO_PROC(ptr)			(0)

#define TO_GLOBALP_STATIC TO_GLOBALP
#define TO_GLOBALB_STATIC TO_GLOBALB
#define TO_LOCAL_STATIC TO_LOCAL

#define isLocal(ptr)			(1)
#define isDirectlyAddressable(ptr)	(1)
#define localize(local, global)		do { (local) = (global); } while (0)
#define globalize(global, local)	do { (global) = (local); } while (0)


#define local_to_global_copy( local, global, nitems )  memcpy( (global), (local), sizeof(*(local)) * (nitems) )
#define global_to_local_copy( global, local, nitems )  memcpy( (local), (global), sizeof(*(local)) * (nitems) )


#endif /* !_INCLUDE_MEM_NARROW_H_ */
