#ifndef _INCLUDE_JAVA_ARRAY_METHODS_H_
#define _INCLUDE_JAVA_ARRAY_METHODS_H_

#include "java_array.h"
#include "native-utils.h"
#include "regions.h"
#include "class_header.h"
#include "java_string.h"
#include "layout!PTAjbyte.h"
#include "layout!LT10Checkpoint4lang2ti.h"
#include "layout!PT10Checkpoint4lang2ti.h"

extern class_header *java_array_desc;

/* external helper functions */
void m35throwArrayIndexOutOfBoundsExceptionIIILT6String4lang4javamT11NativeUtils4lang2ti
     ( jint, jint, jint, LP_JString ) __attribute__((noreturn));

/* internal helper functions */
java_array_header *java_array_alloc( Region, int length, int elementSize, jchar eleminfo, int isShared, const void *pDefaultElt );
java_array_header *java_array_build(         int length, int elementSize, void *data, jchar eleminfo, int isShared );
void java_array_free( java_array_header *array );
int java_array_subclass_of_local(  LT6Object4lang4java sourceObject, int elementSize, int atomicelements);
int java_array_subclass_of_global( PT6Object4lang4java sourceObject, int elementSize, int atomicelements);
int is_java_array_global( PT6Object4lang4java object );
int is_java_array_local(  LT6Object4lang4java object );
void java_local_byte_array_write(void *src, TAjbyte *b, jint off, jint len); 
void java_byte_array_write(void *src, PTAjbyte b, jint off, jint len); 
void java_local_byte_array_read(void *src, TAjbyte *b, jint off, jint len); 
void java_byte_array_read(void *src, PTAjbyte b, jint off, jint len); 

/* checkpoint methods */
void java_array_checkpoint(PT6Object4lang4java, PT10Checkpoint4lang2ti, jboolean);
void java_array_restore(LT6Object4lang4java, LT10Checkpoint4lang2ti);

#define JAVA_ARRAY_INIT_( result, init )  *((java_array_header **) &(result)) = (init)

#ifdef HAVE_MONITORS
  #define JAVA_ARRAY_INIT_MON(Larray) monitor_init(&((Larray)->header.monitor))
#else
  #define JAVA_ARRAY_INIT_MON(Larray) ((void)0)
#endif

#define JAVA_ARRAY_INIT_FIELDS(Larray, arraylength, ElementSize, eleminfo) do { \
  (Larray)->header.class_info = java_array_desc;  \
  (Larray)->length = (arraylength);               \
  (Larray)->size = (ElementSize);                 \
  (Larray)->elem_info = eleminfo;                 \
  JAVA_ARRAY_INIT_MON(Larray);                    \
  } while (0)
  
/* allocate an uninitialized java array */
#define JAVA_ARRAY_ALLOC( result, region, length, ElementType, eleminfo, isShared, pDefaultElt ) \
  (ti_srcpos(), JAVA_ARRAY_INIT_( result, java_array_alloc( (region), (length), sizeof( ElementType ), eleminfo, isShared, pDefaultElt ) ) )

/* allocate a java array and initialize it to given data */
#define JAVA_ARRAY_BUILD( result, length, data, eleminfo, isShared ) \
  (ti_srcpos(), JAVA_ARRAY_INIT_( result, java_array_build( (length), sizeof(*data), data, eleminfo, isShared ) ) )

/* explicitly free a java array allocated in REGION ZERO ONLY */
#define JAVA_ARRAY_FREE( array ) java_array_free(*((java_array_header **)&(array)))

#define JAVA_ARRAY_FIELD_( form, result, array, fieldName, fieldType, primType )	\
  do {									\
    fieldType fieldAddr_;						\
    									\
    CHECK_NULL_ ## form ## _IFBC( array, "in JavaArray."#fieldName" access" ); \
    									\
    FIELD_ADDR_ ## form ( fieldAddr_, array, header.fieldName );	\
    FENCE_PRE_READ();							\
    DEREF_ ## form ##_ ## primType ( result, fieldAddr_ );		\
    FENCE_POST_READ();							\
  } while (0)

/* fetch all the interesting attributes of a Java Array,
   in at most a single communication operation (for the GLOBAL case)
*/
#define JAVA_ARRAY_INFO_( form, length_result, size_result, info_result, array ) do {    \
    java_array_header hdr_;                                             \
    CHECK_NULL_ ## form ## _IFBC( array, "in JavaArray.info access" );  \
    FENCE_PRE_READ();							\
    DEREF_ ## form ##_bulk_noptrs( hdr_, array );		        \
    FENCE_POST_READ();							\
    (length_result) = hdr_.length;                                      \
    (size_result) = hdr_.size;                                          \
    (info_result) = hdr_.elem_info;                                     \
  } while (0)

#define JAVA_ARRAY_LENGTH_GLOBAL( result, array )  JAVA_ARRAY_FIELD_( GLOBAL, result, array, length, Pjint, jint )
#define JAVA_ARRAY_LENGTH_LOCAL(  result, array )  JAVA_ARRAY_FIELD_( LOCAL,  result, array, length, jint*, jint )
#define JAVA_ARRAY_SIZE_GLOBAL(   result, array )  JAVA_ARRAY_FIELD_( GLOBAL, result, array, size,   Pjint, jint )
#define JAVA_ARRAY_SIZE_LOCAL(    result, array )  JAVA_ARRAY_FIELD_( LOCAL,  result, array, size,   jint*, jint )
#define JAVA_ARRAY_ISATOMIC_GLOBAL(   result, array ) do {                     \
    jchar eleminfo_;                                                           \
    JAVA_ARRAY_FIELD_( GLOBAL, eleminfo_, array, elem_info, Pjshort, jshort ); \
    result = (eleminfo_ == TI_ATOMIC_INDEX);                                   \
  } while (0)
#define JAVA_ARRAY_ISATOMIC_LOCAL(   result, array ) do {                      \
    jchar eleminfo_;                                                           \
    JAVA_ARRAY_FIELD_( LOCAL, eleminfo_, array, elem_info, jshort *, jshort ); \
    result = (eleminfo_ == TI_ATOMIC_INDEX);                                   \
  } while (0)
#define JAVA_ARRAY_INFO_GLOBAL( length, size, eleminfo, array ) \
        JAVA_ARRAY_INFO_( GLOBAL, length, size, eleminfo, array )
#define JAVA_ARRAY_INFO_LOCAL( length, size, eleminfo, array ) \
        JAVA_ARRAY_INFO_( LOCAL, length, size, eleminfo, array )

#define JAVA_ARRAY_SUBCLASS_OF_GLOBAL(sourceObject, targetElemType, targetElemAtomic) \
  java_array_subclass_of_global(*(PT6Object4lang4java*)&(sourceObject), sizeof(targetElemType), targetElemAtomic)

#define JAVA_ARRAY_SUBCLASS_OF_LOCAL(sourceObject, targetElemType, targetElemAtomic) \
  java_array_subclass_of_local(*(LT6Object4lang4java*)&(sourceObject), sizeof(targetElemType), targetElemAtomic)

#define IS_JAVA_ARRAY_GLOBAL( object ) is_java_array_global( *(PT6Object4lang4java*)&(object) )
#define IS_JAVA_ARRAY_LOCAL( object )  is_java_array_local(  *(LT6Object4lang4java*)&(object) )

#if BOUNDS_CHECKING
#define JAVA_ARRAY_LENCHECK( arraylen, offset, count, where ) do { \
    if_pf ((offset < 0) | ((offset+count) > arraylen)) {	   \
      m35throwArrayIndexOutOfBoundsExceptionIIILT6String4lang4javamT11NativeUtils4lang2ti( \
        arraylen, offset, count,                                   \
        java_string_build_8( __current_loc_ext(where) ));          \
      abort(); /* never reach here */		                   \
    }								   \
  } while (0)
#define JAVA_ARRAY_CHECK_( form, array, offset, count, where ) do { \
    jint safe_;							    \
    JAVA_ARRAY_LENGTH_ ## form ( safe_, (array) );		    \
    JAVA_ARRAY_LENCHECK( safe_, offset, count, where );             \
  } while (0)  
#else
#define JAVA_ARRAY_CHECK_( form, array, offset, count, where ) ((void)0)
#define JAVA_ARRAY_LENCHECK( arraylen, offset, count, where ) ((void)0)
#endif /* BOUNDS_CHECKING */

#define JAVA_ARRAY_CHECK_GLOBAL( array, offset, count, where )  \
        JAVA_ARRAY_CHECK_( GLOBAL, array, offset, count, where )
#define JAVA_ARRAY_CHECK_LOCAL(  array, offset, count, where )  \
        JAVA_ARRAY_CHECK_( LOCAL,  array, offset, count, where )

#endif /* !_INCLUDE_JAVA_ARRAY_METHODS_H_ */
