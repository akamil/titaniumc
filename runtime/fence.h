#ifndef _INCLUDE_FENCE_H_
#define _INCLUDE_FENCE_H_


#if defined(SEQUENTIAL_CONSISTENCY) && defined(MEMORY_SHARED)

# if defined(__GNUC__)
#  define fence_asm(code) __asm__ __volatile__ (code : : : "memory")
# elif defined(__SUNPRO_C)
#  define fence_asm(code) asm(code)
# elif defined(__xlC__)
#  pragma mc_func fence_sync { "7c0004ac" }
#  define fence_asm(code) fence_sync() /* code must be "sync" */
# else
#  error "don't know how to inject inline assembly using this compiler"
# endif/* compiler */

# if defined(__i386)
#  define FENCE_PRE_READ() fence_asm("lock; addl $0,0(%%esp)")
#  define FENCE_POST_WRITE() fence_asm("")
# elif defined(__sparc)
#  define FENCE_PRE_READ() fence_asm("")
#  define FENCE_POST_WRITE() fence_asm("fence #StoreLoad")
# elif defined(_POWER) || defined(_POWERPC)
#   define FENCE_BETWEEN() fence_asm("sync")
# else
#  error "don't know how to enforce memory ordering on this architecture"
# endif /* cpu */
#endif /* sequentially consistent shared memory */


#ifdef FENCE_BETWEEN
# ifdef FENCE_BETWEEN_IS_PRE
#  define FENCE_PRE_READ() FENCE_BETWEEN()
#  define FENCE_PRE_WRITE() FENCE_BETWEEN()
# else /* between is post */
#  define FENCE_POST_READ() FENCE_BETWEEN()
#  define FENCE_POST_WRITE() FENCE_BETWEEN()
# endif /* between is post */
#endif /* FENCE_BETWEEN */


#ifndef FENCE_PRE_READ
# define FENCE_PRE_READ()
#endif

#ifndef FENCE_POST_READ
# define FENCE_POST_READ()
#endif

#ifndef FENCE_PRE_WRITE
# define FENCE_PRE_WRITE()
#endif

#ifndef FENCE_POST_WRITE
# define FENCE_POST_WRITE()
#endif


#endif /* !_INCLUDE_FENCE_H_ */
