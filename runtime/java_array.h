#ifndef _INCLUDE_JAVA_ARRAY_H_
#define _INCLUDE_JAVA_ARRAY_H_

#include "java_array_header.h"


#define JA_type( element )			\
struct JA_ ## element {				\
  struct java_array_header header;		\
  element data[1];				\
}


#endif
