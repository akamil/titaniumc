/* ti_array.h */
/* This file assumes T is a type and N is an arity. */

#include "runtime-options.h"
#include "native-utils.h"
#include "T6Handle4lang2ti.h"

/* Instructions on how to add a method are in ti_array.c */

#define N_LARGER_THAN_1 N_MINUS_1

#ifndef GLOBAL_ARRAY
#define GLOBAL_ARRAY 1
#endif

#ifndef DEBUG_ARRAYS
#define DEBUG_ARRAYS 0
#endif

#if GLOBAL_ARRAY
  #define LOCAL_PART(p)	(TO_LOCAL(p))
  #define BOX_PART(p)	(TO_BOX(p))
  #define PROC_PART(p)	(TO_PROC(p))
  #define INDEX		INDEX_GLOBAL
  #define EQUAL(ptr1,ptr2) EQUAL_GLOBAL((ptr1),(ptr2))
#else
  #define LOCAL_PART(p) (p)
  #define BOX_PART(p)	MYBOX
  #define PROC_PART(p)  MYPROC
  #define INDEX		INDEX_LOCAL
  #define EQUAL(ptr1,ptr2) ((ptr1) == (ptr2))
#endif

#if GLOBAL_ARRAY && !GLOBAL_ELEMENTS
# define assignable 0
#else
# define assignable 1
#endif

#define TI_ARRAY_H
#include "ti_array_addr.h" /* must come after defines above */

/* Functions defined in ti_array.cc */
extern void ti_dump(ti_ARRAY *px, char *dest, char *userArg);
extern PTR_TO_T ti_get_array_data_ptr(const ti_ARRAY x);
extern ti_ARRAY ti_construct(const char *where, Region pregion, ti_RECTDOMAIN R, int isShared);
#if N_LARGER_THAN_1
extern ti_ARRAY_SLICE SLICE_METHOD(const ti_ARRAY x, jint k, jint j);
#else
void SLICE_METHOD(const ti_ARRAY x, jint k, jint j);
#endif
#if assignable
  extern void EXCHANGE_METHOD(const ti_ARRAY x, T w);
  extern void COPYINNER_METHOD(const ti_ARRAY x, const ti_ARRAY y, T6Handle4lang2ti *handle);
  TI_INLINE(COPYNB_METHOD)
  T6Handle4lang2ti COPYNB_METHOD(const ti_ARRAY x, const ti_ARRAY y) {
    T6Handle4lang2ti handle;
    TI_HANDLE_SETDONE(&handle);
    COPYINNER_METHOD(x, y, &handle);
    return handle;
  }
  void SET_METHOD(const ti_ARRAY x, T w);
  void COPY_WITHDOMAIN_METHOD(const ti_ARRAY x, const ti_ARRAY y, const ti_DOMAIN dom);
  void COPY_WITHRECTDOMAIN_METHOD(const ti_ARRAY x, const ti_ARRAY y, const ti_RECTDOMAIN rdom);
  void COPY_WITHPTARRAY_METHOD(const ti_ARRAY x, const ti_ARRAY y, const ti_1DARRAY_OF_POINT_N ptArray);
  ti_ARRAY MAKELOCALANDCONTIGUOUS_METHOD(const ti_ARRAY x);
  void SCATTER_METHOD(const ti_ARRAY destArray, const ti_1DARRAY_OF_T packedArray, const ti_1DARRAY_OF_POINT_N ptArray);
  void GATHER_METHOD(const ti_ARRAY srcArray, const ti_1DARRAY_OF_T packedArray, const ti_1DARRAY_OF_POINT_N ptArray);
#endif
ti_ARRAY TRANSLATE_METHOD(ti_ARRAY x, ti_POINT p);
ti_ARRAY PERMUTE_METHOD(ti_ARRAY x, ti_POINT p);
ti_ARRAY INJECT_METHOD(ti_ARRAY x, ti_POINT p);
ti_ARRAY PROJECT_METHOD(ti_ARRAY x, ti_POINT p);
jboolean ISCONTIGUOUS_METHOD(const ti_ARRAY x);
jboolean ISCONTIGUOUSOVERDOMAIN_METHOD(const ti_ARRAY x, const ti_RECTDOMAIN R);
void ti_printdomain(ti_ARRAY x, FILE *f);
void ti_boundscheck(const char *where, const ti_ARRAY *x, ti_POINT p);
void ti_abv_ds(const char *errorlocation, const ti_ARRAY *x, int dim,
               int arg1, const char *arg2);
void ti_abv_dsd(const char *errorlocation, const ti_ARRAY *x, int dim,
                int arg1, const char *arg2, int arg3);

/* bulk I/O functions */
#ifndef __bulkio_typedefs
#define __bulkio_typedefs
  /* these three seem to always get generated */
  #include "layout!T16RandomAccessFile2io4java.h"
  #include "layout!T15DataInputStream2io4java.h"
  #include "layout!T16DataOutputStream2io4java.h"

  /* these three are more of a struggle */
  #ifdef _include_layout_PT16RandomAccessFile2io4java_h_
    #define GP_RAF PT16RandomAccessFile2io4java
  #else
    typedef T16RandomAccessFile2io4java _RAF;
    typedef GP_type( _RAF ) GP_RAF;
  #endif
  #ifdef _include_layout_PT15DataInputStream2io4java_h_
    #define GP_DIS PT15DataInputStream2io4java
  #else
    typedef T15DataInputStream2io4java _DIS;
    typedef GP_type( _DIS ) GP_DIS;
  #endif
  #ifdef _include_layout_PT16DataOutputStream2io4java_h_
    #define GP_DOS PT16DataOutputStream2io4java
  #else
    typedef T16DataOutputStream2io4java _DOS;
    typedef GP_type( _DOS ) GP_DOS;
  #endif
#endif

#if ELEMENTS_ARE_ATOMIC
  #if assignable
    void READFROMRAF_METHOD(const ti_ARRAY x, GP_RAF fileobj);
    void READFROMDIS_METHOD(const ti_ARRAY x, GP_DIS fileobj);
  #endif
  void WRITETORAF_METHOD(const ti_ARRAY x, GP_RAF fileobj);
  void WRITETODOS_METHOD(const ti_ARRAY x, GP_DOS fileobj);
#endif

TI_INLINE(DOMAIN_METHOD) 
ti_RECTDOMAIN DOMAIN_METHOD(const ti_ARRAY x) {
  assertExists(x, "(in domain method)");
  return x.domain;
}

TI_INLINE(RESTRICT_METHOD) 
ti_ARRAY RESTRICT_METHOD(const ti_ARRAY x, ti_RECTDOMAIN R) {
  ti_ARRAY n = x;
  assertExists(x, "(in restrict method)");
  n.domain = RECTDOMAIN_INTERSECTION(x.domain, R);
  return n;
}

TI_INLINE(SHRINK1_METHOD) 
ti_ARRAY SHRINK1_METHOD(const ti_ARRAY x, jint k) {
  ti_ARRAY n = x;
  assertExists(x, "(in shrink method)");
  n.domain = RECTDOMAIN_SHRINK1(x.domain, k); /* always safe to omit intersection */
  return n;
}

TI_INLINE(SHRINK2_METHOD) 
ti_ARRAY SHRINK2_METHOD(const ti_ARRAY x, jint k, jint dir) {
  ti_ARRAY n = x;
  assertExists(x, "(in shrink method)");
  n.domain = RECTDOMAIN_SHRINK2(x.domain, k, dir); /* always safe to omit intersection */
  return n;
}

TI_INLINE(BORDER3_METHOD) 
ti_ARRAY BORDER3_METHOD(const ti_ARRAY x, jint k, jint dir, jint shift) {
  ti_ARRAY n = x;
  assertExists(x, "(in border method)");
  /* intersection required in many cases for correctness */
  n.domain = RECTDOMAIN_INTERSECTION(x.domain, RECTDOMAIN_BORDER3(x.domain, k, dir, shift));
  return n;
}

TI_INLINE(SIZE_METHOD) 
jint SIZE_METHOD(const ti_ARRAY x) {
  assertExists(x, "(in size method)");
  return RECTDOMAIN_SIZE(x.domain);
}

TI_INLINE(ISEMPTY_METHOD) 
jboolean ISEMPTY_METHOD(const ti_ARRAY x) {
  assertExists(x, "(in isEmpty method)");
  return RECTDOMAIN_ISNULL(x.domain);
}

TI_INLINE(CREATOR_METHOD) 
jint CREATOR_METHOD(const ti_ARRAY x) {
  assertExists(x, "(in creator method)");
  /* creator returns "the lowest numbered process whose demesne contains
     the object pointed to" */
#if EXPLICITLY_STORE_CREATOR
  return COMM_GetProxyProcNumberForBoxNumber(COMM_GetBoxNumberForProcNumber(x.creator));
#else
  return COMM_GetProxyProcNumberForBoxNumber(BOX_PART(x.A));
#endif
}

TI_INLINE(ISLOCAL_METHOD) 
jboolean ISLOCAL_METHOD(const ti_ARRAY x) {
  assertExists(x, "(in isLocal method)");
#ifndef MEMORY_DISTRIBUTED
  return 1;
#else
  return (COMM_GetBoxNumberForProcNumber(CREATOR_METHOD(x)) == MYBOX);
#endif
}

TI_INLINE(REGIONOF_METHOD) 
Region REGIONOF_METHOD(const ti_ARRAY x) {
  Region lretval;
  RegionId rid;
  assertExists(x, "(in regionOf method)");

  #if GLOBAL_ARRAY
    rid = PG2RegionId(x.A);
  #else
    rid = PL2RegionId(x.A);
  #endif

  if (rid == UNK_REGIONID)
    lretval = NULL;
  else
    lretval = RegionId2Region(rid);
  return lretval;
}

/* checkpoint functions */
extern jchar ti_index;

extern void ti_arr_checkpoint(void *, T10Checkpoint4lang2ti *);
extern void ti_arr_restore(void *, T10Checkpoint4lang2ti *);
extern void ti_register_array();

extern void ti_checkpoint(ti_ARRAY, T10Checkpoint4lang2ti *, jboolean);
extern ti_ARRAY ti_restore(ti_ARRAY, T10Checkpoint4lang2ti *);

#undef TI_ARRAY_H
#undef POINT_PRINT
#undef N_LARGER_THAN_1
#undef LOCAL_PART
#undef BOX_PART
#undef PROC_PART
#undef INDEX
#undef EQUAL
#undef deref
#undef assign
#undef weak_assign
#undef assignable
