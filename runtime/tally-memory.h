#ifndef INCLUDE_TALLY_MEMORY_H
#define INCLUDE_TALLY_MEMORY_H


#ifdef TALLY_MEMORY

extern ti_lock_t memoryTallyLock;
extern unsigned sharedChunks;
extern unsigned nonsharedChunks;
extern unsigned long sharedBytes;
extern unsigned long nonsharedBytes;

#define tally_memory( size, isShared  )		\
	do					\
	  {					\
	    if (isShared)			\
	      tally_memory_( size, shared );	\
	    else				\
	      tally_memory_( size, nonshared );	\
	  }					\
	while (0)

#define tally_memory_( size, sharing )		\
	do					\
	  {					\
	    ti_lock( &memoryTallyLock );	\
	    ++sharing##Chunks;			\
	    sharing##Bytes += size;		\
	    ti_unlock( &memoryTallyLock );	\
	  }					\
	while (0)

void init_memory_tally();
void print_memory_tally();

#else  /* !TALLY_MEMORY */

#define tally_memory( size, isShared )
#define tally_memory_( size, sharing )
#define init_memory_tally()
#define print_memory_tally()

#endif /* !TALLY_MEMORY */


#endif /* !INCLUDE_TALLY_MEMORY_H */
