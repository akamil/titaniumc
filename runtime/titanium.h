#ifndef _TITANIUM_H_
#define _TITANIUM_H_

#include "backend-defines.h"

#include "ti_config.h"
#include "file-64.h"
#include "runtime-options.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#define initloop_head(v, temp, data, arrInit, size)	\
{							\
  int i = (size);					\
  v = (arrInit);					\
  temp = (void *) data;					\
  while (i-- > 0) {


#define initloop_tail(temp, eltType)				\
    temp = (void *) (((char *) temp) + sizeof(eltType));	\
  }								\
}

/* Dynamic call tracing (enabled with -callstats) */
extern int dynamicCallCount_staticNative;
extern int dynamicCallCount_staticSpecial;
extern int dynamicCallCount_staticTitanium;
extern int dynamicCallCount_dynamic;

#define TALLY_CALL(cMethodName, canCallStatically, isSpecial, isNative)  do {\
    if (isSpecial) dynamicCallCount_staticSpecial++;              \
    else if (canCallStatically) {                                 \
      if (isNative) dynamicCallCount_staticNative++;              \
      else dynamicCallCount_staticTitanium++;                     \
    } else dynamicCallCount_dynamic++;                            \
  } while(0)

/* values defined in ti-main */
extern int bounds_checking;
extern const char *tcCompilerFlags;
extern char *RUNTIME_MODEL;

#if BOUNDS_CHECKING
 #define BOUNDS_CHECK(cond, msg) do {                 \
    if_pf (!(cond)) {                                 \
      fprintf(stderr,"BOUNDS_CHECK(%s) failed: %s\n", \
              #cond, #msg);                           \
      fflush(stderr);                                 \
      abort();                                        \
    }                                                 \
  } while (0)
#else
 #define BOUNDS_CHECK(cond, msg) ((void)0)
#endif
#if EXPLICITLY_STORE_ALLOCATION_SITE
#define _TI_ABV_MSG1 " was allocated at %s and"
#define _TI_ABV_MSG2(arr) , (arr).where
#else
#define _TI_ABV_MSG1
#define _TI_ABV_MSG2(arr) 
#endif

#define TI_ABV(arr, dim, created, printdomain, format, arg1, arg2, arg3) \
do {									 \
  fprintf(stderr, "Array bounds violation on processor %d!\n"		 \
	  "Error occurred in loop at %s in dimension %d of an array\n"	 \
	  "that" _TI_ABV_MSG1 " has bounds ",                            \
          MYPROC, (created), (dim) _TI_ABV_MSG2(arr));			 \
  printdomain((arr), stderr);						 \
  fputs(".\nCondition violated: ", stderr);				 \
  fprintf(stderr, (format), (arg1), (arg2), (arg3));			 \
  fputs("\n", stderr);							 \
  abort();								 \
  exit(-11);								 \
} while (0)

/* static headers */
#include "array-addr.h"
#include "broadcast.h"
#include "exceptions.h"
#include <ti-gc.h>
#include "java_array.h"
#include "java_array_methods.h"
#include "java_string.h"
#include "native-utils.h"
#include "primitives.h"
#include "subtype.h"
#include "tally-memory.h"
#include "ti-memory.h"
#include "ti_array_methods.h"
#include "type_header.h"
#if USE_STOPTIFU
/* skip the HUGE compilation overhead of the stoptifu headers unless they're needed */
#include "stoptifu-runtime.h"
#endif

/* generated headers */
#include "domains.h"

/* local grids */
#define _ti_READ(T, N) TI_ARRAY_READ_ ## T ## _ ## N
#define _ti_PTR(T, N) TI_ARRAY_PTR_ ## T ## _ ## N
#define _ti_WRITE(T, N) TI_ARRAY_WRITE_ ## T ## _ ## N
#define _ti_CONV(T, N) TI_ARRAY_CONV_ ## T ## _ ## N
#define _ti_GET_ARRAY_DATA_PTR(T, N) TI_ARRAY_GET_ARRAY_DATA_PTR_ ## T ## _ ## N
#define _ti_GET_ARRAY_DATA_PTR_WITH_DOMAIN(T, N) TI_ARRAY_GET_ARRAY_DATA_PTR_WITH_DOMAIN_ ## T ## _ ## N
#define _ti_DUMP(T, N) TI_ARRAY_DUMP_ ## T ## _ ## N
#define _ti_CONSTRUCT(T, N) TI_ARRAY_CONSTRUCT_ ## T ## _ ## N
#define _ti_EMPTY(T, N) TI_ARRAY_empty_ ## T ## _ ## N
#define _ti_ISNULL(T, N) TI_ARRAY_isnull_ ## T ## _ ## N
#define _ti_EQUALS(T, N) TI_ARRAY_equals_ ## T ## _ ## N
#define _ti_PRINTDOMAIN(T, N) TI_ARRAY_printdomain_ ## T ## _ ## N
#define _ti_ARRAY_BOUNDS_VIOLATIONds(T, N) TI_ARRAY_abv_ds_ ## T ## _ ## N
#define _ti_ARRAY_BOUNDS_VIOLATIONdsd(T, N) TI_ARRAY_abv_dsd_ ## T ## _ ## N
#define _ti_BOUNDSCHECK(T, N) TI_ARRAY_boundscheck_ ## T ## _ ## N
#define _ti_MAYOVERLAP(T, N) TI_ARRAY_mayoverlap_ ## T ## _ ## N
#define _ti_INDEX(T, N) TI_ARRAY_index_ ## T ## _ ## N
#define _ti_ARRAY_CHECKPOINT(T, N) TI_ARRAY_array_checkpoint_ ## T ## _ ## N
#define _ti_ARRAY_RESTORE(T, N) TI_ARRAY_array_restore_ ## T ## _ ## N
#define _ti_REGISTER_ARRAY(T, N) TI_ARRAY_register_array_ ## T ## _ ## N
#define _ti_CHECKPOINT(T, N) TI_ARRAY_checkpoint_ ## T ## _ ## N
#define _ti_RESTORE(T, N) TI_ARRAY_restore_ ## T ## _ ## N

#define _ti_arraymethod_cast(T, N) TI_ARRAY_cast_ ## T ## _ ## N
#define _ti_arraymethod_domain(T, N) TI_ARRAY_domain_ ## T ## _ ## N
#define _ti_arraymethod_isEmpty(T, N) TI_ARRAY_isEmpty_ ## T ## _ ## N
#define _ti_arraymethod_size(T, N) TI_ARRAY_size_ ## T ## _ ## N
#define _ti_arraymethod_shrink1(T, N) TI_ARRAY_shrink1_ ## T ## _ ## N
#define _ti_arraymethod_shrink2(T, N) TI_ARRAY_shrink2_ ## T ## _ ## N
#define _ti_arraymethod_border1(T, N) TI_ARRAY_border1_ ## T ## _ ## N
#define _ti_arraymethod_border2(T, N) TI_ARRAY_border2_ ## T ## _ ## N
#define _ti_arraymethod_border3(T, N) TI_ARRAY_border3_ ## T ## _ ## N
#define _ti_arraymethod_copyinner(T, N) TI_ARRAY_copyinner_ ## T ## _ ## N
#define _ti_arraymethod_copy(T, N) TI_ARRAY_copy_ ## T ## _ ## N
#define _ti_arraymethod_copyNB(T, N) TI_ARRAY_copyNB_ ## T ## _ ## N
#define _ti_arraymethod_copyNBI(T, N) TI_ARRAY_copyNBI_ ## T ## _ ## N
#define _ti_arraymethod_copy_withdomain(T, N) TI_ARRAY_copy_withdomain_ ## T ## _ ## N
#define _ti_arraymethod_copy_withrectdomain(T, N) TI_ARRAY_copy_withrectdomain_ ## T ## _ ## N
#define _ti_arraymethod_copy_withptarray(T, N) TI_ARRAY_copy_withptarray_ ## T ## _ ## N
#define _ti_arraymethod_scatter(T, N) TI_ARRAY_scatter_ ## T ## _ ## N
#define _ti_arraymethod_gather(T, N) TI_ARRAY_gather_ ## T ## _ ## N
#define _ti_arraymethod_isnull(T, N) TI_ARRAY_isnull_ ## T ## _ ## N
#define _ti_arraymethod_equals(T, N) TI_ARRAY_equals_ ## T ## _ ## N
#define _ti_arraymethod_isLocal(T, N) TI_ARRAY_isLocal_ ## T ## _ ## N
#define _ti_arraymethod_regionOf(T, N) TI_ARRAY_regionOf_ ## T ## _ ## N
#define _ti_arraymethod_isContiguous(T, N) TI_ARRAY_isContiguous_ ## T ## _ ## N
#define _ti_arraymethod_isContiguousOverDomain(T, N) TI_ARRAY_isContiguousOverDomain_ ## T ## _ ## N
#define _ti_arraymethod_makeLocalAndContiguous(T, N) TI_ARRAY_makeLocalAndContiguous_ ## T ## _ ## N
#define _ti_arraymethod_tiArrayCopyInner(T, N) TI_ARRAY_tiArrayCopyInner_ ## T ## _ ## N
#define _ti_arraymethod_creator(T, N) TI_ARRAY_creator_ ## T ## _ ## N
#define _ti_arraymethod_restrict(T, N) TI_ARRAY_restrict_ ## T ## _ ## N
#define _ti_arraymethod_translate(T, N) TI_ARRAY_translate_ ## T ## _ ## N
#define _ti_arraymethod_permute(T, N) TI_ARRAY_permute_ ## T ## _ ## N
#define _ti_arraymethod_inject(T, N) TI_ARRAY_inject_ ## T ## _ ## N
#define _ti_arraymethod_project(T, N) TI_ARRAY_project_ ## T ## _ ## N
#define _ti_arraymethod_readFromRAF(T, N) TI_ARRAY_readFromRAF_ ## T ## _ ## N
#define _ti_arraymethod_writeToRAF(T, N) TI_ARRAY_writeToRAF_ ## T ## _ ## N
#define _ti_arraymethod_readFromDIS(T, N) TI_ARRAY_readFromDIS_ ## T ## _ ## N
#define _ti_arraymethod_writeToDOS(T, N) TI_ARRAY_writeToDOS_ ## T ## _ ## N
#define _ti_arraymethod_exchange(T, N) TI_ARRAY_exchange_ ## T ## _ ## N
#define _ti_arraymethod_slice(T, N) TI_ARRAY_slice_ ## T ## _ ## N
#define _ti_arraymethod_broadcast(T, N) TI_ARRAY_broadcast_ ## T ## _ ## N
#define _ti_arraymethod_set(T, N) TI_ARRAY_set_ ## T ## _ ## N
#define _ti_arraymethod_empty(T, N) TI_ARRAY_empty_ ## T ## _ ## N
#define _ti_arraymethod_pack(T, N) TI_ARRAY_pack_ ## T ## _ ## N
#define _ti_arraymethod_unpack(T, N) TI_ARRAY_unpack_ ## T ## _ ## N
/* "methods" that do nothing */
#define _ti_arraymethod_junk(T, N)


/* global grids */
#define _ti_global_READ(T, N) TI_GLOBAL_ARRAY_READ_ ## T ## _ ## N
#define _ti_global_PTR(T, N) TI_GLOBAL_ARRAY_PTR_ ## T ## _ ## N
#define _ti_global_WRITE(T, N) TI_GLOBAL_ARRAY_WRITE_ ## T ## _ ## N
#define _ti_global_CONV(T, N) TI_GLOBAL_ARRAY_CONV_ ## T ## _ ## N
#define _ti_global_GET_ARRAY_DATA_PTR(T, N) TI_GLOBAL_ARRAY_GET_ARRAY_DATA_PTR_ ## T ## _ ## N
#define _ti_global_GET_ARRAY_DATA_PTR_WITH_DOMAIN(T, N) TI_GLOBAL_ARRAY_GET_ARRAY_DATA_PTR_WITH_DOMAIN_ ## T ## _ ## N
#define _ti_global_DUMP(T, N) TI_GLOBAL_ARRAY_DUMP_ ## T ## _ ## N
#define _ti_global_CONSTRUCT(T, N) TI_GLOBAL_ARRAY_CONSTRUCT_ ## T ## _ ## N
#define _ti_global_EMPTY(T, N) TI_GLOBAL_ARRAY_empty_ ## T ## _ ## N
#define _ti_global_ISNULL(T, N) TI_GLOBAL_ARRAY_isnull_ ## T ## _ ## N
#define _ti_global_EQUALS(T, N) TI_GLOBAL_ARRAY_equals_ ## T ## _ ## N
#define _ti_global_PRINTDOMAIN(T, N) TI_GLOBAL_ARRAY_printdomain_ ## T ## _ ## N
#define _ti_global_ARRAY_BOUNDS_VIOLATIONds(T, N) TI_GLOBAL_ARRAY_abv_ds_ ## T ## _ ## N
#define _ti_global_ARRAY_BOUNDS_VIOLATIONdsd(T, N) TI_GLOBAL_ARRAY_abv_dsd_ ## T ## _ ## N
#define _ti_global_BOUNDSCHECK(T, N) TI_GLOBAL_ARRAY_boundscheck_ ## T ## _ ## N
#define _ti_global_MAYOVERLAP(T, N) TI_GLOBAL_ARRAY_mayoverlap_ ## T ## _ ## N
#define _ti_global_INDEX(T, N) TI_GLOBAL_ARRAY_index_ ## T ## _ ## N
#define _ti_global_ARRAY_CHECKPOINT(T, N) TI_GLOBAL_ARRAY_array_checkpoint_ ## T ## _ ## N
#define _ti_global_ARRAY_RESTORE(T, N) TI_GLOBAL_ARRAY_array_restore_ ## T ## _ ## N
#define _ti_global_REGISTER_ARRAY(T, N) TI_GLOBAL_ARRAY_register_array_ ## T ## _ ## N
#define _ti_global_CHECKPOINT(T, N) TI_GLOBAL_ARRAY_checkpoint_ ## T ## _ ## N
#define _ti_global_RESTORE(T, N) TI_GLOBAL_ARRAY_restore_ ## T ## _ ## N

#define _ti_global_arraymethod_cast(T, N) TI_GLOBAL_ARRAY_cast_ ## T ## _ ## N
#define _ti_global_arraymethod_domain(T, N) TI_GLOBAL_ARRAY_domain_ ## T ## _ ## N
#define _ti_global_arraymethod_isEmpty(T, N) TI_GLOBAL_ARRAY_isEmpty_ ## T ## _ ## N
#define _ti_global_arraymethod_size(T, N) TI_GLOBAL_ARRAY_size_ ## T ## _ ## N
#define _ti_global_arraymethod_shrink1(T, N) TI_GLOBAL_ARRAY_shrink1_ ## T ## _ ## N
#define _ti_global_arraymethod_shrink2(T, N) TI_GLOBAL_ARRAY_shrink2_ ## T ## _ ## N
#define _ti_global_arraymethod_border1(T, N) TI_GLOBAL_ARRAY_border1_ ## T ## _ ## N
#define _ti_global_arraymethod_border2(T, N) TI_GLOBAL_ARRAY_border2_ ## T ## _ ## N
#define _ti_global_arraymethod_border3(T, N) TI_GLOBAL_ARRAY_border3_ ## T ## _ ## N
#define _ti_global_arraymethod_copyinner(T, N) TI_GLOBAL_ARRAY_copyinner_ ## T ## _ ## N
#define _ti_global_arraymethod_copy(T, N) TI_GLOBAL_ARRAY_copy_ ## T ## _ ## N
#define _ti_global_arraymethod_copyNB(T, N) TI_GLOBAL_ARRAY_copyNB_ ## T ## _ ## N
#define _ti_global_arraymethod_copyNBI(T, N) TI_GLOBAL_ARRAY_copyNBI_ ## T ## _ ## N
#define _ti_global_arraymethod_copy_withdomain(T, N) TI_GLOBAL_ARRAY_copy_withdomain_ ## T ## _ ## N
#define _ti_global_arraymethod_copy_withrectdomain(T, N) TI_GLOBAL_ARRAY_copy_withrectdomain_ ## T ## _ ## N
#define _ti_global_arraymethod_copy_withptarray(T, N) TI_GLOBAL_ARRAY_copy_withptarray_ ## T ## _ ## N
#define _ti_global_arraymethod_scatter(T, N) TI_GLOBAL_ARRAY_scatter_ ## T ## _ ## N
#define _ti_global_arraymethod_gather(T, N) TI_GLOBAL_ARRAY_gather_ ## T ## _ ## N
#define _ti_global_arraymethod_pack(T, N) TI_GLOBAL_ARRAY_pack_ ## T ## _ ## N
#define _ti_global_arraymethod_unpack(T, N) TI_GLOBAL_ARRAY_unpack_ ## T ## _ ## N
#define _ti_global_arraymethod_isLocal(T, N) TI_GLOBAL_ARRAY_isLocal_ ## T ## _ ## N
#define _ti_global_arraymethod_regionOf(T, N) TI_GLOBAL_ARRAY_regionOf_ ## T ## _ ## N
#define _ti_global_arraymethod_isContiguous(T, N) TI_GLOBAL_ARRAY_isContiguous_ ## T ## _ ## N
#define _ti_global_arraymethod_isContiguousOverDomain(T, N) TI_GLOBAL_ARRAY_isContiguousOverDomain_ ## T ## _ ## N
#define _ti_global_arraymethod_makeLocalAndContiguous(T, N) TI_GLOBAL_ARRAY_makeLocalAndContiguous_ ## T ## _ ## N
#define _ti_global_arraymethod_tiArrayCopyInner(T, N) TI_GLOBAL_ARRAY_tiArrayCopyInner_ ## T ## _ ## N
#define _ti_global_arraymethod_creator(T, N) TI_GLOBAL_ARRAY_creator_ ## T ## _ ## N
#define _ti_global_arraymethod_restrict(T, N) TI_GLOBAL_ARRAY_restrict_ ## T ## _ ## N
#define _ti_global_arraymethod_translate(T, N) TI_GLOBAL_ARRAY_translate_ ## T ## _ ## N
#define _ti_global_arraymethod_permute(T, N) TI_GLOBAL_ARRAY_permute_ ## T ## _ ## N
#define _ti_global_arraymethod_inject(T, N) TI_GLOBAL_ARRAY_inject_ ## T ## _ ## N
#define _ti_global_arraymethod_isnull(T, N) TI_GLOBAL_ARRAY_isnull_ ## T ## _ ## N
#define _ti_global_arraymethod_equals(T, N) TI_GLOBAL_ARRAY_equals_ ## T ## _ ## N
#define _ti_global_arraymethod_project(T, N) TI_GLOBAL_ARRAY_project_ ## T ## _ ## N
#define _ti_global_arraymethod_readFromRAF(T, N) TI_GLOBAL_ARRAY_readFromRAF_ ## T ## _ ## N
#define _ti_global_arraymethod_writeToRAF(T, N) TI_GLOBAL_ARRAY_writeToRAF_ ## T ## _ ## N
#define _ti_global_arraymethod_readFromDIS(T, N) TI_GLOBAL_ARRAY_readFromDIS_ ## T ## _ ## N
#define _ti_global_arraymethod_writeToDOS(T, N) TI_GLOBAL_ARRAY_writeToDOS_ ## T ## _ ## N
#define _ti_global_arraymethod_exchange(T, N) TI_GLOBAL_ARRAY_exchange_ ## T ## _ ## N
#define _ti_global_arraymethod_slice(T, N) TI_GLOBAL_ARRAY_slice_ ## T ## _ ## N
#define _ti_global_arraymethod_broadcast(T, N) TI_GLOBAL_ARRAY_broadcast_ ## T ## _ ## N
#define _ti_global_arraymethod_set(T, N) TI_GLOBAL_ARRAY_set_ ## T ## _ ## N
#define _ti_global_arraymethod_empty(T, N) TI_GLOBAL_ARRAY_empty_ ## T ## _ ## N
#define _ti_global_arraymethod_pack(T, N) TI_GLOBAL_ARRAY_pack_ ## T ## _ ## N
#define _ti_global_arraymethod_unpack(T, N) TI_GLOBAL_ARRAY_unpack_ ## T ## _ ## N
/* "methods" that do nothing */
#define _ti_global_arraymethod_junk(T, N)

/* Print and string routines */
extern char *ItoString(int);
extern char *FtoString(double);
extern char *ti_strcat(char *, char *);

extern void broadcast_buf(int *d, int *s, int size, int proc);
extern void bulk_copy(void * loc, void *rem, int nelems,
		      int elem_size, int stride, int proc);
extern double Ti_all_reduce_max_d(double);
extern double Ti_all_reduce_min_d(double);
extern double Ti_all_reduce_sum_d(double);


#endif /* _TITANIUM_H_ */
