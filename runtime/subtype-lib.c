#include "class_header.h"
#include "interface_header.h"
#include "subtype.h"


int subclassOf( const class_header * const candidate,
		const class_header * const superclass )
{
    return candidate == superclass
	|| (candidate && subclassOf( candidate->super, superclass ));
}


/**********************************************************************/


static int inList( const interface_header * const candidate,
		   const interface_header * const * const list )
{
    if (list) {
	const interface_header * const * available;
	for (available = list; *available; ++available)
	    if (candidate == *available || inList( candidate, (*available)->implements ))
		return 1;
    }
    
    return 0;
}


int classImplements( const class_header * const candidate,
		     const interface_header * const descriptor )
{
    return candidate && (inList( descriptor, candidate->implements )
			 || classImplements( candidate->super, descriptor ));
}
