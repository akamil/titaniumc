#ifndef _INCLUDE_MEM_WIDE_H_
#define _INCLUDE_MEM_WIDE_H_

#define WIDE_POINTERS

#include "instance_header.h"
#include "mem-local.h"

/* PLASSIGN_GLOBAL is illegal. */
/* WEAK_PLASSIGN_GLOBAL is illegal. */

#ifdef NOMEMCHECKS
#  define _PGASSIGN_GLOBAL(ptr, val, primtype ) ASSIGN_GLOBAL_ ## primtype (ptr, val)
#  define _WEAK_PGASSIGN_GLOBAL(ptr, val, primtype ) WEAK_ASSIGN_GLOBAL_ ## primtype (ptr, val)
#else /* NOMEMCHECKS */
#  define _PGASSIGN_GLOBAL(ptr, val, primtype ) do {                             \
        jGPointer _val = CAST_GPTR(val);                                         \
        jGPointer _ptr = CAST_GPTR(ptr);                                         \
        if (!(!isLocal(_val) || SHARED_REGIONID(PL2RegionId(TO_LOCAL(_val)))) && \
           (!isLocal(_ptr) || SHARED_REGIONID(PL2RegionId(TO_LOCAL(_ptr)))))     \
           sharingViolation(__current_loc);                                      \
	else ASSIGN_GLOBAL_ ## primtype (_ptr, _val); } while (0)
#  define _WEAK_PGASSIGN_GLOBAL(ptr, val, primtype ) do {                        \
        jGPointer _val = CAST_GPTR(val);                                         \
        jGPointer _ptr = CAST_GPTR(ptr);                                         \
        if (!(!isLocal(_val) || SHARED_REGIONID(PL2RegionId(TO_LOCAL(_val)))) && \
           (!isLocal(_ptr) || SHARED_REGIONID(PL2RegionId(TO_LOCAL(_ptr)))))     \
           sharingViolation(__current_loc);                                      \
	else WEAK_ASSIGN_GLOBAL_ ## primtype (_ptr, _val); } while (0)
#endif /* NOMEMCHECKS */

/* DOB: see mem-local.h for the naming scheme on these macros
   the prefixes constrain which value types are being accessed,
   so the type suffixes not shown are not meaningful and will never be generated
 */
#define PGASSIGN_GLOBAL_gp(ptr, val) _PGASSIGN_GLOBAL(ptr, val, gp)

#define WEAK_PGASSIGN_GLOBAL_gp(ptr, val) _WEAK_PGASSIGN_GLOBAL(ptr, val, gp)

#define _SPLASSIGN_GLOBAL(ptr, val, primtype)	ASSIGN_GLOBAL_ ## primtype (ptr, val)
#define _SPGASSIGN_GLOBAL(ptr, val, primtype)	ASSIGN_GLOBAL_ ## primtype (ptr, val)
#define _WEAK_SPLASSIGN_GLOBAL(ptr, val, primtype) WEAK_ASSIGN_GLOBAL_ ## primtype (ptr, val)	
#define _WEAK_SPGASSIGN_GLOBAL(ptr, val, primtype) WEAK_ASSIGN_GLOBAL_ ## primtype (ptr, val)	


#define SPGASSIGN_GLOBAL_gp(ptr, val) _SPGASSIGN_GLOBAL(ptr, val, gp)

#define WEAK_SPGASSIGN_GLOBAL_gp(ptr, val) _WEAK_SPGASSIGN_GLOBAL(ptr, val, gp)

#define SPLASSIGN_GLOBAL_lp(ptr, val) _SPLASSIGN_GLOBAL(ptr, val, lp)

#define WEAK_SPLASSIGN_GLOBAL_lp(ptr, val) _WEAK_SPLASSIGN_GLOBAL(ptr, val, lp)


#define TO_LOCAL(ptr)  GET_ADDR(ptr) /* DOB: it's crucial we don't destroy pointer type */
#define TO_PROC(ptr)   GET_PROC(ptr)
#define TO_BOX(ptr)    GET_BOX(ptr)

#define TO_GLOBALP( global, processor, address ) (SET_PROC((global),(processor)),SET_ADDR((global),(address)))
#define TO_GLOBALB( global, box, address)        (SET_BOX((global),(box)),SET_ADDR((global),(address)))

/* performs translation of a local c-static variable address 
   to the address for the corresponding remote copy */
#define TO_GLOBALP_STATIC( global, processor, local_staticaddr ) \
  (SET_PROC((global),(processor)),SET_ADDR((global),TI_TRANSLATE_CSTATIC_ADDR((local_staticaddr), GET_BOX(global))))
#define TO_GLOBALB_STATIC( global, box, local_staticaddr) \
  TO_GLOBALB( (global), (box), TI_TRANSLATE_CSTATIC_ADDR((local_staticaddr), (box)))

/* performs translation of a global, possibly remote, c-static variable address
   to the address for the corresponding local copy */
#define TO_LOCAL_STATIC(remote_staticaddr) TI_TRANSLATE_CSTATIC_ADDR_FROM(GET_ADDR(remote_staticaddr), GET_BOX(remote_staticaddr))

#define FIELD_ADDR_GLOBAL(val, obj, fld)	TO_GLOBALB(val, TO_BOX(obj), &((obj).addr->fld))
#define IFACE_DISPATCH_GLOBAL(ci, mid)          IFACE_DISPATCH_LOCAL(ci, mid)
#define INDEX_GLOBAL(element, base, offset)	TO_GLOBALB(element, TO_BOX(base), ((base).addr + (offset)))
#define SUM_GLOBAL(type, element, base, offset)	TO_GLOBALB(element, TO_BOX(base), (type) ((char *)(TO_LOCAL(base)) + (offset)))
#define EQUAL_GLOBAL(p, q)			((isNull(p) && isNull(q)) || ((TO_BOX(p) == TO_BOX(q)) && (TO_LOCAL(p) == TO_LOCAL(q))))
#define ISNULL_GLOBAL(p)                        (isNull(p))

#ifdef MISALIGNED_CSTATIC_DATA
  #define _CLASS_INFO_HELPER(ci, remote_box) (ci) = TI_TRANSLATE_CSTATIC_ADDR_FROM((ci), (remote_box))
#else
  #define _CLASS_INFO_HELPER(ci, remote_box) ((void)0)
#endif
#define CLASS_INFO_GLOBAL( val, type, obj ) do {	\
    PL ## type field_;					\
    FIELD_ADDR_GLOBAL( field_, obj, class_info );	\
    DEREF_GLOBAL_lp( val, field_ );			\
    _CLASS_INFO_HELPER( val, GET_BOX(obj) );            \
    assert((val) != NULL);                              \
  } while (0)

/* DOB: changed to match lang ref. defn of desmenses on 10/22 */
#define isLocal( global )	(TO_BOX(global) == MYBOX)

#define isDirectlyAddressable( global )	(TO_BOX(global) == MYBOX)

#define globalize( global, local )	TO_GLOBALB( global, MYBOX, local )

#define localize( local, global )				\
  do {								\
    if (isLocal(global)) 					\
      (local) = TO_LOCAL( global );				\
    else {							\
      (local) = NULL; /* avoid "uninitialized var" warnings */  \
      m23throwClassCastExceptionmT11NativeUtils4lang2ti();	\
    }                                                           \
  } while (0)


#define local_to_global_copy( local, global, nitems ) ASSIGN_GLOBAL_anonymous_bulk(global, nitems, local)
#define global_to_local_copy( global, local, nitems ) DEREF_GLOBAL_anonymous_bulk(local, nitems, global)




#endif /* !_INCLUDE_MEM_WIDE_H_ */
