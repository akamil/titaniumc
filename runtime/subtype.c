#include "runtime-options.h"
#include "class_header.h"
#include "subtype.h"
#include "type_header.h"

#include "T11NativeUtils4lang2ti.h"
#include "layout!PLclass_header.h"


TI_INLINE(getClass) 
class_header *getClass( const GP_instance instance ) {
    class_header *info;
    CLASS_INFO_GLOBAL( info, class_header, instance );
    return info;
}    


/**********************************************************************/


int instanceOfClass( const GP_instance instance,
		     const class_header * const descriptor )
{
    return nonNull( instance )
	&& subclassOf( getClass( instance ), descriptor );
}


GP_instance castToClass( const GP_instance instance,
			 const class_header * const descriptor )
{
    if (isNull( instance )
	|| subclassOf( getClass( instance ), descriptor ))
	return instance;
    else
	m23throwClassCastExceptionmT11NativeUtils4lang2ti();

    abort(); /* should never reach here */
}


/**********************************************************************/


int instanceOfInterface( const GP_instance instance,
			 const interface_header * const descriptor )
{
    return nonNull( instance )
	&& classImplements( getClass( instance ), descriptor );
}


GP_instance castToInterface( const GP_instance instance,
			     const interface_header * const descriptor )
{
    if (isNull( instance )
	|| classImplements( getClass( instance ), descriptor ))
	return instance;
    else
	m23throwClassCastExceptionmT11NativeUtils4lang2ti();

    abort(); /* should never reach here */
}


/**********************************************************************/


int instanceOf( const GP_instance instance,
		const type_header * const descriptor )
{
    if (nonNull( instance ))
	switch (descriptor->common_header.category) {
	case Class:
	    return instanceOfClass( instance, &descriptor->class_header );
	case Interface:
	    return instanceOfInterface( instance, &descriptor->interface_header );
	default:
	    abort();
	}

    return 0;
}


GP_instance castTo( const GP_instance instance,
		    const type_header * const descriptor )
{
    if (isNull( instance ))
	return instance;
    else
	switch (descriptor->common_header.category) {
	case Class:
	    return castToClass( instance, &descriptor->class_header );
	case Interface:
	    return castToInterface( instance, &descriptor->interface_header );
	default:
	    abort();
	}
}
