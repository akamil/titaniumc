#ifndef _INCLUDE_VALUE_HEADER_
#define _INCLUDE_VALUE_HEADER_

#include <sys/types.h>
#include "common_header.h"
#include "T5Class4lang4java.h"
#include "layout!LT6String4lang4java.h"


typedef struct value_header
{
    T5Class4lang4java class_object; /* MUST come first (not currently used) */
    LT6String4lang4java class_name;

    enum TypeCategory category;
    enum TypeFlags flags;
    jchar index;
    size_t size;
} value_header;


extern value_header boolean_desc;
extern value_header char_desc;
extern value_header byte_desc;
extern value_header short_desc;
extern value_header int_desc;
extern value_header long_desc;
extern value_header float_desc;
extern value_header double_desc;

extern void value_desc_init();

#endif
