package common::elan;
use base qw(common::tcrun);
use Cwd;

use strict;


########################################################################


sub run ($$@) {
    my $self = shift;
    my $ELANRUN_CMD = "prun -N %N %C";
   
    if ($ENV{TI_ELANRUN_CMD}) {
      $ELANRUN_CMD = $ENV{TI_ELANRUN_CMD};
    } 

    my $pwd = cwd;
    my $cmd = join(" ",@_);

    $ELANRUN_CMD =~ s/%N/$self->{processes}/g;
    $ELANRUN_CMD =~ s/%n/$self->{processes}/g;

    $ELANRUN_CMD =~ s/%C/$cmd/g;
    $ELANRUN_CMD =~ s/%c/$cmd/g;

    $ELANRUN_CMD =~ s/%D/$pwd/g;
    $ELANRUN_CMD =~ s/%d/$pwd/g;

    $ELANRUN_CMD =~ s/%%/%/g;

    $self->SUPER::run(split / /,$ELANRUN_CMD); 
}

sub processes ($$) {
    my ($self, $processes) = @_;
    $self->{processes} = $processes;
}

########################################################################


1;
