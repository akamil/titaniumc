# This file gets included by the various tic backend makefiles
# provides a common file listing of all the files used by the tic backends

libtic_backend_sources =			\
	array-byteswap.c			\
	barrier.c				\
	broadcast.c				\
	bulk.c					\
	com.c					\
	comm_utils.c				\
	dtoa.c					\
	gp-trace.c				\
	handlers.c				\
	main.c					\
	mem.c					\
	monitor.c				\
	newbulk.c				\
	stats.c					\
	store.c					\
	ti-dgc.c				


libtic_backend_headers =			\
	assign.h				\
	backend.h				\
        backend-defines.h                       \
	barrier.h				\
        broadcast.c                             \
	broadcast.h				\
	bulk.h					\
	com.h					\
	comfun.def                              \
	comm.h					\
	comm_utils.h				\
	control.h				\
	deref.h					\
	handlers.h				\
	mem.h					\
	monitor.h				\
	monitor-dist.h                          \
        monitor-shared.c                        \
        monitor-dist.c                          \
	newbulk.h				\
	pointers.h				\
	store.h					\
	tic.h					\
	tic_am2_macros.h			

