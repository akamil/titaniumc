#define TI_NO_SRCPOS
#include <tic.h>

#ifdef MEMORY_SHARED
  static ti_lock_t mutex[ 2 ] = 
   { ti_lock_decl_initializer, ti_lock_decl_initializer };

  #define MULTIPLE_THREADS
  #define ACQUIRE_DTOA_LOCK( n )  ti_lock(   &mutex[ n ] )
  #define FREE_DTOA_LOCK( n )     ti_unlock( &mutex[ n ] )
#endif


#include "../../fp-utils/dtoa-common.c"
