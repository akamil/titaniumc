#ifndef _include_foo_cluster_smp_backend_defines_h_
#define _include_foo_cluster_smp_backend_defines_h_


#define BACKEND_FOO
#define BACKEND_FOO_CLUSTER_SMP
#define TIC_BACKEND_NAME "foo-cluster-smp"
#define MEMORY_DISTRIBUTED
#define MEMORY_SHARED
#define COMM_AM2
#define COMM_AMFOO
#define PTHREAD
#define HAVE_MONITORS
#define WIDE_POINTERS
#define USE_DISTRIBUTED_GC 1

#endif /* !_include_foo_cluster_smp_backend_defines_h_ */
