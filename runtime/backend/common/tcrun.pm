package common::tcrun;

use strict;


########################################################################
#
#  constructor
#

sub new ($) {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = { processes => 1 };
    bless $self, $class;
}


########################################################################
#
#  interal utility routines for use by subclasses
#

sub quote ($) {
    my $word = shift;
    $word =~ s/\\/\\\\/g;
    
    if ($word =~ /[^-_.:\/a-zA-Z0-9]/) {
	$word =~ s/\'/\'\\\'\'/g;
	$word = "'$word'";
    } else {
	$word =~ s/\'/\\\'/g;
    }
    
    return $word;
}


sub trace_exec ($@) {
    my $self = shift;
    if ($self->{verbose} || $self->{show}) {
	my @quoted = map { quote $_ } @_;
	print "@quoted\n";
    }
    return if $self->{show};
    exec { $_[0] } @_
	    or die "could not exec $_[0]: $!\n";
}


sub trace_setenv ($@) {
    my ($self, $name, @value) = @_;
    print "$name=", (quote "@value"), ' ' if $self->{show};
    print "$name=", (quote "@value"), "\n" if $self->{verbose};
    $ENV{$name} = "@value";
}


########################################################################
#
#  public routines for use by "tcrun" script
#

sub keywords ($%) {
}

sub apportionments ($@) {
    my $self = shift;
    warn "ignoring apportionment list \"@_\"\n";
}


sub machines ($@) {
    my $self = shift;
    warn "ignoring machines list \"@_\"\n";
}


sub processes ($$) {
    my ($self, $processes) = @_;
    warn "ignoring process count \"$processes\"\n" if $processes != 1;
}


sub run ($@) {
    my $self = shift;
    $self->trace_exec(@_);
}


sub show ($$) {
    my ($self, $show) = @_;
    $self->{show} = $show;
}

sub cross_compiling ($$) {
    my ($self, $cross_compiling) = @_;
    $self->{cross_compiling} = $cross_compiling;
}

sub verbose ($$) {
    my ($self, $verbose) = @_;
    $self->{verbose} = $verbose;
}


########################################################################


1;
