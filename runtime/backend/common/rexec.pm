package common::rexec;
use base qw(common::tcrun);

use strict;


########################################################################


sub apportionments ($@) {
    my $self = shift;
    $self->trace_setenv('TI_THREADS', @_);
}


sub machines ($@) {
    my $self = shift;
    $self->trace_setenv('REXEC_SVRS', @_);
}


sub processes ($$) {
    my ($self, $processes) = @_;
    $self->{processes} = $processes;
}


sub run ($@) {
    my $self = shift;
    
   # DOB: allow empty REXEC_SVRS - rexec performs dynamic load balancing
   # die "machine list needed; use \"--mapping\" or set \$REXEC_SVRS\n" unless $ENV{REXEC_SVRS};

    unless ($ENV{TI_THREADS} || $ENV{TI_PFORP}) {
	my @apportionments = ('1') x $self->{processes};
	$self->apportionments(@apportionments);
	warn "assuming apportionment list \"@apportionments\"\n";
    }
    
    $self->SUPER::run('rexec', '-p', 1, '-n', $self->{processes}, @_);
}


########################################################################


1;
