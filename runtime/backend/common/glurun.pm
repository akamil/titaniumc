package common::glurun;
use base qw(common::tcrun);

use strict;


########################################################################


sub machines ($@) {
    my $self = shift;
    $self->trace_setenv('GLUNIX_NODES', (join ',', @_));
}


sub processes ($$) {
    my ($self, $processes) = @_;
    $self->{processes} = $processes;
    $self->trace_setenv('TI_NUM_BOXES', $processes);
}


sub run ($@) {
    my $self = shift;
    # DOB: GLUNIX_NODES not required
    # die "machine list needed; use \"--mapping\" or set \$GLUNIX_NODES\n" unless $ENV{GLUNIX_NODES};
    $self->SUPER::run('glurun', "-$self->{processes}", @_);
}


########################################################################


1;
