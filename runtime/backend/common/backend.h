#ifndef _include_tic_backend_h_
#define _include_tic_backend_h_

#include "backend-defines.h"

#include <tic.h>

#ifdef WIDE_POINTERS
  #include "mem-wide.h"
#else
  #include "mem-narrow.h"
#endif

#include "static-global.h"

#include "assign.h"
#include "bulk.h"
#include "deref.h"
#include "comm.h"

#endif /* !_include_tic_backend_h_ */
