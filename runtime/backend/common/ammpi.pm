package common::ammpi;
use base qw(common::tcrun);
use Cwd;

use strict;

#require 'tcrun';

########################################################################


sub run ($$@) {
    my $self = shift;
   
    my $MPIRUN_CMD = main::get_netrun_cmd("MPIRUN_CMD",join(" ",@_),$self->{processes});

    $self->SUPER::run(split / /,$MPIRUN_CMD); 
}

sub processes ($$) {
    my ($self, $processes) = @_;
    $self->{processes} = $processes;
}

########################################################################


1;
