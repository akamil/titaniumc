package common::gasnet_ibv;
use base qw(common::tcrun);
use Cwd;

use strict;

#require 'tcrun';

########################################################################


sub run ($$@) {
    my $self = shift;
   
    my $IBVRUN_CMD = main::get_netrun_cmd("IBVRUN_CMD",join(" ",@_),$self->{processes});

    # bug 493/495 - firehose has safety problems with malloc munmap, so disable it
    $self->trace_setenv("GASNET_DISABLE_MUNMAP", "1") unless defined $ENV{GASNET_DISABLE_MUNMAP};

    $self->SUPER::run(split / /,$IBVRUN_CMD); 
}

sub processes ($$) {
    my ($self, $processes) = @_;
    $self->{processes} = $processes;
}

########################################################################


1;
