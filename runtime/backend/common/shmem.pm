package common::shmem;
use base qw(common::tcrun);
use Cwd;

use strict;


########################################################################


sub run ($$@) {
    my $self = shift;
    my $SHMEMRUN_CMD = "mpirun -np %N %C";
   
    if ($ENV{TI_SHMEMRUN_CMD}) {
      $SHMEMRUN_CMD = $ENV{TI_SHMEMRUN_CMD};
    } 

    my $pwd = cwd;
    my $cmd = join(" ",@_);

    $SHMEMRUN_CMD =~ s/%N/$self->{processes}/g;
    $SHMEMRUN_CMD =~ s/%n/$self->{processes}/g;

    $SHMEMRUN_CMD =~ s/%C/$cmd/g;
    $SHMEMRUN_CMD =~ s/%c/$cmd/g;

    $SHMEMRUN_CMD =~ s/%D/$pwd/g;
    $SHMEMRUN_CMD =~ s/%d/$pwd/g;

    $SHMEMRUN_CMD =~ s/%%/%/g;

    $self->SUPER::run(split / /,$SHMEMRUN_CMD); 
}

sub processes ($$) {
    my ($self, $processes) = @_;
    $self->{processes} = $processes;
}

########################################################################


1;
