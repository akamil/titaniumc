package common::amudp;
use base qw(common::tcrun);

use strict;


########################################################################


my %machines = ( G => { delimiter => ',', envar => 'GLUNIX_NODES',  required => 0 },
		 R => { delimiter => ' ', envar => 'REXEC_SVRS',    required => 0 },
		 S => { delimiter => ' ', envar => 'SSH_SERVERS',   required => 1 },
		 C => { delimiter => ' ', envar => 'TI_CSPAWN_CMD', required => 0 },
		 L => undef );



########################################################################


sub keywords ($%) {
    my ($self, %keywords) = @_;
    
    my $spawn = $ENV{TI_SPAWNFN} || $keywords{DefaultSpawnFunction} || 'S';
    die "unknown AMUDP spawn function: $spawn\n" unless exists $machines{$spawn};
    $self->trace_setenv('TI_SPAWNFN', $spawn);

    if ($spawn eq "C") {
      my $spawncmd = $ENV{TI_CSPAWN_CMD} || $ENV{AMUDP_CSPAWN_CMD} || $keywords{CSpawnCommand};
      die "Need to set \$TI_CSPAWN_CMD or \$AMUDP_CSPAWN_CMD to use custom TI_SPAWNFN\n" unless $spawncmd;  
      $self->trace_setenv('AMUDP_CSPAWN_CMD', $spawncmd);
    }

    $self->{machines} = $machines{$spawn};
}


sub machines ($@) {
    my $self = shift;
    if ($self->{machines}) {
	$self->trace_setenv($self->{machines}->{envar}, (join $self->{machines}->{delimiter}, @_));
    } else {
	$self->SUPER::machines(@_);
    }
}


sub processes ($$) {
    my ($self, $processes) = @_;
    $self->{processes} = $processes;
}


sub run ($$@) {
    my $self = shift;
    my $program = shift;
    
    die "machine list needed; use \"--mapping\" or set \$$self->{machines}->{envar}\n"
	if $self->{machines} && $self->{machines}->{required} && ! $ENV{$self->{machines}->{envar}};
    
    $self->SUPER::run($program, $self->{processes}, @_);
}


########################################################################


1;
