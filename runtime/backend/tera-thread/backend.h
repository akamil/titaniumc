#ifndef _include_tera_thread_backend_h_
#define _include_tera_thread_backend_h_


#define BACKEND_PTHREAD
#define HAVE_MONITORS

#include "mem-narrow.h"
#include "mem-shared.h"

#include "barrier.h"
#include "procs.h"


#endif /* !_include_tera_thread_backend_h_ */
