#ifndef _include_comm_h_
#define _include_comm_h_

#include "config.h"
#include "backend.h"

#include <string.h>

#define ti_write_sync() ((void) 0)
#define ti_read_sync()  ((void) 0)
#define ti_sync()       ((void) 0)

#define ti_bulk_read(d_addr, s_box, s_addr, size, ptr_embedding) \
  (memcpy(d_addr, s_addr, size), ((void) 0))
#define ti_bulk_read_weak(d_addr, s_box, s_addr, size, ptr_embedding) \
  (memcpy(d_addr, s_addr, size), ((void) 0))
#define ti_bulk_write_weak(d_box, d_addr, s_addr, size) \
  (memcpy(d_addr, s_addr, size), ((void) 0))
#define ti_bulk_write(d_box, d_addr, s_addr, size) \
  (memcpy(d_addr, s_addr, size), ((void) 0))

#endif /* _include_comm_h_ */
