#ifndef _include_mem_shared_h_
#define _include_mem_shared_h_


#define MEMORY_SHARED

#include "threads.h"
#include "static-shared.h"

extern int PROCS;

/*
 * When using GCC on Sparc hardware, process ID can be placed in a
 * register.  Otherwise, we need to go through some backend-specific
 * lookup mechanism.
 */

#if !(defined(__GNUC__) && defined(__sparc))
# ifndef NO_PROCID_REGISTER
#  define NO_PROCID_REGISTER
# endif
#endif

#ifndef NO_PROCID_REGISTER
register int MYPROC asm ("g5");
#endif /* !NO_PROCID_REGISTER */

#if 0
#ifdef NO_PROCID_REGISTER

# include <thread.h>

extern int thread_map[100];
# define MYPROC thread_map[thr_self()]

#endif /* NO_PROCID_REGISTER */
#endif

#endif /* !_include_mem_shared_h_ */
