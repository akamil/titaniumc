#ifndef _include_tera_thread_h_
#define _include_tera_thread_h_


void tera_spawn(int, char **);
void tera_spawn_one_cpu(int lo, int hi, int argc, char **argv);

#endif /* !_include_tera_thread_h_ */
