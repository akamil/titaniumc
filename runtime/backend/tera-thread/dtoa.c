#include "tera_sync.h"

static ti_mutex_t *mutex[2];

#define MULTIPLE_THREADS
#define ACQUIRE_DTOA_LOCK( n )  tera_mutex_lock( mutex[ n ] )
#define FREE_DTOA_LOCK( n )     tera_mutex_unlock( mutex[ n ] )
#define INIT_DTOA_LOCK( n )      tera_mutex_alloc_lock( &(mutex[ n ]) )

void tera_dtoa_mutex_init() {
  INIT_DTOA_LOCK(0);
  INIT_DTOA_LOCK(1);
}

#include "../../fp-utils/dtoa-common.c"
