#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <machine/runtime.h>
#include "procs.h"
#include "spawn.h"
#include "tera_sync.h"


static int ti_argc;
static char **ti_argv;


#define MAX_PROCS 8
static int layout[MAX_PROCS];
static int finished[MAX_PROCS];
static int team_id[MAX_PROCS];
static int NUM_CPUS;
static int argc;
static char **argv;

/* 
   Example syntax for TI_NODES:
   "2 3 4": spawn 2 threads on CPU 1, 3 on CPU 2, and 4 on CPU 3. The children
   of these threads will be constrained to their parent CPUs.

   "8": spawn 8 threads on CPU 1. Their children are totally unconstrained.

   There is no way to spawn threads on a single CPU and have constrained
   children, or to spawn threads in an unconstrained manner on multiple CPUs.
*/

/* This function parses a string into the global layout[] array used to spawn 
   off the tasks and threads later. It returns the total number of threads, 
   or 0 if there was an error. It also sets the global var NUM_CPUS to be the 
   total number of CPUs desired. */
int set_ti_nodes(char *ti_nodes) {
  char *tok;
  int proc = 0;
  int total = 0;
  int threads;

  if (!ti_nodes) {
    NUM_CPUS = 1;
    layout[0] = 1;
    return 1;
  }
  tok = strtok(ti_nodes, " ");
  while (tok) {
    if (proc > MAX_PROCS - 1) {
      fprintf(stderr, "Maximum number of processors (%d) exceeded.\n", MAX_PROCS);
      return 0;
    }
    threads = atoi(tok);
    if (threads > 0) {
      layout[proc] = threads;
      total += layout[proc];
    }
    else {
      fprintf(stderr, "Invalid number of threads: %d\n", threads);
      return 0;
    }
    proc++;
    tok = strtok(NULL, " ");
  }
  NUM_CPUS = proc; /* proc is one plus the last array el that got filled. */
  return total;
}

static int current_thread_count;

/* Runs ti_main on the current thread. The "waiter" arg is the location to
   make full when ti_main completes. */
void run_ti_main(void *waiter) {
  /* Fill in the thread id. */
  int my_id = int_fetch_add(&current_thread_count, 1);
  tera_set_task_id(&my_id);
  ti_main(argc, argv);

  /* Tell the parent we're done. */
  writexf((int *)waiter, 1);
}

/* Spawns layout[id] number of threads on the current team. */
void tera_spawn_on_one(void *id_p) {
  /* "id" is the ID of the current team. */
  int id = *((int *)id_p);
  int threads = layout[id];
  /* Since terart_create_thread doesn't have any sort of sync mechanism, we 
     have to synchronize manually. So for now, K.I.S.S, and use a simple
     iterative spawning mechanism. */

  /* This array will store a bunch of F/E vars that will let us know when all
     the spawned threads in this team have completed. */
  int *thr_finished = (int *)malloc(sizeof(int) * threads);

  int thread;

  /* Purge the "thread finished" array. */
  for (thread = 0; thread < threads; thread++) {
    purge(&thr_finished[thread]);
  }

  /* Start up all the threads. */
  
  for (thread = 0; thread < threads; thread++) {
    terart_create_thread_on_team(NULL, run_ti_main, &thr_finished[thread]);
  }

  /* Wait for all the threads. */
  for (thread = 0; thread < threads; thread++) {
    readff(&thr_finished[thread]);
  }

  /* Tell the team spawner we're done. */
  writexf(&finished[id], 1);
}


/* Spawns off the teams on various CPUs given that the layout[] array has been 
   set by set_ti_nodes(). */
void tera_spawn(int local_argc, char **local_argv) {
  unsigned int *team_id_array;
  int proc;
  int actual_teams;

  /* If there is only one CPU, run all threads unconstrained. */
  if (NUM_CPUS == 1) {
    tera_spawn_one_cpu(0, layout[0]-1, local_argc, local_argv); /* Blocks. */
    return;
  }

  team_id_array = (unsigned int *)malloc(sizeof(int) * (NUM_CPUS));
  /* Empty the global finished array. This array lets us wait for a team to 
     finish executing. */
  /* And fill in the team_id array. This array tells a team what index it 
     should use to access the layout and finished arrays. */
  for (proc = 0; proc < NUM_CPUS; proc++) {
    purge(&finished[proc]);
    team_id[proc] = proc;
  }

  /* Set the global argc and argv. These will get copied into the stack by the
     ti_main() function call, so there's no collision problem. */
  argc = local_argc;
  argv = local_argv;

  /* Reset current_thread_count. This is a variable that ensures a globally
     unique id for each thread. */
  current_thread_count = 0;

  /* Spawn the teams. We can't use the batch spawn because they will 
     potentially all have different numbers of threads.
     We spawn one less team that we have because we are currently running in 
     a team right now, which must be good for something. */
  for (proc = 0; proc < NUM_CPUS-1; proc++) {
    terart_create_team(tera_spawn_on_one, (void *)&team_id[proc], 1, &team_id_array[proc]);
  }

  /* Wait for the team create to get done. */
  for (proc = 0; proc < NUM_CPUS-1; proc++) {
    int teamid = readff(&team_id_array[proc]);
    if (teamid == -1) {
      fprintf(stderr, "Warning: Failed to create team #%d\n", proc);
    }
  }

  /* Spawn threads on the current team, too. */
  tera_spawn_on_one((void *)&team_id[NUM_CPUS-1]);

  /* Wait for everybody to finish. tera_spawn_on_one will set the appropriate
     location in the finished[] array when it is done. */
  for (proc = 0; proc < NUM_CPUS-1; proc++) {
    readff(&finished[proc]);
  }
}  

/* Spawns threads from lo to hi, inclusive. This doesn't lock the threads to
   particular processors, meaning that loops can span across CPUs. It also
   waits for the threads to finish. */
void tera_spawn_one_cpu(int lo, int hi, int argc, char **argv) {
  int val;

  int result;
  future int i$, j$;
  int left_child;
  int right_child;

  left_child = lo * 2 + 1;
  right_child = lo * 2 + 2;
  if (left_child <= hi) {
    future i$(left_child, hi, argc, argv) {
      tera_spawn_one_cpu(left_child, hi, argc, argv);
      return 1;
    }
  }
  if (right_child <= hi) {
    future j$(right_child, hi, argc, argv) {
      tera_spawn_one_cpu(right_child, hi, argc, argv);
      return 1;
    }
  }

  /* &lo doesn't dangle because we wait for ti_main. */
  tera_set_task_id(&lo);
  ti_main(argc, argv);

  if (left_child <= hi) {
    result = (i$);
  }
  if (right_child <= hi) {
    result = (j$);
  }

  return;
}

