#ifndef _include_tera_threads_h_
#define _include_tera_threads_h_

#include "tera_sync.h"

#define TI_MUTEX_INIT TERA_MUTEX_INITIALIZER


#define ti_cond_broadcast     tera_cond_broadcast
#define ti_cond_destroy       tera_cond_destroy
#define ti_cond_init(cond)    tera_cond_init((cond),0)
#define ti_cond_signal        tera_cond_signal
#define ti_cond_timedwait     tera_cond_timedwait
#define ti_cond_wait          tera_cond_wait
#define ti_mutex_destroy      tera_mutex_destroy
#define ti_mutex_init(mutex)  tera_mutex_init((mutex))
#define ti_mutex_lock         tera_mutex_lock
#define ti_mutex_unlock       tera_mutex_unlock
#define ti_thread_equal       tera_thread_equal
#define ti_thread_self        tera_get_task_id


#endif /* !_include_tera_threads_h_ */
