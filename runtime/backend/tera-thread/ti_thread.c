#include <stdio.h>
#include <sys/errno.h>
#include <unistd.h>

#include "ti_thread.h"

titanium_thread_list *titanium_threads = NULL;

ti_mutex_t thread_list_lock;
#define LOCK_THREAD_LIST() (ti_mutex_lock(&thread_list_lock))
#define UNLOCK_THREAD_LIST() (ti_mutex_unlock(&thread_list_lock))

titanium_thread_t *find_thread_by_id(ti_thread_t tid) {
  titanium_thread_list *temp = titanium_threads;
  while (temp != NULL) {
    if (temp->thread->tid == tid) {
      return temp->thread;
    }
    temp = temp->cdr;
  }
  return NULL;  
}

/* tid will always be thr_self() */
void thread_cons_monitor(ti_thread_t tid, titanium_monitor_t *monitor) {
  titanium_thread_t *thread;
  titanium_monitor_t *first;

  LOCK_THREAD_LIST();
  thread = find_thread_by_id(tid);
  UNLOCK_THREAD_LIST();
  if (thread == NULL) return;
 
  first = thread->locked_monitors;
  thread->locked_monitors = monitor;
  monitor->cdr = first;
}

/* tid will always be thr_self(); */
void thread_remove_monitor(ti_thread_t tid, titanium_monitor_t *monitor) {
  titanium_thread_t *thread;
  titanium_monitor_t *prev;
  titanium_monitor_t *current;

  LOCK_THREAD_LIST();
  thread = find_thread_by_id(tid);
  UNLOCK_THREAD_LIST();
  if (thread == NULL) return;

  prev = thread->locked_monitors;

  if (prev == NULL) return;
  if (prev == monitor) {
    thread->locked_monitors = NULL;
    return;
  }

  current = prev->cdr;
  while(current != NULL) {
    if (current == monitor) {
      prev->cdr = current->cdr;
      return;
    }
    prev = current;
    current = current->cdr;
  }
}
