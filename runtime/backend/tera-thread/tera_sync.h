#ifndef TERA_SYNC_H
#define TERA_SYNC_H

#include <machine/runtime.h>

typedef struct {
  sync int lock$;
  int waiters;
} ti_cond_t;

typedef struct {
  sync int lock$;
} ti_mutex_t;

typedef int ti_cond_attr;

typedef int ti_thread_t;


/* Block and release mechanism. */
void tera_release(ti_mutex_t *lock);
void tera_block();
void tera_broadcast_release();

/* Thread data. */
void tera_set_task_id(int *id_p);
int tera_get_task_id();
int tera_thread_equal(int id);

/* Initialization. */
void tera_init();

/* Mutexes. */
int tera_mutex_init(ti_mutex_t *lock);
int tera_mutex_lock(ti_mutex_t *lock);
void tera_mutex_alloc_lock(ti_mutex_t **lock);
int tera_mutex_unlock(ti_mutex_t *lock);
int tera_mutex_destroy(ti_mutex_t *lock);

/***** Condition variables. *****/

int tera_cond_broadcast(ti_cond_t *cond);
int tera_cond_destroy(ti_cond_t *cond);
int tera_cond_init(ti_cond_t *cond, ti_cond_attr *attr);
int tera_cond_signal(ti_cond_t *cond);
int tera_cond_timedwait(ti_cond_t *cond, ti_mutex_t *lock);
int tera_cond_wait(ti_cond_t *cond, ti_mutex_t *lock);


#endif


