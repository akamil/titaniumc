#include <stdio.h>
#include <stdlib.h>
#include "ti-gc.h"
#include "procs.h"
#include "spawn.h"
#include "tera_sync.h"


int PROCS;

int main(int argc, char **argv)
{
  const char * envar;

  /* do this *very* early, in case library routines allocate memory */
  ti_mem_thr_init(&argv);

  envar = getenv( "TI_NODES" );

  PROCS = set_ti_nodes(envar);
  if (!PROCS) {
    fprintf(stderr, "Failed to parse TI_NODES.\n");
    return -1;
  }

#if 0
  if (envar)
    {
      char *end;
      PROCS = strtol( envar, &end, 0 );

      if (*end || PROCS <= 0)
	{
	  fprintf (stderr,
		   "%s: (Error) Environment variable TI_NODES must be a number greater than zero: \"%s\".\n",
		   argv[0] ? argv[0] : "<Unknown>",
		   envar ? envar : "<Unknown>");
	  return -1;
	}
    }
  else
    PROCS = 1;
#endif
  
  tera_init();
  tera_spawn(argc, argv);
  return 0;
}
