package gasnet_mpi_smp::tcrun;
use base qw(common::ammpi);

use strict;


########################################################################

sub apportionments ($@) {
    my $self = shift;
    $self->trace_setenv('TI_THREADS', @_);
}

sub run ($@) {
    my $self = shift;

    unless ($ENV{TI_THREADS} || $ENV{TI_PFORP}) {
        my @apportionments = ('1') x $self->{processes};
        $self->apportionments(@apportionments);
        warn "assuming apportionment list \"@apportionments\"\n";
    }


    $self->SUPER::run(@_);
}


########################################################################


1;
