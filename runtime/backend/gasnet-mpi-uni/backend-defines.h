#ifndef _include_gasnet_mpi_uni_backend_defines_h_
#define _include_gasnet_mpi_uni_backend_defines_h_


#define BACKEND_GASNET
#define BACKEND_GASNET_MPI_UNI
#define TIC_BACKEND_NAME "gasnet-mpi-uni"
#define MEMORY_DISTRIBUTED
#define COMM_AM2
#define COMM_GASNET
#define HAVE_MONITORS  
#define WIDE_POINTERS
#define USE_DISTRIBUTED_GC 1

#endif
