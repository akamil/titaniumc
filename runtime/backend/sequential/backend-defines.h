#ifndef _include_uniprocess_backend_defines_h_
#define _include_uniprocess_backend_defines_h_

#define BACKEND_SEQUENTIAL
#define BACKEND_UNIPROC
#define TIC_BACKEND_NAME "sequential"
/* #define BACKEND_MILL */
/* #define BACKEND_MILL_UNIPROCESS */
/* #define MEMORY_DISTRIBUTED */
/* #define MEMORY_SHARED */
/* #define COMM_AM2 */
/* #define COMM_VIA */
/* #define PTHREAD */
/* #define HAVE_MONITORS */
/* #define WIDE_POINTERS */

#endif /* !_include_uniprocess_backend_defines_h_ */
