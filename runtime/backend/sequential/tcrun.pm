package sequential::tcrun;
use base qw(common::tcrun);

use strict;


########################################################################

sub run ($$@) {
    my $self = shift;

    if ($self->{cross_compiling}) {
      my $MPIRUN_CMD = main::get_netrun_cmd("MPIRUN_CMD",join(" ",@_),$self->{processes});
      $self->SUPER::run(split / /,$MPIRUN_CMD);
    } else {
      $self->SUPER::run(@_);
    }
}


1;
