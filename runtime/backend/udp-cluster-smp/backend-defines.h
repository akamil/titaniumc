#ifndef _include_udp_cluster_smp_backend_defines_h_
#define _include_udp_cluster_smp_backend_defines_h_


#define BACKEND_UDP
#define BACKEND_UDP_CLUSTER_SMP
#define TIC_BACKEND_NAME "udp-cluster-smp"
#define MEMORY_DISTRIBUTED
#define MEMORY_SHARED
#define COMM_AM2
#define COMM_AMUDP
#define PTHREAD
#define HAVE_MONITORS
#define WIDE_POINTERS
#define USE_DISTRIBUTED_GC 1

#endif /* !_include_udp_backend_defines_h_ */
