package gasnet_lapi_smp::tcrun;
use base qw(common::tcrun);

use strict;


########################################################################


sub processes ($$) {
    my ($self, $processes) = @_;
    $self->{processes} = $processes;
}

sub apportionments ($@) {
    my $self = shift;
    $self->trace_setenv('TI_THREADS', @_);
}

sub run ($@) {
    my $self = shift;

    unless ($ENV{TI_THREADS} || $ENV{TI_PFORP}) {
        my @apportionments = ('1') x $self->{processes};
        $self->apportionments(@apportionments);
        warn "assuming apportionment list \"@apportionments\"\n";
    }

    $self->trace_setenv('MP_FENCE','--');

    my $progname = shift;

    $self->SUPER::run('poe', $progname, '-nodes', $self->{processes}, 
         '-tasks_per_node', '1', 
         '-rmpool', '1', 
         '-msg_api', 'LAPI', 
         '-euilib', 'us', 
         '-retry', '1', 
         '-retrycount', '10000',
         '--', 
         # bug 1039: poe requires argument overquoting
         map { "\"$_\"" } @_);
}


########################################################################


1;
