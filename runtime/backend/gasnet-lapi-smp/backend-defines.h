#ifndef _include_gasnet_lapi_smp_backend_defines_h_
#define _include_gasnet_lapi_smp_backend_defines_h_


#define BACKEND_GASNET
#define BACKEND_GASNET_LAPI_SMP
#define TIC_BACKEND_NAME "gasnet-lapi-smp"
#define MEMORY_DISTRIBUTED
#define MEMORY_SHARED
#define COMM_AM2
#define COMM_GASNET
#define PTHREAD
#define HAVE_MONITORS
#define WIDE_POINTERS
#define USE_DISTRIBUTED_GC 1

#endif 
