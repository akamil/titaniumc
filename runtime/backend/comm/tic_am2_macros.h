/* tic_am2_macros.h - Tic wrappers around AM entry points - 
 * all AM code should call these rather than calling AM directly 
 */
/* see copyright.txt for usage terms */

#ifndef _TIC_AM2_MACROS_H
#define _TIC_AM2_MACROS_H

#include <primitives.h>

#ifdef COMM_AM2
  /* items common to AM-2 and GASNet */
  EXTERN_DATA size_t TIC_AM_MAX_SHORT;        /* max number of arguments supported by AM */
  EXTERN_DATA size_t TIC_AM_MAX_MEDIUM;       /* max size of medium packet supported by AM */
  EXTERN_DATA size_t TIC_AM_MAX_LONG_REQUEST; /* max size of long request packet supported by AM */
  EXTERN_DATA size_t TIC_AM_MAX_LONG_REPLY;   /* max size of long reply packet supported by AM */
#endif

#ifdef COMM_GASNET
  /* GASNet endpoints are implicit, and there are no bundles */
  typedef gasnet_token_t tic_amtoken_t;
  typedef gasnet_handler_t tic_handler_t;
  typedef gasnet_handlerarg_t tic_handlerarg_t;
  EXTERN_DATA tic_amtoken_t _tic_am_dummytoken;
  #define TIC_AM_DUMMYTOKEN _tic_am_dummytoken
  #define TIC_AM_OK  GASNET_OK

  #define TIC_AM_SEGOFFSET 0

  /* GASNet does not require any locking around the AM layer */
  #define TIC_AM_LOCK()   
  #define TIC_AM_UNLOCK() 
  #define tic_amlock_init()

  /* However, GASNet does require that only HSL locks be used in handlers */
  #define ti_hsl_t                  gasnet_hsl_t
  #define ti_hsl_init(lock)         gasnet_hsl_init(lock)   
  #define ti_hsl_decl_initializer   GASNET_HSL_INITIALIZER
  #define ti_hsl_lock(lock)         gasnet_hsl_lock(lock)
  #define ti_hsl_unlock(lock)       gasnet_hsl_unlock(lock)      

  #define tic_amsize_init() do {                           \
      TIC_AM_MAX_SHORT =        gasnet_AMMaxArgs();        \
      TIC_AM_MAX_MEDIUM =       gasnet_AMMaxMedium();      \
      TIC_AM_MAX_LONG_REQUEST = gasnet_AMMaxLongRequest(); \
      TIC_AM_MAX_LONG_REPLY =   gasnet_AMMaxLongReply();   \
      if (TIC_AM_MAX_LONG_REQUEST > 0x7FFFFFFFu) TIC_AM_MAX_LONG_REQUEST = 0x7FFFFFFF; \
      if (TIC_AM_MAX_LONG_REPLY > 0x7FFFFFFFu) TIC_AM_MAX_LONG_REPLY = 0x7FFFFFFF; \
    } while (0)

#elif defined(COMM_AM2)          
  /* AM2-specific endpoint & bundle data structures */
  EXTERN_DATA eb_t	__tic_AM_bundle;
  EXTERN_DATA ep_t	__tic_AM_endpoint;
  EXTERN_DATA en_t   __tic_AM_endpointname;
  EXTERN_DATA char   __tic_AM_nodedesc[512]; /* for debugging purposes */
  #define TIC_AM_BUNDLE         __tic_AM_bundle
  #define TIC_AM_ENDPOINT       __tic_AM_endpoint
  #define TIC_AM_ENDPOINTNAME   __tic_AM_endpointname
  #define TIC_AM_NODEDESC       __tic_AM_nodedesc
  typedef void * tic_amtoken_t;
  typedef int tic_handler_t;
  typedef int tic_handlerarg_t;
  #define TIC_AM_DUMMYTOKEN NULL
  #ifdef __xlC__
    /* work-around for a bizarre pre-processor bug on xlc */
    static int const TIC_AM_OK = AM_OK;
  #else
    #define TIC_AM_OK  AM_OK
  #endif

  EXTERN_DATA char *TIC_AM_SEGOFFSET;

  EXTERN_DATA ti_lock_t __tic_AM_lock;
  #define TIC_AM_LOCK()      ti_lock(&__tic_AM_lock)
  #define TIC_AM_UNLOCK()    ti_unlock(&__tic_AM_lock)
  #define tic_amlock_init()  ti_lock_init(&__tic_AM_lock)

  /* regular ti_locks are fine, because under AM-2
     when there's only one client thread then all handler
     execution is serialized and runs synchronously on the main thread
   */
  #define ti_hsl_t                  ti_lock_t
  #define ti_hsl_init(lock)         ti_lock_init(lock)   
  #define ti_hsl_decl_initializer   ti_lock_decl_initializer
  #define ti_hsl_lock(lock)         ti_lock(lock)
  #define ti_hsl_unlock(lock)       ti_unlock(lock)

  #define tic_amsize_init() do {                \
      TIC_AM_MAX_SHORT =        AM_MaxShort();  \
      TIC_AM_MAX_MEDIUM =       AM_MaxMedium(); \
      TIC_AM_MAX_LONG_REQUEST = AM_MaxLong();   \
      TIC_AM_MAX_LONG_REPLY =   AM_MaxLong();   \
      if (TIC_AM_MAX_LONG_REQUEST > 0x7FFFFFFFu) TIC_AM_MAX_LONG_REQUEST = 0x7FFFFFFF; \
      if (TIC_AM_MAX_LONG_REPLY > 0x7FFFFFFFu) TIC_AM_MAX_LONG_REPLY = 0x7FFFFFFF; \
    } while (0)

#else /* not AM2 */
  typedef void * tic_amtoken_t;
  typedef int tic_handlerarg_t;

  /* dummies */
  #define TIC_AM_LOCK()   
  #define TIC_AM_UNLOCK() 
  #define tic_amlock_init()

  #define ti_hsl_t                  ti_lock_t
  #define ti_hsl_init(lock)         ti_lock_init(lock)   
  #define ti_hsl_decl_initializer   ti_lock_decl_initializer
  #define ti_hsl_lock(lock)         ti_lock(lock)
  #define ti_hsl_unlock(lock)       ti_unlock(lock)
#endif

#ifdef GASNETI_CONDUIT_THREADS /* gasnet-vapi-*, gasnet-lapi-*, ... */
  /* 
     workaround for bug 493 - it's unsafe to invoke the GC from an AM handler 
     that may be running on a GASNet conduit thread which is hidden from the collector

     ti_malloc_handlersafe/ti_free_handlersafe provide
     allocation and freeing that's safe to invoke from a handler context 
     the region is uncollectable (must be explicitly freed, using ti_free_handlersafe)
     and atomic (may not contain the only pointer to anything in the GC heap)
   */
  #define ti_malloc_handlersafe(s) malloc(s)
  #define ti_free_handlersafe(p)   free(p)                   
#else
  #define ti_malloc_handlersafe(s) ti_malloc_atomic_uncollectable(s)
  #define ti_free_handlersafe(p)   ti_free(p)                   
#endif

typedef void (*tic_handler_fn_t)();  /* prototype for generic handler function */

#define TIC_AM_Safe(__function)                                             \
do {    int __rc;                                                           \
        if_pf ((__rc = __function) != TIC_AM_OK) {                          \
                fprintf(stderr, "ERROR: %s failed with code %d at %s:%i\n", \
                #__function, __rc, __FILE__,__LINE__);                      \
            abort();                                                        \
        }                                                                   \
  } while(0)

#define TIC_AM_Safe_Locked(__function) do { \
  TIC_AM_LOCK();                            \
  TIC_AM_Safe(__function);                  \
  TIC_AM_UNLOCK();                          \
  } while(0)

/* tic_poll() - poll once for incoming AM messages
   tic_poll_until(cond)/tic_poll_while(cond) - poll until/while the given
      condition is true, where cond's value will change upon the arrival
      of a user-level active message (i.e. not just a GASNet put/get)
 */
#ifdef COMM_GASNET
/* rmb below is necessary to ensure that if the caller observes a flag 
 * write by an AM, then it will also observe any data written by that AM,
 * rather than any stale, prefetched values
 * GASNET_BLOCKUNTIL already includes this functionality
 */
  #define tic_poll() do {          \
     TIC_AM_Safe(gasnet_AMPoll()); \
     tic_local_rmb();              \
  } while(0) 

  #define tic_poll_until(cond) GASNET_BLOCKUNTIL(cond)
#else
  #if defined(COMM_AM2)
    #define tic_poll_normb() TIC_AM_Safe_Locked(AM_Poll(TIC_AM_BUNDLE))
  #else
    #define tic_poll_normb() 
  #endif
  #define tic_poll() do { \
    tic_poll_normb();     \
    tic_local_rmb();      \
   } while (0)
  #define tic_poll_until(cond) do {           \
          if (!(cond)) {                      \
            while (1) {                       \
              tic_poll_normb();               \
              if ((cond)) break;              \
              if (politep) tic_sched_yield(); \
              gasnett_spinloop_hint();        \
            }                                 \
          }                                   \
          tic_local_rmb();                    \
    } while (0)
#endif
#define tic_poll_while(cond) tic_poll_until(!(cond))

/* macro for issuing a do-nothing AM reply */
#if AM2_REPLY_REQUIRED
  #define TIC_NULL_REPLY(token) \
    tic_AMReply(0,0,(token, TIC_AMIDX(misc_null_reply)))
#else
  #define TIC_NULL_REPLY(token) 
#endif

/* ------------------------------------------------------------------------------------ */
/* 
  =========================================
    Pointer-width independent AM framework
  =========================================
  AM arguments are always 32-bits wide, but PTR64 platforms need the ability to 
   send their 64-bit pointers as arguments. We do this by packing pointers (and other 64-bit quantities) 
   into 2 arguments. The macros below support this packing/unpacking so you can write the
   calls and handlers once and generate the packing/unpacking code automatically
   as required for the platform.

  See files in this directory for numerous examples of handler definitions and AM calls.

  Here's sample code for declaring a handler:

  .h file declaration:
  --------------------
  TIC_AMSHORT_DECLARE(handler_name, cnt32, cnt64);

     cnt32 = the number of args needed for AM call on PTR32 platform (where each ptr => 1 arg)
     cnt64 = the number of args needed for AM call on PTR64 platform (where each ptr => 2 args)
      (note that 64-bit types like JDOUBLE and JLONG always take 2 arguments)

  handlers.[ch]:
  -------------
  Add an entry to each file, in the obvious place:
    TIC_HANDLER(handler_name),

  .c file definition:
  -------------------
  TI_INLINE(handler_name)
  void handler_name(tic_amtoken_t token, 
    tic_handlerarg_t val, tic_handlerarg_t nbytes, void *dest, void *op) {
    memset(dest, (int)val, nbytes);

    tic_AMReplyShort(cnt32,cnt64,(token, TIC_AMIDX(some_other_handler_name),
                    TIC_AMSEND_PTR(op), 20, TIC_AMSEND_JLONG(0)));
  }
  TIC_AMSHORT(handler_name,cnt32,cnt64,
                (token, a0, a1, TIC_AMRECV_PTR32(a2),     TIC_AMRECV_PTR32(a3)     ),
                (token, a0, a1, TIC_AMRECV_PTR64(a2, a3), TIC_AMRECV_PTR64(a4, a5)));
  --------------------

  On the handler wrapper declaration:
   the first set of parenthesized args is used on 32-bit platforms
   the second set of parenthesized args is used on 64-bit platforms
   TIC_AMSHORT is replaced with TIC_AMMEDIUM or TIC_AMLONG for med/long handlers

  On the AM request/reply calls:
    tic_AM{Request,Reply}{Short,Medium,Long} are used to send AM's
    note that TIC_AMIDX(handler_name) is used to specify the handler index
    cnt32,cnt64 are the number of args for 32 and 64-bit platforms, respectively
    the parenthesized args are the actual args to the AM Request/Reply call
    these handlerarg pack/unpack macros are used for sending arguments:
     TIC_AMSEND_PTR() and TIC_AMSEND_JLONG() and TIC_AMSEND_JDOUBLE() and TIC_AMSEND_JFLOAT()
    and these corresponding macros are used to receive the arguments in the 
    wrapper declaration for the destination AM handler:
     TIC_AMRECV_PTR{32,64}() and TIC_AMRECV_JLONG() and TIC_AMRECV_JDOUBLE() and TIC_AMRECV_JFLOAT()

  On sending static data and function pointers:
    It is NOT safe to assume that static data and function pointers have the 
    same virtual address on all boxes.  Whenever sending the address of a 
    piece of static data (computed using the address of the local copy), 
    you must first translate the address into the remote box's address range by
    using TIC_TRANSLATE_CSTATIC_ADDR(&mystaticdata, targetboxid).
    Similarly, when sending function pointers for execution by a remote
    box, you should use TIC_TRANSLATE_FUNCTION_ADDR(&myfunction, targetboxid)
*/

#if defined(PTR32) && !defined(PTR64)
  #define PTR_BITS 32
#elif defined(PTR64) && !defined(PTR32)
  #define PTR_BITS 64
#else
  #error Must define one of PTR32 or PTR64!
#endif

#define TIC_AMIDX_ENUMENTRY(hname)  tic_AMhidx_ ## hname
#define TIC_AMIDX(hname)            ((tic_handler_t)TIC_AMIDX_ENUMENTRY(hname))
#define TIC_AMWRAPPER_NAME(hname)   TIC_CONCAT(tic_AMhandler_ ## hname ## _, PTR_BITS)
#define TIC_AMHANDLER_ENTRY(hname)  { TIC_AMIDX(hname), (tic_handler_fn_t)TIC_AMWRAPPER_NAME(hname) }

#ifdef PTR32
  #define TIC_AMSHORT_DECLARE(hname, cnt32, cnt64) \
    extern void TIC_AMWRAPPER_NAME(hname)(tic_amtoken_t token TIC_ARGS##cnt32)
  #define TIC_AMMEDIUM_DECLARE(hname, cnt32, cnt64) \
    extern void TIC_AMWRAPPER_NAME(hname)(tic_amtoken_t token, void *addr, size_t nbytes TIC_ARGS##cnt32)
  #define TIC_AMLONG_DECLARE(hname, cnt32, cnt64) \
    extern void TIC_AMWRAPPER_NAME(hname)(tic_amtoken_t token, void *addr, size_t nbytes TIC_ARGS##cnt32)
#else
  #define TIC_AMSHORT_DECLARE(hname, cnt32, cnt64) \
    extern void TIC_AMWRAPPER_NAME(hname)(tic_amtoken_t token TIC_ARGS##cnt64)
  #define TIC_AMMEDIUM_DECLARE(hname, cnt32, cnt64) \
    extern void TIC_AMWRAPPER_NAME(hname)(tic_amtoken_t token, void *addr, size_t nbytes TIC_ARGS##cnt64)
  #define TIC_AMLONG_DECLARE(hname, cnt32, cnt64) \
    extern void TIC_AMWRAPPER_NAME(hname)(tic_amtoken_t token, void *addr, size_t nbytes TIC_ARGS##cnt64)
#endif

/* AM send call macros (use instead of direct calls to e.g. tic_AMRequestShort2)
   args = parenthesized argument list for AM request/reply call, 
     with any pointer args wrapped in a TIC_AMSEND_PTR() macro
     any 64-bit args wrapped in TIC_AMSEND_JLONG() or TIC_AMSEND_JDOUBLE()
     float arguments must be wrapped in TIC_AMSEND_JFLOAT()
 */
#if defined(PTR32)
  #define tic_AMRequest(cnt32, cnt64, args)          tic_AMRequest ## cnt32 args
  #define tic_AMReply(cnt32, cnt64, args)            tic_AMReply ## cnt32 args
  #define tic_AMRequestI(cnt32, cnt64, args)         tic_AMRequestI ## cnt32 args
  #define tic_AMReplyI(cnt32, cnt64, args)           tic_AMReplyI ## cnt32 args
  #define tic_AMRequestXfer(cnt32, cnt64, args)      tic_AMRequestXfer ## cnt32 args
  #define tic_AMReplyXfer(cnt32, cnt64, args)        tic_AMReplyXfer ## cnt32 args
  #define tic_AMRequestXferAsync(cnt32, cnt64, args) tic_AMRequestXferAsync ## cnt32 args
#elif defined(PTR64)
  #define tic_AMRequest(cnt32, cnt64, args)          tic_AMRequest ## cnt64 args
  #define tic_AMReply(cnt32, cnt64, args)            tic_AMReply ## cnt64 args
  #define tic_AMRequestI(cnt32, cnt64, args)         tic_AMRequestI ## cnt64 args
  #define tic_AMReplyI(cnt32, cnt64, args)           tic_AMReplyI ## cnt64 args
  #define tic_AMRequestXfer(cnt32, cnt64, args)      tic_AMRequestXfer ## cnt64 args
  #define tic_AMReplyXfer(cnt32, cnt64, args)        tic_AMReplyXfer ## cnt64 args
  #define tic_AMRequestXferAsync(cnt32, cnt64, args) tic_AMRequestXferAsync ## cnt64 args
#endif

/* splitting and reassembling 64-bit quantities */
#define TIC_MAKEWORD(hi,lo) ((((julong)(hi)) << 32) | (((julong)(lo)) & 0xFFFFFFFF))
#define TIC_HIWORD(arg)     ((juint)(((julong)(arg)) >> 32))
#define TIC_LOWORD(arg)     ((juint)((julong)(arg)))

/* pointer and 64-bit value packing/unpacking helper macros */
#if defined(PTR32)
  #define TIC_AMSEND_PTR(ptr) ((tic_handlerarg_t)ptr)
  #define TIC_AMRECV_PTR32(a0) ((void *)a0)
#elif defined(PTR64)
  #define TIC_AMSEND_PTR(ptr) ((tic_handlerarg_t)TIC_HIWORD(ptr)), ((tic_handlerarg_t)TIC_LOWORD(ptr))
  #define TIC_AMRECV_PTR64(a0,a1) ((void *)TIC_MAKEWORD(a0,a1))
  /* TIC_AMRECV_PTR32 and TIC_AMRECV_PTR64 must differ in name to work-around a 
     bug in the MIPSPro C preprocessor (expands all args regardless of substitution)
  */
#endif
#define TIC_AMSEND_64(type,val)     ((tic_handlerarg_t)TIC_HIWORD(val)), ((tic_handlerarg_t)TIC_LOWORD(val))
#define TIC_AMRECV_64(type,a0,a1)   ((type)TIC_MAKEWORD(a0,a1))

#define TIC_AMSEND_JLONG(val)     TIC_AMSEND_64(jlong,val)
#define TIC_AMRECV_JLONG(a0,a1)   TIC_AMRECV_64(jlong,a0,a1)

/* floating point types need special attention to ensure we get 
   a bit-for-bit copy and not a conversion to integral value 
   also, must talk unsigned instead of double as args because some machines
   pass floats/doubles in fp registers.
*/
#define TIC_AMSEND_JFLOAT(val)    (_tic_jfloattohandlerarg(val))
#define TIC_AMRECV_JFLOAT(a0)     (_tic_handlerargtojfloat(a0))
TI_INLINE(_tic_jfloattohandlerarg)
tic_handlerarg_t _tic_jfloattohandlerarg(jfloat val) {
  union { juint ival; jfloat fval; } x;
  x.fval = val;
  return (tic_handlerarg_t)x.ival;
}
TI_INLINE(_tic_handlerargtojfloat)
jdouble _tic_handlerargtojfloat(tic_handlerarg_t a0) {
  union { juint ival; jfloat fval; } x;
  x.ival = (juint)a0;
  return x.fval;
}

#define TIC_AMSEND_JDOUBLE(val)   (_tic_jdoubletohiarg(val)), (_tic_jdoubletoloarg(val))
#define TIC_AMRECV_JDOUBLE(a0,a1) (_tic_handlerargtojdouble(a0,a1))
TI_INLINE(_tic_jdoubletohiarg)
tic_handlerarg_t _tic_jdoubletohiarg(jdouble val) {
  union { julong ival; jdouble dval; } x;
  x.dval = val;
  return (tic_handlerarg_t)TIC_HIWORD(x.ival);
}
TI_INLINE(_tic_jdoubletoloarg)
tic_handlerarg_t _tic_jdoubletoloarg(jdouble val) {
  union { julong ival; jdouble dval; } x;
  x.dval = val;
  return (tic_handlerarg_t)TIC_LOWORD(x.ival);
}
TI_INLINE(_tic_handlerargtojdouble)
jdouble _tic_handlerargtojdouble(tic_handlerarg_t a0, tic_handlerarg_t a1) {
  union { julong ival; jdouble dval; } x;
  x.ival = TIC_MAKEWORD(a0,a1);
  return x.dval;
}

#if defined(PTR32)
  #define TIC_AMSHORT(hname, cnt32, cnt64, innerargs32, innerargs64)               \
    extern void TIC_AMWRAPPER_NAME(hname)(tic_amtoken_t token TIC_ARGS ## cnt32) { \
      hname innerargs32 ;                                                          \
    } static int _dummy_##hname = sizeof(_dummy_##hname) 
  #define TIC_AMMEDIUM(hname, cnt32, cnt64, innerargs32, innerargs64)                    \
    extern void TIC_AMWRAPPER_NAME(hname)(tic_amtoken_t token, void *addr, size_t nbytes \
                           TIC_ARGS ## cnt32) {                                          \
      hname innerargs32 ;                                                                \
    } static int _dummy_##hname = sizeof(_dummy_##hname) 
  #define TIC_AMLONG(hname, cnt32, cnt64, innerargs32, innerargs64)                      \
    extern void TIC_AMWRAPPER_NAME(hname)(tic_amtoken_t token, void *addr, size_t nbytes \
                           TIC_ARGS ## cnt32) {                                          \
      hname innerargs32 ;                                                                \
    } static int _dummy_##hname = sizeof(_dummy_##hname) 
#elif defined(PTR64)
  #define TIC_AMSHORT(hname, cnt32, cnt64, innerargs32, innerargs64)               \
    extern void TIC_AMWRAPPER_NAME(hname)(tic_amtoken_t token TIC_ARGS ## cnt64) { \
      hname innerargs64 ;                                                          \
    } static int _dummy_##hname = sizeof(_dummy_##hname) 
  #define TIC_AMMEDIUM(hname, cnt32, cnt64, innerargs32, innerargs64)                    \
    extern void TIC_AMWRAPPER_NAME(hname)(tic_amtoken_t token, void *addr, size_t nbytes \
                           TIC_ARGS ## cnt64) {                                          \
      hname innerargs64 ;                                                                \
    } static int _dummy_##hname = sizeof(_dummy_##hname) 
  #define TIC_AMLONG(hname, cnt32, cnt64, innerargs32, innerargs64)                      \
    extern void TIC_AMWRAPPER_NAME(hname)(tic_amtoken_t token, void *addr, size_t nbytes \
                           TIC_ARGS ## cnt64) {                                          \
      hname innerargs64 ;                                                                \
    } static int _dummy_##hname = sizeof(_dummy_##hname) 
#endif

/* ------------------------------------------------------------------------------------ */
/* rest of file is ugly macros which are necessary to implement the above framework
   you should never need to modify anything beneath this line
 */

#if defined(COMM_AMNOW) || defined(COMM_AMVIA) || defined(COMM_AMLAPI)
/* Some questionable AM implementations (ahem, AMNOW) leave out some crucial entry points 
   There's no guarantee this hack will actually work - it depends on the calling convention
    used for the handlers, because this will call the destination handler with more arguments 
    than it was declared to accept, and assumes they will be ignored
*/
  #define AM_Request1(endpoint, dest, handler, a) \
        AM_Request4(endpoint, dest, handler, a, 0, 0, 0)
  #define AM_Request2(endpoint, dest, handler, a, b) \
        AM_Request4(endpoint, dest, handler, a, b, 0, 0)
  #define AM_Request3(endpoint, dest, handler, a, b, c) \
        AM_Request4(endpoint, dest, handler, a, b, c, 0)
  #define AM_Request5(endpoint, dest, handler, a, b, c, d, e) \
        AM_Request8(endpoint, dest, handler, a, b, c, d, e, 0, 0, 0)
  #define AM_Request6(endpoint, dest, handler, a, b, c, d, e, f) \
        AM_Request8(endpoint, dest, handler, a, b, c, d, e, f, 0, 0)
  #define AM_Request7(endpoint, dest, handler, a, b, c, d, e, f, g) \
        AM_Request8(endpoint, dest, handler, a, b, c, d, e, f, g, 0)

  #define AM_RequestI1(endpoint, dest, handler, sourceaddr, nbytes, a) \
        AM_RequestI4(endpoint, dest, handler, sourceaddr, nbytes, a, 0, 0, 0)
  #define AM_RequestI2(endpoint, dest, handler, sourceaddr, nbytes, a, b) \
        AM_RequestI4(endpoint, dest, handler, sourceaddr, nbytes, a, b, 0, 0)
  #define AM_RequestI3(endpoint, dest, handler, sourceaddr, nbytes, a, b, c) \
        AM_RequestI4(endpoint, dest, handler, sourceaddr, nbytes, a, b, c, 0)
  #define AM_RequestI5(endpoint, dest, handler, sourceaddr, nbytes, a, b, c, d, e) \
        AM_RequestI8(endpoint, dest, handler, sourceaddr, nbytes, a, b, c, d, e, 0, 0, 0)
  #define AM_RequestI6(endpoint, dest, handler, sourceaddr, nbytes, a, b, c, d, e, f) \
        AM_RequestI8(endpoint, dest, handler, sourceaddr, nbytes, a, b, c, d, e, f, 0, 0)
  #define AM_RequestI7(endpoint, dest, handler, sourceaddr, nbytes, a, b, c, d, e, f, g) \
        AM_RequestI8(endpoint, dest, handler, sourceaddr, nbytes, a, b, c, d, e, f, g, 0)

  #define AM_RequestXfer1(endpoint, dest, destoffset, handler, sourceaddr, nbytes, a) \
        AM_RequestXfer4(endpoint, dest, destoffset, handler, sourceaddr, nbytes, a, 0, 0, 0)
  #define AM_RequestXfer2(endpoint, dest, destoffset, handler, sourceaddr, nbytes, a, b) \
        AM_RequestXfer4(endpoint, dest, destoffset, handler, sourceaddr, nbytes, a, b, 0, 0)
  #define AM_RequestXfer3(endpoint, dest, destoffset, handler, sourceaddr, nbytes, a, b, c) \
        AM_RequestXfer4(endpoint, dest, destoffset, handler, sourceaddr, nbytes, a, b, c, 0)
  #define AM_RequestXfer5(endpoint, dest, destoffset, handler, sourceaddr, nbytes, a, b, c, d, e) \
        AM_RequestXfer8(endpoint, dest, destoffset, handler, sourceaddr, nbytes, a, b, c, d, e, 0, 0, 0)
  #define AM_RequestXfer6(endpoint, dest, destoffset, handler, sourceaddr, nbytes, a, b, c, d, e, f) \
        AM_RequestXfer8(endpoint, dest, destoffset, handler, sourceaddr, nbytes, a, b, c, d, e, f, 0, 0)
  #define AM_RequestXfer7(endpoint, dest, destoffset, handler, sourceaddr, nbytes, a, b, c, d, e, f, g) \
        AM_RequestXfer8(endpoint, dest, destoffset, handler, sourceaddr, nbytes, a, b, c, d, e, f, g, 0)

  #define AM_Reply1(token, handler, a) \
        AM_Reply4(token, handler, a, 0, 0, 0)
  #define AM_Reply2(token, handler, a, b) \
        AM_Reply4(token, handler, a, b, 0, 0)
  #define AM_Reply3(token, handler, a, b, c) \
        AM_Reply4(token, handler, a, b, c, 0)
  #define AM_Reply5(token, handler, a, b, c, d, e) \
        AM_Reply8(token, handler, a, b, c, d, e, 0, 0, 0)
  #define AM_Reply6(token, handler, a, b, c, d, e, f) \
        AM_Reply8(token, handler, a, b, c, d, e, f, 0, 0)
  #define AM_Reply7(token, handler, a, b, c, d, e, f, g) \
        AM_Reply8(token, handler, a, b, c, d, e, f, g, 0)

  #define AM_ReplyI1(token, handler, sourceaddr, nbytes, a) \
        AM_ReplyI4(token, handler, sourceaddr, nbytes, a, 0, 0, 0)
  #define AM_ReplyI2(token, handler, sourceaddr, nbytes, a, b) \
        AM_ReplyI4(token, handler, sourceaddr, nbytes, a, b, 0, 0)
  #define AM_ReplyI3(token, handler, sourceaddr, nbytes, a, b, c) \
        AM_ReplyI4(token, handler, sourceaddr, nbytes, a, b, c, 0)
  #define AM_ReplyI5(token, handler, sourceaddr, nbytes, a, b, c, d, e) \
        AM_ReplyI8(token, handler, sourceaddr, nbytes, a, b, c, d, e, 0, 0, 0)
  #define AM_ReplyI6(token, handler, sourceaddr, nbytes, a, b, c, d, e, f) \
        AM_ReplyI8(token, handler, sourceaddr, nbytes, a, b, c, d, e, f, 0, 0)
  #define AM_ReplyI7(token, handler, sourceaddr, nbytes, a, b, c, d, e, f, g) \
        AM_ReplyI8(token, handler, sourceaddr, nbytes, a, b, c, d, e, f, g, 0)

  #define AM_ReplyXfer1(token, destoffset, handler, sourceaddr, nbytes, a) \
        AM_ReplyXfer4(token, destoffset, handler, sourceaddr, nbytes, a, 0, 0, 0)
  #define AM_ReplyXfer2(token, destoffset, handler, sourceaddr, nbytes, a, b) \
        AM_ReplyXfer4(token, destoffset, handler, sourceaddr, nbytes, a, b, 0, 0)
  #define AM_ReplyXfer3(token, destoffset, handler, sourceaddr, nbytes, a, b, c) \
        AM_ReplyXfer4(token, destoffset, handler, sourceaddr, nbytes, a, b, c, 0)
  #define AM_ReplyXfer5(token, destoffset, handler, sourceaddr, nbytes, a, b, c, d, e) \
        AM_ReplyXfer8(token, destoffset, handler, sourceaddr, nbytes, a, b, c, d, e, 0, 0, 0)
  #define AM_ReplyXfer6(token, destoffset, handler, sourceaddr, nbytes, a, b, c, d, e, f) \
        AM_ReplyXfer8(token, destoffset, handler, sourceaddr, nbytes, a, b, c, d, e, f, 0, 0)
  #define AM_ReplyXfer7(token, destoffset, handler, sourceaddr, nbytes, a, b, c, d, e, f, g) \
        AM_ReplyXfer8(token, destoffset, handler, sourceaddr, nbytes, a, b, c, d, e, f, g, 0)
#endif

#ifdef COMM_AMVIA
  #define AM_RequestXfer4(request_endpoint, reply_endpoint, dest_offset,   \
                          handler, source_addr, nbytes, arg0, arg1,        \
                          arg2, arg3)                                      \
          AM_RequestXfer8(request_endpoint, reply_endpoint, dest_offset,   \
                          handler, source_addr, nbytes, arg0, arg1, arg2,  \
                          arg3, 0, 0, 0, 0)

  #define AM_ReplyXfer4(token, dest_offset, handler, source_addr,  \
                        nbytes, arg0, arg1, arg2, arg3)            \
          AM_ReplyXfer8(token, dest_offset, handler, source_addr,  \
                        nbytes, arg0, arg1, arg2, arg3, 0, 0, 0, 0)
#endif /* COMM_AMVIA */

/* argument-list macros */
#define TIC_ARGS0
#define TIC_ARGS1 , tic_handlerarg_t a0
#define TIC_ARGS2 , tic_handlerarg_t a0, tic_handlerarg_t a1
#define TIC_ARGS3 , tic_handlerarg_t a0, tic_handlerarg_t a1, tic_handlerarg_t a2
#define TIC_ARGS4 , tic_handlerarg_t a0, tic_handlerarg_t a1, tic_handlerarg_t a2, tic_handlerarg_t a3

#define TIC_ARGS5 , tic_handlerarg_t a0, tic_handlerarg_t a1, tic_handlerarg_t a2, tic_handlerarg_t a3, \
                    tic_handlerarg_t a4
#define TIC_ARGS6 , tic_handlerarg_t a0, tic_handlerarg_t a1, tic_handlerarg_t a2, tic_handlerarg_t a3, \
                    tic_handlerarg_t a4, tic_handlerarg_t a5
#define TIC_ARGS7 , tic_handlerarg_t a0, tic_handlerarg_t a1, tic_handlerarg_t a2, tic_handlerarg_t a3, \
                    tic_handlerarg_t a4, tic_handlerarg_t a5, tic_handlerarg_t a6
#define TIC_ARGS8 , tic_handlerarg_t a0, tic_handlerarg_t a1, tic_handlerarg_t a2, tic_handlerarg_t a3, \
                    tic_handlerarg_t a4, tic_handlerarg_t a5, tic_handlerarg_t a6, tic_handlerarg_t a7

#define TIC_ARGS9  , tic_handlerarg_t a0,  tic_handlerarg_t a1,  tic_handlerarg_t a2,  tic_handlerarg_t a3,  \
                     tic_handlerarg_t a4,  tic_handlerarg_t a5,  tic_handlerarg_t a6,  tic_handlerarg_t a7,  \
                     tic_handlerarg_t a8
#define TIC_ARGS10 , tic_handlerarg_t a0,  tic_handlerarg_t a1,  tic_handlerarg_t a2,  tic_handlerarg_t a3,  \
                     tic_handlerarg_t a4,  tic_handlerarg_t a5,  tic_handlerarg_t a6,  tic_handlerarg_t a7,  \
                     tic_handlerarg_t a8,  tic_handlerarg_t a9
#define TIC_ARGS11 , tic_handlerarg_t a0,  tic_handlerarg_t a1,  tic_handlerarg_t a2,  tic_handlerarg_t a3,  \
                     tic_handlerarg_t a4,  tic_handlerarg_t a5,  tic_handlerarg_t a6,  tic_handlerarg_t a7,  \
                     tic_handlerarg_t a8,  tic_handlerarg_t a9,  tic_handlerarg_t a10
#define TIC_ARGS12 , tic_handlerarg_t a0,  tic_handlerarg_t a1,  tic_handlerarg_t a2,  tic_handlerarg_t a3,  \
                     tic_handlerarg_t a4,  tic_handlerarg_t a5,  tic_handlerarg_t a6,  tic_handlerarg_t a7,  \
                     tic_handlerarg_t a8,  tic_handlerarg_t a9,  tic_handlerarg_t a10, tic_handlerarg_t a11


#define TIC_ARGS13 , tic_handlerarg_t a0,  tic_handlerarg_t a1,  tic_handlerarg_t a2,  tic_handlerarg_t a3,  \
                     tic_handlerarg_t a4,  tic_handlerarg_t a5,  tic_handlerarg_t a6,  tic_handlerarg_t a7,  \
                     tic_handlerarg_t a8,  tic_handlerarg_t a9,  tic_handlerarg_t a10, tic_handlerarg_t a11, \
                     tic_handlerarg_t a12
#define TIC_ARGS14 , tic_handlerarg_t a0,  tic_handlerarg_t a1,  tic_handlerarg_t a2,  tic_handlerarg_t a3,  \
                     tic_handlerarg_t a4,  tic_handlerarg_t a5,  tic_handlerarg_t a6,  tic_handlerarg_t a7,  \
                     tic_handlerarg_t a8,  tic_handlerarg_t a9,  tic_handlerarg_t a10, tic_handlerarg_t a11, \
                     tic_handlerarg_t a12, tic_handlerarg_t a13
#define TIC_ARGS15 , tic_handlerarg_t a0,  tic_handlerarg_t a1,  tic_handlerarg_t a2,  tic_handlerarg_t a3,  \
                     tic_handlerarg_t a4,  tic_handlerarg_t a5,  tic_handlerarg_t a6,  tic_handlerarg_t a7,  \
                     tic_handlerarg_t a8,  tic_handlerarg_t a9,  tic_handlerarg_t a10, tic_handlerarg_t a11, \
                     tic_handlerarg_t a12, tic_handlerarg_t a13, tic_handlerarg_t a14
#define TIC_ARGS16 , tic_handlerarg_t a0,  tic_handlerarg_t a1,  tic_handlerarg_t a2,  tic_handlerarg_t a3,  \
                     tic_handlerarg_t a4,  tic_handlerarg_t a5,  tic_handlerarg_t a6,  tic_handlerarg_t a7,  \
                     tic_handlerarg_t a8,  tic_handlerarg_t a9,  tic_handlerarg_t a10, tic_handlerarg_t a11, \
                     tic_handlerarg_t a12, tic_handlerarg_t a13, tic_handlerarg_t a14, tic_handlerarg_t a15

#define TIC_ARGNAMES0
#define TIC_ARGNAMES1 , a0
#define TIC_ARGNAMES2 , a0, a1
#define TIC_ARGNAMES3 , a0, a1, a2
#define TIC_ARGNAMES4 , a0, a1, a2, a3

#define TIC_ARGNAMES5 , a0, a1, a2, a3, a4
#define TIC_ARGNAMES6 , a0, a1, a2, a3, a4, a5
#define TIC_ARGNAMES7 , a0, a1, a2, a3, a4, a5, a6
#define TIC_ARGNAMES8 , a0, a1, a2, a3, a4, a5, a6, a7

#define TIC_ARGNAMES9  , a0, a1, a2, a3, a4, a5, a6, a7, a8
#define TIC_ARGNAMES10 , a0, a1, a2, a3, a4, a5, a6, a7, a8, a9
#define TIC_ARGNAMES11 , a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10
#define TIC_ARGNAMES12 , a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11

#define TIC_ARGNAMES13 , a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12
#define TIC_ARGNAMES14 , a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13
#define TIC_ARGNAMES15 , a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14
#define TIC_ARGNAMES16 , a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15

/* ------------------------------------------------------------------------------------ */

#ifdef COMM_GASNET

/* GASNet requests - rename, strip the endpoint argument, and reorder the arguments for Long */

#define tic_AMRequest0(dest,handler)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort0(dest,handler))
#define tic_AMRequest1(dest,handler,a1)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort1(dest,handler,a1))
#define tic_AMRequest2(dest,handler,a1,a2)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort2(dest,handler,a1,a2))
#define tic_AMRequest3(dest,handler,a1,a2,a3)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort3(dest,handler,a1,a2,a3))
#define tic_AMRequest4(dest,handler,a1,a2,a3,a4)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort4(dest,handler,a1,a2,a3,a4))
#define tic_AMRequest5(dest,handler,a1,a2,a3,a4,a5)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort5(dest,handler,a1,a2,a3,a4,a5))
#define tic_AMRequest6(dest,handler,a1,a2,a3,a4,a5,a6)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort6(dest,handler,a1,a2,a3,a4,a5,a6))
#define tic_AMRequest7(dest,handler,a1,a2,a3,a4,a5,a6,a7)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort7(dest,handler,a1,a2,a3,a4,a5,a6,a7))
#define tic_AMRequest8(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort8(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8))
#define tic_AMRequest9(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort9(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9))
#define tic_AMRequest10(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort10(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10))
#define tic_AMRequest11(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort11(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11))
#define tic_AMRequest12(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort12(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12))
#define tic_AMRequest13(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort13(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13))
#define tic_AMRequest14(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort14(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14))
#define tic_AMRequest15(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort15(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15))
#define tic_AMRequest16(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestShort16(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16))


#define tic_AMRequestI0(dest,handler,sourceaddr,nbytes)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium0(dest,handler,sourceaddr,nbytes))
#define tic_AMRequestI1(dest,handler,sourceaddr,nbytes,a1)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium1(dest,handler,sourceaddr,nbytes,a1))
#define tic_AMRequestI2(dest,handler,sourceaddr,nbytes,a1,a2)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium2(dest,handler,sourceaddr,nbytes,a1,a2))
#define tic_AMRequestI3(dest,handler,sourceaddr,nbytes,a1,a2,a3)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium3(dest,handler,sourceaddr,nbytes,a1,a2,a3))
#define tic_AMRequestI4(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium4(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4))
#define tic_AMRequestI5(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium5(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5))
#define tic_AMRequestI6(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium6(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6))
#define tic_AMRequestI7(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium7(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7))
#define tic_AMRequestI8(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium8(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8))
#define tic_AMRequestI9(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium9(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9))
#define tic_AMRequestI10(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium10(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10))
#define tic_AMRequestI11(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium11(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11))
#define tic_AMRequestI12(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium12(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12))
#define tic_AMRequestI13(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium13(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13))
#define tic_AMRequestI14(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium14(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14))
#define tic_AMRequestI15(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium15(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15))
#define tic_AMRequestI16(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestMedium16(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16))


#define tic_AMRequestXfer0(dest,destaddr,handler,sourceaddr,nbytes)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong0(dest,handler,sourceaddr,nbytes,(void*)(destaddr)))
#define tic_AMRequestXfer1(dest,destaddr,handler,sourceaddr,nbytes,a1)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong1(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1))
#define tic_AMRequestXfer2(dest,destaddr,handler,sourceaddr,nbytes,a1,a2)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong2(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2))
#define tic_AMRequestXfer3(dest,destaddr,handler,sourceaddr,nbytes,a1,a2,a3)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong3(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3))
#define tic_AMRequestXfer4(dest,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong4(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4))
#define tic_AMRequestXfer5(dest,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong5(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5))
#define tic_AMRequestXfer6(dest,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong6(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6))
#define tic_AMRequestXfer7(dest,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong7(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7))
#define tic_AMRequestXfer8(dest,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong8(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8))
#define tic_AMRequestXfer9(dest,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong9(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9))
#define tic_AMRequestXfer10(dest,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong10(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9,a10))
#define tic_AMRequestXfer11(dest,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong11(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11))
#define tic_AMRequestXfer12(dest,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong12(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12))
#define tic_AMRequestXfer13(dest,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong13(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13))
#define tic_AMRequestXfer14(dest,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong14(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14))
#define tic_AMRequestXfer15(dest,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong15(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15))
#define tic_AMRequestXfer16(dest,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16)    \
  TIC_AM_Safe_Locked(gasnet_AMRequestLong16(dest,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16))

/* GASNet replies - rename, reorder the arguments for Long */

#define tic_AMReply0(token,handler)    \
  TIC_AM_Safe(gasnet_AMReplyShort0(token,handler))
#define tic_AMReply1(token,handler,a1)    \
  TIC_AM_Safe(gasnet_AMReplyShort1(token,handler,a1))
#define tic_AMReply2(token,handler,a1,a2)    \
  TIC_AM_Safe(gasnet_AMReplyShort2(token,handler,a1,a2))
#define tic_AMReply3(token,handler,a1,a2,a3)    \
  TIC_AM_Safe(gasnet_AMReplyShort3(token,handler,a1,a2,a3))
#define tic_AMReply4(token,handler,a1,a2,a3,a4)    \
  TIC_AM_Safe(gasnet_AMReplyShort4(token,handler,a1,a2,a3,a4))
#define tic_AMReply5(token,handler,a1,a2,a3,a4,a5)    \
  TIC_AM_Safe(gasnet_AMReplyShort5(token,handler,a1,a2,a3,a4,a5))
#define tic_AMReply6(token,handler,a1,a2,a3,a4,a5,a6)    \
  TIC_AM_Safe(gasnet_AMReplyShort6(token,handler,a1,a2,a3,a4,a5,a6))
#define tic_AMReply7(token,handler,a1,a2,a3,a4,a5,a6,a7)    \
  TIC_AM_Safe(gasnet_AMReplyShort7(token,handler,a1,a2,a3,a4,a5,a6,a7))
#define tic_AMReply8(token,handler,a1,a2,a3,a4,a5,a6,a7,a8)    \
  TIC_AM_Safe(gasnet_AMReplyShort8(token,handler,a1,a2,a3,a4,a5,a6,a7,a8))
#define tic_AMReply9(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9)    \
  TIC_AM_Safe(gasnet_AMReplyShort9(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9))
#define tic_AMReply10(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10)    \
  TIC_AM_Safe(gasnet_AMReplyShort10(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10))
#define tic_AMReply11(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11)    \
  TIC_AM_Safe(gasnet_AMReplyShort11(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11))
#define tic_AMReply12(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12)    \
  TIC_AM_Safe(gasnet_AMReplyShort12(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12))
#define tic_AMReply13(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13)    \
  TIC_AM_Safe(gasnet_AMReplyShort13(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13))
#define tic_AMReply14(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14)    \
  TIC_AM_Safe(gasnet_AMReplyShort14(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14))
#define tic_AMReply15(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15)    \
  TIC_AM_Safe(gasnet_AMReplyShort15(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15))
#define tic_AMReply16(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16)    \
  TIC_AM_Safe(gasnet_AMReplyShort16(token,handler,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16))


#define tic_AMReplyI0(token,handler,sourceaddr,nbytes)    \
  TIC_AM_Safe(gasnet_AMReplyMedium0(token,handler,sourceaddr,nbytes))
#define tic_AMReplyI1(token,handler,sourceaddr,nbytes,a1)    \
  TIC_AM_Safe(gasnet_AMReplyMedium1(token,handler,sourceaddr,nbytes,a1))
#define tic_AMReplyI2(token,handler,sourceaddr,nbytes,a1,a2)    \
  TIC_AM_Safe(gasnet_AMReplyMedium2(token,handler,sourceaddr,nbytes,a1,a2))
#define tic_AMReplyI3(token,handler,sourceaddr,nbytes,a1,a2,a3)    \
  TIC_AM_Safe(gasnet_AMReplyMedium3(token,handler,sourceaddr,nbytes,a1,a2,a3))
#define tic_AMReplyI4(token,handler,sourceaddr,nbytes,a1,a2,a3,a4)    \
  TIC_AM_Safe(gasnet_AMReplyMedium4(token,handler,sourceaddr,nbytes,a1,a2,a3,a4))
#define tic_AMReplyI5(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5)    \
  TIC_AM_Safe(gasnet_AMReplyMedium5(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5))
#define tic_AMReplyI6(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6)    \
  TIC_AM_Safe(gasnet_AMReplyMedium6(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6))
#define tic_AMReplyI7(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7)    \
  TIC_AM_Safe(gasnet_AMReplyMedium7(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7))
#define tic_AMReplyI8(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8)    \
  TIC_AM_Safe(gasnet_AMReplyMedium8(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8))
#define tic_AMReplyI9(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9)    \
  TIC_AM_Safe(gasnet_AMReplyMedium9(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9))
#define tic_AMReplyI10(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10)    \
  TIC_AM_Safe(gasnet_AMReplyMedium10(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10))
#define tic_AMReplyI11(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11)    \
  TIC_AM_Safe(gasnet_AMReplyMedium11(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11))
#define tic_AMReplyI12(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12)    \
  TIC_AM_Safe(gasnet_AMReplyMedium12(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12))
#define tic_AMReplyI13(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13)    \
  TIC_AM_Safe(gasnet_AMReplyMedium13(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13))
#define tic_AMReplyI14(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14)    \
  TIC_AM_Safe(gasnet_AMReplyMedium14(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14))
#define tic_AMReplyI15(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15)    \
  TIC_AM_Safe(gasnet_AMReplyMedium15(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15))
#define tic_AMReplyI16(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16)    \
  TIC_AM_Safe(gasnet_AMReplyMedium16(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16))


#define tic_AMReplyXfer0(token,destaddr,handler,sourceaddr,nbytes)    \
  TIC_AM_Safe(gasnet_AMReplyLong0(token,handler,sourceaddr,nbytes,(void*)(destaddr)))
#define tic_AMReplyXfer1(token,destaddr,handler,sourceaddr,nbytes,a1)    \
  TIC_AM_Safe(gasnet_AMReplyLong1(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1))
#define tic_AMReplyXfer2(token,destaddr,handler,sourceaddr,nbytes,a1,a2)    \
  TIC_AM_Safe(gasnet_AMReplyLong2(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2))
#define tic_AMReplyXfer3(token,destaddr,handler,sourceaddr,nbytes,a1,a2,a3)    \
  TIC_AM_Safe(gasnet_AMReplyLong3(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3))
#define tic_AMReplyXfer4(token,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4)    \
  TIC_AM_Safe(gasnet_AMReplyLong4(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4))
#define tic_AMReplyXfer5(token,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5)    \
  TIC_AM_Safe(gasnet_AMReplyLong5(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5))
#define tic_AMReplyXfer6(token,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6)    \
  TIC_AM_Safe(gasnet_AMReplyLong6(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6))
#define tic_AMReplyXfer7(token,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7)    \
  TIC_AM_Safe(gasnet_AMReplyLong7(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7))
#define tic_AMReplyXfer8(token,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8)    \
  TIC_AM_Safe(gasnet_AMReplyLong8(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8))
#define tic_AMReplyXfer9(token,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9)    \
  TIC_AM_Safe(gasnet_AMReplyLong9(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9))
#define tic_AMReplyXfer10(token,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10)    \
  TIC_AM_Safe(gasnet_AMReplyLong10(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9,a10))
#define tic_AMReplyXfer11(token,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11)    \
  TIC_AM_Safe(gasnet_AMReplyLong11(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11))
#define tic_AMReplyXfer12(token,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12)    \
  TIC_AM_Safe(gasnet_AMReplyLong12(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12))
#define tic_AMReplyXfer13(token,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13)    \
  TIC_AM_Safe(gasnet_AMReplyLong13(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13))
#define tic_AMReplyXfer14(token,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14)    \
  TIC_AM_Safe(gasnet_AMReplyLong14(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14))
#define tic_AMReplyXfer15(token,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15)    \
  TIC_AM_Safe(gasnet_AMReplyLong15(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15))
#define tic_AMReplyXfer16(token,destaddr,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16)    \
  TIC_AM_Safe(gasnet_AMReplyLong16(token,handler,sourceaddr,nbytes,(void*)(destaddr),a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16))

/* ------------------------------------------------------------------------------------ */
#else /* not GASNet */

/* AM2 requests */

#define tic_AMRequest0(dest,handler) \
  TIC_AM_Safe_Locked(AM_Request0(TIC_AM_ENDPOINT,dest,handler))
#define tic_AMRequest1(dest,handler,a1) \
  TIC_AM_Safe_Locked(AM_Request1(TIC_AM_ENDPOINT,dest,handler, (tic_handlerarg_t)(a1)))
#define tic_AMRequest2(dest,handler,a1,a2) \
  TIC_AM_Safe_Locked(AM_Request2(TIC_AM_ENDPOINT,dest,handler, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2)))
#define tic_AMRequest3(dest,handler,a1,a2,a3) \
  TIC_AM_Safe_Locked(AM_Request3(TIC_AM_ENDPOINT,dest,handler, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3)))
#define tic_AMRequest4(dest,handler,a1,a2,a3,a4) \
  TIC_AM_Safe_Locked(AM_Request4(TIC_AM_ENDPOINT,dest,handler, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4)))
#define tic_AMRequest5(dest,handler,a1,a2,a3,a4,a5) \
  TIC_AM_Safe_Locked(AM_Request5(TIC_AM_ENDPOINT,dest,handler, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5)))
#define tic_AMRequest6(dest,handler,a1,a2,a3,a4,a5,a6) \
  TIC_AM_Safe_Locked(AM_Request6(TIC_AM_ENDPOINT,dest,handler, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6)))
#define tic_AMRequest7(dest,handler,a1,a2,a3,a4,a5,a6,a7) \
  TIC_AM_Safe_Locked(AM_Request7(TIC_AM_ENDPOINT,dest,handler, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6), (tic_handlerarg_t)(a7)))
#define tic_AMRequest8(dest,handler,a1,a2,a3,a4,a5,a6,a7,a8) \
  TIC_AM_Safe_Locked(AM_Request8(TIC_AM_ENDPOINT,dest,handler, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6), (tic_handlerarg_t)(a7), (tic_handlerarg_t)(a8)))

#define tic_AMRequestI0(dest,handler,sourceaddr,nbytes) \
  TIC_AM_Safe_Locked(AM_RequestI0(TIC_AM_ENDPOINT,dest,handler,sourceaddr,nbytes))
#define tic_AMRequestI1(dest,handler,sourceaddr,nbytes,a1) \
  TIC_AM_Safe_Locked(AM_RequestI1(TIC_AM_ENDPOINT,dest,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1)))
#define tic_AMRequestI2(dest,handler,sourceaddr,nbytes,a1,a2) \
  TIC_AM_Safe_Locked(AM_RequestI2(TIC_AM_ENDPOINT,dest,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2)))
#define tic_AMRequestI3(dest,handler,sourceaddr,nbytes,a1,a2,a3) \
  TIC_AM_Safe_Locked(AM_RequestI3(TIC_AM_ENDPOINT,dest,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3)))
#define tic_AMRequestI4(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4) \
  TIC_AM_Safe_Locked(AM_RequestI4(TIC_AM_ENDPOINT,dest,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4)))
#define tic_AMRequestI5(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5) \
  TIC_AM_Safe_Locked(AM_RequestI5(TIC_AM_ENDPOINT,dest,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5)))
#define tic_AMRequestI6(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6) \
  TIC_AM_Safe_Locked(AM_RequestI6(TIC_AM_ENDPOINT,dest,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6)))
#define tic_AMRequestI7(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7) \
  TIC_AM_Safe_Locked(AM_RequestI7(TIC_AM_ENDPOINT,dest,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6), (tic_handlerarg_t)(a7)))
#define tic_AMRequestI8(dest,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8) \
  TIC_AM_Safe_Locked(AM_RequestI8(TIC_AM_ENDPOINT,dest,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6), (tic_handlerarg_t)(a7), (tic_handlerarg_t)(a8)))

#define tic_AMRequestXfer0(dest,destoffset,handler,sourceaddr,nbytes) \
  TIC_AM_Safe_Locked(AM_RequestXfer0(TIC_AM_ENDPOINT,dest,destoffset,handler,sourceaddr,nbytes))
#define tic_AMRequestXfer1(dest,destoffset,handler,sourceaddr,nbytes,a1) \
  TIC_AM_Safe_Locked(AM_RequestXfer1(TIC_AM_ENDPOINT,dest,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1)))
#define tic_AMRequestXfer2(dest,destoffset,handler,sourceaddr,nbytes,a1,a2) \
  TIC_AM_Safe_Locked(AM_RequestXfer2(TIC_AM_ENDPOINT,dest,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2)))
#define tic_AMRequestXfer3(dest,destoffset,handler,sourceaddr,nbytes,a1,a2,a3) \
  TIC_AM_Safe_Locked(AM_RequestXfer3(TIC_AM_ENDPOINT,dest,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3)))
#define tic_AMRequestXfer4(dest,destoffset,handler,sourceaddr,nbytes,a1,a2,a3,a4) \
  TIC_AM_Safe_Locked(AM_RequestXfer4(TIC_AM_ENDPOINT,dest,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4)))
#define tic_AMRequestXfer5(dest,destoffset,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5) \
  TIC_AM_Safe_Locked(AM_RequestXfer5(TIC_AM_ENDPOINT,dest,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5)))
#define tic_AMRequestXfer6(dest,destoffset,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6) \
  TIC_AM_Safe_Locked(AM_RequestXfer6(TIC_AM_ENDPOINT,dest,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6)))
#define tic_AMRequestXfer7(dest,destoffset,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7) \
  TIC_AM_Safe_Locked(AM_RequestXfer7(TIC_AM_ENDPOINT,dest,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6), (tic_handlerarg_t)(a7)))
#define tic_AMRequestXfer8(dest,destoffset,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8) \
  TIC_AM_Safe_Locked(AM_RequestXfer8(TIC_AM_ENDPOINT,dest,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6), (tic_handlerarg_t)(a7), (tic_handlerarg_t)(a8)))

/* AM2 replies */

#define tic_AMReply0(token,handler) \
  TIC_AM_Safe(AM_Reply0(token,handler))
#define tic_AMReply1(token,handler,a1) \
  TIC_AM_Safe(AM_Reply1(token,handler, (tic_handlerarg_t)(a1)))
#define tic_AMReply2(token,handler,a1,a2) \
  TIC_AM_Safe(AM_Reply2(token,handler, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2)))
#define tic_AMReply3(token,handler,a1,a2,a3) \
  TIC_AM_Safe(AM_Reply3(token,handler, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3)))
#define tic_AMReply4(token,handler,a1,a2,a3,a4) \
  TIC_AM_Safe(AM_Reply4(token,handler, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4)))
#define tic_AMReply5(token,handler,a1,a2,a3,a4,a5) \
  TIC_AM_Safe(AM_Reply5(token,handler, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5)))
#define tic_AMReply6(token,handler,a1,a2,a3,a4,a5,a6) \
  TIC_AM_Safe(AM_Reply6(token,handler, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6)))
#define tic_AMReply7(token,handler,a1,a2,a3,a4,a5,a6,a7) \
  TIC_AM_Safe(AM_Reply7(token,handler, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6), (tic_handlerarg_t)(a7)))
#define tic_AMReply8(token,handler,a1,a2,a3,a4,a5,a6,a7,a8) \
  TIC_AM_Safe(AM_Reply8(token,handler, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6), (tic_handlerarg_t)(a7), (tic_handlerarg_t)(a8)))

#define tic_AMReplyI0(token,handler,sourceaddr,nbytes) \
  TIC_AM_Safe(AM_ReplyI0(token,handler,sourceaddr,nbytes))
#define tic_AMReplyI1(token,handler,sourceaddr,nbytes,a1) \
  TIC_AM_Safe(AM_ReplyI1(token,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1)))
#define tic_AMReplyI2(token,handler,sourceaddr,nbytes,a1,a2) \
  TIC_AM_Safe(AM_ReplyI2(token,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2)))
#define tic_AMReplyI3(token,handler,sourceaddr,nbytes,a1,a2,a3) \
  TIC_AM_Safe(AM_ReplyI3(token,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3)))
#define tic_AMReplyI4(token,handler,sourceaddr,nbytes,a1,a2,a3,a4) \
  TIC_AM_Safe(AM_ReplyI4(token,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4)))
#define tic_AMReplyI5(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5) \
  TIC_AM_Safe(AM_ReplyI5(token,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5)))
#define tic_AMReplyI6(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6) \
  TIC_AM_Safe(AM_ReplyI6(token,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6)))
#define tic_AMReplyI7(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7) \
  TIC_AM_Safe(AM_ReplyI7(token,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6), (tic_handlerarg_t)(a7)))
#define tic_AMReplyI8(token,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8) \
  TIC_AM_Safe(AM_ReplyI8(token,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6), (tic_handlerarg_t)(a7), (tic_handlerarg_t)(a8)))

#define tic_AMReplyXfer0(token,destoffset,handler,sourceaddr,nbytes) \
  TIC_AM_Safe(AM_ReplyXfer0(token,destoffset,handler,sourceaddr,nbytes))
#define tic_AMReplyXfer1(token,destoffset,handler,sourceaddr,nbytes,a1) \
  TIC_AM_Safe(AM_ReplyXfer1(token,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1)))
#define tic_AMReplyXfer2(token,destoffset,handler,sourceaddr,nbytes,a1,a2) \
  TIC_AM_Safe(AM_ReplyXfer2(token,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2)))
#define tic_AMReplyXfer3(token,destoffset,handler,sourceaddr,nbytes,a1,a2,a3) \
  TIC_AM_Safe(AM_ReplyXfer3(token,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3)))
#define tic_AMReplyXfer4(token,destoffset,handler,sourceaddr,nbytes,a1,a2,a3,a4) \
  TIC_AM_Safe(AM_ReplyXfer4(token,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4)))
#define tic_AMReplyXfer5(token,destoffset,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5) \
  TIC_AM_Safe(AM_ReplyXfer5(token,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5)))
#define tic_AMReplyXfer6(token,destoffset,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6) \
  TIC_AM_Safe(AM_ReplyXfer6(token,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6)))
#define tic_AMReplyXfer7(token,destoffset,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7) \
  TIC_AM_Safe(AM_ReplyXfer7(token,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6), (tic_handlerarg_t)(a7)))
#define tic_AMReplyXfer8(token,destoffset,handler,sourceaddr,nbytes,a1,a2,a3,a4,a5,a6,a7,a8) \
  TIC_AM_Safe(AM_ReplyXfer8(token,destoffset,handler,sourceaddr,nbytes, (tic_handlerarg_t)(a1), (tic_handlerarg_t)(a2), (tic_handlerarg_t)(a3), (tic_handlerarg_t)(a4), (tic_handlerarg_t)(a5), (tic_handlerarg_t)(a6), (tic_handlerarg_t)(a7), (tic_handlerarg_t)(a8)))

#endif /* !COMM_GASNET */

#endif /* _TIC_AM2_MACROS_H */

