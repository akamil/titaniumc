/* handlers.h - Definitions for AM handlers */
/* see copyright.txt for usage terms */

#ifndef __HANDLERS_H__
#define __HANDLERS_H__

#include <tic.h>

#ifdef COMM_GASNET
  #define TIC_HANDLER_BASE 128
#else
  #define TIC_HANDLER_BASE 0
#endif

/* misc handler declarations */
#if defined(COMM_AMUDP) || defined(COMM_AMMPI)
  TIC_AMMEDIUM_DECLARE(amstats_request, 1, 1);
#endif

/* intentional tentative (common-block) definition 
   apps may override these from native code to register their own handlers */
tic_handler_fn_t tic_app_amhandler1, tic_app_amhandler2, tic_app_amhandler3, tic_app_amhandler4;

#define TIC_HANDLER TIC_AMIDX_ENUMENTRY
enum tic_AMhidx_list {
  TIC_HANDLER(__FIRST_HANDLER) = TIC_HANDLER_BASE-1,

  /* barrier.c */
  TIC_HANDLER(barrier_incr_request),

  /* mem.c */
  TIC_HANDLER(decr_ctr_reply),
  TIC_HANDLER(decr_put_ctr_reply),

  TIC_HANDLER(get_b_request),
  TIC_HANDLER(get_sh_request),
  TIC_HANDLER(get_i_request),
  TIC_HANDLER(get_f_request),
  TIC_HANDLER(get_d_request),
  TIC_HANDLER(get_l_request),
  TIC_HANDLER(get_lp_request),
  TIC_HANDLER(get_gp_request),

  TIC_HANDLER(get_b_reply),
  TIC_HANDLER(get_sh_reply),
  TIC_HANDLER(get_i_reply),
  TIC_HANDLER(get_f_reply),
  TIC_HANDLER(get_d_reply),
  TIC_HANDLER(get_l_reply),
  TIC_HANDLER(get_lp_reply),
  TIC_HANDLER(get_gp_reply),

  TIC_HANDLER(put_b_request),
  TIC_HANDLER(put_sh_request),
  TIC_HANDLER(put_i_request),
  TIC_HANDLER(put_f_request),
  TIC_HANDLER(put_d_request),
  TIC_HANDLER(put_l_request),
  TIC_HANDLER(put_lp_request),
  TIC_HANDLER(put_gp_request),
  
  /* store.c */
  TIC_HANDLER(store_b_request),
  TIC_HANDLER(store_sh_request),
  TIC_HANDLER(store_i_request),
  TIC_HANDLER(store_f_request),
  TIC_HANDLER(store_d_request),
  TIC_HANDLER(store_l_request),
  TIC_HANDLER(store_lp_request),
  TIC_HANDLER(store_gp_request),

  /* bulk.c */
  TIC_HANDLER(bulk_get_request),
  TIC_HANDLER(bulk_put_complete_reply),
  TIC_HANDLER(bulk_store_request),
  TIC_HANDLER(bulk_put_request),
  TIC_HANDLER(bulk_get_complete_reply),

  /* newbulk.c */
  TIC_HANDLER(misc_null_reply),
  TIC_HANDLER(misc_delete_request),
  TIC_HANDLER(misc_alloc_request),
  TIC_HANDLER(misc_alloc_reply),

  TIC_HANDLER(strided_pack_request),
  TIC_HANDLER(strided_pack_reply),
  TIC_HANDLER(strided_unpackAll_request),
  TIC_HANDLER(strided_unpack_reply),
  TIC_HANDLER(strided_unpackOnly_request),

  TIC_HANDLER(sparse_simpleScatter_request),
  TIC_HANDLER(sparse_done_reply),
  TIC_HANDLER(sparse_generalScatter_request),
  TIC_HANDLER(sparse_largeScatterNoDelete_request),
  TIC_HANDLER(sparse_simpleGather_request),
  TIC_HANDLER(sparse_simpleGather_reply),
  TIC_HANDLER(sparse_generalGather_request),
  TIC_HANDLER(sparse_largeGather_request),
  TIC_HANDLER(sparse_largeGather_reply),

  /* monitor-dist.c */
  TIC_HANDLER(monitor_enter_request),
  TIC_HANDLER(monitor_enter_reply),
  TIC_HANDLER(monitor_exit_request),
  TIC_HANDLER(monitor_exit_reply),
  TIC_HANDLER(monitor_wait_request),
  TIC_HANDLER(monitor_wait_reply),
  TIC_HANDLER(monitor_notify_request),
  TIC_HANDLER(monitor_notify_all_request),
  TIC_HANDLER(monitor_signal_request),
  TIC_HANDLER(monitor_cancel_wait_request),

  /* stats.c */
#if defined(COMM_AMUDP) || defined(COMM_AMMPI)
  TIC_HANDLER(amstats_request),
#endif

  /* comm_utils.c */
#ifdef MISALIGNED_CSTATIC_DATA
  TIC_HANDLER(staticdata_tablegather),
  TIC_HANDLER(staticdata_tablebcast),
#endif

  /* reserved for application use */
  TIC_HANDLER(apphandler_1),
  TIC_HANDLER(apphandler_2),
  TIC_HANDLER(apphandler_3),
  TIC_HANDLER(apphandler_4),

  TIC_HANDLER(__LAST_HANDLER)
};
#undef TIC_HANDLER

#ifdef COMM_AM2
  #ifdef COMM_GASNET
    typedef gasnet_handlerentry_t tic_handler_func_t;
  #else
    typedef struct tic_handler_function_T {
	    handler_t 	index;
	    void 	(*fnptr)();
    } tic_handler_func_t;
  #endif

  extern tic_handler_func_t tic_handler_list[];
  extern int tic_numhandlers;
#endif

#endif
	
