/* see copyright.txt for usage terms */

#ifndef __BARRIER_H__
#define __BARRIER_H__

TIC_AMSHORT_DECLARE(barrier_incr_request, 1, 2);

extern void barrier_init(void);
extern void barrier_up(void);
extern void barrier_down(void);

#ifdef COMM_GASNET
  extern void barrier_gasnet(void);
  #define barrier() do { ti_srcpos(); barrier_gasnet(); } while(0)
#elif defined(COMM_AM2)
  #define barrier() do { barrier_up(); barrier_down(); } while (0)
#else /* smp only needs one */
  #define barrier() barrier_up()
#endif

/* A barrier of just the threads on this box (used for initialization) */
#ifndef MEMORY_SHARED
  #define local_barrier()
#else
  extern void _local_barrier();
  #define local_barrier() _local_barrier()
#endif

#endif

