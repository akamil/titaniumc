#define TI_NO_SRCPOS
#include "backend-defines.h"

#if defined(MEMORY_DISTRIBUTED) && !defined(COMM_AM2) 
  #error monitors not implemented in this file for distributed layers other than AM2
#endif

#ifdef MEMORY_DISTRIBUTED
	#include "monitor-dist.c"
#else
	#include "monitor-shared.c"
#endif
