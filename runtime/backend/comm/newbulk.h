#ifndef _include_newbulk_h_
#define _include_newbulk_h_

#ifdef COMM_AM2
  
  /*
     initialize prealloc and pipelining by reading console environment variables
  */
  void gather_init();

  /* AM-based scatter-gather primitives (for internal use in ti_array.c only) */

  /* rectangular scatter/gather */
  void get_array(void *pack_method, void *copy_desc, int copy_desc_size, 
	         int tgt_box, void *buffer, int atomicelements);
  void put_array(void *unpack_method, void *copy_desc, int copy_desc_size,
	         void *array_data, int array_data_size, int tgt_box);

  /* sparse scatter/gather - transfer data back & forth between a 
   * local, packed data list and the memory space of a remote proc
   * using a list of addresses on the remote proc
   * requires: sizeof(data_list) == num_elem * elem_sz
   *           sizeof(remote_addr_list) == num_elem * sizeof(void*)
   */
  void sparse_scatter(void **remote_addr_list, void *src_data_list, 
                      int remote_box, int num_elem, int elem_sz, int atomic_elements);
  void sparse_gather(void *tgt_data_list, void **remote_addr_list, 
                      int remote_box, int num_elem, int elem_sz, int atomic_elements);


  /* handler declarations */

TIC_AMSHORT_DECLARE(misc_null_reply, 0, 0);
TIC_AMSHORT_DECLARE(misc_delete_request, 1, 2);
TIC_AMSHORT_DECLARE(misc_alloc_request, 2, 3);
TIC_AMSHORT_DECLARE(misc_alloc_reply, 2, 4);

TIC_AMMEDIUM_DECLARE(strided_pack_request, 4, 7);
TIC_AMMEDIUM_DECLARE(strided_pack_reply, 4, 7);
TIC_AMMEDIUM_DECLARE(strided_unpackAll_request, 3, 5);
TIC_AMSHORT_DECLARE(strided_unpack_reply, 1, 2);
TIC_AMMEDIUM_DECLARE(strided_unpackOnly_request, 3, 6);

TIC_AMMEDIUM_DECLARE(sparse_simpleScatter_request, 3, 4);
TIC_AMSHORT_DECLARE(sparse_done_reply, 1, 2);
TIC_AMSHORT_DECLARE(sparse_generalScatter_request, 4, 6);
TIC_AMMEDIUM_DECLARE(sparse_simpleGather_request, 5, 7);
TIC_AMMEDIUM_DECLARE(sparse_simpleGather_reply, 4, 6);
TIC_AMSHORT_DECLARE(sparse_generalGather_request, 4, 6);
TIC_AMLONG_DECLARE(sparse_largeGather_request, 5, 7);
TIC_AMLONG_DECLARE(sparse_largeScatterNoDelete_request, 3, 4);
TIC_AMLONG_DECLARE(sparse_largeGather_reply, 1, 2);

#endif

#endif /* !_include_newbulk_h_ */




