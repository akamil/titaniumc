#ifndef _include_assign_h_
#define _include_assign_h_

#include <gp-trace/gp-trace.h>
#include <tic.h>

#ifdef WIDE_POINTERS
#ifdef COMM_GASNET
  #define ASSIGN_GLOBAL_T(ptr,val,T) do {                               \
    ti_srcpos();                                                        \
    gasnet_put_val(GET_BOX(ptr), GET_ADDR(ptr), (val), sizeof(T));      \
  } while (0)

  #define WEAK_ASSIGN_GLOBAL_T(ptr,val,T) do {                          \
     ti_srcpos();                                                       \
     gasnet_put_nbi_val(GET_BOX(ptr), GET_ADDR(ptr), (val), sizeof(T)); \
  } while (0)

  #define ASSIGN_GLOBAL_TP(ptr,val,T) do {                              \
    T const _temp = (val);                                              \
    ti_srcpos();                                                        \
    gasnet_put(GET_BOX(ptr), GET_ADDR(ptr), (void *)&_temp, sizeof(T)); \
  } while (0)

  #define WEAK_ASSIGN_GLOBAL_TP(ptr,val,T) do {                             \
    T const _temp = (val);                                                  \
    ti_srcpos();                                                            \
    gasnet_put_nbi(GET_BOX(ptr), GET_ADDR(ptr), (void *)&_temp, sizeof(T)); \
  } while (0)

  #define ASSIGN_GLOBAL_jboolean(ptr, val)      ASSIGN_GLOBAL_T(ptr,val,jbyte)
  #define ASSIGN_GLOBAL_jbyte(ptr, val)         ASSIGN_GLOBAL_T(ptr,val,jbyte)
  #define ASSIGN_GLOBAL_jchar(ptr, val)         ASSIGN_GLOBAL_T(ptr,val,jchar)
  #define ASSIGN_GLOBAL_jshort(ptr, val)        ASSIGN_GLOBAL_T(ptr,val,jshort)
  #define ASSIGN_GLOBAL_jint(ptr, val)          ASSIGN_GLOBAL_T(ptr,val,jint)
  #if SIZEOF_GASNET_REGISTER_VALUE_T >= 8
  #define ASSIGN_GLOBAL_jlong(ptr, val)         ASSIGN_GLOBAL_T(ptr,val,jlong)
  #else
  #define ASSIGN_GLOBAL_jlong(ptr, val)         ASSIGN_GLOBAL_TP(ptr,val,jlong)
  #endif
  #define ASSIGN_GLOBAL_jfloat(ptr, val)        ASSIGN_GLOBAL_TP(ptr,val,jfloat)
  #define ASSIGN_GLOBAL_jdouble(ptr, val)       ASSIGN_GLOBAL_TP(ptr,val,jdouble)
  #define ASSIGN_GLOBAL_lp(ptr, val)            ASSIGN_GLOBAL_T(ptr,(uintptr_t)val,void *)
  #define ASSIGN_GLOBAL_gp(ptr, val)            ASSIGN_GLOBAL_TP(ptr,CAST_GPTR(val),jGPointer)

  #define WEAK_ASSIGN_GLOBAL_jboolean(ptr, val) WEAK_ASSIGN_GLOBAL_T(ptr,val,jbyte)
  #define WEAK_ASSIGN_GLOBAL_jbyte(ptr, val)    WEAK_ASSIGN_GLOBAL_T(ptr,val,jbyte)
  #define WEAK_ASSIGN_GLOBAL_jchar(ptr, val)    WEAK_ASSIGN_GLOBAL_T(ptr,val,jchar)
  #define WEAK_ASSIGN_GLOBAL_jshort(ptr, val)   WEAK_ASSIGN_GLOBAL_T(ptr,val,jshort)
  #define WEAK_ASSIGN_GLOBAL_jint(ptr, val)     WEAK_ASSIGN_GLOBAL_T(ptr,val,jint)
  #if SIZEOF_GASNET_REGISTER_VALUE_T >= 8
  #define WEAK_ASSIGN_GLOBAL_jlong(ptr, val)    WEAK_ASSIGN_GLOBAL_T(ptr,val,jlong)
  #else
  #define WEAK_ASSIGN_GLOBAL_jlong(ptr, val)    WEAK_ASSIGN_GLOBAL_TP(ptr,val,jlong)
  #endif
  #define WEAK_ASSIGN_GLOBAL_jfloat(ptr, val)   WEAK_ASSIGN_GLOBAL_TP(ptr,val,jfloat)
  #define WEAK_ASSIGN_GLOBAL_jdouble(ptr, val)  WEAK_ASSIGN_GLOBAL_TP(ptr,val,jdouble)
  #define WEAK_ASSIGN_GLOBAL_lp(ptr, val)       WEAK_ASSIGN_GLOBAL_T(ptr,val,void *)
  #define WEAK_ASSIGN_GLOBAL_gp(ptr, val)       WEAK_ASSIGN_GLOBAL_TP(ptr,CAST_GPTR(val),jGPointer)
#else
  #define ASSIGN_GLOBAL_jboolean(ptr, val)               __b_write(CAST_GPTR(ptr), (jbyte)val)
  #define ASSIGN_GLOBAL_jbyte(ptr, val)                  __b_write(CAST_GPTR(ptr), (jbyte)val)
  #define ASSIGN_GLOBAL_jchar(ptr, val)                  __sh_write(CAST_GPTR(ptr), val)
  #define ASSIGN_GLOBAL_jshort(ptr, val)                 __sh_write(CAST_GPTR(ptr), val)
  #define ASSIGN_GLOBAL_jint(ptr, val)                   __i_write(CAST_GPTR(ptr), val)
  #define ASSIGN_GLOBAL_jlong(ptr, val)                  __l_write(CAST_GPTR(ptr), val)
  #define ASSIGN_GLOBAL_jfloat(ptr, val)                 __f_write(CAST_GPTR(ptr), val)
  #define ASSIGN_GLOBAL_jdouble(ptr, val)                __d_write(CAST_GPTR(ptr), val)
  #define ASSIGN_GLOBAL_lp(ptr, val)                     __lp_write(CAST_GPTR(ptr), (void *)(val))
  #define ASSIGN_GLOBAL_gp(ptr, val)                     __gp_write(CAST_GPTR(ptr), CAST_GPTR(val))

  #define WEAK_ASSIGN_GLOBAL_jboolean(ptr, val)          __b_put(CAST_GPTR(ptr), (jbyte)val)
  #define WEAK_ASSIGN_GLOBAL_jbyte(ptr, val)             __b_put(CAST_GPTR(ptr), (jbyte)val)
  #define WEAK_ASSIGN_GLOBAL_jchar(ptr, val)             __sh_put(CAST_GPTR(ptr), val)
  #define WEAK_ASSIGN_GLOBAL_jshort(ptr, val)            __sh_put(CAST_GPTR(ptr), val)
  #define WEAK_ASSIGN_GLOBAL_jint(ptr, val)              __i_put(CAST_GPTR(ptr), val)
  #define WEAK_ASSIGN_GLOBAL_jlong(ptr, val)             __l_put(CAST_GPTR(ptr), val)
  #define WEAK_ASSIGN_GLOBAL_jfloat(ptr, val)            __f_put(CAST_GPTR(ptr), val)
  #define WEAK_ASSIGN_GLOBAL_jdouble(ptr, val)           __d_put(CAST_GPTR(ptr), val)
  #define WEAK_ASSIGN_GLOBAL_lp(ptr, val)                __lp_put(CAST_GPTR(ptr), (void *)(val))
  #define WEAK_ASSIGN_GLOBAL_gp(ptr, val)                __gp_put(CAST_GPTR(ptr), CAST_GPTR(val))
#endif

#define ASSIGN_GLOBAL_bulk(gptr, val) \
    ti_bulk_write(GET_BOX(gptr), GET_ADDR(gptr), (void *)&(val), sizeof(val))

#define ASSIGN_GLOBAL_anonymous_bulk(gptr, nitems, pval) \
    ti_bulk_write(GET_BOX(gptr), GET_ADDR(gptr), (void *)(pval), sizeof(*(pval)) * (nitems))

#define WEAK_ASSIGN_GLOBAL_bulk(gptr, val) \
    ti_bulk_write_weak(GET_BOX(gptr), GET_ADDR(gptr), (void *)&(val), sizeof(val))

#define WEAK_ASSIGN_GLOBAL_anonymous_bulk(gptr, nitems, pval) \
    ti_bulk_write_weak(GET_BOX(gptr), GET_ADDR(gptr), (void *)(pval), sizeof(*(pval)) * (nitems))

#endif /* WIDE_POINTERS */


#endif /* !_include_assign_h_ */
