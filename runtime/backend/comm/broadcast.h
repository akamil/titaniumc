#ifndef _include_tic_broadcast_h_
#define _include_tic_broadcast_h_

#include "backend.h"
#include "bcast-common.h"

/* Broadcast Send sends the value to processor 0 which
   then broadcasts it to everyone */

#define broadcast_jboolean all_bcast_bool
#define broadcast_jbyte    all_bcast_b
#define broadcast_jchar    all_bcast_c
#define broadcast_jdouble  all_bcast_d
#define broadcast_jfloat   all_bcast_f
#define broadcast_jint     all_bcast_i
#define broadcast_jlong    all_bcast_l
#define broadcast_jshort   all_bcast_sh
#define broadcast_lp(value)       all_bcast_lp((void*)(value))
#define broadcast_gp(value)       all_bcast_gp(CAST_GPTR(value))
#define broadcast_bulk(value, length)  all_bcast_buffer((void *)value, length)

#if defined(MEMORY_DISTRIBUTED) || defined(MEMORY_SHARED) 

#define _BROADCAST( result, type, sender, value, primtype )              \
  do {                                                                   \
    int myproc = MYPROC;                                                 \
    if (myproc == sender) {                                              \
      if (myproc != 0) {                                                 \
         jGPointer senderBuffer;                                         \
         TO_GLOBALB_STATIC(senderBuffer, 0, (void *)&BUFFER);            \
         FENCE_PRE_WRITE();                                              \
         ASSIGN_GLOBAL_ ## primtype (senderBuffer, value);               \
         FENCE_POST_WRITE();                                             \
      } else BUFFER = value;                                             \
      /* printf("%d sending %lld in broadcast_" #primtype "\n" , */      \
      /*        MYPROC, (jlong)BUFFER); */                               \
    }                                                                    \
    if (sender != 0) barrier();                                          \
    result = broadcast_ ## primtype (BUFFER);                            \
    /* printf("%d received %lld in broadcast_" #primtype " from %d\n",*/ \
    /*   MYPROC, (jlong)result, sender); */                              \
    CYCLE = !CYCLE;                                                      \
  } while (0)               

#define BROADCAST_lp( result, type, sender, value )          \
  do {                                                       \
    int myproc = MYPROC;                                     \
    if (myproc == sender) {                                  \
      if (myproc != 0) {                                     \
        jGPointer senderBuffer;                              \
        TO_GLOBALB_STATIC(senderBuffer, 0, (void *)&BUFFER); \
        FENCE_PRE_WRITE();                                   \
        ASSIGN_GLOBAL_lp (senderBuffer, value);              \
        FENCE_POST_WRITE();                                  \
      } else BUFFER = value;                                 \
    }                                                        \
    if (sender != 0) barrier();                              \
    *(void**)(&(result)) = broadcast_lp (BUFFER);            \
    CYCLE = !CYCLE;                                          \
  } while (0)               

#define BROADCAST_gp( result, type, sender, value )                               \
  do {                                                                            \
    int myproc = MYPROC;                                                          \
    if (myproc == sender) {                                                       \
       if (myproc != 0) {                                                         \
          jGPointer senderBuffer;                                                 \
          TO_GLOBALB_STATIC(senderBuffer, 0, (void *)&BUFFER);                    \
          FENCE_PRE_WRITE();                                                      \
          ASSIGN_GLOBAL_gp (senderBuffer, value);                                 \
          FENCE_POST_WRITE();                                                     \
       } else BUFFER = value;                                                     \
    /* printf("%d sending <0x%p, box:%d, proc:%d> in broadcast_gp\n", */          \
    /*        sender, TO_LOCAL(BUFFER), GET_BOX(BUFFER), GET_PROC(BUFFER)); */    \
    }                                                                             \
    if (sender != 0) barrier();                                                   \
    *(jGPointer *)&(result) = broadcast_gp (BUFFER);                              \
    /* printf("%d received <0x%p, box:%d, proc:%d> in broadcast_gp from %d\n", */ \
    /*   MYPROC, TO_LOCAL(result), GET_BOX(result), GET_PROC(result), sender); */ \
    CYCLE = !CYCLE;                                                               \
  } while (0)               


#define BROADCAST_bulk( result, type, sender, value )             \
  do {                                                            \
    int myproc = MYPROC;                                          \
    if (myproc == sender) {                                       \
      if (myproc != 0) {                                          \
        jGPointer senderBuffer;                                   \
        TO_GLOBALB_STATIC(senderBuffer, 0, (void *)&BUFFER);      \
        FENCE_PRE_WRITE();                                        \
        ASSIGN_GLOBAL_bulk (senderBuffer, value);                 \
        FENCE_POST_WRITE();                                       \
      } else BUFFER = value;                                      \
    }                                                             \
    if (sender != 0) barrier();                                   \
    result = *((type *)broadcast_bulk (&BUFFER, sizeof(BUFFER))); \
    CYCLE = !CYCLE;                                               \
  } while (0)               


#else /* !shared !distributed */
#define _BROADCAST( result, type, sender, value, primtype )  \
  do { result = value; } while(0)

#define BROADCAST_lp( result, type, sender, value )  \
  do { result = value; } while(0)

#define BROADCAST_gp( result, type, sender, value )  \
  do { result = value; } while(0)

#define BROADCAST_bulk( result, type, sender, value )  \
  do { memcpy(&(result), &(value), sizeof(value)); } while(0)
#endif /* MEM_DIST or MEM_SHARED */

#define BROADCAST_jboolean(result, type, sender, value) \
   _BROADCAST ( result, type, sender,  value, jboolean)
#define BROADCAST_jbyte(result, type, sender,  value) \
   _BROADCAST ( result, type, sender,  value, jbyte)
#define BROADCAST_jchar(result, type, sender,  value) \
   _BROADCAST ( result, type, sender,  value, jchar)
#define BROADCAST_jdouble(result, type, sender,  value) \
   _BROADCAST ( result, type, sender,  value, jdouble)
#define BROADCAST_jfloat(result, type, sender,  value) \
   _BROADCAST ( result, type, sender,  value, jfloat)
#define BROADCAST_jint(result, type, sender,  value) \
   _BROADCAST ( result, type, sender,  value, jint)
#define BROADCAST_jlong(result, type, sender,  value) \
   _BROADCAST ( result, type, sender,  value, jlong)
#define BROADCAST_jshort(result, type, sender,  value) \
   _BROADCAST ( result, type, sender,  value, jshort)


#define BROADCAST_END( result, type, sender) 


#endif /* !_include_tic_broadcast_h_ */
