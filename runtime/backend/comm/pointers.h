/* pointers.h - Make global pointers happen. Compiler specific stuff. */
/* see copyright.txt for usage terms */

#ifndef __POINTERS_H
#define __POINTERS_H

/* Mhh, do we really want to do this? -TvE */
#ifndef NULL
#  define NULL (0)
#endif

/* 
 * Global pointer representation now lives in primitives.h
 */

#include <primitives.h>


/* .............................
 *
 * Constructing global addresses (with implicit temp)
 *
 * .............................
 */

/* Creates a global pointer which is proc:local */
TI_INLINE(toglobalp) 
jGPointer toglobalp(Process proc, void *local) {
	jGPointer tempPtr;

        SET_PROC(tempPtr, proc);
        SET_ADDR(tempPtr, local);
	return tempPtr;
}

/* Creates a global pointer which is box:local */
TI_INLINE(toglobalb) 
jGPointer toglobalb(Box box, void *local) {
	jGPointer tempPtr;

        SET_BOX(tempPtr, box);
        SET_ADDR(tempPtr, local);
	return tempPtr;
}

/* performs translation of a local static variable address 
   to the address for the corresponding remote copy */
#define toglobalb_static(box, local_static) \
  toglobalb((box), TIC_TRANSLATE_CSTATIC_ADDR((local_static), (box)))

/* Mutates a global pointer which is proc:local */
#define mutateglobal(globPtr, proc, local) \
  do {\
    SET_ADDR(globPtr, local);\
    SET_PROC(globPtr, proc);\
    SET_BOX(globPtr, COMM_GetBoxNumberForProcNumber(proc)); \
} while(0)

/* Mutates a global pointer which is proc:local */
#define mutateglobalwithbox(globPtr, proc, box, local) \
  do {   \
	SET_ADDR(globPtr, local); \
	SET_PROC(globPtr, proc);  \
	SET_BOX(globPtr, box);    \
  } while (0)                       

#define toproc(globPtr) 	GET_PROC(globPtr)
#define tobox(globPtr)		GET_BOX(globPtr)
#define tolocal(globPtr)        GET_ADDR(globPtr) /* Allow tolocal to be an lvalue */

#define toboxproc(globPtr) (*(jUIntPointer *)(&globPtr))
#define boxproctoproc(boxproc) (GET_PROC(*(jGPointer *)(&boxproc)))
#define boxproctobox(boxproc)  (GET_BOX(*(jGPointer *)(&boxproc)))

/* ..........................
 *
 * Global pointer comparisons
 *
 * ..........................
 */

TI_INLINE(___null_ptr_check) 
int ___null_ptr_check(jGPointer gPtr) {
	return GET_ADDR(gPtr) == 0;
}

TI_INLINE(___not_null_ptr_check) 
int ___not_null_ptr_check(jGPointer gPtr) {
	return GET_ADDR(gPtr) != 0;
}

TI_INLINE(___eq_ptr_check) 
int ___eq_ptr_check(jGPointer gPtr1, jGPointer gPtr2) {
	return ((GET_ADDR(gPtr1) == GET_ADDR(gPtr2)) &&
	        (GET_PROC(gPtr1) == GET_PROC(gPtr2)))
             || (___null_ptr_check(gPtr1) && ___null_ptr_check(gPtr2));
}

TI_INLINE(___neq_ptr_check) 
int ___neq_ptr_check(jGPointer gPtr1, jGPointer gPtr2) {
	return GET_ADDR(gPtr1) != GET_ADDR(gPtr2)
	    || GET_PROC(gPtr1) != GET_PROC(gPtr2);

}

#endif /* __POINTERS_H */
