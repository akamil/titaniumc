/* control.h - Control macros. */
/* see copyright.txt for usage terms */

#ifndef __CONTROL_H__
#define __CONTROL_H__

/* Determine number of local elements */
#define my_elements(n) ((int) (((n) + PROCS - 1 - MYPROC)/PROCS))

/* Determine maximum number of elements owned by a processor */
#define max_elements(n) ((int) (((n) + PROCS - 1)/PROCS))

/* Execute a section of code on one processor (0) or on a specified processor */
#define on_one     if (MYPROC==0)
#define on_proc(x) if (MYPROC==(x))

/*
 * The following macros iterate over a global matrix with the number
 * of dimensions specified by x in for_my_xd and for_my_xD.  
 * The indices i, j, k are set to the local elements on this processor.
 *
 * for_my_xD uses an auxiliary variable l which is the linear index
 * into the matrix.  These macros are more efficient than the
 * corresponding for_my_xd (except for for_my_1d).
 */

/* -------------------------- 1 Dimension ------------------------- */

#define for_my_1d(i,N) for (i=MYPROC;(i)<(N);i+=PROCS)

/* Provided only for completeness */
#define for_my_1D(i,l,N) for (l=i=MYPROC;(i)<(N);i+=PROCS, l=i)


/* -------------------------- 2 Dimensions ------------------------- */

/* Iterate over success local elements binding i,j to the 
   associated point in the [0,R)X[0,C) space.
*/
#if 0
#define	for_my_2d(i,j,R,C) for (i=MYPROC/(C), j=MYPROC%(C);\
				((i)<(R)); \
				i=(i*C+j+PROCS)/C,\
				j=(i*C+j+PROCS)%C)
#endif

/* Optimized. . . */
#define	for_my_2d(i,j,R,C) for (i=MYPROC/(C), j=MYPROC%(C);\
				((i)<(R)); \
				i=(i*C+j+PROCS)/C, j=(j+PROCS)%C)

#define	for_my_2D(i,j,l,R,C) for (l=MYPROC,i=(l)/(C), j=(l)%(C);\
				((i)<(R)); \
				l=l+PROCS,i=(l)/C,j=(l)%C)

#define my_elements_2D(n,m) my_elements((n)*(m))

/* -------------------------- 3 Dimensions ------------------------- */

#define	for_my_3D(i,j,k,l,D,R,C) \
  for (l=MYPROC,i=(l)/((C)*(R)), j=((l)%((C)*(R)))/(C), k=(l)%(C);\
       ((i)<(D)); \
       l=l+PROCS,i=(l)/((C)*(R)), j=((l)%((C)*(R)))/(C), k=(l)%(C))

#if 0
#define	for_my_3d(i,j,k,D,R,C) \
  for (i=MYPROC/((C)*(R)), j=(MYPROC%((C)*(R)))/(C), k=MYPROC%(C);\
       ((i)<(D)); \
       i=(i*(R)*(C)+j*(C)+k+PROCS)/((C)*(R)), j=((i*(R)*(C)+j*(C)+k+PROCS)%((C)*(R)))/(C), \
       k=(i*(R)*(C)+j*(C)+k+PROCS)%(C))
     
#endif  

/* Optimized. . . */
#define	for_my_3d(i,j,k,D,R,C) \
  for (i=MYPROC/((C)*(R)), j=(MYPROC%((C)*(R)))/(C), k=MYPROC%(C);\
       ((i)<(D)); \
       i=(i*(R)*(C)+j*(C)+k+PROCS)/((C)*(R)), j=((j*(C)+k+PROCS)%((C)*(R)))/(C), \
       k=(k+PROCS)%(C))
     

/* -------------------------- 4 Dimensions ------------------------- */

#define	for_my_4D(h,i,j,k,l,B,D,R,C) \
  for (l=MYPROC,h=(l)/((D)*(R)*(C)),i=((l)%((C)*(R)*(D)))/((C)*(R)), \
	        j=((l)%((C)*(R)))/(C), k=(l)%(C);\
       ((h)<(B)); \
       l=l+PROCS,h=(l)/((D)*(R)*(C)),i=((l)%((C)*(R)*(D)))/((C)*(R)), \
	        j=((l)%((C)*(R)))/(C), k=(l)%(C))


#define	for_my_4d(h,i,j,k,B,D,R,C) \
  for (h=MYPROC/((D)*(R)*(C)),i=(MYPROC%((C)*(R)*(D)))/((C)*(R)), \
	        j=(MYPROC%((C)*(R)))/(C), k=MYPROC%(C);\
       ((h)<(B)); \
       h=(h*(D)*(R)*(C)+i*(R)*(C)+j*(C)+k+PROCS)/((D)*(R)*(C)), \
          i=((i*(R)*(C)+j*(C)+k+PROCS)%((C)*(R)*(D)))/((C)*(R)), \
          j=((j*(C)+k+PROCS)%((C)*(R)))/(C), k=(k+PROCS)%(C))

#endif /* __CONTROL_H__ */









