#ifndef __MONITOR_H_
#define __MONITOR_H_

#ifdef HAVE_MONITORS

#ifdef COMM_AM2
  #include "monitor-dist.h"
#endif /* COMM_AM2 */

#ifndef DEBUG_MONITORS
#ifdef NDEBUG
#define DEBUG_MONITORS 0
#else
#define DEBUG_MONITORS 1
#endif
#endif

/* note: with the current definition of static, class monitors are useless
   (i.e. the synchronized keyword means nothing on a static method) */
# define MONITOR_FETCH_CLASS( staticstruct, holder )  globalize( (holder), &STATIC_REF(staticstruct, monitor))
/* holder is always global, but object might be local */
#define MONITOR_FETCH_INSTANCE_LOCAL(  holder, object )  globalize( (holder), &((object)->monitor) )
#define MONITOR_FETCH_INSTANCE_GLOBAL( holder, object )  FIELD_ADDR_GLOBAL( holder, object, monitor )
#define MONITOR_LOCK(   holder )  do {           \
   ti_trace_printf(("LOCK synchronized-enter")); \
   monitor_enter( CAST_GPTR(holder) );           \
  } while(0)
#define MONITOR_UNLOCK( holder )  do {           \
   ti_trace_printf(("LOCK synchronized-exit"));  \
   monitor_exit(  CAST_GPTR(holder) );           \
  } while(0)
#else /* no monitors */
# define MONITOR_FETCH_CLASS( staticstruct, holder )
# define MONITOR_FETCH_INSTANCE_LOCAL(  holder, object )
# define MONITOR_FETCH_INSTANCE_GLOBAL( holder, object )
# define MONITOR_LOCK(   holder )  
# define MONITOR_UNLOCK( holder ) 
#endif /* no monitors */


#ifdef CAUTIOUS_MONITORS
  /* init monitor state that implements cautious waits */
  #define CAUTIOUS_MONITOR_INIT(mon) do { \
    mon->numwaiting = 0;                  \
    mon->numsignaled = 0;                 \
    } while(0)
#else
  #define CAUTIOUS_MONITOR_INIT(mon)
#endif

#ifdef COMM_GASNET
  #define GASNET_MONITOR_INIT(mon) \
    gasnet_hsl_init(&(mon->hsl))
#else
  #define GASNET_MONITOR_INIT(mon) 
#endif

#ifdef COMM_AM2
TI_INLINE(monitor_init_inner) 
void monitor_init_inner(tic_monitor_t mon) {
  mon->proc = (Process)-1;
  mon->box = (Box)-1;

  GASNET_MONITOR_INIT(mon);

  mon->waitingProcs = NULL; /* no processes waiting to enter the monitor */
  mon->tryingProcs  = NULL; /* no processes trying to enter the monitor */
}
#else 
TI_INLINE(monitor_init_inner) 
void monitor_init_inner(tic_monitor_t mon) {
  /* This lock controls access to the monitor structure */
  mon->lock = ti_lock_initializer;
  mon->condvar = ti_cond_initializer;

  mon->proc = (Process)-1;

  mon->nesting = 0;
 
  CAUTIOUS_MONITOR_INIT(mon); /* init special fields if we're running CAUTIOUS_MONITORS */

}
#endif
#define monitor_init(pmon) (*(pmon) = NULL)

TI_INLINE(simplemonitor_init) 
void simplemonitor_init(tic_simplemonitor_t *mon) {
  mon->lock = ti_lock_initializer;
  mon->condvar = ti_cond_initializer;
  CAUTIOUS_MONITOR_INIT(mon); /* init special fields if we're running CAUTIOUS_MONITORS */
}

void monitor_startup();
void monitor_destroy(tic_monitor_t *monitor);
void monitor_enter(jGPointer monitor);
void monitor_exit(jGPointer monitor);
void monitor_wait(jGPointer monitor, ti_time_t *tm);
void monitor_notify(jGPointer monitor);
void monitor_notify_all(jGPointer monitor);

#endif /* __MONITOR_H_ */
