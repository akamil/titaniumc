/* com.h - Header file for entry points in com.c */
/* see copyright.txt for usage terms */

#ifndef TIC_COM_H
#define TIC_COM_H


/*
  This library provides global communication support for Tic, including
  broadcasts, reductions, and scans.  

  It provides operations in which all processors participate.
  There are two basic styles: a processor-oriented functional interface
  and a spread array interface.

  Currently only the processor interface is supported.

  Eventually it will also provide autonomous operations.

  An all_ routine end with a barrier, so that there is no conflict on
  use of internal state. 

*/



/* reduce to Proc extern */
extern jint all_reduce_to_one_iadd(jint val);
extern jbyte all_reduce_to_one_badd(jbyte val);
extern jchar all_reduce_to_one_cadd(jchar val);
extern jshort all_reduce_to_one_shadd(jshort val);
extern jlong all_reduce_to_one_ladd(jlong val);
extern jfloat all_reduce_to_one_fadd(jfloat val);
extern jdouble all_reduce_to_one_dadd(jdouble val);

extern jint all_reduce_to_one_imult(jint val);
extern jbyte all_reduce_to_one_bmult(jbyte val);
extern jchar all_reduce_to_one_cmult(jchar val);
extern jshort all_reduce_to_one_shmult(jshort val);
extern jlong all_reduce_to_one_lmult(jlong val);
extern jfloat all_reduce_to_one_fmult(jfloat val);
extern jdouble all_reduce_to_one_dmult(jdouble val);

extern jint all_reduce_to_one_imax(jint val);
extern jbyte all_reduce_to_one_bmax(jbyte val);
extern jchar all_reduce_to_one_cmax(jchar val);
extern jshort all_reduce_to_one_shmax(jshort val);
extern jlong all_reduce_to_one_lmax(jlong val);
extern jfloat all_reduce_to_one_fmax(jfloat val);
extern jdouble all_reduce_to_one_dmax(jdouble val);

extern jint all_reduce_to_one_imin(jint val);
extern jbyte all_reduce_to_one_bmin(jbyte val);
extern jchar all_reduce_to_one_cmin(jchar val);
extern jshort all_reduce_to_one_shmin(jshort val);
extern jlong all_reduce_to_one_lmin(jlong val);
extern jfloat all_reduce_to_one_fmin(jfloat val);
extern jdouble all_reduce_to_one_dmin(jdouble val);

extern jboolean all_reduce_to_one_or(jboolean val);
extern jboolean all_reduce_to_one_xor(jboolean val);
extern jboolean all_reduce_to_one_and(jboolean val);

/* broadcast to all nodes */

extern jint all_bcast_i(jint val);
extern jboolean all_bcast_bool(jboolean val);
extern jbyte all_bcast_b(jbyte val);
extern jchar all_bcast_c(jchar val);
extern jshort all_bcast_sh(jshort val);
extern jlong all_bcast_l(jlong val);
extern jfloat all_bcast_f(jfloat val);
extern jdouble all_bcast_d(jdouble val);
extern void * all_bcast_lp(void * val);
extern jGPointer all_bcast_gp(jGPointer val);

extern void * all_bcast_buffer(void * val, juint length);

/* reduction to all nodes */
extern jint all_reduce_to_all_iadd(jint val);
extern jbyte all_reduce_to_all_badd(jbyte val);
extern jchar all_reduce_to_all_cadd(jchar val);
extern jshort all_reduce_to_all_shadd(jshort val);
extern jlong all_reduce_to_all_ladd(jlong val);
extern jfloat all_reduce_to_all_fadd(jfloat val);
extern jdouble all_reduce_to_all_dadd(jdouble val);

extern jint all_reduce_to_all_imult(jint val);
extern jbyte all_reduce_to_all_bmult(jbyte val);
extern jchar all_reduce_to_all_cmult(jchar val);
extern jshort all_reduce_to_all_shmult(jshort val);
extern jlong all_reduce_to_all_lmult(jlong val);
extern jfloat all_reduce_to_all_fmult(jfloat val);
extern jdouble all_reduce_to_all_dmult(jdouble val);

extern jint all_reduce_to_all_imax(jint val);
extern jbyte all_reduce_to_all_bmax(jbyte val);
extern jchar all_reduce_to_all_cmax(jchar val);
extern jshort all_reduce_to_all_shmax(jshort val);
extern jlong all_reduce_to_all_lmax(jlong val);
extern jfloat all_reduce_to_all_fmax(jfloat val);
extern jdouble all_reduce_to_all_dmax(jdouble val);

extern jint all_reduce_to_all_imin(jint val);
extern jbyte all_reduce_to_all_bmin(jbyte val);
extern jchar all_reduce_to_all_cmax(jchar val);
extern jshort all_reduce_to_all_shmax(jshort val);
extern jlong all_reduce_to_all_lmax(jlong val);
extern jfloat all_reduce_to_all_fmin(jfloat val);
extern jdouble all_reduce_to_all_dmin(jdouble val);

extern jboolean all_reduce_to_all_or(jboolean val);
extern jboolean all_reduce_to_all_xor(jboolean val);
extern jboolean all_reduce_to_all_and(jboolean val);


/* upward scan from Proc 0 to Proc (PROCS - 1) */
extern jint all_scan_iadd(jint val);
extern jbyte all_scan_badd(jbyte val);
extern jshort all_scan_shadd(jshort val);
extern jchar all_scan_cadd(jchar val);
extern jlong all_scan_ladd(jlong val);
extern jfloat all_scan_fadd(jfloat val);
extern jdouble all_scan_dadd(jdouble val);

extern jint all_scan_imult(jint val);
extern jbyte all_scan_bmult(jbyte val);
extern jshort all_scan_shmult(jshort val);
extern jchar all_scan_cmult(jchar val);
extern jlong all_scan_lmult(jlong val);
extern jfloat all_scan_fmult(jfloat val);
extern jdouble all_scan_dmult(jdouble val);

extern jint all_scan_imax(jint val);
extern jbyte all_scan_bmax(jbyte val);
extern jshort all_scan_shmax(jshort val);
extern jchar all_scan_cmax(jchar val);
extern jlong all_scan_lmax(jlong val);
extern jfloat all_scan_fmax(jfloat val);
extern jdouble all_scan_dmax(jdouble val);

extern jint all_scan_imin(jint val);
extern jbyte all_scan_bmin(jbyte val);
extern jshort all_scan_shmin(jshort val);
extern jchar all_scan_cmin(jchar val);
extern jlong all_scan_lmin(jlong val);
extern jfloat all_scan_fmin(jfloat val);
extern jdouble all_scan_dmin(jdouble val);

extern jboolean all_scan_or(jboolean val);
extern jboolean all_scan_xor(jboolean val);
extern jboolean all_scan_and(jboolean val);

extern void com_init();

#endif








