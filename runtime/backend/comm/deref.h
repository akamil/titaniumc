#ifndef _include_deref_h_
#define _include_deref_h_

#include <gp-trace/gp-trace.h>
#include <tic.h>
#include <ctype.h>

#ifdef WIDE_POINTERS
#ifdef COMM_GASNET
  #define DEREF_GLOBAL_T(val,ptr,T) do {                            \
    ti_srcpos();                                                    \
    gasnet_get((T*)&(val), GET_BOX(ptr), GET_ADDR(ptr), sizeof(T)); \
  } while(0)

  #define WEAK_DEREF_GLOBAL_T(val,ptr,T) do {                           \
    ti_srcpos();                                                        \
    gasnet_get_nbi((T*)&(val), GET_BOX(ptr), GET_ADDR(ptr), sizeof(T)); \
  } while(0)

  #ifdef GASNET_TRACE
    int _ti_trace_amgetptr_seen;
    int _ti_trace_amgetptr_tracelocal;
    #define TI_TRACE_AMGETPTR(ptr, nbytes) do {                                         \
        jGPointer _ptr = (ptr);                                                         \
        if (isDirectlyAddressable(_ptr)) {                                              \
          if (!_ti_trace_amgetptr_seen) {                                               \
              const char *p = getenvMaster("GASNET_TRACELOCAL");                        \
                _ti_trace_amgetptr_tracelocal = !p || atoi(p) || toupper(*p) == 'Y';    \
                  _ti_trace_amgetptr_seen = 1;                                          \
          }                                                                             \
          if (_ti_trace_amgetptr_tracelocal)                                            \
              ti_trace_printf(("GET_LOCAL: sz = %6llu", (unsigned long long)(nbytes))); \
        } else {                                                                        \
          ti_trace_printf(("GET_AM: sz = %6llu", (unsigned long long)(nbytes)));        \
        }                                                                               \
      } while (0)
  #else
    #define TI_TRACE_AMGETPTR(ptr, nbytes) ((void)0)
  #endif

  #define DEREF_GLOBAL_jboolean(val, ptr)      DEREF_GLOBAL_T(val,ptr,jbyte)
  #define DEREF_GLOBAL_jbyte(val, ptr)         DEREF_GLOBAL_T(val,ptr,jbyte)
  #define DEREF_GLOBAL_jchar(val, ptr)         DEREF_GLOBAL_T(val,ptr,jchar)
  #define DEREF_GLOBAL_jshort(val, ptr)        DEREF_GLOBAL_T(val,ptr,jshort)
  #define DEREF_GLOBAL_jint(val, ptr)          DEREF_GLOBAL_T(val,ptr,jint)
  #define DEREF_GLOBAL_jlong(val, ptr)         DEREF_GLOBAL_T(val,ptr,jlong)
  #define DEREF_GLOBAL_jfloat(val, ptr)        DEREF_GLOBAL_T(val,ptr,jfloat)
  #define DEREF_GLOBAL_jdouble(val, ptr)       DEREF_GLOBAL_T(val,ptr,jdouble)
  #define DEREF_GLOBAL_lp(val, ptr)  IF_DISTGC({TI_TRACE_AMGETPTR(CAST_GPTR(ptr), sizeof(void*)); \
                                                *(void **)(&(val)) = __lp_read(CAST_GPTR(ptr));}, \
                                               DEREF_GLOBAL_T(val,ptr,void *) )
  #define DEREF_GLOBAL_gp(val, ptr)  IF_DISTGC({TI_TRACE_AMGETPTR(CAST_GPTR(ptr), sizeof(jGPointer)); \
                                                *(jGPointer *)(&(val)) = __gp_read(CAST_GPTR(ptr));}, \
                                               DEREF_GLOBAL_T(val,ptr,jGPointer) )

  #define WEAK_DEREF_GLOBAL_jboolean(val, ptr) WEAK_DEREF_GLOBAL_T(val,ptr,jbyte)
  #define WEAK_DEREF_GLOBAL_jbyte(val, ptr)    WEAK_DEREF_GLOBAL_T(val,ptr,jbyte)
  #define WEAK_DEREF_GLOBAL_jchar(val, ptr)    WEAK_DEREF_GLOBAL_T(val,ptr,jchar)
  #define WEAK_DEREF_GLOBAL_jshort(val, ptr)   WEAK_DEREF_GLOBAL_T(val,ptr,jshort)
  #define WEAK_DEREF_GLOBAL_jint(val, ptr)     WEAK_DEREF_GLOBAL_T(val,ptr,jint)
  #define WEAK_DEREF_GLOBAL_jlong(val, ptr)    WEAK_DEREF_GLOBAL_T(val,ptr,jlong)
  #define WEAK_DEREF_GLOBAL_jfloat(val, ptr)   WEAK_DEREF_GLOBAL_T(val,ptr,jfloat)
  #define WEAK_DEREF_GLOBAL_jdouble(val, ptr)  WEAK_DEREF_GLOBAL_T(val,ptr,jdouble)
  #define WEAK_DEREF_GLOBAL_lp(val, ptr) IF_DISTGC({TI_TRACE_AMGETPTR(CAST_GPTR(ptr), sizeof(void*)); \
                                                    __lp_get(&val, CAST_GPTR(ptr));},                 \
                                               WEAK_DEREF_GLOBAL_T(val,ptr,void *) )
  #define WEAK_DEREF_GLOBAL_gp(val, ptr) IF_DISTGC({TI_TRACE_AMGETPTR(CAST_GPTR(ptr), sizeof(jGPointer)); \
                                                    __gp_get(&CAST_GPTR(val), CAST_GPTR(ptr));},          \
                                               WEAK_DEREF_GLOBAL_T(val,ptr,jGPointer) )
#else
  #define DEREF_GLOBAL_jboolean(val, ptr)           val = (jboolean)__b_read(CAST_GPTR(ptr))
  #define DEREF_GLOBAL_jbyte(val, ptr)              val = (jbyte)__b_read(CAST_GPTR(ptr))
  #define DEREF_GLOBAL_jchar(val, ptr)              val = (jchar)__sh_read(CAST_GPTR(ptr))
  #define DEREF_GLOBAL_jshort(val, ptr)             val = __sh_read(CAST_GPTR(ptr))
  #define DEREF_GLOBAL_jint(val, ptr)               val = __i_read(CAST_GPTR(ptr))
  #define DEREF_GLOBAL_jlong(val, ptr)              val = __l_read(CAST_GPTR(ptr))
  #define DEREF_GLOBAL_jfloat(val, ptr)             val = __f_read(CAST_GPTR(ptr))
  #define DEREF_GLOBAL_jdouble(val, ptr)            val = __d_read(CAST_GPTR(ptr))
  #define DEREF_GLOBAL_lp(val, ptr)                 *(void **)(&(val)) = __lp_read(CAST_GPTR(ptr))
  #define DEREF_GLOBAL_gp(val, ptr)                 *(jGPointer *)(&(val)) = __gp_read(CAST_GPTR(ptr))

  #define WEAK_DEREF_GLOBAL_jboolean(val, ptr)      __b_get((jbyte *)&val, CAST_GPTR(ptr))
  #define WEAK_DEREF_GLOBAL_jbyte(val, ptr)         __b_get((jbyte *)&val, CAST_GPTR(ptr))
  #define WEAK_DEREF_GLOBAL_jchar(val, ptr)         __sh_get((jchar *)&val, CAST_GPTR(ptr))
  #define WEAK_DEREF_GLOBAL_jshort(val, ptr)        __sh_get(&val, CAST_GPTR(ptr))
  #define WEAK_DEREF_GLOBAL_jint(val, ptr)          __i_get(&val, CAST_GPTR(ptr))
  #define WEAK_DEREF_GLOBAL_jlong(val, ptr)         __l_get(&val, CAST_GPTR(ptr))
  #define WEAK_DEREF_GLOBAL_jfloat(val, ptr)        __f_get(&val, CAST_GPTR(ptr))
  #define WEAK_DEREF_GLOBAL_jdouble(val, ptr)       __d_get(&val, CAST_GPTR(ptr))
  #define WEAK_DEREF_GLOBAL_lp(val, ptr)            __lp_get(&val, CAST_GPTR(ptr))
  #define WEAK_DEREF_GLOBAL_gp(val, ptr)            __gp_get(&CAST_GPTR(val), CAST_GPTR(ptr))
#endif

#define DEREF_GLOBAL_bulk_(val, gptr, ptrembed)              \
    ti_bulk_read((void *)&(val),                             \
                 GET_BOX(gptr), GET_ADDR(gptr), sizeof(val), \
                 ptrembed)

#define DEREF_GLOBAL_bulk(val, gptr)        DEREF_GLOBAL_bulk_(val, gptr, tic_has_ptrs)
#define DEREF_GLOBAL_bulk_noptrs(val, gptr) DEREF_GLOBAL_bulk_(val, gptr, tic_no_ptrs)

#define DEREF_GLOBAL_anonymous_bulk(pval, nitems, gptr)                     \
    ti_bulk_read((void *)(pval),                                            \
                 GET_BOX(gptr), GET_ADDR(gptr), sizeof(*(pval)) * (nitems), \
                 tic_has_ptrs)

#define WEAK_DEREF_GLOBAL_bulk(val, gptr)                         \
    ti_bulk_read_weak((void *)&(val),                             \
                      GET_BOX(gptr), GET_ADDR(gptr), sizeof(val), \
                      tic_has_ptrs)

#define WEAK_DEREF_GLOBAL_anonymous_bulk(pval, nitems, gptr)                     \
    ti_bulk_read_weak((void *)(pval),                                            \
                      GET_BOX(gptr), GET_ADDR(gptr), sizeof(*(pval)) * (nitems), \
                      tic_has_ptrs)

#endif /* WIDE_POINTERS */

#endif /* !_include_deref_h_ */
