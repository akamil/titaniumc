#define TI_NO_SRCPOS
/* store.c - signaling global memory accesses (store) */
/* see copyright.txt for usage terms */

#include <tic.h>

ti_hsl_t	Store_mutex;

void store_init(void)
{
  if (MYBOXPROC == 0) {
    ti_hsl_init(&Store_mutex);
  }
}

/* .....................
 *
 * Keep track of STORESs
 *
 * .....................
 */

Counter sentBytes = 0;
Counter recvBytes = 0;

#ifdef COMM_AM2
/* Wait until all stores from all processors have completed */
void all_store_sync(void)
{
	int res;

	barrier();
	do {
		res = all_reduce_to_all_iadd(recvBytes-sentBytes);
	} while(res != 0);

	ti_hsl_lock(&Store_mutex);
	recvBytes = sentBytes = 0;
	ti_hsl_unlock(&Store_mutex);
	/* need this if ___i_reduce is synthesized or if interrupts are turned
	   on - ssl */
	barrier();
}
#else
#define all_store_sync()
#endif /* AMII */

#ifdef COMM_AM2
void all_store_sync_ctr(Counter *rbytes, Counter *sbytes)
{
	int res;

	barrier();
	do {
		res = all_reduce_to_all_iadd(*rbytes - *sbytes);
	} while(res != 0);

	ti_hsl_lock(&Store_mutex);
	*rbytes = *sbytes = 0;
	ti_hsl_unlock(&Store_mutex);
	/* need this if ___i_reduce is synthesized or if interrupts are turned
	   on - ssl */
	barrier();
}
#else 
#define all_store_sync_ctr(a, b)
#endif /* AMII */

/* ............................
 *
 * STOREs with explicit counter
 *
 * ............................
 */


#ifdef COMM_AM2
/* ------------------------------------------------------------------------------------ */
#define _TIC_STORE_HANDLER(shorttype, type)                                                         \
TI_INLINE(store_ ## shorttype ## _request)                                                         \
void store_ ## shorttype ## _request(tic_amtoken_t token, type *addr,                               \
                                      type data, Counter *ctr) {                                    \
  *addr = data;                                                                                     \
  __incr_ctr_size(ctr, sizeof(type));                                                               \
  TIC_NULL_REPLY(token);                                                                            \
}                                                                                                   \
TIC_AMSHORT(store_ ## shorttype ## _request, 3, 5,                                                  \
              (token, (type *)TIC_AMRECV_PTR32(a0),     (type)a1, (Counter *)TIC_AMRECV_PTR32(a2)    ), \
              (token, (type *)TIC_AMRECV_PTR64(a0, a1), (type)a2, (Counter *)TIC_AMRECV_PTR64(a3, a4)))

_TIC_STORE_HANDLER(b, jbyte);
_TIC_STORE_HANDLER(sh, jshort);
_TIC_STORE_HANDLER(i, jint);
/* ------------------------------------------------------------------------------------ */
TI_INLINE(store_f_request)
void store_f_request(tic_amtoken_t token, jfloat *addr,
                     jfloat data, Counter *ctr) {
  *addr = data;
  __incr_ctr_size(ctr, sizeof(jfloat));
  TIC_NULL_REPLY(token);
}
TIC_AMSHORT(store_f_request, 3, 5,
  (token, (jfloat *)TIC_AMRECV_PTR32(a0),     TIC_AMRECV_JFLOAT(a1), (Counter *)TIC_AMRECV_PTR32(a2)    ),
  (token, (jfloat *)TIC_AMRECV_PTR64(a0, a1), TIC_AMRECV_JFLOAT(a2), (Counter *)TIC_AMRECV_PTR64(a3, a4)));
/* ------------------------------------------------------------------------------------ */
TI_INLINE(store_d_request)
void store_d_request(tic_amtoken_t token, jdouble *addr,
                     jdouble data, Counter *ctr) {
  *addr = data;
  __incr_ctr_size(ctr, sizeof(jdouble));
  TIC_NULL_REPLY(token);
}
TIC_AMSHORT(store_d_request, 4, 6,
  (token, (jdouble *)TIC_AMRECV_PTR32(a0),     TIC_AMRECV_JDOUBLE(a1, a2), (Counter *)TIC_AMRECV_PTR32(a3)    ),
  (token, (jdouble *)TIC_AMRECV_PTR64(a0, a1), TIC_AMRECV_JDOUBLE(a2, a3), (Counter *)TIC_AMRECV_PTR64(a4, a5)));
/* ------------------------------------------------------------------------------------ */
TI_INLINE(store_l_request)
void store_l_request(tic_amtoken_t token, jlong *addr,
                     jlong data, Counter *ctr) {
  *addr = data;
  __incr_ctr_size(ctr, sizeof(jlong));
  TIC_NULL_REPLY(token);
}
TIC_AMSHORT(store_l_request, 4, 6,
  (token, (jlong *)TIC_AMRECV_PTR32(a0),     TIC_AMRECV_JLONG(a1, a2), (Counter *)TIC_AMRECV_PTR32(a3)    ),
  (token, (jlong *)TIC_AMRECV_PTR64(a0, a1), TIC_AMRECV_JLONG(a2, a3), (Counter *)TIC_AMRECV_PTR64(a4, a5)));
/* ------------------------------------------------------------------------------------ */
TI_INLINE(store_lp_request)
void store_lp_request(tic_amtoken_t token, void *_addr,
                     void *data, Counter *ctr) {
  void **addr = (void **)_addr;
  *addr = data;
  __incr_ctr_size(ctr, sizeof(void *));
  TIC_NULL_REPLY(token);
}
TIC_AMSHORT(store_lp_request, 3, 6,
  (token, (void *)TIC_AMRECV_PTR32(a0),     TIC_AMRECV_PTR32(a1),     (Counter *)TIC_AMRECV_PTR32(a2)    ),
  (token, (void *)TIC_AMRECV_PTR64(a0, a1), TIC_AMRECV_PTR64(a2, a3), (Counter *)TIC_AMRECV_PTR64(a4, a5)));
/* ------------------------------------------------------------------------------------ */
TI_INLINE(store_gp_request)
void store_gp_request(tic_amtoken_t token, void *_dest,
                     Box boxData, void *addrData, Counter *ctr) {
  jGPointer *dest = (jGPointer *)_dest;
  *dest = toglobalb(boxData, addrData);
  __incr_ctr_size(ctr, sizeof(jGPointer));
  TIC_NULL_REPLY(token);
}
TIC_AMSHORT(store_gp_request, 4, 7,
  (token, (void *)TIC_AMRECV_PTR32(a0),     a1, TIC_AMRECV_PTR32(a2),     (Counter *)TIC_AMRECV_PTR32(a3)    ),
  (token, (void *)TIC_AMRECV_PTR64(a0, a1), a2, TIC_AMRECV_PTR64(a3, a4), (Counter *)TIC_AMRECV_PTR64(a5, a6)));
/* ------------------------------------------------------------------------------------ */
#endif /* AMII */

