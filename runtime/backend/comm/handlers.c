/* handlers.c - Install AM handlers */
/* see copyright.txt for usage terms */

#define TI_NO_SRCPOS
#include <tic.h>

#ifndef COMM_AM2
int _tic_dummy_handler_c = 0; /* define at least one symbol to avoid warnings */
#else /* COMM_AM2 */
#if defined(AMNOW)
extern void __error_handler(int status, op_t opcode, void *argblock);
#endif
static void tic_default_app_amhandler();

#define TIC_HANDLER TIC_AMHANDLER_ENTRY
tic_handler_func_t tic_handler_list[] = {
  #if defined(AMNOW)
  {	0,		(void (*)())__error_handler	},
  #endif

  /* barrier.c */
  TIC_HANDLER(barrier_incr_request),

  /* mem.c */
  TIC_HANDLER(decr_ctr_reply),
  TIC_HANDLER(decr_put_ctr_reply),

  TIC_HANDLER(get_b_request),
  TIC_HANDLER(get_sh_request),
  TIC_HANDLER(get_i_request),
  TIC_HANDLER(get_f_request),
  TIC_HANDLER(get_d_request),
  TIC_HANDLER(get_l_request),
  TIC_HANDLER(get_lp_request),
  TIC_HANDLER(get_gp_request),

  TIC_HANDLER(get_b_reply),
  TIC_HANDLER(get_sh_reply),
  TIC_HANDLER(get_i_reply),
  TIC_HANDLER(get_f_reply),
  TIC_HANDLER(get_d_reply),
  TIC_HANDLER(get_l_reply),
  TIC_HANDLER(get_lp_reply),
  TIC_HANDLER(get_gp_reply),

  TIC_HANDLER(put_b_request),
  TIC_HANDLER(put_sh_request),
  TIC_HANDLER(put_i_request),
  TIC_HANDLER(put_f_request),
  TIC_HANDLER(put_d_request),
  TIC_HANDLER(put_l_request),
  TIC_HANDLER(put_lp_request),
  TIC_HANDLER(put_gp_request),
  
  /* store.c */
  TIC_HANDLER(store_b_request),
  TIC_HANDLER(store_sh_request),
  TIC_HANDLER(store_i_request),
  TIC_HANDLER(store_f_request),
  TIC_HANDLER(store_d_request),
  TIC_HANDLER(store_l_request),
  TIC_HANDLER(store_lp_request),
  TIC_HANDLER(store_gp_request),

  /* bulk.c */
  TIC_HANDLER(bulk_get_request),
  TIC_HANDLER(bulk_put_complete_reply),
  TIC_HANDLER(bulk_store_request),
  TIC_HANDLER(bulk_put_request),
  TIC_HANDLER(bulk_get_complete_reply),

  /* newbulk.c */
  TIC_HANDLER(misc_null_reply),
  TIC_HANDLER(misc_delete_request),
  TIC_HANDLER(misc_alloc_request),
  TIC_HANDLER(misc_alloc_reply),

  TIC_HANDLER(strided_pack_request),
  TIC_HANDLER(strided_pack_reply),
  TIC_HANDLER(strided_unpackAll_request),
  TIC_HANDLER(strided_unpack_reply),
  TIC_HANDLER(strided_unpackOnly_request),

  TIC_HANDLER(sparse_simpleScatter_request),
  TIC_HANDLER(sparse_done_reply),
  TIC_HANDLER(sparse_generalScatter_request),
  TIC_HANDLER(sparse_largeScatterNoDelete_request),
  TIC_HANDLER(sparse_simpleGather_request),
  TIC_HANDLER(sparse_simpleGather_reply),
  TIC_HANDLER(sparse_generalGather_request),
  TIC_HANDLER(sparse_largeGather_request),
  TIC_HANDLER(sparse_largeGather_reply),

  /* monitor-dist.c */
  TIC_HANDLER(monitor_enter_request),
  TIC_HANDLER(monitor_enter_reply),
  TIC_HANDLER(monitor_exit_request),
  TIC_HANDLER(monitor_exit_reply),
  TIC_HANDLER(monitor_wait_request),
  TIC_HANDLER(monitor_wait_reply),
  TIC_HANDLER(monitor_notify_request),
  TIC_HANDLER(monitor_notify_all_request),
  TIC_HANDLER(monitor_signal_request),
  TIC_HANDLER(monitor_cancel_wait_request),

  /* stats.c */
#if defined(COMM_AMUDP) || defined(COMM_AMMPI)
  TIC_HANDLER(amstats_request),
#endif

  /* comm_utils.c */
#ifdef MISALIGNED_CSTATIC_DATA
  TIC_HANDLER(staticdata_tablegather),
  TIC_HANDLER(staticdata_tablebcast),
#endif

  /* reserved for application use */
  { TIC_AMIDX(apphandler_1), tic_default_app_amhandler },
  { TIC_AMIDX(apphandler_2), tic_default_app_amhandler },
  { TIC_AMIDX(apphandler_3), tic_default_app_amhandler },
  { TIC_AMIDX(apphandler_4), tic_default_app_amhandler }

};
#undef TIC_HANDLER

int tic_numhandlers = (sizeof(tic_handler_list) / sizeof(tic_handler_func_t));

void tic_app_amhandler_init() {
  #define TIC_APP_HANDLER(idx) \
    if (tic_app_amhandler##idx) tic_handler_list[TIC_AMIDX(apphandler_##idx)-TIC_HANDLER_BASE].fnptr = tic_app_amhandler##idx
  TIC_APP_HANDLER(1);
  TIC_APP_HANDLER(2);
  TIC_APP_HANDLER(3);
  TIC_APP_HANDLER(4);
}
static void tic_default_app_amhandler() {
  printf("ERROR: called a non-registered application AM handler\n");
  abort();
}

#if defined(COMM_AMNOW)
#include <am2/arg_block.h>

void __error_handler(int status, op_t opcode, arg_block_t *argblock)
{
        int i;

        printf("AM layer failed on node %d.\n", myProcess());

        printf ("opcode = %d\n", argblock->opcode);
        printf ("request endpoint: %#X\n", argblock->request_endpoint);
        printf ("reply endpoint: %d\n", argblock->reply_endpoint);
        printf ("dest offset: %d\n", argblock->dest_offset);
        printf ("source addr: %#X\n", argblock->source_addr);
        printf ("nbytes: %d\n", argblock->nbytes);
        printf ("handler: %d\n", argblock->handler);
        for (i = 0; i < 8; i++)
            printf ("data[%d]: %d\n", i, argblock->data[i]);

        switch (status) {
          case EBADARGS:
                printf("Invalid arguements to request or reply\n");
                break;
          case EBADENTRY:
                printf("Unbound table entry\n");
                break;
          case EBADTAG:
                printf("Sender Tag did not match Receiver Tag\n");
                break;
          case EBADHANDLER:
                printf("Invalid index into receiver's handler table\n");
                break;
          case EBADSEGOFF:
                printf("Invalid offset into destination memory segment\n");
                break;
          case EBADLENGTH:
                printf("Bulk transfer length exceeds sengment length\n");
                break;
          case EBADENDPOINT:
                printf("Destination endpoint does not exist\n");
                break;
          case ECONGESTION:
                printf("Persistent congestion at destination\n");
                break;
          case EUNREACHABLE:
                printf("Unreachable destination\n");
                break;
          default:
                printf("Unknown status value: %d\n", status);
        }

        free_resources ();

        fflush(stdout);
        exit(-1);
}
#endif /* COMM_AMNOW */

#endif /*AMII*/
	
	
	

