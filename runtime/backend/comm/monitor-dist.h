#ifndef MONITOR_DIST_H
#define MONITOR_DIST_H

TIC_AMSHORT_DECLARE(monitor_enter_request, 3, 4);
TIC_AMSHORT_DECLARE(monitor_enter_reply, 2, 2);
TIC_AMSHORT_DECLARE(monitor_exit_request, 3, 4);
TIC_AMSHORT_DECLARE(monitor_exit_reply, 3, 3);
TIC_AMSHORT_DECLARE(monitor_wait_request, 3, 4);
TIC_AMSHORT_DECLARE(monitor_wait_reply, 3, 3);
TIC_AMSHORT_DECLARE(monitor_notify_request, 1, 2);
TIC_AMSHORT_DECLARE(monitor_notify_all_request, 3, 4);
TIC_AMSHORT_DECLARE(monitor_signal_request, 1, 1);
TIC_AMSHORT_DECLARE(monitor_cancel_wait_request, 3, 4);

#endif /*  MONITOR_DIST_H */

