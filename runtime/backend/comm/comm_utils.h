/* comm_utils.h - miscellaneous declarations & utilities */
/* see copyright.txt for usage terms */

#ifndef _COMM_UTILS_H
#define _COMM_UTILS_H

/* get MAXHOSTNAMELEN */
#ifdef PLATFORM_OS_SOLARIS
#include <netdb.h>
#else
#include <sys/param.h>
#endif
#ifndef MAXHOSTNAMELEN
  #ifdef HOST_NAME_MAX
    #define MAXHOSTNAMELEN HOST_NAME_MAX
  #else
    #define MAXHOSTNAMELEN 1024 /* give up */
  #endif
#endif

#define INIT_DONE = 4;

/* tic_monitor_t is used for language level monitors (objects, classes, etc). */
#ifdef COMM_AM2
	typedef struct proc_queue {
    		volatile Box box;
    		volatile Process proc;
    		volatile struct proc_queue * volatile cdr;
	} proc_queue;

	typedef struct tic_monitor_t {
  		volatile Process proc;			/* Process that owns the monitor */
  		volatile Box box;			/* Box that the process lives on */
                #ifdef COMM_GASNET
                  gasnet_hsl_t hsl;
                #endif
  		volatile proc_queue * volatile waitingProcs; 	/* list of waiting processes */
  		volatile proc_queue * volatile tryingProcs;	/* list of trying processes */
	} *tic_monitor_t;
#else
	typedef struct tic_monitor_t {
  		ti_lock_t lock;
  		ti_cond_t condvar;
  		volatile Process proc;
  		volatile int nesting;
  		#ifdef CAUTIOUS_MONITORS
    		  /* some pthread implementations have wait functions require some extra help */
    		  volatile int numwaiting;
    		  volatile int numsignaled;
  		#endif
	} *tic_monitor_t;
#endif /* COMM_AM2 */

/* tic_simplemonitor_t is used for other special purposes in the runtime
 * and always uses real thread locks/condvars, regardless of backend
 */
typedef struct tic_simplemonitor_t {
  ti_lock_t lock;
  ti_cond_t condvar;
  #ifdef CAUTIOUS_MONITORS
    /* some pthread implementations have wait functions require some extra help */
    volatile int numwaiting;
    volatile int numsignaled;
  #endif
} tic_simplemonitor_t;


typedef struct process_monitor_table {
  jGPointer monitor;
  unsigned int nesting;
  struct process_monitor_table *cdr;
} process_monitor_table;

struct boxInfo;

typedef struct _processInfo {
    Process processNumber;           		/* Process Number */
    Process boxProcessNumber;        		/* Process Number w.r.t. this box */
    struct boxInfo *box;             		/* The box this process is on */
    tic_thread_t threadID;     		        /* Local thread ID on this box */

    process_monitor_table *locked_monitors;    	/* List of monitors (PMT) locked by this thread */
    process_monitor_table *pmt_freelist;        /* freelist of PMT entries */
    volatile int waiting_for_monitor;        	/* Current monitor this process is waiting for */
    volatile Process signal_proc;		/* Used to signal next proc for lock */
    volatile Box signal_box;			/* Corresponding box */

    volatile int barrier_phase;			/* Private state used to implement local barrier in barrier.c */
    volatile int broadcast_cycle;		/* Private state used to implement local broadcast */
    volatile int broadcast_phase;	

    char pad[256];                		/* Ensure each process struct lives on a separate cache line */
} processInfo;

typedef struct boxInfo {
  char *DNSname;                   /* DNS name of box */
  int pid;                         /* Process ID */
  int argc;                        /* Number of Arguments to Titanium */
  char **argv;                     /* Arguments to Titanium */
  Box boxNumber;                   /* Box Number */
  char *processesPerProcessors;    /* Processes per Processor */
  Process countProcesses;          /* Number of processes on this box */
  Process countProcessors;         /* Processors we want to use per box */
  Process totalProcessors;         /* Processors per box */
  processInfo *processes;          /* List of processes on this box */
} boxInfo;


/* Function Prototypes */

int COMM_Init_One();
int COMM_Init_Two(char *memory);
void COMM_Init_Three(char *memory);
void COMM_Init_Four(int argc, char **argv);
void COMM_Init_Gpid();
processInfo *COMM_GetHisBoxProcess(Process p) __attribute__ ((const));
processInfo *COMM_GetHisProcess(Process p) __attribute__ ((const));
#define COMM_GetMyProcess() myProcess()
juint COMM_GetMyGuid() __attribute__ ((const));
julong COMM_GetMyGuid64() __attribute__ ((const));
Box COMM_GetBoxNumberForProcNumber(Process processNumber) __attribute__ ((const));
Process COMM_GetHisBoxProcNumber(Process processNumber) __attribute__ ((const));
Process COMM_GetProxyProcNumberForBoxNumber(Box boxNumber) __attribute__ ((const));
float COMM_GetMyProcsPerProc() __attribute__ ((const));

/* For init-time use ONLY! Use tic alternatives for better performance */
Box COMM_GetMyBoxNumber() __attribute__ ((const)); /* MYBOX */
Process COMM_GetParallelDegree() __attribute__ ((const)); /* PROCS */
Process COMM_GetMyBParallelDegree() __attribute__ ((const)); /* MYBOXPROCS */
Box COMM_GetBParallelDegree() __attribute__ ((const)); /* BOXES */

#ifndef COMM_AM2
	void COMM_thread_cons_monitor(processInfo *pi, jGPointer monitor);
	void COMM_thread_remove_monitor(processInfo *pi, jGPointer monitor);
#endif

/* define MISALIGNED_CSTATIC_DATA on platforms where the static data segment
   has a different base address in virtual memory on each process
 */ 
#ifdef MEMORY_DISTRIBUTED
  #if defined(__crayx1)
    #define MISALIGNED_CSTATIC_DATA
  #endif
#endif

#ifdef MISALIGNED_CSTATIC_DATA
  extern intptr_t *_tic_cstaticaddr_translationtable;

  #define TIC_TRANSLATE_CSTATIC_ADDR(localaddr, targetbox) \
    (assert((unsigned)(targetbox) < BOXES),                \
     assert(_tic_cstaticaddr_translationtable != NULL),    \
     (void *)(((intptr_t)(localaddr))+_tic_cstaticaddr_translationtable[(targetbox)]))
  #define TIC_TRANSLATE_CSTATIC_ADDR_FROM(remoteaddr, remotebox) \
    (assert((unsigned)(remotebox) < BOXES),                      \
     assert(_tic_cstaticaddr_translationtable != NULL),          \
     (void *)(((intptr_t)(remoteaddr))-_tic_cstaticaddr_translationtable[(remotebox)]))

  /* currently we assume (and assert) that static data segment and code text segment
     are offset by the same amount on each box, because this is true on the X1.
     future platforms may require separate logic here for function ptrs
   */
  #define TIC_TRANSLATE_FUNCTION_ADDR TIC_TRANSLATE_CSTATIC_ADDR
  extern void tic_init_staticdata_translation();
  TIC_AMMEDIUM_DECLARE(staticdata_tablegather, 1, 1);
  TIC_AMMEDIUM_DECLARE(staticdata_tablebcast, 0, 0);
#else
  #define TIC_TRANSLATE_CSTATIC_ADDR(localaddr, targetbox) \
    (assert((unsigned)(targetbox) < BOXES), (localaddr))
  #define TIC_TRANSLATE_CSTATIC_ADDR_FROM(remoteaddr, remotebox) \
    (assert((unsigned)(remotebox) < BOXES), (remoteaddr))
  #define TIC_TRANSLATE_FUNCTION_ADDR TIC_TRANSLATE_CSTATIC_ADDR
  #define tic_init_staticdata_translation()
#endif
#define TI_TRANSLATE_CSTATIC_ADDR TIC_TRANSLATE_CSTATIC_ADDR
#define TI_TRANSLATE_CSTATIC_ADDR_FROM TIC_TRANSLATE_CSTATIC_ADDR_FROM

#endif /* _COMM_UTILS_H */
