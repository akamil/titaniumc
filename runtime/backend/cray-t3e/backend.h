#ifndef _include_cray_t3e_backend_h_
#define _include_cray_t3e_backend_h_


#include "backend-defines.h"

#define barrier() shmem_barrier_all()

extern int MYPROC;
extern int PROCS;
#define MYBOX MYPROC
#define BOXES PROCS
#define MYBOXPROCS 1
#define MYBOXPROC 0
#define MAX_BOX_PROCS 1


#ifndef _CRAYT3E
#error not _CRAYT3E
#endif

#define _TI_PRAGMA(x) _Pragma ( #x )
#define TI_INLINE(fnname) _TI_PRAGMA(_CRI inline fnname) static

#include <mpp/shmem.h>

#include "craystubs.h"
#include "primitives.h"
#include "monitor.h"
#include "mem-wide.h"
#include "static-global.h"

/* this macro requires dest and src have alignment of at least length */
#define FAST_MEMCPY(dest, src, length) do {           \
  switch(length) {                                    \
    case sizeof(jubyte):                              \
      *((jubyte *)(dest)) = *((jubyte *)(src));       \
      break;                                          \
    case sizeof(juint):                               \
      *((juint *)(dest)) = *((juint *)(src));         \
      break;                                          \
    case sizeof(julong):                              \
      *((julong *)(dest)) = *((julong *)(src));       \
      break;                                          \
    default:                                          \
      memcpy(dest, src, length);                      \
  } } while(0)

#include "assign.h"
#include "deref.h"
#include "bulk.h"
#include "comm.h"


#endif /* !_include_cray_t3e_backend_h_ */
