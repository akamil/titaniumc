#include <netdb.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/param.h>
#include <signal.h>
#include <stdio.h>

#include "backend.h"
#include "native-utils.h"

char *RUNTIME_MODEL = TIC_BACKEND_NAME;

/* these strings are read from the executable by ident and tcrun */
TI_IDENT(BackendIdent,"$TitaniumBackend: " TIC_BACKEND_NAME " $");

extern void region_init(void);
extern void stats_init(void);
void printExitStats(int sglobal);

int MYPROC = -1;
int PROCS = -1;

int curr_sade_num = 0;

volatile int tic_frozen = 0;
void unfreeze() { /* use "call unfreeze()" in totalview */
  printf("Titanium application unfrozen - ready for debugging...\n");
  tic_frozen = 0;
}

void free_resources(void) {
}

int main(int argc, char **argv)
{
  MYPROC = shmem_my_pe(); /* could also use _my_pe() here */
  PROCS = shmem_n_pes();  /* could also use _n_pes() or _num_pes() here */

  if (MYPROC == 0 && !getenv("TI_BACKEND_SILENT")) {
    printf("Running %s on %d nodes\n", argv[0], PROCS);
    fflush(stdout);
  }
  
  if (MYPROC < 0 || MYPROC >= PROCS || PROCS < 0 || PROCS > 65536) {
    fprintf(stderr,"ERROR: Inconsistent processor info: MYPROC=%i PROCS=%i\n", 
            MYPROC, PROCS); 
    fflush(stderr);
    abort();
  }

  if (getenv("TI_FREEZE"))
    {
      char hostname[MAXHOSTNAMELEN] = "(unknown)";
      tic_frozen = 1;

      gethostname(hostname, sizeof(hostname));
      printf("frozen for debugger: host %s, pid %d\n", hostname, getpid());

      while (tic_frozen)
        sleep(1);
    }

  #if 0
    fprintf(stderr,
     "Processor starting MYPROC=%i PROCS=%i MYBOX=%i BOXES=%i MYBOXPROC=%i MYBOXPROCS=%i\n", 
      MYPROC, PROCS, MYBOX, BOXES, MYBOXPROC, MYBOXPROCS); 
    fflush(stderr);
  #endif

  bcast_init();
  barrier();
  region_init();
  stats_init();

  srand(MYPROC);

  /* Setup a trap handler for SIGFPE (floating point exception) */
  sigignore(SIGFPE);

  barrier();

  ti_main(argc, argv);

  barrier();

  printExitStats(1);

  return 0;
}
