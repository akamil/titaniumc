#ifndef _include_assign_h_
#define _include_assign_h_

#include "ti_config.h"
#include "backend.h"
#include "primitives.h"
#include "runtime-options.h"
#include "eregs.h"

#include <mpp/shmem.h>
#include <mpp/mpphw_t3e.h>

/* Put routines */

TI_INLINE(be_put_8) 
void be_put_8(int proc, void *addr, jlong val) {
  if (proc == MYPROC) {
    *((jlong *) addr) = val;
    return;
  } else {
    int ereg_num = ER_SADE_TO_EREG(curr_sade_num);
    volatile long * const Ecmd = ER_WRITE_WEAK64_CMD_ADDR(ereg_num);
    volatile jlong * const Edata = ER_EREG_DATA_ADDR(volatile jlong *,
						     ereg_num);

    /* Load ereg and give command */
    *Edata = val;
    er_local_write_memory_sync();
    *Ecmd = ER_READ_WRITE_CMD(proc, addr);
    er_local_memory_sync();

    curr_sade_num = ER_NEXT_SADE(curr_sade_num);
    /* The write will be done asynchronously */
  }
}
#pragma _CRI inline be_put_8

TI_INLINE(be_put_4) 
void be_put_4(int proc, void *addr, jint val) {
  if (proc == MYPROC) {
    *((jint *) addr) = val;
    return;
  } else {
    int ereg_num = ER_SADE_TO_EREG(curr_sade_num);
    volatile long * const Ecmd = ER_WRITE_WEAK32_CMD_ADDR(ereg_num);
    volatile jint * const Edata = ER_EREG_DATA_ADDR(volatile jint *,
						    ereg_num);

    /* Load ereg and give command */
    *Edata = val;
    er_local_write_memory_sync();
    *Ecmd = ER_READ_WRITE_CMD(proc, addr);
    er_local_memory_sync();

    curr_sade_num = ER_NEXT_SADE(curr_sade_num);
    /* The write will be done asynchronously */
  }
}
#pragma _CRI inline be_put_4

/* Write routines */

TI_INLINE(be_write_4) 
void be_write_4(int proc, void *addr, jint val) {
  if (proc == MYPROC) {
    *((jint *) addr) = val;
    return;
  } else {
    int ereg_num = ER_SADE_TO_EREG(curr_sade_num);
    volatile long * const Ecmd = ER_WRITE_WEAK32_CMD_ADDR(ereg_num);
    volatile jint * const Edata = ER_EREG_DATA_ADDR(volatile jint *,
						    ereg_num);

    /* Must make sure all previous global reads/writes have completed. */
    er_global_memory_sync();

    /* Load ereg and give command */
    *Edata = val;
    er_local_write_memory_sync();
    *Ecmd = ER_READ_WRITE_CMD(proc, addr);
    er_local_memory_sync();

    /* ... and wait for completion. */
    er_global_write_memory_sync();
  }
}
#pragma _CRI inline be_write_4

TI_INLINE(be_write_8) 
void be_write_8(int proc, void *addr, jlong val) {
  if (proc == MYPROC) {
    *((jlong *) addr) = val;
    return;
  } else {
    int ereg_num = ER_SADE_TO_EREG(curr_sade_num);
    volatile long * const Ecmd = ER_WRITE_WEAK64_CMD_ADDR(ereg_num);
    volatile jlong * const Edata = ER_EREG_DATA_ADDR(volatile jlong *,
						     ereg_num);

    /* Must make sure all previous global reads/writes have completed. */
    er_global_memory_sync();

    /* Load ereg and give command */
    *Edata = val;
    er_local_write_memory_sync();
    *Ecmd = ER_READ_WRITE_CMD(proc, addr);
    er_local_memory_sync();

    /* ... and wait for completion. */
    er_global_write_memory_sync();
  }
}
#pragma _CRI inline be_write_8

#define BACKEND_ASSIGN_GLOBAL(ptr, val, size, send)	\
  do {							\
    if (isLocal(ptr))					\
      FAST_MEMCPY(TO_LOCAL(ptr), &val, size);	        \
    else {						\
      switch (size) {				        \
      case 4:						\
	be_ ## send ## _4 (TO_PROC(ptr),		\
			   (void *) TO_LOCAL(ptr),	\
			   *(jint *)&val);		\
	break;						\
      case 8:						\
	be_ ## send ## _8 (TO_PROC(ptr),		\
			   (void *) TO_LOCAL(ptr),	\
			   *(jlong *)&val);		\
	break;						\
      default:						\
	be_bulk_ ## send (TO_PROC(ptr),			\
			  (void *) TO_LOCAL(ptr),	\
			  &val,				\
			  sizeof(val));			\
      }							\
    }							\
  } while (0)

#define ASSIGN_GLOBAL(     ptr, val, size)  BACKEND_ASSIGN_GLOBAL(ptr, val, size, write)
#define WEAK_ASSIGN_GLOBAL(ptr, val, size)  BACKEND_ASSIGN_GLOBAL(ptr, val, size, put)

#endif /* !_include_assign_h_ */
