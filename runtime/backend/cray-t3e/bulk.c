#include "ti_config.h"
#include "backend.h"
#include "primitives.h"
#include "deref.h"
#include "assign.h"

#include <mpp/shmem.h>

void be_bulk_read(void *d_addr, int s_proc, void *s_addr, int size)
{
  er_global_write_memory_sync();
  shmem_getmem(d_addr, s_addr, size, s_proc);
  shmem_quiet();
  er_global_memory_sync();
}

void be_bulk_write(int d_proc, void *d_addr, void *s_addr, int size)
{
  er_global_memory_sync();
  shmem_putmem(d_addr, s_addr, size, d_proc);
  shmem_quiet();
  er_global_memory_sync();
}

void be_bulk_put(int d_proc, void *d_addr, void *s_addr, int size)
{
  /* Flush Eregs for shmem to use them */
  er_global_memory_sync();
  shmem_putmem(d_addr, s_addr, size, d_proc);
}
