#ifndef _include_comm_h_
#define _include_comm_h_

#include "ti_config.h"
#include "backend.h"
#include "primitives.h"
#include "runtime-options.h"
#include "eregs.h"
#include "deref.h"
#include "assign.h"

#define ti_write_sync() er_global_write_memory_sync()
#define ti_read_sync()  er_global_read_memory_sync()
#define ti_sync()       er_global_memory_sync()

#define ti_bulk_read(d_addr, s_box, s_addr, size, ptr_embedding) \
  be_bulk_read(d_addr, s_box, s_addr, size)
#define ti_bulk_read_weak(d_addr, s_box, s_addr, size, ptr_embedding) \
  be_bulk_read(d_addr, s_box, s_addr, size)
#define ti_bulk_write_weak(d_box, d_addr, s_addr, size) \
  be_bulk_put(d_box, d_addr, s_addr, size)
#define ti_bulk_write(d_box, d_addr, s_addr, size) \
  be_bulk_write(d_box, d_addr, s_addr, size)

#endif /* _include_comm_h_ */
