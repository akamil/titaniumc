#include "primitives.h"

#if defined(__CRAY__) && defined(__GNUC__)

/* The dtoa does not seem to work, perhaps because there is no
   32-bit integer type.  So, fake it. */

#  include <stdio.h>

char *
g_fmt(char *b, double x)
{
  sprintf(b, "%20.10f", x);
}

#else /* defined(__CRAY__) && defined(__GNUC__) */

/* Other Cray compilers should be okay. */

#  include "../../fp-utils/dtoa-common.c"

#endif /* defined(__CRAY__) && defined(__GNUC__) */
