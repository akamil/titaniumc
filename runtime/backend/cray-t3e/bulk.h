#ifndef _include_crayt3e_bulk_h_
#define _include_crayt3e_bulk_h_

#include <string.h>
#include "backend.h"

#define ASSIGN_GLOBAL_anonymous_bulk( global, count, local )                 \
  do {                                                                          \
    if (isDirectlyAddressable( global ))                                        \
      /* can't use FAST_MEMCPY because data may not be properly aligned */      \
      memcpy(   TO_LOCAL( global ), (local), sizeof(*(local)) * (count) );      \
    else                                                                        \
      be_bulk_write( TO_PROC( global ),                                         \
		     TO_LOCAL( global ),                                        \
		     (local),                                                   \
		     sizeof(*(local)) * (count) );                              \
  } while (0)


#define DEREF_GLOBAL_anonymous_bulk( local, count, global )                  \
  do {                                                                          \
    if (isDirectlyAddressable( global ))                                        \
      /* can't use FAST_MEMCPY because data may not be properly aligned */      \
      memcpy(   (local), TO_LOCAL( global ), sizeof(*(local)) * (count) );      \
    else                                                                        \
      be_bulk_read( (local),                                                    \
		    TO_PROC( global ),                                          \
		    TO_LOCAL( global ),                                         \
		    sizeof(*(local)) * (count) );                               \
  } while (0)



#endif /* !_include_bulk_h_ */
