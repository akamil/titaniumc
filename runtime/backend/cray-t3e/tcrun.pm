package cray_t3e::tcrun;
use base qw(common::tcrun);

use strict;


########################################################################


sub processes ($$) {
    my ($self, $processes) = @_;
    $self->{processes} = $processes;
}


sub run ($@) {
    my $self = shift;
    $self->SUPER::run('mpprun', '-n', $self->{processes}, @_);
}


########################################################################


1;
