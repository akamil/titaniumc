#ifndef _include_deref_h_
#define _include_deref_h_

#include "ti_config.h"
#include "backend.h"
#include "primitives.h"
#include "runtime-options.h"
#include "eregs.h"

#include <mpp/shmem.h>
#include <mpp/mpphw_t3e.h>

/* Read routines */

TI_INLINE(be_read_8) 
jlong be_read_8(int proc, void *addr) {
  if (proc == MYPROC) {
    return *((jlong *) addr);
  } else {
    int ereg_num = ER_SADE_TO_EREG(curr_sade_num);
    volatile long * const Ecmd = ER_READ64_CMD_ADDR(ereg_num);
    volatile jlong * const Edata = ER_EREG_DATA_ADDR(volatile jlong *,
						     ereg_num);

    /* Must make sure all previous global puts have completed. */
    er_global_write_memory_sync();

    er_local_write_memory_sync();
    *Ecmd = ER_READ_WRITE_CMD(proc, addr);
    er_local_memory_sync();

    /* The following read will block until the data is returned. */
    return *((jlong *) Edata);
  }
}
#pragma _CRI inline be_read_8

TI_INLINE(be_read_4) 
jint be_read_4(int proc, void *addr) {
  if (proc == MYPROC) {
    return *((jint *) addr);
  } else {
    int ereg_num = ER_SADE_TO_EREG(curr_sade_num);
    volatile long * const Ecmd = ER_READ32_CMD_ADDR(ereg_num);
    volatile jint * const Edata = ER_EREG_DATA_ADDR(volatile jint *,
						    ereg_num);

    /* Must make sure all previous global puts have completed. */
    er_global_write_memory_sync();

    er_local_write_memory_sync();
    *Ecmd = ER_READ_WRITE_CMD(proc, addr);
    er_local_memory_sync();

    /* The following read will block until the data is returned. */
    return *((jint *) Edata);
  }
}
#pragma _CRI inline be_read_4

TI_INLINE(be_read_1) 
jbyte be_read_1(int proc, void *addr) {
  if (proc == MYPROC) {
    return *((jbyte *) addr);
  } else {
    /* There is no 8-bit read, so read 64-bits and only return the correct
       portion. */
    int ereg_num = ER_SADE_TO_EREG(curr_sade_num);
    volatile long * const Ecmd = ER_READ64_CMD_ADDR(ereg_num);
    volatile jlong * const Edata = ER_EREG_DATA_ADDR(volatile jlong *,
						     ereg_num);
    jUIntPointer fix_addr;
    jlong temp;

    /* Must make sure all previous global puts have completed. */
    er_global_write_memory_sync();

    fix_addr = (jUIntPointer) addr & 0x7;
    er_local_write_memory_sync();
    *Ecmd = ER_READ_WRITE_CMD(proc, (jUIntPointer) addr - fix_addr);
    er_local_memory_sync();

    /* The following read will block until the data is returned. */
    temp = *((jlong *) Edata);

    return *((jbyte *) ((jUIntPointer) (&temp) + fix_addr));
  }
}
#pragma _CRI inline be_read_1

#define DEREF_GLOBAL(result, ptr, size)                 \
  do {							\
    if (isLocal(ptr))					\
      FAST_MEMCPY(&result, TO_LOCAL(ptr), size);	\
    else {						\
    switch (size) {	  			        \
      case 1:						\
	*((jbyte *) &result) =				\
          be_read_1(TO_PROC(ptr),			\
		    (void *) TO_LOCAL(ptr));		\
	break;						\
      case 4:						\
	*((jint *) &result) =				\
          be_read_4(TO_PROC(ptr),			\
		    (void *) TO_LOCAL(ptr));		\
	break;						\
      case 8:						\
	*((jlong *) &result) =				\
          be_read_8(TO_PROC(ptr),			\
		    (void *) TO_LOCAL(ptr));		\
	break;						\
      default:						\
	be_bulk_read(&result,				\
		     TO_PROC(ptr),			\
		     (void *) TO_LOCAL(ptr),		\
		     size);                             \
      }							\
    }							\
  } while (0)


#endif /* !_include_deref_h_ */
