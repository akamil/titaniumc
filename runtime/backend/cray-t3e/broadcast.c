#include "ti_config.h"
#include "backend.h"
#include "primitives.h"
#include <bcast-common.c>

#include <mpp/shmem.h>

static long pSync[_SHMEM_BCAST_SYNC_SIZE];

/* Broadcast a jint */

static jint bcast_4_from_val;
static jint bcast_4_to_val;

jint be_bcast_4(jint val)
{
  bcast_4_from_val = val;

  barrier();
  shmem_broadcast32((jint *) &bcast_4_to_val,
		    (jint *) &bcast_4_from_val, 1, 0, 0, 0, PROCS, pSync);

  barrier();
  if (MYPROC == 0) {
    bcast_4_to_val = val;
  }

  return bcast_4_to_val;
}

/* Broadcast a jlong */

static jlong bcast_8_from_val;
static jlong bcast_8_to_val;

jlong be_bcast_8(jlong val)
{
  bcast_8_from_val = val;

  barrier();
  shmem_broadcast64((jlong *) &bcast_8_to_val,
		    (jlong *) &bcast_8_from_val, 1, 0, 0, 0, PROCS, pSync);

  barrier();
  if (MYPROC == 0) {
    bcast_8_to_val = val;
  }

  return bcast_8_to_val;
}

/* Broadcast a buffer */

static jlong bcast_16_from_val[2];
static jlong bcast_16_to_val[2];

void be_bcast_buffer(void *buffer, int size)
{
  int i;

  if (((size % (2*sizeof(jlong))) == 0) &&
      (((jUIntPointer) buffer % sizeof(jlong)) == 0)) {
    /* 128-bit aligned size, 64-bit aligned buffer pointer */
    jlong *p = (jlong *) buffer;

    for (i = 0; i < (size / sizeof(jlong)); i += 2) {
      jlong val1 = p[i];
      jlong val2 = p[i + 1];

      bcast_16_from_val[0] = val1;
      bcast_16_from_val[1] = val2;

      barrier();
      shmem_broadcast64((jlong *) &bcast_16_to_val,
			(jlong *) &bcast_16_from_val, 2, 0, 0, 0, PROCS,
			pSync);

      barrier();
      if (MYPROC == 0) {
	bcast_16_to_val[0] = val1;
	bcast_16_to_val[1] = val2;
      }

      p[i] = bcast_16_to_val[0];
      p[i + 1] = bcast_16_to_val[1];
    }
  } else if (((size % sizeof(jlong)) == 0) &&
      (((jUIntPointer) buffer % sizeof(jlong)) == 0)) {
    /* 64-bit aligned size, 64-bit aligned buffer pointer */
    jlong *p = (jlong *) buffer;

    for (i = 0; i < (size / sizeof(jlong)); i++) {
      p[i] = be_bcast_8(p[i]);
    }
  } else if (((size % sizeof(jint)) == 0) &&
      (((jUIntPointer) buffer % sizeof(jint)) == 0)) {
    /* 32-bit aligned size, 32-bit aligned buffer pointer */
    jint *p = (jint *) buffer;

    for (i = 0; i < (size / sizeof(jint)); i++) {
      p[i] = be_bcast_4(p[i]);
    }
  } else {
    /* Buffer and size not aligned. */
    jbyte *p = (jbyte *) buffer;

    for (i = 0; i < (size / sizeof(jbyte)); i++) {
      union {
	jint int32;
	jbyte int8;
      } thunk_send, thunk_recv;
      thunk_send.int8 = p[i];
      thunk_recv.int32 = be_bcast_4(thunk_send.int32);
      p[i] = thunk_recv.int8;
    }
  }
}

/* Initializer */

void bcast_init()
{
  int i;

  for (i = 0; i < _SHMEM_BCAST_SYNC_SIZE; i++) {
    pSync[i] = _SHMEM_SYNC_VALUE;
  }

  barrier();
}
