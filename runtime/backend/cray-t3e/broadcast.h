#ifndef _include_cray_t3e_broadcast_h_
#define _include_cray_t3e_broadcast_h_

#include "backend.h"
#include "bcast-common.h"


#define BROADCAST_SEND( result, type, value )		\
  do {							\
    GP_type( type ) zeroBuffer;				\
    TO_GLOBALB_STATIC( zeroBuffer, 0, (type *) &BUFFER );	\
    ASSIGN_GLOBAL( zeroBuffer, (value), sizeof(type) );	\
  } while (0)


#define BROADCAST_RECEIVE( result, type, sender )


#define BROADCAST_END( result, type, sender )				\
  do {									\
    barrier();								\
    if (sizeof( type ) <= sizeof( jint )) {				\
      union {								\
	jint pretend;							\
	type actual;							\
      } convert;							\
      if (MYPROC == 0) convert.actual = BUFFER;				\
      convert.pretend = be_bcast_4( convert.pretend );			\
      (result) = convert.actual;					\
    } else if (sizeof( type ) <= sizeof( jlong )) {			\
      union {								\
	jlong pretend;							\
	type actual;							\
      } convert;							\
      if (MYPROC == 0) convert.actual = BUFFER;				\
      convert.pretend = be_bcast_8( convert.pretend );			\
      (result) = convert.actual;					\
    } else {								\
      be_bcast_buffer( (void *) &BUFFER, sizeof( type ) );		\
      (result) = BUFFER;						\
    }									\
    CYCLE = !CYCLE;							\
  } while (0)


#endif /* _include_cray_t3e_broadcast_h_ */
