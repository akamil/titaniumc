#ifndef __monitor_h_
#define __monitor_h_

/* Cray T3E Monitor Type */

/*
  Note that the "long" and "short" are purposely used (instead of a j* type)
  and must match the shmem functions called within monitor.c.

  Fields:
  lock_owner  Set to the MYPROC of the owner of the lock. If the lock is
              free, it is set to NO_LOCK_OWNER.

  nesting     Track the nesting level of the lock acquires. The lock is freed
              only when the nesting level returns to zero.
*/

#ifndef HAVE_MONITORS
#error the cray-t3e backend assumes HAVE_MONITORS
#endif

#include <sys/time.h>

#define NO_LOCK_OWNER -1

typedef struct {
  long lock_owner;
  short nesting;
} titanium_monitor_t;

#define tic_monitor_t titanium_monitor_t

/* declared here to help C inliner */
static void monitor_init(titanium_monitor_t *m)
{
  m->lock_owner = NO_LOCK_OWNER;
  m->nesting = 0;
}

void monitor_destroy(titanium_monitor_t *m);
void monitor_enter(titanium_monitor_t *m, int proc);
void monitor_exit(titanium_monitor_t *m, int proc);

#define ti_time_t struct timespec
#define ti_set_wait_timer(tm, millis) do {                          \
    struct timeval now;                                                 \
    gettimeofday(&now, NULL);                                           \
    (tm)->tv_sec = now.tv_sec;                                          \
    (tm)->tv_sec += (now.tv_usec + millis*1000)/1000000;                \
    (tm)->tv_nsec = ((now.tv_usec + millis*1000)%1000000) * 1000;       \
    } while (0)

void monitor_wait(jGPointer monitor, ti_time_t *tm);
void monitor_notify(jGPointer monitor);
void monitor_notify_all(jGPointer monitor);

# define MONITOR_FETCH_CLASS( staticstruct, holder )  globalize( (holder), &STATIC_REF(staticstruct, monitor))
/* holder is always global, but object might be local */
#define MONITOR_FETCH_INSTANCE_LOCAL(  holder, object )  globalize( (holder), &((object)->monitor) )
#define MONITOR_FETCH_INSTANCE_GLOBAL( holder, object )  FIELD_ADDR_GLOBAL( holder, object, monitor )

#define MONITOR_LOCK(   holder )  \
  monitor_enter( GET_ADDR(CAST_GPTR(holder)), GET_PROC(CAST_GPTR(holder)) )
#define MONITOR_UNLOCK( holder )  \
  monitor_exit(  GET_ADDR(CAST_GPTR(holder)), GET_PROC(CAST_GPTR(holder)) )


#endif /* !__monitor_h_ */
