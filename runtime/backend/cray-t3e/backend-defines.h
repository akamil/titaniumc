#ifndef _include_cray_t3e_backend_defines_h_
#define _include_cray_t3e_backend_defines_h_


#define BACKEND_CRAY_T3E
#define MEMORY_DISTRIBUTED
/* #define MEMORY_SHARED */
#define HAVE_MONITORS
#define WIDE_POINTERS
#define TIC_BACKEND_NAME "cray-t3e"
/* #define USE_DISTRIBUTED_GC 1 */

#endif /* !_include_cray_t3e_backend_defines_h_ */
