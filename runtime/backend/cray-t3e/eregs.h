#ifndef _include_eregs_h_
#define _include_eregs_h_

#include "ti_config.h"
#include "backend.h"
#include "primitives.h"

#include <mpp/shmem.h>
#include <mpp/mpphw_t3e.h>

#define ER_READ64_CMD_ADDR(ereg) ((volatile long *) (_GET(ereg)))
#define ER_WRITE_WEAK64_CMD_ADDR(ereg) ((volatile long *) (_PUT(ereg)))
#define ER_READ32_CMD_ADDR(ereg) \
  ((volatile long *) (_GET(ereg) | _MPC_EOM_32BIT))
#define ER_WRITE_WEAK32_CMD_ADDR(ereg) \
  ((volatile long *) (_PUT(ereg) | _MPC_EOM_32BIT))

#define ER_READ_WRITE_CMD(proc, addr) \
  (((_MPC_E_REG_STRIDE1 >> 2) << _MPC_BS_EDATA_MOBE) | \
   ((proc) << _MPC_BS_DFLTCENTPE) | \
   ((long) (addr)))

#define er_local_write_memory_sync() _write_memory_barrier()
#define er_local_memory_sync() _memory_barrier()
#define er_global_write_memory_sync() EREG_PUT_WAIT()
#define er_global_read_memory_sync() EREG_GET_WAIT()
#define er_global_memory_sync() EREG_WAIT()

#define ER_EREG_DATA_ADDR(type, ereg) \
  (( type ) ((jUIntPointer) _MPC_E_REG_BASE + \
	     ((jUIntPointer) (ereg))*sizeof(void *)))

#define ER_SADE_TO_EREG(sade_num) \
  (_MPC_E_REG_SADE + (sade_num))

#define ER_NUM_SADES _MPC_NUME_SADES
#define ER_NEXT_SADE(sade) (((sade) + 1) % ER_NUM_SADES)
extern int curr_sade_num;

#endif /* !_include_eregs_h_ */
