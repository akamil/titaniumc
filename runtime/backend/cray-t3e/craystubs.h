
/* This file defines the type-aware tic comm entry points back into the 
 * untyped operations implemented on the Cray 
 */

#ifndef _runtime_backend_cray_t3e_craystubs_h
#define _runtime_backend_cray_t3e_craystubs_h

/* ------------------------------------------------------------------------- */
/* Pointer manipulations - shadow tic entry points used by mem-wide.h */

#define tolocal(gp) 	GET_ADDR(gp)
#define toproc(gp)	GET_PROC(gp)
#define tobox(gp)	GET_BOX(gp)
#define mutateglobal(gp, proc, addr) mutateglobalwithbox(gp, proc, proc, addr)
#define mutateglobalwithbox(gp, proc, box, addr) do { 	\
	SET_ADDR(gp, addr);				\
	SET_PROC(gp, proc);				\
 	SET_BOX(gp, box);				\
	} while(0)	


/* ------------------------------------------------------------------------- */
/* Broadcast */

#define _BROADCAST(result, type, sender, value) do { 		\
  if (sender == MYPROC) BROADCAST_SEND(result, type, value); 	\
  else BROADCAST_RECEIVE(result, type, sender);		 	\
  } while (0)

#define BROADCAST_jboolean(result, type, sender, value) \
  _BROADCAST(result, type, sender, value)
#define BROADCAST_jbyte(result, type, sender,  value) \
  _BROADCAST(result, type, sender, value)
#define BROADCAST_jchar(result, type, sender,  value) \
  _BROADCAST(result, type, sender, value)
#define BROADCAST_jdouble(result, type, sender,  value) \
  _BROADCAST(result, type, sender, value)
#define BROADCAST_jfloat(result, type, sender,  value) \
  _BROADCAST(result, type, sender, value)
#define BROADCAST_jint(result, type, sender,  value) \
  _BROADCAST(result, type, sender, value)
#define BROADCAST_jlong(result, type, sender,  value) \
  _BROADCAST(result, type, sender, value)
#define BROADCAST_void(result, type, sender,  value) \
  _BROADCAST(result, type, sender, value)
#define BROADCAST_jshort(result, type, sender,  value) \
  _BROADCAST(result, type, sender, value)
#define BROADCAST_lp( result, type, sender, value )  \
  _BROADCAST(result, type, sender, value)
#define BROADCAST_gp( result, type, sender, value )  \
  _BROADCAST(result, type, sender, value)
#define BROADCAST_bulk( result, type, sender, value )  \
  _BROADCAST(result, type, sender, value)

/* ------------------------------------------------------------------------- */
/* Assign */

#define ASSIGN_GLOBAL_jboolean(ptr, val)          ASSIGN_GLOBAL(ptr, val, sizeof(jboolean))     
#define ASSIGN_GLOBAL_jbyte(ptr, val)             ASSIGN_GLOBAL(ptr, val, sizeof(jbyte))  
#define ASSIGN_GLOBAL_jchar(ptr, val)             ASSIGN_GLOBAL(ptr, val, sizeof(jchar))  
#define ASSIGN_GLOBAL_jdouble(ptr, val)           ASSIGN_GLOBAL(ptr, val, sizeof(jdouble))  
#define ASSIGN_GLOBAL_jfloat(ptr, val)            ASSIGN_GLOBAL(ptr, val, sizeof(jfloat))  
#define ASSIGN_GLOBAL_jint(ptr, val)              ASSIGN_GLOBAL(ptr, val, sizeof(jint))  
#define ASSIGN_GLOBAL_jlong(ptr, val)             ASSIGN_GLOBAL(ptr, val, sizeof(jlong))  
#define ASSIGN_GLOBAL_void(ptr, val)              ASSIGN_GLOBAL(ptr, val, sizeof(void))  
#define ASSIGN_GLOBAL_jshort(ptr, val)            ASSIGN_GLOBAL(ptr, val, sizeof(jshort))  
#define ASSIGN_GLOBAL_lp(ptr, val)                ASSIGN_GLOBAL(ptr, val, sizeof(void *))  
#define ASSIGN_GLOBAL_gp(ptr, val)                ASSIGN_GLOBAL(ptr, val, sizeof(jGPointer))  
#define ASSIGN_GLOBAL_bulk(ptr, val)              ASSIGN_GLOBAL(ptr, val, sizeof(val))  

#define WEAK_ASSIGN_GLOBAL_jboolean(ptr, val)     WEAK_ASSIGN_GLOBAL(ptr, val, sizeof(jboolean))   
#define WEAK_ASSIGN_GLOBAL_jbyte(ptr, val)        WEAK_ASSIGN_GLOBAL(ptr, val, sizeof(jbyte))   
#define WEAK_ASSIGN_GLOBAL_jchar(ptr, val)        WEAK_ASSIGN_GLOBAL(ptr, val, sizeof(jchar))   
#define WEAK_ASSIGN_GLOBAL_jdouble(ptr, val)      WEAK_ASSIGN_GLOBAL(ptr, val, sizeof(jdouble))   
#define WEAK_ASSIGN_GLOBAL_jfloat(ptr, val)       WEAK_ASSIGN_GLOBAL(ptr, val, sizeof(jfloat))   
#define WEAK_ASSIGN_GLOBAL_jint(ptr, val)         WEAK_ASSIGN_GLOBAL(ptr, val, sizeof(jint))   
#define WEAK_ASSIGN_GLOBAL_jlong(ptr, val)        WEAK_ASSIGN_GLOBAL(ptr, val, sizeof(jlong))   
#define WEAK_ASSIGN_GLOBAL_void(ptr, val)         WEAK_ASSIGN_GLOBAL(ptr, val, sizeof(void))   
#define WEAK_ASSIGN_GLOBAL_jshort(ptr, val)       WEAK_ASSIGN_GLOBAL(ptr, val, sizeof(jshort))   
#define WEAK_ASSIGN_GLOBAL_lp(ptr, val)           WEAK_ASSIGN_GLOBAL(ptr, val, sizeof(void *))   
#define WEAK_ASSIGN_GLOBAL_gp(ptr, val)           WEAK_ASSIGN_GLOBAL(ptr, val, sizeof(jGPointer))   
#define WEAK_ASSIGN_GLOBAL_bulk(ptr, val)         WEAK_ASSIGN_GLOBAL(ptr, val, sizeof(val))   

#define WEAK_ASSIGN_GLOBAL_anonymous_bulk(ptr, length, val) \
	ASSIGN_GLOBAL_anonymous_bulk(ptr, length, val)

/* ------------------------------------------------------------------------- */
/* Deref */

#define DEREF_GLOBAL_jboolean(val, ptr)           DEREF_GLOBAL(val, ptr, sizeof(jboolean))   
#define DEREF_GLOBAL_jbyte(val, ptr)              DEREF_GLOBAL(val, ptr, sizeof(jbyte))    
#define DEREF_GLOBAL_jchar(val, ptr)              DEREF_GLOBAL(val, ptr, sizeof(jchar))  
#define DEREF_GLOBAL_jdouble(val, ptr)            DEREF_GLOBAL(val, ptr, sizeof(jdouble))  
#define DEREF_GLOBAL_jfloat(val, ptr)             DEREF_GLOBAL(val, ptr, sizeof(jfloat))  
#define DEREF_GLOBAL_jint(val, ptr)               DEREF_GLOBAL(val, ptr, sizeof(jint))  
#define DEREF_GLOBAL_jlong(val, ptr)              DEREF_GLOBAL(val, ptr, sizeof(jlong))  
#define DEREF_GLOBAL_void(val, ptr)               DEREF_GLOBAL(val, ptr, sizeof(void))  
#define DEREF_GLOBAL_jshort(val, ptr)             DEREF_GLOBAL(val, ptr, sizeof(jshort))  
#define DEREF_GLOBAL_lp(val, ptr)                 DEREF_GLOBAL(val, ptr, sizeof(void *))  
#define DEREF_GLOBAL_gp(val, ptr)                 DEREF_GLOBAL(val, ptr, sizeof(jGPointer))  
#define DEREF_GLOBAL_bulk(val, ptr)               DEREF_GLOBAL(val, ptr, sizeof(val))  

/* we don't have a weak deref right now on the Cray */
#define WEAK_DEREF_GLOBAL(val, ptr, size)  DEREF_GLOBAL(val, ptr, size)

#define WEAK_DEREF_GLOBAL_jboolean(val, ptr)      WEAK_DEREF_GLOBAL(val, ptr, sizeof(jboolean)) 
#define WEAK_DEREF_GLOBAL_jbyte(val, ptr)         WEAK_DEREF_GLOBAL(val, ptr, sizeof(jbyte))  
#define WEAK_DEREF_GLOBAL_jchar(val, ptr)         WEAK_DEREF_GLOBAL(val, ptr, sizeof(jchar))  
#define WEAK_DEREF_GLOBAL_jdouble(val, ptr)       WEAK_DEREF_GLOBAL(val, ptr, sizeof(jdouble))  
#define WEAK_DEREF_GLOBAL_jfloat(val, ptr)        WEAK_DEREF_GLOBAL(val, ptr, sizeof(jfloat))  
#define WEAK_DEREF_GLOBAL_jint(val, ptr)          WEAK_DEREF_GLOBAL(val, ptr, sizeof(jint))  
#define WEAK_DEREF_GLOBAL_jlong(val, ptr)         WEAK_DEREF_GLOBAL(val, ptr, sizeof(jlong))  
#define WEAK_DEREF_GLOBAL_void(val, ptr)          WEAK_DEREF_GLOBAL(val, ptr, sizeof(void))  
#define WEAK_DEREF_GLOBAL_jshort(val, ptr)        WEAK_DEREF_GLOBAL(val, ptr, sizeof(jshort))  
#define WEAK_DEREF_GLOBAL_lp(val, ptr)            WEAK_DEREF_GLOBAL(val, ptr, sizeof(void *))  
#define WEAK_DEREF_GLOBAL_gp(val, ptr)            WEAK_DEREF_GLOBAL(val, ptr, sizeof(jGPointer))  
#define WEAK_DEREF_GLOBAL_bulk(val, ptr)          WEAK_DEREF_GLOBAL(val, ptr, sizeof(val))  

#define WEAK_DEREF_GLOBAL_anonymous_bulk(val, length, ptr) \
   	DEREF_GLOBAL_anonymous_bulk(val, length, ptr)


/* ------------------------------------------------------------------------- */
/* SMP locks (no-op on Cray, which is MEMORY_DISTRIBUTED only) */

#define ti_lock_t int
#define ti_cond_t int

#define ti_lock_init(lock) 0     
#define ti_lock_initializer  0
#define ti_lock_decl_initializer 0
#define ti_cond_init(cond, lock)
#define ti_cond_initializer  0
#define ti_cond_decl_initializer 0

#define ti_lock(lock) 0 
#define ti_try_lock(lock) 1          
#define ti_unlock(lock) 0         
#define ti_mon_wait(mon)           abort()
#define ti_mon_timedwait(mon, tm)  abort()
#define ti_mon_signal(mon)         abort()
#define ti_mon_broadcast(mon)      abort()

#define ti_lock_destroy(lock) 0
#define ti_cond_destroy(cond) 0

#define local_barrier()

/* ------------------------------------------------------------------------- */

#define getenvMaster getenv
#define COMM_GetProxyProcNumberForBoxNumber(box) (box)
#define COMM_GetMyBParallelDegree() (1)
#define COMM_GetBoxNumberForProcNumber(proc) (proc)
#define COMM_GetHisBoxProcNumber(proc) (0)
#define COMM_GetParallelDegree() (PROCS)
#define COMM_GetBParallelDegree() (BOXES)
#define TIC_BEGIN_FUNCTION 
#define ti_set_srcpos(file,line) ((void)0)
#define ti_get_srcpos(file,line) ((void)0)
#define ti_srcpos() ((void)0)
#define ti_trace_enterregion(name) ((void)0)
#define ti_trace_leaveregion(name) ((void)0)
#define ti_srcpos_freeze()   0
#define ti_srcpos_unfreeze() 0
#define ti_trace_printf(parenthesized_args) ((void)0)


/* c static data all aligned on T3E */
#define TI_TRANSLATE_CSTATIC_ADDR(addr,box) (addr)
#define TI_TRANSLATE_CSTATIC_ADDR_FROM(addr,box) (addr)

#endif

