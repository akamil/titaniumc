#ifndef _BROADCAST_H_
#define _BROADCAST_H_

#include "lapi_comm.h"

/* All threads must have the same initial value for this! */
extern enum BroadcastCycle {Low, High} BroadcastCycle[MAX_BOX_PROCS];
extern volatile int waitcntr[2];

#define CYCLE FILE_STATIC_STATIC_REF(BroadcastCycle)
#define BUFFER (buffer[ CYCLE ])
#define WAITCNTR (waitcntr[ CYCLE ])


/* The broadcast works like this:

There is one BUFFER per node per textual instance of the broadcast operation in
the program. This is done because we want to allocate BUFFER statically, and
its size must therefore be different for each broadcast of a different sized 
type. 

Thread 0 on each node is assigned to perform all the communication operations.

The sender thread (sender == MYPROC) writes the broadcast data to
BUFFER on its node. Than a global barrier happens--all threads on all
nodes must wait for the data to be written. After that, all nodes
except for the home node of the broadcast perform a communication
operation to fetch the data into their local BUFFERs. Each thread on
each node then copies the data from the local BUFFER into their
destination addresses. 

Why is BUFFER an array? It's to protect it against adjacent
broadcasts; a subsequent broadcast on the same node as the sender
would clobber a simple buffer before all the other threads and nodes
got a chance to read it. Each broadcast instance does have a separate
textual declaration of BUFFER, but it's declared as static (to allow
sharing between threads) and the compiler might merge BUFFERs of
identical type. We fix the problem by making BUFFER a 2-element array
and having each thread maintain CYCLE, a toggle which changes what
BUFFER element gets used by a thread for broadcast send and receive.
The toggle is flipped on a thread after that thread exits a broadcast,
so that adjacent broadcasts end up using different memory
locations. We could just do a global barrier at the end of the
broadcast, but this way is more fun (and faster).
*/

/* The initial brace is closed in BROADCAST_END. */

#define BROADCAST_BEGIN( type, sender )					   \
  do {                                                                     \
  static volatile type buffer[ 2 ];					   \
  do {									   \
    if ((sender) < 0 || (sender) > PROCS) {				   \
      fprintf( stderr, "fatal error: cannot broadcast from process %d\n", (sender) );	\
      abort();							           \
    }								           \
    if (MYTHREAD == 0) {                                                   \
       LAPI_Setcntr(lapi_hndl, (int *)&WAITCNTR, 0);                       \
    }                                                                      \
  } while (0)

#define BROADCAST_SETUP(target, type, sender, value)   \
  do {                                                 \
    if (MYPROC == sender)                              \
      BUFFER = value;                                  \
  } while (0)
  
#define BROADCAST_END( result, type, sender)                \
  do {                                                      \
    int mythread = MYTHREAD;                                \
    barrier();                                              \
    if ((MYPROC != sender) && (NODE_MAP(sender) != MYNODE)) {                \
      if (mythread == 0) {                                  \
        LAPI_Get(lapi_hndl, NODE_MAP(sender), sizeof(type), (void *)&BUFFER, \
                 (void *)&BUFFER, NULL, (int *)&WAITCNTR);  \
        LAPI_WAIT(WAITCNTR);                                \
      }                                                     \
      while (!WAITCNTR) { }                                 \
    }                                                       \
    result = BUFFER;                                        \
    CYCLE = !CYCLE;                                         \
  } while(0);                                               \
  } while(0)
/* ^^^--Not a typo. */

#endif
