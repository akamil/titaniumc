#ifndef _INCLUDE_EXCEPTIONS_H_
#define _INCLUDE_EXCEPTIONS_H_

#include <setjmp.h>
#include "config.h"
#include "instance_header.h"
#include "ti-memory.h"


#define SETJMP setjmp
#define LONGJMP longjmp


struct class_header;
struct instance_header;


extern struct ExceptionState {
    struct ExceptionState *prior;
    int handling;
    GP_instance value;
    jmp_buf handler;
} *STATIC_DEF(exception_state);


#define NO_HANDLER -1

int find_handler( const struct ExceptionState * const,
		  const struct class_header * const [] );

void toss( const GP_instance exception ) __attribute__((noreturn));


#endif
