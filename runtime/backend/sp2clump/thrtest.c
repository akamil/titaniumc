#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <math.h>

static pthread_t *threads;  /* Stores all the thread handles. */
pthread_key_t myProcKey;    /* For thread-local data. */
int THREADS_PER_NODE = 16;   /* Make this flexible later. */
int THREADS_DONE = 0;
pthread_mutex_t global_lock;

void *thr_main(void *id) {
  int iters = 1000000;
  double d = 55.55;
  while (iters--) {
    d = sin(d);
  }
  return NULL;
}

/* What do PROCS, MYPROC, etc. mean? See the note in lapi_comm.h. */
void spawn(void) {
  int count;
  void *thr_retval;
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_mutex_init(&global_lock, NULL);

  printf("Spawning %d threads per node.\n", THREADS_PER_NODE);
  threads = (pthread_t *)malloc(THREADS_PER_NODE * sizeof(pthread_t));
  pthread_key_create(&myProcKey, NULL);
  for (count = 0; count < THREADS_PER_NODE; count++) {
    pthread_create(&threads[count], &attr, thr_main, NULL);
  }
  for (count = 0; count < THREADS_PER_NODE; count++) {
    pthread_join(threads[count], &thr_retval);
  }
}

int main(int argc, char **argv) {
  struct timeval start, end;
  THREADS_PER_NODE = atoi(argv[1]);
  if (!THREADS_PER_NODE) THREADS_PER_NODE = 8;
  gettimeofday(&start, NULL);
  spawn();
  gettimeofday(&end, NULL);
  printf("Elapsed: %f\n", (end.tv_sec - start.tv_sec) + ((end.tv_usec - start.tv_usec) / 1000000.0));
}
