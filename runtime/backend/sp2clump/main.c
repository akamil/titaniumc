#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include "lapi_comm.h"

char *RUNTIME_MODEL = "sp2clump";

extern void monitor_system_init();
extern void dgc_init();

static pthread_t *threads;  /* Stores all the thread handles. */
pthread_key_t myProcKey;    /* For thread-local data. */
int THREADS_PER_NODE = 0;  
int ti_argc; char **ti_argv;  /* Global versions of the args. */
int *node_map;              /* Maps threads to nodes. */
pthread_mutex_t global_lock; /* All-purpose node mutex. */

void *mycalloc(size_t size, size_t elsize) {
  return (void *)calloc(size, elsize);
}

/* Init code for comm/barrier profiling. */
swclock_handle_t profclock; /* For SP/2 high-res timing. */
double *totalTimeBuf, *totalSizeBuf, *countBuf, _startTime;
int timerOn = 0;
static void commview_init() {
  int i,size;
  profclock = swclockInit();
  size = THREADS_PER_NODE * (NODES + 1); 
  totalTimeBuf = (double *)malloc(size * sizeof(double));
  totalSizeBuf = (double *)malloc(size * sizeof(double));
  countBuf = (double *)malloc(size * sizeof(double));
  for (i = 0; i < size; i++) {
    totalTimeBuf[i] = 0.0;
    totalSizeBuf[i] = 0.0;
    countBuf[i] = 0.0;
  } 
}  

extern double *monwaitTime;
extern int bulkIters;
void *thr_main(void *id) {
  pthread_setspecific(myProcKey, id);

  /* Doesn't make a difference.
  if (bindprocessor(BINDTHREAD, thread_self(), MYTHREAD) < 0) {
    printf("Can't give thread its own CPU - continuing execution.\n");
  }
  */

  FILE_STATIC_STATIC_REF(BroadcastCycle) = 0;

  {
    swclock_handle_t totalclock;
    double mystart;
    totalclock = swclockInit();
   
    mystart = swclockReadSec(totalclock);
    ti_main(ti_argc, ti_argv);
  }

#if COMMVIEW
  if (timerOn)
  {
    int node;
    int myThread = MYTHREAD;
    NODELOCK;
    for (node = 0; node <= NODES; node++) {
      int index = myThread * (NODES+1) + node;
      if (node == NODES) {
	printf("%d (barriers): total_time: %f num_barriers: %f\n", MYPROC, totalTimeBuf[index], countBuf[index]);
      }
      else {
	printf("%d->%d total_time: %f total_size: %f num_xfers: %f\n", MYPROC, node, totalTimeBuf[index],totalSizeBuf[index],countBuf[index]);
      }
    }
    NODEUNLOCK;
  }
#endif
  return NULL;
}

void spawn(void) {
  int count;
  void *thr_retval;
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_mutex_init(&global_lock, NULL);

  threads = (pthread_t *)malloc(THREADS_PER_NODE * sizeof(pthread_t));
  if (MYPROC == 0) printf("Spawning %d threads on %d nodes.\n", THREADS_PER_NODE, NODES);
  barrier_init();
  node_map = (int *)malloc(PROCS * sizeof(int));
  pthread_key_create(&myProcKey, NULL);
  for (count = 0; count < PROCS; count++) {
    node_map[count] = count / THREADS_PER_NODE;
  }
  pthread_attr_setstacksize(&attr, 7000000);
  pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
  for (count = 0; count < THREADS_PER_NODE; count++) {
    pthread_create(&threads[count], &attr, thr_main, (void *)(MYNODE * THREADS_PER_NODE + count));
  }
  for (count = 0; count < THREADS_PER_NODE; count++) {
    pthread_join(threads[count], &thr_retval);
  }
}

/* Pull off the backend args from the user command line. */
void processArgs(int *argcP, char ***argvP) {
  int argc = *argcP;
  int arg;
  char **newArgv = (char **)calloc(sizeof(char *),argc);
  arg = 1;
  while (arg < *argcP) {
    if (strcmp((*argvP)[arg], "-threads") == 0) {
      THREADS_PER_NODE = atoi((*argvP)[arg+1]);
      arg += 2;
      argc -= 2;
    }
    else if (strcmp((*argvP)[arg], "-profile") == 0) {
      timerOn = 1;
      arg++;
      argc--;
    }
    else {
      newArgv[arg] = (*argvP)[arg];
      arg++;
    }
  }
  *argcP = argc;
  *argvP = newArgv;
}
  
int main(int argc, char **argv) {
  int rcode;

  dgc_init();
  region_init();

  if (rcode = sp2_init()) {
    fprintf(stderr, "Failed to initialize SP/2 communications layer.\n");
    exit(rcode);
  }

  processArgs(&argc, &argv);

  if (!THREADS_PER_NODE) {
    /* Use the system reported max number of CPUs minus one. */
    THREADS_PER_NODE = sysconf(_SC_NPROCESSORS_ONLN) - 1;
  }
  PROCS = THREADS_PER_NODE * NODES;

  commview_init();
  monitor_system_init(); /* Depends on THREADS_PER_NODE */
  ti_argc = argc; ti_argv = argv;
  spawn();

  sp2_shutdown();
  if (getenv("TI_GCSTATS")) {
    printf("Heap size: %d bytes.\n", ti_gc_usedheapsize());
  }
}
