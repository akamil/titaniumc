#ifndef _DEREF_H_
#define _DEREF_H_

#include "lapi_comm.h"
#include <fp.h>

#define DEREF_GLOBAL(result, ptr) \
  do { \
    if (isNodeLocal(ptr)) \
      result = *(ptr).addr; \
    else { \
      lapi_cntr_t lapi_cntr; \
      NODELOCK; \
      COMMVIEW_START; \
      lapi_cntr = 0; \
      LAPI_Get(lapi_hndl, TO_NODE(ptr), sizeof(result), (void *)((ptr).addr), \
                (void *)(&(result)), NULL, &lapi_cntr); \
      NODEUNLOCK; \
      LAPI_WAIT(lapi_cntr); \
      COMMVIEW_END(TO_NODE(ptr),sizeof(result)); \
    } \
  } while(0)

#endif
