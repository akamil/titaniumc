#ifndef TIC_H
#define TIC_H

/* This file contains the SP implementations of the TIC library functions. 
   It duplicates a lot of code from the TIC header files. */

#include <stdio.h>

/* .............................
 *
 * Control `extern' and `inline'
 *
 * .............................
 */


/* Usage: 
 * The proper (portable) way to declare inline functions in tic headers is:
 *
 *   INLINE_MODIFIER int foo(int bar) 
 *   INLINE ({
 *     function body...
 *     })
 */

/* start with a clean slate */
#ifdef EXTERN
#undef EXTERN
#endif
#ifdef INLINE
#undef INLINE
#endif

/* Defining TIC_DEBUG has the effect of disabling all inlining of functions
 * which helps when debugging. */
#if defined(TIC_DEBUG)
#	define	INLINE(code) ;
#else
#	define	INLINE(code) {code}
#endif

/* Defining TIC_INTERNAL_DEFINE has the effect of producing a
 * real (non-extern) version of all functions and variables. 
 *
 * Exactly one .c file should define this in the distribution to 
 * support the TIC_DEBUG option and non-GNU compilers
 */
#ifdef TIC_INTERNAL_DEFINE
#      define EXTERN
#      undef INLINE
#      define INLINE(code) {code}
#      define FORCE_NO_RUNTIME_INLINE
#      define FORCE_STATIC 0
#else
#      define EXTERN extern
#      define FORCE_STATIC 1
#endif

#include "runtime-options-inl.h" /* INLINE_MODIFIER */


#include "backend-defines.h"
#include <primitives.h>
#include "gp-type.h"


/* ............................
 *
 * Control debugging statements
 *
 * ............................
 */

#ifdef TIC_DEBUG
#define SC_DEBUG(a) a
#else
#define SC_DEBUG(a)
#endif

/* ..........
 *
 * Boxes and Processors
 *
 * ..........
 */

#define MAX_BOX_PROCS 1024
#define MAX_PROCS 1024

#define ___MYPROC       myProcessNumber()
#define ___MYBOXPROC    myBoxProcessNumber()
extern int ___PROCS;
extern int ___BOXES;

/*extern tic_thread_key_t thread_key; */

#define MYPROC     ___MYPROC
#define PROCS      ___PROCS
#define MYBOX      ___MYBOX
#define MYBOXPROC  ___MYBOXPROC
#define MYBOXPROCS ___MYBOXPROCS
#define BOXES      ___BOXES
#define TO_LOCAL(ptr)  ((ptr).addr)
#define TO_PROC(ptr)   ((ptr).proc)
#define TO_BOX(ptr)    (TO_NODE(ptr))
#define SET_PROC(x, val)  {  (x).proc = val; }
#define SET_BOX(x, val)   {  (x).box = val; }
#define SET_ADDR(x, val)  {  (x).addr = (void *)val; }

extern int ___AM_MAX_SHORT; /* size of short packet supported by AM */
extern int ___AM_MAX_MEDIUM; /* size of short packet supported by AM */
extern int ___AM_MAX_LONG; /* size of short packet supported by AM */

extern void *tic_main(int argc, char **argv);

#include <pthread.h>

#define tic_thread_t pthread_t
#define tic_create_thread(func, arg, threadptr, result)  \
  do {    \
    pthread_attr_t attr;    \
    pthread_attr_init(&attr);   \
    if (COMM_GetMyProcsPerProc() <= 1.0) { \
       pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM); \
    } \
    result = pthread_create(threadptr, &attr, func, arg);    \
  } while (0)
#define tic_thread_join(thread, retval)  pthread_join(thread, retval)
#define tic_thread_set_concurrency(conc) 

#define tic_thread_key_t pthread_key_t 
#define tic_thread_key_init(key)     pthread_key_create(key, NULL)
#define tic_thread_get_key_value(key, ptr)  (*ptr) = pthread_getspecific(key)
#define tic_thread_set_key_value(key, ptr)  pthread_setspecific(key, ptr)


/* Locks and Mutexes */

/* used by timedwait */
#define ti_sleepuntil(tm) do {				\
	int millis;						\
	struct timeval now;					\
	gettimeofday(&now, NULL);				\
	millis = ((tm)->tv_sec - now.tv_sec) * 1000 +		\
	         ((tm)->tv_nsec / 1000 - now.tv_usec) / 1000000;	\
	if (millis > 0) m5sleepJmT6Thread4lang4java(millis);	\
	} while (0)

#define ti_lock_t pthread_mutex_t
#define ti_cond_t pthread_cond_t

/* If at runtime we know there's only one processor on this box, we 
   don't need to perform synchronization. */
#define ti_lock_init(lock)      ((MYBOXPROCS > 1) ? pthread_mutex_init(lock, NULL) : 0)


#define ti_lock_initializer  __tic_new_lock()
#define ti_lock_decl_initializer PTHREAD_MUTEX_INITIALIZER

#define ti_cond_init(cond, lock)   ((MYBOXPROCS > 1) ? pthread_cond_init(cond, NULL) : 0)

extern ti_cond_t __tic_new_cond(void);
extern ti_lock_t __tic_new_lock(void);
#define ti_cond_initializer __tic_new_cond()
#define ti_cond_decl_initializer PTHREAD_COND_INITIALIZER

#define ti_lock(lock)           ((MYBOXPROCS > 1) ? pthread_mutex_lock(lock) : 0)
#define ti_try_lock(lock)       ((MYBOXPROCS > 1) ? pthread_mutex_trylock(lock) : 1)
#define ti_unlock(lock)         ((MYBOXPROCS > 1) ? pthread_mutex_unlock(lock) : 0)

#define ti_mon_wait(mon)   do { if (MYBOXPROCS > 1) pthread_cond_wait(&(mon->condvar), &(mon->lock)); } while (0)
#define ti_mon_timedwait(mon, tm)   do {						\
	if (MYBOXPROCS > 1) pthread_cond_timedwait(&(mon->condvar), &(mon->lock), tm);	\
	else  ti_sleepuntil(tm);							\
	} while (0)
#define ti_mon_signal(mon)    do { if (MYBOXPROCS > 1) pthread_cond_signal(&(mon->condvar)); } while (0)
#define ti_mon_broadcast(mon) do { if (MYBOXPROCS > 1) pthread_cond_broadcast(&(mon->condvar)); } while (0)

#define ti_lock_destroy(lock)   ((MYBOXPROCS > 1) ? pthread_mutex_destroy(lock) : 0)
#define ti_cond_destroy(cond)   ((MYBOXPROCS > 1) ? pthread_cond_destroy(cond) : 0)

#define ti_time_t	struct timespec
#define ti_set_wait_timer(tm, millis) do { 				\
    struct timeval now;							\
    gettimeofday(&now, NULL);						\
    (tm)->tv_sec = now.tv_sec;						\
    (tm)->tv_sec += (now.tv_usec + millis*1000)/1000000;		\
    (tm)->tv_nsec = ((now.tv_usec + millis*1000)%1000000) * 1000;	\
    } while (0)


#include <unistd.h>

extern tic_thread_key_t tic_thread_key;


EXTERN ti_lock_t AM_lock;

/* ........................
 *
 * Message Layer
 *
 * ........................
 */

#define LOWWORD(i)    ((julong)(i) & 0xFFFFFFFFUL)
#define HIGHWORD(i)   LOWWORD((julong)(i) >> 32) 

#define SETLOWWORD(ptr, i)  (*(ptr)) = HIGHWORD(*(ptr)) | LOWWORD(i)
#define SETHIGHWORD(ptr, i) (*(ptr)) = (LOWWORD(((julong)(i))) << 32) | LOWWORD(*(ptr))

typedef volatile int Counter;

#define tic_poll()
#define sync_ctr(a) 
#define is_sync_ctr(a)  (1)
extern int politep;

/* Constants specifying when to use segments
 * (these should go somewhere else... -TvE */
#define		STORE_STRUCT_LEN	60
#define		PUT_STRUCT_LEN		32

/* .........................
 *
 * Timer library
 *
 * .........................
 */

EXTERN double get_seconds();


#define isLocal( global )	(TO_PROC(global) == MYPROC)
#define isDirectlyAddressable( global )	(TO_BOX(global) == MYBOX)

#define globalize( global, local )	TO_GLOBAL( global, MYPROC, local )

#define localize( local, global )				\
  do {								\
    if (isLocal(global))					\
      (local) = TO_LOCAL( global );				\
    else							\
      m23throwClassCastExceptionmT11NativeUtils4lang2ti();	\
  } while (0)

#define mutateglobal(globPtr, proc, local) \
  do {\
    SET_ADDR(globPtr, local);\
    SET_PROC(globPtr, proc);\
    SET_BOX(globPtr, COMM_GetBoxNumberForProcNumber(proc)); \
} while(0)

#define mutateglobalwithbox(globPtr, proc, box, local) \
  do {   \
        SET_ADDR(globPtr, local); \
        SET_PROC(globPtr, proc);  \
        SET_BOX(globPtr, box);    \
  } while (0)              

#define MONITOR_LOCK(   holder )  monitor_enter( CAST_GPTR(holder) )
#define MONITOR_UNLOCK( holder )  monitor_exit(  CAST_GPTR(holder) )

/* note: with the current definition of static, class monitors are useless
   (i.e. the synchronized keyword means nothing on a static method) */
# define MONITOR_FETCH_CLASS( staticstruct, holder )  TO_GLOBAL_STATIC( (holder), 0, &STATIC_REF(staticstruct, monitor))

/* holder is always global, but object might be local */
#define MONITOR_FETCH_INSTANCE_LOCAL(  holder, object )  TO_GLOBAL( (holder), MYPROC, &((object)->monitor) )

#define MONITOR_FETCH_INSTANCE_GLOBAL( holder, object )  FIELD_ADDR_GLOBAL( holder, object, monitor )

#define COMM_GetBoxNumberForProcNumber NODE_MAP

#endif
