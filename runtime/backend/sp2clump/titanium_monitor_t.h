#ifndef __titanium_monitor_t_h_
#define __titanium_monitor_t_h_

#include "ti-memory.h"
#include "backend.h"
#include <pthread.h>

struct titanium_monitor_t;

typedef struct {
  struct titanium_monitor_t *monitor;
  int req_proc;
} mon_req_msg;

typedef struct waiting_proc {
  int proc;
  struct waiting_proc *next;
} waiting_proc;

typedef struct titanium_monitor_t {
  int who;
  int nesting;
  mon_req_msg compBuf;
  struct waiting_proc *sleepingProcs;
  struct waiting_proc *waitingProcs;
  pthread_mutex_t lock;
} titanium_monitor_t;

#endif /* !__titanium_monitor_t_h_ */
