#ifndef _include_pthread_threads_h_
#define _include_pthread_threads_h_

#include <pthread.h>


#define TI_MUTEX_INIT PTHREAD_MUTEX_INITIALIZER

typedef pthread_cond_t  ti_cond_t;
typedef pthread_mutex_t ti_mutex_t;
typedef pthread_t       ti_thread_t;

#define ti_cond_broadcast     pthread_cond_broadcast
#define ti_cond_destroy       pthread_cond_destroy
#define ti_cond_init(cond)    pthread_cond_init((cond), 0)
#define ti_cond_signal        pthread_cond_signal
#define ti_cond_timedwait     pthread_cond_timedwait
#define ti_cond_wait          pthread_cond_wait
#define ti_mutex_destroy      pthread_mutex_destroy
#define ti_mutex_init(mutex)  pthread_mutex_init((mutex), 0)
#define ti_mutex_lock         pthread_mutex_lock
#define ti_mutex_unlock       pthread_mutex_unlock
#define ti_thread_equal       pthread_equal
#define ti_thread_self        pthread_self


#endif /* !_include_pthread_threads_h_ */
