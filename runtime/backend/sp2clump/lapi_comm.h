/* Include this file to get all the whizzy-o LAPI stuff. */

#ifndef _LAPI_COMM_H_
#define _LAPI_COMM_H_


#include <lapi.h>
#include <stdio.h>
#include <swclock.h>
#include "deref.h"
#include "assign.h"
#include "backend.h"
#include "ti-memory.h"
#include "sp2-tic.h"
#include "broadcast.h"
#include <pthread.h>

extern char lapi_err_msg_buf[LAPI_MAX_ERR_STRING];
extern int lapi_rcode;
extern int lapi_max_size;
extern lapi_handle_t lapi_hndl;
extern lapi_info_t lapi_info;
extern int PROCS;
extern int MYNODE, NODES, THREADS_PER_NODE;
extern void **lapi_address_buf;
extern pthread_key_t myProcKey;
extern int *node_map;

typedef struct {
        int proc;
        char *addr;
} ___globPtrType;

extern ___globPtrType ___create_global_ptr(int proc, void *addr);
extern int __i_read(___globPtrType ptr);
int lapi_init();
void lapi_shutdown();
extern pthread_mutex_t global_lock;
void barrier();
void local_barrier();

/* PROCS means "all the threads executing in the program".
   MYPROC means "what thread out of all the threads am I?"
   MYNODE means "what node (i.e. machine, box, shared memory space) am I 
     running in?"
   TO_NODE means "what node is this pointer on?"
   isNodeLocal means "is this Titanium pointer on this node?"
   MYTHREAD means "what thread out of the threads on this node am I?"
*/

#define myProcessNumber() ((int)pthread_getspecific(myProcKey))
#define myBoxProcessNumber() (MYTHREAD)
#define NODE_MAP(procnum) (node_map[procnum])
#define TO_NODE(ptr) (NODE_MAP(TO_PROC(ptr)))
#define isNodeLocal(ptr) (TO_NODE(ptr) == MYNODE)
#define NODELOCK  while (pthread_mutex_trylock(&global_lock)) { } 
#define NODEUNLOCK pthread_mutex_unlock(&global_lock);
#define MYTHREAD ((int)MYPROC % THREADS_PER_NODE) /* Optimize? */
#define TO_THREAD(proc) ((int)proc % THREADS_PER_NODE)
#define ___MYBOX MYNODE
#define ___MYBOXPROCS THREADS_PER_NODE

/* COMMVIEW sets up communications profiling for each thread. If it is defined,
   at the end of application execution the program will print out how much
   time each thread spent in communication with each other thread. Thread N
   out of N threads is not a thread at all (because the threads are numbered 
   from 0 onwards); it is the time the thread spent in barriers. 

   The timerOn flag allows you to turn the profiling on and off at will with
   a little gencode hacking.

   Hint: "sort -n" the output for legibility.... */
#define COMMVIEW 1

#if COMMVIEW

extern double *totalTimeBuf, *totalSizeBuf, *countBuf, _startTime;
extern swclock_handle_t profclock;
extern int timerOn;

#define COMMVIEW_START if (timerOn) _startTime = swclockReadSec(profclock)
#define COMMVIEW_END(node,size) if (timerOn) { \
                              double _elapsed = swclockReadSec(profclock) - _startTime; \
                              int _index = MYTHREAD * (NODES+1) + node; \
                              totalTimeBuf[_index] += _elapsed; \
                              countBuf[_index]++; \
			      totalSizeBuf[_index] += size; \
                           }


#else

#define COMMVIEW_START
#define COMMVIEW_END(node,size)

#endif /* COMMVIEW */

#define local_to_global_copy(local, global, count) \
  do { \
    if (isNodeLocal(global)) \
      memcpy(TO_LOCAL(global), (local), sizeof(*(local)) * (count)); \
    else { \
      lapi_cntr_t tgt_cntr, cmpl_cntr; \
      NODELOCK; \
      COMMVIEW_START; \
      LAPI_Setcntr(lapi_hndl, &cmpl_cntr, 0); \
      LAPI_Put(lapi_hndl, TO_NODE(global), sizeof(*(local)) * (count), \
               (void *)((global).addr), (void *)(local), NULL, &tgt_cntr, \
               &cmpl_cntr); \
      NODEUNLOCK; \
      LAPI_WAIT(cmpl_cntr); \
      COMMVIEW_END(TO_NODE(global),sizeof(*(local))); \
    }\
  } while(0)

#define global_to_local_copy(global, local, count) \
  do { \
    if (isNodeLocal( global )) \
      memcpy(   (local), TO_LOCAL( global ), sizeof(*(local)) * (count) ); \
    else { \
      lapi_cntr_t lapi_cntr; \
      NODELOCK; \
      COMMVIEW_START; \
      LAPI_Setcntr(lapi_hndl, &lapi_cntr, 0); \
      LAPI_Get(lapi_hndl, TO_NODE(global), sizeof(*(local)) * (count), \
               (void *)((global).addr), (void *)(local), NULL, \
               &lapi_cntr); \
      NODEUNLOCK; \
      LAPI_WAIT(lapi_cntr); \      
      COMMVIEW_END(TO_NODE(global), sizeof(*(local))); \
    }\
  } while(0)

#ifndef NO_LAPI_ERR_CHECKS

#define LAPI_SAFE(lapicall) \
   do { \
      if ((lapi_rcode = lapicall) != LAPI_SUCCESS) { \
          LAPI_Msg_string(lapi_rcode, lapi_err_msg_buf); \
          fprintf(stderr, "LAPI error: %s\n", lapi_err_msg_buf); \
          exit(lapi_rcode); \
      } \
   } while (0)

#else

#define LAPI_SAFE(lapicall) lapicall

#endif /* NO_LAPI_ERR_CHECKS */
  
#define LAPI_ERROR(rc) \
    do { \
        LAPI_Msg_String(lapi_rcode, lapi_err_msg_buf); \
        fprintf(stderr, "LAPI error: %s\n", lapi_err_msg_buf); \
    } while(0)

/* A wait-on-sync-while-polling loop. You must zero the counter before your async LAPI call. */

#define LAPI_WAIT(cntr) \
  do { \
    while (!cntr) { \
      LAPI_Probe(lapi_hndl); \
    } \
  } while(0) 

void get_array(void *pack_method, void *copy_desc, int copy_desc_size, 
	       int tgt_node, void *buffer);

void fast_get_array(void *pack_method, void *copy_desc, int copy_desc_size, 
		    int tgt_node, void *buffer, int data_size);

#endif /* _LAPI_COMM_H */
