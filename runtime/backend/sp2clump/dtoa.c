#include "primitives.h"
#include <pthread.h>

#define Long jint
#define ULong juint
#define Llong jlong
#define ULLong julong

static pthread_mutex_t mutex[ 2 ] =
   { PTHREAD_MUTEX_INITIALIZER, PTHREAD_MUTEX_INITIALIZER };

  #define MULTIPLE_THREADS
  #define ACQUIRE_DTOA_LOCK( n )  pthread_mutex_lock(   &mutex[ n ] )
  #define FREE_DTOA_LOCK( n )     pthread_mutex_unlock( &mutex[ n ] )

#include "../../fp-utils/dtoa-common.c"
