#ifndef _INCLUDE_SP2_TIC_H_
#define _INCLUDE_SP2_TIC_H__

/* This file contains the type-insensitive gluing needed to get the compiler to
   work with the sp2clump backend. */

#include "instance_header.h"
#include "mem-local.h"
#include "primitives.h"


#ifdef NOMEMCHECKS
#  define _PGASSIGN_GLOBAL(ptr, val, primtype )
  do { ASSIGN_GLOBAL(ptr, val); } while (0)
#  define _WEAK_PGASSIGN_GLOBAL(ptr, val, primtype ) \
  do { WEAK_ASSIGN_GLOBAL(ptr, val); } while (0)
#else /* NOMEMCHECKS */
#  define _PGASSIGN_GLOBAL(ptr, val, primtype ) \
  do { if (!(!isLocal(val) || SHARED_REGIONID(PL2RegionId(TO_LOCAL(val)))) && \
	   (!isLocal(ptr) || SHARED_REGIONID(PL2RegionId(TO_LOCAL(ptr))))) \
	   sharingViolation(__current_loc); \
	else ASSIGN_GLOBAL(ptr, val); } while (0)
#  define _WEAK_PGASSIGN_GLOBAL(ptr, val, primtype ) \
  do { if (!(!isLocal(val) || SHARED_REGIONID(PL2RegionId(TO_LOCAL(val)))) && \
	   (!isLocal(ptr) || SHARED_REGIONID(PL2RegionId(TO_LOCAL(ptr))))) \
	   sharingViolation(__current_loc); \
	else WEAK_ASSIGN_GLOBAL(ptr, val); } while (0)
#endif /* NOMEMCHECKS */

#define PGASSIGN_GLOBAL_jboolean(ptr, val) _PGASSIGN_GLOBAL(ptr, val, jboolean)
#define PGASSIGN_GLOBAL_jbyte(ptr, val) _PGASSIGN_GLOBAL(ptr, val, jbyte)
#define PGASSIGN_GLOBAL_jchar(ptr, val) _PGASSIGN_GLOBAL(ptr, val, jchar)
#define PGASSIGN_GLOBAL_jdouble(ptr, val) _PGASSIGN_GLOBAL(ptr, val, jdouble)
#define PGASSIGN_GLOBAL_jfloat(ptr, val) _PGASSIGN_GLOBAL(ptr, val, jfloat)
#define PGASSIGN_GLOBAL_jint(ptr, val) _PGASSIGN_GLOBAL(ptr, val, jint)
#define PGASSIGN_GLOBAL_jlong(ptr, val) _PGASSIGN_GLOBAL(ptr, val, jlong)
#define PGASSIGN_GLOBAL_void(ptr, val) _PGASSIGN_GLOBAL(ptr, val, void)
#define PGASSIGN_GLOBAL_jshort(ptr, val) _PGASSIGN_GLOBAL(ptr, val, jshort)
#define PGASSIGN_GLOBAL_lp(ptr, val) _PGASSIGN_GLOBAL(ptr, val, lp)
#define PGASSIGN_GLOBAL_gp(ptr, val) _PGASSIGN_GLOBAL(ptr, val, gp)
#define PGASSIGN_GLOBAL_bulk(ptr, val) _PGASSIGN_GLOBAL(ptr, val, bulk)

#define WEAK_PGASSIGN_GLOBAL_jboolean(ptr, val) _WEAK_PGASSIGN_GLOBAL(ptr, val, jboolean)
#define WEAK_PGASSIGN_GLOBAL_jbyte(ptr, val) _WEAK_PGASSIGN_GLOBAL(ptr, val, jbyte)
#define WEAK_PGASSIGN_GLOBAL_jchar(ptr, val) _WEAK_PGASSIGN_GLOBAL(ptr, val, jchar)
#define WEAK_PGASSIGN_GLOBAL_jdouble(ptr, val) _WEAK_PGASSIGN_GLOBAL(ptr, val, jdouble)
#define WEAK_PGASSIGN_GLOBAL_jfloat(ptr, val) _WEAK_PGASSIGN_GLOBAL(ptr, val, jfloat)
#define WEAK_PGASSIGN_GLOBAL_jint(ptr, val) _WEAK_PGASSIGN_GLOBAL(ptr, val, jint)
#define WEAK_PGASSIGN_GLOBAL_jlong(ptr, val) _WEAK_PGASSIGN_GLOBAL(ptr, val, jlong)
#define WEAK_PGASSIGN_GLOBAL_void(ptr, val) _WEAK_PGASSIGN_GLOBAL(ptr, val, void)
#define WEAK_PGASSIGN_GLOBAL_jshort(ptr, val) _WEAK_PGASSIGN_GLOBAL(ptr, val, jshort)
#define WEAK_PGASSIGN_GLOBAL_lp(ptr, val) _WEAK_PGASSIGN_GLOBAL(ptr, val, lp)
#define WEAK_PGASSIGN_GLOBAL_gp(ptr, val) _WEAK_PGASSIGN_GLOBAL(ptr, val, gp)
#define WEAK_PGASSIGN_GLOBAL_bulk(ptr, val) _WEAK_PGASSIGN_GLOBAL(ptr, val, bulk)

#define _SPLASSIGN_GLOBAL(ptr, val, primtype)	ASSIGN_GLOBAL(ptr, val)
#define _SPGASSIGN_GLOBAL(ptr, val, primtype)	ASSIGN_GLOBAL(ptr, val)
#define _WEAK_SPLASSIGN_GLOBAL(ptr, val, primtype) WEAK_ASSIGN_GLOBAL(ptr, val)	
#define _WEAK_SPGASSIGN_GLOBAL(ptr, val, primtype) WEAK_ASSIGN_GLOBAL(ptr, val)	


#define SPGASSIGN_GLOBAL_jboolean(ptr, val) _SPGASSIGN_GLOBAL(ptr, val, jboolean)
#define SPGASSIGN_GLOBAL_jbyte(ptr, val) _SPGASSIGN_GLOBAL(ptr, val, jbyte)
#define SPGASSIGN_GLOBAL_jchar(ptr, val) _SPGASSIGN_GLOBAL(ptr, val, jchar)
#define SPGASSIGN_GLOBAL_jdouble(ptr, val) _SPGASSIGN_GLOBAL(ptr, val, jdouble)
#define SPGASSIGN_GLOBAL_jfloat(ptr, val) _SPGASSIGN_GLOBAL(ptr, val, jfloat)
#define SPGASSIGN_GLOBAL_jint(ptr, val) _SPGASSIGN_GLOBAL(ptr, val, jint)
#define SPGASSIGN_GLOBAL_jlong(ptr, val) _SPGASSIGN_GLOBAL(ptr, val, jlong)
#define SPGASSIGN_GLOBAL_void(ptr, val) _SPGASSIGN_GLOBAL(ptr, val, void)
#define SPGASSIGN_GLOBAL_jshort(ptr, val) _SPGASSIGN_GLOBAL(ptr, val, jshort)
#define SPGASSIGN_GLOBAL_lp(ptr, val) _SPGASSIGN_GLOBAL(ptr, val, lp)
#define SPGASSIGN_GLOBAL_gp(ptr, val) _SPGASSIGN_GLOBAL(ptr, val, gp)
#define SPGASSIGN_GLOBAL_bulk(ptr, val) _SPGASSIGN_GLOBAL(ptr, val, bulk)

#define WEAK_SPGASSIGN_GLOBAL_jboolean(ptr, val) _WEAK_SPGASSIGN_GLOBAL(ptr, val, jboolean)
#define WEAK_SPGASSIGN_GLOBAL_jbyte(ptr, val) _WEAK_SPGASSIGN_GLOBAL(ptr, val, jbyte)
#define WEAK_SPGASSIGN_GLOBAL_jchar(ptr, val) _WEAK_SPGASSIGN_GLOBAL(ptr, val, jchar)
#define WEAK_SPGASSIGN_GLOBAL_jdouble(ptr, val) _WEAK_SPGASSIGN_GLOBAL(ptr, val, jdouble)
#define WEAK_SPGASSIGN_GLOBAL_jfloat(ptr, val) _WEAK_SPGASSIGN_GLOBAL(ptr, val, jfloat)
#define WEAK_SPGASSIGN_GLOBAL_jint(ptr, val) _WEAK_SPGASSIGN_GLOBAL(ptr, val, jint)
#define WEAK_SPGASSIGN_GLOBAL_jlong(ptr, val) _WEAK_SPGASSIGN_GLOBAL(ptr, val, jlong)
#define WEAK_SPGASSIGN_GLOBAL_void(ptr, val) _WEAK_SPGASSIGN_GLOBAL(ptr, val, void)
#define WEAK_SPGASSIGN_GLOBAL_jshort(ptr, val) _WEAK_SPGASSIGN_GLOBAL(ptr, val, jshort)
#define WEAK_SPGASSIGN_GLOBAL_lp(ptr, val) _WEAK_SPGASSIGN_GLOBAL(ptr, val, lp)
#define WEAK_SPGASSIGN_GLOBAL_gp(ptr, val) _WEAK_SPGASSIGN_GLOBAL(ptr, val, gp)
#define WEAK_SPGASSIGN_GLOBAL_bulk(ptr, val) _WEAK_SPGASSIGN_GLOBAL(ptr, val, bulk)

#define SPLASSIGN_GLOBAL_jboolean(ptr, val) _SPLASSIGN_GLOBAL(ptr, val, jboolean)
#define SPLASSIGN_GLOBAL_jbyte(ptr, val) _SPLASSIGN_GLOBAL(ptr, val, jbyte)
#define SPLASSIGN_GLOBAL_jchar(ptr, val) _SPLASSIGN_GLOBAL(ptr, val, jchar)
#define SPLASSIGN_GLOBAL_jdouble(ptr, val) _SPLASSIGN_GLOBAL(ptr, val, jdouble)
#define SPLASSIGN_GLOBAL_jfloat(ptr, val) _SPLASSIGN_GLOBAL(ptr, val, jfloat)
#define SPLASSIGN_GLOBAL_jint(ptr, val) _SPLASSIGN_GLOBAL(ptr, val, jint)
#define SPLASSIGN_GLOBAL_jlong(ptr, val) _SPLASSIGN_GLOBAL(ptr, val, jlong)
#define SPLASSIGN_GLOBAL_void(ptr, val) _SPLASSIGN_GLOBAL(ptr, val, void)
#define SPLASSIGN_GLOBAL_jshort(ptr, val) _SPLASSIGN_GLOBAL(ptr, val, jshort)
#define SPLASSIGN_GLOBAL_lp(ptr, val) _SPLASSIGN_GLOBAL(ptr, val, lp)
#define SPLASSIGN_GLOBAL_gp(ptr, val) _SPLASSIGN_GLOBAL(ptr, val, gp)
#define SPLASSIGN_GLOBAL_bulk(ptr, val) _SPLASSIGN_GLOBAL(ptr, val, bulk)

#define WEAK_SPLASSIGN_GLOBAL_jboolean(ptr, val) _WEAK_SPLASSIGN_GLOBAL(ptr, val, jboolean)
#define WEAK_SPLASSIGN_GLOBAL_jbyte(ptr, val) _WEAK_SPLASSIGN_GLOBAL(ptr, val, jbyte)
#define WEAK_SPLASSIGN_GLOBAL_jchar(ptr, val) _WEAK_SPLASSIGN_GLOBAL(ptr, val, jchar)
#define WEAK_SPLASSIGN_GLOBAL_jdouble(ptr, val) _WEAK_SPLASSIGN_GLOBAL(ptr, val, jdouble)
#define WEAK_SPLASSIGN_GLOBAL_jfloat(ptr, val) _WEAK_SPLASSIGN_GLOBAL(ptr, val, jfloat)
#define WEAK_SPLASSIGN_GLOBAL_jint(ptr, val) _WEAK_SPLASSIGN_GLOBAL(ptr, val, jint)
#define WEAK_SPLASSIGN_GLOBAL_jlong(ptr, val) _WEAK_SPLASSIGN_GLOBAL(ptr, val, jlong)
#define WEAK_SPLASSIGN_GLOBAL_void(ptr, val) _WEAK_SPLASSIGN_GLOBAL(ptr, val, void)
#define WEAK_SPLASSIGN_GLOBAL_jshort(ptr, val) _WEAK_SPLASSIGN_GLOBAL(ptr, val, jshort)
#define WEAK_SPLASSIGN_GLOBAL_lp(ptr, val) _WEAK_SPLASSIGN_GLOBAL(ptr, val, lp)
#define WEAK_SPLASSIGN_GLOBAL_gp(ptr, val) _WEAK_SPLASSIGN_GLOBAL(ptr, val, gp)
#define WEAK_SPLASSIGN_GLOBAL_bulk(ptr, val) _WEAK_SPLASSIGN_GLOBAL(ptr, val, bulk)

#define TO_GLOBAL( global, processor, address )	 mutateglobal(CAST_GPTR((global)), (processor), (address))
#define TO_GLOBALWITHBOX( global, box, processor, address) \
   mutateglobalwithbox(CAST_GPTR(global), proc, box, address)

#define FIELD_ADDR_GLOBAL(val, obj, fld)	TO_GLOBAL(val, TO_PROC(obj), (&obj.addr->fld))
#define IFACE_DISPATCH_GLOBAL(p, mid)		(IFACE_DISPATCH_LOCAL(p.addr, mid))
#define INDEX_GLOBAL(element, base, offset)	TO_GLOBAL(element, TO_PROC(base), (base.addr + offset))
#define SUM_GLOBAL(type, element, base, offset)	TO_GLOBAL(element, TO_PROC(base), (type) ((char *)(TO_LOCAL(base)) + offset))
#define EQUAL_GLOBAL(p, q)			((isNull(p) && isNull(q)) || ((TO_PROC(p) == TO_PROC(q)) && (TO_LOCAL(p) == TO_LOCAL(q))))

#define _CLASS_INFO_GLOBAL( val, type, obj, primtype )  \
  do {							\
    PL ## type field_;					\
    FIELD_ADDR_GLOBAL( field_, obj, class_info );	\
    DEREF_GLOBAL( val, field_ );		\
  } while (0)

#define CLASS_INFO_GLOBAL_jboolean(val, type, obj) _CLASS_INFO_GLOBAL(val, type, obj, jboolean)
#define CLASS_INFO_GLOBAL_jbyte(val, type, obj)    _CLASS_INFO_GLOBAL(val, type, obj, jbyte)
#define CLASS_INFO_GLOBAL_jchar(val, type, obj)    _CLASS_INFO_GLOBAL(val, type, obj, jchar)
#define CLASS_INFO_GLOBAL_jdouble(val, type, obj)  _CLASS_INFO_GLOBAL(val, type, obj, jdouble)
#define CLASS_INFO_GLOBAL_jfloat(val, type, obj)   _CLASS_INFO_GLOBAL(val, type, obj, jfloat)
#define CLASS_INFO_GLOBAL_jint(val, type, obj)     _CLASS_INFO_GLOBAL(val, type, obj, jint)
#define CLASS_INFO_GLOBAL_jlong(val, type, obj)    _CLASS_INFO_GLOBAL(val, type, obj, jlong)
#define CLASS_INFO_GLOBAL_void(val, type, obj)     _CLASS_INFO_GLOBAL(val, type, obj, void)
#define CLASS_INFO_GLOBAL_jshort(val, type, obj)   _CLASS_INFO_GLOBAL(val, type, obj, jshort)
#define CLASS_INFO_GLOBAL_lp(val, type, obj)       _CLASS_INFO_GLOBAL(val, type, obj, lp)
#define CLASS_INFO_GLOBAL_gp(val, type, obj)       _CLASS_INFO_GLOBAL(val, type, obj, gp)
#define CLASS_INFO_GLOBAL_bulk(val, type, obj)     _CLASS_INFO_GLOBAL(val, type, obj, bulk)

#define DEREF_GLOBAL_jboolean            DEREF_GLOBAL 
#define DEREF_GLOBAL_jbyte               DEREF_GLOBAL 
#define DEREF_GLOBAL_jchar               DEREF_GLOBAL
#define DEREF_GLOBAL_jdouble             DEREF_GLOBAL         
#define DEREF_GLOBAL_jfloat              DEREF_GLOBAL          
#define DEREF_GLOBAL_jint                DEREF_GLOBAL            
#define DEREF_GLOBAL_jlong               DEREF_GLOBAL           
#define DEREF_GLOBAL_void                DEREF_GLOBAL            
#define DEREF_GLOBAL_jshort              DEREF_GLOBAL          
#define DEREF_GLOBAL_lp                  DEREF_GLOBAL              
#define DEREF_GLOBAL_gp                  DEREF_GLOBAL
#define DEREF_GLOBAL_bulk                DEREF_GLOBAL
#define DEREF_GLOBAL_anonymous_bulk(val, length, gptr) \
   global_to_local_copy(gptr, val, length)

#define WEAK_DEREF_GLOBAL_jboolean WEAK_DEREF_GLOBAL
#define WEAK_DEREF_GLOBAL_jbyte    WEAK_DEREF_GLOBAL   
#define WEAK_DEREF_GLOBAL_jchar    WEAK_DEREF_GLOBAL   
#define WEAK_DEREF_GLOBAL_jdouble  WEAK_DEREF_GLOBAL 
#define WEAK_DEREF_GLOBAL_jfloat WEAK_DEREF_GLOBAL  
#define WEAK_DEREF_GLOBAL_jint   WEAK_DEREF_GLOBAL    
#define WEAK_DEREF_GLOBAL_jlong  WEAK_DEREF_GLOBAL   
#define WEAK_DEREF_GLOBAL_void   WEAK_DEREF_GLOBAL    
#define WEAK_DEREF_GLOBAL_jshort WEAK_DEREF_GLOBAL  
#define WEAK_DEREF_GLOBAL_lp     WEAK_DEREF_GLOBAL      
#define WEAK_DEREF_GLOBAL_gp     WEAK_DEREF_GLOBAL      
#define WEAK_DEREF_GLOBAL_bulk   WEAK_DEREF_GLOBAL

#define WEAK_DEREF_GLOBAL_anonymous_bulk(val, length, gptr) \
   global_to_local_copy(gptr, val, length)

#define ASSIGN_GLOBAL_jboolean   ASSIGN_GLOBAL
#define ASSIGN_GLOBAL_jbyte   ASSIGN_GLOBAL   
#define ASSIGN_GLOBAL_jchar   ASSIGN_GLOBAL   
#define ASSIGN_GLOBAL_jdouble   ASSIGN_GLOBAL 
#define ASSIGN_GLOBAL_jfloat   ASSIGN_GLOBAL  
#define ASSIGN_GLOBAL_jint   ASSIGN_GLOBAL    
#define ASSIGN_GLOBAL_jlong   ASSIGN_GLOBAL   
#define ASSIGN_GLOBAL_void   ASSIGN_GLOBAL    
#define ASSIGN_GLOBAL_jshort   ASSIGN_GLOBAL  

#define ASSIGN_GLOBAL_lp   ASSIGN_GLOBAL      
#define ASSIGN_GLOBAL_gp   ASSIGN_GLOBAL      
#define ASSIGN_GLOBAL_bulk ASSIGN_GLOBAL

#define ASSIGN_GLOBAL_anonymous_bulk(gptr, length, val)  \
    local_to_global_copy(val, gptr, length)

#define WEAK_ASSIGN_GLOBAL_jboolean   ASSIGN_GLOBAL
#define WEAK_ASSIGN_GLOBAL_jbyte   ASSIGN_GLOBAL   
#define WEAK_ASSIGN_GLOBAL_jchar   ASSIGN_GLOBAL   
#define WEAK_ASSIGN_GLOBAL_jdouble   ASSIGN_GLOBAL 
#define WEAK_ASSIGN_GLOBAL_jfloat   ASSIGN_GLOBAL  
#define WEAK_ASSIGN_GLOBAL_jint   ASSIGN_GLOBAL    
#define WEAK_ASSIGN_GLOBAL_jlong   ASSIGN_GLOBAL   
#define WEAK_ASSIGN_GLOBAL_void   ASSIGN_GLOBAL    
#define WEAK_ASSIGN_GLOBAL_jshort   ASSIGN_GLOBAL  

#define WEAK_ASSIGN_GLOBAL_lp   ASSIGN_GLOBAL      
#define WEAK_ASSIGN_GLOBAL_gp   ASSIGN_GLOBAL      
#define WEAK_ASSIGN_GLOBAL_bulk ASSIGN_GLOBAL  

#define WEAK_ASSIGN_GLOBAL_anonymous_bulk(gptr, length, val)  \
    local_to_global_copy(val, gptr, length)

#define BROADCAST_jboolean BROADCAST_SETUP
#define BROADCAST_jbyte    BROADCAST_SETUP
#define BROADCAST_jchar    BROADCAST_SETUP
#define BROADCAST_jdouble  BROADCAST_SETUP
#define BROADCAST_jfloat   BROADCAST_SETUP
#define BROADCAST_jint     BROADCAST_SETUP
#define BROADCAST_jlong    BROADCAST_SETUP
#define BROADCAST_void     BROADCAST_SETUP
#define BROADCAST_jshort   BROADCAST_SETUP
#define BROADCAST_lp       BROADCAST_SETUP
#define BROADCAST_gp       BROADCAST_SETUP
#define BROADCAST_bulk     BROADCAST_SETUP


#endif /* !_INCLUDE_MEM_WIDE_H_ */
