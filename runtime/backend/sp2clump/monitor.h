#ifndef __monitor_h_
#define __monitor_h_

#include <pthread.h>

typedef struct waiting_proc {
  int proc;
  struct waiting_proc *next;
} waiting_proc;

typedef struct {
  struct titanium_monitor_t *monitor;
  int req_proc;
} mon_req_msg;

typedef struct titanium_monitor_t {
  int who;
  int nesting;
  mon_req_msg compBuf;
  struct waiting_proc *sleepingProcs;
  struct waiting_proc *waitingProcs;
  pthread_mutex_t lock;
} titanium_monitor_t;

typedef struct titanium_monitor_t tic_monitor_t;

#include "ti-memory.h"
#include "backend.h"

#endif /* !__monitor_h_ */
