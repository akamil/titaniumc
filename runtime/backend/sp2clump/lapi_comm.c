/* Module containing generic LAPI interface stuff. */

#include "lapi_comm.h"

lapi_handle_t lapi_hndl;               /* What we use to talk to LAPI. */
char lapi_err_msg_buf[LAPI_MAX_ERR_STRING]; /* Where LAPI error strings go. */
int lapi_rcode;                        /* The return code from a LAPI_SAFEed call. */
int lapi_max_size;                     /* Max LAPI data transfer size. */
int PROCS;                      /* The famous Titanium globals. */
int MYNODE, NODES;
lapi_info_t lapi_info;                 /* Stores LAPI system info. */
void **lapi_address_buf;            /* LAPI does not have addr mirroring. */
int num_threads = 8;

___globPtrType ___create_global_ptr(int proc, void *addr) {
  ___globPtrType ptr;
  ptr.proc = proc;
  ptr.addr = addr;
  return ptr;
}

ti_lock_t __tic_new_lock(void) 
{                                  
  ti_lock_t var = PTHREAD_MUTEX_INITIALIZER;  
  return var;                                    
}

ti_cond_t __tic_new_cond(void) 
{                                  
  ti_cond_t var = PTHREAD_COND_INITIALIZER;  
  return var;                                   
}


int __i_read(___globPtrType ptr) {
  int result;

  DEREF_GLOBAL(result, ptr);
  return result;
}

int sp2_init() {
  int rc;

  /* Init the LAPI system and get some process-local values. */
  LAPI_SAFE(LAPI_Init(&lapi_hndl, &lapi_info)); 
  LAPI_SAFE(LAPI_Qenv(lapi_hndl, MAX_DATA_SZ, &lapi_max_size));
  LAPI_SAFE(LAPI_Qenv(lapi_hndl, TASK_ID, &MYNODE));
  LAPI_SAFE(LAPI_Qenv(lapi_hndl, NUM_TASKS, &NODES));
  if (!NODES) { /* Qenv(NUM_TASKS) acts funny. */
     NODES = 1;
  }  
  /* Turn off error checking for performance. */
  LAPI_SAFE(LAPI_Senv(lapi_hndl, ERROR_CHK, 0)); 
  /* Use polling instead of interrupts. */
  LAPI_SAFE(LAPI_Senv(lapi_hndl, INTERRUPT_SET, 0));

  /* Allocate the address exchange buffer. */
  lapi_address_buf = (void **)malloc(NODES * sizeof(void *));
  return 0;
}

void sp2_shutdown() {
  LAPI_Gfence(lapi_hndl);
  LAPI_Term(lapi_hndl);
  free(lapi_address_buf);
}
  
