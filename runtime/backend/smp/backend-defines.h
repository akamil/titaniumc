#ifndef _include_smp_backend_defines_h_
#define _include_smp_backend_defines_h_


#define BACKEND_SMP
#define TIC_BACKEND_NAME "smp"
#define MEMORY_SHARED
#define PTHREAD
#define HAVE_MONITORS
/* #define WIDE_POINTERS */

#endif /* !_include_smp_backend_defines_h_ */
