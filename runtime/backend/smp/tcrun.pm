package smp::tcrun;
use base qw(common::tcrun);

use strict;


########################################################################


sub apportionments ($@) {
    my $self = shift;
    $self->trace_setenv('TI_THREADS', @_);
}


sub processes ($$) {
    my ($self, $processes) = @_;
    $self->{processes} = $processes;
}


sub run ($@) {
    my $self = shift;
    $self->apportionments($self->{'processes'}) unless ($ENV{TI_THREADS} || $ENV{TI_PFORP});

    if ($self->{cross_compiling}) {
      my $MPIRUN_CMD = main::get_netrun_cmd("MPIRUN_CMD",join(" ",@_),$self->{processes});
      $self->SUPER::run(split / /,$MPIRUN_CMD);
    } else {
      $self->SUPER::run(@_);
    }
}


########################################################################


1;
