#ifndef _include_mill_cluster_smp_am2_backend_defines_h_
#define _include_mill_cluster_smp_am2_backend_defines_h_


#define BACKEND_MILL
#define BACKEND_MILL_CLUSTER_SMP_AM2
#define TIC_BACKEND_NAME "mill-cluster-smp"
#define MEMORY_DISTRIBUTED
#define MEMORY_SHARED
#define COMM_AM2
#define COMM_AMVIA
#define PTHREAD
#define HAVE_MONITORS
#define WIDE_POINTERS
#define USE_DISTRIBUTED_GC 1

#endif /* !_include_mill_cluster_smp_am2_backend_defines_h_ */
