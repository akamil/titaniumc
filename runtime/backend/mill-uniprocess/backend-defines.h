#ifndef _include_mill_uniprocess_backend_defines_h_
#define _include_mill_uniprocess_backend_defines_h_


#define BACKEND_MILL
#define BACKEND_MILL_UNIPROCESS
#define TIC_BACKEND_NAME "mill-uniprocess"
/* #define MEMORY_DISTRIBUTED */
/* #define MEMORY_SHARED */
/* #define COMM_AM2 */
/* #define COMM_AMVIA */
/* #define PTHREAD */
/* #define HAVE_MONITORS */
#define WIDE_POINTERS

#endif /* !_include_mill_uniprocess_backend_defines_h_ */
