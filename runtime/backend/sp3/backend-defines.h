#ifndef _include_amlapi_backend_defines_h_
#define _include_amlapi_backend_defines_h_


#define BACKEND_SP3
#define TIC_BACKEND_NAME "sp3"
#define MEMORY_DISTRIBUTED
#define MEMORY_SHARED
#define COMM_AM2
#define COMM_AMLAPI
#define PTHREAD
#define HAVE_MONITORS
#define WIDE_POINTERS
#define USE_DISTRIBUTED_GC 1

#endif /* !_include_amlapi_defines_h_ */
