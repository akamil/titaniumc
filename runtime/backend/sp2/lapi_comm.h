/* Include this file to get all the whizzy-o LAPI stuff. */

#ifndef _LAPI_COMM_H_
#define _LAPI_COMM_H_


#include <lapi.h>
#include <stdio.h>
#include <swclock.h>
#include "deref.h"
#include "assign.h"
#include "mem-wide.h"
#include "ti-memory.h"

extern char lapi_err_msg_buf[LAPI_MAX_ERR_STRING];
extern int lapi_rcode;
extern int lapi_max_size;
extern lapi_handle_t lapi_hndl;
extern lapi_info_t lapi_info;
extern int MYPROC, PROCS;
extern void **lapi_address_buf;
extern swclock_handle_t profclock;

typedef struct {
        int proc;
        char *addr;
} ___globPtrType;

extern ___globPtrType ___create_global_ptr(int proc, void *addr);
extern int __i_read(___globPtrType ptr);
int lapi_init();
void lapi_shutdown();
extern double commTime, startTime, barrTime;
extern int timerOn;

#ifdef COMMVIEW
/*
#define COMMVIEW_START printf("TIME: %f\t", swclockReadSec(profclock));
#define COMMVIEW_END(from,to) \
   printf("%d %d %f\n", from, to, swclockReadSec(profclock));
*/

#define COMMVIEW_START startTime = swclockReadSec(profclock);
#define COMMVIEW_END(from,to) if (timerOn) commTime += swclockReadSec(profclock) - startTime;

#else

#define COMMVIEW_START
#define COMMVIEW_END(from,to)

#endif /* COMMVIEW */

#define local_to_global_copy(local, global, count) \
  do { \
    if (isLocal(global)) \
      memcpy(TO_LOCAL(global), (local), sizeof(*(local)) * (count)); \
    else { \
      lapi_cntr_t tgt_cntr, cmpl_cntr; \
      LAPI_Setcntr(lapi_hndl, &cmpl_cntr, 0); \
      LAPI_Put(lapi_hndl, (global).proc, sizeof(*(local)) * (count), \
               (void *)((global).addr), (void *)(local), NULL, &tgt_cntr, \
               &cmpl_cntr); \
      LAPI_WAIT(cmpl_cntr); \
    }\
  } while(0)

#define global_to_local_copy( global, local, count ) \
  do { \
    if (isLocal( global )) \
      memcpy(   (local), TO_LOCAL( global ), sizeof(*(local)) * (count) ); \
    else { \
      lapi_cntr_t lapi_cntr; \
      LAPI_Setcntr(lapi_hndl, &lapi_cntr, 0); \
      LAPI_Get(lapi_hndl, (global).proc, sizeof(*(local)) * (count), \
               (void *)((global).addr), (void *)(local), NULL, \
               &lapi_cntr); \
      LAPI_WAIT(lapi_cntr); \      
    }\
  } while(0)

#ifndef NO_LAPI_ERR_CHECKS

#define LAPI_SAFE(lapicall) \
   do { \
      if ((lapi_rcode = lapicall) != LAPI_SUCCESS) { \
          LAPI_Msg_string(lapi_rcode, lapi_err_msg_buf); \
          fprintf(stderr, "LAPI error: %s\n", lapi_err_msg_buf); \
          exit(lapi_rcode); \
      } \
   } while (0)

#else

#define LAPI_SAFE(lapicall) lapicall

#endif /* NO_LAPI_ERR_CHECKS */
  
#define LAPI_ERROR(rc) \
    do { \
        LAPI_Msg_String(lapi_rcode, lapi_err_msg_buf); \
        fprintf(stderr, "LAPI error: %s\n", lapi_err_msg_buf); \
    } while(0)

/* A wait-on-sync-while-polling loop. You must zero the counter before your async LAPI call. */

/*
#define LAPI_WAIT(cntr) \
  do { \
    int _cntrval_ = 0; \
    LAPI_Probe(lapi_hndl); \
    while (!_cntrval_) { \
      LAPI_Getcntr(lapi_hndl, &cntr, &_cntrval_); \
      LAPI_Probe(lapi_hndl); \
    } \
  } while(0) 
*/

#define LAPI_WAIT(cntr) \
  do { \
    while (!cntr) { \
      LAPI_Probe(lapi_hndl); \
    } \
  } while(0) 
  

void get_array(void *pack_method, void *copy_desc, int copy_desc_size, 
	       int tgt_node, void *buffer);

void fast_get_array(void *pack_method, void *copy_desc, int copy_desc_size, 
		    int tgt_node, void *buffer, int data_size);

#endif /* _LAPI_COMM_H */
