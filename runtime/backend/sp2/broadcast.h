#ifndef _BROADCAST_H_
#define _BROADCAST_H_

#include "lapi_comm.h"

extern enum BroadcastCycle { Low, High } STATIC_DEF( BroadcastCycle );

#define CYCLE STATIC_REF( BroadcastCycle )
#define BUFFER (buffer[ CYCLE ])


#define BROADCAST_BEGIN( type, sender )							\
  static volatile type buffer[ 2 ];							\
  do {											\
    if ((sender) < 0 || (sender) > PROCS) {						\
      fprintf( stderr, "fatal error: cannot broadcast from process %d\n", (sender) );	\
      abort();										\
    }											\
    LAPI_Address_init(lapi_hndl, (void *)&BUFFER, lapi_address_buf); \
  } while (0)


#define BROADCAST_SEND( result, type, value )           \
  do {							\
    BUFFER = value; \
  } while (0)

#define BROADCAST_RECEIVE( result, type, sender )

#define BROADCAST_END( result, type, sender) \
  do {                                        \
    lapi_cntr_t lapi_cntr;                    \
    barrier();                                \
    if (MYPROC != sender) {                   \
      lapi_cntr_t lapi_cntr;                  \
      COMMVIEW_START;                         \
      LAPI_Setcntr(lapi_hndl, &lapi_cntr, 0); \
      LAPI_Get(lapi_hndl, sender, sizeof(type), (void *)lapi_address_buf[sender], \
               (void *)(&(result)), NULL, &lapi_cntr); \
      LAPI_WAIT(lapi_cntr);                   \
      COMMVIEW_END(MYPROC, sender);           \
    } \
    else { \
      result = BUFFER; \
    } \
    CYCLE = !CYCLE; \
  } while(0)  \

#endif
