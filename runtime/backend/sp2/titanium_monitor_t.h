#ifndef __titanium_monitor_t_h_
#define __titanium_monitor_t_h_

#include "ti-memory.h"
#include "backend.h"

#ifdef MEMORY_SHARED


typedef struct titanium_monitor_t {
  ti_mutex_t lock;
  ti_cond_t condvar;
  ti_thread_t who;
  int nesting;
  struct titanium_monitor_t *cdr;
#ifdef IRIX_THREADS
  int numwaiting;
  int numsignaled;
#endif
} titanium_monitor_t;


#else  /* !MEMORY_SHARED */
#ifdef MEMORY_DISTRIBUTED


struct waiting_proc;

typedef struct titanium_monitor_t {
  int who;
  int nesting;
  struct waiting_proc *sleepingProcs;
  struct waiting_proc *waitingProcs;
} titanium_monitor_t;


#else  /* !MEMORY_DISTRIBUTED */


/* CM: the type should be void, but some compilers do not seem to like
   that, so make it a void* instead to fgive it a size. */

/* typedef void titanium_monitor_t; */
typedef void *titanium_monitor_t;


#endif /* !MEMORY_DISTRIBUTED */
#endif /* !MEMORY_SHARED */


#endif /* !__titanium_monitor_t_h_ */
