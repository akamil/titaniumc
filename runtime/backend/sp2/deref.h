#ifndef _DEREF_H_
#define _DEREF_H_

#include "lapi_comm.h"

#define DEREF_GLOBAL(result, ptr) \
  do { \
    if (isLocal(ptr)) \
      result = *(ptr).addr; \
    else { \
      lapi_cntr_t lapi_cntr; \
      COMMVIEW_START; \
      lapi_cntr = 0; \
      LAPI_Get(lapi_hndl, (ptr).proc, sizeof(result), (void *)((ptr).addr), \
                (void *)(&(result)), NULL, &lapi_cntr); \
      LAPI_WAIT(lapi_cntr); \
      COMMVIEW_END(MYPROC, (ptr).proc); \
    } \
  } while(0)

#endif
