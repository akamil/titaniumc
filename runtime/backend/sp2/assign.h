#ifndef _ASSIGN_H_
#define _ASSIGN_H_

#include "lapi_comm.h"

/* The gnarly switch construct is to account for ASSIGN_GLOBAL(x+y) 
   or whatever. Note the obfuscated language hackery here, where 
   ASSIGN_GLOBAL(ptr, x+y) gets processed into either *&x + y or &x + y, where
   the latter case compiles but doesn't execute. */

#define ASSIGN_GLOBAL(ptr, val) \
  do { \
    if (isLocal(ptr)) \
      *(ptr).addr = val; \
    else { \
      lapi_cntr_t tgt_cntr, cmpl_cntr; \
      COMMVIEW_START; \
      LAPI_Setcntr(lapi_hndl, &cmpl_cntr, 0); \
      switch(sizeof(val)) { \
      case sizeof(char): { \
        char __buf__ = *(char *)&val; \
        LAPI_Put(lapi_hndl, ptr.proc, sizeof(val), (void *)((ptr).addr), \
	          (void *)&(__buf__), &tgt_cntr, NULL, &cmpl_cntr); \
        break; \
      } \
      case sizeof(short): { \
        short __buf__ = *(short *)&val; \
        LAPI_Put(lapi_hndl, ptr.proc, sizeof(val), (void *)((ptr).addr), \
	          (void *)&(__buf__), &tgt_cntr, NULL, &cmpl_cntr); \
        break; \
      } \
      case sizeof(int): { \
        int __buf__ = *(int *)&val; \
        LAPI_Put(lapi_hndl, ptr.proc, sizeof(val), (void *)((ptr).addr), \
	          (void *)&(__buf__), &tgt_cntr, NULL, &cmpl_cntr); \
        break; \
      } \
      case sizeof(double): { \
	double __buf__ = *(double *)&val; \
        LAPI_Put(lapi_hndl, ptr.proc, sizeof(val), (void *)((ptr).addr), \
	          (void *)&(__buf__), &tgt_cntr, NULL, &cmpl_cntr); \
        break; \
      } \
      default: \
        LAPI_Put(lapi_hndl, ptr.proc, sizeof(val), (void *)((ptr).addr), \
	          (void *)&val, &tgt_cntr, NULL, &cmpl_cntr); \
      } \
      LAPI_WAIT(cmpl_cntr); \
      COMMVIEW_END(MYPROC, ptr.proc); \
    } \
  } while(0)


#define WEAK_ASSIGN_GLOBAL(ptr, val) \
  do { \
    if (isLocal(ptr)) \
      *(ptr).addr = val; \
    else { \
      lapi_cntr_t org_cntr; \
      COMMVIEW_START; \
      LAPI_Setcntr(lapi_hndl, &org_cntr, 0); \
      switch(sizeof(val)) { \
      case sizeof(char): { \
        char __buf__ = *(char *)&val; \
        LAPI_Put(lapi_hndl, ptr.proc, sizeof(val), (void *)((ptr).addr), \
	          (void *)&(__buf__), NULL, &org_cntr, NULL); \
        break; \
      } \
      case sizeof(short): { \
        short __buf__ = *(short *)&val; \
        LAPI_Put(lapi_hndl, ptr.proc, sizeof(val), (void *)((ptr).addr), \
	          (void *)&(__buf__), NULL, &org_cntr, NULL); \
        break; \
      }\
      case sizeof(int): { \
        int __buf__ = *(int *)&val; \
        LAPI_Put(lapi_hndl, ptr.proc, sizeof(val), (void *)((ptr).addr), \
	          (void *)&(__buf__), NULL, &org_cntr, NULL); \
        break; \
      } \
      case sizeof(double): { \
	double __buf__ = *(double *)&val; \
        LAPI_Put(lapi_hndl, ptr.proc, sizeof(val), (void *)((ptr).addr), \
	          (void *)&(__buf__), NULL, &org_cntr, NULL); \
        break; \
      } \
      default: \
        LAPI_Put(lapi_hndl, ptr.proc, sizeof(val), (void *)((ptr).addr), \
	          (void *)&val, NULL, &org_cntr, NULL); \
      } \
      LAPI_WAIT(org_cntr); \
      COMMVIEW_END(MYPROC, ptr.proc); \
    } \
  } while(0)
            
   

#endif /* _ASSIGN_H_ */
