#include "lapi_comm.h"

extern swclock_handle_t profclock;

void barrier() {
  double startTime, endTime;
  COMMVIEW_START;
  LAPI_Gfence(lapi_hndl);
  COMMVIEW_END(MYPROC, -1);
}
