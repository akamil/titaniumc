#include <stdio.h>
#include <swclock.h>

char *RUNTIME_MODEL = "sp2";

extern void monitor_system_init();
extern void dgc_init();

swclock_handle_t profclock;

int main(int argc, char **argv) {
  int rcode;

  dgc_init();
  region_init();

  if (rcode = sp2_init()) {
    fprintf(stderr, "Failed to initialize SP/2 communications layer.\n");
    exit(rcode);
  }
  monitor_system_init();
  profclock = swclockInit();
  ti_main(argc, argv);

  sp2_shutdown();
  if (getenv("TI_GCSTATS")) {
    printf("Heap size: %d bytes.\n", ti_gc_usedheapsize());
  }
}
