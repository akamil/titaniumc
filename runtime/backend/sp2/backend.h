#ifndef _include_sp2_backend_h_
#define _include_sp2_backend_h_

#define BACKEND_LAPI
#define MEMORY_DISTRIBUTED
#define USE_DISTRIBUTED_GC
#define HAVE_MONITORS

#include "mem-wide.h"
#include "static-global.h"

#include "assign.h"
#include "deref.h"
#include "lapi_comm.h"

#endif /* _include_sp2_backend_h_ */
