#ifndef _include_now_cluster_uniprocess_backend_defines_h_
#define _include_now_cluster_uniprocess_backend_defines_h_


#define BACKEND_NOW
#define BACKEND_NOW_CLUSTER_UNIPROCESS
#define TIC_BACKEND_NAME "now-cluster-uniprocess"
#define MEMORY_DISTRIBUTED
/* #define MEMORY_SHARED */
#define COMM_AM2
#define COMM_AMNOW
/* #define PTHREAD */
#define HAVE_MONITORS
#define WIDE_POINTERS
#define USE_DISTRIBUTED_GC 1

#endif /* !_include_now_cluster_uniprocess_backend_defines_h_ */
