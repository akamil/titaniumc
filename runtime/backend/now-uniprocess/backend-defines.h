#ifndef _include_now_uniprocess_backend_defines_h_
#define _include_now_uniprocess_backend_defines_h_


#define BACKEND_NOW
#define BACKEND_NOW_UNIPROCESS
#define TIC_BACKEND_NAME "now-uniprocess"
/* #define MEMORY_DISTRIBUTED */
/* #define MEMORY_SHARED */
/* #define COMM_AM2 */
/* #define COMM_VIA */
/* #define PTHREAD */
/* #define HAVE_MONITORS */
#define WIDE_POINTERS

#endif /* !_include_now_uniprocess_backend_defines_h_ */
