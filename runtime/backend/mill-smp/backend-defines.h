#ifndef _include_now_am2_backend_defines_h_
#define _include_now_am2_backend_defines_h_


#define BACKEND_MILL
#define BACKEND_MILL_SMP
#define TIC_BACKEND_NAME "mill-smp"
#define MEMORY_SHARED
#define PTHREAD
#define HAVE_MONITORS
#define WIDE_POINTERS

#endif /* !_include_now_am2_backend_defines_h_ */
