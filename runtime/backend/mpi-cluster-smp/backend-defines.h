#ifndef _include_mpi_cluster_smp_backend_defines_h_
#define _include_mpi_cluster_smp_backend_defines_h_


#define BACKEND_MPI
#define BACKEND_MPI_CLUSTER_SMP
#define TIC_BACKEND_NAME "mpi-cluster-smp"
#define MEMORY_DISTRIBUTED
#define MEMORY_SHARED
#define COMM_AM2
#define COMM_AMMPI
#define PTHREAD
#define HAVE_MONITORS
#define WIDE_POINTERS
#define USE_DISTRIBUTED_GC 1

#endif /* !_include_mpi_cluster_smp_backend_defines_h_ */
