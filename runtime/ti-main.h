#ifndef _INCLUDE_TI_MAIN_H_
#define _INCLUDE_TI_MAIN_H_

/* this file is compiled into every application at app build time */

#if BOUNDS_CHECKING
int bounds_checking = 1;
#else
int bounds_checking = 0;
#endif

#endif
