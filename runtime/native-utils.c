#include "ti-gc.h"
#include "mem-local.h"
#include "java_string.h"
#include "native-utils.h"

void tossMonitorStateException_str(char * const ascii)
{
    const LP_JString message = java_string_build_8( (ascii?ascii:"") );
    m33throwIllegalMonitorStateExceptionLT6String4lang4javamT11NativeUtils4lang2ti( message );
    abort();			/* should never be reached */
}


void tossIllegalArgumentException_str(char * const ascii)
{

    const LP_JString message = java_string_build_8( (ascii?ascii:"") );
    m29throwIllegalArgumentExceptionLT6String4lang4javamT11NativeUtils4lang2ti( message );
    
    abort();                    /* should never be reached */
}

void tossClassNotFoundException_str(char * const ascii)
{

    const LP_JString message = java_string_build_8( (ascii?ascii:"") );
    m27throwClassNotFoundExceptionLT6String4lang4javamT11NativeUtils4lang2ti( message );
    
    abort();                    /* should never be reached */
}

void tossNullPointerException_str(char * const ascii) 
{
    const LP_JString message = java_string_build_8( (ascii?ascii:"") );
    m25throwNullPointerExceptionLT6String4lang4javamT11NativeUtils4lang2ti( message );
    
    abort();                    /* should never be reached */
}

void tossArrayStoreException_str(char * const ascii) 
{
    const LP_JString message = java_string_build_8( (ascii?ascii:"") );
    m24throwArrayStoreExceptionLT6String4lang4javamT11NativeUtils4lang2ti( message );
    
    abort();                    /* should never be reached */
}

/* build a code-location string (used by __current_loc) */
char *build_loc_str(const char *funcname, const char *filename, int linenum, const char *extrainfo) {
  int sz;
  char *loc;
  char *extra_str = "";
  int free_extra = 0;
  if (extrainfo) {
    extra_str = ti_malloc(strlen(extrainfo)+10);
    if (!extra_str) extra_str = "(*out of mem*)";
    else { free_extra = 1; sprintf(extra_str, " (%s)", extrainfo); }
  } 
  if (!funcname) funcname = "";
  if (!filename) filename = "*unknown file*";
  sz = strlen(funcname) + strlen(filename) + strlen(extra_str) + 20;
  loc = ti_malloc(sz);
  if (!loc) return "*out of mem*";
  if (*funcname)
    sprintf(loc,"%s%s at %s:%i%s", 
           funcname, 
           (funcname[strlen(funcname)-1] == ')'?"":"()"),
           filename, linenum, extra_str);
  else
    sprintf(loc,"%s:%i%s", filename, linenum, extra_str);
  if (free_extra) ti_free(extra_str);
  return loc;
}


