#ifndef BOUNDS_CHECKING
#define BOUNDS_CHECKING 1
#endif

#if !BOUNDS_CHECKING
/* turn off region safety checks when bounds checking is off */
#define NOMEMCHECKS 1
#endif

#if !defined(NDEBUG) && defined(__OPTIMIZE__)
  #if !defined(TI_ALLOW_OPTIMIZED_DEBUG) && !defined(GASNET_ALLOW_OPTIMIZED_DEBUG)
    #error Tried to compile Titanium code with backend compiler optimization enabled but without NDEBUG to disable runtime assertions (which is important for good performance). Reconfigure/rebuild Titanium without --enable-debug
  #endif
#endif

/* things related to variable-size stack arrays */
#include "runtime-options-var.h"
