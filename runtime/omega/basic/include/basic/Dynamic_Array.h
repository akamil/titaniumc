/* $Id: */
#ifndef Already_Included_Dynamic_Array
#define Already_Included_Dynamic_Array

template <class T> class Dynamic_Array2;
template <class T> class Dynamic_Array3;
template <class T> class Dynamic_Array4;

template <class T, int d> class Dynamic_Array
    {
    public:
	Dynamic_Array(Dynamic_Array<T,d> &D);
        ~Dynamic_Array();

    protected:
	Dynamic_Array();
	bool partial;
	int *bounds;
	T *elements;

	void do_constr();
	void do_destruct();
    };


#if (__GNUC__ == 3 && __GNUC_MINOR__ == 4) || __GNUC__ > 3
 // DOB: work around an annoying bug in g++ 3.4.0 and 4.0+
 #define USING(N) \
    using Dynamic_Array<T, N>::bounds;   \
    using Dynamic_Array<T, N>::elements; \
    using Dynamic_Array<T, N>::partial; \
    using Dynamic_Array<T, N>::do_constr; \
    using Dynamic_Array<T, N>::do_destruct; 
#else 
 #define USING(N)
#endif

template <class T> class Dynamic_Array1 : public Dynamic_Array<T,1>
    {
    USING(1)
    public:
	Dynamic_Array1(char *s0 = 0);
	Dynamic_Array1(int d0);
	void resize(int d0);
        T& operator[](int d);

	friend class Dynamic_Array2<T>;

    private:
	void do_construct(int d0);
    };


template <class T> class Dynamic_Array2 : public Dynamic_Array<T,2>
    {
    USING(2)
    public:
	Dynamic_Array2(char *s0 = 0, char *s1 = 0);
	Dynamic_Array2(int d0, int d1);
	void resize(int d0, int d1);
  	Dynamic_Array1<T> operator[](int d);

	friend class Dynamic_Array3<T>;

    private:
	void do_construct(int d0, int d1);
    };


template <class T> class Dynamic_Array3 : public Dynamic_Array<T,3>
    {
    USING(3)
    public:
	Dynamic_Array3(char *s0 = 0, char *s1 = 0, char *s2 = 0);
	Dynamic_Array3(int d0, int d1, int d2);
	void resize(int d0, int d1, int d2);
  	Dynamic_Array2<T> operator[](int d);

	friend class Dynamic_Array4<T>;

    private:
	void do_construct(int d0, int d1, int d2);
    };

template <class T> class Dynamic_Array4 : public Dynamic_Array<T,4>
    {
    USING(4)
    public:
	Dynamic_Array4(char *s0 = 0, char *s1 = 0, char *s2 = 0, char *s3 = 0);
	Dynamic_Array4(int d0, int d1, int d2, int d3);
	void resize(int d0, int d1, int d2, int d3);
  	Dynamic_Array3<T> operator[](int d);

    private:
	void do_construct(int d0, int d1, int d2, int d3);
    };

#if ! defined DONT_INCLUDE_TEMPLATE_CODE
#include <basic/Dynamic_Array.cc>
#endif

#define instantiate_Dynamic_Array1(T)	template class Dynamic_Array1<T>; \
					template class Dynamic_Array<T,1>;

#define instantiate_Dynamic_Array2(T)	template class Dynamic_Array2<T>;  \
					template class Dynamic_Array<T,2>; \
					instantiate_Dynamic_Array1(T);

#define instantiate_Dynamic_Array3(T)	template class Dynamic_Array3<T>;  \
					template class Dynamic_Array<T,3>; \
					instantiate_Dynamic_Array2(T);

#define instantiate_Dynamic_Array4(T)	template class Dynamic_Array4<T>;  \
					template class Dynamic_Array<T,4>; \
					instantiate_Dynamic_Array3(T);
#endif
