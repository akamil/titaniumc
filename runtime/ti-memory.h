#ifndef _include_ti_memory_h_
#define _include_ti_memory_h_

#include "backend.h"
#include "mem-local.h"
#include "regions.h"

#define THROW(ptr)                toss(*((GP_instance *)&(ptr)))
#define THROW_UNIVERSAL(ptr)      toss_universal(*((GP_instance *)&(ptr)))

#define isNull(ptr)     (TO_LOCAL(ptr) == 0)
#define nonNull(ptr)    (TO_LOCAL(ptr) != 0)

#define OBJECT_MALLOC(result, typename, isShared) do {   \
    (result) = (typename *) ti_malloc(sizeof(typename)); \
    (result)->class_info = &fi5cdesc##typename;          \
    tally_memory(sizeof(typename), isShared);            \
  } while (0)

#define OBJECT_MALLOC_WITH_FINALIZER(finalizer, result, typename, isShared) do {   \
    (result) = (typename *) ti_malloc(sizeof(typename)); \
    (result)->class_info = &fi5cdesc##typename;          \
    tally_memory(sizeof(typename), isShared);            \
    ti_register_finalizer(result, finalizer);              \
  } while (0)

#define OBJECT_RALLOC_WITHEXTRA(result, region, typename, isShared, extrabytes) do {   \
    (result) = (typename *) ralloc((region), sizeof(typename) + (extrabytes));         \
    (result)->class_info = &fi5cdesc##typename;                                        \
    tally_memory(sizeof(typename), isShared);                                          \
  } while (0)

#define OBJECT_RALLOC(result, region, typename, isShared) \
        OBJECT_RALLOC_WITHEXTRA(result, region, typename, isShared, 0)

#define OBJECT_RALLOC_WITH_FINALIZER(finalizer, result, region, typename, isShared) do {   \
    if (region) {                                                                      \
      (result) = (typename *) ralloc((region), sizeof(typename) + 2 * sizeof(void *)); \
      *(void **) (result) = region->finalize_list;                                     \
      region->finalize_list = (void **) (result);                                       \
      (result) = (typename *) (((void **)(result)) + 1);                                \
      *(void **) (result) = (void *) finalizer;                                        \
      (result) = (typename *) (((void **)(result)) + 1);                                \
      (result)->class_info = &fi5cdesc##typename;                                      \
      tally_memory(sizeof(typename), isShared);                                        \
    } else                                                                             \
      OBJECT_MALLOC_WITH_FINALIZER(finalizer, result, typename, isShared);             \
  } while (0)

#endif /* !_include_ti_memory_h_ */
