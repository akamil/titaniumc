#ifndef _INCLUDE_CLASS_HEADER_
#define _INCLUDE_CLASS_HEADER_

/* must appear before <sys/types.h> on Solaris */
#include "file-64.h"

#include <sys/types.h>
#include "ti_config.h"
#include "common_header.h"
struct class_header;
typedef struct class_header class_header;
#include "layout!LT6String4lang4java.h"
#include "layout!LT6Object4lang4java.h"
#include "layout!LT10Checkpoint4lang2ti.h"
#include "layout!PT6Object4lang4java.h"
#include "layout!PT10Checkpoint4lang2ti.h"
#include "T5Class4lang4java.h"


struct instance_header;
struct interface_header;

struct class_header
{
    T5Class4lang4java class_object; /* MUST come first */
    LT6String4lang4java class_name;

    enum TypeCategory category;
    enum TypeFlags flags;
    jchar index;
    const struct interface_header * const *implements;
    void (* const *intf_info)();
    const struct class_header *super;
    size_t size;
    void (*nullifyLocalFields)( struct instance_header * );
    void (*newInstance)( LT6Object4lang4java );
    void (*checkpoint)( PT6Object4lang4java, PT10Checkpoint4lang2ti, jboolean );
    void (*restore)( LT6Object4lang4java, LT10Checkpoint4lang2ti );
};

#endif
