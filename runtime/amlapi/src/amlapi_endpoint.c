#include "amlapi_endpoint.h"
#include "amlapi_const.h"
#include "amlapi_token.h"
#include "amlapi_node.h"
#include <assert.h>
#include <stdlib.h>

/*
 * NOTE: see amlapi_task_queue.c for implementation of the
 * task queue allocation and deallocation routines
 */

int AM_AllocateBundle (int type, eb_t * endb) {
    eb_t endpointBundle = (eb_t) calloc (sizeof (struct amlapi_eb),1);
    int  qlen = AMLAPI_ENDPOINT_BUNDLE_TASK_QUEUE_INC;

    if (endpointBundle == NULL) {
	goto err_1;
    }
    if (type != AM_SEQ) goto err_1;
    endpointBundle->semaphore = 0;
    endpointBundle->type = type;
    endpointBundle->size = 0;
    endpointBundle->endpoints = (ep_t *) malloc (sizeof (ep_t) * AMLAPI_ENDPOINT_BUNDLE_INC);
    if (endpointBundle->endpoints == NULL) {
	goto err_2;
    }
    endpointBundle->cursize = 0;

    if (!alloc_initial_tq_elements(endpointBundle,qlen)) {
	goto err_3;
    }
    /* this will be the "firewall" element, it serves to
     * allow the producer thread and consumer thread to
     * operate on the queue without locking.
     */
    endpointBundle->tq_head = new_tq_element(endpointBundle);
    endpointBundle->tq_tail = endpointBundle->tq_head;

    *endb = endpointBundle;
    return AM_OK;

    /* error return cases ... cleanup */
    err_3:
    free(endpointBundle->endpoints);
    err_2:
    free(endpointBundle);
    err_1:
    *endb = NULL;
    return AM_ERR_RESOURCE;
}

int AMLAPI_InsertEndpointToBundle (ep_t endpoint, eb_t bundle) {

    if (endpoint == NULL || bundle == NULL) {
	return AM_ERR_BAD_ARG;
    }
    /* stick the endpoint into the bundle */
    if (bundle->size == bundle->cursize) {
	/* filled current buffer. must grow */
	int i;
	ep_t * oldBuffer;
	int newsize = bundle->cursize + AMLAPI_ENDPOINT_BUNDLE_INC;
	ep_t * newBuffer = (ep_t *) malloc (sizeof (ep_t) * newsize);
	if (newBuffer == NULL) {
	    return AM_ERR_RESOURCE;
	}
	for (i = 0; i < bundle->cursize; i++) {
	    newBuffer [i] = bundle->endpoints [i];
	}
	oldBuffer = bundle->endpoints;
	bundle->endpoints = newBuffer;
	bundle->cursize = newsize;
	free (oldBuffer);
    }
    bundle->endpoints [bundle->size] = endpoint;
    bundle->size++;
    endpoint->endpoint_bundle = bundle;
    return AM_OK;
}

int AM_AllocateEndpoint (eb_t bundle, ep_t *endp, en_t * endpoint_name) {
    ep_t endpoint;
    int retval;

    if (bundle == NULL || endp == NULL || endpoint_name == NULL) {
	return AM_ERR_BAD_ARG;
    }

    /* initialize the name and insert into per-node list. */
    endpoint = (ep_t) calloc (sizeof (struct amlapi_ep),1);
    (*endpoint_name).endpoint_id = nextEndpointId;
    /* insert into the node's list of all endpoints.
       Identical to InsertEndpointToBundle */
    if (endpointsInNode_size == nextEndpointId) {
	/* Filled the array. must grow */
	int i;
	int newsize = endpointsInNode_size + AMLAPI_ENDPOINTSINNODE_INC;
	ep_t * newBuffer = (ep_t *) malloc (sizeof (ep_t) * newsize);
	if (newBuffer == NULL) {
	    return AM_ERR_RESOURCE;
	}
	for (i = 0; i < nextEndpointId; i++) {
	    newBuffer [i] = endpointsInNode [i];
	}
	endpointsInNode = newBuffer;
	endpointsInNode_size = newsize;
    }
    endpointsInNode [nextEndpointId] = endpoint;
    nextEndpointId++;
    (*endpoint_name).node = AMLAPI_MYNODE;

    /* initialize the endpoint */
    endpoint->name = *endpoint_name;
    endpoint->segAddr = NULL;
    endpoint->tag = AM_NONE;
    endpoint->segLength = 0;

    retval = AMLAPI_InsertEndpointToBundle (endpoint, bundle);
    if (retval != AM_OK) { return retval; }

    *endp = endpoint;
    return AM_OK;
}

int AM_Map (ep_t ea, int index, en_t endpoint, tag_t tag) {
    if (ea == NULL) {
	return AM_ERR_BAD_ARG;
    }
    if (index < 0 || index >= AMLAPI_MAX_NUMTRANSLATIONS) {
	return AM_ERR_RESOURCE;
    }
    if (ea->translation [index].inuse) {
	return AM_ERR_IN_USE;
    } /* already used */

    ea->translation [index].inuse = TRUE;
    ea->translation [index].remoteEndpoint = endpoint;
    ea->translation [index].tag = tag;
    return AM_OK;
}

int AM_MapAny (ep_t ea, int *index, en_t endpoint, tag_t tag) {
    int ind;

    if (ea == NULL || index == NULL) {
	return AM_ERR_BAD_ARG;
    }
    for (ind = 0; ind < AMLAPI_MAX_NUMTRANSLATIONS; ind++) {
	if (!(ea->translation [ind].inuse)) {
	    ea->translation [ind].inuse = TRUE;
	    ea->translation [ind].remoteEndpoint = endpoint;
	    ea->translation [ind].tag = tag;
	    *index = ind;
	    return AM_OK;
	}
    }
    return AM_ERR_RESOURCE;
}

int AM_Unmap (ep_t ea, int index) {

    if (ea == NULL) {
	return AM_ERR_BAD_ARG;
    }
    if (index < 0 || index >= AMLAPI_MAX_NUMTRANSLATIONS) {
	return AM_ERR_RESOURCE;
    }
    if (!ea->translation [index].inuse) {
	return AM_ERR_IN_USE;
    } /* entry not really used */
    ea->translation [index].inuse = FALSE;
    return AM_OK;
}

int AMLAPI_RemoveEndpointFromBundle (ep_t ea) {
    eb_t bundle;
    int i;
    char foundit = FALSE;
    bundle = ea->endpoint_bundle;
    if (ea == NULL || bundle == NULL) {
	return AM_ERR_BAD_ARG;
    }
    for (i = 0; i < bundle->size; i++) {
	if (bundle->endpoints [i] == ea) { 
	    foundit = TRUE;
	    /* swap the last one into hole */
	    bundle->endpoints [i] = bundle->endpoints [bundle->size - 1]; 
	}
    }
    if (foundit) {
	bundle->size--;
    } else {
	return AM_ERR_RESOURCE;
    }
    return AM_OK;
}

int AM_FreeEndpoint (ep_t ea) {
    int retval;
    int id;

    if (ea == NULL) { return AM_ERR_BAD_ARG; }
    retval = AMLAPI_RemoveEndpointFromBundle (ea);
    if (retval != AM_OK) { return retval; }

    id = ea->name.endpoint_id;
    if (id >= nextEndpointId) { return AM_ERR_BAD_ARG; }
    endpointsInNode [id] = NULL;

    free (ea);
    return AM_OK;
}

int AM_FreeBundle (eb_t bundle) {
    int i;

    if (bundle == NULL) { return AM_ERR_BAD_ARG; }
    for (i = 0; i < bundle->size; i++) {
	int retval = AM_FreeEndpoint (bundle->endpoints [i]);
	if (retval != AM_OK) { return retval; }
    }

    free (bundle->endpoints);
    free_bundle_task_queue(bundle);
    free (bundle);
    return AM_OK;
}

int AM_MoveEndpoint (ep_t ea, eb_t from_bundle, eb_t to_bundle) {
    int retval;

    if (from_bundle == NULL || to_bundle == NULL || ea == NULL) {
	return AM_ERR_BAD_ARG;
    }
    if (ea->endpoint_bundle != from_bundle) {
	return AM_ERR_BAD_ARG;
    }
    retval = AMLAPI_RemoveEndpointFromBundle (ea);
    if (retval != AM_OK) {
	return retval;
    }
    retval = AMLAPI_InsertEndpointToBundle (ea, to_bundle);
    if (retval != AM_OK) {
	return retval;
    }
    return AM_OK;
}

int AM_SetExpectedResources (ep_t ea, int n_endpoints, int n_outstanding_requests) {
    /* since all ams are "instantaneous" using LAPI, there is no need to set resources */
    return AM_OK;
}

int AM_SetTag (ep_t ea, tag_t tag) {
    if (ea == NULL) { return AM_ERR_BAD_ARG; }
    ea->tag = tag;
    return AM_OK;
}

int AM_GetTag (ep_t ea, tag_t* tag) {
    if (ea == NULL || tag == NULL) { return AM_ERR_BAD_ARG; }
    *tag = ea->tag;
    return AM_OK;
}

int AM_GetTranslationName (ep_t ea, int i , en_t * gan) {
    if (ea == NULL || gan == NULL) {
	return AM_ERR_BAD_ARG;
    }
    if (!ea->translation [i].inuse) {
	return AM_ERR_IN_USE;
    }
    if (i < 0 || i >= AMLAPI_MAX_NUMTRANSLATIONS) {
	return AM_ERR_RESOURCE;
    }
    *gan = ea->translation [i].remoteEndpoint;
    return AM_OK;
}

int AM_GetTranslationTag (ep_t ea, int i, tag_t * tag) {
    if (ea == NULL || tag == NULL) {
	return AM_ERR_BAD_ARG;
    }
    if (!ea->translation [i].inuse) {
	return AM_ERR_IN_USE;
    }
    if (i < 0 || i >= AMLAPI_MAX_NUMTRANSLATIONS) {
	return AM_ERR_RESOURCE;
    }
    *tag = ea->translation [i].tag;
    return AM_OK;
}

int AM_GetTranslationInuse (ep_t ea, int i) {
    if (ea == NULL) { return AM_ERR_BAD_ARG; }
    if (i < 0 || i >= AMLAPI_MAX_NUMTRANSLATIONS) {
	return AM_ERR_RESOURCE;
    }
    return ea->translation [i].inuse ? AM_OK : AM_ERR_IN_USE;
}

int AM_MaxNumTranslations (int * ntrans) {
    *ntrans = AMLAPI_MAX_NUMTRANSLATIONS;
    return AM_OK;
}

int AM_GetNumTranslations (ep_t ea, int * ntrans) {
    if (ea == NULL) {
	return AM_ERR_BAD_ARG;
    }
    *ntrans = AMLAPI_MAX_NUMTRANSLATIONS;
    return AM_OK;
}

int AM_SetNumTranslations (ep_t ea, int ntrans) {
    /* does nothing. */
    if (ea == NULL) {
	return AM_ERR_BAD_ARG;
    }
    return AM_OK;
}

int AM_GetSourceEndpoint (void * token, en_t *gan) {
    struct amlapi_token * t;
    if (token == NULL || gan == NULL) {
	return AM_ERR_BAD_ARG;
    }
    t = (struct amlapi_token *) token;
    gan->node = t->sourceNode;
    gan->endpoint_id = t->sourceEndpointId;
    return AM_OK;
}

int AM_GetDestEndpoint (void * token, en_t *gan) {
    struct amlapi_token * t;
    if (token == NULL || gan == NULL) {
	return AM_ERR_BAD_ARG;
    }
    t = (struct amlapi_token *) token;
    gan->node = t->destNode;
    gan->endpoint_id = t->destEndpointId;
    return AM_OK;
}

int AM_GetMsgTag (void * token, tag_t * tag) {
    struct amlapi_token * t;
    if (token == NULL || tag == NULL) {
	return AM_ERR_BAD_ARG;
    }
    t = (struct amlapi_token *) token;
    *tag = t->tag;
    return AM_OK;
}

int AM_SetHandler (ep_t ea, handler_t handler, void (*function)()) {
    if (ea == NULL) {
	return AM_ERR_BAD_ARG;
    }
    if (handler >= AMLAPI_MAX_NUMHANDLERS) {
	return AM_ERR_RESOURCE;
    }
    ea->handler [handler] = function;
    ea->handler_inuse [handler] = TRUE;
    return AM_OK;
}

int AM_SetHandlerAny (ep_t ea, handler_t * handler, void (*function)()) {
    handler_t i;
    if (ea == NULL) {
	return AM_ERR_BAD_ARG;
    }
    for (i = 0; i < AMLAPI_MAX_NUMHANDLERS; i++) {
	if (!(ea->handler_inuse [i])) {
	    ea->handler [i] = function;
	    ea->handler_inuse [i] = TRUE;
	    *handler = i;
	    return AM_OK;
	}
    }
    return AM_ERR_RESOURCE;
}

int AM_GetNumHandlers (ep_t ea, int * n_handlers) {
    if (ea == NULL || n_handlers == NULL) {
	return AM_ERR_BAD_ARG;
    }
    *n_handlers = AMLAPI_MAX_NUMHANDLERS;
    return AM_OK;
}

int AM_SetNumberHandlers (ep_t ea, int n_handlers) {
    if (ea == NULL) { return AM_ERR_BAD_ARG; }
    if (n_handlers >= AMLAPI_MAX_NUMHANDLERS) {
	return AM_ERR_RESOURCE;
    }
    return AM_OK;
}

int AM_MaxNumHandlers () { return AMLAPI_MAX_NUMHANDLERS; }

int AM_SetSeg (ep_t ea, void * addr, uintptr_t nbytes) {
    if (ea == NULL) {
	return AM_ERR_BAD_ARG;
    }
    if (nbytes > AMLAPI_MAX_SEGLENGTH) {
	return AM_ERR_RESOURCE;
    }
    ea->segAddr = addr;
    ea->segLength = nbytes;
    return AM_OK;
}

int AM_GetSeg (ep_t ea, void ** addr, uintptr_t * nbytes) {
    if (ea == NULL || addr == NULL || nbytes == NULL) {
	return AM_ERR_BAD_ARG;
    }
    *addr = ea->segAddr;
    *nbytes = ea->segLength;
    return AM_OK;
}

int AM_MaxSegLength (uintptr_t * nbytes) {
    if (nbytes == NULL) {
	return AM_ERR_BAD_ARG;
    }
    *nbytes = AMLAPI_MAX_SEGLENGTH;
    return AM_OK;
}

int AM_GetEventMask (eb_t eb) {
    if (eb == NULL) {
	return AM_ERR_BAD_ARG;
    }
    return eb->event_mask;
}

int AM_SetEventMask (eb_t eb, int mask) {
    if (eb == NULL) {
	return AM_ERR_BAD_ARG;
    }
    eb->event_mask = (uint8_t) mask;
    return AM_OK;
}


int AM_WaitSema (eb_t eb) {
  char done = FALSE;

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif

    if (eb == NULL) { return AM_ERR_BAD_ARG; }
    eb->semaphore = 0;

    while (!done) {

	/* first check if the task queue has elements */
	if (eb->tq_head != eb->tq_tail) {
	    done = TRUE;
	    break;
	}

	/* if not, did a header_handler execute a user
	 * handler directly?  we must check the semaphore
	 */

	if (eb->semaphore > 0) {
	    done = TRUE;
	}

	/* if neither, kick the network and try again */
	if (!done) {
	    LAPI_Probe(lapi_hndl);
	}
    }
    return AM_OK;
}

/*
int AM_WaitSema (eb_t eb) {

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif

    return AMLAPI_WaitForTask (eb);
}

int AMLAPI_WaitForTask (eb_t eb) {
    char done = FALSE;
    if (eb == NULL) {
	return AM_ERR_BAD_ARG;
    }
    while (!done) {
	if (eb->tq_head != eb->tq_tail) { done = TRUE; }
	if (!done) {
	    LAPI_Probe(lapi_hndl);
	}
    }
    return AM_OK;
}
*/
