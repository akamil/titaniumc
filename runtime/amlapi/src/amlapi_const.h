#ifndef _AMLAPI_CONST_H_
#define _AMLAPI_CONST_H_

#include <inttypes.h>

#define AM_OK 10000
#define AM_ERR_BAD_ARG  10001
#define AM_ERR_RESOURCE 10002
#define AM_ERR_NOT_SENT 10003
#define AM_ERR_IN_USE   10004

#define AM_SEQ 100
#define AM_PAR 101

#define AM_NONE 0x0000

#define AM_NOEVENTS 0x0000
#define AM_NOTEMPTY 0x0010

#define AMLAPI_NUM_TASK_TO_EXEC_ON_POLL 1

#define AMLAPI_MAX_SHORT 8
#define AMLAPI_MAX_LONG 262144

/* The number of free buffers in the pool for
 * medium sized messages
 */
#define AMLAPI_MEDIUM_BUFFER_POOL 16

/* run LAPI in interrupt or polling mode */
typedef enum {
    AMLAPI_POLLING = 0,
    AMLAPI_INTERRUPT = 1
} AMLAPI_MODE;

/* define the following if you wish to shortcut reply
 * handlers that run in the context of the user
 * thread.
 * Comment it out (of undef) if you want all user
 * handlers to pass through the task queue.
 */
#define AMLAPI_TQ_SHORTCUT 1

typedef uint64_t tag_t;
typedef uint8_t handler_t;
typedef void (*amlapi_handler_t) ();

#endif /* _AMLAPI_CONST_H_ */
