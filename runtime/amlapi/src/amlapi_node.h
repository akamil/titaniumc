#ifndef _AMLAPI_NODE_H_
#define _AMLAPI_NODE_H_

#include <pthread.h>
#include <lapi.h>
#include "amlapi_const.h"
#include "amlapi_endpoint.h"

/* Per node variables */

/* per-node number to identify all the endpoints.
 * Monotonically increasing.
 * Equal to the number of endpoints ever created in this node.
 */
extern int nextEndpointId;

/* a dynamically grown array that keeps track of all endpoints
 * on this node
 */
extern ep_t * endpointsInNode; 
extern int endpointsInNode_size; 

/* when run out of endpoint space, how many more to allocate */
#define AMLAPI_ENDPOINTSINNODE_INC 10

extern int AMLAPI_MYNODE;
extern int AMLAPI_NUMNODES;
extern lapi_handle_t lapi_hndl;
extern lapi_info_t lapi_info;

/* array to store the virtual addresses of the header handlers
 * on the remote nodes for request and reply messages
 */
extern void **req_hndlr;

/* array to store the virtual addresses of the header handlers
 * on the remote nodes for AM_GetXfer operations
 */
extern void **get_hndlr;

/* cache the current LAPI mode (interrupt or polling) */
extern AMLAPI_MODE lapi_cur_mode;

/* cache the current user thread id */
extern pthread_t amlapi_current_id;

/* non-exported funciton prototypes */
ep_t AMLAPI_ExtractEndpointFromToken (struct amlapi_token * token);
amlapi_handler_t AMLAPI_ExtractHandlerFromEndpoint (ep_t destEndpoint,
						    handler_t index);
int AMLAPI_SendAM (ep_t source_endpoint, en_t dest_endpoint, handler_t handler,
		   void * source_addr, int nbytes, int type, int M, int * argv,
		   uintptr_t dest_offset, char isAsync, int req_type);
int AMLAPI_Set_Mode(AMLAPI_MODE mode);
int AMLAPI_RemoveEndpointFromBundle (ep_t ea);
int AMLAPI_WaitForTask (eb_t eb);
qe_t* AMLAPI_ScheduleTaskOnBundle (eb_t bundle, amlapi_handler_t handler,
				   struct amlapi_token* token, void* user_info, int nbytes,
				   int numArgs, int* argv, int type,
				   int is_ready);
char AMLAPI_ExecuteTaskFromBundle (eb_t bundle);
char AMLAPI_checkHeaderType (int type);

#endif /* _AMLAPI_NODE_H_ */
