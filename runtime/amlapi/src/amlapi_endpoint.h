#ifndef _AMLAPI_ENDPOINT_H_
#define _AMLAPI_ENDPOINT_H_

#include <pthread.h>

/* End point object */

#define AMLAPI_MAX_NUMTRANSLATIONS 256
#define AMLAPI_MAX_NUMHANDLERS 256
#define AMLAPI_MAX_SEGLENGTH ((uintptr_t)-1)  /* unlimited */
#define AMLAPI_ENDPOINT_BUNDLE_INC 10
#define AMLAPI_ENDPOINT_BUNDLE_TASK_QUEUE_INC 32

#include "amlapi_token.h"
#include "amlapi_const.h"

typedef struct amlapi_ep_name {
    int node;
    int endpoint_id;
} en_t;

struct amlapi_translation_entry_t {
    char inuse;
    en_t remoteEndpoint; /* end point living on the remote node */
    tag_t tag;
    uint8_t id;
};

typedef struct amlapi_handler_q_entry {
    amlapi_handler_t handler;
    struct amlapi_token token;
    void* user_info;
    int nbytes;
    int numArgs;
    int argv[AMLAPI_MAX_SHORT];
    int type;
    char ready;
    struct amlapi_handler_q_entry * volatile next;
} qe_t;

typedef struct amlapi_eb {
    struct amlapi_ep ** endpoints; /* dynamically-grown array of endpoints in bundle (read ep_t *)*/ 
    int size;                      /* how many endpoints are there in the bundle */
    int cursize;                   /* how much space is allocated */
    uint8_t event_mask;
    int type;
    volatile int semaphore;

    /* task queue head, tail and free list */
    qe_t * volatile tq_head; 
    qe_t * volatile tq_tail; 
    qe_t * volatile free_head; 
    qe_t * volatile free_tail; 
} *eb_t;

typedef struct amlapi_ep {
    eb_t endpoint_bundle; /* endpoint bundle this endpoint belongs to */
    tag_t tag;
    void *segAddr;        /* Start address of EP VM segment */
    uintptr_t segLength;   /* Length of EP VM segment    */  
    en_t name;

    struct amlapi_translation_entry_t translation[AMLAPI_MAX_NUMTRANSLATIONS]; /* translation table */
    amlapi_handler_t  handler[AMLAPI_MAX_NUMHANDLERS]; /* handler table */
    char handler_inuse [AMLAPI_MAX_NUMHANDLERS]; /* checking if handler table is in use */
} *ep_t;

/* ---------------------------------------------------------------------
 * function prototypes for the task queue element allocation and dealloc
 * Implemented in amlapi_task_queue.c
 * ---------------------------------------------------------------------
 */
qe_t* new_tq_element(eb_t eb);
void free_tq_element(eb_t eb, qe_t *x);
void memory_sync(void);
int alloc_initial_tq_elements(eb_t eb, int num);
void free_bundle_task_queue(eb_t eb);
void* med_buf_alloc(void);
void med_buf_free(void *x);

#endif /*_AMLAPI_ENDPOINT_H_*/
