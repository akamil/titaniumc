#ifndef _AMLAPI_UHEADER_H_
#define _AMLAPI_UHEADER_H_

#define AMLAPI_UHEADER_PLAIN 10
#define AMLAPI_UHEADER_I 11
#define AMLAPI_UHEADER_XFER 12
#define AMLAPI_UHEADER_XFERASYNC 13
#define AMLAPI_UHEADER_MASK 0xf

#include "amlapi_const.h"
#include "amlapi_token.h"


extern int amlapi_max_uhdr_size;
extern int amlapi_max_stuffed;

/* chunk of data passed to the dispatcher to figure out which handler
 * to call and what arguments to pass in */
typedef struct amlapi_uheader {
    struct amlapi_token token;
    handler_t handler;
    int type;        /* type of the lapi message */
    int nbytes;      /* size of accompanying data */
    int M;           /* Number of arguments to be passed to the handler */
    uintptr_t destOffset;  /* offset into remote VM Segment to place data */
    int argv [8];
    char stuffed[1];  /* where stuffed data will live */
} uhdr_t;

typedef struct amlapi_getXfer_uheader {
    struct amlapi_token token;
    handler_t handler;
    int type;
    int nbytes;
    int numArgs;
    int argv [8];
    uintptr_t dest_offset;
    uintptr_t source_offset;
} getxferhdr;

#endif /* _AMLAPI_UHEADER_H_ */
