#ifndef _AMLAPI_TOKEN_H_
#define _AMLAPI_TOKEN_H_

#include "amlapi_const.h"

/* chunk of data passed to and from AM */
typedef struct amlapi_token {
  int sourceNode;
  int sourceEndpointId;
  int destNode;
  int destEndpointId;
  tag_t tag;
} amlapi_token;

#endif /* _AMLAPI_TOKEN_H_ */
