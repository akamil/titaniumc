#include "amlapi_node.h"
#include "amlapi_const.h"
#include "amlapi_endpoint.h"
#include <pthread.h>
#include <stdlib.h>

#define A1 13
#define A2 15
#define A3 136
#define A4 1234

void highestHandler (void* token, void* buf, int nbytes) {
  en_t gan;
  int * a = (int *) buf;
  int i;
  AM_GetSourceEndpoint (token, &gan);
  printf ("%i received a request from %i/%i\n", AMLAPI_MYNODE, gan.node, gan.endpoint_id);
  for (i = 0; i < 200; i++) { if (a [i] != 200 + i) { printf ("Error: %i is %i \n", i, a [i]); } }
  printf ("data: %i and %i \n", a [0], a [1]);
  AM_Reply4 (token, 2, A1, A2, A3, A4);
}

void highestHandler_plain (void * token, int a1, int a2, int a3, int a4) {
  en_t gan;
  AM_GetSourceEndpoint (token, &gan);
  printf ("%i received a request from %i/%i\n", AMLAPI_MYNODE, gan.node, gan.endpoint_id);
  if (a1 != A1 || a2 != A2 || a3 != A3 || a4 != A4) { printf ("args are wrong!\n"); }
}

void getXFerCheckHandler (void * token, void* buf, int nbytes) {
  int * a = (int *) buf;
  int i;
  printf ("%i's remote get is complete. Checking...\n", AMLAPI_MYNODE);
  for (i = 0; i < 90; i++) { if (a [i] != 300 + i + 10) { printf ("Error: %i is %i \n", i, a [i]); } }
  printf ("check finished.\n");
}

void * thrmain (void* id) {
  int ret;
  ep_t endpoint;
  eb_t bundle;
  eb_t bundle2;
  en_t endpoint_name;
  int * a;

  /*printf ("Allocating bundle.\n");*/
  ret = AM_AllocateBundle (AM_SEQ, &bundle);
  ret = AM_AllocateBundle (AM_SEQ, &bundle2);
  /*printf ("%i returned from allocate bundle.\n", ret);*/
  ret = AM_AllocateEndpoint (bundle, &endpoint, &endpoint_name);
  /*printf ("%i returned from allocate endpoint.\n", ret);*/
  ret = AM_SetHandler (endpoint, 3, highestHandler);
  ret = AM_SetHandler (endpoint, 2, highestHandler_plain);
  ret = AM_SetHandler (endpoint, 4, getXFerCheckHandler);
  ret = AM_MoveEndpoint (endpoint, bundle, bundle2);
  /*printf ("Moved endpoint: bundle 1 has %i and bundle2 has %i\n", bundle->size, bundle2->size);*/
  ret = AM_MoveEndpoint (endpoint, bundle2, bundle);
  /*printf ("Moved endpoint: bundle 1 has %i and bundle2 has %i\n", bundle->size, bundle2->size);*/
  a = (int*) malloc (sizeof (int) * 100);
  AM_SetSeg (endpoint, a, sizeof (int) * 100);
  LAPI_Gfence (*lapi_hndl);
  printf ("Synching... \n");
  if (AMLAPI_MYNODE == 0) { 
    en_t name;
    int * hey = (int*) malloc (sizeof (int) * 200);
    int i;
    for (i = 0; i < 200; i++) { hey [i] = 200 + i; }
    for (i = 0; i < 100; i++) { a [i] = 300 + i; }
    name.node = 1;
    name.endpoint_id = 0;
    AM_Map (endpoint, 0, name, 0);
    AM_RequestI0 (endpoint, 0, 3, hey, sizeof(int) * 200); 
    printf ("%i thinks that am has been sent.\n", AMLAPI_MYNODE);
  }
  if (AMLAPI_MYNODE == 1) {
    en_t name;
    name.node = 0;
    name.endpoint_id = 0;
    AM_Map (endpoint, 0, name, 0);
    AM_Request4 (endpoint, 0, 2, A1, A2, A3, A4); 
    printf ("%i thinks that am has been sent.\n", AMLAPI_MYNODE);
  }
  LAPI_Gfence (*lapi_hndl);  
  AM_Poll (bundle);
  if (AMLAPI_MYNODE == 0) { /* zero has a reply coming up... better wait for it. */
    printf ("zero waiting for reply...\n");
    AMLAPI_WaitForTask (bundle);
    printf ("zero got the reply ...\n");
    AM_Poll (bundle);
  }
  LAPI_Gfence (*lapi_hndl);
  /* Test the Get Remote XFer **/
  if (AMLAPI_MYNODE == 1) { /* do a remote get on zero, skewed by 10 ints */
    AM_GetXfer0 (endpoint, 0, sizeof (int) * 10, 4, 0, sizeof (int) * 90);
    AMLAPI_WaitForTask (bundle);
    AM_Poll (bundle);
    AM_Request4 (endpoint, 0, 2, A1, A2, A3, A4); /* let zero know it can proceed now. */
  }
  if (AMLAPI_MYNODE == 0) {
    AMLAPI_WaitForTask (bundle);
    AM_Poll (bundle);
  }
  LAPI_Gfence (*lapi_hndl);
  ret = AM_FreeBundle (bundle);
  /*printf ("%i: %i returned from free bundle.\n", AMLAPI_MYNODE, ret);*/
  ret = AM_FreeBundle (bundle2);
  /*printf ("%i returned from free bundle.\n", ret);*/
  return;
}  

void spawn () {
  int i;
  pthread_t ** threads = (pthread_t **) malloc (sizeof (pthread_t *) * 1);
  AMLAPI_LAPI_Init ();
  AM_Init ();
  for (i = 0; i < 1; i++) {
    pthread_attr_t attr;
    pthread_t * thread;
    pthread_attr_init(&attr);
    thread = (pthread_t *)malloc(sizeof(pthread_t));
    pthread_create (thread, &attr, thrmain, NULL);
    threads [i] = thread;
  }
  for (i = 0; i < 1; i++) {
    void* thr_retval;
    pthread_t * thread;
    thread = threads [i];
    pthread_join (*thread, &thr_retval);
  }
}

main (int argc, char ** argv) {
  printf ("initialising next point id lock.\n");
  nextEndpointId = 0;
  spawn ();
  printf ("last end point = %i\n", nextEndpointId);
}
