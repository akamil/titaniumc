#include <lapi.h>
#include "amlapi_node.h"

/* user-visible routines on AMLAPI not present in AM spec */
int AMLAPI_MyProc () { return AMLAPI_MYNODE; }
int AMLAPI_NumProcs () { return AMLAPI_NUMNODES; }
lapi_handle_t * AMLAPI_GetLAPIHandle () { return &lapi_hndl; }
lapi_info_t * AMLAPI_GetLAPIInfo () { return &lapi_info; }
void AMLAPI_Barrier () { LAPI_Gfence (lapi_hndl); }
