#ifdef THREAD_STATS
#include <pthread.h>
#endif
#include "amlapi.h"
#include "amlapi_endpoint.h"
#include "amlapi_node.h"
#include "amlapi_uheader.h"
#include "amlapi_const.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

void* header_handler (lapi_handle_t * hndl, void * uhdr, uint * uhdr_len,
                      unsigned long * msg_len, compl_hndlr_t ** comp_h,
                      void ** user_info);


/* define variables declaired in amlapi_node.h */
int nextEndpointId;
ep_t * endpointsInNode;
int endpointsInNode_size;
int AMLAPI_MYNODE;
int AMLAPI_NUMNODES;
lapi_handle_t lapi_hndl;
lapi_info_t lapi_info;
void **req_hndlr;
void **get_hndlr;
AMLAPI_MODE lapi_cur_mode;
pthread_t amlapi_current_id;

extern const char AMLAPI_IdentString_Version[];
const char AMLAPI_IdentString_Version[] = "$AMLAPILibraryVersion: " AMLAPI_LIBRARY_VERSION_STR " $";

#define REQUEST  0
#define REPLY    1

#ifdef THREAD_STATS
/* ---------------------------------------------------------------
 * Some function to investigate which threads are executing
 * the LAPI header_handler functions.  Used only for debugging.
 * Should be disabled in a production build.
 * ---------------------------------------------------------------
 */

#define MAX_THREAD_STAT 10

typedef struct pstat_rec {
    pthread_t id;
    int       in_use;
    int       task_id;
    int       hndl_cnt;
    int       cmpl_cnt;
    char      name[128];
} pstat_t;

static pthread_key_t stat_key;
static pstat_t pstat[MAX_THREAD_STAT];
static int header_count = 0;
static int shortcut_count = 0;

void pstat_thread_attr(pstat_t *p, int index)
{
    /* would like to determine contention scope, scheduling
     * policy, scheduling priority, and number of kernel
     * threads mapped to this process
     */
    printf("[%d] THREAD %s index %d id %d count %d\n",
	   p->task_id,p->name,index,(int)p->id,__n_pthreads);
    fflush(stdout);
}

void pstat_thread_summary(pstat_t *p, int index)
{
    char *mode = (lapi_cur_mode == AMLAPI_POLLING ? "POLL" : "INT");
    printf("[%d] THREAD SUMMARY: %10s mode=%4s, ix=%2d, th_id=%8d, hdr_cnt=%8d, compl_cnt=%8d\n",
	   p->task_id,  p->name, mode, index, (long)(p->id),
	   p->hndl_cnt, p->cmpl_cnt);
}

pstat_t* pstat_alloc(char *str)
{
    int i;
    pstat_t *psp = NULL;

    /* find unused slot in array */
    for (i = 0; i < MAX_THREAD_STAT; i++) {
	if (! pstat[i].in_use) {
	    psp = &(pstat[i]);
	    psp->in_use = 1;
	    strcpy(psp->name,str);
	    psp->id = pthread_self();
	    psp->hndl_cnt = 0;
	    psp->cmpl_cnt = 0;
	    pstat_thread_attr(psp,i);
	    pthread_setspecific(stat_key,(void*)psp);
	    return psp;
	}
    }
    /* no slots available, abort */
    fprintf(stderr,"No pstat slots available\n");
    exit(0);
}
#endif

/* ---------------------------------------------------------------
 * Functions to minimize the cost of allocating and freeing
 * the message buffers for medium sized messages.
 * Medium sized messages are used heavily in Titanium and
 * their use should be optimized as much as possible.
 *
 * We implement a free of list unused message buffers.  The list
 * must allow simultanious access by a producer and consumer thread
 * without having to lock the list.
 * See notes in amlapi_task_queue.c for a discuss of this type of list.
 *
 * ---------------------------------------------------------------
 */
#if 1
/* starting in LAPI 2, packet size is 2KB */
#define MAX_SWITCH_PACKET_SIZE 2048
#else
#define MAX_SWITCH_PACKET_SIZE 1024
#endif
typedef union med_buf_rec {
    union med_buf_rec * volatile next;
    char buf[MAX_SWITCH_PACKET_SIZE];
} mbr;
static mbr* volatile mb_head = NULL;
static mbr* volatile mb_tail = NULL;

#define MALLOC_8BYTEALIGNED(mem_size) \
    ((void*)((( ((uintptr_t)malloc((mem_size)+8))) + 7) & (((uintptr_t)-1)<<3)))

/* ---------------------------------------------------------------
 * Initialize the free list with at most "num" buffers.
 * ---------------------------------------------------------------
 */
static int med_buf_init(int num) {
    int cnt = 0;
    for (cnt = 0; cnt < num; cnt++) {
	mbr *x = (mbr*)MALLOC_8BYTEALIGNED(sizeof(mbr));
	if (x == NULL) {
	    return cnt;
	}
	x->next = NULL;
	if (mb_tail == NULL) {
	    /* first element */
	    mb_head = mb_tail = x;
	} else {
	    mb_tail->next = x;
	    x = mb_tail;
	}
    }
    return cnt;
}

/* try to pull a buffer off the free list, if not available, use malloc */
void* med_buf_alloc(void) {
    mbr *x = NULL;

    if (mb_head == mb_tail) {
	/* list is empty, malloc */
	x = (mbr*)MALLOC_8BYTEALIGNED(sizeof(mbr));
	if (x == NULL) {
	    /* fatal */
	    fprintf(stderr,"Fatal: cant alloc needed medium buffer\n");
	    exit(1);
	}
	return (void*)x;
    }
    /* items on the free list, grab one */
    x = mb_head;
    mb_head = mb_head->next;
    return (void*)x;
}

/* put this buffer back on the free list */
void med_buf_free(void *x) {
    assert(mb_tail != NULL);
    mb_tail->next = (mbr*)x;
    memory_sync();
    mb_tail = (mbr*)x;
}

int AM_Poll (eb_t bundle) {
    int i;

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif

    LAPI_Probe(lapi_hndl);
    
    for (i = 0; i < AMLAPI_NUM_TASK_TO_EXEC_ON_POLL; i++) {
	if (!AMLAPI_ExecuteTaskFromBundle (bundle)) break;
    }
    /*printf ("%i polled %i tasks from queue.\n", AMLAPI_MYNODE, i);*/
    return AM_OK;
}

/* Given an amlapi_token, extract endpoint from the list of all endpoints */
ep_t AMLAPI_ExtractEndpointFromToken (struct amlapi_token * token)
{
    ep_t destEndpoint;
    /* printf ("header handler called. Source node/endpoint: %i/%i. Dest node/endpoint: %i/%i\n",AMLAPI_MYNODE, token->sourceNode, token->sourceEndpointId, token->destNode, token->destEndpointId);*/
    /* Sanity check */
    if (AMLAPI_MYNODE != token->destNode) {
	printf ("Holy shit!!! Wrong delivery!!\n");
	return NULL;
    }
    if (token->destEndpointId >= nextEndpointId) {
	printf ("Hey, invalid endpoint number!\n");
	return NULL;
    }
    /* Now fetch the correct function pointer and invoke it. */
    destEndpoint = endpointsInNode [token->destEndpointId];
    if (destEndpoint == NULL) {
	printf ("Hey, %ith endpoint at node %i has be freed.\n",
		token->destEndpointId, AMLAPI_MYNODE);
    }
    return destEndpoint;
}

/* Given an endpoint and handler_t index, extract the handler from the endpoint */
amlapi_handler_t AMLAPI_ExtractHandlerFromEndpoint (ep_t destEndpoint,
						    handler_t index)
{
    amlapi_handler_t handler;
    handler = destEndpoint->handler [index];
    return handler;
}

/* lookup the en_t from the translation table of an endpoint. */
int lookupTranslationTable (ep_t request_endpoint, int reply_endpoint,
			    en_t *entry)
{
    /* Sanity check */  
    if (request_endpoint == NULL) {
	printf ("Request endpoint invalid.\n");
	return 0;
    }
    if (!request_endpoint->translation [reply_endpoint].inuse) {
	printf ("Translation table entry not in use.\n");
	return 0;
    }
    *entry = request_endpoint->translation [reply_endpoint].remoteEndpoint;
    if (entry->node >= AMLAPI_NUMNODES) {
	printf ("Endpoint has wrong destination node.\n");
	return 0;
    }
    return 1;
}

int fillToken (struct amlapi_token * tokenPtr, ep_t request_endpoint,
	       en_t reply_endpoint)
{
    if (request_endpoint == NULL) { return AM_ERR_BAD_ARG; }
    if (tokenPtr == NULL) { return AM_ERR_BAD_ARG; }
    tokenPtr->sourceNode = AMLAPI_MYNODE;
    tokenPtr->sourceEndpointId = request_endpoint->name.endpoint_id;
    tokenPtr->destNode = reply_endpoint.node;
    tokenPtr->destEndpointId = reply_endpoint.endpoint_id;
    return AM_OK;
}

/* Application-visible API: */
int AM_Request0 (ep_t request_endpoint, int reply_endpoint, handler_t handler)
{
    en_t reply_endpoint_name;

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    if (! lookupTranslationTable (request_endpoint, reply_endpoint,
				  &reply_endpoint_name) ) {
	return AM_ERR_NOT_SENT;
    }
    return AMLAPI_SendAM (request_endpoint, reply_endpoint_name, handler, NULL,
			  0, AMLAPI_UHEADER_PLAIN, 0, NULL, 0, FALSE, REQUEST);
}

int AM_Request4 (ep_t request_endpoint, int reply_endpoint, handler_t handler,
		 int arg0, int arg1, int arg2, int arg3)
{
    en_t reply_endpoint_name;
    int argv[4];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    if (! lookupTranslationTable (request_endpoint, reply_endpoint,
				  &reply_endpoint_name) ) {
	return AM_ERR_NOT_SENT;
    }
    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    return AMLAPI_SendAM (request_endpoint, reply_endpoint_name, handler, NULL,
			  0, AMLAPI_UHEADER_PLAIN, 4, argv, 0, FALSE, REQUEST);
}

int AM_Request8 (ep_t request_endpoint, int reply_endpoint, handler_t handler,
		 int arg0, int arg1, int arg2, int arg3, int arg4, int arg5,
		 int arg6, int arg7)
{
    en_t reply_endpoint_name;
    int argv[8];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    if (! lookupTranslationTable (request_endpoint, reply_endpoint,
				  &reply_endpoint_name) ) {
	return AM_ERR_NOT_SENT;
    }
    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    argv [4] = arg4;
    argv [5] = arg5;
    argv [6] = arg6;
    argv [7] = arg7;
    return AMLAPI_SendAM (request_endpoint, reply_endpoint_name, handler, NULL,
			  0, AMLAPI_UHEADER_PLAIN, 8, argv, 0, FALSE, REQUEST);
}

int AM_RequestI0 (ep_t request_endpoint, int reply_endpoint, handler_t handler,
		  void * source_addr, int nbytes)
{
    en_t reply_endpoint_name;

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    if (! lookupTranslationTable (request_endpoint, reply_endpoint,
				  &reply_endpoint_name) ) {
	return AM_ERR_NOT_SENT;
    }
    return AMLAPI_SendAM (request_endpoint, reply_endpoint_name, handler,
			  source_addr, nbytes, AMLAPI_UHEADER_I, 0, NULL, 0,
			  FALSE, REQUEST);
}

int AM_RequestI4 (ep_t request_endpoint, int reply_endpoint, handler_t handler,
		  void * source_addr, int nbytes, int arg0, int arg1,
		  int arg2, int arg3)
{
    en_t reply_endpoint_name;
    int argv[4];
#if 0
printf("%i: Called AM_RequestI4(EP,dest=%i,handler=%i,src=%08x,nbytes=%i,arg0=%i,arg1=%i,arg2=%i,arg3=%i\n",
AMLAPI_MYNODE,reply_endpoint,handler,source_addr,nbytes,arg0,arg1,arg2,arg3);fflush(stdout);
#endif
#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    if (! lookupTranslationTable (request_endpoint, reply_endpoint,
				  &reply_endpoint_name) ) {
	return AM_ERR_NOT_SENT;
    }
    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    return AMLAPI_SendAM (request_endpoint, reply_endpoint_name, handler,
			  source_addr, nbytes, AMLAPI_UHEADER_I, 4, argv,
			  0, FALSE, REQUEST);
}

int AM_RequestI8 (ep_t request_endpoint, int reply_endpoint, handler_t handler,
		  void * source_addr, int nbytes, int arg0, int arg1, int arg2,
		  int arg3, int arg4, int arg5, int arg6, int arg7)
{
    en_t reply_endpoint_name;
    int argv[8];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    if (! lookupTranslationTable (request_endpoint, reply_endpoint,
				  &reply_endpoint_name) ) {
	return AM_ERR_NOT_SENT;
    }
    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    argv [4] = arg4;
    argv [5] = arg5;
    argv [6] = arg6;
    argv [7] = arg7;
    return AMLAPI_SendAM (request_endpoint, reply_endpoint_name, handler,
			  source_addr, nbytes, AMLAPI_UHEADER_I, 8, argv,
			  0, FALSE, REQUEST);
}

int AM_RequestXfer0 (ep_t request_endpoint, int reply_endpoint, uintptr_t dest_offset,
		     handler_t handler, void * source_addr, int nbytes)
{
    en_t reply_endpoint_name;

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif

    if (! lookupTranslationTable (request_endpoint, reply_endpoint,
				  &reply_endpoint_name) ) {
	return AM_ERR_NOT_SENT;
    }
    return AMLAPI_SendAM (request_endpoint, reply_endpoint_name, handler,
			  source_addr, nbytes, AMLAPI_UHEADER_XFER, 0,
			  NULL, dest_offset, FALSE, REQUEST);
}

int AM_RequestXfer4 (ep_t request_endpoint, int reply_endpoint, uintptr_t dest_offset,
		     handler_t handler, void * source_addr, int nbytes, 
		     int arg0, int arg1, int arg2, int arg3)
{
    en_t reply_endpoint_name;
    int argv[4];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    if (! lookupTranslationTable (request_endpoint, reply_endpoint,
				  &reply_endpoint_name) ) {
	return AM_ERR_NOT_SENT;
    }

    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    return AMLAPI_SendAM (request_endpoint, reply_endpoint_name, handler,
			  source_addr, nbytes, AMLAPI_UHEADER_XFER, 4,
			  argv, dest_offset, FALSE, REQUEST);
			  
}

int AM_RequestXfer8 (ep_t request_endpoint, int reply_endpoint, uintptr_t dest_offset,
		     handler_t handler, void * source_addr, int nbytes, 
		     int arg0, int arg1, int arg2, int arg3, int arg4,
		     int arg5, int arg6, int arg7)
{
    en_t reply_endpoint_name;
    int argv[8];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    if (! lookupTranslationTable (request_endpoint, reply_endpoint,
				  &reply_endpoint_name) ) {
	return AM_ERR_NOT_SENT;
    }

    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    argv [4] = arg4;
    argv [5] = arg5;
    argv [6] = arg6;
    argv [7] = arg7;
    return AMLAPI_SendAM (request_endpoint, reply_endpoint_name, handler,
			  source_addr, nbytes, AMLAPI_UHEADER_XFER, 8,
			  argv, dest_offset, FALSE, REQUEST);
			  
}

int AM_RequestXferAsync0 (ep_t request_endpoint, int reply_endpoint,
			  uintptr_t dest_offset, handler_t handler,
			  void * source_addr, int nbytes)
{
    en_t reply_endpoint_name;

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    if (! lookupTranslationTable (request_endpoint, reply_endpoint,
				  &reply_endpoint_name) ) {
	return AM_ERR_NOT_SENT;
    }
    return AMLAPI_SendAM (request_endpoint, reply_endpoint_name, handler,
			  source_addr, nbytes, AMLAPI_UHEADER_XFER, 0,
			  NULL, dest_offset, TRUE, REQUEST);
}

int AM_RequestXferAsync4 (ep_t request_endpoint, int reply_endpoint,
			  uintptr_t dest_offset, handler_t handler,
			  void * source_addr, int nbytes,
			  int arg0, int arg1, int arg2, int arg3)
{
    en_t reply_endpoint_name;
    int argv[4];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    if (! lookupTranslationTable (request_endpoint, reply_endpoint,
				  &reply_endpoint_name) ) {
	return AM_ERR_NOT_SENT;
    }
    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    return AMLAPI_SendAM (request_endpoint, reply_endpoint_name, handler,
			  source_addr, nbytes, AMLAPI_UHEADER_XFER, 4,
			  argv, dest_offset, TRUE, REQUEST);
}

int AM_RequestXferAsync8 (ep_t request_endpoint, int reply_endpoint,
			  uintptr_t dest_offset, handler_t handler,
			  void * source_addr, int nbytes,
			  int arg0, int arg1, int arg2, int arg3,
			  int arg4, int arg5, int arg6, int arg7)
{
    en_t reply_endpoint_name;
    int argv[8];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    if (! lookupTranslationTable (request_endpoint, reply_endpoint,
				  &reply_endpoint_name) ) {
	return AM_ERR_NOT_SENT;
    }
    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    argv [4] = arg4;
    argv [5] = arg5;
    argv [6] = arg6;
    argv [7] = arg7;
    return AMLAPI_SendAM (request_endpoint, reply_endpoint_name, handler,
			  source_addr, nbytes, AMLAPI_UHEADER_XFER, 8,
			  argv, dest_offset, TRUE, REQUEST);
			  
}

int AM_Reply0 (void* token, handler_t handler)
{
    struct amlapi_token * t = (struct amlapi_token *) token;
    ep_t endpoint = AMLAPI_ExtractEndpointFromToken (t);
    en_t origin_endpoint;

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    origin_endpoint.node = t->sourceNode;
    origin_endpoint.endpoint_id = t->sourceEndpointId;
    return AMLAPI_SendAM (endpoint, origin_endpoint, handler, NULL, 0,
			  AMLAPI_UHEADER_PLAIN, 0, NULL, 0, FALSE, REPLY);
}

int AM_Reply4 (void* token, handler_t handler,
	       int arg0, int arg1, int arg2, int arg3)
{
    struct amlapi_token * t = (struct amlapi_token *) token;
    ep_t endpoint = AMLAPI_ExtractEndpointFromToken (t);
    en_t origin_endpoint;
    int argv[4];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    origin_endpoint.node = t->sourceNode;
    origin_endpoint.endpoint_id = t->sourceEndpointId;
    return AMLAPI_SendAM (endpoint, origin_endpoint, handler, NULL, 0,
			  AMLAPI_UHEADER_PLAIN, 4, argv, 0, FALSE, REPLY);
}

int AM_Reply8 (void* token, handler_t handler,
	       int arg0, int arg1, int arg2, int arg3,
	       int arg4, int arg5, int arg6, int arg7)
{
    struct amlapi_token * t = (struct amlapi_token *) token;
    ep_t endpoint = AMLAPI_ExtractEndpointFromToken (t);
    en_t origin_endpoint;
    int argv[8];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    argv [4] = arg4;
    argv [5] = arg5;
    argv [6] = arg6;
    argv [7] = arg7;
    origin_endpoint.node = t->sourceNode;
    origin_endpoint.endpoint_id = t->sourceEndpointId;
    return AMLAPI_SendAM (endpoint, origin_endpoint, handler, NULL, 0,
			  AMLAPI_UHEADER_PLAIN, 8, argv, 0, FALSE, REPLY);
}

int AM_ReplyI0 (void* token, handler_t handler, void * source_addr, int nbytes)
{
    struct amlapi_token * t = (struct amlapi_token *) token;
    ep_t endpoint = AMLAPI_ExtractEndpointFromToken (t);
    en_t origin_endpoint;

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    origin_endpoint.node = t->sourceNode;
    origin_endpoint.endpoint_id = t->sourceEndpointId;
    return AMLAPI_SendAM (endpoint, origin_endpoint, handler,
			  source_addr, nbytes,
			  AMLAPI_UHEADER_I, 0, NULL, 0, FALSE, REPLY);
}

int AM_ReplyI4 (void* token, handler_t handler, void * source_addr, int nbytes,
		int arg0, int arg1, int arg2, int arg3)
{
    struct amlapi_token * t = (struct amlapi_token *) token;
    ep_t endpoint = AMLAPI_ExtractEndpointFromToken (t);
    en_t origin_endpoint;
    int argv[4];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    origin_endpoint.node = t->sourceNode;
    origin_endpoint.endpoint_id = t->sourceEndpointId;
    return AMLAPI_SendAM (endpoint, origin_endpoint, handler,
			  source_addr, nbytes,
			  AMLAPI_UHEADER_I, 4, argv, 0, FALSE, REPLY);
}

int AM_ReplyI8 (void* token, handler_t handler, void * source_addr, int nbytes,
		int arg0, int arg1, int arg2, int arg3,
		int arg4, int arg5, int arg6, int arg7)
{
    struct amlapi_token * t = (struct amlapi_token *) token;
    ep_t endpoint = AMLAPI_ExtractEndpointFromToken (t);
    en_t origin_endpoint;
    int argv[8];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    argv [4] = arg4;
    argv [5] = arg5;
    argv [6] = arg6;
    argv [7] = arg7;
    origin_endpoint.node = t->sourceNode;
    origin_endpoint.endpoint_id = t->sourceEndpointId;
    return AMLAPI_SendAM (endpoint, origin_endpoint, handler,
			  source_addr, nbytes,
			  AMLAPI_UHEADER_I, 8, argv, 0, FALSE, REPLY);
}

int AM_ReplyXfer0 (void* token, uintptr_t dest_offset, handler_t handler,
		   void * source_addr, int nbytes)
{
    struct amlapi_token * t = (struct amlapi_token *) token;
    ep_t endpoint = AMLAPI_ExtractEndpointFromToken (t);
    en_t origin_endpoint;

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    origin_endpoint.node = t->sourceNode;
    origin_endpoint.endpoint_id = t->sourceEndpointId;
    return AMLAPI_SendAM (endpoint, origin_endpoint, handler,
			  source_addr, nbytes,
			  AMLAPI_UHEADER_XFER, 0, NULL, dest_offset,
			  FALSE, REPLY);
}

int AM_ReplyXfer4 (void* token, uintptr_t dest_offset, handler_t handler,
		   void * source_addr, int nbytes,
		   int arg0, int arg1, int arg2, int arg3)
{
    struct amlapi_token * t = (struct amlapi_token *) token;
    ep_t endpoint = AMLAPI_ExtractEndpointFromToken (t);
    en_t origin_endpoint;
    int argv[4];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    origin_endpoint.node = t->sourceNode;
    origin_endpoint.endpoint_id = t->sourceEndpointId;
    return AMLAPI_SendAM (endpoint, origin_endpoint, handler,
			  source_addr, nbytes,
			  AMLAPI_UHEADER_XFER, 4, argv, dest_offset,
			  FALSE, REPLY);
}

int AM_ReplyXfer8 (void* token, uintptr_t dest_offset, handler_t handler,
		   void * source_addr, int nbytes,
		   int arg0, int arg1, int arg2, int arg3,
		   int arg4, int arg5, int arg6, int arg7)
{
    struct amlapi_token * t = (struct amlapi_token *) token;
    ep_t endpoint = AMLAPI_ExtractEndpointFromToken (t);
    en_t origin_endpoint;
    int argv[8];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    argv [4] = arg4;
    argv [5] = arg5;
    argv [6] = arg6;
    argv [7] = arg7;
    origin_endpoint.node = t->sourceNode;
    origin_endpoint.endpoint_id = t->sourceEndpointId;
    return AMLAPI_SendAM (endpoint, origin_endpoint, handler,
			  source_addr, nbytes,
			  AMLAPI_UHEADER_XFER, 8, argv, dest_offset,
			  FALSE, REPLY);
}

int AM_GetXfer0 (ep_t request_endpoint, int reply_endpoint, uintptr_t source_offset,
		 handler_t handler, uintptr_t dest_offset, int nbytes)
{

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    return AM_GetXferM(request_endpoint, reply_endpoint, source_offset,
		       handler, dest_offset, nbytes, 0, NULL);
}

int AM_GetXfer4 (ep_t request_endpoint, int reply_endpoint, uintptr_t source_offset,
		 handler_t handler, uintptr_t dest_offset, int nbytes,
		 int arg0, int arg1, int arg2, int arg3)
{
    int argv[4];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    return AM_GetXferM (request_endpoint, reply_endpoint, source_offset,
			handler, dest_offset, nbytes, 4, argv);
}

int AM_GetXfer8 (ep_t request_endpoint, int reply_endpoint, uintptr_t source_offset,
		 handler_t handler, uintptr_t dest_offset, int nbytes,
		 int arg0, int arg1, int arg2, int arg3,
		 int arg4, int arg5, int arg6, int arg7)
{
    int argv[8];

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    argv [0] = arg0;
    argv [1] = arg1;
    argv [2] = arg2;
    argv [3] = arg3;
    argv [4] = arg4;
    argv [5] = arg5;
    argv [6] = arg6;
    argv [7] = arg7;
    return AM_GetXferM (request_endpoint, reply_endpoint, source_offset,
			handler, dest_offset, nbytes, 8, argv);
}

int AM_MaxShort () { return AMLAPI_MAX_SHORT; }
int AM_MaxMedium () { return amlapi_max_stuffed; }
int AM_MaxLong () { return AMLAPI_MAX_LONG; }

/* get Xfer handler. Just issue an am_Xfer */
void get_xfer_completion_handler (lapi_handle_t * hndl, void * user_info)
{
    getxferhdr * xferuheader = (getxferhdr *) user_info;
    ep_t source_endpoint = AMLAPI_ExtractEndpointFromToken (&(xferuheader->token));
    int nbytes = xferuheader->nbytes;
    void * source_addr = &(((char*) source_endpoint->segAddr) [xferuheader->source_offset]);
    en_t dest_endpoint_name;
    dest_endpoint_name.node = xferuheader->token.sourceNode;
    dest_endpoint_name.endpoint_id = xferuheader->token.sourceEndpointId;
    AMLAPI_SendAM (source_endpoint, dest_endpoint_name, xferuheader->handler,
		   source_addr, xferuheader->nbytes, AMLAPI_UHEADER_XFER,
		   xferuheader->numArgs, xferuheader->argv,
		   xferuheader->dest_offset, FALSE, REQUEST);
}

void* get_xfer_header_handler (lapi_handle_t * hndl, void * uhdr,
			       uint * uhdr_len, unsigned long * msg_len,
			       compl_hndlr_t ** comp_h, void ** user_info)
{
    getxferhdr* xferheader = (getxferhdr *) malloc (sizeof (getxferhdr));
    memcpy (xferheader, uhdr, sizeof (getxferhdr));
    *user_info = (void*) xferheader;
    *comp_h = get_xfer_completion_handler;
}

int AM_GetXferM (ep_t request_endpoint, int reply_endpoint, uintptr_t source_offset,
		 handler_t handler, uintptr_t dest_offset, int nbytes,
		 int numArgs, int * argv)
{
    lapi_cntr_t cntr;
    int val = 0;
    getxferhdr * xferuheader = (getxferhdr *) malloc (sizeof (getxferhdr));
    en_t reply_endpoint_name;
    int tgt, i;

#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif
    
    if (! lookupTranslationTable (request_endpoint, reply_endpoint,
				  &reply_endpoint_name) ) {
	return AM_ERR_NOT_SENT;
    }

    xferuheader->handler = handler;
    xferuheader->source_offset = source_offset;
    xferuheader->dest_offset = dest_offset;
    xferuheader->handler = handler;
    xferuheader->nbytes = nbytes;
    xferuheader->numArgs = numArgs;
    xferuheader->token.sourceNode = request_endpoint->name.node;
    xferuheader->token.sourceEndpointId = request_endpoint->name.endpoint_id;
    xferuheader->token.destNode = reply_endpoint_name.node;
    xferuheader->token.destEndpointId = reply_endpoint_name.endpoint_id;
    for (i = 0; i < numArgs; i++) {
	xferuheader->argv[i] = argv[i];
    }
    LAPI_Setcntr (lapi_hndl, &cntr, 0);
    tgt = reply_endpoint_name.node;
    LAPI_Amsend (lapi_hndl, tgt, (void*)get_hndlr[tgt],
		 xferuheader, sizeof (getxferhdr), NULL, 0,
		 NULL, &cntr, NULL);
    LAPI_Waitcntr(lapi_hndl, &cntr, 1, &val);
    return AM_OK;
}

/* This function will execute the reply handler directly
 * (from the header_handler) in situations where all the data arrives
 * in the initial packet and in which the main thread is executing
 * the header_handler.  It is a short cut, and equivalent to a
 * combinatioan of
 *   AMLAPI_ScheduleTaskOnBundle with (is_ready == TRUE) followed by
 *   AMLAPI_ExecuteTaskFromBundle
*/
static void AMLAPI_ExecuteNow (amlapi_handler_t handler,
			       void* token, void* data, int nbytes,
			       int numArgs, int* argv, int type)
{
#ifdef THREAD_STATS
    shortcut_count++;
#endif
    switch (type) {
    case AMLAPI_UHEADER_PLAIN:
	switch (numArgs) {
	case 0:
	    handler(token);
	    break;
	case 4:
	    handler(token, argv[0], argv[1], argv[2], argv[3]);
	    break;
	case 8:
	    handler(token, argv[0], argv[1], argv[2], argv[3],
		    argv[4], argv[5], argv[6], argv[7]);
	    break;
	default:
            printf("BARF!!!!! Unknown numArgs=%i in AMLAPI_ExecuteNow\n",numArgs);fflush(stdout);abort();
	    break;
	}
	break;
	
    case AMLAPI_UHEADER_I:
	switch (numArgs) {
	case 0:
	    handler(token, data, nbytes);
	    break;
	case 4:
	    handler(token, data, nbytes,
		    argv[0], argv[1], argv[2], argv[3]);
	    break;
	case 8:
	    handler(token, data, nbytes,
		    argv[0], argv[1], argv[2], argv[3],
		    argv[4], argv[5], argv[6], argv[7]);
	    break;
	default:
            printf("BARF!!!!! Unknown numArgs=%i in AMLAPI_ExecuteNow\n",numArgs);fflush(stdout);abort();
	    break;
	}
	break;
	
    case AMLAPI_UHEADER_XFER:
	switch (numArgs) {
	case 0:
	    handler(token, data, nbytes);
	    break;
	case 4:
	    handler(token, data, nbytes,
		    argv[0], argv[1], argv[2], argv[3]);
	    break;
	case 8:
	    handler(token, data, nbytes,
		    argv[0], argv[1], argv[2], argv[3],
		    argv[4], argv[5], argv[6], argv[7]);
	    break;
	default:
            printf("BARF!!!!! Unknown numArgs=%i in AMLAPI_ExecuteNow\n",numArgs);fflush(stdout);abort();
	    break;
	}
	break;
	
    case AMLAPI_UHEADER_XFERASYNC:
	switch (numArgs) {
	case 0:
	    handler(token, data, nbytes);
	    break;
	case 4:
	    handler(token, data, nbytes,
		    argv[0], argv[1], argv[2], argv[3]);
	    break;
	case 8:
	    handler(token, data, nbytes,
		    argv[0], argv[1], argv[2], argv[3],
		    argv[4], argv[5], argv[6], argv[7]);
	    break;
	default:
            printf("BARF!!!!! Unknown numArgs=%i in AMLAPI_ExecuteNow\n",numArgs);fflush(stdout);abort();
	    break;
	}
	break;
	
    default:
        printf("BARF!!!!! Unknown type=%i in AMLAPI_ExecuteNow\n",type);fflush(stdout);abort();
	break;
    }

}

/* ---------------------------------------------------------------------
 * MLW: 19/26/01
 *
 * This is the completion handler used by LAPI_Amsend's header
 * handler function when called by AMLAPI_SendAM().
 *
 * This does not get called in the case of AMLAPI_UHEADER_PLAIN
 * because there is no need to wait for data to arrive in that case.
 *
 */
void completion_handler (lapi_handle_t * hndl, void * user_info)
{
    qe_t *q = (qe_t*)user_info;
    
#ifdef THREAD_STATS
    {
	pstat_t *psp;

	psp = (pstat_t*)pthread_getspecific(stat_key);
	if (psp == NULL) {
	    psp = pstat_alloc("COMPLETION");
	}
	psp->cmpl_cnt++;
    }
#endif
    /* printf ("%i completion handler called.\n", AMLAPI_MYNODE); */
    /* Mark this element, which is queued on the task queue,
     * ready for execution.
     */
    if (q == NULL) {
	fprintf(stderr,"Completion handler executed with NULL entry\n");
	exit(1);
    }
    q->ready = 1;
    return;
}

/* ---------------------------------------------------------------------
 * This is the header handler used by LAPI_Amsend() in the
 * AMLAPI_SendAM function.
 * ---------------------------------------------------------------------
 */
void* header_handler (lapi_handle_t * hndl, void * uhdr, uint * uhdr_len,
		      unsigned long * msg_len, compl_hndlr_t ** comp_h,
		      void ** user_info)
{
    uhdr_t *uheader = (uhdr_t*) uhdr;
    int nbytes = uheader->nbytes;
    int type = uheader->type;
    int M = uheader->M;
    handler_t handler_index = uheader->handler;
    uintptr_t destOffset = uheader->destOffset;
    /* freed when the AM handler is executed from the task queue */
    void* dest_addr = NULL;
    int is_ready = 0;
    ep_t endpoint;
    amlapi_handler_t handler;
    char* segAddr_byte_view;
    int is_reply = 0;
    int can_shortcut = 0;

#if 0
    printf("%i: in header_handler (handler %i)\n",AMLAPI_MYNODE,handler_index);fflush(stdout);
#endif

    /* Is this a request or reply handler? */
    is_reply = (type >> 8) & 0x1;
    type = type & AMLAPI_UHEADER_MASK;
    *user_info = NULL;

#ifdef THREAD_STATS
    {
	pstat_t *psp;

	header_count++;
	psp = (pstat_t*)pthread_getspecific(stat_key);
	if (psp == NULL) {
	    psp = pstat_alloc("HEADER");
	}
	psp->hndl_cnt++;
    }
#endif
    endpoint = AMLAPI_ExtractEndpointFromToken (&(uheader->token));
    handler = AMLAPI_ExtractHandlerFromEndpoint (endpoint, handler_index);

    /* has all the data already arrived? */
    is_ready = (nbytes <= amlapi_max_stuffed);
#ifdef AMLAPI_TQ_SHORTCUT 
    can_shortcut = (is_reply &&
		    pthread_equal(pthread_self(),amlapi_current_id));
#endif

    switch (type) {
    case AMLAPI_UHEADER_PLAIN:
	is_ready = 1;
	*comp_h = NULL;
	dest_addr = NULL;
	break;
	
    case AMLAPI_UHEADER_I:
	/* freed when the AM handler is executed from the task queue */
	if (can_shortcut) {
	    /* will call handler directly, use data area in uhdr */
	    assert( is_ready );
	    dest_addr = &uheader->stuffed[0];
	    *comp_h = NULL;
	} else {
	    /* data is here, but cant call handler directly, must copy data */
	    /* dest_addr = (void*) malloc (nbytes); */
	    dest_addr = med_buf_alloc();
	    if (is_ready) {
		*comp_h = NULL;
		memcpy(dest_addr,&uheader->stuffed[0],nbytes);
	    } else {
		*comp_h = completion_handler;
	    }
	}
	break;

    case AMLAPI_UHEADER_XFER:
    case AMLAPI_UHEADER_XFERASYNC:
	segAddr_byte_view = endpoint->segAddr;
	if (endpoint->segAddr == NULL) {
	    printf ("Endpoint  does not have associated VM segment!! \n");
	}
	if (endpoint->segLength < (destOffset + nbytes)) {
	    printf ("Endpoint's VM segment isn't that big!! (%i + %i > %i) \n",
		    destOffset, nbytes, endpoint->segLength);
	}
	dest_addr = (void*) (&(segAddr_byte_view [destOffset]));
	if (is_ready) {
	    /* data is stuffed in uheader, copy to segAddr and
	     * bypass the completion handler */
	    memcpy(dest_addr,&uheader->stuffed[0],nbytes);
	    *comp_h = NULL;
	} else {
	    /* normal case, arrange for completion handler to be called */
	    *comp_h = completion_handler;
	}

	break;
    default:
	printf("header_handler: Invalid type [%d]\n",type);
        printf("BARF!!!!!\n");fflush(stdout);abort();
	break;
    }

    /* Can only execute reply headers directly.  Reply handlers
     * are not allowed to make communication calls.  Request handlers
     * can make LAPI calls, which would cause a deadlock waiting on
     * the lock we already hold.
     */
    if (is_ready && can_shortcut) {
        /* Execute handler immediately.
	 * This is only allowed for reply handlers in which all
	 * the data has already arrived, and if we are a user thread.
         */
	eb_t eb = endpoint->endpoint_bundle;
	AMLAPI_ExecuteNow(handler,&(uheader->token),dest_addr,nbytes,M,&(uheader->argv[0]),type);
     	/* signal that a handler ran for AM_WaitSema (the only possible waiter
	 * thread is this same thread, earlier on the stack)
	 */
	eb->semaphore++;
    } else {
	qe_t *q;

	/* put on a task queue for processing later */
	q = AMLAPI_ScheduleTaskOnBundle(endpoint->endpoint_bundle, handler,
					&(uheader->token), dest_addr, nbytes, M, &(uheader->argv[0]),
					type, is_ready);
	/* send pointer to queue element to completion queue so that
	 * the entry can be marked ready without a search of the
	 * queue elements.
	 */
	*user_info = (is_ready ? NULL : (void*)q);
    }
	
    return (is_ready ? NULL : dest_addr);
}

/* handles the common code needed to send an AM over LAPI. */
/* MLW - 10/26/01
 * If called synchronously (isAsync == FALSE), then we must wait until
 * the origin counter to increment before returning.  This will indicate
 * that the data has been copied out of the source_addr and thus may
 * be re-written.
 *
 * If called in async mode, then the user may not modify the source_addr
 * memory until the AM reply handler executes.  The reply handler will
 * only execute after the request handler on the remote node executes.
 * At this point in time, the data in source_addr has been sent to
 * the remote node.  Thus it is not necessary to set any LAPI counters.
 */
int AMLAPI_SendAM (ep_t source_endpoint, en_t dest_endpoint, handler_t handler,
		   void * source_addr, int nbytes, int type, int M, int * argv,
		   uintptr_t dest_offset, char isAsync, int req_type)
{
    lapi_cntr_t orig_cntr;
    int val = 0;

    int i;
    int filledToken;
    char uheader_raw[MAX_SWITCH_PACKET_SIZE+8];  
    uhdr_t *uheader = (uhdr_t *)((uintptr_t)&uheader_raw[7] & ((uintptr_t)-1 << 3));
    int uheader_len = sizeof(uhdr_t) - 1;
    int is_stuffed = 0;
    int tgt;

    if (  nbytes <= amlapi_max_stuffed ) {
	/* we will stuff the data in the uheader for faster processing */
	if (nbytes > 0) {
	    uheader_len += nbytes;
	    memcpy(&uheader->stuffed[0],source_addr,nbytes);
	}
	is_stuffed = 1;
    }
    uheader_len = (uheader_len + 7) & (-1 << 3); /* round up to mult 8 as LAPI requires */
    filledToken = fillToken (&uheader->token, source_endpoint, dest_endpoint);
    if (filledToken != AM_OK) { return filledToken; }
    uheader->handler = handler;
    uheader->nbytes = nbytes;
    uheader->type = (req_type << 8) | type;
    uheader->M = M;  
    uheader->destOffset = dest_offset;
    for (i = 0; i < M; i++) {
	uheader->argv[i] = argv[i];
    }

    if (!AMLAPI_checkHeaderType (type)) {
	printf ("Wrong header type: %i.\n", type);
	return AM_ERR_BAD_ARG;
    }

    /* NOTE:
     * Given that the uheader is on the stack, do we have to
     * wait for the local completion counter to pop before we return?
     * This should not be the case, LAPI should have to copy it into
     * a network buffer before returning.  The local completion counter
     * should really be for when the data payload (from source_addr)
     * has been copied.  Given that, I think its ok not to wait for
     * the origin counter in the Async case
     */
    if (! isAsync) {
	LAPI_Setcntr (lapi_hndl, &orig_cntr, 0);
    }
    tgt = dest_endpoint.node;
    assert(uheader_len % 8 == 0 && uheader_len <= amlapi_max_uhdr_size);
    LAPI_Amsend (lapi_hndl, tgt,(void*) req_hndlr[tgt],
		 (void*)uheader, uheader_len,
		 (is_stuffed ? NULL : (void*) source_addr),
		 (is_stuffed ? 0    : nbytes),
		 NULL,
		 (isAsync ? NULL : &orig_cntr),
		 NULL);
    
    if (! isAsync) {
	LAPI_Waitcntr(lapi_hndl, &orig_cntr, 1, &val);
    }

    return AM_OK;
}

void AMLAPI_LAPI_Init () {
#ifdef THREAD_STATS
    printf("Before LAPI_Init: id = %d, count = %d\n",
	   (int)pthread_self(), __n_pthreads);
    fflush(stdout);
#endif
    bzero(&lapi_info, sizeof(lapi_info_t));
    lapi_info.protocol = TB3_DEV;
    lapi_info.err_hndlr = NULL;
    LAPI_Init (&lapi_hndl, &lapi_info);
#ifdef THREAD_STATS
    printf("After LAPI_Init: id = %d, count = %d\n",
	   (int)pthread_self(), __n_pthreads);
    fflush(stdout);
#endif
}

/* Must not be called before AMLAPI_LAPI_Init */
int AM_Init () {
    int rc;
    int mode;
    
#ifdef AMLAPI_TQ_SHORTCUT
    amlapi_current_id = pthread_self();
#endif

    /* default to polling mode, it performs better */
    AMLAPI_Set_Mode(AMLAPI_POLLING);
    
    LAPI_Qenv(lapi_hndl, TASK_ID, &AMLAPI_MYNODE);
    LAPI_Qenv(lapi_hndl, NUM_TASKS, &AMLAPI_NUMNODES);
    LAPI_Qenv(lapi_hndl, INTERRUPT_SET, &mode);
    lapi_cur_mode = (mode ? AMLAPI_INTERRUPT : AMLAPI_POLLING);
    LAPI_Qenv(lapi_hndl, MAX_UHDR_SZ, &amlapi_max_uhdr_size);
    amlapi_max_uhdr_size = amlapi_max_uhdr_size & (-1<<3); /* round down to mult 8 */
    amlapi_max_stuffed = amlapi_max_uhdr_size - sizeof(uhdr_t) + 1;
    assert(MAX_SWITCH_PACKET_SIZE >= amlapi_max_stuffed);
    
#if 0
    printf("amlapi_uheader = %d bytes, max_uhdr_size = %d bytes, max_stuffed = %d bytes\n",
	   sizeof(uhdr_t)-1,amlapi_max_uhdr_size,
	   amlapi_max_stuffed);
#endif
    
    LAPI_Gfence (lapi_hndl);

#ifdef THREAD_STATS
    /* this in called only once by the main thread.
     * Init the stats array and set-up main thread.
     */
    {
	int i;
	pstat_t *psp = NULL;
	
	for (i = 0; i < MAX_THREAD_STAT; i++) {
	    pstat[i].in_use = 0;
	    pstat[i].hndl_cnt = 0;
	    pstat[i].cmpl_cnt = 0;
	    pstat[i].name[0] = NULL;
	    pstat[i].task_id = AMLAPI_MYNODE;
	}

	/* create the key -- MUST only be done once */
	pthread_key_create(&stat_key,NULL);

	/* set the info for the main thread */
	psp = pstat_alloc("MAIN");
    }
#endif

    /* allocate medium size buffer pool */
    if (!med_buf_init(AMLAPI_MEDIUM_BUFFER_POOL)) {
	/* oops, can't even alloc one.  Fail */
	fprintf(stderr,"Fatal: cant alloc initial medium buffer\n");
	exit(1);
    }

    /* must exchange addresses of header_handler functions
     * for LAPI_Amsend() calls */
    req_hndlr = (void*)malloc( AMLAPI_NUMNODES * sizeof(void*) );
    if (req_hndlr == NULL) {
	return AM_ERR_RESOURCE;
    }
    get_hndlr = (void*)malloc( AMLAPI_NUMNODES * sizeof(void*) );
    if (get_hndlr == NULL) {
	return AM_ERR_RESOURCE;
    }
    rc = LAPI_Address_init(lapi_hndl,(void*)&header_handler,req_hndlr);
    if (rc != LAPI_SUCCESS) {
	fprintf(stderr,"Cant exchange addresses of header_handler [%d]\n",rc);
	return AM_ERR_RESOURCE;
    }
    assert(req_hndlr[AMLAPI_MYNODE] == (void*)&header_handler);
    rc = LAPI_Address_init(lapi_hndl,(void*)&get_xfer_header_handler,
			   get_hndlr);
    if (rc != LAPI_SUCCESS) {
	fprintf(stderr,"Cant exchange addresses of get_xfer_handler [%d]\n",rc);
	return AM_ERR_RESOURCE;
    }
    assert(get_hndlr[AMLAPI_MYNODE] == (void*)&get_xfer_header_handler);
    
    nextEndpointId = 0;
    return AM_OK;
}

int AM_Terminate () {
    LAPI_Term (lapi_hndl);
#ifdef THREAD_STATS
    {
	int i;
	for(i = 0; i < MAX_THREAD_STAT; i++) {
	    if (pstat[i].in_use) {
		pstat_thread_summary( &pstat[i] , i);
	    }
	}
	printf("MAIN: Total Header Handler Count = %d\n",header_count);
	printf("MAIN: Total Shortcut Count       = %d\n",shortcut_count);
    }
#endif
    return AM_OK;
}

int AMLAPI_Set_Mode(AMLAPI_MODE mode)
{
    int intr = (int)mode;
    int rc = LAPI_Senv(lapi_hndl,INTERRUPT_SET,intr);
    if (rc == LAPI_SUCCESS) {
	lapi_cur_mode = mode;
    }
    return rc;
}
