#!/bin/csh
# @ environment = "LL_JOB=TRUE"; MP_EUILIB=us
# @ node_usage = not_shared
# @ network.LAPI=css0,not_shared,US
# @ arguments = ""
# @ input = /dev/null
# @ notification = never
# @ output = testamlapi.4.1.out
# @ error = testamlapi.4.1.err
# @ initialdir = /rmount/paci/ucb/ux452200/amlapi/test 
# @ job_type = parallel
# @ node = 4
# @ tasks_per_node = 1
# @ wall_clock_limit = 00:02:00
# @ class = express 
# @ queue
./testamlapi 4 1
