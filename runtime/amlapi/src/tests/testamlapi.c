#include <stdio.h>
#include <stdlib.h>

#include "amlapi.h"
#include "apputils.h"

#define ABASE 0x69690000

#define A1  (ABASE + 1)
#define A2  (ABASE + 2)
#define A3  (ABASE + 3)
#define A4  (ABASE + 4)
#define A5  (ABASE + 5)
#define A6  (ABASE + 6)
#define A7  (ABASE + 7)
#define A8  (ABASE + 8)


#define SHORT_0REQ_HANDLER  10
#define SHORT_4REQ_HANDLER  14
#define SHORT_8REQ_HANDLER  18

#define SHORT_0REP_HANDLER  20
#define SHORT_4REP_HANDLER  24
#define SHORT_8REP_HANDLER  28

int numreq = 0;
int numrep = 0;
int myproc;
int numprocs;
/* ------------------------------------------------------------------------------------ */
void short_0req_handler(void *token) {
  printf ("%i Request handler0 called.\n", myproc);
  numreq++;
   AM_Reply0(token, SHORT_0REP_HANDLER);
  }
void short_4req_handler(void *token, int a1, int a2, int a3, int a4) {
  printf ("%i Request handler4 called.\n", myproc);
  if (a1!=A1||a2!=A2||a3!=A3||a4!=A4) {
    fprintf(stderr, "Arg mismatch on P%i %i %i %i %i\n", myproc, a1, a2, a3, a4);
    abort();
  }
  numreq++;
   AM_Reply4(token, SHORT_4REP_HANDLER, a1, a2, a3, a4);
  }
void short_8req_handler(void *token, int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8) {
  printf ("%i Request handler8 called.\n", myproc);
  if (a1!=A1||a2!=A2||a3!=A3||a4!=A4||a5!=A5||a6!=A6||a7!=A7||a8!=A8) {
    fprintf(stderr, "Arg mismatch on P%i %i %i %i %i %i %i %i %i \n", myproc, a1, a2, a3, a4, a5, a6, a7);
    abort();
  }
  numreq++;
   AM_Reply8(token, SHORT_8REP_HANDLER, a1, a2, a3, a4, a5, a6, a7, a8);
  }
/* ------------------------------------------------------------------------------------ */
void short_0rep_handler(void *token) {
  printf ("%i Reply handler0 called.\n", myproc);
  numrep++;

  }

void short_4rep_handler(void *token, int a1, int a2, int a3, int a4) {
  printf ("%i Reply handler4 called.\n", myproc);
  if (a1!=A1||a2!=A2||a3!=A3||a4!=A4) {
    fprintf(stderr, "Arg mismatch on P%i\n %i %i %i %i !!\n", myproc, a1, a2, a3, a4);
    abort();
  }
  numrep++;

  }
void short_8rep_handler(void *token, int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8) {
  printf ("%i Reply handler8 called.\n", myproc);
  if (a1!=A1||a2!=A2||a3!=A3||a4!=A4||a5!=A5||a6!=A6||a7!=A7||a8!=A8) {
    fprintf(stderr, "Arg mismatch on P%i %i %i %i %i %i %i %i %i !!\n", myproc, a1, a2, a3, a4, a5, a6, a7 ,a8);
    abort();
  }
  numrep++;

  }
/* ------------------------------------------------------------------------------------ */
int main(int argc, char **argv) {
  eb_t eb;
  ep_t ep;
  en_t en;
  uint64_t networkpid;
  int partner;
  int iters=0, depth=0, polling = 1, i;
  int temp;
system("uname -a");

  if (argc < 2) {
    printf("Usage: %s (iters) (Poll/Block) (netdepth)\n", argv[0]);
    exit(1);
    }

  if (argc > 5) depth = atoi(argv[5]);
  if (!depth) depth = 4;

  putenv("A=A");
  putenv("B=B");
  putenv("C=C");
  putenv("ABC=ABC");
  putenv("AReallyLongEnvironmentName=A Really Long Environment Value");

  /* call startup */
  /*
  AM_Safe(AMUDP_SPMDStartup(&argc, &argv, 
                            0, depth, NULL, 
                            &networkpid, &eb, &ep));
  */
  if (!eb || !ep) return (AM_ERR_BAD_ARG);
  AMLAPI_LAPI_Init ();
  if (AM_Init() != AM_OK) {
    printf ("Init.\n");
    return 1;
  }
  temp = AM_AllocateBundle(AM_SEQ, &eb);
  if (temp != AM_OK) {
    printf ("Allocate bundle\n");
    return 1;
  }
  temp = AM_AllocateEndpoint(eb, &ep, &en);
  if (temp != AM_OK) {
    printf ("Allocate endpoint\n");
    return 1;
  }
  /* setup translation table */
  for (i = 0; i < AMLAPI_NumProcs (); i++) {
    en_t e;
    e.node = i;
    e.endpoint_id = 0;
    temp = AM_Map(ep, i, e, AM_NOTEMPTY);
    if (temp != AM_OK) {
      printf ("AM Map\n");
      return 1;
    }
  }

  temp = AM_SetExpectedResources(ep, AMLAPI_NumProcs (), 10);
  if (temp != AM_OK) {
    printf ("Set expected resources.\n");
    return 1;
  }
  /* set tag */
  temp = AM_SetTag(ep, AM_NOTEMPTY);
  if (temp != AM_OK) {
    printf ("Set tag.\n");
    return 1;
  }

  if (argc > 1) iters = atoi(argv[1]);
  if (!iters) iters = 1;

  if (argc > 2) {
    switch(argv[2][0]) {
      case 'p': case 'P': polling = 1; break;
      case 'b': case 'B': polling = 0; break;
      default: printf("polling must be 'P' or 'B'..\n"); exit(1);
      }
    }

  /* setup handlers */
  AM_Safe(AM_SetHandler(ep, SHORT_0REQ_HANDLER, short_0req_handler));
  AM_Safe(AM_SetHandler(ep, SHORT_4REQ_HANDLER, short_4req_handler));
  AM_Safe(AM_SetHandler(ep, SHORT_8REQ_HANDLER, short_8req_handler));

  AM_Safe(AM_SetHandler(ep, SHORT_0REP_HANDLER, short_0rep_handler));
  AM_Safe(AM_SetHandler(ep, SHORT_4REP_HANDLER, short_4rep_handler));
  AM_Safe(AM_SetHandler(ep, SHORT_8REP_HANDLER, short_8rep_handler));
  setupUtilHandlers(ep, eb);
  
  numreq = 0;
  numrep = 0;

  /* barrier */
  /*
  AM_Safe(AMUDP_SPMDBarrier());
  */
  AMLAPI_Barrier ();

  /* get SPMD info */
  /*
  myproc = AMUDP_SPMDMyProc();
  numprocs = AMUDP_SPMDNumProcs();
  */
  myproc = AMLAPI_MyProc ();
  numprocs = AMLAPI_NumProcs ();

  partner = (myproc + 1)%numprocs;

  /* compute */

  for (i=0; i < iters; i++) {
    AM_Safe(AM_Request0(ep, partner, SHORT_0REQ_HANDLER));
    AM_Safe(AM_Request4(ep, partner, SHORT_4REQ_HANDLER, A1, A2, A3, A4));
    AM_Safe(AM_Request8(ep, partner, SHORT_8REQ_HANDLER, A1, A2, A3, A4, A5, A6, A7, A8));
    printf ("%i thinks that request 8 0 is finished.\n", AMLAPI_MyProc ());

    while (numrep < 3*(i+1)) {
      if (polling) {
	int x = AM_Poll(eb);;
	/*if (x > 0) { printf ("%i pulled %i from task queue.\n", AMLAPI_MyProc (), x); } */
        } 
      else {
        AM_Safe(AM_SetEventMask(eb, AM_NOTEMPTY));
        AM_Safe(AM_WaitSema(eb));
        /*AM_Safe(AM_Poll(eb));*/
	AM_Poll (eb);
        }
      }
    }

  /* have to wait for all the request handlers to run as well */
  while (numreq < 3*iters) {
      if (polling) {
          int x = AM_Poll(eb);;
      } else {
          AM_Safe(AM_SetEventMask(eb, AM_NOTEMPTY));
          AM_Safe(AM_WaitSema(eb));
          AM_Poll (eb);
      }
  }

  printf ("%i Finished testing handlers.\n", AMLAPI_MyProc ());

  /* barrier */
  AMLAPI_Barrier();

  /* exit */
  /*
  AM_Safe(AMUDP_SPMDExit(0));
  */
  AM_Terminate ();

  printf ("Terminated.\n");

  return 0;
  }
/*------------------------------------------------------------------------------------*/
