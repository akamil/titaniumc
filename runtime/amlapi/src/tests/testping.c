#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <amlapi.h>

#include "apputils.h"

#ifdef DEBUG
#define VERBOSE 0
#else
#define VERBOSE 0
#endif

#define PING_REQ_HANDLER 1
#define PING_REP_HANDLER 2

static volatile int numleft;

int myproc;
int numprocs;

static void ping_request_handler(void *token) {
    numleft--;

#if VERBOSE
    printf("%i: ping_request_handler(). sending reply...\n", myproc); fflush(stdout);
#endif

    AM_Safe(AM_Reply0(token, PING_REP_HANDLER));
}

static void ping_reply_handler(void *token) {

#if VERBOSE
    printf("%i: ping_reply_handler()\n", myproc); fflush(stdout);
#endif

    numleft--;
}

/* usage: testping  numprocs  spawnfn  iters  P/B  depth
 */
int main(int argc, char **argv) {
    eb_t eb;
    ep_t ep;
    en_t en;
    uint64_t networkpid;
/*     clock_t begin, end; */
    int64_t begin, end;
    double  total;
    int polling = 1;
    int k;
    int iters = 0;
    int depth = 0;
    int temp, i;
    const time_t * Timer;
    char BufferPointer [26];

    if (argc < 3) {
	printf("Usage: %s (iters) (Poll/Block)\n",
	       argv[0]);
	exit(1);
    }

    /* call startup */
    /*
      AM_Safe(AMUDP_SPMDStartup(&argc, &argv, 
      0, depth, NULL, 
      &networkpid, &eb, &ep));
    */
    if (!eb || !ep) return (AM_ERR_BAD_ARG);
    AMLAPI_LAPI_Init ();
    if (AM_Init() != AM_OK) {
	printf ("Init.\n");
	return 1;
    }
    temp = AM_AllocateBundle(AM_SEQ, &eb);
    if (temp != AM_OK) {
	printf ("Allocate bundle\n");
	return 1;
    }
    temp = AM_AllocateEndpoint(eb, &ep, &en);
    if (temp != AM_OK) {
	printf ("Allocate endpoint\n");
	return 1;
    }
    /* setup translation table */
    for (i = 0; i < AMLAPI_NumProcs (); i++) {
	en_t e;
	e.node = i;
	e.endpoint_id = 0;
	temp = AM_Map(ep, i, e, AM_NOTEMPTY);
	if (temp != AM_OK) {
	    printf ("AM Map\n");
	    return 1;
	}
    }

    temp = AM_SetExpectedResources(ep, AMLAPI_NumProcs (), 10);
    if (temp != AM_OK) {
	printf ("Set expected resources.\n");
	return 1;
    }
    /* set tag */
    temp = AM_SetTag(ep, AM_NOTEMPTY);
    if (temp != AM_OK) {
	printf ("Set tag.\n");
	return 1;
    }

    /* setup handlers */
    AM_Safe(AM_SetHandler(ep, PING_REQ_HANDLER, ping_request_handler));
    AM_Safe(AM_SetHandler(ep, PING_REP_HANDLER, ping_reply_handler));

    setupUtilHandlers(ep, eb);

    /* get SPMD info */
    /*
      myproc = AMUDP_SPMDMyProc();
      numprocs = AMUDP_SPMDNumProcs();
    */

    myproc = AMLAPI_MyProc ();
    numprocs = AMLAPI_NumProcs ();

    if (argc > 1) iters = atoi(argv[1]);
    if (!iters) iters = 1;
    if (argc > 2) {
	switch(argv[2][0]) {
	case 'p': case 'P': polling = 1; break;
	case 'b': case 'B': polling = 0; break;
	default: printf("polling must be 'P' or 'B'..\n"); return 1;
	}
    }

    if (myproc == 0) numleft = (numprocs-1)*iters;
    else numleft = iters;

    /*
      AM_Safe(AMUDP_SPMDBarrier());
    */
    AMLAPI_Barrier ();
  
    if (myproc == 0) printf("Running %i iterations of ping test...\n", iters);

    begin = getMicrosecondTimeStamp();
    /* begin = clock (); */

    if (myproc != 0) { /* everybody sends packets to 0 */
	for (k=0;k < iters; k++) {
#if VERBOSE
	    printf("%i: sending request...", myproc); fflush(stdout);
#endif
	    AM_Safe(AM_Request0(ep, 0, PING_REQ_HANDLER));
	} 
    }

    if (polling) { /* poll until everyone done */
	while (numleft) {
	    /*AM_Safe(AM_Poll(eb));*/
	    AM_Poll (eb);
	}
    }
    else {
	while (numleft) {
	    AM_Safe(AM_SetEventMask(eb, AM_NOTEMPTY)); 
	    AM_Safe(AM_WaitSema(eb));
	    /*AM_Safe(AM_Poll(eb));*/
	    AM_Poll (eb);
	}
    }

    end = getMicrosecondTimeStamp();
    /* end = clock (); */

    total = (double)(end - begin);
    if (myproc != 0) printf("Slave %i: %9.3f microseconds, throughput: %9.3f requests/sec (%9.3f us / request)\n",
			    myproc, total,
			    ((double)(1000000*iters))/total,
			    total/((double)iters));
    else printf("Slave 0 done.\n");
    fflush(stdout);

    /* dump stats */
    /*
      AM_Safe(AMUDP_SPMDBarrier());
      printGlobalStats();
      AM_Safe(AMUDP_SPMDBarrier());
      AM_Safe(AMUDP_SPMDExit(0));
    */

    AMLAPI_Barrier ();

    /* exit */

    return 0;
}
/*------------------------------------------------------------------------------------*/
