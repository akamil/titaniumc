#include "amlapi.h"
#include "apputils.h"

/* init by setupUtilHandlers */
static ep_t ep = NULL;
static eb_t eb = NULL;

#define STATS_REQ_HANDLER     (APPUTIL_HANDLER_BASE+0)

#define GET_REQ_HANDLER       (APPUTIL_HANDLER_BASE+1)
#define GET_REP_HANDLER       (APPUTIL_HANDLER_BASE+2)
#define PUT_REQ_HANDLER       (APPUTIL_HANDLER_BASE+3)
#define PUT_REP_HANDLER       (APPUTIL_HANDLER_BASE+4)

#define READ_REQ_HANDLER      (APPUTIL_HANDLER_BASE+5)
#define READ_REP_HANDLER      (APPUTIL_HANDLER_BASE+6)
#define WRITE_REQ_HANDLER     (APPUTIL_HANDLER_BASE+7)
#define WRITE_REP_HANDLER     (APPUTIL_HANDLER_BASE+8)

/* synchronous gets and puts */
static void get_reply_handler(void *token, int ctr, int dest, int val, int dump) {
  uint32_t *pctr;
  uint32_t *pdest;
  
  pctr = (uint32_t *)ctr;
  pdest = (uint32_t *)dest;
  assert(pctr);
  assert(pdest);
  *pdest = (uint32_t)val;
  *pctr = TRUE;
  }

static void get_request_handler(void *token, int ctr, int dest, int addr, int dump) {
  uint32_t *paddr;

  paddr = (uint32_t *)addr;
  assert(paddr);

  AM_Safe(AM_Reply4(token, GET_REP_HANDLER, 
                    ctr, dest, *paddr, 0));
  }

uint32_t getWord(int proc, void *addr) {
  volatile uint32_t getdone = FALSE;
  volatile uint32_t getval = 0;
  AM_Safe(AM_Request4(ep, proc, GET_REQ_HANDLER, 
                      (int)&getdone, (int)&getval, (int)addr, 0));
  while (!getdone) AM_PollBlock(eb);
  return getval;
  }

static void put_reply_handler(void *token, int ctr, int dump1, int dump2, int dump3) {
  uint32_t *pctr;

  pctr = (uint32_t *)ctr;
  assert(pctr);
  *pctr = TRUE;
  }

static void put_request_handler(void *token, int ctr, int dest, int val, int dump) {
  uint32_t *paddr;

  paddr = (uint32_t *)dest;
  assert(paddr);
  *paddr = (uint32_t)val;

  AM_Safe(AM_Reply4(token, PUT_REP_HANDLER, 
                    ctr, 0, 0, 0));
  }

void putWord(int proc, void *addr, uint32_t val) {
  volatile uint32_t putdone = FALSE;
  AM_Safe(AM_Request4(ep, proc, PUT_REQ_HANDLER, 
                      (int)&putdone, (int)addr, (int)val, 0));
  while (!putdone) AM_PollBlock(eb);
  return;
  }

static volatile uint32_t readCtr = 0;
static void read_reply_handler(void *token, int ctr, int dest, int val, int dump) {
  uint32_t *pctr;
  uint32_t *pdest;

  pctr = (uint32_t *)ctr;
  pdest = (uint32_t *)dest;
  assert(pctr);
  assert(pdest);
  *pdest = (uint32_t)val;
  (*pctr)--;
  }

static void read_request_handler(void *token, int ctr, int dest, int addr, int dump) {
  uint32_t *paddr;

  paddr = (uint32_t *)addr;
  assert(paddr);

  AM_Safe(AM_Reply4(token, READ_REP_HANDLER, 
                    ctr, dest, *paddr, 0));
  }


void readWord(void *destaddr, int proc, void *addr) {
  AM_Safe(AM_Request4(ep, proc, READ_REQ_HANDLER, 
                      (int)&readCtr, (int)destaddr, (int)addr, 0));
  readCtr++;
  return;
  }

void readSync() {
  while (readCtr) AM_PollBlock(eb);
  }

static volatile uint32_t writeCtr = 0;
static void write_reply_handler(void *token, int ctr, int dump1, int dump2, int dump3) {
  uint32_t *pctr;

  pctr = (uint32_t *)ctr;
  assert(pctr);
  (*pctr)--;
  }

static void write_request_handler(void *token, int ctr, int dest, int val, int dump) {
  uint32_t *paddr;

  paddr = (uint32_t *)dest;
  assert(paddr);
  *paddr = (uint32_t)val;

  AM_Safe(AM_Reply4(token, WRITE_REP_HANDLER, 
                    ctr, 0, 0, 0));
  }

void writeWord(int proc, void *addr, uint32_t val) {
  AM_Safe(AM_Request4(ep, proc, WRITE_REQ_HANDLER, 
                      (int)&writeCtr, (int)addr, (int)val, 0));
  writeCtr++;
  return;
  }

void writeSync() {
  while (writeCtr) AM_PollBlock(eb);
  }
void setupUtilHandlers(ep_t activeep, eb_t activeeb) {
  assert(activeep && activeeb);
  ep = activeep;
  eb = activeeb;

  AM_Safe(AM_SetHandler(ep, GET_REQ_HANDLER, get_request_handler));
  AM_Safe(AM_SetHandler(ep, GET_REP_HANDLER, get_reply_handler));
  AM_Safe(AM_SetHandler(ep, PUT_REQ_HANDLER, put_request_handler));
  AM_Safe(AM_SetHandler(ep, PUT_REP_HANDLER, put_reply_handler));

  AM_Safe(AM_SetHandler(ep, READ_REQ_HANDLER, read_request_handler));
  AM_Safe(AM_SetHandler(ep, READ_REP_HANDLER, read_reply_handler));
  AM_Safe(AM_SetHandler(ep, WRITE_REQ_HANDLER, write_request_handler));
  AM_Safe(AM_SetHandler(ep, WRITE_REP_HANDLER, write_reply_handler));

}
/* -----------------------------------------------------------
 * Timer function
 * -----------------------------------------------------------
 */
int64_t getMicrosecondTimeStamp(void) {
    int64_t retval;
    struct timeval tv;
    if (gettimeofday(&tv, NULL)) {
	perror("gettimeofday");
	abort();
    }
    retval = ((int64_t)tv.tv_sec) * 1000000 + tv.tv_usec;
    return retval;
}
