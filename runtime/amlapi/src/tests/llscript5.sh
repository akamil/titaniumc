#!/bin/csh
# @ environment = "LL_JOB=TRUE"; MP_EUILIB=us
# @ node_usage = not_shared
# @ network.LAPI=css0,not_shared,US
# @ arguments = ""
# @ input = /dev/null
# @ notification = never
# @ output = testbulk.2.1.out
# @ error = testbulk.2.1.err
# @ initialdir = /rmount/paci/ucb/ux452200/amlapi/test 
# @ job_type = parallel
# @ node = 2
# @ tasks_per_node = 1
# @ wall_clock_limit = 00:05:00
# @ class = express 
# @ queue
./testbulk 2 1 10000 262144
