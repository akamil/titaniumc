#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <amlapi.h>
#include "apputils.h"

#define VERBOSE 0

#define BULK_REQ_HANDLER 1
#define BULK_REP_HANDLER 2

int my_id = -1;
int peer_id = -1;
int numprocs;

int num_iter = 10000;
int verbose = 0;
int debug = 0;
int polling = 0;
int LAPI_polling = 0;

int done = 0;
char *VMseg;

eb_t eb;
ep_t ep;

/* Message sizes */
#define KB  1024
#define MB  (KB*KB)
/* #define MAX_MSG_SIZE (128*KB) */
#define MAX_MSG_SIZE (32*KB)

#define FIRSTSZ (1)
/* #define NEXTSZ(x) (x*2) */
#define NEXTSZ(x) (next_size(x))
#define DONESZ(x) (x > MAX_MSG_SIZE)

/* Macro to check return value from LAPI function call */
char    err_msg_str[LAPI_MAX_ERR_STRING];
int     rc;
#define L_CHECK(name,func)           \
if ((rc = func) != LAPI_SUCCESS) {   \
   LAPI_Msg_string(rc,err_msg_str);       \
   fprintf(stderr,"LAPI Error in %s, [%s] rc = %d\n",name,err_msg_str,rc); \
   exit(rc);                         \
}

/* function prototypes */
double Pingpong(char* srcmem, int size, int niter);
int next_size(int size);

/* -----------------------------------------------------------
 * This runs on the "remote" host and effectively receives the
 * array of data.
 * It then sends the reply
 * -----------------------------------------------------------
 */
static void bulk_request_handler(void *token, void *buf, int nbytes,
				 int arg, int arg2, int arg3, int arg4)
{
    char *recvdbuf = (char *)buf;

    if (verbose) {
	printf("%i: bulk_request_handler(). starting...", my_id);
	fflush(stdout);
    }

    /*
      assert(arg == 666);
      assert(buf == ((char *)VMseg) + 100);
    */

    /*  verify the result */
    if (debug) {
	int i;
	for (i = 0; i < nbytes; i++) {
	    if (recvdbuf[i] != (char)(i%256)) {
		printf("%i: ERROR: mismatched data recvdbuf[%i]=%i\n",
		       my_id, i, (int)recvdbuf[i]);
		fflush(stdout);
		abort();
	    }
	}
    }

    if (verbose) {
	printf("%i: bulk_request_handler(). sending reply...", my_id);
	fflush(stdout);
    }

    AM_Safe(AM_Reply0(token, BULK_REP_HANDLER));
    done++;
}

/* -----------------------------------------------------------
 * This runs on the local host when the request is completed
 * on the remote host.
 * -----------------------------------------------------------
 */
static void bulk_reply_handler(void *token, int ctr, int dest, int val)
{
    if (verbose) {
	printf("%i: bulk_reply_handler()\n", my_id);
	fflush(stdout);
    }

    done++;
}

/* -----------------------------------------------------------
 * Parse the command line arguments
 *   -v      => verbose mode
 *   -d lev  => debug level
 *   -i iter => number of iterations
 *   -p      => use LAPI Put for Pingpong
 *   -g      => use LAPI Get for Pingpong
 * -----------------------------------------------------------
 */
char usage[] = "usage: pingpong [d:vn:pP] \n"
"\t d lev       => debug level\n"
"\t v           => verbose mode\n"
"\t n num_iter  => number of time to iterate per size\n"
"\t p           => AM Polling mode\n"
"\t P           => LAPI Polling (vs Interrupt) mode\n"
"\n";
int parse_args(int argc, char *argv[])
{
    int i;
    
    /* parse the command line options */
    while ( (i=getopt(argc,argv,"d:vn:pP")) != -1 ) {
        switch(i) {
        case 'n':
            num_iter = atoi(optarg);
            break;
        case 'v':
            verbose = 1;
            break;
        case 'd':
	    debug = atoi(optarg);
            break;
        case 'p':
            polling = 1;
            break;
        case 'P':
            LAPI_polling = 1;
            break;
 
        default:
            printf("invalid switch: %d\n",i);
            perror(usage);
        }
    }
    argv += optind;
    argc -= optind;

 
    /* now process the rest of the command line */

    return 1;
}

/* -----------------------------------------------------------
 * 
 * -----------------------------------------------------------
 */
int main(int argc, char **argv) {
    en_t en;
    int temp, i;
    char *srcmem;
    double mb_factor, dt;
    int size;
    char *lapi_mode;
    char *am_mode;
    lapi_handle_t *context = NULL;

    AMLAPI_LAPI_Init ();
    if (AM_Init() != AM_OK) {
	printf ("Init.\n");
	return 1;
    }
    temp = AM_AllocateBundle(AM_SEQ, &eb);
    if (temp != AM_OK) {
	printf ("Allocate bundle\n");
	return 1;
    }
    temp = AM_AllocateEndpoint(eb, &ep, &en);
    if (temp != AM_OK) {
	printf ("Allocate endpoint\n");
	return 1;
    }
    context = AMLAPI_GetLAPIHandle();
    /* setup translation table */
    for (i = 0; i < AMLAPI_NumProcs (); i++) {
	en_t e;
	e.node = i;
	e.endpoint_id = 0;
	temp = AM_Map(ep, i, e, AM_NOTEMPTY);
	if (temp != AM_OK) {
	    printf ("AM Map\n");
	    return 1;
	}
    }

    temp = AM_SetExpectedResources(ep, AMLAPI_NumProcs (), 10);
    if (temp != AM_OK) {
	printf ("Set expected resources.\n");
	return 1;
    }
    /* set tag */
    temp = AM_SetTag(ep, AM_NOTEMPTY);
    if (temp != AM_OK) {
	printf ("Set tag.\n");
	return 1;
    }

    /* setup handlers */
    AM_Safe(AM_SetHandler(ep, BULK_REQ_HANDLER, bulk_request_handler));
    AM_Safe(AM_SetHandler(ep, BULK_REP_HANDLER, bulk_reply_handler));

    setupUtilHandlers(ep, eb);

    my_id = AMLAPI_MyProc ();
    numprocs = AMLAPI_NumProcs ();
    peer_id = (my_id + 1) % 2;
    mb_factor = (1000000.0 / (double)(MB));

    /* get the command line args, if any */
    parse_args(argc,argv);

    /* insure we can send message as one packet */
    if (my_id == 0) {
	if (AM_MaxLong() < MAX_MSG_SIZE) {
	    printf("MAX_MSG_SIZE=%d too big, MaxLong=%d\n",
		   MAX_MSG_SIZE,AM_MaxLong());
	    exit(-1);
	}
    }

    /* alloc and setup memory buffers and VMseg */
    size = MAX_MSG_SIZE;
    srcmem = valloc(size);
    if (srcmem == NULL) {
	printf("Unable to valloc %d bytes\n",size);
	exit(-1);
    }
    memset(srcmem, 0, size);
    VMseg = (char *)valloc(size+100);
    if (VMseg == NULL) {
	printf("Unable to valloc %d bytes\n",size+100);
	exit(-1);
    }
    memset(VMseg, 0, size+100);
    AM_Safe(AM_SetSeg(ep, VMseg, size+100));

    if (my_id == 0) { /*  init my source mem */
	int i;
	for (i=0; i < size; i++)
	    srcmem[i] = (char)(i%256);
    }

    if (debug > 0) {
	L_CHECK("Senv",LAPI_Senv(*context, ERROR_CHK, 1));
    } else {
	L_CHECK("Senv",LAPI_Senv(*context, ERROR_CHK, 0));
    }
    if (LAPI_polling) {
	L_CHECK("Set Polling",AMLAPI_Set_Mode(AMLAPI_POLLING));
	lapi_mode = "POLLING";
    } else {
	L_CHECK("Set Interrupt",AMLAPI_Set_Mode(AMLAPI_INTERRUPT));
	lapi_mode = "INTERRUPT";
    }
    if (polling) {
	am_mode = "polling";
    } else {
	am_mode = "non-polling";
    }
	
    if (my_id == 0) {
	/* LAPI parameters */
	int    max_data_sz;
	int    max_uhdr_sz;
	int    error_check;
	int    timeout;
	int    min_timeout;
	int    max_timeout;
	int    intr_set;
	int    max_ports;
	int    max_pkt_size;
	int    num_rex_bufs;
	int    rex_buf_size;
	int    addr_tbl_size;

	L_CHECK("Qenv",LAPI_Qenv(*context, MAX_DATA_SZ, &max_data_sz));
	L_CHECK("Qenv",LAPI_Qenv(*context, MAX_UHDR_SZ, &max_uhdr_sz));
	L_CHECK("Qenv",LAPI_Qenv(*context, ERROR_CHK, &error_check));
	L_CHECK("Qenv",LAPI_Qenv(*context, TIMEOUT, &timeout));
	L_CHECK("Qenv",LAPI_Qenv(*context, MIN_TIMEOUT, &min_timeout));
	L_CHECK("Qenv",LAPI_Qenv(*context, MAX_TIMEOUT, &max_timeout));
	L_CHECK("Qenv",LAPI_Qenv(*context, INTERRUPT_SET, &intr_set));
	L_CHECK("Qenv",LAPI_Qenv(*context, MAX_PORTS, &max_ports));
	L_CHECK("Qenv",LAPI_Qenv(*context, MAX_PKT_SZ, &max_pkt_size));
	L_CHECK("Qenv",LAPI_Qenv(*context, NUM_REX_BUFS, &num_rex_bufs));
	L_CHECK("Qenv",LAPI_Qenv(*context, REX_BUF_SZ, &rex_buf_size));
	L_CHECK("Qenv",LAPI_Qenv(*context, LOC_ADDRTBL_SZ, &addr_tbl_size));
	
	printf("My ID                           = %d\n",my_id);
	printf("My Peer                         = %d\n",peer_id);
	printf("Num Iter                        = %d\n",num_iter);
	printf("Max Msg Size                    = %d\n",MAX_MSG_SIZE);
	printf("AM mode                         = %s\n",am_mode);
	printf("LAPI Interrupt Mode             = %s\n",lapi_mode);
	printf("LAPI Max Data Size              = %d\n",max_data_sz);
	printf("LAPI Max UHeader Size           = %d\n",max_uhdr_sz);
	printf("LAPI Error Check                = %d\n",error_check);
	printf("LAPI Timeout                    = %d\n",timeout);
	printf("LAPI Min Timeout                = %d\n",min_timeout);
	printf("LAPI Max Timeout                = %d\n",max_timeout);
	printf("LAPI Max Ports                  = %d\n",max_ports);
	printf("LAPI Max Packet Size            = %d\n",max_pkt_size);
	printf("LAPI Num Retransmission Buffers = %d\n",num_rex_bufs);
	printf("LAPI Retransmission Buffer Size = %d\n",rex_buf_size);
	printf("LAPI Num Address Entries        = %d\n",addr_tbl_size);
    }
	

    AMLAPI_Barrier();

    /* prime the pump */
    dt = Pingpong(srcmem,10,5);

    /* Run the test */
    for (size = FIRSTSZ; !DONESZ(size); size = NEXTSZ(size)) {
	dt = Pingpong(srcmem,size,num_iter);
	if (my_id == 0) {
	    printf("P%i-P%i: size=%8i bytes, latency= %9.3f us, bandwidth= %9.3f MB/sec\n",
		   my_id,peer_id,size,( dt / (double)num_iter ),
		   ((double)size)*num_iter*mb_factor/dt );
	}
    }

    fflush(stdout);

    AMLAPI_Barrier ();
    AM_Terminate();

    return 0;
}

/* -----------------------------------------------------------
 * Pingpong function times num_iter iterations of:
 * - Sending size bytes to the remote process
 * - receiving the ACK from the remote process
 * -----------------------------------------------------------
 */
double Pingpong(char* srcmem, int size, int niter)
{
    int64_t start, stop;
    int     iter;
    int     max_med = AM_MaxMedium();

    done = 0;

    AMLAPI_Barrier ();
    start = getMicrosecondTimeStamp();

    for (iter = 1; iter <= niter; iter++) {

	if (my_id == 0) {
	    if (size <= max_med) {
		AM_Safe(AM_RequestI4(ep, peer_id, BULK_REQ_HANDLER,
				     srcmem, size, 666, 666, 666, 666));     
	    } else {
		AM_Safe(AM_RequestXfer4(ep, peer_id, 100, BULK_REQ_HANDLER,
					srcmem, size, 666, 666, 666, 666));
	    }
	}

	/* wait until ack is received */
	if (polling) {
	    while( done < iter ) {
		AM_Poll(eb);
	    }
	} else {
	    while ( done < iter ) {
		AM_Safe(AM_SetEventMask(eb, AM_NOTEMPTY)); 
		AM_Safe(AM_WaitSema(eb));
		AM_Poll(eb);
	    }
	}

    }
    stop =  getMicrosecondTimeStamp();
    AMLAPI_Barrier ();

    return (double)(stop-start);
}

/* -----------------------------------------------------------
 * next_size:
 * Function to determine the sequence of message sizes.
 * The increment between points grows as the message size
 * increases.
 * -----------------------------------------------------------
 */
#define round(a, b) ((a) -((a)%(b)))

int next_size(int size)
{
   int delta = 0;
   double dsize = size;

   /* for small sizes, return fixed increments */
   /* choose not quite power of two increments so that we don't
    * always end up on some nice boundary
    * choose multiple of four because it will be rare to send smaller
    * amounts (bytes or chars or shorts).
    */
   if (size < 4)         delta = 1;
   else if (size < 16)   delta = 4;
   else if (size < 128)  delta = 22;
   else if (size < 1000) delta = 50;
   else if (size < 8192) delta = 300;
   else {
     dsize *= 1.2;
     delta = dsize - size;
   }

   delta = round(delta, 1);

   return(size+delta);
}

/* -----------------------------------------------------------
 * 
 * -----------------------------------------------------------
 */

/* -----------------------------------------------------------
 * 
 * -----------------------------------------------------------
 */
