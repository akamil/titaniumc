#!/usr/bin/perl

use Getopt::Std;
use IO::File;

getopts('dvr:p:');
$debug = $opt_d;
$verbose = $opt_v;
$root = "O_P2P";
$root = $opt_r;
$prefix = "X";

$pp_file = $ARGV[0];
if (! -f $pp_file) {
    die "Cant find file :$pp_file:";
}
if ($pp_file =~ /\.(\d+)$/) {
    $prefix = $1;
}
$prefix = $opt_p if $opt_p;

$F = new IO::File "< $pp_file";
die "Unable to open $pp_file" if (! $F);

$depth = -1;
$outfile="";
undef $O;
$op = "UNKNOWN OPERATION";
$mode = "UNKNOWN MODE";
undef $LAT;
undef $BW;

$latfile = $prefix . "_amlapi_lat";
$LAT = new IO::File "> $latfile";
die "Unable to create :$latfile:"if (! $LAT);
print $LAT "\#set terminal postscript landscape color solid \"Times-Roman\" 14\n";
print $LAT "\#set size 1.0, 1.0\n";
print $LAT "\#set output \"${latfile}.ps\"\n";
print $LAT "set grid\n";
print $LAT "set title \"AMLAPI Latency: Ping + Ack\"\n";
print $LAT "set key left top\n";
print $LAT "\#set key inside\n";
print $LAT "set xrange [0:1500]\n";
print $LAT "set xlabel \"Message Size (bytes)\"\n";
print $LAT "set ylabel \"Latency (usec)\"\n";
print $LAT "plot \\\n";

$bw_file = $prefix . "_amlapi_bw";
$BW = new IO::File "> $bw_file";
die "Unable to create :$bw_file:"if (! $BW);
print $BW "\#set terminal postscript landscape color solid \"Times-Roman\" 14\n";
print $BW "\#set size 1.0, 1.0\n";
print $BW "\#set output \"${bw_file}.ps\"\n";
print $BW "set grid\n";
print $BW "set title \"AMLAPI Bandwidth: Ping + Ack\"\n";
print $BW "set key left top\n";
print $BW "\#set key inside\n";
print $BW "set xlabel \"Message Size (bytes)\"\n";
print $BW "set ylabel \"Bandwidth (MB/sec)\"\n";
print $BW "plot \\\n";
$first = 1;


# now, parse the file
while (<$F>) {
    # strip leading blanks
    s/^\s+//;
    # skip blank lines
    next if /^$/;
    if (/^AM mode\s*\=\s*(\S+)/) {
	$am_mode = $1;
    }
    if (/^LAPI Interrupt Mode\s*\=\s*(\S+)/) {
	$mode = $1;
	print "Found pingpong in AM $am_mode and LAPI $mode mode\n" if $verbose;
	if ($O) {
	    # close previous outfile
	    undef $O;
	}
	if ($am_mode =~ /^polling/) {
	    $am_mode = "AM_Poll";
	} else {
	    $am_mode = "AM_Wait";
	}
	if ($mode =~ /POLLING/) {
	    $mode = "LAPI_Poll";
	} else {
	    $mode = "LAPI_Intr";
	}
	$outfile = sprintf("%s_%s_%s",$prefix,$am_mode,$mode);
	$label = sprintf("%s\/%s",$am_mode,$mode);
	
	$O = new IO::File "> $outfile";
	die "Unable to create :$outfile:"if (! $O);

	print "Opened outfile = $outfile\n" if $verbose;
	if ($first) {
	    $first = 0;
	} else {
	    print $BW  ", \\\n";
	    print $LAT ", \\\n";
	}
	print $LAT "\"$outfile\"  using 1:2 title \"$label\" with linespoints lw 2";
	print $BW "\"$outfile\"  using 1:3 title \"$label\" with linespoints lw 2";
    }
    if (/ size\=\s+(\d+)\s+bytes\, latency\=\s*(\S+)\s+us\, bandwidth\=\s*(\S+)/) {
	$size = $1;
	$lat  = $2;
	$bw   = $3;
	printf $O "%-6d  %8.3f  %12.3f\n",$size,$lat,$bw;
    }
}
undef $O;
undef $F;
print $LAT "\n";
undef $LAT;
print $BW "\n";
undef $BW;

exit 0;

    

