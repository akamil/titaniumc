#include "amlapi_uheader.h"

/* These get defined in AM_Init() */
int amlapi_max_uhdr_size = -1;
int amlapi_max_stuffed = -1;

/* Functions related to uheader */
char AMLAPI_checkHeaderType (int type) {
  return (
	  type == AMLAPI_UHEADER_PLAIN ||
	  type == AMLAPI_UHEADER_I ||
	  type == AMLAPI_UHEADER_XFER ||
	  type == AMLAPI_UHEADER_XFERASYNC
	  );
}
