#ifndef _AMLAPI_H_
#define _AMLAPI_H_

#include <lapi.h>
#include "amlapi_endpoint.h"
#include "amlapi_const.h"

/* miscellaneous macro helpers */
#define _STRINGIFY_HELPER(x) #x
#define _STRINGIFY(x) _STRINGIFY_HELPER(x)

#define AMLAPI_LIBRARY_VERSION      1.4
#define AMLAPI_LIBRARY_VERSION_STR  _STRINGIFY(AMLAPI_LIBRARY_VERSION)

/* From AMLAPI_util */
int AMLAPI_MyProc ();
int AMLAPI_NumProcs ();
lapi_handle_t * AMLAPI_GetLAPIHandle ();
lapi_info_t * AMLAPI_GetLAPIInfo ();
void AMLAPI_Barrier ();

/* From AMLAPI_Endpoint */
int AM_AllocateBundle (int type, eb_t * endb);
int AM_AllocateEndpoint (eb_t bundle, ep_t *endp, en_t * endpoint_name);
int AM_Map (ep_t ea, int index, en_t endpoint, tag_t tag);
int AM_MapAny (ep_t ea, int *index, en_t endpoint, tag_t tag);
int AM_Unmap (ep_t ea, int index);
int AM_FreeEndpoint (ep_t ea);
int AM_FreeBundle (eb_t bundle);
int AM_MoveEndpoint (ep_t ea, eb_t from_bundle, eb_t to_bundle);
int AM_SetExpectedResources (ep_t ea, int n_endpoints, int n_outstanding_requests);
int AM_SetTag (ep_t ea, tag_t tag);
int AM_GetTag (ep_t ea, tag_t* tag);
int AM_GetTranslationName (ep_t ea, int i , en_t * gan);
int AM_GetTranslationTag (ep_t ea, int i, tag_t * tag);
int AM_GetTranslationInuse (ep_t ea, int i);
int AM_MaxNumTranslations (int * ntrans);
int AM_GetNumTranslations (ep_t ea, int * ntrans);
int AM_SetNumTranslations (ep_t ea, int ntrans);
int AM_GetSourceEndpoint (void * token, en_t *gan);
int AM_GetDestEndpoint (void * token, en_t *gan);
int AM_GetMsgTag (void * token, tag_t * tag);
int AM_SetHandler (ep_t ea, handler_t handler, void (*function)());
int AM_SetHandlerAny (ep_t ea, handler_t * handler, void (*function)());
int AM_GetNumHandlers (ep_t ea, int * n_handlers);
int AM_SetNumberHandlers (ep_t ea, int n_handlers);
int AM_MaxNumHandlers ();
int AM_SetSeg (ep_t ea, void * addr, uintptr_t nbytes);
int AM_GetSeg (ep_t ea, void ** addr, uintptr_t * nbytes);
int AM_MaxSegLength (uintptr_t* nbytes);
int AM_GetEventMask (eb_t eb);
int AM_SetEventMask (eb_t eb, int mask);
int AM_WaitSema (eb_t eb);
 
/* From AMLAPI_AM */
void AMLAPI_LAPI_Init ();
int AMLAPI_Set_Mode(AMLAPI_MODE);
int AM_Init ();
int AM_Terminate ();
int AM_Poll (eb_t bundle);
int AM_Request0 (ep_t request_endpoint, int reply_endpoint, handler_t handler);
int AM_Request4 (ep_t request_endpoint, int reply_endpoint, handler_t handler, int arg0, int arg1, int arg2, int arg3);
int AM_Request8 (ep_t request_endpoint, int reply_endpoint, handler_t handler, int arg0, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7);
int AM_RequestI0 (ep_t request_endpoint, int reply_endpoint, handler_t handler, void * source_addr, int nbytes);
int AM_RequestI4 (ep_t request_endpoint, int reply_endpoint, handler_t handler, void * source_addr, int nbytes, int arg0, int arg1, int arg2, int arg3);
int AM_RequestI8 (ep_t request_endpoint, int reply_endpoint, handler_t handler, void * source_addr, int nbytes, int arg0, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7);
int AM_RequestXfer0 (ep_t request_endpoint, int reply_endpoint, uintptr_t dest_offset, handler_t handler, void * source_addr, int nbytes);
int AM_RequestXfer4 (ep_t request_endpoint, int reply_endpoint, uintptr_t dest_offset, handler_t handler, void * source_addr, int nbytes, int arg0, int arg1, int arg2, int arg3);
int AM_RequestXfer8 (ep_t request_endpoint, int reply_endpoint, uintptr_t dest_offset, handler_t handler, void * source_addr, int nbytes, int arg0, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7);
int AM_RequestXferAsync0 (ep_t request_endpoint, int reply_endpoint, uintptr_t dest_offset, handler_t handler, void * source_addr, int nbytes);
int AM_RequestXferAsync4 (ep_t request_endpoint, int reply_endpoint, uintptr_t dest_offset, handler_t handler, void * source_addr, int nbytes, int arg0, int arg1, int arg2, int arg3);
int AM_RequestXferAsync8 (ep_t request_endpoint, int reply_endpoint, uintptr_t dest_offset, handler_t handler, void * source_addr, int nbytes, int arg0, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7);
int AM_Reply0 (void* token, handler_t handler);
int AM_Reply4 (void* token, handler_t handler, int arg0, int arg1, int arg2, int arg3);
int AM_Reply8 (void* token, handler_t handler, int arg0, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7);
int AM_ReplyI0 (void* token, handler_t handler, void * source_addr, int nbytes);
int AM_ReplyI4 (void* token, handler_t handler, void * source_addr, int nbytes, int arg0, int arg1, int arg2, int arg3);
int AM_ReplyI8 (void* token, handler_t handler, void * source_addr, int nbytes, int arg0, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7);
int AM_ReplyXfer0 (void* token, uintptr_t dest_offset, handler_t handler, void * source_addr, int nbytes);
int AM_ReplyXfer4 (void* token, uintptr_t dest_offset, handler_t handler, void * source_addr, int nbytes, int arg0, int arg1, int arg2, int arg3);
int AM_ReplyXfer8 (void* token, uintptr_t dest_offset, handler_t handler, void * source_addr, int nbytes, int arg0, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7);
int AM_GetXfer0 (ep_t request_endpoint, int reply_endpoint, uintptr_t source_offset, handler_t handler, uintptr_t dest_offset, int nbytes);
int AM_GetXfer4 (ep_t request_endpoint, int reply_endpoint, uintptr_t source_offset, handler_t handler, uintptr_t dest_offset, int nbytes, int arg0, int arg1, int arg2, int arg3);
int AM_GetXfer8 (ep_t request_endpoint, int reply_endpoint, uintptr_t source_offset, handler_t handler, uintptr_t dest_offset, int nbytes, int arg0, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7);
int AM_MaxShort ();
int AM_MaxMedium ();
int AM_MaxLong ();

#endif /* _AMLAPI_H_ */
