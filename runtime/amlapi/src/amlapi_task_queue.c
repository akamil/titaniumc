#include "amlapi_endpoint.h"
#include "amlapi_uheader.h"
#include "amlapi_node.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

/* -----------------------------------------------------------------
 * MLW: 12/12/2001
 *
 * The bundle task queue is implemented as a linked list such that
 * a single producer thread and a single consumer thread may access
 * and modify the queue without locking.
 * The queue is a linked list of elements in which items are added
 * at the tail and removed from the region between the head and
 * the tail.
 * In order to insure that the two threads are not modifying
 * the same element, the tail always points to an empty "firewall"
 * element.
 * To add to the queue, a new element is added at the end, the
 * data is copied into the old firewall element, and finally the
 * tail is moved to the new element.  At this point, a memory barrier
 * must be issued to insure the other thread(s) see the update.
 * The queue is empty when head == tail (in this case, both point
 * to the empty firewall element.
 * The consumer thread can remove any element from the list (except
 * the empty tail).
 *
 * Rather than constantly using malloc and free to alloc and dealloc
 * queue elements, a free list is implemented to contain the unused
 * elements.  The free list is also maintained such that a single
 * producer and consumer thread may access and modify the list
 * simultaniously without locking.  When the free list is empty
 * new_tq_element mallocs a single new element and returns it.
 * this element will be added to the free list when it is no longer
 * needed.  Once allocated, elements are never de-allocated, just
 * put on the free list for re-use.  All the elements are deallocated
 * when the bundle is distroyed.
 * ------------------------------------------------------------------
 */

/* memory barrier function */
#if defined(_POWER) || defined(_POWERPC)
#ifdef __GNUC__
void memory_sync(void) {
  asm volatile ("sync" : : : "memory");
}
#else
/* VisualAge C compiler (mpcc_r) has no support for inline symbolic assembly
 * you have to hard-code the opcodes in a pragma that defines an assembly
 * function - see /usr/include/sys/atomic_op.h on AIX for examples
 * opcodes can be aquired by placing the mnemonics in inline.s and running:
 * as -sinline.lst inline.s
 */
#pragma mc_func _do_sync { \
 "7c0004ac" /* sync (same opcode used for dcs)*/ \
}
void memory_sync(void) {
 _do_sync();
}
#endif
#else
void memory_sync(void) {}
#endif

/* allocate as many as num elements to the free list
 * of this task queue
 */
int alloc_initial_tq_elements(eb_t eb, int num) {
    int cnt = 0;
    qe_t *x;

    /* Is this the first time we are allocating elements?
     * If so, allocate the intitial element and set the
     * head and tail pointers.  After this, the list
     * will ALWAYS have an element on it.
     * The ability to have a producer and consumer thread
     * operate on the list without locking requires this
     * one "firewall" element.
     */
    assert(eb->free_tail == NULL);
    assert(eb->free_head == NULL);
    x = (qe_t*)malloc(sizeof(qe_t));
    if (x == NULL) {
	fprintf(stderr,"FATAL: cant alloc intial queue element\n");
	exit(1);
    }
    memset(x,(char)0,sizeof(qe_t));
    eb->free_head = eb->free_tail = x;
    cnt = 1;

    /* Now we can alloc remainder of list */
    for (; cnt < num; cnt++) {
	x = (qe_t*)malloc(sizeof(qe_t));
	if (x == NULL) {
	    break;
	}
	memset(x,(char)0,sizeof(qe_t));
	eb->free_tail->next = x;
	eb->free_tail = x;
    }
    return cnt;
}

void free_bundle_task_queue(eb_t eb) {
    qe_t *x = eb->tq_head;

    /* both lists must always contain a "firewall" element */
    assert(eb->tq_tail != NULL);
    assert(eb->free_tail != NULL);
    
    /* trash any elements on the task queue */
    while (x != eb->tq_tail) {
	eb->tq_head = x->next;
	free(x);
	x = eb->tq_head;
    }
    free(eb->tq_head);
    eb->tq_head = eb->tq_tail = NULL;

    /* now trash any elements on the free list */
    x = eb->free_head;
    while (x != eb->free_tail) {
	eb->free_head = x->next;
	free(x);
	x = eb->free_head;
    }
    free(eb->free_head);
    eb->free_head = eb->free_tail = NULL;
}

/* pull an element off the task queue free list and return */
qe_t* new_tq_element(eb_t eb) {
    qe_t *x = NULL;
    
    if (eb->free_head == eb->free_tail) {
	/* empty free list
	 * We must malloc a single element and return.
	 * we cannot alloc more and add to the free list because
	 * there could be contention with second thread adding
	 * an element back to the list.
	 */
	x = (qe_t*)malloc(sizeof(qe_t));
	if (x == NULL) {
	    fprintf(stderr,"FATAL: cant alloc additional queue element\n");
	    exit(1);
	}
	memset(x,(char)0,sizeof(qe_t));
	return x;
    }
    assert(eb->free_head != NULL);
    x = eb->free_head;
    eb->free_head = x->next;
    memset(x,(char)0,sizeof(qe_t));
    return x;
}
    
/* put element back on task queue free list */
void free_tq_element(eb_t eb, qe_t *x) {
    assert(eb->free_tail != NULL);
    eb->free_tail->next = x;
    /* need to insure this link is committed before
     * we update the tail pointer.  without this,
     * for memory systems with out-of-order writes,
     * this would cause an inconstant data structure.
     */
    memory_sync();
    eb->free_tail = x;
}


/* #define VERBOSE 1 */

#define TQ_LARGE_INDEX 1000000

/* This is called by the task queue producer: the LAPI header_handler.
 * We are guaranteed that only one header_handler executes at a time
 * so we may add an element without locking the queue.
 * Entry is added at queue tail.
 * Return the queue element that holds the data.  It will be
 * passed to the completion handler to mark as "ready" when
 * all the message data arrives.
 */
qe_t* AMLAPI_ScheduleTaskOnBundle (eb_t bundle, amlapi_handler_t handler,
				   struct amlapi_token* token, void* user_info, int nbytes,
				   int numArgs, int* argv, int type,
				   int is_ready)
{
    qe_t *entry, *tail;
    
    if ( (entry = new_tq_element(bundle)) == NULL) {
	/* oops, cant get another entry.  Fail */
	return NULL;
    }
    /* current tail entry is always empty.  Use it.
     * Note that we do not have to lock the queue because the
     * queue consumer never touches this entry.
     */
    assert(bundle->tq_tail != NULL);
    tail = bundle->tq_tail;
    tail->handler   = handler;
    memcpy(&(tail->token), token, sizeof(struct amlapi_token));
    tail->user_info = user_info;
    tail->nbytes    = nbytes;
    tail->numArgs   = numArgs;
    memcpy(&(tail->argv[0]), argv, sizeof(int)*numArgs);
    tail->type      = type;
    tail->ready     = is_ready;
    tail->next = entry;
    /* need memory barrier here, to insure data is not
     * written out-of-order.  Must do this before
     * advancing the tail pointer.
     */
    memory_sync();
    bundle->tq_tail = entry;
    return tail;
}

/* This is the consumer of the task queue elements.  Since we
 * are assuming AM_Seq, only one thread may execute in the
 * bundle at a time.  Thus, we can comsume elements without
 * having to lock against other consumers.
 * We also do not have to lock against the (single) producer
 * because it adds elements to the tail of the queue.
 * The entry pointed to by the tail is empty, and thus
 * never accessed by the consumer.
 */
   
/* returns FALSE if didn't execute any task, TRUE if executed a task */
char AMLAPI_ExecuteTaskFromBundle (eb_t bundle) {
    amlapi_handler_t handler;
    void* token;
    void* user_info;
    int nbytes;
    int numArgs;
    int* argv;
    int type;
    qe_t *x;
    qe_t *prev = NULL;
    int found = 0;

    x = bundle->tq_head;
    while (x != bundle->tq_tail) {
	if (x->ready) {
	    /* this is the guy we want */
	    found = 1;
	    if (prev == NULL) {
		bundle->tq_head = x->next;
	    } else {
		prev->next = x->next;
	    }
	    break;
	}
	prev = x;
	x = x->next;
    }
    if (! found ) {
	return FALSE;
    }
    type      = x->type;
    handler   = x->handler;
    numArgs   = x->numArgs;
    nbytes    = x->nbytes;
    argv      = &(x->argv[0]);
    token     = (void *)&(x->token);
    user_info = x->user_info;

    switch (type) {
    case AMLAPI_UHEADER_PLAIN:
	switch (numArgs) {
	case 0:
	    handler(token);
	    break;
	case 4:
	    handler(token, argv[0], argv[1], argv[2], argv[3]);
	    break;
	case 8:
	    handler(token, argv[0], argv[1], argv[2], argv[3],
		    argv[4], argv[5], argv[6], argv[7]);
	    break;
	default:
            printf("BARF!!!!! Unknown numArgs=%i in AMLAPI_ExecuteTaskFromBundle\n",numArgs);fflush(stdout);abort();
	    break;
	}
	break;
	
    case AMLAPI_UHEADER_I:
	switch (numArgs) {
	case 0:
	    handler(token, user_info, nbytes);
	    break;
	case 4:
	    handler(token, user_info, nbytes,
		    argv[0], argv[1], argv[2], argv[3]);
	    break;
	case 8:
	    handler(token, user_info, nbytes,
		    argv[0], argv[1], argv[2], argv[3],
		    argv[4], argv[5], argv[6], argv[7]);
	    break;
	default:
            printf("BARF!!!!! Unknown numArgs=%i in AMLAPI_ExecuteTaskFromBundle\n",numArgs);fflush(stdout);abort();
	    break;
	}
	break;
	
    case AMLAPI_UHEADER_XFER:
	switch (numArgs) {
	case 0:
	    handler(token, user_info, nbytes);
	    break;
	case 4:
	    handler(token, user_info, nbytes,
		    argv[0], argv[1], argv[2], argv[3]);
	    break;
	case 8:
	    handler(token, user_info, nbytes,
		    argv[0], argv[1], argv[2], argv[3],
		    argv[4], argv[5], argv[6], argv[7]);
	    break;
	default:
            printf("BARF!!!!! Unknown numArgs=%i in AMLAPI_ExecuteTaskFromBundle\n",numArgs);fflush(stdout);abort();
	    break;
	}
	break;
	
    case AMLAPI_UHEADER_XFERASYNC:
	switch (numArgs) {
	case 0:
	    handler(token, user_info, nbytes);
	    break;
	case 4:
	    handler(token, user_info, nbytes,
		    argv[0], argv[1], argv[2], argv[3]);
	    break;
	case 8:
	    handler(token, user_info, nbytes,
		    argv[0], argv[1], argv[2], argv[3],
		    argv[4], argv[5], argv[6], argv[7]);
	    break;
	default:
            printf("BARF!!!!! Unknown numArgs=%i in AMLAPI_ExecuteTaskFromBundle\n",numArgs);fflush(stdout);abort();
	    break;
	}
	break;
	
    default:
        printf("BARF!!!!! Unknown type=%i in AMLAPI_ExecuteTaskFromBundle\n",type);fflush(stdout);abort();
	break;
    }

    if (type == AMLAPI_UHEADER_I) {
	/* we allocated this space in the header handler */
	/* free (user_info); */
	med_buf_free(user_info);
    }
    free_tq_element(bundle,x);
    return TRUE;
}

