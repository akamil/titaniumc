#ifndef _INCLUDE_TLIB_NATIVE_FILE_UTILS_H_
#define _INCLUDE_TLIB_NATIVE_FILE_UTILS_H_

#include "layout!PT14FileDescriptor2io4java.h"
#include "layout!PT6String4lang4java.h"
#include "layout!PTAjbyte.h"
#include <unistd.h>

#ifndef STDIN_FILENO
  #define STDIN_FILENO 0
#endif
#ifndef STDOUT_FILENO
  #define STDOUT_FILENO 1
#endif
#ifndef STDERR_FILENO
  #define STDERR_FILENO 2
#endif

int getFd( const PT14FileDescriptor2io4java );

void jopen( const PT14FileDescriptor2io4java, const PT6String4lang4java, int );
void jclose( const PT14FileDescriptor2io4java );
jint jread( const PT14FileDescriptor2io4java );
jint jreadBytes( const PT14FileDescriptor2io4java, PTAjbyte, jint, jint );
void jwrite( const PT14FileDescriptor2io4java, char );
void jwriteBytes( const PT14FileDescriptor2io4java, PTAjbyte, jint, jint );
jlong jseek( const PT14FileDescriptor2io4java, jlong, int );

void tossIOException() __attribute__((noreturn));
void tossIOException_str(char * const string) __attribute__((noreturn));


#endif
