#ifndef _INCLUDE_INTERFACE_HEADER_H_
#define _INCLUDE_INTERFACE_HEADER_H_

#include "backend.h"
#include "common_header.h"
#include "T5Class4lang4java.h"
#include "layout!LT6String4lang4java.h"


typedef struct interface_header
{
    T5Class4lang4java class_object; /* MUST come first */
    LT6String4lang4java class_name;

    enum TypeCategory category;
    enum TypeFlags flags;
    jchar index;
    const struct interface_header * const *implements;
} interface_header;


#endif
