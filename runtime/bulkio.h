#ifndef _include_bulkio_h_
#define _include_bulkio_h_

#include "ti_config.h"

/*  configuration parameters */
#define DEBUG_BULKIO 0  /*  produce debugging output */
#define PORTABLE_ONDISK_REPRESENTATION  1  /*  perform byte-swap on little endian platforms  */

/*  debugging output */
#if DEBUG_BULKIO
  #define DEBUGSTREAM stdout
  #define DEBUGHDR() { \
    jint thisproc = m8thisProcmT2Ti4lang2ti();            \
    jint numprocs = m8numProcsmT2Ti4lang2ti();            \
    fprintf(DEBUGSTREAM, "%i/%i: ", thisproc, numprocs);  \
    }
  #define DEBUGMSG(s) \
    do { DEBUGHDR(); fprintf(DEBUGSTREAM, s ## "\n"); fflush(DEBUGSTREAM); } while(0)
  #define DEBUGMSG1(s, a1) \
    do { DEBUGHDR(); fprintf(DEBUGSTREAM, s ## "\n",a1); fflush(DEBUGSTREAM); } while(0)
  #define DEBUGMSG2(s, a1, a2)  \
    do { DEBUGHDR(); fprintf(DEBUGSTREAM, s ## "\n",a1,a2); fflush(DEBUGSTREAM); } while(0)
  #define DEBUGMSG3(s, a1, a2, a3) \
    do { DEBUGHDR(); fprintf(DEBUGSTREAM, s ## "\n",a1,a2,a3); fflush(DEBUGSTREAM); } while(0)
#else
  #define DEBUGMSG(s) 
  #define DEBUGMSG1(s, a1) 
  #define DEBUGMSG2(s, a1, a2) 
  #define DEBUGMSG3(s, a1, a2, a3) 
#endif

#endif

