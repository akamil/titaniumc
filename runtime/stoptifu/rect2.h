#ifndef _STOPTIFU_RECT2_H_
#define _STOPTIFU_RECT2_H_

typedef struct { int l0, l1, h0, h1; } rect2;

/*
extern rect2 rect2_compute_alpha(int n, const int *p, const int * const *v,
				 const int * const *inv, int k, int **r,
				 int dbg);
*/

#define rect2_copy(x) (x)

TI_INLINE(rect2_intersection) rect2 rect2_intersection(rect2 r, rect2 s)
{
  if (s.l0 > r.l0)
    r.l0 = s.l0;
  if (s.h0 < r.h0)
    r.h0 = s.h0;
  if (s.l1 > r.l1)
    r.l1 = s.l1;
  if (s.h1 < r.h1)
    r.h1 = s.h1;
  return r;
}

/* bounding box of union */
TI_INLINE(rect2_rect2_approximate_union) rect2 rect2_rect2_approximate_union(rect2 r, rect2 s)
{
  if (s.l0 < r.l0)
    r.l0 = s.l0;
  if (s.h0 > r.h0)
    r.h0 = s.h0;
  if (s.l1 < r.l1)
    r.l1 = s.l1;
  if (s.h1 > r.h1)
    r.h1 = s.h1;
  return r;
}

TI_INLINE(rect2_is_empty) bool rect2_is_empty(rect2 r)
{
  return ((r.l0 > r.h0) || (r.l1 > r.h1));
}

TI_INLINE(rect2_contains1) bool rect2_contains1(rect2 r, int x)
{
  return r.l0 <= x && x <= r.h0;
}

TI_INLINE(rect2_to_ivseti) ivseti *rect2_to_ivseti(rect2 r)
{
  if (rect2_is_empty(r))
    return ivseti_emptyset();
  return interval_cross_ivseti(make_iinterval(r.l0, r.h0), ivseti_single_interval(make_iinterval(r.l1, r.h1)));
}

/* Return bounding box. */
TI_INLINE(ivseti_to_rect2) rect2 ivseti_to_rect2(ivseti *s)
{
  rect2 result;
  ivseti_range(s, 0, &result.l0, &result.h0);
  ivseti_range(s, 1, &result.l1, &result.h1);
  return result;
}

TI_INLINE(ivseti_rect2_union) ivseti *ivseti_rect2_union(ivseti *r, rect2 s)
{
  return ivseti_union(r, rect2_to_ivseti(s));
}

TI_INLINE(ivseti_rect2_intersection) ivseti *ivseti_rect2_intersection(ivseti *r, rect2 s)
{
  return ivseti_intersection(r, rect2_to_ivseti(s));
}

/* bounding box of union */
TI_INLINE(rect2_ivseti_approximate_union) rect2 rect2_ivseti_approximate_union(rect2 r, ivseti *s)
{
  return rect2_rect2_approximate_union(r, ivseti_to_rect2(s));
}

TI_INLINE(rect2_is_shifted_subset) bool rect2_is_shifted_subset(rect2 s, rect2 t, int d0, int d1)
{
  return (range_superset(t.l0, t.h0, s.l0 + d0, s.h0 + d0) &&
	  range_superset(t.l1, t.h1, s.l1 + d1, s.h1 + d1)); 
}

/* is s shifted by some vector a subset of t? */
TI_INLINE(rect2_ivseti_is_shifted_subset) bool rect2_ivseti_is_shifted_subset(rect2 s, ivseti *t, int d0, int d1)
{
  return ivseti_is_shifted_subset2(rect2_to_ivseti(s), t, d0, d1);
}

/* is s shifted by some vector a subset of t? */
TI_INLINE(ivseti_rect2_is_shifted_subset) bool ivseti_rect2_is_shifted_subset(ivseti *s, rect2 t, int d0, int d1)
{
  return rect2_is_shifted_subset(ivseti_to_rect2(s), t, d0, d1);
}

/* Caller should free the result. */
#define rect2_to_string_body						\
{									\
  char *q = ALLOC(char, 200);						\
  sprintf(q, "{ (%d, %d) - (%d, %d) }", r.l0, r.l1, r.h0, r.h1);	\
  return q;								\
}

TI_INLINE(rect2_to_string) char *rect2_to_string(rect2 r)
rect2_to_string_body
#undef rect2_to_string_body

#endif
