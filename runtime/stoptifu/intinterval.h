#ifndef _INT_INTERVAL_H_
#define _INT_INTERVAL_H_

/* Provides intervals of integers (type iinterval).
   Fields lo and hi have the obvious meanings.  Functions provided:

     create_iinterval
     include_in_iinterval
     divide_iinterval
     multiply_iinterval
     shift_iinterval
     divide_int_rounding_to_minus_inf
     divide_int_rounding_to_plus_inf
*/

#include "alloc.h"
#include "divide-int-with-rounding.h"

#define T int
#define INCLUDE_INTERVAL_DIVISION 1
#define divide_rounding_to_minus_inf divide_int_rounding_to_minus_inf
#define divide_rounding_to_plus_inf divide_int_rounding_to_plus_inf
#define interval iinterval
#define include_in_interval include_in_iinterval
#define divide_interval divide_iinterval
#define divide_rounding_to_minus_inf divide_int_rounding_to_minus_inf
#define divide_rounding_to_plus_inf divide_int_rounding_to_plus_inf
#define multiply_interval multiply_iinterval
#define shift_interval shift_iinterval
#define containg_interval containg_iinterval
#define intersect_intervals intersect_iintervals
#define copy_interval copy_iinterval
#define make_interval make_iinterval
#define is_empty is_empty_iinterval

#include "interval.h"

#undef T
#undef INCLUDE_INTERVAL_DIVISION
#undef divide_rounding_to_minus_inf
#undef divide_rounding_to_plus_inf
#undef interval
#undef include_in_interval
#undef divide_interval
#undef divide_rounding_to_minus_inf
#undef divide_rounding_to_plus_inf
#undef multiply_interval
#undef shift_interval
#undef containg_interval
#undef intersect_intervals
#undef copy_interval
#undef make_interval
#undef is_empty

#endif
