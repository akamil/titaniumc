#include "iseti.h"

#define P(s) do { iseti_print(s); putchar('\n'); } while (0)

main()
{
  int i;
  iseti x = iseti_singleton(3), y = iseti_singleton(5), z;
  P(x);
  P(y);
  x = iseti_union(x, y);
  P(x);
  x = iseti_union(x, iseti_singleton(4));
  P(x);

  for (i = 0; i < 10; i++)
    x = iseti_union(x, iseti_singleton(i * i));
  P(x);

  for (i = 10; i < 20; i++)
    x = iseti_union(x, iseti_single_interval(make_iinterval(i * 3, i * 4)));
  P(x);

  for (i = 20; i < 2000; i = (int) i * 2.3)
    x = iseti_union(x, iseti_single_interval(make_iinterval(i, i * 2 + 3)));
  P(x);
    
  for (i = 4; i < 1000; i = (int) i * 3.1)
    y = iseti_union(y, iseti_single_interval(make_iinterval(i, i * 3 + 1)));
  P(y);

  z = iseti_intersection(x, y);
  P(z);

  z = iseti_intersection(z, z);
  P(z);

  x = iseti_emptyset();
  for (i = 0; i < 20; i++)
    x = iseti_union(x, iseti_single_interval
		    (shift_iinterval(singleton_iinterval(i * 50), -10)));
  z = iseti_intersection(z, x);
  P(z);

  exit(0);
}
