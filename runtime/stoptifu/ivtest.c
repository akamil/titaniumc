#include "ivseti.h"

#define P(s) 					\
do {						\
  ivseti *q = (s);				\
  ivseti_print(q);				\
  if (ivseti_is_empty(q))			\
    putchar('E');				\
  putchar('\n');				\
} while (0)

#define S(x) ivseti_singleton((x))

main()
{
  int a, b, c;
  ivseti *i, *j, *k, *l;

  puts("test 1d sets");

  i = ivseti_singleton(5);
  P(i);

  j = ivseti_singleton(8);
  P(j);

  i = ivseti_union(i, j);
  P(i);

  i = ivseti_union(i, ivseti_singleton(6));
  i = ivseti_union(i, ivseti_singleton(7));
  i = ivseti_union(i, ivseti_singleton(10));
  P(i);

  i = ivseti_intersection(i, j);
  P(i);

  for (a = -10; a < 10; a++)
    i = ivseti_union(i, S(a * 4));
  for (a = -10; a < 10; a++)
    i = ivseti_union(i, S(a * 4 + 1));
  for (a = -10; a < 10; a++)
    i = ivseti_union(i, S(a * 4 + 2));
  P(i);
  
  j = ivseti_emptyset();
  for (a = 0; a < 7; a++)
    j = ivseti_union(j, ivseti_single_interval
		     (make_iinterval(a * a - 20, a * a + a - 20)));
  P(j);

  i = ivseti_intersection(i, j);
  P(i);

  k = ivseti_emptyset();
  for (a = -10; a < 10; a++)
    k = ivseti_union(k, S(a * 4 + 3));

  i = ivseti_intersection(i, k);
  P(i);
  P(j);
  P(k);

  puts("test cross product");

  k = ivseti_cons(3, j);
  P(k);

  j = iseti_cross_ivseti(ivseti_to_iseti(j), k);
  P(j);
  /* P(ivseti_copy(j)); */

  puts("more testing");

  i = ivseti_emptyset();
  i = ivseti_cons(6, i);
  i = ivseti_cons(3, i);
  k = ivseti_emptyset();
  k = ivseti_cons(7, k);
  k = ivseti_cons(3, k);
  P(i);
  P(k);
  i = ivseti_union(i, k);
  i = ivseti_cons(5, i);
  P(i);
  l = ivseti_copy(i);
  i = ivseti_intersection(i, j);
  P(i);
  P(l);

  exit(0);
}
