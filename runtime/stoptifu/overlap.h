/* Return false iff the two specified sets of ints do not overlap.
   May return false positives. */
TI_INLINE(may_overlap) bool
may_overlap(jIntPointer x, int xn, jint *xd0, jint *x1, jint *xds,
	    jint *xs, jint *xbase, int *xsideFactors,
	    jIntPointer y, int yn, jint *yd0, jint *y1, jint *yds,
	    jint *ys, jint *ybase, int *ysideFactors)
{
  return true;
}
