typedef struct {
  key k; value v;
} pair;

TI_INLINE(make_pair) pair make_pair(key k, value v)
{ pair p; p.k = k; p.v = v; return p; }

