#ifndef _STOPTIFU_RUNTIME_H_
#define _STOPTIFU_RUNTIME_H_

#include <runtime-options-var.h>
#include <ti-gc.h>

/****************************************************************************
 * Create macros for using alloca, if available.
 ****************************************************************************/

#if defined(USE_RUNTIME_VARARRAY) && !defined(FORCE_NO_RUNTIME_VARARRAY) && HAVE_ALLOCA_H && ALLOCA_IN_C
#define STOPTIFU_VARARRAY_CREATE(type, var, size) \
    do { var = (type *) alloca(sizeof(type)*(size)); } while(0)
#define STOPTIFU_VARARRAY_CREATE_(casttype, type, var, size) \
    do { var = (casttype *) alloca(sizeof(type)*(size)); } while(0)
#define STOPTIFU_VARARRAY_DESTROY(var) 
#else /* USE_RUNTIME_VARARRAY && !FORCE_NO_RUNTIME_VARARRAY */
#define STOPTIFU_VARARRAY_CREATE(type, var, size) \
    do { var = (type *) ti_malloc(sizeof(type)*(size)); } while(0)
#define STOPTIFU_VARARRAY_CREATE_(casttype, type, var, size) \
    do { var = (casttype *) ti_malloc(sizeof(type)*(size)); } while(0)
#define STOPTIFU_VARARRAY_DESTROY(var) ti_free(var)
#endif /* USE_RUNTIME_VARARRAY && !FORCE_NO_RUNTIME_VARARRAY */

/* broadcast of a local pointer */

#define STOPTIFU_BCAST_LP(result, v, from, T)				\
do {									\
  typedef T stoptifu_bcast_type;					\
  int stoptifu_from = (from);						\
  BROADCAST_BEGIN(stoptifu_bcast_type, stoptifu_from);			\
  BROADCAST_bulk((result), stoptifu_bcast_type, stoptifu_from, (v));	\
  BROADCAST_END((result), stoptifu_bcast_type, stoptifu_from);		\
} while(0)

/****************************************************************************
 * Manipulating parstruct_array (a variable created by tc, whose type is int**)
 * Also, some synchronization primitives and other utilities.
 ****************************************************************************/

#define INDEX_PARSTRUCT_ARRAY1(elt, lo, eltsize, offset) \
  (parstruct_array[((elt) - (lo)) * (eltsize) + (offset)])
#define DEREF_PARSTRUCT_ARRAY1(elt, lo, eltsize, offset) \
  (*INDEX_PARSTRUCT_ARRAY1(elt, lo, eltsize, offset))

#ifndef STOPTIFU_FENCE_PRE_READ
#define STOPTIFU_FENCE_PRE_READ() FENCE_PRE_READ()
#endif

#ifndef STOPTIFU_FENCE_POST_READ
#define STOPTIFU_FENCE_POST_READ() FENCE_POST_READ()
#endif

/* Keep reading *(&(e)) until it is non-zero. */
#ifdef SPIN_LOCK
#undef SPIN_LOCK
#endif
#define SPIN_LOCK(v, e, T)			\
  do { 						\
    T volatile *spin_lock_ptr = &(e);		\
    STOPTIFU_FENCE_PRE_READ();			\
    while ((v = *spin_lock_ptr) == 0);		\
    STOPTIFU_FENCE_POST_READ();			\
  } while (0)

#if XXWAIT
#define SPIN_MSG(format, x, y) \
  do { printf("[%d] ", MYPROC); printf(format, x, y); } while (0) 
#else
#define SPIN_MSG(format, x, y) 
#endif

#ifndef SLEEPY
#define SLEEPY
#endif

/* Keep reading *p until it is at least val. */
/* #define WAIT_UNTIL_AT_LEAST(p, val, T)	do {} while (0) */
#if 0
#define WAIT_UNTIL_AT_LEAST(p, val, T)			\
  do {							\
    T volatile *wait_until_ptr = (p);			\
    T wait_until_val = (val);				\
    STOPTIFU_FENCE_PRE_READ();				\
    while (!(*wait_until_ptr >= wait_until_val));	\
    STOPTIFU_FENCE_POST_READ();				\
  } while (0)
#endif
#define WAIT_UNTIL_AT_LEAST(p, val, T)					\
  do {									\
    T volatile *wait_until_ptr = (p);					\
    T wait_until_val = (val);						\
    while (!(*wait_until_ptr >= wait_until_val)) {			\
      STOPTIFU_FENCE_POST_READ();					\
      SLEEPY;								\
      SPIN_MSG("wait_until_at_least: %d vs %d\n", *wait_until_ptr,	\
	       wait_until_val);						\
      STOPTIFU_FENCE_PRE_READ();					\
    }									\
  } while (0)

#if DEBUG_TEMP_ARRAYS
#define ACCESS_TEMP_ARRAY(name, is_read, base, index)			 \
  (printf("%s %s[%d]\n", (is_read ? "read" : "write"), (name), (index)), \
   (base)[index])
#else
#define ACCESS_TEMP_ARRAY(name, is_read, base, index) ((base)[index])
#endif

#include <limits.h>
#if !defined(MAXINT)
#    define MAXINT INT_MAX
#endif
#include <stoptifu/alloc.h>
#include <stoptifu/ivseti.h>
#include <stoptifu/overlap.h>
#include <stoptifu/rect1.h>
#include <stoptifu/rect2.h>
#include <stoptifu/rect3.h>

#ifndef STOPTIFU_MAXINT
#define STOPTIFU_MAXINT MAXINT
#endif

#ifndef COMPUTE_ALPHA_DEBUG_LEVEL
#define COMPUTE_ALPHA_DEBUG_LEVEL 0
#endif

#ifndef STOPTIFU_STATS
#if DEBUG_TILE_EXECUTION
#define STOPTIFU_STATS 1
#else
#define STOPTIFU_STATS 0
#endif
#endif

#ifndef DEBUG_TILING_SETUP
#if DEBUG_TILE_EXECUTION
#define DEBUG_TILING_SETUP 1
#else
#define DEBUG_TILING_SETUP 0
#endif
#endif

extern ivseti *ivseti_compute_alpha(int n, const int *p, const int * const *v,
				    const int * const *inv, int k, int **r,
				    int dbg);

extern void compute_alpha_1D(int mult, int lo, int hi, int *plo, int *phi);

#endif
