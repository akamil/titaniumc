#ifndef _HASHCODE_INT_
#define _HASHCODE_INT_
TI_INLINE(hashcode_int) int hashcode_int(key k, int N)
{
  return (k >= 0 ? k : -k) % N;
}
#endif
