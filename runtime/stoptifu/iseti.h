#ifndef _ISETI_H_
#define _ISETI_H_

/* Sets of integers represented as ordered lists of intervals

   You may wish to define ALLOC and FREE.  If they are not defined,
   they will be defined by alloc.h to use malloc and free.

   The following types/operations related to intervals are provided:

    iinterval
    singleton_iinterval
    include_in_iinterval
    divide_int_rounding_to_minus_inf
    divide_int_rounding_to_plus_inf
    divide_iinterval
    multiply_iinterval
    shift_iinterval
    containing_iinterval
    intersect_iintervals
    copy_iinterval
    make_iinterval

   The following types/operations related to interval lists are provided:
    iinterval_list
    cons_interval
    iinterval_list_contains
    (and others)

   The following types/operations related to sets are provided:

    iseti
    iseti_print
    iseti_emptyset
    iseti_universe
    iseti_is_empty
    iseti_is_universe
    iseti_min
    iseti_max
    iseti_singleton
    iseti_adjoin
    iseti_share_adjoin
    iseti_destructive_adjoin
    iseti_union
    iseti_destructive_union
    iseti_intersection
    iseti_single_interval
    iseti_copy
    iinterval_abutting_or_overlapping
    iseti_canonicalize
    iseti_equal
    iseti_contains
    (and others?)

*/

#include <stdio.h>
#include "alloc.h"
#include "divide-int-with-rounding.h"

#define  T int
#define  interval iinterval
#define  singleton_interval singleton_iinterval
#define  include_in_interval include_in_iinterval
#define  INCLUDE_INTERVAL_DIVISION 1
#define  divide_rounding_to_minus_inf divide_int_rounding_to_minus_inf
#define  divide_rounding_to_plus_inf divide_int_rounding_to_plus_inf
#define  divide_interval divide_iinterval
#define  multiply_interval multiply_iinterval
#define  shift_interval shift_iinterval
#define  containing_interval containing_iinterval
#define  intersect_intervals intersect_iintervals
#define  copy_interval copy_iinterval
#define  make_interval make_iinterval
#define  interval_contains iinterval_contains
#define  is_empty is_empty_iinterval

#include "interval.h"

#undef  T
#undef  is_empty

#define type int
#define set iseti
#define seti iinterval_list
#define seti_to_string iinterval_list_to_string
#define seti_contains iinterval_list_contains
#define print iseti_print
#define emptyset iseti_emptyset
#define minset iseti_min
#define maxset iseti_max
#define minseti seti_min
#define maxseti seti_max
#define universe iseti_universe
#define is_empty iseti_is_empty
#define is_universe iseti_is_universe
#define singleton iseti_singleton
#define adjoin iseti_adjoin
#define share_adjoin iseti_share_adjoin
#define destructive_adjoin iseti_destructive_adjoin
#define union iseti_union
#define destructive_union iseti_destructive_union
#define destructive_union_helper iseti_destructive_union_helper
#define intersect_lists iseti_intersect_lists
#define intersection iseti_intersection
#define difference iseti_difference
#define single_interval iseti_single_interval
#define copy iseti_copy
#define to_string iseti_to_string
#define MAKE_CANONICAL 1
#define abutting_or_overlapping iinterval_abutting_or_overlapping
#define canonicalize iseti_canonicalize
#define make_bottomopen_interval iseti_make_bottomopen_interval
#define make_topopen_interval iseti_make_topopen_interval
#define diff_lists iseti_diff_lists
#define difference iseti_difference
#define equal iseti_equal
#define contains iseti_contains
#define contains_interval iseti_contains_interval

#include "interval-set.h"

#undef  interval
#undef  singleton_interval
#undef  include_in_interval
#undef  INCLUDE_INTERVAL_DIVISION
#undef  divide_rounding_to_minus_inf
#undef  divide_rounding_to_plus_inf
#undef  divide_interval
#undef  multiply_interval
#undef  shift_interval
#undef  containing_interval
#undef  intersect_intervals
#undef  intersect_lists
#undef  copy_interval
#undef  make_interval
#undef  interval_contains
#undef  make_bottomopen_interval
#undef  make_topopen_interval
#undef  diff_lists
#undef  difference
#undef  equal
#undef  contains
#undef  contains_interval
#undef  seti_contains

#undef set
#undef type 
#undef print 
#undef emptyset 
#undef minset
#undef maxset
#undef universe 
#undef is_empty 
#undef is_universe 
#undef singleton 
#undef adjoin
#undef share_adjoin
#undef destructive_adjoin
#undef union 
#undef destructive_union
#undef destructive_union_helper
#undef intersection 
#undef difference
#undef single_interval 
#undef copy
#undef to_string
#undef MAKE_CANONICAL 
#undef abutting_or_overlapping 
#undef canonicalize 

#define seti_is_empty(x) ((x) == NULL)

#endif /* _ISETI_H_ */
