#ifndef _INT_IVSETI_HASH_H_
#define _INT_IVSETI_HASH_H_

/* This file provides the following types:
   ivpair:  pairs of ints (named k and v)
   ivhash:  hash table from int to int
   ivlist:  list of ivpair

   This file provides the following functions related to ivpair:
    ivmake_pair

   This file provides the following functions related to ivlist:
    ivcons      
    ivhead      
    ivtail      
    ivset_tail  
    ivset_head
    ivlength    
    ivdreverse  
    ivfreecell

   This file provides the following functions related to ivhash:
    ivhashtable           
    ivhashtable_with_size 
    ivdelete_hashtable    
    ivinsert              
    ivdelete              
    ivcontains            
    ivget                 
    ivget_if_present      
*/

#include "alloc.h"

#define key int
#define value struct ivseti *
#define hashcode hashcode_int

#include "hashcode_int.h"

#define make_pair ivmake_pair
#define pair ivpair

#ifndef _IVPAIR_H_
#define _IVPAIR_H_
#include "pair.h"
#endif

#define  T           ivpair      
#define  list        ivlist      
#define  cons        ivcons      
#define  head        ivhead      
#define  tail        ivtail      
#define  set_tail    ivset_tail  
#define  set_head    ivset_head
#define  length      ivlength    
#define  dreverse    ivdreverse
#define  dreverse_and_extend    ivdreverse_and_extend
#define  freecell    ivfreecell

#include "list.h"

#define  hash                  ivhash
#define  hashtable             ivhashtable
#define  hashtable_with_size   ivhashtable_with_size
#define  delete_hashtable      ivdelete_hashtable
#define  insert                ivinsert
#define  delete                ivdelete
#define  contains              ivcontains
#define  get                   ivget
#define  get_if_present        ivget_if_present      

#include "hash.h"

#undef key
#undef value
#undef hashcode
#undef make_pair
#undef pair
#undef T        
#undef list  	  
#undef cons  	  
#undef head  	  
#undef tail  	  
#undef set_tail 
#undef set_head 
#undef length	  
#undef dreverse
#undef dreverse_and_extend
#undef freecell
#undef hash                
#undef hashtable           
#undef hashtable_with_size 
#undef delete_hashtable    
#undef insert              
#undef delete              
#undef contains            
#undef get                 
#undef get_if_present      

#endif
