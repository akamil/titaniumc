#ifndef _STOPTIFU_RECT1_H_
#define _STOPTIFU_RECT1_H_

typedef struct { int l0, h0; } rect1;


/*
extern rect1 rect1_compute_alpha(int n, const int *p, const int * const *v,
				 const int * const *inv, int k, int **r,
				 int dbg);
*/

#define rect1_copy(x) (x)

TI_INLINE(rect1_intersection) rect1 rect1_intersection(rect1 r, rect1 s)
{
  if (s.l0 > r.l0)
    r.l0 = s.l0;
  if (s.h0 < r.h0)
    r.h0 = s.h0;
  return r;
}

/* bounding box of union */
TI_INLINE(rect1_rect1_approximate_union) rect1 rect1_rect1_approximate_union(rect1 r, rect1 s)
{
  if (s.l0 < r.l0)
    r.l0 = s.l0;
  if (s.h0 > r.h0)
    r.h0 = s.h0;
  return r;
}

TI_INLINE(rect1_is_empty) bool rect1_is_empty(rect1 r)
{
  return (r.l0 > r.h0);
}

TI_INLINE(rect1_to_ivseti) ivseti *rect1_to_ivseti(rect1 r)
{
  if (rect1_is_empty(r))
    return ivseti_emptyset();
  return ivseti_single_interval(make_iinterval(r.l0, r.h0));
}

/* Return bounding box. */
TI_INLINE(ivseti_to_rect1) rect1 ivseti_to_rect1(ivseti *s)
{
  rect1 result;
  ivseti_range(s, 0, &result.l0, &result.h0);
  return result;
}

TI_INLINE(ivseti_rect1_union) ivseti *ivseti_rect1_union(ivseti *r, rect1 s)
{
  return ivseti_union(r, rect1_to_ivseti(s));
}

TI_INLINE(ivseti_rect1_intersection) ivseti *ivseti_rect1_intersection(ivseti *r, rect1 s)
{
  return ivseti_intersection(r, rect1_to_ivseti(s));
}

/* bounding box of union */
TI_INLINE(rect1_ivseti_approximate_union) rect1 rect1_ivseti_approximate_union(rect1 r, ivseti *s)
{
  return rect1_rect1_approximate_union(r, ivseti_to_rect1(s));
}

/* Is {rlo, ..., rhi} a superset of {slo, ..., shi} ?
   Assumes rlo <= rhi and slo <= shi. */
TI_INLINE(range_superset) bool range_superset(int rlo, int rhi, int slo, int shi)
{
  return (rlo <= slo && rhi >= shi);
}

TI_INLINE(rect1_is_shifted_subset) bool rect1_is_shifted_subset(rect1 s, rect1 t, int d0)
{
  return range_superset(t.l0, t.h0, s.l0 + d0, s.h0 + d0); 
}

/* is s shifted by some vector a subset of t? */
TI_INLINE(rect1_ivseti_is_shifted_subset) bool rect1_ivseti_is_shifted_subset(rect1 s, ivseti *t, int d0)
{
  return ivseti_is_shifted_subset1(rect1_to_ivseti(s), t, d0);
}

/* is s shifted by some vector a subset of t? */
TI_INLINE(ivseti_rect1_is_shifted_subset) bool ivseti_rect1_is_shifted_subset(ivseti *s, rect1 t, int d0)
{
  return rect1_is_shifted_subset(ivseti_to_rect1(s), t, d0);
}

/* Caller should free the result. */
#define rect1_to_string_body			\
{						\
  char *q = ALLOC(char, 100);			\
  sprintf(q, "{ %d - %d }", r.l0, r.h0);	\
  return q;					\
}

TI_INLINE(rect1_to_string) char *rect1_to_string(rect1 r)
rect1_to_string_body
#undef rect1_to_string_body

#endif
