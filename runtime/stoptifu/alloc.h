#ifndef _ALLOC_H_
#define _ALLOC_H_


#ifndef ALLOC
#define ALLOC(T, n) ((T *) ti_malloc(sizeof(T) * (n)))
#endif

#ifndef FREE
#define FREE(p) ti_free((void *) p)
#endif


#endif
