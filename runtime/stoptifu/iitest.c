#include <stdio.h>
#include <assert.h>
#define RESIZE_RATIO 3.0
#include "intinthash.h"

main()
{
  int i;
  iihash *h = iihashtable();
  for (i = 0; i < 100; i++)
    iiinsert(i * i, i, h);
  for (i = 0; i < 100; i++)
    assert(iiget(i * i, h) == i);
  
  printf("Entries before deletions: %d", h->entries);
  for (i = 0; i < 100; i++)
    iidelete(i * i, h);
  printf("; after: %d\n", h->entries);

  for (i = 0; i < 1000; i += 3)
    iiinsert(i * i * i + i * i * 7 - 1234, i, h);
  for (i = 0; i < 1000; i++)
    if ((i % 3) == 0)
      assert(iiget(i * i * i + i * i * 7 - 1234, h) == i);
    else
      assert(!iicontains(i * i * i - i * i * 7, h));
}
