#ifndef _INT_INT_HASH_H_
#define _INT_INT_HASH_H_

/* This file provides the following types:
   iipair:  pairs of ints (named k and v)
   iihash:  hash table from int to int
   iilist:  list of iipair

   This file provides the following functions related to iipair:
    iimake_pair

   This file provides the following functions related to iilist:
    iicons      
    iihead      
    iitail      
    iiset_tail  
    iiset_head
    iilength    
    iidreverse  
    iifreecell

   This file provides the following functions related to iihash:
    iihashtable           
    iihashtable_with_size 
    iidelete_hashtable    
    iiinsert              
    iidelete              
    iicontains            
    iiget                 
    iiget_if_present      
*/

#include "alloc.h"

#define key int
#define value int
#define hashcode hashcode_int

#include "hashcode_int.h"

#define make_pair iimake_pair
#define pair iipair

#ifndef _IIPAIR_H_
#define _IIPAIR_H_
#include "pair.h"
#endif

#define  T           iipair      
#define  list        iilist      
#define  cons        iicons      
#define  head        iihead      
#define  tail        iitail      
#define  set_tail    iiset_tail  
#define  set_head    iiset_head
#define  length      iilength    
#define  dreverse    iidreverse
#define  dreverse_and_extend    iidreverse_and_extend
#define  freecell    iifreecell

#include "list.h"

#define  hash                  iihash
#define  hashtable             iihashtable
#define  hashtable_with_size   iihashtable_with_size
#define  delete_hashtable      iidelete_hashtable
#define  insert                iiinsert
#define  delete                iidelete
#define  contains              iicontains
#define  get                   iiget
#define  get_if_present        iiget_if_present      

#include "hash.h"

#undef key
#undef value
#undef hashcode
#undef make_pair
#undef pair
#undef T        
#undef list  	  
#undef cons  	  
#undef head  	  
#undef tail  	  
#undef set_tail 
#undef set_head 
#undef length	  
#undef dreverse 
#undef dreverse_and_extend
#undef freecell
#undef hash                
#undef hashtable           
#undef hashtable_with_size 
#undef delete_hashtable    
#undef insert              
#undef delete              
#undef contains            
#undef get                 
#undef get_if_present      

#endif
