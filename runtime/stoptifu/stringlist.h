#ifndef _STRINGLIST_H_
#define _STRINGLIST_H_

/* Lists of char *. */

#define T char *
#define list stringlist
#define cons cons_stringlist
#define head  head_stringlist
#define tail  tail_stringlist
#define length  length_stringlist
#define freecell  freecell_stringlist
#define set_head  set_head_stringlist
#define set_tail  set_tail_stringlist
#define dreverse  dreverse_stringlist
#define dreverse_and_extend  dreverse_stringlist_and_extend

#include "list.h"

#undef T
#undef list
#undef cons
#undef head
#undef tail
#undef length
#undef freecell
#undef set_head
#undef set_tail
#undef dreverse
#undef dreverse_and_extend

#endif /* _STRINGLIST_H_ */
