#ifndef _DIVIDE_INT_WITH_ROUNDING_H_
#define _DIVIDE_INT_WITH_ROUNDING_H_


TI_INLINE(divide_int_rounding_to_minus_inf) int divide_int_rounding_to_minus_inf(int a, int b)
{
  if ((a > 0) == (b > 0)) return a / b;
  else if (b > 0) return (a - (b - 1)) / b;
  else return (a - (b + 1)) / b;
}

TI_INLINE(divide_int_rounding_to_plus_inf) int divide_int_rounding_to_plus_inf(int x, int y)
{
  return -divide_int_rounding_to_minus_inf(-x, y);
}


#endif /* _DIVIDE_INT_WITH_ROUNDING_H_ */
