#include "java_array.h"
#include "java_string.h"
#include "native-stubs.h"
#include "titanium.h"

#include "T11NativeUtils4lang2ti.h"
#include "T6String4lang4java.h"
#include "layout!TAPT6String4lang4java.h"
#include "layout!TAjbyte.h"


char * globalJstringToCstring( GP_JString string )
{
  ti_srcpos_freeze();
  {
  const LTAjbyte bytes =
    m8getBytesPT6String4lang4javamT11NativeUtils4lang2ti( string );
  
  const int length = bytes->header.length; 
  char * const result = ti_malloc_atomic_huge( length + 1 );
  
  memcpy( result, &bytes->data, length );
  result[ length ] = 0;
  ti_srcpos_unfreeze();
  return result;
  }
}


char * localJstringToCstring( LP_JString localString )
{
  GP_JString string;
  globalize( string, localString );
  return globalJstringToCstring( string );
}


LP_JString java_string_build_16( int length, jchar literal[] )
{
    LP_JString result;
    LTAjchar array;

    ti_srcpos_freeze();
    JAVA_ARRAY_BUILD( array, length, literal, TI_ATOMIC_INDEX, 1 );
    result = m11buildStringLTAjcharmT11NativeUtils4lang2ti( array );
    JAVA_ARRAY_FREE( array );
    ti_srcpos_unfreeze();
    
    return result;
}


LP_JString java_string_build_8( char literal[] )
{
    const int length = strlen( literal );
    LP_JString result;
    LTAjbyte array;
    
    ti_srcpos_freeze();
    JAVA_ARRAY_BUILD( array, length, (jbyte *) literal, TI_ATOMIC_INDEX, 1 );
    result = m11buildStringLTAjbytemT11NativeUtils4lang2ti( array );
    JAVA_ARRAY_FREE( array );
    ti_srcpos_unfreeze();
    
    return result;
}


PTAPT6String4lang4java java_strings_build( int argc, char *argv[] )
{
    TAPT6String4lang4java *strings;
    JAVA_ARRAY_ALLOC( strings, NULL, argc, GP_JString, TI_GLOBAL_OBJECT_INDEX, 1, NULL );

    while (argc--) {
	LP_JString newbie = java_string_build_8( argv[ argc ] );
	globalize( strings->data[argc], newbie );
    }

    {
	PTAPT6String4lang4java result;
	globalize( result, strings );
	return result;
    }
}

extern jboolean globalJString_equalto( GP_JString str1, GP_JString str2) {
  jboolean result;
  ti_srcpos_freeze();
  result = m6equalsPT6Object4lang4javamT6String4lang4java( str1, *(PT6Object4lang4java*)&str2 );
  ti_srcpos_unfreeze();
  return result;
}

/* intern a string in the local java.lang.String intern table */
extern LP_JString java_string_localintern(LP_JString str) {
  LP_JString result;
  ti_srcpos_freeze();
  result = m6internLT6String4lang4javamT11NativeUtils4lang2ti(str);
  ti_srcpos_unfreeze();
  return result;
}

extern LP_JString localJString_empty() {
  static LP_JString localString = NULL;
  if (!localString) localString = java_string_build_8("");
  return localString;
}

extern GP_JString globalJString_empty() {
  GP_JString string;
  globalize( string, localJString_empty() );
  return string;
}

