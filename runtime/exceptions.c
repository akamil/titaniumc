#define TI_NO_SRCPOS
#include "backend.h"
#include <assert.h>
#include <stdio.h>
#include <backend-defines.h>
#include "static-global.h"
#include "exceptions.h"
#include "native-utils.h"
#include "subtype.h"
#include "gp-type.h"
#include "layout!ti5cdescmT5Error4lang4java.h"
#include "layout!ti5cdescmT16RuntimeException4lang4java.h"
#include "layout!PT9Throwable4lang4java.h"
#include "T9Throwable4lang4java.h"

TI_EXNSTATE_ALIGN ti_ExceptionState *PERPROC_DEF(exception_state);

__attribute__((noreturn))
static void toss_internal( const GP_instance exception , int exn_isuniversal, 
                           ti_ExceptionState *state);

int find_handler( const ti_ExceptionState * const state,
		  const ti_ExceptionHandler catchers[] )
{
    if (!state->handling) {
	int candidate;
	for (candidate = 0; catchers[ candidate ].catch_class; ++candidate) {
            /* PR824: A universal catch clause will only catch universal exceptions, 
               a non-universal clause will catch both universal and non-universal exceptions. */
	    if ((state->exn_isuniversal || !catchers[ candidate ].catch_isuniversal) &&
                /* only catch exception instances that are a subtype of the catch clause formal arg type */
                instanceOfClass( state->exn_instance, catchers[ candidate ].catch_class )) {

                /* PR824: prevent non-universal checked catch blocks on try stmt with global effects
                          from catching unchecked exceptions */
                if (!catchers[ candidate ].catch_uncheckedAllowed &&
                    (instanceOfClass( state->exn_instance, 
                      (const struct class_header *)&fi5cdescT5Error4lang4java ) || 
                     instanceOfClass( state->exn_instance, 
                      (const struct class_header *)&fi5cdescT16RuntimeException4lang4java ) ) ) {
                      fprintf(stderr, "fatal error on processor %i:\n"
                                      "A catch block for a non-universal checked exception "
                                      "on a try block with global effects caught an unchecked exception at %s\n",
                                      MYPROC, catchers[ candidate ].catch_where);
                      fflush(stderr);
                      toss_internal(state->exn_instance, 0, NULL); /* toss it out of main to get an exception dump */
                } else { /* got a match */
                      return candidate;
                }
            }
        }
    }
    
    return NO_HANDLER;
}

extern void m13dumpExceptionPT9Throwable4lang4javamT11NativeUtils4lang2ti(PT9Throwable4lang4java);
extern void printExitStats(int sglobal);

extern volatile int ti_frozen; /* export to simplify debugger restart */

__attribute__((noreturn))
static void toss_internal( const GP_instance exception , int exn_isuniversal, 
                           ti_ExceptionState *state) {
    CHECK_NULL_GLOBAL_IFBC( exception, "while throwing exception" );
    
    if (!state) {
	PT9Throwable4lang4java exn;
	fputs( "fatal error: uncaught exception", stderr );
        fprintf(stderr, " on processor %i:\n", MYPROC);
        fflush(stderr);
            
        if (getenvMaster("TI_FREEZE_ON_ERROR")) gasnett_freezeForDebuggerNow(&ti_frozen,"ti_frozen");

        if (!getenvMaster("TI_NOCRASHSTACK")) { /* dump the exn to stderr */
          exn = *(PT9Throwable4lang4java*)&(exception);
          m13dumpExceptionPT9Throwable4lang4javamT11NativeUtils4lang2ti(exn);
          fflush(stderr);
        }
        printExitStats(0);
        fflush(stderr);
	abort();
    }
    
#if defined(linux) && defined(i386)
#if defined(__GLIBC__) && defined(JB_BP) /* less old glibc */
    assert( state->handler[0].__jmpbuf[ JB_BP ] );
    assert( state->handler[0].__jmpbuf[ JB_SP ] );
    assert( state->handler[0].__jmpbuf[ JB_PC ] );
#elif 0  /* Linux, very old libc - no way to detect? */
    assert( state->handler[0].__bp );
    assert( state->handler[0].__sp );
    assert( state->handler[0].__pc );
#endif /* Linux, old libc */
#else  /* not Linux */
    /* add other system-specific tests for reasonable jmp_buf */
#endif /* not Linux */
    
    state->exn_instance = exception;
    state->exn_isuniversal = exn_isuniversal;
    LONGJMP( state->handler, 1 );
}

void toss( const GP_instance exception ) {
  toss_internal( exception, 0, PERPROC_REF(exception_state) );
}
void toss_universal( const GP_instance exception ) {
  toss_internal( exception, 1, PERPROC_REF(exception_state) );
}
void retoss( ti_ExceptionState *state ) {
  toss_internal( state->exn_instance, state->exn_isuniversal, state->prior );
}
