#ifndef _INCLUDE_RUNTIME_OPTIONS_VAR_H_
#define _INCLUDE_RUNTIME_OPTIONS_VAR_H_

/* user-requested defaults */
#include "ti_config.h"

/* capabilities known to be absent from specific compilers */

/* Sun WorkShop Pro */
#ifdef __SUNPRO_C
# define FORCE_NO_RUNTIME_VARARRAY
#endif

#ifdef _AIX
# define FORCE_NO_RUNTIME_VARARRAY
#endif

/* Variable size stack arrays */
#define MAX_VARARRAY_STACK_ALLOC 1048576 /* 1 MB seems like a reasonable max */

#if defined(USE_RUNTIME_VARARRAY) && !defined(FORCE_NO_RUNTIME_VARARRAY) && HAVE_ALLOCA_H && ALLOCA_IN_C
#    include <alloca.h>
#    define VARARRAY_DECLARE(type, var, size) int __ISDYNAMIC__ ## var=0; type * var

#    define VARARRAY_CREATE(type, var, size) do {                             \
            if (sizeof(type)*(size) > MAX_VARARRAY_STACK_ALLOC) { /*  too big for stack */   \
              /*  use dynamic allocation */                                   \
              var = (type *) ti_malloc_atomic_huge(sizeof(type)*(size));      \
              __ISDYNAMIC__ ## var = 1;                                       \
              }                                                               \
            else {                                                            \
              /*  use stack allocation */                                     \
              var = (type *) alloca(sizeof(type)*(size));                     \
              if (var) __ISDYNAMIC__ ## var = 0;                              \
              else {  /* alloca failed - use dynamic instead */               \
                var = (type *) ti_malloc_atomic_huge(sizeof(type)*(size));    \
                __ISDYNAMIC__ ## var = 1;                                     \
                }                                                             \
              }                                                               \
            } while(0)

#    define VARARRAY_DESTROY(var) do {                                        \
            if ( __ISDYNAMIC__ ## var ) {  /*  dynamically allocated */       \
              ti_free(var);                                                   \
              }                                                               \
            } while(0)

#else /* defined(USE_RUNTIME_VARARRAY) && !defined(FORCE_NO_RUNTIME_VARARRAY) && HAVE_ALLOCA_H && ALLOCA_IN_C */
#  include "ti-gc.h"
#  define VARARRAY_DECLARE(type, var, size) type * var
#  define VARARRAY_CREATE(type, var, size) var = (type *) ti_malloc_atomic_huge(sizeof(type)*(size))
#  define VARARRAY_DESTROY(var) ti_free(var)
#endif /* defined(USE_RUNTIME_VARARRAY) && !defined(FORCE_NO_RUNTIME_VARARRAY) && HAVE_ALLOCA_H && ALLOCA_IN_C */

#endif /* _INCLUDE_RUNTIME_OPTIONS_VAR_H_ */
