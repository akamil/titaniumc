#ifndef _INCLUDE_COMMON_HEADER_H_
#define _INCLUDE_COMMON_HEADER_H_

#include "backend.h"
#include "layout!T5Class4lang4java.h"
#include "layout!LT6String4lang4java.h"
#include "gp-type.h"
#include "primitives.h"

typedef enum TypeCategory {
    Value,
    Interface,
    Class,
    JavaArray
} TypeCategory;

typedef GP_type( TypeCategory )  GP_TypeCategory;

typedef enum TypeFlags {
  typeflag_None      = 0,
  typeflag_Public    = 1 << 0,
  typeflag_Private   = 1 << 1,
  typeflag_Protected = 1 << 2,
  typeflag_Final     = 1 << 3,
  typeflag_Immutable = 1 << 4,
  typeflag_Strictfp  = 1 << 5,
  typeflag_Abstract  = 1 << 6
} TypeFlags;

typedef struct common_header
{
    T5Class4lang4java class_object; /* MUST come first */
    LT6String4lang4java class_name;

    enum TypeCategory category;
    enum TypeFlags flags;
    jchar index;
} common_header;

#endif
