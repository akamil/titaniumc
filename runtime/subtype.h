#ifndef _INCLUDE_SUBTYPE_H_
#define _INCLUDE_SUBTYPE_H_

#include "instance_header.h"


struct class_header;
struct interface_header;
union type_header;


#define SUBCLASS_OF(      sub, super )  subclassOf(      (const struct class_header *) (sub), (const struct     class_header *) (&super) )
#define CLASS_IMPLEMENTS( sub, super )  classImplements( (const struct class_header *) (sub), (const struct interface_header *) (&super) )

int subclassOf(      const struct class_header * const, const struct     class_header * const );
int classImplements( const struct class_header * const, const struct interface_header * const );

int instanceOf(          const GP_instance, const union       type_header * const );
int instanceOfClass(     const GP_instance, const struct     class_header * const );
int instanceOfInterface( const GP_instance, const struct interface_header * const );

GP_instance castTo(          const GP_instance, const union       type_header * const );
GP_instance castToClass(     const GP_instance, const struct     class_header * const );
GP_instance castToInterface( const GP_instance, const struct interface_header * const );


#endif
