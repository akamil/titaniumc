#ifndef _INCLUDE_FILE_64_H_
#define _INCLUDE_FILE_64_H_

/* these get us 64-bit file declarations under several Unixen */
#define _LARGEFILE64_SOURCE 1
#define _LARGEFILE_SOURCE 1

#include "ti_config.h"
#include "primitives.h"
#ifdef __CYGWIN__
  /* recent Cygwin versions define these for internal use 
     (and autoconf finds them) but as of 6/03 they're not
     user-usable (types are not defined in app code)
  */
  #undef HAVE_FSTAT64
  #undef HAVE_LSEEK64
  #undef HAVE_OPEN64
  #undef HAVE_STAT64
#endif
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#ifndef HAVE_OPEN64
# define open64 open
#endif /* !HAVE_OPEN64 */

#ifndef HAVE_LSEEK64
# ifdef HAVE_LLSEEK
#  define lseek64 llseek
# else /* !HAVE_LLSEEK */
#  define lseek64 lseek
# endif /* !HAVE_LLSEEK */
#endif /* !HAVE_LSEEK64 */

#ifndef HAVE_STAT64
# define stat64 stat
#endif /* !HAVE_STAT64 */

#ifndef HAVE_FSTAT64
# define fstat64 fstat
#endif

#ifndef O_LARGEFILE
# define O_LARGEFILE 0
#endif /* !O_LARGEFILE */


#if defined(STAT64_CONTAINS_LONG_LONGS) || !defined(HAVE_STAT64)
#define CONVERT_OFF64_TO_LONG_LONG(x) (x)
#define CONVERT_LSEEK64_OFFSET(x) (x)
#else
#define CONVERT_OFF64_TO_LONG_LONG(x) convert_to_long_long(x)
static long long convert_to_long_long(off64_t x)
{
  return (*(long long *)(&(x)));
}
#define CONVERT_LSEEK64_OFFSET(x) convert_from_long_long((long long) (x))
static off64_t convert_from_long_long(long long x)
{
  return (*(off64_t *)(&(x)));
}
#endif

#endif /* _INCLUDE_FILE_64_H_ */
