#ifndef _INCLUDE_TYPE_HEADER_H_
#define _INCLUDE_TYPE_HEADER_H_

#include "common_header.h"
#include "class_header.h"
#include "interface_header.h"
#include "value_header.h"
#include "T5Class4lang4java.h"
#include "layout!LT6String4lang4java.h"
#include "layout!LT10Checkpoint4lang2ti.h"


typedef union type_header {
  struct common_header common_header;
  struct value_header value_header;
  struct interface_header interface_header;
  struct class_header class_header;
} type_header;

extern void ti_register_class(type_header *t);
extern PT5Class4lang4java ti_get_class(GP_JString name, int allowSpecial);

#define TI_ATOMIC_INDEX ((jchar) 0)
#define TI_LOCAL_OBJECT_INDEX ((jchar) 1)
#define TI_GLOBAL_OBJECT_INDEX ((jchar) 2)
#define TI_UNDEFINED_INDEX ((jchar) 3)
#define TI_IMMUTABLE_NONATOMIC_START_INDEX ((jchar) 4)

typedef struct cp_methods {
  jchar index;
  void (*checkpoint)(void *, LT10Checkpoint4lang2ti);
  void (*restore)(void *, LT10Checkpoint4lang2ti);
} cp_methods;

extern jchar ti_register_nonatomic_immutable(void (*checkpoint)(void *, LT10Checkpoint4lang2ti),
					     void (*restore)(void *, LT10Checkpoint4lang2ti));
extern cp_methods *ti_get_checkpoint_methods(jchar index);

#endif
