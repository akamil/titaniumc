#include "type_header.h"
#include "primitives.h"
#include "T5Class4lang4java.h"
#include "java_string.h"
#include "java_array_methods.h"
#include "backend.h"

#include "T4Void4lang4java.h"
#include "T7Boolean4lang4java.h"
#include "T9Character4lang4java.h"
#include "T4Byte4lang4java.h"
#include "T5Short4lang4java.h"
#include "T7Integer4lang4java.h"
#include "T4Long4lang4java.h"
#include "T5Float4lang4java.h"
#include "T6Double4lang4java.h"

#define PRIMITIVE_INIT(type)       \
 { { &fi5cdescT5Class4lang4java }, \
   NULL,                           \
   Value,                          \
   typeflag_None,                  \
   TI_ATOMIC_INDEX,                \
   sizeof(type) }

struct value_header    void_desc = PRIMITIVE_INIT( jboolean );
struct value_header boolean_desc = PRIMITIVE_INIT( jboolean );
struct value_header    char_desc = PRIMITIVE_INIT( jchar );
struct value_header    byte_desc = PRIMITIVE_INIT( jbyte );
struct value_header   short_desc = PRIMITIVE_INIT( jshort );
struct value_header     int_desc = PRIMITIVE_INIT( jint );
struct value_header    long_desc = PRIMITIVE_INIT( jlong );
struct value_header   float_desc = PRIMITIVE_INIT( jfloat );
struct value_header  double_desc = PRIMITIVE_INIT( jdouble );

#define VALUELIST_SZ 25
struct value_header *ti_valuelist[VALUELIST_SZ];
size_t ti_valuelist_cnt = 0;

#ifdef HAVE_MONITORS
#define INIT_CLASS_OBJECT(desc, name) do { 		\
  monitor_init(&((desc).class_object.monitor)); 	\
  (desc).class_name = java_string_build_8(name);	\
  ti_valuelist[ti_valuelist_cnt++] = &(desc);           \
  } while(0)
#else
#define INIT_CLASS_OBJECT(desc, name) do { 		\
  (desc).class_name = java_string_build_8(name);	\
  ti_valuelist[ti_valuelist_cnt++] = &(desc);           \
  } while(0)
#endif
#define INIT_TYPE_FIELD(desc, name) do {                                       \
  TO_GLOBALB_STATIC( STATIC_REF(fi5cdescT ## name ## 4lang4java_static_fields, \
                         f4TYPET ## name ## 4lang4java),                       \
              0, &(desc.class_object));                                        \
  } while(0)

void value_desc_init() {
  if (MYBOXPROC == 0) { /* first thread on each box does reg */
  INIT_CLASS_OBJECT(void_desc, 	        "void");
  INIT_CLASS_OBJECT(boolean_desc,	"boolean");
  INIT_CLASS_OBJECT(char_desc,		"char");
  INIT_CLASS_OBJECT(byte_desc,		"byte");
  INIT_CLASS_OBJECT(short_desc,		"short");
  INIT_CLASS_OBJECT(int_desc,		"int");
  INIT_CLASS_OBJECT(long_desc,		"long");
  INIT_CLASS_OBJECT(float_desc,		"float");
  INIT_CLASS_OBJECT(double_desc,	"double");

  INIT_CLASS_OBJECT(*(value_header *)java_array_desc,	"JavaArray"); /* this one is temporary */
  assert(ti_valuelist_cnt < VALUELIST_SZ);
  }

  /* all threads set their .TYPE static fields */
  INIT_TYPE_FIELD(void_desc, 4Void);
  INIT_TYPE_FIELD(boolean_desc, 7Boolean);
  INIT_TYPE_FIELD(char_desc, 9Character);
  INIT_TYPE_FIELD(byte_desc, 4Byte);
  INIT_TYPE_FIELD(short_desc, 5Short);
  INIT_TYPE_FIELD(int_desc, 7Integer);
  INIT_TYPE_FIELD(long_desc, 4Long);
  INIT_TYPE_FIELD(float_desc, 5Float);
  INIT_TYPE_FIELD(double_desc, 6Double);

  local_barrier(); 
}

/* Initial simple implementation of class registration, use a simple list */
type_header **ti_classlist = NULL;
size_t ti_classlist_sz = 0;
size_t ti_classlist_cnt = 0;
#define TI_CLASSLIST_INITSZ  200

extern void ti_register_class(type_header *t) {
  assert(MYBOXPROC == 0); /* first thread on each box does reg */
  if (ti_classlist == NULL) { 
    ti_classlist = (type_header **)ti_malloc(sizeof(type_header *)*TI_CLASSLIST_INITSZ);
    ti_classlist_sz = TI_CLASSLIST_INITSZ;
  }
  if (ti_classlist_cnt == ti_classlist_sz) {
    type_header **tmp;
    ti_classlist_sz *= 2;
    tmp = (type_header **)ti_malloc(sizeof(type_header *)*ti_classlist_sz);
    memcpy(tmp, ti_classlist, sizeof(type_header *)*ti_classlist_cnt);
    ti_free(ti_classlist);
    ti_classlist = tmp;
  }
  ti_classlist[ti_classlist_cnt] = t;
  ti_classlist_cnt++;
}

extern PT5Class4lang4java ti_get_class(GP_JString name, int allowSpecial) {
  int i;
  PT5Class4lang4java global_classptr;
  type_header *result = NULL;
  char *lname = NULL;

  if (allowSpecial) {
    for (i = 0; i < ti_valuelist_cnt; i++) {
      GP_JString tmpname; 
      globalize(tmpname, ti_valuelist[i]->class_name);      
      if (globalJString_equalto(tmpname, name)) {
        result = (type_header *)ti_valuelist[i];
        break;
      }
    }
  }

  if (result == NULL) {
    /* all Java arrays map to the same Class object */
    lname = globalJstringToCstring(name); 
    if (lname[0] == '[' || !strcmp(lname, "JavaArray")) {
      result = (type_header *)java_array_desc;
    }
    ti_free(lname);
  }

  if (result == NULL) {
    for (i = 0; i < ti_classlist_cnt; i++) {
      GP_JString tmpname; 
      globalize(tmpname, ((common_header*)ti_classlist[i])->class_name);      
      if (globalJString_equalto(tmpname, name)) {
        result = (type_header *)ti_classlist[i];
        break;
      }
    }
  }

  if (result == NULL) tossClassNotFoundException_str(globalJstringToCstring(name));

  TO_GLOBALB_STATIC(global_classptr, 0, (T5Class4lang4java *)result); /* canonical Class objects are on P0 */
  return global_classptr;
}

/* Support for checkpointing arrays */
cp_methods **ti_cplist = NULL;
jchar ti_cplist_sz = 0;
jchar ti_cplist_cnt = TI_IMMUTABLE_NONATOMIC_START_INDEX;
#define TI_CPLIST_INITSZ  25

/* this doesn't yet work for ti arrays */
extern jchar ti_register_nonatomic_immutable(void (*checkpoint)(void *, LT10Checkpoint4lang2ti),
					     void (*restore)(void *, LT10Checkpoint4lang2ti)) {
  assert(MYBOXPROC == 0); /* first thread on each box does reg */
  if (ti_cplist == NULL) { 
    ti_cplist = (cp_methods **)ti_malloc(sizeof(cp_methods *)*TI_CPLIST_INITSZ);
    ti_cplist_sz = TI_CPLIST_INITSZ;
  }
  if (ti_cplist_cnt == ti_cplist_sz) {
    cp_methods **tmp;
    ti_cplist_sz *= 2;
    tmp = (cp_methods **)ti_malloc(sizeof(cp_methods *)*ti_cplist_sz);
    memcpy(tmp, ti_cplist, sizeof(cp_methods *)*ti_cplist_cnt);
    ti_free(ti_cplist);
    ti_cplist = tmp;
  }
  ti_cplist[ti_cplist_cnt] = (cp_methods *) ti_malloc(sizeof(cp_methods));
  ti_cplist[ti_cplist_cnt]->checkpoint = checkpoint;
  ti_cplist[ti_cplist_cnt]->restore = restore;
  ti_cplist[ti_cplist_cnt]->index = ti_cplist_cnt;
  return ti_cplist_cnt++;
}

extern cp_methods *ti_get_checkpoint_methods(jchar index) {
  assert(index >= TI_IMMUTABLE_NONATOMIC_START_INDEX && index < ti_cplist_cnt);
  return ti_cplist[index];
}
