#ifndef _include_static_global_h_
#define _include_static_global_h_


#ifdef MEMORY_SHARED 

#if 0
  /* the "old" way */
  #define STATIC_DEF(structname)	      structname[MAX_BOX_PROCS]
  #define STATIC_REF(structname, fld)         (structname[MYBOXPROC].fld)
  #define STATIC_INIT(STATICS_METAFN)         ((void)0)
#else
  /* the "new" way (2.409) */
  #define STATIC_DEF(structname)	      *structname[MAX_BOX_PROCS]
  #define STATIC_REF(structname, fld)         (structname[MYBOXPROC]->fld)

  #define _STATIC_ALIGNMENT ((size_t)16)  /* structure alignment to maintain for each static struct */
  #define _STATIC_CACHEPAD  ((size_t)256) /* trailing padding to isolate statics in cache (one per pthread) */
  #define _STATIC_ALIGNUP(sz) ( ((size_t)(sz) + (_STATIC_ALIGNMENT-1)) & ~(_STATIC_ALIGNMENT-1) )

  #define _STATIC_SUMSZ(T,N)  totalsz += _STATIC_ALIGNUP(sizeof(T));
  #define _STATIC_SETPTR(T,N) N[MYBOXPROC] = (T*)p; p += _STATIC_ALIGNUP(sizeof(T));
  #define STATIC_INIT(STATICS_METAFN)  do {                         \
    char *buffer; char *p;                                          \
    size_t totalsz = 0;                                             \
    STATICS_METAFN(_STATIC_SUMSZ)                                   \
    totalsz += _STATIC_CACHEPAD; /* ensure cache block isolation */ \
    assert(totalsz == _STATIC_ALIGNUP(totalsz));                    \
    buffer = ti_malloc_huge(totalsz);                               \
    p = buffer;                                                     \
    STATICS_METAFN(_STATIC_SETPTR)                                  \
    assert(p - buffer + _STATIC_CACHEPAD == totalsz);               \
    } while (0)
#endif

  #define FILE_STATIC_STATIC_DEF(name, type)   static type name[MAX_BOX_PROCS]
  #define FILE_STATIC_STATIC_REF(name)         (name[MYBOXPROC])

  #define PERPROC_DEF(name)                    name[MAX_BOX_PROCS]
  #define PERPROC_REF(name)                    (name[MYBOXPROC])

#else /* UNIPROC */

  #define STATIC_DEF(name)                    name 
  #define STATIC_REF(structname, fld)         (structname.fld)
  #define STATIC_INIT(STATICS_METAFN)         ((void)0)

  #define FILE_STATIC_STATIC_DEF(name, type)   static type name
  #define FILE_STATIC_STATIC_REF(name)         (name)

  #define PERPROC_DEF(name)                     name 
  #define PERPROC_REF(name)                    (name)

#endif


#endif /* !_include_static_global_h_ */
