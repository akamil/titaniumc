/* this file is included in the generated array layout header,
 * which is also included very early before ti_array.[ch]
 */
#include "backend.h"
#include "ti_array_flags.h"

/* ensure all the relevant defs exist */
#ifndef ti_ARRAY
# error ti_ARRAY undefined
#endif /* !ti_ARRAY */

#ifndef ti_RECTDOMAIN
# error ti_RECTDOMAIN undefined
#endif /* !ti_RECTDOMAIN */

#ifndef PTR_TO_T
# error PTR_TO_T undefined
#endif /* !PTR_TO_T */

#ifndef GLOBAL_ARRAY
# error GLOBAL_ARRAY undefined
#endif /* !GLOBAL_ARRAY */

#ifndef DUMPABLE_ARRAYS 
# error DUMPABLE_ARRAYS undefined
#endif

#ifndef N
# error N undefined
#endif

#ifndef EXPLICITLY_STORE_ALLOCATION_SITE
# error EXPLICITLY_STORE_ALLOCATION_SITE undefined
#endif

#ifndef EXPLICITLY_STORE_CREATOR
# error EXPLICITLY_STORE_CREATOR undefined
#endif


struct ti_ARRAY {
  /* NOTE: the following TiArray methods rely on this field ordering:
      ti_cast_to_local, ti_cast_to_global, COPY_METHOD
   */
  PTR_TO_T A;            /* pointer comes first for optimal struct packing and access performance */
  PTR_TO_T ancestor;     /* pointer to the start of the memory block returned by malloc 
                            which contains this array (for GC purposes) */

  ti_RECTDOMAIN domain;  /* The domain of the array. */

  jint stride[N];        /* The stride for index calc.  Must be >= 1. */
  jint base[N];          /* The lower bound of domain. */
  int sideFactors[N];    /* Used in the address calculation. */

#if DUMPABLE_ARRAYS
  void (*dumpfn)();
#endif
#if EXPLICITLY_STORE_ALLOCATION_SITE
  const char *where;     /* where (in source code) the array was created */
#endif
#if EXPLICITLY_STORE_CREATOR
  jint creator;
#endif

#if defined(__mips64) && defined(__GNUC__) && __GNUC__ < 3
  /* DOB: FRAGILE HACK - fixes a gcc bug on the Origin 2000 (PR 307) */     
  #if defined(WIDE_POINTERS) && GLOBAL_ARRAY
    jbyte _pad[8*N+1]; 
  #else
    jbyte _pad[5];
  #endif
#endif
};

/*
 * Array desc. size breakdown (bytes)      LOCAL           GLOBAL
 * ----------------------------------   ILP32   LP64    ILP32   LP64
 * 2*sizeof(PTR_TO_T) + // A, ancestor=   8      16       16     32
 * 4*4*N + // rectdomain              =      16N             16N
 * 3*4*N + // stride, base, sideFactors=     12N             12N
 * (optional extras: 4+2*sizeof(void*))= (12)   (20)     (12)   (20)
 * TOTAL (default config - no extras):  8+28N  16+28N   16+28N 32+28N
 *                                 1D:   36      44       44     60
 *                                 2D:   64      72       72     88
 *                                 3D:   92     100      100    116
 */

