#ifndef _include_array_byteswap_h_
#define _include_array_byteswap_h_

#include "primitives.h"

#define Ljbyte jbyte *

/*  use macros below instead of these directly */
void arrayByteSwap(Ljbyte array, jint sizeofelem, jint numofelem);
void arrayCopyByteSwap(Ljbyte toarray, const Ljbyte fromarray, jint sizeofelem, jint numofelem);

/*------------------------------------------------------------------------------------*/

/*  in-place byte swap on a local C-array of data elements */
#define ARRAY_BYTE_SWAP_LOCAL(localdataptr, sizeofelem, numofelem) \
    arrayByteSwap((localdataptr),(sizeofelem), (numofelem))

/*  in-place byte swap on a global C-array of data elements */
/*  exploits any locality available */
#define ARRAY_BYTE_SWAP_GLOBAL(globaldataptr, sizeofelem, numofelem)  do {        \
    if (isDirectlyAddressable(globaldataptr)) {                                                 \
      ARRAY_BYTE_SWAP_LOCAL(TO_LOCAL(globaldataptr), (sizeofelem), (numofelem));  \
      }                                                                           \
    else {                                                                        \
	    int copy_sz = (sizeofelem) * (numofelem);                                   \
	    VARARRAY_DECLARE(jbyte, tmp_buf, copy_sz);                                  \
	    VARARRAY_CREATE(jbyte, tmp_buf, copy_sz);                                   \
	    global_to_local_copy((globaldataptr), tmp_buf, copy_sz);                    \
      ARRAY_BYTE_SWAP_LOCAL(tmp_buf, (sizeofelem), (numofelem));                  \
	    local_to_global_copy(tmp_buf, (globaldataptr), copy_sz);                    \
	    VARARRAY_DESTROY(tmp_buf);                                                  \
      } \
    } while(0)

/*  byte swap and copy between local C-arrays of data elements */
#define ARRAY_COPY_BYTE_SWAP_LOCAL(localtodataptr, localfromdataptr, sizeofelem, numofelem) \
    arrayCopyByteSwap((localtodataptr),(localfromdataptr),(sizeofelem), (numofelem))

/*  byte swap and copy between global C-arrays of data elements */
/*  exploits any locality available */
#define ARRAY_COPY_BYTE_SWAP_GLOBAL(globaltodataptr, globalfromdataptr, sizeofelem, numofelem) do { \
      if (isDirectlyAddressable(globaltodataptr) && isDirectlyAddressable(globalfromdataptr)) {                                 \
        ARRAY_COPY_BYTE_SWAP_LOCAL(TO_LOCAL(globaltodataptr), TO_LOCAL(globalfromdataptr), (sizeofelem), (numofelem));  \
        }                                                                               \
      else if (isDirectlyAddressable(globaltodataptr) && !isDirectlyAddressable(globalfromdataptr)) {               \
	      int copy_sz = (sizeofelem) * (numofelem);                                       \
	      global_to_local_copy((globalfromdataptr), TO_LOCAL(globaltodataptr), copy_sz);  \
        ARRAY_BYTE_SWAP_LOCAL(TO_LOCAL(globaltodataptr), (sizeofelem), (numofelem));    \
        }                                                                               \
      else if (!isDirectlyAddressable(globaltodataptr) && isDirectlyAddressable(globalfromdataptr)) {               \
	      int copy_sz = (sizeofelem) * (numofelem);                     \
	      VARARRAY_DECLARE(jbyte, tmp_buf, copy_sz);                    \
	      VARARRAY_CREATE(jbyte, tmp_buf, copy_sz);                     \
        ARRAY_COPY_BYTE_SWAP_LOCAL(tmp_buf, TO_LOCAL(globalfromdataptr), (sizeofelem), (numofelem));    \
	      local_to_global_copy(tmp_buf, (globaltodataptr), copy_sz);    \
	      VARARRAY_DESTROY(tmp_buf);                                    \
        }                                                             \
      else if (!isDirectlyAddressable(globaltodataptr) && !isDirectlyAddressable(globalfromdataptr)) { \
	      int copy_sz = (sizeofelem) * (numofelem);                     \
	      VARARRAY_DECLARE(jbyte, tmp_buf, copy_sz);                    \
	      VARARRAY_CREATE(jbyte, tmp_buf, copy_sz);                     \
	      global_to_local_copy((globalfromdataptr), tmp_buf, copy_sz);  \
        ARRAY_BYTE_SWAP_LOCAL(tmp_buf, (sizeofelem), (numofelem));    \
	      local_to_global_copy(tmp_buf, (globaltodataptr), copy_sz);    \
	      VARARRAY_DESTROY(tmp_buf);                                    \
        }                                                             \
      } while (0)

#endif

#undef Ljbyte
