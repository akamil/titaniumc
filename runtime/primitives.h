#ifndef _INCLUDE_TLIB_PRIMITIVES_H
#define _INCLUDE_TLIB_PRIMITIVES_H

#include "ti_config.h"
#include "backend-defines.h"
/* portable_platform.h - part of GASNet, provides identification macros for platforms */
#include "portable_platform.h"

/* Define the ABI for use by the Titanium runtime. This should match
   the ABI specified by Java.

   Item         Description
   ----         -----------
   jboolean     A non-signed 1-bit integer (unsigned 8-bit integer)
   jubyte       An unsigned 8-bit integer (*)
   jchar        An unsigned 16-bit integer
   juint        An unsigned 32-bit integer (*)
   julong       An unsigned 64-bit integer (*)
   jbyte        A signed 8-bit integer
   jshort       A signed 16-bit integer
   jint         A signed 32-bit integer
   jlong        A signed 64-bit integer

   jIntPointer  A signed integer that holds a pointer (*)
   jUIntPointer An unsigned integer that holds a pointer (*)

   jfloat       A single precision floating point number
   jdouble      A double precision floating point number

   (*) These are unspecified within the Java language itself.

   */

#if defined(__TERA__)
/* The Tera */
typedef unsigned      char jboolean;
typedef unsigned      char jubyte;
typedef unsigned __short16 jchar;
typedef unsigned   __int32 juint;
typedef unsigned   __int64 julong;
typedef   signed      char jbyte;
typedef          __short16 jshort;
typedef            __int32 jint;
typedef            __int64 jlong;
typedef            __int64 jIntPointer;
typedef unsigned   __int64 jUIntPointer;

#elif defined(USE_RUNTIME_CRAY_GNU_C)
/* The Cray T3E */
/* Special Gnu C compiler: ILP64 (with 16-bit shorts) */
typedef unsigned  char jboolean;
typedef unsigned  char jubyte;
typedef unsigned short jchar;
typedef unsigned   int juint;  /* This is 64-bits, should be 32 !!! */
typedef unsigned  long julong;
typedef   signed  char jbyte;
typedef          short jshort;
typedef            int jint;   /* This is 64-bits, should be 32 !!! */
typedef           long jlong;
typedef           long jIntPointer;
typedef unsigned  long jUIntPointer;

#elif defined(USE_RUNTIME_CRAY_T3E_C)
/* The Cray T3E C compiler */
/* ILP64 with 32-bit shorts */
typedef unsigned  char jboolean;
typedef unsigned  char jubyte;
typedef unsigned short jchar;  /* This is 32-bits, should be 16 !!! */
typedef unsigned short juint;
typedef unsigned  long julong;
typedef   signed  char jbyte;
typedef          short jshort; /* This is 32-bits, should be 16 !!! */
typedef          short jint;
typedef           long jlong;
typedef           long jIntPointer;
typedef unsigned  long jUIntPointer;

#else
/* portable_inttypes.h - part of GASNet, provides all the standard inttypes on most platforms */
#    include "portable_inttypes.h"
typedef        uint8_t jboolean;
typedef        uint8_t jubyte;
typedef       uint16_t jchar;
typedef       uint32_t juint;
typedef       uint64_t julong;
typedef         int8_t jbyte;
typedef        int16_t jshort;
typedef        int32_t jint;
typedef        int64_t jlong;
typedef       intptr_t jIntPointer;
typedef      uintptr_t jUIntPointer;

#endif 

/* The floating point numbers */
typedef          float jfloat;
typedef         double jdouble;

#if SIZEOF_VOID_P == 8
 #define PTR64
#else 
 #define PTR32
#endif

#ifdef PTR64
typedef juint Process;
typedef juint Box;
#else
typedef jchar Process;
typedef jchar Box;
#endif /* PTR64 */

#ifdef WIDE_POINTERS
typedef struct {
    Box box; /* this is also the proc number when !MEMORY_SHARED */
    void *addr;
  } jGPointer;

#ifdef MEMORY_SHARED
#define GET_PROC(x)             COMM_GetProxyProcNumberForBoxNumber((x).box)
#else
#define GET_PROC(x)             (x).box
#endif
#define GET_BOX(x)              (x).box
#define GET_ADDR(x)             (x).addr

/* client only needs to set either box or proc, and box is more efficient */  
#define SET_BOX(x, val)  ((x).box = val)
#ifdef MEMORY_SHARED
#define SET_PROC(x, val) ((x).box = COMM_GetBoxNumberForProcNumber(val))
#else
#define SET_PROC(x, val) ((x).box = val)
#endif
#define SET_ADDR(x, val) ((x).addr = (void *)(val))

#else

typedef void *jGPointer;

#define GET_PROC(x)       0
#define GET_BOX(x)        0
#define GET_ADDR(x)       ((void *)(x))

#define SET_PROC(x, val)
#define SET_BOX(x, val)
#define SET_ADDR(x, val)  (x = (jGPointer)(val))

#endif /* WIDE_POINTERS */


/* Cast a GP_type pointer to a jGPointer */
#define CAST_GPTR(ptr)        (*(jGPointer *)(&(ptr)))
#define CAST_GPTR_TO_TYPE(type, ptr)  (*(type *)(&(ptr)))

#endif /* _INCLUDE_TLIB_PRIMITIVES_H */
