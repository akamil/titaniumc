#ifndef _INCLUDE_JAVA_ARRAY_HEADER_H_
#define _INCLUDE_JAVA_ARRAY_HEADER_H_


#include "instance_header.h"
#include "primitives.h"


typedef struct java_array_header {

  struct instance_header header;

   /* for maintaining double-word alignment of data which follows the header 
    * padding must immediately follow the instance_header field 
    * to guarantee this works right when sizeof(instance_header)%8 < sizeof(jint)
    */
  #define __EXP ((8 - (sizeof(struct instance_header) + sizeof(jint) + sizeof(jchar)*2)%8)%8)
  #ifdef __GNUC__
    jbyte _alignment_padding[__EXP];
  #else 
    /* non-GNU compilers may choke on 0-length array in a struct */
    jbyte _alignment_padding[__EXP == 0 ? 8 : __EXP]; 
  #endif
  #undef __EXP

  jint length;             /* number of elements */
  jchar size;              /* sizeof(element) */
  jchar elem_info;         /* 0 = atomic
			    * 1 = Object or subclass thereof
			    * 2+ = non-atomic immutable
			    */

  /* if you add fields to this struct, update the alignment expression above */

} java_array_header;

#define JAVA_ARRAY_ELT_ATOMIC(Lhdr) (((Lhdr)->elem_info) == TI_ATOMIC_INDEX)
#define JAVA_ARRAY_SET_ELT_ATOMIC(Lhdr) do {  \
    (Lhdr)->elem_info = TI_ATOMIC_INDEX;      \
  } while (0)
    

#endif /* !_INCLUDE_JAVA_ARRAY_HEADER_H_ */
