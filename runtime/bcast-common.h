#ifndef _include_bcast_common_h_
#define _include_bcast_common_h_

#include "backend.h"

#ifdef MEMORY_SHARED
 #define CYCLE (myProcess()->broadcast_cycle)
#else
 extern int PERPROC_DEF( BroadcastCycle );
 #define CYCLE PERPROC_REF( BroadcastCycle )
#endif

#define BUFFER (buffer[ CYCLE ])


#define BROADCAST_BEGIN( type, sender )							\
  static type volatile buffer[ 2 ];							\
  do {											\
    ti_trace_printf(("COLLECTIVE broadcast: sz = %i, sender = %i, "                     \
                    #type, (int)sizeof(type), (sender)));                               \
    if ((sender) < 0 || (sender) >= PROCS) {						\
      fprintf( stderr, "fatal error in %s:\n"                                           \
               " cannot broadcast from process %d, there are only %d processes!\n",     \
               __current_loc, (sender), PROCS );	                                \
      fflush(stderr);                                                                   \
      abort();										\
    }											\
  } while (0)


#endif /* !_include_bcast_common_h_ */
