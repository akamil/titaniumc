#include <backend-defines.h>
#include <class_header.h>
#include <ti-gc.h>
#include <java_array.h>
#include <java_array_methods.h>
#include <tally-memory.h>

#include "T6Object4lang4java.h"
#include "T9Cloneable4lang4java.h"
#include "T5Class4lang4java.h"
#include "T10Checkpoint4lang2ti.h"
#include "layout!PTAjbyte.h"
#include "layout!Pclass_header.h"
#include "layout!PLti5cdescmT6Object4lang4java.h"
#include "layout!LT10Checkpoint4lang2ti.h"
#include "layout!PT10Checkpoint4lang2ti.h"

/* someday, we should have one of these for each java array type
   with information about the element type
 */

/* should include Cloneable and java.io.Serializable (Java 1.1) */
static const struct interface_header *_java_array_impl[] = 
  { (interface_header *)&fi5cdescT9Cloneable4lang4java, NULL };

static struct ti5cdescmT6Object4lang4java _java_array_desc = {
  { &fi5cdescT5Class4lang4java },
  NULL,
  JavaArray,
  typeflag_None,
  TI_UNDEFINED_INDEX,
  _java_array_impl, /* implements */
  0, /* intf_info */
  (class_header *)&fi5cdescT6Object4lang4java, /* super */
  0, /* size field (should not be trusted) */
  NULL, /* nullify locals (should not be trusted) */
  NULL, /* don't allow newInstance on arrays */
  mi10checkpointmT6Object4lang4java, /* checkpoint */
  mi7restoremT6Object4lang4java, /* restore */
m8hashCodemT6Object4lang4java,
m6equalsPT6Object4lang4javamT6Object4lang4java,
ml10localClonemT6Object4lang4java,
m5clonemT6Object4lang4java,
m8toStringmT6Object4lang4java,
m8finalizemT6Object4lang4java
};
class_header *java_array_desc = (class_header *)&_java_array_desc;

java_array_header *java_array_alloc( Region pregion, int length, int elementSize,
				     jchar eleminfo, int isShared, const void *pDefaultElt )
{
  int atomicelements = (eleminfo == TI_ATOMIC_INDEX);
  const size_t size = sizeof(java_array_header) + elementSize * length;
  java_array_header *array;

  tally_memory( size, isShared );

  if (pregion) {
    if (atomicelements) {
      array = (java_array_header *) ralloc_atomic( pregion, size );
      memset(array, 0, size); /* not automatically cleared for atomic */
    }
    else {
      array = (java_array_header *) ralloc( pregion, size );
    }
  }
  else {
    if (atomicelements) {
      array = (java_array_header *) ti_malloc_atomic_huge( size );
    }
    else {
      array = (java_array_header *) ti_malloc_huge( size );
    }
  }
  if (pDefaultElt) { /* PR274: decide if we have a non-zero default value for the immutable elements */
    int allzeros = 1;
    const char *p = (const char *)pDefaultElt;
    size_t j;
    for (j=0; j < elementSize; j++) {
      if (p[j]) { allzeros = 0; break; }
    }
    if (allzeros) { /* just zeros */
      if (atomicelements) memset(array, 0, size); /* not automatically cleared for atomic */
    } else { /* non-zero initializer, set all the elements */
      char *data = (char *)(array+1);
      for (j=0; j < length; j++) {
        memcpy(data, pDefaultElt, elementSize);
        data += elementSize;
      }
    }
  } else if (atomicelements) { 
    memset(array, 0, size); /* not automatically cleared for atomic */
  }
 
  JAVA_ARRAY_INIT_FIELDS(array, length, elementSize, eleminfo); 
  
  return array;
}

void java_array_free( java_array_header *array ) {
  assert(PL2RegionId(array) == UNK_REGIONID);

  #ifdef HAVE_MONITORS
    monitor_destroy(&array->header.monitor);
  #endif
  ti_free(array);
}

java_array_header *java_array_build( int length, int elementSize, void *data, jchar eleminfo, int isShared )
{
  java_array_header * const array = java_array_alloc( 0, length, elementSize, eleminfo, isShared, NULL );
  if (length > 0) {
    memcpy( array + 1, data, length * elementSize );
  }
  return array;
}

int is_java_array_global( PT6Object4lang4java object ) {
    typedef GP_type( TypeCategory ) GP_TypeCategory;

    ti5cdescmT6Object4lang4java *info;
    Pclass_header header;
    GP_TypeCategory categoryField;
    TypeCategory category;

    CLASS_INFO_GLOBAL( info, ti5cdescmT6Object4lang4java, object );
    TO_GLOBALB( header, TO_BOX( object ), (class_header *) info );
    FIELD_ADDR_GLOBAL( categoryField, header, category );
    FENCE_PRE_READ();
    /* MUST use _bulk here because width of TypeCategory might not always be sizeof(jint) */
    DEREF_GLOBAL_bulk( category, categoryField );
    FENCE_POST_READ();
    return (category == JavaArray);
}

int is_java_array_local( LT6Object4lang4java object ) {
  jGPointer gObject;
  globalize( gObject, object );
  return is_java_array_global(*(PT6Object4lang4java*)&gObject);
} 

int java_array_subclass_of_global( PT6Object4lang4java sourceObject, int elementSize, int atomicelements) {
  jint length, size, isatomic;
  jchar eleminfo;
  if (!is_java_array_global(sourceObject)) return 0;
  JAVA_ARRAY_INFO_GLOBAL( length, size, eleminfo, *(PTAjbyte *)&sourceObject);
  isatomic = (eleminfo == TI_ATOMIC_INDEX);
  return (size == elementSize && !!isatomic == !!atomicelements);
}

int java_array_subclass_of_local(  LT6Object4lang4java sourceObject, int elementSize, int atomicelements) {
  jGPointer gsourceObject;
  globalize( gsourceObject, sourceObject );
  return java_array_subclass_of_global(*(PT6Object4lang4java*)&gsourceObject, elementSize, atomicelements);
}

void java_local_byte_array_write(void *src, TAjbyte *b, jint off, jint len) {
  jbyte* localbuf;

  /* we could use JAVA_ARRAY_ADDR_GLOBAL here, but that includes a redundant check */
  FIELD_ADDR_LOCAL( localbuf, b, data[0] );
  INDEX_LOCAL( localbuf, localbuf, off );	

  memcpy(localbuf, src, len);
}

void java_byte_array_write(void *src, PTAjbyte b, jint off, jint len) {
  JAVA_ARRAY_CHECK_GLOBAL( b, off, len, "in java_byte_array_write()" );

  if (isDirectlyAddressable(b)) { /*  read into local array */
    java_local_byte_array_write(src, TO_LOCAL(b), off, len);
  }
  else { /*  read into global array */
    VARARRAY_DECLARE( jbyte, buffer, len );
    VARARRAY_CREATE( jbyte, buffer, len );

    memcpy(buffer, src, len);

    Pjbyte rem_buf;
    /* we could use JAVA_ARRAY_ADDR_GLOBAL here, but that includes a redundant check */
    FIELD_ADDR_GLOBAL( rem_buf, b, data[0] );
    INDEX_GLOBAL( rem_buf, rem_buf, off );	

    local_to_global_copy( buffer, rem_buf, len );
    VARARRAY_DESTROY( buffer );
  }
}

void java_local_byte_array_read(void *src, TAjbyte *b, jint off, jint len) {
  jbyte* localbuf;

  /* we could use JAVA_ARRAY_ADDR_GLOBAL here, but that includes a redundant check */
  FIELD_ADDR_LOCAL( localbuf, b, data[0] );
  INDEX_LOCAL( localbuf, localbuf, off );	

  memcpy(src, localbuf, len);
}

void java_byte_array_read(void *src, PTAjbyte b, jint off, jint len) {
  JAVA_ARRAY_CHECK_GLOBAL( b, off, len, "in java_byte_array_read()" );

  if (isDirectlyAddressable(b)) { /*  read into local array */
    java_local_byte_array_read(src, TO_LOCAL(b), off, len);
  }
  else { /*  read into global array */
    VARARRAY_DECLARE( jbyte, buffer, len );
    VARARRAY_CREATE( jbyte, buffer, len );

    Pjbyte rem_buf;
    /* we could use JAVA_ARRAY_ADDR_GLOBAL here, but that includes a redundant check */
    FIELD_ADDR_GLOBAL( rem_buf, b, data[0] );
    INDEX_GLOBAL( rem_buf, rem_buf, off );	

    global_to_local_copy(rem_buf, buffer, len);

    memcpy(src, buffer, len);
    VARARRAY_DESTROY( buffer );
  }
}

void java_array_checkpoint(PT6Object4lang4java obj, PT10Checkpoint4lang2ti cp, jboolean reg) {
  jint length;
  jchar size, eleminfo;
  LT10Checkpoint4lang2ti lcp;
  //if (!is_java_array_global(sourceObject)) return 0;
  JAVA_ARRAY_INFO_GLOBAL(length, size, eleminfo, *(PTAjbyte *)&obj);
  CHECK_NULL_GLOBAL_IFBC(cp, "java_array_checkpoint");

  TAjbyte *arr;

  if (reg) {
    jboolean b = m8registerPT6Object4lang4javamT10Checkpoint4lang2ti(cp, obj);
    if (!b) return;
    lcp = TO_LOCAL(cp); // must be local
    ml13dumpJavaArrayPT6Object4lang4javaICmT10Checkpoint4lang2ti(lcp, obj, sizeof(java_array_header) + size * length, eleminfo);
  }

  arr = TO_LOCAL(*(PTAjbyte *) &obj); // must be local

  if (eleminfo == TI_ATOMIC_INDEX)
    return;
  else if (eleminfo == TI_LOCAL_OBJECT_INDEX || eleminfo == TI_GLOBAL_OBJECT_INDEX) {
    jbyte *rawbuf;
    FIELD_ADDR_LOCAL(rawbuf, arr, data[0]);
    if (eleminfo == TI_GLOBAL_OBJECT_INDEX) { // global elements
      PT6Object4lang4java *buf = (PT6Object4lang4java *) rawbuf;
      Lti5cdescmT6Object4lang4java cinfo;
      int i;
      for (i = 0; i < length; i++, buf++) {
	if (TO_LOCAL(*buf) != NULL) {
	  CLASS_INFO_GLOBAL(cinfo, ti5cdescmT6Object4lang4java, *buf);
	  cinfo->checkpoint(*buf, cp, ((jboolean) 1));
	}
      }
    } else { // local elements
      LT6Object4lang4java *buf = (LT6Object4lang4java *) rawbuf;
      PT6Object4lang4java elt;
      Lti5cdescmT6Object4lang4java cinfo;
      int i;
      for (i = 0; i < length; i++, buf++) {
	if (*buf != NULL) {
	  CLASS_INFO_LOCAL(cinfo, ti5cdescmT6Object4lang4java, *buf);
	  TO_GLOBALB(elt, MYBOX, *buf);
	  cinfo->checkpoint(elt, cp, ((jboolean) 1));
	}
      }
    }
  } else {
    jbyte *rawbuf;
    FIELD_ADDR_LOCAL(rawbuf, arr, data[0]);
    cp_methods *methods = ti_get_checkpoint_methods(eleminfo);
    int i;
    for (i = 0; i < length; i++, rawbuf += size) {
      methods->checkpoint(rawbuf, lcp);
    }
  }
}

void java_array_restore(LT6Object4lang4java obj, LT10Checkpoint4lang2ti cp) {
  jint length;
  jchar size, eleminfo;
  //if (!is_java_array_global(sourceObject)) return 0;
  TAjbyte *arr = (TAjbyte *) obj;
  PTAjbyte garr;
  TO_GLOBALB(garr, MYBOX, arr);
  JAVA_ARRAY_INFO_GLOBAL(length, size, eleminfo, garr);
  CHECK_NULL_LOCAL_IFBC(cp, "java_array_restore");

  if (eleminfo == TI_ATOMIC_INDEX)
    return;
  else if (eleminfo == TI_LOCAL_OBJECT_INDEX || eleminfo == TI_GLOBAL_OBJECT_INDEX) {
    jbyte *rawbuf;
    FIELD_ADDR_LOCAL(rawbuf, arr, data[0]);
    if (eleminfo == TI_GLOBAL_OBJECT_INDEX) { // global elements
      PT6Object4lang4java *buf = (PT6Object4lang4java *) rawbuf;
      int i;
      for (i = 0; i < length; i++, buf++) {
	*buf = ml16unswizzle_globalPT6Object4lang4javamT10Checkpoint4lang2ti(cp, *buf);
      }
    } else { // local elements
      LT6Object4lang4java *buf = (LT6Object4lang4java *) rawbuf;
      int i;
      for (i = 0; i < length; i++, buf++) {
	*buf = ml15unswizzle_localLT6Object4lang4javamT10Checkpoint4lang2ti(cp, *buf);
      }
    }
  } else {
    jbyte *rawbuf;
    FIELD_ADDR_LOCAL(rawbuf, arr, data[0]);
    cp_methods *methods = ti_get_checkpoint_methods(eleminfo);
    int i;
    for (i = 0; i < length; i++, rawbuf += size) {
      methods->restore(rawbuf, cp);
    }
  }
}

