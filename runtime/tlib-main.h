#ifndef _INCLUDE_TLIB_MAIN_H_
#define _INCLUDE_TLIB_MAIN_H_

/* this file sucks in various utilities to be compiled in with the tlibs */

#include <gp-trace/gp-trace.h>

#include "broadcast.c"
#include "java_string.c"
#include "native-file-utils.c"
#include "native-str-utils.c"
#include "native-utils.c"
#include "subtype.c"
#include "tally-memory.c"
#include "java_array_methods.c"
#include "primitive_desc.c"
#include "subtype-lib.c"
#include "ti_array_methods.c"
#include "exceptions.c"

#endif
