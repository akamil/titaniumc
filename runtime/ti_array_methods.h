#ifndef _INCLUDE_TI_ARRAY_METHODS_H_
#define _INCLUDE_TI_ARRAY_METHODS_H_

#include "primitives.h"
#include "layout!LT10Checkpoint4lang2ti.h"

void ti_array_checkpoint(void *, jint length, jchar size, jchar eleminfo, LT10Checkpoint4lang2ti);
void ti_array_restore(void *, jint length, jchar size, jchar eleminfo, LT10Checkpoint4lang2ti);


#endif /* !_INCLUDE_TI_ARRAY_METHODS_H_ */
