#include <stdlib.h>
#include <signal.h>
#include "native-stubs.h"

void free_resources ();
void printExitStats(int sglobal);

void m12exitInternalImT7Runtime4lang4java(PT7Runtime4lang4java me,
						 jint status)
{

#ifdef MEMORY_SHARED
  /* need to prevent more than one thread at a time from running this */
  static ti_lock_t exit_lock = ti_lock_decl_initializer;
  ti_lock(&exit_lock);
#endif

#ifdef COMM_AM2
  TIC_AM_LOCK();  /* make sure all pending AM ops are complete */
#endif

  printExitStats(0); /* not sglobal because we're not guaranteed that all boxes call this */

/* exit with correct return value */
#ifdef COMM_GASNET
  gasnet_exit(status);
#elif defined(COMM_AM2)
  if (usingAMSPMD) {
  /* add a pause to help ensure all final output has reached the console */
  fflush(stdout); fflush(stderr);
  tic_sched_yield(); sleep(1); tic_sched_yield();
  fflush(stdout); fflush(stderr); 
  #if defined(COMM_AMUDP) 
    AMUDP_SPMDExit(status);
  #elif defined(COMM_AMMPI)
    AMMPI_SPMDExit(status);
  #endif
  }
#endif

  free_resources();

#ifdef GLUNIX
  /* Glunix needs this to prevent orphaned processes */
  Glib_Signal(Glib_GetMyNpid(), VNN_ALL, SIGTERM);
#endif

  exit(status);

  abort(); /* we should never reach here */
}
     
void m2gcmT7Runtime4lang4java(PT7Runtime4lang4java me) {
  ti_gc();
}

jlong m10freeMemorymT7Runtime4lang4java(PT7Runtime4lang4java me) {
  jlong regionTotalHeapSize, regionHeapInUse;
  jlong freeSize;
  ti_get_region_stats(&regionTotalHeapSize, &regionHeapInUse);
  freeSize = regionTotalHeapSize - regionHeapInUse;
  #ifndef USE_GC_NONE
    freeSize += ti_gc_freeheapsize(); 
  #endif
  return freeSize;
}

jlong m11totalMemorymT7Runtime4lang4java(PT7Runtime4lang4java me) {
  jlong totalSize;
  ti_get_region_stats(&totalSize, NULL);
  #ifndef USE_GC_NONE
    totalSize += ti_gc_totalheapsize(); 
  #endif
  return totalSize;
}

unimplemented(m17traceInstructionsZmT7Runtime4lang4java,void,(PT7Runtime4lang4java me, jboolean on))
unimplemented(m16traceMethodCallsZmT7Runtime4lang4java,void,(PT7Runtime4lang4java me, jboolean on))
#if TLIB_VERSION_VAL >= TLIB_VERSION_VAL_1_4
unimplemented(m19availableProcessorsmT7Runtime4lang4java,jint,(PT7Runtime4lang4java me))
unimplemented(m9maxMemorymT7Runtime4lang4java,jlong,(PT7Runtime4lang4java me))
unimplemented(m16runFinalization0mT7Runtime4lang4java,void,())
#else /* tlib 1.0 */
unimplemented(m12buildLibNamePT6String4lang4javaPT6String4lang4javamT7Runtime4lang4java,PT6String4lang4java,(PT7Runtime4lang4java me, PT6String4lang4java pathname, PT6String4lang4java filename))
unimplemented(m24initializeLinkerInternalmT7Runtime4lang4java,PT6String4lang4java,(PT7Runtime4lang4java me))
unimplemented(m16loadFileInternalPT6String4lang4javamT7Runtime4lang4java,jboolean,(PT7Runtime4lang4java me, PT6String4lang4java filename))
unimplemented(m15runFinalizationmT7Runtime4lang4java,void,(PT7Runtime4lang4java me))
#endif

