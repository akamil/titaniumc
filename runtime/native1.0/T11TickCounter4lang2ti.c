#if 1
#include <gasnet_tools.h>

jlong m7currentmT11TickCounter4lang2ti() {
  gasnett_tick_t current = -1;
  
#if 0
  unsigned long long current = -1;
#ifdef __GNUC__
# ifdef __sparc__
  asm("rd %%tick,%%g1; stx %%g1,%0" : "=m" (current));
# endif /* __sparc__ */
# ifdef __i386__
  asm(".byte 0x0f, 0x31" : "=A" (current));
# endif /* __i386__ */
#endif /* __GNUC__ */
#endif
  assert(sizeof(gasnett_tick_t) <= sizeof(jlong));
  current = gasnett_ticks_now();
  return *(jlong *)&current;
}

jdouble m12ticksToNanosJmT11TickCounter4lang2ti(jlong ticks) {
  gasnett_tick_t cnt = *(gasnett_tick_t *)&ticks;
  return (jdouble)gasnett_ticks_to_ns(cnt);
}

jdouble m11granularitymT11TickCounter4lang2ti() {
  return (jdouble)gasnett_tick_granularityus();
}

jdouble m8overheadmT11TickCounter4lang2ti() {
  return (jdouble)gasnett_tick_overheadus();
}

#else /* no GASNet tools */

#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

jlong m7currentmT11TickCounter4lang2ti() {
  jlong retval;
  struct timeval tv;
  if (gettimeofday(&tv, NULL)) {
      perror("gettimeofday");
      abort();
  }
  retval = ((jlong)tv.tv_sec) * 1000000 + tv.tv_usec;
  return retval;
}

jdouble m13ticksToMicrosJmT11TickCounter4lang2ti(jlong ticks) {
  return (jdouble)ticks;
}

static double _tickcounter_metric[2];
static int _tickcounter_metricinit;
static double tickcounter_metric(unsigned int idx) {
  assert(idx <= 1);
  if (!_tickcounter_metricinit) {
    int i, ticks, iters = 1000, minticks = 10;
    jlong min = ((jlong)1) << 63;
    jlong start = m7currentmT11TickCounter4lang2ti();
    jlong last = start;
    for (i=0,ticks=0; i < iters || ticks < minticks; i++) {
      jlong x = m7currentmT11TickCounter4lang2ti();
      jlong curr = (x - last);
      if (curr > 0) { 
        ticks++;
        if (curr < min) min = curr;
      }
      last = x;
    }
    /* granularity */
    _tickcounter_metric[0] = ((double)(min*1000))/1000.0;
    /* overhead */
    _tickcounter_metric[1] = ((double)(last - start))/i;

    _tickcounter_metricinit = 1;
  }
  return _tickcounter_metric[idx];
}

jdouble m11granularitymT11TickCounter4lang2ti() {
  return (jdouble)tickcounter_metric(0);
}

jdouble m8overheadmT11TickCounter4lang2ti() {
  return (jdouble)tickcounter_metric(1);
}

#endif

