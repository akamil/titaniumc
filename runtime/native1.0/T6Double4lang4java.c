#include "native-str-utils.h"


typedef    T6Double4lang4java    Double;
typedef PT6Double4lang4java GP_Double;


GP_Double m7valueOfPT6String4lang4javamT6Double4lang4java(PT6String4lang4java s)
{
    const jdouble value = stringToDouble(s);
    
    GP_Double result;
    Double *newbie = (Double *) ti_malloc( sizeof( Double ) );
    newbie->class_info = &fi5cdescT6Double4lang4java;
    m6DoubleDcT6Double4lang4java( newbie, value );
    
    globalize( result, newbie );
    return result;
}


#if TLIB_VERSION_VAL >= TLIB_VERSION_VAL_1_4
/* MODIFICATION: by kamil on 7/08/05 -- rename to Double.doubleToRawLongBits() */
jlong m19doubleToRawLongBitsDmT6Double4lang4java( jdouble value )
#else /* tlib 1.0 */
jlong m16doubleToLongBitsDmT6Double4lang4java( jdouble value )
#endif
{
    /* !! This may not give the same bit representation
       as Java, particularly for Inf and NaN.
       */
    assert( sizeof(jdouble) == sizeof(jlong) );
    return *((jlong *) &value);
}


jdouble m16longBitsToDoubleJmT6Double4lang4java( jlong bits )
{
    assert( sizeof(jdouble) == sizeof(jlong) );
    return *((jdouble *) &bits);
}


PT6String4lang4java m8toStringDmT6Double4lang4java( jdouble value )
{
    return doubleToString( value );
}

void m12setPrecisionJmT6Double4lang4java( jlong sigdigits ) {
    setDoubleToStringPrecision((jint) sigdigits);
}
