#include <stdio.h>
#include "java_string.h"
#include "java_array_methods.h"
#include "ti_array_methods.h"

#define PTR PT6Object4lang4java
#define GET_PTR_PROC(p) GET_PROC(p)
#define PTR_TO_INT(a) ((jIntPointer) TO_LOCAL(a))
#define CONVERT_TO_FAKE_PTR(a, b, c) do { TO_GLOBALP(a, b, c); } while(0)
#define CONVERT_TO_REAL_PTR(a) ((void *) TO_LOCAL(a))
#define ADD_PTR_MARKER(a, b) a ## PT6Object4lang4java ## b
#define MY_PROC_REP COMM_GetProxyProcNumberForBoxNumber(MYBOX)

void m12objectToDataLT6Object4lang4javaLTAjbyteImT10Checkpoint4lang2ti(LT6Object4lang4java o, LTAjbyte b, jint len) {
  java_local_byte_array_write((void *) o, b, 0, len);
}

void ADD_PTR_MARKER(m13tiArrayToData, LTAjbyteImT10Checkpoint4lang2ti)(PTR p, LTAjbyte b, jint len) {
  java_local_byte_array_write((void *) CONVERT_TO_REAL_PTR(p), b, 0, len);
}

PTR m15objectToPointerPT6Object4lang4javamT10Checkpoint4lang2ti(PT6Object4lang4java o) {
  T6Object4lang4java *ptr = (T6Object4lang4java *) TO_LOCAL(o);
  PTR p;
  CONVERT_TO_FAKE_PTR(p, TO_PROC(o), ptr);
  return p;
}

PT6Object4lang4java ADD_PTR_MARKER(m15pointerToObject, mT10Checkpoint4lang2ti)(PTR p) {
  PT6Object4lang4java result;
  T6Object4lang4java *o = (T6Object4lang4java *) CONVERT_TO_REAL_PTR(p);
  TO_GLOBALP(result, GET_PTR_PROC(p), o);
  return result;
}

LT6Object4lang4java m17allocateJavaArrayLT6Region4lang2tiICmT10Checkpoint4lang2ti(LT6Region4lang2ti r, jint size, jchar eleminfo) {
  void *arr;
  if (eleminfo == TI_ATOMIC_INDEX) {
    if (r)
      arr = (void *) ralloc_atomic((Region) r, size);
    else
      arr = ti_malloc_atomic_huge(size);
  } else {
    if (r)
      arr = ralloc((Region) r, size);
    else
      arr = ti_malloc_huge(size);
  }
  return (LT6Object4lang4java) arr;
}

LT6Object4lang4java m14allocateObjectLT6Region4lang2tiImT10Checkpoint4lang2ti(LT6Region4lang2ti r, jint size) {
  if (r)
    // preserve old region -- don't allocate in my version, but in given version
    return (LT6Object4lang4java) ralloc((Region) r, size);
  else
    return (LT6Object4lang4java) ti_malloc(size);
}

void m13restoreObjectLT6Object4lang4javaLTAjbyteImT10Checkpoint4lang2ti(LT6Object4lang4java o, LTAjbyte b, jint len) {
  java_local_byte_array_read((void *) o, b, 0, len);
}

PTR m15allocateTiArrayLT6Region4lang2tiLTAjbyteICmT10Checkpoint4lang2ti(LT6Region4lang2ti r, LTAjbyte b, jint size, jchar eleminfo) {
  void *arr;
  PTR p;
  if (eleminfo == TI_ATOMIC_INDEX) {
    if (r)
      arr = (void *) ralloc_atomic((Region) r, size);
    else
      arr = ti_malloc_atomic_huge(size);
  } else {
    if (r)
      arr = ralloc((Region) r, size);
    else
      arr = ti_malloc_huge(size);
  }
  java_local_byte_array_read(arr, b, 0, size);
  CONVERT_TO_FAKE_PTR(p, MY_PROC_REP, arr);
  return p;
}

void ADD_PTR_MARKER(ml14restoreTiArray, ICCmT10Checkpoint4lang2ti)(LT10Checkpoint4lang2ti cp, PTR p, jint size, jchar elemsize, jchar eleminfo) {
  jbyte *arr = (jbyte *) CONVERT_TO_REAL_PTR(p);
  assert(size % elemsize == 0);
  ti_array_restore(arr, size / elemsize, elemsize, eleminfo, cp);
}

void ADD_PTR_MARKER(ml17checkpointTiArray, IICCmT10Checkpoint4lang2ti)(LT10Checkpoint4lang2ti cp, PTR p, jint start, jint size, jchar elemsize, jchar eleminfo) {
  jbyte *arr = (jbyte *) CONVERT_TO_REAL_PTR(p);
  arr += start;
  assert(size % elemsize == 0);
  ti_array_checkpoint(arr, size / elemsize, elemsize, eleminfo, cp);
}

void m16checkpointObjectPT6Object4lang4javaZmT10Checkpoint4lang2ti(PT10Checkpoint4lang2ti cp, PT6Object4lang4java obj, jboolean reg) {
  Lti5cdescmT6Object4lang4java tmp;
  CLASS_INFO_GLOBAL(tmp, ti5cdescmT6Object4lang4java, obj);
  tmp->checkpoint(obj, cp, reg);
}

void ml13restoreObjectLT6Object4lang4javamT10Checkpoint4lang2ti(LT10Checkpoint4lang2ti cp, LT6Object4lang4java obj) {
  Lti5cdescmT6Object4lang4java tmp;
  CLASS_INFO_LOCAL(tmp, ti5cdescmT6Object4lang4java, obj);
  tmp->restore(obj, cp);
}

PT6String4lang4java ADD_PTR_MARKER(m18pointerToHexString, mT10Checkpoint4lang2ti)(PTR p) {
  char tmp[sizeof(PTR) * 2 + 1];
  LT6String4lang4java ls;
  PT6String4lang4java gs;
  sprintf(tmp, "%p", CONVERT_TO_REAL_PTR(p));
  ls = java_string_build_8(tmp);
  globalize(gs, ls);
  return gs;
}

PTR m14parseHexStringPT6String4lang4javamT10Checkpoint4lang2ti(PT6String4lang4java s) {
  return m14parseHexStringIPT6String4lang4javamT10Checkpoint4lang2ti(MY_PROC_REP, s);
}

PTR m14parseHexStringIPT6String4lang4javamT10Checkpoint4lang2ti(jint proc, PT6String4lang4java s) {
  char *tmp = globalJstringToCstring(s);
  void *res;
  PTR p;
  sscanf(tmp, "%p", &res);
  CONVERT_TO_FAKE_PTR(p, proc, res);
  return p;
}

jint ADD_PTR_MARKER(m12pointerToInt, mT10Checkpoint4lang2ti)(PTR p) {
  return (jint) PTR_TO_INT(p);
}

void m10intToBytesLTAjbyteIImT10Checkpoint4lang2ti(LTAjbyte b, jint off, jint i) {
  java_local_byte_array_write((void *) &i, b, off, sizeof(jint));
}

void ADD_PTR_MARKER(m14pointerToBytesLTAjbyteI, mT10Checkpoint4lang2ti)(LTAjbyte b, jint off, PTR p) {
  void *v = CONVERT_TO_REAL_PTR(p);
  java_local_byte_array_write((void *) &v, b, off, sizeof(void *));
}

jint m10bytesToIntLTAjbyteImT10Checkpoint4lang2ti(LTAjbyte b, int off) {
  jint res;
  java_local_byte_array_read((void *) &res, b, off, sizeof(jint));
  return res;
}

PTR m14bytesToPointerLTAjbyteImT10Checkpoint4lang2ti(LTAjbyte b, int off) {
  return m14bytesToPointerLTAjbyteIImT10Checkpoint4lang2ti(b, off, MY_PROC_REP);
}

PTR m14bytesToPointerLTAjbyteIImT10Checkpoint4lang2ti(LTAjbyte b, int off, int proc) {
  void *v;
  PTR p;
  java_local_byte_array_read((void *) &v, b, off, sizeof(void *));
  CONVERT_TO_FAKE_PTR(p, proc, v);
  return p;
}

jint m13sizeofPointermT10Checkpoint4lang2ti() {
  return sizeof(void *);
}

extern const char *tiUniqueID;

jint m11getIdentityLTAjbyteImT10Checkpoint4lang2ti(LTAjbyte b, int off) {
  char *tmp =  (char *) tiUniqueID;
  int len = strlen(tmp);
  java_local_byte_array_write((void *) tmp, b, off, len * sizeof(char));
  return (jint) len;
}
