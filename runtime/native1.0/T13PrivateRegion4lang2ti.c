
struct T13PrivateRegion4lang2ti *newpregion(void)
{
  struct T6Region4lang2ti *r = newregion();
  struct T13PrivateRegion4lang2ti *pr = (struct T13PrivateRegion4lang2ti *)r;

  pr->class_info = &fi5cdescT13PrivateRegion4lang2ti;
  /* this is just an region allocator - constructor will be run by caller code */
  /* m13PrivateRegionmT13PrivateRegion4lang2ti(pr); */
  r->id = (jIntPointer) r;
  initregion(r);

  return pr;
}

void m6deletemT13PrivateRegion4lang2ti(PT13PrivateRegion4lang2ti me)
{
  Region r = (Region)(TO_LOCAL(me));

  if (r->rc)
    abort(); /* Should throw exception */

  deleteregion(r);
}

