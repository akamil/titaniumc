#include "native-str-utils.h"


typedef T5Float4lang4java     Float;
typedef PT5Float4lang4java GP_Float;


GP_Float m7valueOfPT6String4lang4javamT5Float4lang4java(PT6String4lang4java s)
{
    const jdouble value = stringToDouble(s);

    GP_Float result;
    Float *newbie = (Float *) ti_malloc( sizeof( Float ) );
    newbie->class_info = &fi5cdescT5Float4lang4java;
    m5FloatFcT5Float4lang4java( newbie, value );
    
    globalize( result, newbie );
    return result;
}

#if TLIB_VERSION_VAL >= TLIB_VERSION_VAL_1_4
/* MODIFICATION: by kamil on 7/08/05 -- rename to Float.floatToRawIntBits() */
jint m17floatToRawIntBitsFmT5Float4lang4java( jfloat value )
#else /* tlib 1.0 */
jint m14floatToIntBitsFmT5Float4lang4java( jfloat value )
#endif
{
    /* !! This may not give the same bit representation
       as Java, particularly for Inf and NaN.
       */
    assert( sizeof(jfloat) == sizeof(jint) );
    return *((jint *) &value);
}


jfloat m14intBitsToFloatImT5Float4lang4java( jint bits )
{
    assert( sizeof(jfloat) == sizeof(jint) );
    return *((jfloat *) &bits);
}


#if TLIB_VERSION_VAL < TLIB_VERSION_VAL_1_4
PT6String4lang4java m8toStringFmT5Float4lang4java( jfloat value )
{
    return doubleToString( value );
}
#endif
