#include "backend.h"


extern void m5printPT6String4lang4javamT5Trace4lang2ti(PT6String4lang4java s) {
#if defined(GASNET_TRACE)
  char *msg = globalJstringToCstring(s);
  ti_srcpos_freeze();
  ti_trace_printf(("%s", msg));
  ti_srcpos_unfreeze();
#endif
}

extern PT6String4lang4java m12getTraceMaskmT5Trace4lang2ti() {
#if defined(GASNET_TRACE)
  GP_JString string;
  LP_JString mask = java_string_build_8((char *)GASNETT_TRACE_GETMASK());
  globalize( string, mask );
  return string;
#else
  return globalJString_empty();
#endif
}

extern PT6String4lang4java m12getStatsMaskmT5Trace4lang2ti() {
#if defined(GASNET_TRACE)
  GP_JString string;
  LP_JString mask = java_string_build_8((char *)GASNETT_STATS_GETMASK());
  globalize( string, mask );
  return string;
#else
  return globalJString_empty();
#endif
}

extern void m12setTraceMaskPT6String4lang4javamT5Trace4lang2ti(PT6String4lang4java s) {
#if defined(GASNET_TRACE)
 char *newmask = globalJstringToCstring(s);
 GASNETT_TRACE_SETMASK(newmask);
#endif
}

extern void m12setStatsMaskPT6String4lang4javamT5Trace4lang2ti(PT6String4lang4java s) {
#if defined(GASNET_TRACE)
 char *newmask = globalJstringToCstring(s);
 GASNETT_STATS_SETMASK(newmask);
#endif
}

extern jboolean m13getTraceLocalmT5Trace4lang2ti() {
#if defined(GASNET_TRACE)
  return (jboolean)GASNETT_TRACE_GET_TRACELOCAL();
#else
  return (jboolean)0;
#endif
}

extern void m13setTraceLocalZmT5Trace4lang2ti(jboolean val) {
#if defined(GASNET_TRACE)
  GASNETT_TRACE_SET_TRACELOCAL(val);
#endif
}

