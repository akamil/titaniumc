
#define BULKIO_READFUNC_HEADER void m10readArray0PT6Object4lang4javaIImT19BulkDataInputStream2io2ti \
      ( PT19BulkDataInputStream2io2ti me, PT6Object4lang4java primjavaarray, jint arrayoffset, jint count)

#define BULKIO_READFUNC_CALLSUPER(me, array, offset, count)   \
      m9readFullyPTAjbyteIImT15DataInputStream2io4java(       \
        (* (PT15DataInputStream2io4java *) &(me)),            \
        (array), (offset), (count) )

#include "bulkio.c" /*  all the interesting stuff is in this file */
