#include "file-64.h" /* must come first */

#include <sys/stat.h>
#include <unistd.h>
#include "layout!PLT14FileDescriptor2io4java.h"
#include "native-file-utils.h"
#include "native-stubs.h"

typedef PT16RandomAccessFile2io4java GP_RandomAccessFile;
typedef  LT14FileDescriptor2io4java LP_FileDesc;
typedef  PT14FileDescriptor2io4java GP_FileDesc;
typedef PLT14FileDescriptor2io4java GP_LP_FileDesc;
typedef GP_type(GP_FileDesc) GP_GP_FileDesc;

#define getFileDescriptor m5getFDmT16RandomAccessFile2io4java

void m4openPT6String4lang4javaImT16RandomAccessFile2io4java( GP_RandomAccessFile me,
							     PT6String4lang4java name,
							     jint mode )
{
  jopen( getFileDescriptor( me ), name,
	 (mode & (jint) 0x2) ? (O_RDWR | O_CREAT) : O_RDONLY );
}


jint m4readmT16RandomAccessFile2io4java( GP_RandomAccessFile me )
{
  return jread( getFileDescriptor( me ) );
}


jint m9readBytesPTAjbyteIImT16RandomAccessFile2io4java( GP_RandomAccessFile me,
							PTAjbyte b, jint off, jint len )
{
  return jreadBytes( getFileDescriptor( me ), b, off, len );
}


void m5writeImT16RandomAccessFile2io4java( GP_RandomAccessFile me, jint b )
{
  jwrite( getFileDescriptor( me ), b );
}


void m10writeBytesPTAjbyteIImT16RandomAccessFile2io4java( GP_RandomAccessFile me,
							  PTAjbyte b, jint off, jint len )
{
  jwriteBytes( getFileDescriptor( me ), b, off, len );
}


jlong m14getFilePointermT16RandomAccessFile2io4java( GP_RandomAccessFile me )
{
  return jseek( getFileDescriptor( me ), 0, SEEK_CUR );
}

void m4seekJmT16RandomAccessFile2io4java( GP_RandomAccessFile me, jlong pos )
{
  jseek( getFileDescriptor( me ), pos, SEEK_SET );
}


jlong m6lengthmT16RandomAccessFile2io4java( GP_RandomAccessFile me )
{
  struct stat64 stats;

  if (fstat64( getFd( getFileDescriptor( me ) ), &stats ) == -1)
    tossIOException();
  else
    return (jlong) CONVERT_OFF64_TO_LONG_LONG(stats.st_size);
}


void m6close0mT16RandomAccessFile2io4java( GP_RandomAccessFile me )
{
  GP_GP_FileDesc fdRefPtr;
  GP_FileDesc globalNull, fdPtr;
  globalize(globalNull, NULL);

  fdPtr = getFileDescriptor(me);

  // Check if the file descriptor is invalid
  if(ISNULL_GLOBAL(fdPtr) || getFd(fdPtr) < 0) return;

  // Close out the file descriptor
  jclose( getFileDescriptor( me ) );

  //Invalidate the fd reference in the java stream
  FIELD_ADDR_GLOBAL(fdRefPtr, me, f2fdT16RandomAccessFile2io4java);
  PGASSIGN_GLOBAL_gp(fdRefPtr, globalNull);
}

#if TLIB_VERSION_VAL >= TLIB_VERSION_VAL_1_4
void m7initIDsmT16RandomAccessFile2io4java() { }

unimplemented(m9setLengthJmT16RandomAccessFile2io4java,void,(PT16RandomAccessFile2io4java var1, jlong var2))
#endif

