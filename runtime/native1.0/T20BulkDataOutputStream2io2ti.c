
#define BULKIO_WRITEFUNC_HEADER void m11writeArray0PT6Object4lang4javaIImT20BulkDataOutputStream2io2ti \
      ( PT20BulkDataOutputStream2io2ti me, PT6Object4lang4java primjavaarray, jint arrayoffset, jint count) 

#define BULKIO_WRITEFUNC_CALLSUPER(me, array, offset, count)   \
      m5writePTAjbyteIImT16DataOutputStream2io4java(           \
        (* (PT16DataOutputStream2io4java *) &(me)),            \
        (array), (offset), (count) )

#include "bulkio.c" /*  all the interesting stuff is in this file */
