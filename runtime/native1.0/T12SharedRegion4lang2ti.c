#include <backend.h>

struct T12SharedRegion4lang2ti *newsregion(void) {
  ti_region_threadinfo_t *info = ti_myregioninfo();
  info->rstart += 64*MYBOXPROC; /* stagger shared regions across cache lines */
  {
  struct T6Region4lang2ti *r = newregion();
  struct T12SharedRegion4lang2ti *sr = (struct T12SharedRegion4lang2ti *)r;
  int id;

  sr->class_info = &fi5cdescT12SharedRegion4lang2ti;
  /* this is just an region allocator - constructor will be run by caller code */
  /* m12SharedRegionmT12SharedRegion4lang2ti(sr); */
  info->nextSharedId += 2;
  id = info->nextSharedId + 1;
#ifndef BACKEND_UNIPROC
  {
    /* Find a free id */
    int firstid = id;
    Region *pMysregionidmap = info->sregionidmap;
    assert(((firstid & 0x1) == 1) && (firstid != 1));

    for (;;){
        if (id >= MAXSREGIONS) id = id % MAXSREGIONS;
        if (id == 1) id = 3;  /* 1 is GC stuff, skip it */

        if (!pMysregionidmap[id >> 1])
          break; /* found a free one */

        id += 2;

        if (id == firstid) {
            fprintf(stderr, "More than %d simultaneous shared regions - aborting\n",
                    (MAXSREGIONS >> 1) - 1);
            fflush(stderr);
            abort();
        }
    }

    pMysregionidmap[id >> 1] = (Region)r;
  }
#endif


  r->id = id;
  initregion(r);
  /* Synchronise to ensure that all procs have created the region
     before any proc returns (otherwise races are possible where
     one process sees objects in a not-yet-created-locally
     shared region) */
  barrier();

  return sr;
}}


void m6deletemT12SharedRegion4lang2ti(PT12SharedRegion4lang2ti me)
{
  /* Make sure we get the local representative of the region */
  Region r = MyGRegion(*(GRegion*)&me);
#ifndef BACKEND_UNIPROC
  int id = r->id;
#endif


  /* Don't start freeing memory before everyone gets here */
  barrier();

#ifdef BACKEND_UNIPROC
  if (r->rc)
    abort(); /* Should throw exception */
#endif


  deleteregion(r);

#ifndef BACKEND_UNIPROC
  /* Region id available again */
  ti_myregioninfo()->sregionidmap[id >> 1] = NULL;
#endif
}
