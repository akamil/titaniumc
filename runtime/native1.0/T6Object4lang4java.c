#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>

#include <class_header.h>
#include <native-stubs.h>
#include <ti-memory.h>
#include "java_array_methods.h"
#include "T9Cloneable4lang4java.h"

#ifdef HAVE_MONITORS
#include "layout!Ptic_monitor_t.h"
#endif


typedef PT6Object4lang4java GP_Object;
typedef LT6Object4lang4java LP_Object;

#define arraycopy m9arraycopyPT6Object4lang4javaIPT6Object4lang4javaIImT6System4lang4java
void arraycopy(GP_Object src, jint src_pos, GP_Object dst, jint dst_pos, jint length);

jboolean m7isLocalmT6Object4lang4java(PT6Object4lang4java me) {
  return !isNull(me) && isLocal(me);
}

jint m7creatormT6Object4lang4java(PT6Object4lang4java me) {
  CHECK_NULL_GLOBAL(me);
  return COMM_GetProxyProcNumberForBoxNumber(TO_BOX(me));
}

jint m8hashCodemT6Object4lang4java (PT6Object4lang4java p) {
  return TO_BOX(p) ^ ((int)(jUIntPointer) TO_LOCAL(p));
}

void m15object_finalizemT6Object4lang4java(PT6Object4lang4java p) {
#ifdef HAVE_MONITORS
  T6Object4lang4java *moribund;
  tic_monitor_t *monitor;
  
  localize(moribund, p);
  FIELD_ADDR_LOCAL(monitor, moribund, monitor);
  monitor_destroy(monitor);
#endif /* monitors */
}

void m8finalizemT6Object4lang4java(PT6Object4lang4java p) {
  /* AK (PR832): this does nothing, as the JLS requires */
}

void m6notifymT6Object4lang4java(PT6Object4lang4java p) {
if (PROCS > 1) {
#ifdef HAVE_MONITORS
  Ptic_monitor_t monitor;
  
  MONITOR_FETCH_INSTANCE_GLOBAL(monitor, p);
  monitor_notify(CAST_GPTR(monitor));
#else /* no monitors */
  fputs( "unimplemented native method: m6notifymT6Object4lang4java\n", stderr );
  exit(2);
#endif /* no monitors */
 }
}

void m9notifyAllmT6Object4lang4java(PT6Object4lang4java p) {
if (PROCS > 1) {
#ifdef HAVE_MONITORS
  Ptic_monitor_t monitor;
  
  MONITOR_FETCH_INSTANCE_GLOBAL(monitor, p);
  monitor_notify_all(CAST_GPTR(monitor));
#else /* no monitors */
  fputs( "unimplemented native method: m9notifyAllmT6Object4lang4java\n", stderr );
  exit(2);
#endif /* no monitors */
 }
}

extern void m5sleepJmT6Thread4lang4java(jlong);

void m4waitJmT6Object4lang4java(PT6Object4lang4java p, jlong millis) {
/* millis==0 means block indefinately
 * otherwise, it means block for about that many millis, max 
 */
if (PROCS > 1) {
#ifdef HAVE_MONITORS
  Ptic_monitor_t monitor;
  
  MONITOR_FETCH_INSTANCE_GLOBAL(monitor, p);
  if (millis == 0) {
    monitor_wait(CAST_GPTR(monitor), NULL);
    }
  else {
    ti_time_t tm;
    ti_set_wait_timer(&tm, millis);
    monitor_wait(CAST_GPTR(monitor), &tm);
    }
#else /* no monitors */
  fputs( "unimplemented native method: m4waitJmT6Object4lang4java\n", stderr );
  exit(2);
#endif /* no monitors */
}
else { /* single processor */
  if (millis == 0) {
    fputs( "ERROR: called java.lang.Object.wait(0) with Ti.numProcs() == 1", stderr);
    exit(2);
    }
  else m5sleepJmT6Thread4lang4java(millis);
  }
}


Region m8regionOfmT6Object4lang4java(GP_Object me) {
  RegionId rid =  PG2RegionId(me);

  if (rid == UNK_REGIONID)
    return NULL;
  else
    return RegionId2Region(rid);
}


static size_t allocateClone( Region r, const ti5cdescmT6Object4lang4java *info, LP_Object *duplicate )
{
  if (CLASS_IMPLEMENTS( info, fi5cdescT9Cloneable4lang4java ))
    {
      if (r) *duplicate = (LP_Object) ralloc( r, info->size );
      else *duplicate = (LP_Object) ti_malloc( info->size );
      return info->size;
    }
  else
    {
      m31throwCloneNotSupportedExceptionmT11NativeUtils4lang2ti();
      return 0;
    }
}


LP_Object ml10localClonemT6Object4lang4java( LP_Object me )
{
  const ti5cdescmT6Object4lang4java * const info = me->class_info;
  GP_Object srcobj;
  Region region = NULL; 
  globalize(srcobj, me);
  region = m8regionOfmT6Object4lang4java(srcobj); /* copy source's region */

  if (info->category == JavaArray) {
    /* array cloning needs to be handled specially */
    java_array_header *pheader = (java_array_header *)me;
    java_array_header *newarr;
    GP_Object destobj;
    jint len;
    jint sz;
    jchar eleminfo;
    int isShared = 1; /* if we ever add shared bits to the object rep, we can improve this */

    len = pheader->length;
    sz = pheader->size;
    eleminfo = pheader->elem_info;

    newarr = java_array_alloc(region, len, sz, eleminfo, isShared, NULL );

    globalize(destobj, (LP_Object)newarr);

    arraycopy(srcobj, 0, destobj, 0, len); /* use arraycopy to leverage all the built-in checks */

    return (LP_Object)newarr;
  }
  else {
    LP_Object duplicate;
    const size_t size = allocateClone( region, info, &duplicate );
  
    memcpy( duplicate, me, size );
    return duplicate;
  }
}


GP_Object m5clonemT6Object4lang4java( GP_Object me )
{
  ti5cdescmT6Object4lang4java *info;
  GP_Object srcobj = me;
  Region region = m8regionOfmT6Object4lang4java(srcobj); /* copy source's region */
  CLASS_INFO_GLOBAL( info, ti5cdescmT6Object4lang4java, me );
  
  if (info->category == JavaArray) {
    java_array_header header;
    java_array_header *pheader = &header;
    java_array_header *newarr;
    GP_Object destobj;
    jint len;
    jint sz;
    jchar eleminfo;
    int isShared = 1; /* if we ever add shared bits to the object rep, we can improve this */

    /* get the entire header at once to avoid multiple messages */
    global_to_local_copy( me, (char *)pheader, sizeof(java_array_header) );

    len = pheader->length;
    sz = pheader->size;
    eleminfo = pheader->elem_info;

    newarr = java_array_alloc(region, len, sz, eleminfo, isShared, NULL );

    srcobj = me;
    globalize(destobj, (LP_Object)newarr);

    arraycopy(srcobj, 0, destobj, 0, len); /* use arraycopy to leverage all the built-in checks */

    return destobj;

  }
  else {
    LP_Object duplicate;
    GP_Object destobj;

    const size_t size = allocateClone( region, info, &duplicate );
    char * const bytes = (char *) duplicate;
  
    global_to_local_copy( me, bytes, size );
    info->nullifyLocalFields( duplicate );

    globalize(destobj, duplicate);
    return destobj;
  }
}

     
PT5Class4lang4java m8getClassmT6Object4lang4java(GP_Object me) {
  PT5Class4lang4java class;
  ti5cdescmT6Object4lang4java *info;
  class_header *ch;
  CHECK_NULL_GLOBAL(me);
  CLASS_INFO_GLOBAL( info, ti5cdescmT6Object4lang4java, me);
  ch = (class_header *)info;

  assert(ch->category == Class || ch->category == JavaArray);

  TO_GLOBALB_STATIC(class, 0, (T5Class4lang4java *)ch); /* canonical Class objects are on P0 */
  return class;
}


void mi10checkpointmT6Object4lang4java(PT6Object4lang4java me, PT10Checkpoint4lang2ti cp, jboolean reg) { 
  PT5Class4lang4java class;
  ti5cdescmT6Object4lang4java *info;
  class_header *ch;
  CHECK_NULL_GLOBAL(me);
  CLASS_INFO_GLOBAL( info, ti5cdescmT6Object4lang4java, me);
  ch = (class_header *)info;

  if (ch->category == JavaArray) {
    java_array_checkpoint(me, cp, reg);
    return;
  }

  if (reg) {
    LT10Checkpoint4lang2ti lcp;
    CHECK_NULL_GLOBAL_IFBC(cp, "Object.internal_checkpoint");
    jboolean b = m8registerPT6Object4lang4javamT10Checkpoint4lang2ti(cp, me);
    if (!b) return;
    lcp = TO_LOCAL(cp); /* must be local */
    ml4dumpPT6Object4lang4javaImT10Checkpoint4lang2ti(lcp, me, (sizeof(T6Object4lang4java)));
  }
}


void mi7restoremT6Object4lang4java(LT6Object4lang4java me, LT10Checkpoint4lang2ti cp) { 
  PT5Class4lang4java class;
  ti5cdescmT6Object4lang4java *info;
  class_header *ch;
  CHECK_NULL_LOCAL(me);
  CLASS_INFO_LOCAL( info, ti5cdescmT6Object4lang4java, me);
  ch = (class_header *)info;

  if (ch->category == JavaArray) {
    java_array_restore(me, cp);
    return;
  }
}


#if TLIB_VERSION_VAL >= TLIB_VERSION_VAL_1_4
void m15registerNativesmT6Object4lang4java() { }
#endif

