#include "native-stubs.h"

unimplemented(m12compileClassPT5Class4lang4javamT8Compiler4lang4java,jboolean,(PT5Class4lang4java clazz))
unimplemented(m14compileClassesPT6String4lang4javamT8Compiler4lang4java,jboolean,(PT6String4lang4java string))
unimplemented(m7commandPT6Object4lang4javamT8Compiler4lang4java,PT6Object4lang4java,(PT6Object4lang4java any))
#if TLIB_VERSION_VAL >= TLIB_VERSION_VAL_1_4
unimplemented(m7disablemT8Compiler4lang4java,void,())

void m10initializemT8Compiler4lang4java() { }

void m15registerNativesmT8Compiler4lang4java() { }
#else /* tlib 1.0 */
unimplemented(m10initializemT8Compiler4lang4java,void,(void))
unimplemented(m6enablemT8Compiler4lang4java,void,(void))
unimplemented(m7disablemT8Compiler4lang4java,void,(void))
#endif

