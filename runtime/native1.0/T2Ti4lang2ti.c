#include "TBP1jint.h"
#include "domains.h"
#include "T9tiDomain17domains2ti.h"
#include "T9tiDomain27domains2ti.h"
#include "T9tiDomain37domains2ti.h"
#include "T13tiRectDomain17domains2ti.h"
#include "T13tiRectDomain27domains2ti.h"
#include "T13tiRectDomain37domains2ti.h"
#include <stdarg.h>
#include <titanium.h>


jint m8thisProcmT2Ti4lang2ti()
{   /* note - this native method is manually inlined by the compiler in code-call.cc */
    return MYPROC;
}

jint m8numProcsmT2Ti4lang2ti()
{   /* note - this native method is manually inlined by the compiler in code-call.cc */
    return PROCS;
}

void m4pollmT2Ti4lang2ti() { 
#ifdef COMM_AM2
  tic_poll();
#elif defined(BACKEND_LAPI)
  LAPI_Probe(lapi_hndl);
#else
  /* do nothing */
#endif
  }

int ti_never_gc = 0; /* TI_NOGC hard-disable setting */
static ti_lock_t ti_autogc_lock = ti_lock_decl_initializer;
int ti_autogc_suspendcnt = 0; /* number of local threads suspending GC */

void m14disable_autogcmT2Ti4lang2ti() { 
  if (ti_never_gc) return;
  ti_lock(&ti_autogc_lock);
    assert(ti_autogc_suspendcnt >= 0 && ti_autogc_suspendcnt < MYBOXPROCS);
    #ifndef USE_GC_NONE
      if (ti_autogc_suspendcnt == 0) assert(!GC_dont_gc);
      GC_dont_gc = 1; /* disable collections */
    #endif
    ti_autogc_suspendcnt++;
  ti_unlock(&ti_autogc_lock);
}

void m13enable_autogcmT2Ti4lang2ti() { 
  if (ti_never_gc) return;
  ti_lock(&ti_autogc_lock);
    assert(ti_autogc_suspendcnt > 0 && ti_autogc_suspendcnt <= MYBOXPROCS);
    ti_autogc_suspendcnt--;
    #ifndef USE_GC_NONE
      assert(GC_dont_gc);
      if (ti_autogc_suspendcnt == 0) GC_dont_gc = 0; /* re-enable collections */
    #endif
  ti_unlock(&ti_autogc_lock);
}

TBL1jint m6myTeammT2Ti4lang2ti(void)
{
  jint i;
  TBL1jint x;
  T8tiPoint17domains2ti zero, one, procs;

  mi9constructmT8tiPoint17domains2ti(zero, 0);
  mi9constructmT8tiPoint17domains2ti(one, 1);
  mi9constructmT8tiPoint17domains2ti(procs, PROCS);

  x = _ti_CONSTRUCT(jint, 1)("(myTeam)", NULL, m13tiRectDomain1T8tiPoint17domains2tiT8tiPoint17domains2tiT8tiPoint17domains2ticT13tiRectDomain17domains2ti(zero, procs, one), 1);
  for (i = 0; i < PROCS; i++) ASSIGN_LOCAL(x.A + i, i);
  return x;
}

#define shortcut( type, arity )  PT2ti7domains2ti ## type ## arity
#define D(n)    shortcut( Domain, n )

#if 0
#define define_printN(N)				\
							\
void printrd ## N (RD ## N R)				\
{							\
  T12tiRectDomain7domains2ti ## N ## _mt_println(R);	\
}							\
							\
void printd ## N (D(N) f0_d)				\
{							\
  T8tiDomain7domains2ti ## N ## _mt_println_G(f0_d);	\
}

define_printN(1)
define_printN(2)
define_printN(3)
#endif

typedef void (voidfn)(); 

/* This is for dumping arrays.  The first thing in the array is
assumed to be a pointer to a function that dumps the array. */
void ti_array_dump(voidfn **f)
{
  (*((void (*)(voidfn **, char *, char *)) *f))(f, NULL, NULL);
}

void ti_array_dump2(char *dest, voidfn **f)
{
  (*((void (*)(voidfn **, char *, char *)) *f))(f, dest, NULL);
}

void m7barriermT2Ti4lang2ti()
{ /* note - this native method is manually inlined by the compiler in code-call.cc */
    barrier();
}
