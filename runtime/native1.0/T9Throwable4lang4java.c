#include "java_string.h"
#include "layout!Pjint.h"
#include "layout!Pjlong.h"
#include "T8Demangle4util2ti.h"
#include "native-stubs.h"

#if HAVE_EXECINFO_H
  #include <execinfo.h>
#endif

#if defined(HAVE_BACKTRACE) && (defined(HAVE_BACKTRACE_SYMBOLS) || defined(ADDR2LINE_PATH))
#define TI_EXNBT 1
#endif

LT9Throwable4lang4java ml16fillInStackTracemT9Throwable4lang4java( LT9Throwable4lang4java me ) {
  return mlp16fillInStackTracemT9Throwable4lang4java(me);
}

int ti_exnbtenabled = -1;
LT9Throwable4lang4java mlp16fillInStackTracemT9Throwable4lang4java( LT9Throwable4lang4java me ) {
  #if TI_EXNBT
    #define BTMAX 1024
    void *btbuf[BTMAX];
    int numframes = 0;
    if (ti_exnbtenabled == -1) ti_exnbtenabled = !getenvMaster("TI_NOEXNBT");
    if (ti_exnbtenabled) numframes = backtrace(btbuf, BTMAX);
    if (numframes) {
      java_array_header * jarr;
      JAVA_ARRAY_ALLOC( jarr, 0, numframes*sizeof(void*), jbyte, 0, 1, NULL );
      memcpy(jarr+1, btbuf, numframes*sizeof(void*));
      globalize(me->f9backtraceT9Throwable4lang4java, (LT6Object4lang4java)jarr);
    } else
  #endif
    TO_GLOBALB(me->f9backtraceT9Throwable4lang4java, 0, NULL);
  
  return me;
}

void m16printStackTrace0LTAjbytePT11PrintStream2io4javamT9Throwable4lang4java(
    LTAjbyte stack, PT11PrintStream2io4java s) {
  CHECK_NULL_LOCAL(stack);
  CHECK_NULL_GLOBAL(s);
  #if TI_EXNBT
  { jint num;
    java_array_header * jarr = (java_array_header *)stack;
    void **btaddrs = (void **)(jarr+1);
 
    JAVA_ARRAY_LENGTH_LOCAL( num, stack );
    num /= sizeof(void*);
    assert(num > 0);

    if (num) {
      static ti_lock_t pst_lock = ti_lock_decl_initializer;
      static char xlstack[4096];
      char *pstack = xlstack;
      char **fnnames = NULL;
      int i;

      ti_lock(&pst_lock);

      #if HAVE_BACKTRACE_SYMBOLS
        fnnames = backtrace_symbols(btaddrs, num);
      #endif
      for (i=0; i < num && btaddrs[i]; i++) {
        #define XLBUF 1024
        static char xlstr[XLBUF];
        xlstr[0] = '\0';

        #ifdef ADDR2LINE_PATH /* use addr2line when available to retrieve symbolic info */
          { static char cmd[255];
            static char xlfn[XLBUF];
            static char xlpos[XLBUF];
            FILE *xlate;
            GP_JString fnstr;
            xlfn[0] = '\0';
            xlpos[0] = '\0';
            sprintf(cmd,"%s -f -e '%s' %p", ADDR2LINE_PATH, ti_exepath, btaddrs[i]);
            xlate = popen(cmd, "r");
            if (xlate) {
              char *p;
              fgets(xlfn, XLBUF, xlate);
              p = strchr(xlfn, '\n');
              if (p) *p = '\0';
              fgets(xlpos, XLBUF, xlate);
              p = strchr(xlpos, '\n');
              if (p) *p = '\0';
              pclose(xlate);
            }
            globalize(fnstr, java_string_build_8(xlfn));
            fnstr = m8demanglePT6String4lang4javamT8Demangle4util2ti(fnstr);
            sprintf(xlstr, "\tat %s(%s)", globalJstringToCstring(fnstr), xlpos);
          }
        #else
          sprintf(xlstr, "\tat %s", (fnnames?fnnames[i]:"???""(""???"")"));
        #endif
        /* blacklist some functions that are used for dumping the stack and would just be confusing */
        if (!(strstr(xlstr, "fillInStackTrace") && strstr(xlstr,"Throwable")) &&
            !strstr(xlstr, "crash_backtrace")) { 
            GP_JString Pmesg;
            LP_JString mesg = java_string_build_8(xlstr);
            globalize(Pmesg, mesg);
            /* output each line as we get it, in case this is an infinite recursion */
            m7printlnPT6String4lang4javamT11PrintStream2io4java( s, Pmesg );
        }
      }
      if (fnnames) free(fnnames);
      ti_unlock(&pst_lock);
    } 
  }
  #else
    abort(); /* should never reach here */
  #endif
}

#if TLIB_VERSION_VAL >= TLIB_VERSION_VAL_1_4
unimplemented(m18getStackTraceDepthmT9Throwable4lang4java,jint,(PT9Throwable4lang4java var1))
unimplemented(m20getStackTraceElementImT9Throwable4lang4java,PT17StackTraceElement4lang4java,(PT9Throwable4lang4java var1, jint var2))
#endif

