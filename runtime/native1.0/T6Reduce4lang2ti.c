void m5traceILT6String4lang4javaLT6String4lang4javamT6Reduce4lang2ti(jint isScan, 
 LT6String4lang4java opname,
 LT6String4lang4java type) {
  #ifdef TI_TRACE
   char *copname = localJstringToCstring(opname);
   char *ctype = localJstringToCstring(type);
   ti_trace_printf(("COLLECTIVE %s%s(%s)", (isScan?"Scan.":"Reduce."), copname, ctype));
   ti_free(copname);
   ti_free(ctype);
  #endif
}

