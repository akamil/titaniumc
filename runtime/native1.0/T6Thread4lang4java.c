#include "native-stubs.h"
#include <unistd.h>
#include <time.h>
#include <errno.h>
#if defined(HAVE_NANOSLEEP) || defined(HAVE_NSLEEP)
#include <sys/time.h>
#endif
#ifdef HAVE_SCHED_YIELD
#include <sched.h>
#endif

void m5sleepJmT6Thread4lang4java(jlong t) { /* in milliseconds */
#if HAVE_NANOSLEEP
  struct timespec tm;
  struct timespec tmremaining;
  tm.tv_sec = t / 1000;       /* in seconds */
  tm.tv_nsec = (t % 1000) * 1000000;  /* in nanoseconds */
 
  while(nanosleep(&tm, &tmremaining) == -1 && errno == EINTR)
    tm = tmremaining;

#elif HAVE_NSLEEP
  struct timestruc_t tm;
  struct timestruc_t tmremaining;
  tm.tv_sec = t / 1000;       /* in seconds */
  tm.tv_nsec = (t % 1000) * 1000000;  /* in nanoseconds */
 
  while(nsleep(&tm, &tmremaining) == -1 && errno == EINTR)
    tm = tmremaining;

#elif HAVE_USLEEP
  if (t < 1000) { /* SunOS limit for this function is 1 million usecs */	
    usleep(t*1000); /* in microsecs */
    }
  else { /* must use larger granularity */
    sleep( t / 1000 ); /* in seconds */
    usleep((t % 1000)*1000); /* in microsecs */
    }
#else
  sleep( t / 1000 ); /* can't give the user exactly what they want - do our best */
#endif
  }

void m5yieldmT6Thread4lang4java() {
#if HAVE_SCHED_YIELD
  sched_yield();
#else
  sleep(0); 
#endif
  }


void m5startmT6Thread4lang4java(PT6Thread4lang4java var1) {
  fputs("ERROR in java.lang.Thread.start: dynamic thread creation currently unsupported in Titanium\n", stderr);
  exit(2);
}
unimplemented(m5stop0PT6Object4lang4javamT6Thread4lang4java,void,(PT6Thread4lang4java me, PT6Object4lang4java p))
unimplemented(m7isAlivemT6Thread4lang4java,jboolean,(PT6Thread4lang4java me))
unimplemented(m12setPriority0ImT6Thread4lang4java,void,(PT6Thread4lang4java me, jint p))
unimplemented(m16countStackFramesmT6Thread4lang4java,jint,(PT6Thread4lang4java me))
unimplemented(m8suspend0mT6Thread4lang4java,void,(PT6Thread4lang4java me))
unimplemented(m7resume0mT6Thread4lang4java,void,(PT6Thread4lang4java me))
#if TLIB_VERSION_VAL >= TLIB_VERSION_VAL_1_4
unimplemented(m13isInterruptedZmT6Thread4lang4java,jboolean,(PT6Thread4lang4java me, jboolean p))
unimplemented(m9holdsLockPT6Object4lang4javamT6Thread4lang4java,jboolean,(PT6Object4lang4java p))
unimplemented(m10interrupt0mT6Thread4lang4java,void,(PT6Thread4lang4java me))

void m15registerNativesmT6Thread4lang4java() { }
#else /* tlib 1.0 */
unimplemented(m13currentThreadmT6Thread4lang4java,PT6Thread4lang4java,(void))
#endif

