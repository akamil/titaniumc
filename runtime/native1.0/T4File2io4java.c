#include "file-64.h" /* must come first */

#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include "java_string.h"

/*#define DEBUG_FILE */

#define isFile m7isFile0mT4File2io4java
#define isDirectory m12isDirectory0mT4File2io4java

/*------------------------------------------------------------------------------------*/

typedef GP_type(GP_JString) GP_GP_JString;

const char * getFilename(PT4File2io4java me) {
  GP_GP_JString field;
  GP_JString path;

  CHECK_NULL_GLOBAL( me );

  FIELD_ADDR_GLOBAL( field, me, f4pathT4File2io4java );
  FENCE_PRE_READ();
  DEREF_GLOBAL_gp( path, field );
  FENCE_POST_READ();

  return globalJstringToCstring( path );
  }

/*------------------------------------------------------------------------------------*/
jlong m7length0mT4File2io4java(PT4File2io4java me) {
  struct stat64 buf;
  const char *path;
  int temp;

  path = getFilename(me);
  /* Get data associated with pathname */   
  temp = stat64( path, &buf );
  if (temp) {
    #ifdef DEBUG_FILE
      fprintf(stderr, "Failed to stat '%s' in File.length0()\n", path);
    #endif
    return 0; 
    }
  else return (jlong) CONVERT_OFF64_TO_LONG_LONG(buf.st_size);
  }
/*------------------------------------------------------------------------------------*/
jboolean m7exists0mT4File2io4java(PT4File2io4java me) {
  struct stat buf;
  const char *path;

  path = getFilename(me);

  if (stat( path, &buf )) return 0;
  else return 1;
  }
/*------------------------------------------------------------------------------------*/
jboolean m12isDirectory0mT4File2io4java(PT4File2io4java me) {
  struct stat buf;
  const char *path;

  path = getFilename(me);

  if (stat( path, &buf )) {
    #ifdef DEBUG_FILE
      fprintf(stderr, "Failed to stat '%s' in File.isDirectory0()\n", path);
    #endif
    return 0;
    }
  else return S_ISDIR(buf.st_mode);
  }
/*------------------------------------------------------------------------------------*/
jboolean m7isFile0mT4File2io4java(PT4File2io4java me) {
  struct stat buf;
  const char *path;

  path = getFilename(me);

  if (stat( path, &buf )) {
    #ifdef DEBUG_FILE
      fprintf(stderr, "Failed to stat '%s' in File.isFile0()\n", path);
    #endif
    return 0;
    }
  else return S_ISREG(buf.st_mode);  
  }
/*------------------------------------------------------------------------------------*/
jlong m13lastModified0mT4File2io4java(PT4File2io4java me) {
  struct stat buf;
  const char *path;

  path = getFilename(me);

  if (stat( path, &buf )) {
    #ifdef DEBUG_FILE
      fprintf(stderr, "Failed to stat '%s' in File.isFile0()\n", path);
    #endif
    return 0;
    }
  else return (jlong) buf.st_mtime;  
  }
/*------------------------------------------------------------------------------------*/
jboolean m10isAbsolutemT4File2io4java(PT4File2io4java me) {
  const char *path;

  path = getFilename(me);
  return (path[0] == '/'); /* this is a hack, but no other portable mechanism seems to exist */
  }
/*------------------------------------------------------------------------------------*/
jboolean m8canRead0mT4File2io4java(PT4File2io4java me) {
  const char *path;
  int fd;

  if (!isFile(me)) return 0; /* doesn't exist, or not a file */

  path = getFilename(me);
  fd = open64( path, O_RDONLY | O_LARGEFILE);
  if (fd >= 0) {
    if (close(fd)) 
      fprintf(stderr, "Error closing descriptor of '%s' in File.canRead0()\n", path);
    return 1;
    }
  else return 0;
  }
/*------------------------------------------------------------------------------------*/
jboolean m9canWrite0mT4File2io4java(PT4File2io4java me) {
  const char *path;
  int fd;

  if (!isFile(me)) return 0; /* doesn't exist, or not a file */

  path = getFilename(me);
  fd = open64( path, O_WRONLY | O_LARGEFILE); 
  if (fd >= 0) {
    if (close(fd)) 
      fprintf(stderr, "Error closing descriptor of '%s' in File.canWrite0()\n", path);
    return 1;
    }
  else return 0;
  }
/*------------------------------------------------------------------------------------*/
jboolean m9renameTo0PT4File2io4javamT4File2io4java(PT4File2io4java me, PT4File2io4java dest) {
  const char *frompath;
  const char *topath;

  frompath = getFilename(me);
  topath = getFilename(dest);
  if (rename(frompath, topath)) {
    #ifdef DEBUG_FILE
      fprintf(stderr, "Failed while renaming '%s' -> '%s' in File.renameTo0()\n", frompath, topath);
    #endif
    return 0;
    }
  else return 1;
  }
/*------------------------------------------------------------------------------------*/
jboolean m6mkdir0mT4File2io4java(PT4File2io4java me) {
  const char *path;

  path = getFilename(me);

  if (mkdir(path, S_IRWXU | S_IRWXG | S_IRWXO )) {
    #ifdef DEBUG_FILE
      fprintf(stderr, "Failed to create directory '%s' in File.mkdir0()\n", path);
    #endif
    return 0;
    }
  else return 1;
  }
/*------------------------------------------------------------------------------------*/
jboolean m7delete0mT4File2io4java(PT4File2io4java me) {
  const char *path;

  path = getFilename(me);

  if (remove(path)) {
    #ifdef DEBUG_FILE
      fprintf(stderr, "Failed to remove '%s' in File.delete()\n", path);
    #endif
    return 0;
    }
  else return 1;
  }
/*------------------------------------------------------------------------------------*/
PTAPT6String4lang4java m5list0mT4File2io4java(PT4File2io4java me) {
  const char *path;
  int approxfilecount;
  DIR* dir;
  struct dirent* entry;
  int filecount;
  char **files;
  PTAPT6String4lang4java returnlist;
  int i;

  if (!isDirectory(me)) {
    TAPT6String4lang4java *strings;
	  PTAPT6String4lang4java result;
    #ifdef DEBUG_FILE
      fprintf(stderr, "File.list0('%s') called on non-directory\n", path);
    #endif
    JAVA_ARRAY_ALLOC( strings, NULL, 0, GP_JString, TI_GLOBAL_OBJECT_INDEX, 1, NULL );
	  globalize( result, strings );
	  return result;
    }

  path = getFilename(me);

  dir = opendir(path);
  if (!dir) { /* failed to open dir */
    TAPT6String4lang4java *strings;
	  PTAPT6String4lang4java result;
    #ifdef DEBUG_FILE
      fprintf(stderr, "File.list0() failed to opendir('%s')\n", path);
    #endif
    JAVA_ARRAY_ALLOC( strings, NULL, 0, GP_JString, TI_GLOBAL_OBJECT_INDEX, 1, NULL );
	  globalize( result, strings );
	  return result;
    }

  approxfilecount = 0; /* approximate the number of files we need space for */
  while (readdir(dir)) { /* use readdir_r for thread-safety */
    approxfilecount++;
    }

  files = (char **)ti_malloc(sizeof(char*) * approxfilecount);

  /* do the following carefully to prevent corrupting memory */
  /* in the presence of asynchronous updates to the directory */
  rewinddir(dir);
  filecount = 0;
  while ((entry = readdir(dir)) && filecount < approxfilecount) { /* read the actual files */
    if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")) { /* remove . and .. */
      files[filecount] = ti_malloc(strlen(entry->d_name)+2);
      strcpy(files[filecount], entry->d_name);
      filecount++;
      }
    }

  returnlist = java_strings_build(filecount, files);

  for (i = 0; i < filecount; i++) ti_free(files[i]);
  ti_free(files);

  return returnlist;
  }
/*------------------------------------------------------------------------------------*/
