#include "native-stubs.h"
#include "native-file-utils.h"
#include "native-utils.h"

typedef PT16FileOutputStream2io4java GP_FileOutputStream;
typedef PT14FileDescriptor2io4java GP_FileDesc;
typedef GP_type(GP_FileDesc) GP_GP_FileDesc;


static PT14FileDescriptor2io4java getFileDescriptor( GP_FileOutputStream me )
{
    GP_GP_FileDesc field;
    GP_FileDesc fileDescriptor;

    CHECK_NULL_GLOBAL( me );

    FIELD_ADDR_GLOBAL( field, me, f2fdT16FileOutputStream2io4java );
    FENCE_PRE_READ();
    DEREF_GLOBAL_gp( fileDescriptor, field );
    FENCE_POST_READ();
    return fileDescriptor;
}


void m6close0mT16FileOutputStream2io4java( GP_FileOutputStream me )
{
  GP_GP_FileDesc fdRefPtr;
  GP_FileDesc globalNull, fdPtr;
  globalize(globalNull, NULL);

  fdPtr = getFileDescriptor(me);

  // Check if the file descriptor is invalid
  if(ISNULL_GLOBAL(fdPtr) || getFd(fdPtr) < 0) return;

  // Close out the file descriptor
  jclose( getFileDescriptor( me ) );

  //Invalidate the fd reference in the java stream
  FIELD_ADDR_GLOBAL(fdRefPtr, me, f2fdT16FileOutputStream2io4java);
  PGASSIGN_GLOBAL_gp(fdRefPtr, globalNull);
}


void m4openPT6String4lang4javamT16FileOutputStream2io4java( GP_FileOutputStream me,
							    PT6String4lang4java name )
{
  jopen( getFileDescriptor( me ), name,
	 O_CREAT | O_WRONLY | O_TRUNC );
}


void m10writeBytesPTAjbyteIImT16FileOutputStream2io4java( GP_FileOutputStream me,
							  PTAjbyte b, jint off, jint len )
{
  jwriteBytes( getFileDescriptor( me ), b, off, len );
}


void m5writeImT16FileOutputStream2io4java( GP_FileOutputStream me, jint byte )
{
  jwrite( getFileDescriptor( me ), byte );
}

#if TLIB_VERSION_VAL >= TLIB_VERSION_VAL_1_4
void m7initIDsmT16FileOutputStream2io4java() { }

unimplemented(m10openAppendPT6String4lang4javamT16FileOutputStream2io4java,void,(PT16FileOutputStream2io4java var1, PT6String4lang4java var2))
#endif

