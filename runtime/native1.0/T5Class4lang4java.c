#include "type_header.h"
#include "native-stubs.h"

/* DOB: 
   Key invariants to this code:

   every single Class instance in existence is embedded as the first field of a 
   type_header-family (class_info) struct, which means all T5Class*'s are 
   interchangeable with type_header*'s for the corresponding type 
   (note the first field of type_header is a T5Class4lang4java, 
   NOT a PT5Class4lang4java or LT5Class4lang4java).

   Since these data types are statically allocated by construction, they can
   be safely assumed to live at the corresponding static address on all processors. 
   Thus, given a local ptr to a Class or type_header object, we can forge 
   a global pointer to the same corresponding object on P0, or vice-versa. 
   (Java requires that each source-level type have a unique Class object so 
   equality on Class references amounts to checking type equality - the "canonical" 
   Class objects passed to the user always live on P0).
*/

PT11ClassLoader4lang4java m15getClassLoader0mT5Class4lang4java(PT5Class4lang4java className) {
  PT11ClassLoader4lang4java temp;
  TO_GLOBALB(temp, 0, NULL);
  return temp;
}

PT6String4lang4java m8getName0mT5Class4lang4java(PT5Class4lang4java me) {
  PT6String4lang4java name;
  type_header *th;
  CHECK_NULL_GLOBAL(me);
  th = (type_header *)TO_LOCAL_STATIC(me);
  globalize(name, th->common_header.class_name);
  return name;
}

PT5Class4lang4java m13getSuperclassmT5Class4lang4java(PT5Class4lang4java me) {
  type_header *th;
  class_header *superch;
  PT5Class4lang4java superclass;
  CHECK_NULL_GLOBAL(me);
  th = (type_header *)TO_LOCAL_STATIC(me);

  if (th->common_header.category == Class || th->common_header.category == JavaArray) 
    superch = (class_header *)th->class_header.super;
  else 
    superch = NULL;

  TO_GLOBALB_STATIC(superclass, 0, (T5Class4lang4java *)superch); /* canonical Class objects are on P0 */
  return superclass;
}

PTAPT5Class4lang4java m13getInterfacesmT5Class4lang4java(PT5Class4lang4java me) {
  PTAPT5Class4lang4java result;
  TAPT5Class4lang4java *lresult;
  type_header *th;
  const struct interface_header * const *implements = NULL;

  CHECK_NULL_GLOBAL(me);
  th = (type_header *)TO_LOCAL_STATIC(me);

  if (th->common_header.category == Class || th->common_header.category == JavaArray)
    implements = th->class_header.implements;
  else if (th->common_header.category == Interface) 
    implements = th->interface_header.implements;

  if (implements && implements[0]) {
    PT5Class4lang4java *data;
    int numitems = 0;
    int i;

    while (implements[numitems]) numitems++; /* count */
    data = (PT5Class4lang4java *)ti_malloc((numitems+1) * sizeof(PT5Class4lang4java)); 
    for (i=0; i < numitems; i++) {
      TO_GLOBALB_STATIC(data[i], 0, (T5Class4lang4java *)implements[i]); /* canonical interfaces on proc 0 */
    }
    JAVA_ARRAY_BUILD(lresult, numitems, data, TI_GLOBAL_OBJECT_INDEX, 1);
    ti_free(data);
  }
  else { /* no interfaces - return zero length array */
    JAVA_ARRAY_ALLOC(lresult, NULL, 0, PT5Class4lang4java, TI_GLOBAL_OBJECT_INDEX, 1, NULL);
  }

  globalize(result, lresult);
  return result;
}

jboolean m11isInterfacemT5Class4lang4java(PT5Class4lang4java me) {
  type_header *th;
  CHECK_NULL_GLOBAL(me);
  th = (type_header *)TO_LOCAL_STATIC(me);

  return (jboolean)(th->common_header.category == Interface);
}

#if TLIB_VERSION_VAL >= TLIB_VERSION_VAL_1_4
jboolean m7isArraymT5Class4lang4java(PT5Class4lang4java me) {
  type_header *th;
  CHECK_NULL_GLOBAL(me);
  th = (type_header *)TO_LOCAL_STATIC(me);

  return (jboolean)(th->common_header.category == JavaArray);
}

jboolean m11isPrimitivemT5Class4lang4java(PT5Class4lang4java me) {
  type_header *th;
  CHECK_NULL_GLOBAL(me);
  th = (type_header *)TO_LOCAL_STATIC(me);

  return (jboolean)(th->common_header.category == Value &&
                    !(th->common_header.flags & typeflag_Immutable));
}
#endif

PT5Class4lang4java m7forNamePT6String4lang4javamT5Class4lang4java(PT6String4lang4java className) {
  CHECK_NULL_GLOBAL(className);
  return ti_get_class(className, 0);
}

PT6Object4lang4java m17newInstanceNativemT5Class4lang4java(PT5Class4lang4java me) {
  type_header *th;
  LT6Object4lang4java lretval = NULL;
  PT6Object4lang4java retval;

  CHECK_NULL_GLOBAL(me);

  th = (type_header *)TO_LOCAL_STATIC(me);

  /* only permitted for Object subclasses */
  if (th->common_header.category == Class) {
    class_header *ch = (class_header *)th;
    TypeFlags flags = ch->flags;

    /* check legality */
    if ( !(flags & (typeflag_Immutable | typeflag_Abstract)) &&
         ch->newInstance != NULL) {

      /* Allocate object */
      lretval = ti_malloc(ch->size);
      lretval->class_info = (void *)ch;
      tally_memory(ch->size, 1);

      /* run default constructor */
      ch->newInstance(lretval); 
    }
  }
  
  TO_GLOBALB(retval, MYBOX, lretval);
  return retval;
}

#if TLIB_VERSION_VAL >= TLIB_VERSION_VAL_1_4
unimplemented(m8forName0PT6String4lang4javaZPT11ClassLoader4lang4javamT5Class4lang4java,PT5Class4lang4java,(PT6String4lang4java var1, jboolean var2, PT11ClassLoader4lang4java var3))
unimplemented(m10isInstancePT6Object4lang4javamT5Class4lang4java,jboolean,(PT5Class4lang4java var1, PT6Object4lang4java var2))
unimplemented(m16isAssignableFromPT5Class4lang4javamT5Class4lang4java,jboolean,(PT5Class4lang4java var1, PT5Class4lang4java var2))
unimplemented(m16getComponentTypemT5Class4lang4java,PT5Class4lang4java,(PT5Class4lang4java var1))
unimplemented(m12getModifiersmT5Class4lang4java,jint,(PT5Class4lang4java var1))
unimplemented(m10getSignersmT5Class4lang4java,PTAPT6Object4lang4java,(PT5Class4lang4java var1))
unimplemented(m10setSignersPTAPT6Object4lang4javamT5Class4lang4java,void,(PT5Class4lang4java var1, PTAPT6Object4lang4java var2))
unimplemented(m17getDeclaringClassmT5Class4lang4java,PT5Class4lang4java,(PT5Class4lang4java var1))
unimplemented(m20getProtectionDomain0mT5Class4lang4java,PT16ProtectionDomain8security4java,(PT5Class4lang4java var1))
unimplemented(m20setProtectionDomain0PT16ProtectionDomain8security4javamT5Class4lang4java,void,(PT5Class4lang4java var1, PT16ProtectionDomain8security4java var2))
unimplemented(m17getPrimitiveClassPT6String4lang4javamT5Class4lang4java,PT5Class4lang4java,(PT6String4lang4java var1))
unimplemented(m18getDeclaredFields0ZmT5Class4lang4java,PTAPT5Field7reflect4lang4java,(PT5Class4lang4java var1, jboolean var2))
unimplemented(m19getDeclaredMethods0ZmT5Class4lang4java,PTAPT6Method7reflect4lang4java,(PT5Class4lang4java var1, jboolean var2))
unimplemented(m24getDeclaredConstructors0ZmT5Class4lang4java,PTAPT11Constructor7reflect4lang4java,(PT5Class4lang4java var1, jboolean var2))
unimplemented(m19getDeclaredClasses0mT5Class4lang4java,PTAPT5Class4lang4java,(PT5Class4lang4java var1))
unimplemented(m23desiredAssertionStatus0PT5Class4lang4javamT5Class4lang4java,jboolean,(PT5Class4lang4java var1))

void m15registerNativesmT5Class4lang4java() { }
#endif

