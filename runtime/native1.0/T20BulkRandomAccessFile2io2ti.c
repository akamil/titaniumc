
#define BULKIO_READFUNC_HEADER void m10readArray0PT6Object4lang4javaIImT20BulkRandomAccessFile2io2ti \
      ( PT20BulkRandomAccessFile2io2ti me, PT6Object4lang4java primjavaarray, jint arrayoffset, jint count)

#define BULKIO_READFUNC_CALLSUPER(me, array, offset, count)   \
      m9readFullyPTAjbyteIImT16RandomAccessFile2io4java(      \
        (* (PT16RandomAccessFile2io4java *) &(me)),           \
        (array), (offset), (count) )

#define BULKIO_WRITEFUNC_HEADER void m11writeArray0PT6Object4lang4javaIImT20BulkRandomAccessFile2io2ti \
      ( PT20BulkRandomAccessFile2io2ti me, PT6Object4lang4java primjavaarray, jint arrayoffset, jint count) 

#define BULKIO_WRITEFUNC_CALLSUPER(me, array, offset, count)   \
      m5writePTAjbyteIImT16RandomAccessFile2io4java(           \
        (* (PT16RandomAccessFile2io4java *) &(me)),            \
        (array), (offset), (count) )

#include "bulkio.c" /*  all the interesting stuff is in this file */
