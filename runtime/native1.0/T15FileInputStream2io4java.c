#include "ti_config.h"
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <unistd.h>
#ifdef HAVE_SYS_FILIO_H
  #include <sys/filio.h>
#else
  #include <sys/ioctl.h>
#endif
#include "native-file-utils.h"
#include "native-utils.h"

typedef PT15FileInputStream2io4java GP_FileInputStream;
typedef PT14FileDescriptor2io4java GP_FileDesc;
typedef GP_type(GP_FileDesc) GP_GP_FileDesc;


static GP_FileDesc getFileDescriptor( GP_FileInputStream me )
{
    GP_GP_FileDesc field;
    GP_FileDesc fileDescriptor;

    CHECK_NULL_GLOBAL( me );

    FIELD_ADDR_GLOBAL( field, me, f2fdT15FileInputStream2io4java );
    FENCE_PRE_READ();
    DEREF_GLOBAL_gp( fileDescriptor, field );
    FENCE_POST_READ();
    return fileDescriptor;
}


jint m9availablemT15FileInputStream2io4java( GP_FileInputStream me )
{
    const int fd = getFd( getFileDescriptor( me ) );
    const jlong current =
      (jlong) CONVERT_OFF64_TO_LONG_LONG(lseek64(fd, CONVERT_LSEEK64_OFFSET(0),
						 SEEK_CUR));
    jint nread;

    if (current != -1) {
	struct stat stats;
	if (!fstat( fd, &stats ) && S_ISREG( stats.st_mode ))
	    return stats.st_size - current;
    }
  #if PLATFORM_OS_CYGWIN || PLATFORM_OS_BLRTS || PLATFORM_OS_CATAMOUNT
    return 0; /* no such ioctl() ? */
  #else
    if (ioctl( fd, FIONREAD, &nread ))
	tossIOException();
    else
	return nread;
  #endif
}


void m6close0mT15FileInputStream2io4java( GP_FileInputStream me )
{
  GP_GP_FileDesc fdRefPtr;
  GP_FileDesc globalNull, fdPtr;
  globalize(globalNull, NULL);

  fdPtr = getFileDescriptor(me);
  
  // Check if the file descriptor is invalid
  if(ISNULL_GLOBAL(fdPtr) || getFd(fdPtr) < 0) return;

  // Close out the file descriptor
  jclose( getFileDescriptor( me ) );

  //Invalidate the fd reference in the java stream
  FIELD_ADDR_GLOBAL(fdRefPtr, me, f2fdT15FileInputStream2io4java);
  PGASSIGN_GLOBAL_gp(fdRefPtr, globalNull);
}


void m4openPT6String4lang4javamT15FileInputStream2io4java(
    GP_FileInputStream me,
    PT6String4lang4java name )
{
    jopen( getFileDescriptor( me ), name, O_RDONLY );
}


jint m4readmT15FileInputStream2io4java( GP_FileInputStream me )
{
  return jread( getFileDescriptor( me ) );
}


jint m9readBytesPTAjbyteIImT15FileInputStream2io4java( GP_FileInputStream me,
						       PTAjbyte b, jint off, jint len )
{
  return jreadBytes( getFileDescriptor( me ), b, off, len );
}


jlong m4skipJmT15FileInputStream2io4java( GP_FileInputStream me, jlong n )
{
  /* should this return less than "n" if we seek past EOF? */
  jseek( getFileDescriptor( me ), n, SEEK_CUR );
  return n;
}

#if TLIB_VERSION_VAL >= TLIB_VERSION_VAL_1_4
void m7initIDsmT15FileInputStream2io4java() { }
#endif

