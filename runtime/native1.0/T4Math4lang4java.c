#include <math.h>
#include "ti_config.h"


#define func1( fl, f )								\
jdouble m ## fl ## f ## DmT4Math4lang4java ( jdouble value )			\
{										\
    return f( value );								\
}

#define func2( fl, f )								\
jdouble m ## fl ## f ## DDmT4Math4lang4java ( jdouble x, jdouble y )	\
{										\
    return f( x, y );								\
}

/* code-genned calls to these native methods are automatically inlined in code-call.cc */
func1( 4, acos )
func1( 4, asin )
func1( 4, atan )
func1( 4, ceil )
func1( 3, cos )
func1( 3, exp )
func1( 5, floor )
func1( 3, log )
func1( 4, rint )
func1( 3, sin )
func1( 4, sqrt )
func1( 3, tan )

func2( 13, IEEEremainder )
func2( 5, atan2 )
func2( 3, pow )

/* -------------------------------------------------------------------- */
/* intpow(): a fast version of pow() optimized for integer arguments
 * Copyright 2001, Dan Bonachea <bonachea@cs.berkeley.edu>
 *  beats the pow() in gcc-3.01's libmath by about 2x on Linux and 4x Solaris for integer arguments
 */

/* some helper macros that do exponentiation by shifting */
/* constraints: 
 *  x_const == 2 ^ (2 ^ y_shift)
 *  x_const ^ y_bound fits in 63 bits
 *  y_bound << y_shift fits in 31 bits
 */
#define X_CASEP(x_const, y_bound, y_shift) \
   case  x_const: if (y <= y_bound) return (double) (((int64_t)1) << (y<<y_shift)); \
   case -x_const: if (y <= y_bound) return (y&0x1)? \
                                          (double)-(((int64_t)1) << (y<<y_shift)): \
                                          (double) (((int64_t)1) << (y<<y_shift));
/* constraints: 
 *  x_const == 2 ^ y_mult
 *  x_const ^ y_bound fits in 63 bits
 *  y_bound * y_mult fits in 31 bits
 */
#define X_CASEM(x_const, y_bound, y_mult) \
   case  x_const: if (y <= y_bound) return (double) (((int64_t)1) << (y*y_mult)); \
   case -x_const: if (y <= y_bound) return (y&0x1)? \
                                          (double)-(((int64_t)1) << (y*y_mult)): \
                                          (double) (((int64_t)1) << (y*y_mult));
jdouble m3powIImT4Math4lang4java ( jint x, jint y ) {
  static jdouble zero = 0.0;
  switch (y) {
    case  0: return 1.0;
    case  1: return (double)x;
    case -1: return 1.0/(double)x;
    default: 
      if (y < 0) {
        switch (x) {
          case 0: return 1.0/zero; /* avoid a compiler warning */
          case 1: return 1.0;
        }
      } else {
        switch (x) { /* handle some easy cases thanks to bit-shifting */
          case    0: return 0.0;
          case    1: return 1.0;
          case   -1: return (y&0x1)?-1.0:1.0;
          case    2: if (y <= 62) return         (double) (((int64_t)1) << y);
          case   -2: if (y <= 62) return (y&0x1)?(double)-(((int64_t)1) << y): 
                                                 (double) (((int64_t)1) << y);
          X_CASEP(4,31,1);
          X_CASEM(8,20,3);
          X_CASEP(16,15,2);
          X_CASEM(32,12,5);
          X_CASEM(64,10,6);
        }
      }
  } 
  { /* optimized for integer exponent */
    register double temp = (y>0 ? (x) : (y=-y, 1.0/x));
    register double retval = 1.0;
    while (y > 1) {
      if (y&0x1) retval *= temp;
      y = y >> 1;
      temp *= temp;
    }
    return retval * temp;
  }
}
/* -------------------------------------------------------------------- */

