#include <errno.h>
#include <fcntl.h>
#include "T14FileDescriptor2io4java.h"
#include "layout!Pjint.h"
#include "native-utils.h"
#include "native-stubs.h"

#if TLIB_VERSION_VAL >= TLIB_VERSION_VAL_1_4

unimplemented(m4syncmT14FileDescriptor2io4java,void,(PT14FileDescriptor2io4java var1))
void m7initIDsmT14FileDescriptor2io4java() {}

#else /* tlib 1.0 */

PT14FileDescriptor2io4java
m12initSystemFDPT14FileDescriptor2io4javaImT14FileDescriptor2io4java(
    PT14FileDescriptor2io4java fdObj, jint desc )
{
  CHECK_NULL_GLOBAL( fdObj );
  {
    Pjint field;
    FIELD_ADDR_GLOBAL( field, fdObj, f2fdT14FileDescriptor2io4java );
    FENCE_PRE_WRITE();
    ASSIGN_GLOBAL_jint( field, desc );
    FENCE_POST_WRITE();
    return fdObj;
  }
}


jboolean m5validmT14FileDescriptor2io4java(
    PT14FileDescriptor2io4java me )
{
  CHECK_NULL_GLOBAL( me );
  {
    Pjint field;
    jint fd;
    
    FIELD_ADDR_GLOBAL( field, me, f2fdT14FileDescriptor2io4java );
    FENCE_PRE_READ();
    DEREF_GLOBAL_jint( fd, field );
    FENCE_POST_READ();
    
    fcntl( fd, F_GETFL );    
    return errno != EBADF;
  }
}
#endif

