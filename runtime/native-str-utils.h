#ifndef _INCLUDE_TLIB_NATIVE_STR_UTILS_H_
#define _INCLUDE_TLIB_NATIVE_STR_UTILS_H_

#include "layout!PT6String4lang4java.h"
#include "primitives.h"


void tossNumberFormatException( PT6String4lang4java ) __attribute__((noreturn));

jdouble stringToDouble( PT6String4lang4java );
PT6String4lang4java doubleToString( jdouble );

void setDoubleToStringPrecision(jint sigdigits);

#endif
