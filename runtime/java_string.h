#ifndef _INCLUDE_JAVA_STRING_H_
#define _INCLUDE_JAVA_STRING_H_

#include "layout!PTAPT6String4lang4java.h"
#include "layout!PT6String4lang4java.h"
#include "layout!LT6String4lang4java.h"
#include "primitives.h"


typedef LT6String4lang4java LP_JString;
typedef PT6String4lang4java GP_JString;


extern LP_JString java_string_build_16( int, jchar [] );
extern LP_JString java_string_build_8(        char [] );

extern PTAPT6String4lang4java java_strings_build( int, char *[] );
extern char *localJstringToCstring( LP_JString );
extern char *globalJstringToCstring( GP_JString );

/* intern a string in the local java.lang.String intern table */
extern LP_JString java_string_localintern(LP_JString);

extern jboolean globalJString_equalto( GP_JString str1, GP_JString str2);

/* efficiently return a non-interned empty string, local to the caller */
extern LP_JString localJString_empty();
extern GP_JString globalJString_empty();

#endif
