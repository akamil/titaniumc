#include "backend.h"
#include <primitives.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define _gptrace_c
#include <gp-trace/real.h>


static FILE *gp_trace_file;


void gp_trace_open()
{
  const char *command = getenv( "GP_TRACER" );
  if (!command) command = "gp-tracer";
  gp_trace_file = popen( command, "w" );
  
  if (errno)
    {
      const int code = errno;
      fprintf( stderr, "cannot launch gp-tracer: %s\n", strerror( code ) );
      exit( code );
    }

  fprintf( gp_trace_file, "%d\n", MYPROC );
}


void gp_trace_close()
{
  pclose( gp_trace_file );
}


void gp_trace_message( const char file[], int line, const char function[],
		       const char action[], int wasLocal )
{
  fprintf( gp_trace_file, "%s\t%d\t%s\t%s\t%d\n",
	   file, line, function, action, wasLocal );
}
