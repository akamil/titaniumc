#ifndef _include_gp_trace_real_h_
#define _include_gp_trace_real_h_

#ifdef _include_gp_trace_stub_h_
  #ifdef _gptrace_c
    /* need to "undo" the effects of stub when compiling gp-trace.c */
    #undef gp_trace_open
    #undef gp_trace_close
    #undef gp_trace_event
  #else
    #error included both gp-trace/real.h and gp-trace/stub.h
  #endif
#endif

void gp_trace_open();
void gp_trace_close();

#define gp_trace_event( action, local )  gp_trace_message( __FILE__, __LINE__, __PRETTY_FUNCTION__, action, local )
void gp_trace_message( const char [], int, const char [], const char [], int );


#endif /* !_include_gp_trace_real_h_ */
