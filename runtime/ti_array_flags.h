#ifndef _include_ti_array_flags_h_
#define _include_ti_array_flags_h_

#include "backend.h"

#ifndef DUMPABLE_ARRAYS
#define DUMPABLE_ARRAYS 0
#endif

#ifndef EXPLICITLY_STORE_CREATOR
#define EXPLICITLY_STORE_CREATOR 0
#endif

#ifndef EXPLICITLY_STORE_ALLOCATION_SITE
#define EXPLICITLY_STORE_ALLOCATION_SITE 0
#endif

#endif /* !_include_ti_array_flags_h_ */
