#ifndef TC_NATIVE_STR_UTILS
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include "fp-utils.h"
#include "ti-gc.h"
#include "java_array.h"
#include "native-str-utils.h"

#include "T6String4lang4java.h"
#include "layout!PT6String4lang4java.h"

#define tossNumberFormatException m26throwNumberFormatExceptionPT6String4lang4javamT11NativeUtils4lang2ti
#endif

int ti_isinf(double d) {
  uint64_t bits = *(uint64_t*)&d;
  return bits == 0x7FF0000000000000ull || 
         bits == 0xFFF0000000000000ull;
}

int ti_isnan(double d) {
  uint64_t bits = *(uint64_t*)&d;
  return ((bits & 0x7FF0000000000000ull) == 0x7FF0000000000000ull) && !ti_isinf(d);
}

#ifndef TC_NATIVE_STR_UTILS
jdouble stringToDouble( GP_JString str )
{
      const char * begin = globalJstringToCstring( str );
      char *end;
      jdouble result;

      while (*begin && isspace(*begin)) begin++; /* eat leading ws */
      if (!*begin) tossNumberFormatException( str ); /* empty */
      result = strtod( begin, &end );
      while (*end && isspace(*end)) end++; /* eat trailing ws */

      if (begin == end || *end) /* bad string or trailing garbage */ 
	tossNumberFormatException( str );
      else
	return result;

      abort(); /* should never reach here */
}
#endif

static int doubleToString_sigdigits = 0;

#ifndef TC_NATIVE_STR_UTILS
void setDoubleToStringPrecision(jint sigdigits) {
  doubleToString_sigdigits = (int) sigdigits;
}
#endif

static void doubleToChars(double value, char *buffer) {
    /* ensure the correct Java-compliant spelling for special values (20.10.15) */
    if (ti_isnan(value)) strcpy( buffer, "NaN");
    else if (ti_isinf(value)) {
      if (value < 0.0) strcpy( buffer, "-Infinity");
      else strcpy( buffer, "Infinity");
    } else {
      char temp[ 32 ];
      if (doubleToString_sigdigits <= 0) /* default is max precision available */
        g_fmt( temp, value );
      else {
        /* use C string conversion */
        sprintf( temp, "%.*g", doubleToString_sigdigits, value );
      }

      /* Java's non-special doubles always contain a decimal point and 
       * have at least one digit before and after the decimal point
       * the '+' character should never appear, and 'e' is always 'E'
       */
      if (strchr(temp,'e')) *strchr(temp,'e') = 'E';
      while (strchr(temp,'+')) {
        char *p = strchr(temp,'+');
        *p = '\0';
        strcpy(buffer,temp);
        strcat(buffer,p+1);
        strcpy(temp,buffer);
      }

      if (temp[0] == '.') {
        strcpy(buffer,"0.");
        strcat(buffer, temp+1);
      } else if (!strncmp(temp,"-.",2)) {
        strcpy(buffer,"-0.");
        strcat(buffer,temp+2);
      } else if (!strchr(temp,'.')) {
        char *p = strchr(temp,'E');
        if (p) {
          *p = '\0';
          strcpy(buffer,temp);
          strcat(buffer,".0");
          strcat(buffer,"E");
          strcat(buffer,p+1);
        } else {
          strcpy(buffer,temp);
          strcat(buffer,".0");
        }
      } else {
        strcpy(buffer, temp);
        if (buffer[strlen(buffer)-1] == '.') strcat(buffer, "0");
      }
    }
}

#ifndef TC_NATIVE_STR_UTILS
PT6String4lang4java doubleToString( jdouble value )
{
    PT6String4lang4java result;
    
    char buffer[ 32 ];
    doubleToChars(value, buffer);
    
    globalize( result, java_string_build_8( buffer ) );
    return result;
}
#endif
