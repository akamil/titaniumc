#ifdef TALLY_MEMORY
#include "tally-memory.h"


ti_lock_t memoryTallyLock;

unsigned sharedChunks;
unsigned nonsharedChunks;
unsigned long sharedBytes;
unsigned long nonsharedBytes;


void init_memory_tally()
{
  memoryTallyLock = ti_lock_initializer;
}


void print_memory_tally()
{
  printf( "memory tally: shared: %u allocations; %lu bytes\n"
	  "memory tally: nonshared: %u allocations; %lu bytes\n",
	  sharedChunks, sharedBytes, nonsharedChunks, nonsharedBytes );
}


#endif
