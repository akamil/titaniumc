#ifndef _INCLUDE_INSTANCE_HEADER_
#define _INCLUDE_INSTANCE_HEADER_

#include "gp-type.h"
#include "backend.h"


struct class_header;

typedef struct instance_header
{
    struct class_header * class_info;
#ifdef HAVE_MONITORS
  tic_monitor_t monitor;
#endif /* HAVE_MONITORS */
} instance_header;

typedef GP_type( instance_header )  GP_instance;

#endif
