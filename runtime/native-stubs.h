#ifndef _INCLUDE_TLIB_NATIVE_STUBS_H_
#define _INCLUDE_TLIB_NATIVE_STUBS_H_

#include <stdlib.h>
#include <stdio.h>
#include "primitives.h"
#include "java_string.h"
#include "T8Demangle4util2ti.h"

/* avoid a missing return warning on compilers that care */
#if defined(__digital__)
  #pragma message disable (missingreturn)
#elif defined(_SGI_COMPILER_VERSION)
  #pragma set woff 1116
#endif

#define unimplemented(func, ret, args)                                                 \
ret func args {                                                                        \
  GP_JString fnstr;     /* dump initial message, in case we happen to crash below */   \
  fputs( "unimplemented native method: " # func "\n", stderr );                        \
  globalize(fnstr, java_string_build_8(#func));                                        \
  fnstr = m8demanglePT6String4lang4javamT8Demangle4util2ti(fnstr);                     \
  fprintf(stderr, "ERROR: The native method %s has not been implemented.\n"            \
                  "Please email a request to titanium-devel@cs.berkeley.edu.\n",       \
                  globalJstringToCstring(fnstr));                                      \
  exit(2);                                                                             \
}

#define identity(func, ret, args)  ret func args { return x; }


#endif /* !_INCLUDE_TLIB_NATIVE_STUBS_H_ */
