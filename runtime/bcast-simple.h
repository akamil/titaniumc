#ifndef _include_bcast_simple_h_
#define _include_bcast_simple_h_

#include "backend.h"
#include "bcast-common.h"


#define BROADCAST_SEND( result, type, value )		\
  do {							\
    BUFFER = (value);					\
    barrier();						\
    (result) = (value);					\
  } while (0)


#define _BROADCAST_RECEIVE( result, type, sender, primtype )	\
  do {							\
    GP_type( type ) remote;				\
    TO_GLOBALP_STATIC( remote, (sender), (type *) &BUFFER );	\
    barrier();						\
    DEREF_GLOBAL_ ## primtype ( (result), remote );	\
  } while (0)

#define BROADCAST_RECEIVE_jboolean(result, type, sender) \
   _BROADCAST_RECEIVE ( result, type, sender, jboolean)
#define BROADCAST_RECEIVE_jbyte(result, type, sender) \
   _BROADCAST_RECEIVE ( result, type, sender, jbyte)
#define BROADCAST_RECEIVE_jchar(result, type, sender) \
   _BROADCAST_RECEIVE ( result, type, sender, jchar)
#define BROADCAST_RECEIVE_jdouble(result, type, sender) \
   _BROADCAST_RECEIVE ( result, type, sender, jdouble)
#define BROADCAST_RECEIVE_jfloat(result, type, sender) \
   _BROADCAST_RECEIVE ( result, type, sender, jfloat)
#define BROADCAST_RECEIVE_jint(result, type, sender) \
   _BROADCAST_RECEIVE ( result, type, sender, jint)
#define BROADCAST_RECEIVE_jlong(result, type, sender) \
   _BROADCAST_RECEIVE ( result, type, sender, jlong)
#define BROADCAST_RECEIVE_jshort(result, type, sender) \
   _BROADCAST_RECEIVE ( result, type, sender, jshort)
#define BROADCAST_RECEIVE_lp(result, type, sender) \
   _BROADCAST_RECEIVE ( result, type, sender, lp)
#define BROADCAST_RECEIVE_gp(result, type, sender) \
   _BROADCAST_RECEIVE ( result, type, sender, gp)
#define BROADCAST_RECEIVE_bulk(result, type, sender) \
   _BROADCAST_RECEIVE ( result, type, sender, bulk)


#define BROADCAST_END( result, type, sender )  (CYCLE = !CYCLE)


#endif /* !_include_bcast_simple_h_ */
