#include <zlib/zlib.h>
#include "zip_util.h"
#include "native-utils.h"
#include "native-file-utils.h"

#include "layout!PTAjbyte.h"
#include "layout!Pjbyte.h"
#include "layout!Pjint.h"
#include "layout!TAjbyte.h"

#define jlong_zero ((jlong) 0)

#define DEF_MEM_LEVEL 8

jlong m4initIIZmT8Deflater3zip4util4java(jint level, jint strategy, jboolean nowrap)
{
  z_stream *strm = calloc(1, sizeof(z_stream));
  
  if (strm == 0) {
    JNU_ThrowOutOfMemoryError(env, 0);
    return jlong_zero;
  } else {
    char *msg;
    switch (deflateInit2(strm, level, Z_DEFLATED,
			 nowrap ? -MAX_WBITS : MAX_WBITS,
			 DEF_MEM_LEVEL, strategy)) {
    case Z_OK:
      return ptr_to_jlong(strm);
    case Z_MEM_ERROR:
      free(strm);
      JNU_ThrowOutOfMemoryError(env, 0);
      return jlong_zero;
    case Z_STREAM_ERROR:
      free(strm);
      JNU_ThrowIllegalArgumentException(env, 0);
      return jlong_zero;
    default:
      msg = strm->msg;
      free(strm);
      JNU_ThrowInternalError(env, msg);
      return jlong_zero;
    }
  }
}

void m13setDictionaryJPTAjbyteIImT8Deflater3zip4util4java(jlong strm, PTAjbyte b, jint off, jint len)
{
  Pjbyte gbuf;
  jbyte *buf;
  int res;
  int directly_addressable = isDirectlyAddressable(b);
  VARARRAY_DECLARE( jbyte, buffer, len );

  JAVA_ARRAY_CHECK_GLOBAL( b, off, len, "in m13setDictionaryJPTAjbyteIImT8Deflater3zip4util4java()" );

  if(directly_addressable) {
    FIELD_ADDR_LOCAL( buf, (TAjbyte *)TO_LOCAL(b), data[0] );
    INDEX_LOCAL( buf, buf, off ); 
  } else {
    VARARRAY_CREATE( jbyte, buffer, len );
    buf = buffer;

    /* we could use JAVA_ARRAY_ADDR_GLOBAL here, but that includes a redundant check */
    FIELD_ADDR_GLOBAL( gbuf, b, data[0] );
    INDEX_GLOBAL( gbuf, gbuf, off );      

    global_to_local_copy( gbuf, buffer, len );
  }


  if (buf == 0) {/* out of memory */
    return;
  }

  res = deflateSetDictionary((z_stream *)jlong_to_ptr(strm), (Bytef *) buf/* + off*/, len);

  if(!directly_addressable)
    VARARRAY_DESTROY( buffer );
  switch (res) {
  case Z_OK:
    break;
  case Z_STREAM_ERROR:
    JNU_ThrowIllegalArgumentException(env, 0);
    break;
  default:
    JNU_ThrowInternalError(env, ((z_stream *)jlong_to_ptr(strm))->msg);
    break;
  }
}

jint m12deflateBytesPTAjbyteIImT8Deflater3zip4util4java(PT8Deflater3zip4util4java me, PTAjbyte b, jint off, jint len)
{

  z_stream *strm;
  Pjlong strm_long_ptr;
  jlong strm_long;

  CHECK_NULL_GLOBAL_IFBC( me, "in m12deflateBytesPTAjbyteIImT8Deflater3zip4util4java()" );
  JAVA_ARRAY_CHECK_GLOBAL( b, off, len, "in m12deflateBytesPTAjbyteIImT8Deflater3zip4util4java()" );

  FIELD_ADDR_GLOBAL(strm_long_ptr, me, f4strmT8Deflater3zip4util4java);
  DEREF_GLOBAL_jlong(strm_long, strm_long_ptr);
  strm = jlong_to_ptr_s(strm_long);

  if (strm == 0) {
    JNU_ThrowNullPointerException(env, 0);
    return 0;
  } else {
    PPTAjbyte gbuf_ptr;
    PTAjbyte gbuf;
    Pjbyte in_temp_buf, out_temp_buf;
    Pjint off_ptr, len_ptr, level_ptr, strategy_ptr;
    jint this_off, this_len;
    Pjboolean setParams_ptr;
    jboolean setParams;
    Bytef *in_buf;
    Bytef *out_buf;
    int res;
    int out_directly_addressable, in_directly_addressable;
    VARARRAY_DECLARE( jbyte, inbuffer, len );
    VARARRAY_DECLARE( jbyte, outbuffer, len);

    FIELD_ADDR_GLOBAL(off_ptr, me, f3offT8Deflater3zip4util4java);
    DEREF_GLOBAL_jint(this_off, off_ptr);

    FIELD_ADDR_GLOBAL(len_ptr, me, f3lenT8Deflater3zip4util4java);
    DEREF_GLOBAL_jint(this_len, len_ptr);
    
    /* check if b array is NULL */
    if(isNull(b)) {
      return 0;
    }

    FIELD_ADDR_GLOBAL(gbuf_ptr, me, f3bufT8Deflater3zip4util4java);
    DEREF_GLOBAL_gp(gbuf, gbuf_ptr);

    /* check if gbuf is NULL */
    if(isNull(gbuf)) {
      return 0;
    }
    
    in_directly_addressable = isDirectlyAddressable(gbuf);

    /* set in_buf */
    if(in_directly_addressable) {
      jbyte *in_buf_jbyte;
      FIELD_ADDR_LOCAL( in_buf_jbyte, (TAjbyte *)TO_LOCAL(gbuf), data[0] );
      in_buf = (Bytef *) in_buf_jbyte;
    } else {
      VARARRAY_CREATE( jbyte, inbuffer, len );
      in_buf = (Bytef *) inbuffer;
      /* we could use JAVA_ARRAY_ADDR_GLOBAL here, but that includes a redundant check */
      FIELD_ADDR_GLOBAL( in_temp_buf, gbuf, data[0] );
      global_to_local_copy( in_temp_buf, inbuffer, this_off+this_len );
    }

    out_directly_addressable = isDirectlyAddressable(b);

    /* set out_buf */
    if(out_directly_addressable) {
      jbyte *out_buf_jbyte;
      FIELD_ADDR_LOCAL( out_buf_jbyte, (TAjbyte *)TO_LOCAL(b), data[0] );
      out_buf = (Bytef *) out_buf_jbyte;
    } else {
      VARARRAY_CREATE( jbyte, outbuffer, len );
      out_buf = (Bytef *) outbuffer;
      /* we could use JAVA_ARRAY_ADDR_GLOBAL here, but that includes a redundant check */
      FIELD_ADDR_GLOBAL( out_temp_buf, b, data[0] );
      global_to_local_copy( out_temp_buf, outbuffer, off+len );
    }    

    FIELD_ADDR_GLOBAL(setParams_ptr, me, f9setParamsT8Deflater3zip4util4java);
    DEREF_GLOBAL_jboolean(setParams, setParams_ptr);

    if (setParams) {
      int level, strategy;

      FIELD_ADDR_GLOBAL(level_ptr, me, f5levelT8Deflater3zip4util4java);
      DEREF_GLOBAL_jint(level, level_ptr);      

      FIELD_ADDR_GLOBAL(strategy_ptr, me, f8strategyT8Deflater3zip4util4java);
      DEREF_GLOBAL_jint(strategy, strategy_ptr);  

      strm->next_in = in_buf + this_off;
      strm->next_out = out_buf + off;
      strm->avail_in = this_len;
      strm->avail_out = len;
      res = deflateParams(strm, level, strategy);

      if(!in_directly_addressable)
	VARARRAY_DESTROY( inbuffer );
      if(!out_directly_addressable) {
	local_to_global_copy( outbuffer, out_temp_buf, off+len );
	VARARRAY_DESTROY( outbuffer );
      }
      switch (res) {
      case Z_OK:
	ASSIGN_GLOBAL_jboolean(setParams_ptr, 0);
	this_off += this_len - strm->avail_in;
	ASSIGN_GLOBAL_jint(off_ptr, this_off);
	ASSIGN_GLOBAL_jint(len_ptr, strm->avail_in);
	return len - strm->avail_out;
      case Z_BUF_ERROR:
	ASSIGN_GLOBAL_jboolean(setParams_ptr, 0);	
	return 0;
      default:
	JNU_ThrowInternalError(env, strm->msg);
	return 0;
      }
    } else {
      Pjboolean finish_ptr, finished_ptr;
      jboolean finish;

      FIELD_ADDR_GLOBAL(finish_ptr, me, f6finishT8Deflater3zip4util4java);
      DEREF_GLOBAL_jboolean(finish, finish_ptr);            

      strm->next_in = in_buf + this_off;
      strm->next_out = out_buf + off;
      strm->avail_in = this_len;
      strm->avail_out = len;
      res = deflate(strm, finish ? Z_FINISH : Z_NO_FLUSH);

      if(!in_directly_addressable)
	VARARRAY_DESTROY( inbuffer );
      if(!out_directly_addressable) {
	local_to_global_copy( outbuffer, out_temp_buf, off+len );
	VARARRAY_DESTROY( outbuffer );
      }
      switch (res) {
      case Z_STREAM_END:
	FIELD_ADDR_GLOBAL(finished_ptr, me, f8finishedT8Deflater3zip4util4java);
	ASSIGN_GLOBAL_jboolean(finished_ptr, 1);
      case Z_OK:
	this_off += this_len - strm->avail_in;
	ASSIGN_GLOBAL_jint(off_ptr, this_off);
	ASSIGN_GLOBAL_jint(len_ptr, strm->avail_in);
	return len - strm->avail_out;
      case Z_BUF_ERROR:
	return 0;
      default:
	JNU_ThrowInternalError(env, strm->msg);
	return 0;
      }
    }
  }

}

jint m8getAdlerJmT8Deflater3zip4util4java(jlong strm)
{
  return ((z_stream *)jlong_to_ptr(strm))->adler;
}

jint m10getTotalInJmT8Deflater3zip4util4java(jlong strm)
{
  return ((z_stream *)jlong_to_ptr(strm))->total_in;
}

jint m11getTotalOutJmT8Deflater3zip4util4java(jlong strm)
{
  return ((z_stream *)jlong_to_ptr(strm))->total_out;
}

void m5resetJmT8Deflater3zip4util4java(jlong strm)
{
  if (deflateReset((z_stream *)jlong_to_ptr(strm)) != Z_OK) {
    JNU_ThrowInternalError(env, 0);
  }
}

void m3endJmT8Deflater3zip4util4java(jlong strm)
{
  if (deflateEnd((z_stream *)jlong_to_ptr(strm)) == Z_STREAM_ERROR) {
    JNU_ThrowInternalError(env, 0);
  } else {
    free((z_stream *)jlong_to_ptr(strm));
  } 
}

void m7initIDsmT8Deflater3zip4util4java() {
}

