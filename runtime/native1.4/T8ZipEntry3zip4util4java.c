#include <stdio.h>
#include <stdlib.h>
#include "native-utils.h"
#include "zip_util.h"
#include <sys/time.h>


void m10initFieldsJmT8ZipEntry3zip4util4java(PT8ZipEntry3zip4util4java me, jlong zentry)
{
  PPT6String4lang4java entry_str_ptr;
  PT6String4lang4java entry_str, gstr;
  LT6String4lang4java lstr = NULL;
  Pjlong temp_long;
  Pjint temp_int;
  LTAjbyte lba;
  PTAjbyte entry_extra, gba;
  PPTAjbyte entry_extra_ptr;
  char *ename;

  CHECK_NULL_GLOBAL(me);

  jzentry *ze = jlong_to_ptr_e(zentry);
  FIELD_ADDR_GLOBAL(entry_str_ptr, me, f4nameT8ZipEntry3zip4util4java);
  lstr = java_string_build_8(ze->name);
  globalize(gstr, lstr);
  if (TO_BOX(entry_str_ptr) != MYBOX) {
    GC_PTR_ESC(&(gstr), 1);
  }
  PGASSIGN_GLOBAL_gp(entry_str_ptr,gstr);

  FIELD_ADDR_GLOBAL(temp_long, me, f4timeT8ZipEntry3zip4util4java);
  ASSIGN_GLOBAL_jlong(temp_long,(jlong) ze->time & 0xffffffffUL);

  FIELD_ADDR_GLOBAL(temp_long, me, f3crcT8ZipEntry3zip4util4java);
  ASSIGN_GLOBAL_jlong(temp_long,(jlong) ze->crc & 0xffffffffUL);

  FIELD_ADDR_GLOBAL(temp_long, me, f4sizeT8ZipEntry3zip4util4java);
  ASSIGN_GLOBAL_jlong(temp_long,(jlong) ze->size);

  if (ze->csize == 0) {
    FIELD_ADDR_GLOBAL(temp_long, me, f5csizeT8ZipEntry3zip4util4java);
    ASSIGN_GLOBAL_jlong(temp_long,(jlong) ze->size);
    FIELD_ADDR_GLOBAL(temp_int, me, f6methodT8ZipEntry3zip4util4java);
    ASSIGN_GLOBAL_jint(temp_int,STORED);
  } else {
    FIELD_ADDR_GLOBAL(temp_long, me, f5csizeT8ZipEntry3zip4util4java);
    ASSIGN_GLOBAL_jlong(temp_long,(jlong) ze->csize);
    FIELD_ADDR_GLOBAL(temp_int, me, f6methodT8ZipEntry3zip4util4java);
    ASSIGN_GLOBAL_jint(temp_int,DEFLATED);
  }
  if (ze->extra != 0) {
    unsigned char *bp = (unsigned char *)&ze->extra[0];
    int len = (bp[0] | (bp[1] << 8));
    JAVA_ARRAY_BUILD(lba, len, &ze->extra[2], 1, 1);
    FIELD_ADDR_GLOBAL(entry_extra_ptr, me, f5extraT8ZipEntry3zip4util4java);
    /* Cast LTAjbyte to PTAjbyte */
    TO_GLOBALB(gba, MYBOX, lba);
    if (TO_BOX(entry_extra_ptr) != MYBOX)
      {
        GC_PTR_ESC(&(gba), 1);
      }
    PGASSIGN_GLOBAL_gp(entry_extra_ptr,gba);
  }
  if (ze->comment != 0) {
    FIELD_ADDR_GLOBAL(entry_str_ptr, me, f7commentT8ZipEntry3zip4util4java);
    lstr = java_string_build_8(ze->comment);
    globalize(gstr, lstr);
    if (TO_BOX(entry_str_ptr) != MYBOX) {
      GC_PTR_ESC(&(gstr), 1);
    }
    PGASSIGN_GLOBAL_gp(entry_str_ptr,gstr);
  }
  
}

jlong m19javaToDosTimeNativeJmT8ZipEntry3zip4util4java(jlong millitime) 
{
  time_t sectime = (jlong) millitime / 1000;
  struct tm *ltime;
  jlong localTime = 0;

  ltime = localtime(&sectime);
  localTime = ((ltime->tm_year - 80) << 25) | ((ltime->tm_mon + 1) << 21)
    | (ltime->tm_mday << 16) | (ltime->tm_hour << 11) | (ltime->tm_min << 5) 
    | (ltime->tm_sec >> 1);
  return localTime;
}

jlong m19dosToJavaTimeNativeJmT8ZipEntry3zip4util4java(jlong dostime) 
{
  time_t sectime;
  struct tm stime;
  
  stime.tm_sec  = (dostime << 1) & 0x3E;
  stime.tm_min  = (dostime >> 5) & 0x3F;
  stime.tm_hour = (dostime >> 11) & 0x1F;
  stime.tm_mday = (dostime >> 16) & 0x1F;
  stime.tm_mon  = ((dostime >> 21) & 0x0F) - 1;
  stime.tm_year = (dostime >> 25) + 80;
  stime.tm_isdst = -1;
  sectime = mktime(&stime);
  /* will only be precise to 2 seconds since Dos time is not not very precise */
  return sectime * (jlong) 1000;
}

void m7initIDsmT8ZipEntry3zip4util4java() {
}
