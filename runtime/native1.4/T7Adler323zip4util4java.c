#include <zlib/zlib.h>
#include "zip_util.h"
#include "native-file-utils.h"

#include "layout!PTAjbyte.h"
#include "layout!Pjbyte.h"
#include "layout!Pjint.h"
#include "layout!TAjbyte.h"

jint m6updateIImT7Adler323zip4util4java(jint adler, jint b)
{
  Bytef buf[1];
  
  buf[0] = (Bytef)b;
  return adler32(adler, buf, 1);
}

jint m11updateBytesIPTAjbyteIImT7Adler323zip4util4java(jint adler, PTAjbyte b, jint off, jint len)
{
  jbyte *buf;

  JAVA_ARRAY_CHECK_GLOBAL( b, off, len, "in m11updateBytesIPTAjbyteIImT7Adler323zip4util4java()" );

  if(isDirectlyAddressable(b)) {
    FIELD_ADDR_LOCAL( buf, (TAjbyte *)TO_LOCAL(b), data[0] );
    INDEX_LOCAL( buf, buf, off );
    if(buf)
      adler = adler32(adler, (Bytef *) buf, len);
  } else {
    Pjbyte temp_buf;
    VARARRAY_DECLARE( jbyte, buffer, len );
    VARARRAY_CREATE( jbyte, buffer, len );
    buf = buffer;
    /* we could use JAVA_ARRAY_ADDR_GLOBAL here, but that includes a redundant check */
    FIELD_ADDR_GLOBAL( temp_buf, b, data[0] );
    INDEX_GLOBAL( temp_buf, temp_buf, off);
    global_to_local_copy( temp_buf, buffer, len );
    if(buf) {
      adler = adler32(adler, (Bytef *) buf, len);  
    }
    VARARRAY_DESTROY(buffer);
  }
  return adler;
}
