#include "native-stubs.h"
#include "zip_util.h"
#include "native-file-utils.h"
#include "native-utils.h"

#include "layout!PTAjbyte.h"
#include "layout!Pjbyte.h"
#include "layout!Pjint.h"
#include "layout!TAjbyte.h"

#define OPEN_READ 0x1
#define OPEN_DELETE 0x4
#define JVM_O_DELETE 0x10000

#include "zip_util.c"

jlong m4openPT6String4lang4javaIJmT7ZipFile3zip4util4java(PT6String4lang4java name, jint mode, jlong lastModified)
{
  char *path;
  jlong result = 0;
  int flag = 0;

  CHECK_NULL_GLOBAL_IFBC( name, "in m4openPT6String4lang4javaIJmT7ZipFile3zip4util4java()" );

  path = globalJstringToCstring(name);
  
  if (mode & OPEN_READ) flag |= O_RDONLY;
  if (mode & OPEN_DELETE) flag |= JVM_O_DELETE;
  
  if (path != 0) {
    char *msg;
    jzfile *zip = ZIP_Open_Generic(path, &msg, flag, lastModified);
    if (zip != 0) {
      result = ptr_to_jlong(zip);
    } else if (msg != 0) {
      ThrowZipException(env, msg);
    } else if (errno == ENOMEM) {
      JNU_ThrowOutOfMemoryError(env, 0);
    } else {
      ThrowZipException(env, "error in opening zip file");
    }
  }
  return result;
}

jint m8getTotalJmT7ZipFile3zip4util4java(jlong zfile)
{
    jzfile *zip = jlong_to_ptr_f(zfile);

    return zip->total;
}

jlong m8getEntryJPT6String4lang4javamT7ZipFile3zip4util4java(jlong zfile, PT6String4lang4java name)
{
#define MAXNAME 1024
  jzfile *zip = jlong_to_ptr_f(zfile);
  int ulen;
  char buf[MAXNAME+1], *path;
  jzentry *ze;
  
  CHECK_NULL_GLOBAL_IFBC( name, "in m8getEntryJPT6String4lang4javamT7ZipFile3zip4util4java()" );

  path = globalJstringToCstring(name);
  ze = ZIP_GetEntry(zip, path);

  ti_free(path);

  return ptr_to_jlong(ze);
}

void m9freeEntryJJmT7ZipFile3zip4util4java(jlong zfile, jlong zentry)
{
  jzfile *zip = jlong_to_ptr_f(zfile);
  jzentry *ze = jlong_to_ptr_e(zentry);
  ZIP_FreeEntry(zip, ze);
}

jint m9getMethodJmT7ZipFile3zip4util4java(jlong zentry)
{
  jzentry *ze = jlong_to_ptr_e(zentry);
  
  return ze->csize != 0 ? DEFLATED : STORED;
}

jlong m12getNextEntryJImT7ZipFile3zip4util4java(jlong zfile, jint n)
{
  jzentry *ze = ZIP_GetNextEntry(jlong_to_ptr_f(zfile), n);
  
  return ptr_to_jlong(ze);
}

void m5closeJmT7ZipFile3zip4util4java(jlong zfile)
{
  ZIP_Close(jlong_to_ptr_f(zfile));
}

jint m4readJJIPTAjbyteIImT7ZipFile3zip4util4java(jlong zfile, jlong zentry, 
						 jint pos, PTAjbyte bytes, 
						 jint off, jint len)
{
  jzfile *zip = jlong_to_ptr_f(zfile);
  char *msg;
  char errmsg[128];

  JAVA_ARRAY_CHECK_GLOBAL( bytes, off, len, "in m4readJJIPTAjbyteIImT7ZipFile3zip4util4java()" );
  
  ZIP_Lock(zip);
  len = jzipReadBytes(zip, jlong_to_ptr_e(zentry), pos, bytes, off, len);
  msg = zip->msg;
  ZIP_Unlock(zip);

  if (len == -1) {
    if (msg != 0) {
      ThrowZipException(env, msg);
    } else {
      sprintf(errmsg, "errno: %d, error: %s\n", errno, "Error reading ZIP file");
      JNU_ThrowIOExceptionWithLastError(env, errmsg);
    }
  }
  return len;
}

jint m8getCSizeJmT7ZipFile3zip4util4java(jlong zentry)
{
  jzentry *ze = jlong_to_ptr_e(zentry);
  
  return ze->csize != 0 ? ze->csize : ze->size;
}

jint m7getSizeJmT7ZipFile3zip4util4java(jlong zentry)
{
  jzentry *ze = jlong_to_ptr_e(zentry);
  
  return ze->size;
}

PT6String4lang4java m13getZipMessageJmT7ZipFile3zip4util4java(jlong zfile)
{
  jzfile *zip = jlong_to_ptr_f(zfile);
  char *msg = zip->msg;
  LT6String4lang4java lmsg = NULL;
  PT6String4lang4java gmsg;

  if (msg == NULL) {
    globalize(gmsg, lmsg);
    return gmsg; 
  } 

  lmsg = java_string_build_8(msg);
  globalize(gmsg, lmsg);
  return gmsg;
}

void m7initIDsmT7ZipFile3zip4util4java() {
}

