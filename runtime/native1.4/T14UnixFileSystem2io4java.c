#include "file-64.h" /* must come first */

#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include "java_string.h"

/*#define DEBUG_FILE */

#include "native-stubs.h"

#include "canonicalize_md.c"

unimplemented(m21createFileExclusivelyPT6String4lang4javamT14UnixFileSystem2io4java,jboolean,(PT14UnixFileSystem2io4java var1, PT6String4lang4java var2))
unimplemented(m12deleteOnExitPT4File2io4javamT14UnixFileSystem2io4java,jboolean,(PT14UnixFileSystem2io4java var1, PT4File2io4java var2))
unimplemented(m19setLastModifiedTimePT4File2io4javaJmT14UnixFileSystem2io4java,jboolean,(PT14UnixFileSystem2io4java var1, PT4File2io4java var2, jlong var3))
unimplemented(m11setReadOnlyPT4File2io4javamT14UnixFileSystem2io4java,jboolean,(PT14UnixFileSystem2io4java var1, PT4File2io4java var2))


typedef GP_type(GP_JString) GP_GP_JString;

#define isFile isFile0
#define isDirectory isDirectory0

const char * getFilename(PT4File2io4java me) {
  GP_GP_JString field;
  GP_JString path;

  CHECK_NULL_GLOBAL( me );

  FIELD_ADDR_GLOBAL( field, me, f4pathT4File2io4java );
  FENCE_PRE_READ();
  DEREF_GLOBAL_gp( path, field );
  FENCE_POST_READ();

  return globalJstringToCstring( path );
  }

static jboolean isFile0(PT4File2io4java me) {
  struct stat buf;
  const char *path;

  path = getFilename(me);

  if (stat( path, &buf )) {
    #ifdef DEBUG_FILE
      fprintf(stderr, "Failed to stat '%s' in UnixFileSystem.isFile0()\n", path);
    #endif
    return 0;
    }
  else return S_ISREG(buf.st_mode);
  }

static jboolean isDirectory0(PT4File2io4java me) {
  struct stat buf;
  const char *path;

  path = getFilename(me);

  if (stat( path, &buf )) {
    #ifdef DEBUG_FILE
      fprintf(stderr, "Failed to stat '%s' in UnixFileSystem.isDirectory0()\n", path);
    #endif
    return 0;
    }
  else return S_ISDIR(buf.st_mode);
  }


static jboolean canRead(PT4File2io4java me) {
  const char *path;
  int fd;

  if (!isFile(me)) return 0; /* doesn't exist, or not a file */

  path = getFilename(me);
  fd = open64( path, O_RDONLY | O_LARGEFILE);
  if (fd >= 0) {
    if (close(fd)) 
      fprintf(stderr, "Error closing descriptor of '%s' in UnixFileSystem.canRead()\n", path);
    return 1;
    }
  else return 0;
  }

static jboolean canWrite(PT4File2io4java me) {
  const char *path;
  int fd;

  if (!isFile(me)) return 0; /* doesn't exist, or not a file */

  path = getFilename(me);
  fd = open64( path, O_WRONLY | O_LARGEFILE); 
  if (fd >= 0) {
    if (close(fd)) 
      fprintf(stderr, "Error closing descriptor of '%s' in UnixFileSystem.canWrite()\n", path);
    return 1;
    }
  else return 0;
  }

/*------------------------------------------------------------------------------------*/
jint m21getBooleanAttributes0PT4File2io4javamT14UnixFileSystem2io4java(PT14UnixFileSystem2io4java me, PT4File2io4java f) {
  struct stat buf;
  const char *path;

  path = getFilename(f);

  if (stat( path, &buf )) {
    return 0; //does not exist
    }
  else if(S_ISDIR(buf.st_mode)) {
    return 0x05;  //is directory
  }
  else if(S_ISREG(buf.st_mode)) {
    return 0x03; //is regular
  }
  else return 0x01;
}
/*------------------------------------------------------------------------------------*/
jlong m9getLengthPT4File2io4javamT14UnixFileSystem2io4java(PT14UnixFileSystem2io4java me, PT4File2io4java f) {
  struct stat64 buf;
  const char *path;
  int temp;

  path = getFilename(f);
  /* Get data associated with pathname */   
  temp = stat64( path, &buf );
  if (temp) {
    #ifdef DEBUG_FILE
      fprintf(stderr, "Failed to stat '%s' in UnixFileSystem.getLength()\n", path);
    #endif
    return 0; 
    }
  else return (jlong) CONVERT_OFF64_TO_LONG_LONG(buf.st_size);
  }
/*------------------------------------------------------------------------------------*/
jlong m19getLastModifiedTimePT4File2io4javamT14UnixFileSystem2io4java(PT14UnixFileSystem2io4java me, PT4File2io4java f) {
  struct stat buf;
  const char *path;

  path = getFilename(f);

  if (stat( path, &buf )) {
    #ifdef DEBUG_FILE
      fprintf(stderr, "Failed to stat '%s' in UnixFileSystem.getLastModifiedTime()\n", path);
    #endif
    return 0;
    }
  else return (jlong) buf.st_mtime;  
  }
/*------------------------------------------------------------------------------------*/
jboolean m11checkAccessPT4File2io4javaZmT14UnixFileSystem2io4java(PT14UnixFileSystem2io4java me, PT4File2io4java f, jboolean write) {
  if(write)
    return canWrite(f);
  else
    return canRead(f);
}
/*------------------------------------------------------------------------------------*/
jboolean m7rename0PT4File2io4javaPT4File2io4javamT14UnixFileSystem2io4java(PT14UnixFileSystem2io4java me, PT4File2io4java f1, PT4File2io4java f2) {
  const char *frompath;
  const char *topath;

  frompath = getFilename(f1);
  topath = getFilename(f2);
  if (rename(frompath, topath)) {
    #ifdef DEBUG_FILE
      fprintf(stderr, "Failed while renaming '%s' -> '%s' in UnixFileSystem.rename0()\n", frompath, topath);
    #endif
    return 0;
    }
  else return 1;
  }
/*------------------------------------------------------------------------------------*/
jboolean m15createDirectoryPT4File2io4javamT14UnixFileSystem2io4java(PT14UnixFileSystem2io4java me, PT4File2io4java f) {
  const char *path;

  path = getFilename(f);

  if (mkdir(path, S_IRWXU | S_IRWXG | S_IRWXO )) {
    #ifdef DEBUG_FILE
      fprintf(stderr, "Failed to create directory '%s' in UnixFileSystem.createDirectory()\n", path);
    #endif
    return 0;
    }
  else return 1;
  }
/*------------------------------------------------------------------------------------*/
jboolean m7delete0PT4File2io4javamT14UnixFileSystem2io4java(PT14UnixFileSystem2io4java me, PT4File2io4java f) {
  const char *path;

  path = getFilename(f);

  if (remove(path)) {
    #ifdef DEBUG_FILE
      fprintf(stderr, "Failed to remove '%s' in File.delete()\n", path);
    #endif
    return 0;
    }
  else return 1;
  }
/*------------------------------------------------------------------------------------*/
PTAPT6String4lang4java m4listPT4File2io4javamT14UnixFileSystem2io4java(PT14UnixFileSystem2io4java me, PT4File2io4java f) {
  const char *path;
  int approxfilecount;
  DIR* dir;
  struct dirent* entry;
  int filecount;
  char **files;
  PTAPT6String4lang4java returnlist;
  int i;

  if (!isDirectory(f)) {
    TAPT6String4lang4java *strings;
	  PTAPT6String4lang4java result;
    #ifdef DEBUG_FILE
      fprintf(stderr, "UnixFileSystem.list('%s') called on non-directory\n", path);
    #endif
    JAVA_ARRAY_ALLOC( strings, NULL, 0, GP_JString, 0, 1, NULL );
	  globalize( result, strings );
	  return result;
    }

  path = getFilename(f);

  dir = opendir(path);
  if (!dir) { /* failed to open dir */
    TAPT6String4lang4java *strings;
	  PTAPT6String4lang4java result;
    #ifdef DEBUG_FILE
      fprintf(stderr, "UnixFileSystem.list() failed to opendir('%s')\n", path);
    #endif
    JAVA_ARRAY_ALLOC( strings, NULL, 0, GP_JString, 0, 1, NULL );
	  globalize( result, strings );
	  return result;
    }

  approxfilecount = 0; /* approximate the number of files we need space for */
  while (readdir(dir)) { /* use readdir_r for thread-safety */
    approxfilecount++;
    }

  files = (char **)ti_malloc(sizeof(char*) * approxfilecount);

  /* do the following carefully to prevent corrupting memory */
  /* in the presence of asynchronous updates to the directory */
  rewinddir(dir);
  filecount = 0;
  while ((entry = readdir(dir)) && filecount < approxfilecount) { /* read the actual files */
    if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")) { /* remove . and .. */
      files[filecount] = ti_malloc(strlen(entry->d_name)+2);
      strcpy(files[filecount], entry->d_name);
      filecount++;
      }
    }

  returnlist = java_strings_build(filecount, files);

  for (i = 0; i < filecount; i++) ti_free(files[i]);
  ti_free(files);

  return returnlist;
  }
/*------------------------------------------------------------------------------------*/
PT6String4lang4java m13canonicalize0PT6String4lang4javamT14UnixFileSystem2io4java(PT14UnixFileSystem2io4java var1, PT6String4lang4java var2) {
  CHECK_NULL_GLOBAL(var1);
  CHECK_NULL_GLOBAL(var2);

  PT6String4lang4java result;
  LT6String4lang4java lresult = NULL;
  char *path = globalJstringToCstring(var2);
  char canonicalPath[PATH_MAX];
  /* AK -- JVM_NativePath does nothing on *nix systems */
  if (canonicalize(/*JVM_NativePath(*/(char *)path/*)*/,
		   canonicalPath, PATH_MAX) < 0) {
    tossIOException_str("Bad pathname");
  } else {
    lresult = java_string_build_8(canonicalPath);
  }
  globalize(result, lresult);
  return result;
}
/*------------------------------------------------------------------------------------*/
void m7initIDsmT14UnixFileSystem2io4java() {
}

