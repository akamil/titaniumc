#include "ti_config.h"
#include "../fp-utils.h"

#ifdef WORDS_BIGENDIAN
# define IEEE_MC68k
#else
# define IEEE_8087
#endif

#if SIZEOF_LONG != 4
# if SIZEOF_INT == 4
#  define Long int
# else
#  if SIZEOF_SHORT == 4
#   define Long short
#  else
#   error no 32-bit integer type
#  endif /* not 32-bit shorts */
# endif /* not 32-bit ints */
#endif /* not 32-bit longs */

#include "dtoa.c"
#include "g_fmt.c"
