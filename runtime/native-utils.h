#ifndef _INCLUDE_TLIB_NATIVE_UTILS_H_
#define _INCLUDE_TLIB_NATIVE_UTILS_H_

#include "ti-memory.h"
#include "primitives.h"
#include "gasnet_tools.h"

#ifndef _CONCAT
  #define _CONCAT_HELPER(a,b) a ## b
  #define _CONCAT(a,b) _CONCAT_HELPER(a,b)
#endif
#ifndef _IDENTITY
  #define _IDENTITY(x) (x)
#endif
#ifndef _STRINGIFY
  #define _STRINGIFY_HELPER(x) #x
  #define _STRINGIFY(x) _STRINGIFY_HELPER(x)
#endif

/* __TI_CURRENT_FUNCTION__ is re-defined for code-genned methods to a demangled
 name, but try to get one from the C compiler for native code */
#if defined(__GNUC__) || defined(__FUNCTION__)
  #define __TI_CURRENT_FUNCTION__ __FUNCTION__
#elif defined(HAVE_FUNC)
  /* __func__ should also work for ISO C99 compilers */
  #define __TI_CURRENT_FUNCTION__ __func__
#else
  #define __TI_CURRENT_FUNCTION__ ""
#endif
extern char *build_loc_str(const char *funcname, const char *filename, int linenum, const char *extrainfo);
#define __current_loc  build_loc_str(__TI_CURRENT_FUNCTION__,__FILE__,__LINE__,NULL)
#define __current_loc_ext(where)  build_loc_str(__TI_CURRENT_FUNCTION__,__FILE__,__LINE__,where)

void ti_main(int, char **);
extern char ti_exepath[1024];

/* TI_IDENT() takes a unique identifier and a textual string and embeds the textual
   string in the executable file
 */
#define TI_IDENT(identName, identText) GASNETT_IDENT(identName, identText)

#define CHECK_NULL_GLOBAL( object ) \
    if_pf (isNull(object)) tossNullPointerException_str(__current_loc)

#define CHECK_NULL_LOCAL( object ) \
    if_pf (object == NULL) tossNullPointerException_str(__current_loc)

#if BOUNDS_CHECKING
  #define CHECK_NULL_GLOBAL_IFBC( object, where ) \
    if_pf (isNull(object)) tossNullPointerException_str(__current_loc_ext(where))
  #define CHECK_NULL_LOCAL_IFBC( object, where ) \
    if_pf (object == NULL) tossNullPointerException_str(__current_loc_ext(where))
#else
  #define CHECK_NULL_GLOBAL_IFBC( object, where ) ((void)0)
  #define CHECK_NULL_LOCAL_IFBC( object, where ) ((void)0)
#endif

#if BOUNDS_CHECKING
  #define CHECK_PTR_TO_SHARED_IFBC(ptr,context,where) do { \
    if (!SHARED_REGIONID(PG2RegionId(ptr))) { \
      fprintf(stderr,  "Error at %s: Proc %i tried to %s a pointer to a private region object\n", where, MYPROC, context); \
      abort(); \
    } \
  } while (0)
#else
  #define CHECK_PTR_TO_SHARED_IFBC(ptr,context,where) ((void)0)
#endif

#define TI_BEGIN_FUNCTION TIC_BEGIN_FUNCTION

#ifdef COMM_GASNET
  #define TI_PUT_NB_BULK(phandle, destbox, destaddr, srcaddr, nbytes) \
    *(gasnet_handle_t *)phandle = gasnet_put_nb_bulk(destbox, destaddr, srcaddr, nbytes)
  #define TI_PUT_NBI_BULK(destbox, destaddr, srcaddr, nbytes) \
    gasnet_put_nbi_bulk(destbox, destaddr, srcaddr, nbytes)
  #define TI_GET_NB_BULK_NOPTRS(phandle, destaddr, srcbox, srcaddr, nbytes) \
    *(gasnet_handle_t *)phandle = gasnet_get_nb_bulk(destaddr, srcbox, srcaddr, nbytes)
  #define TI_GET_NBI_BULK_NOPTRS(destaddr, srcbox, srcaddr, nbytes) \
    gasnet_get_nbi_bulk(destaddr, srcbox, srcaddr, nbytes)
  #define TI_HANDLE_SETDONE(phandle) \
    *(gasnet_handle_t *)phandle = GASNET_INVALID_HANDLE
  #define TI_WAIT_NB(phandle) gasnet_wait_syncnb(*(gasnet_handle_t *)phandle)
  #define TI_WAIT_NBI() gasnet_wait_syncnbi_all()                          
#else
  /* Non-GASNet backends - currently use blocking ops for NB and ignore handles */
  #define TI_PUT_NB_BULK(phandle, destbox, destaddr, srcaddr, nbytes) \
     ti_bulk_write(destbox, destaddr, srcaddr, nbytes) 
  #define TI_PUT_NBI_BULK(destbox, destaddr, srcaddr, nbytes) \
     ti_bulk_write_weak(destbox, destaddr, srcaddr, nbytes)
  #define TI_GET_NB_BULK_NOPTRS(phandle, destaddr, srcbox, srcaddr, nbytes) \
     ti_bulk_read(destaddr, srcbox, srcaddr, nbytes, tic_no_ptrs)   
  #define TI_GET_NBI_BULK_NOPTRS(destaddr, srcbox, srcaddr, nbytes) \
     ti_bulk_read_weak(destaddr, srcbox, srcaddr, nbytes, tic_no_ptrs)
  #define TI_HANDLE_SETDONE(phandle) 
  #define TI_WAIT_NB(phandle) 
  #define TI_WAIT_NBI()  ti_sync()
#endif

#define HANDLE_NBI ((T6Handle4lang2ti *)(uintptr_t)-1) 

void tossNullPointerException_str(char * const string) __attribute__((noreturn)
);
 
void tossMonitorStateException_str(char * const string) __attribute__((noreturn));

void tossArrayStoreException_str(char * const string) __attribute__((noreturn));

void tossIllegalArgumentException_str(char * const string) __attribute__((noreturn));

void tossClassNotFoundException_str(char * const string) __attribute__((noreturn));

void m9dumpStackmT6Thread4lang4java();

#ifdef HAVE_DREM
#define IEEEremainder drem
#else
#ifdef HAVE_REMAINDER
#define IEEEremainder remainder
#else
#error Cannot locate IEEE remainder function.
#endif
#endif

#endif
