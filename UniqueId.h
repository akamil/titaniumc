#ifndef _INCLUDE_UNIQUE_ID_H_
#define _INCLUDE_UNIQUE_ID_H_

#include <string>
#include "using-std.h"


class UniqueId
{
public:
    UniqueId( const char [] );

    const string next();    
    void reset();

private:
    UniqueId( const UniqueId & ); // forbidden

    const char * const prefix;
    unsigned sequence;
};


inline UniqueId::UniqueId( const char prefix[] )
    : prefix( prefix ),
      sequence( 0 )
{
}


inline void UniqueId::reset()
{
    sequence = 0;
}


#endif
