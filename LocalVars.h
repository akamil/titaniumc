#ifndef _INCLUDE_LocalVars_h_
#define _INCLUDE_LocalVars_h_

#define __SINST__		// g++ header bug

#include <iosfwd>
#include <map>
#include <new>
#include <string>


class CtRegistry;
class CtType;
class File;


class LocalVars : private multimap< string, string, less< string > > {
public:
  void depend( const CtType & );
  void declare( const string &, const CtType & );

protected:
  explicit LocalVars( const LocalVars & );
  explicit LocalVars( CtRegistry & );
  void codeGen( ostream &, int ) const;

private:
  CtRegistry &dependencies;

  // forbidden
  bool operator ==( const LocalVars & ) const;
  static void *operator new( size_t );
};


#endif
