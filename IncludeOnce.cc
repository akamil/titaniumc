#include "IncludeOnce.h"


void IncludeOnce::emitHeading( const string &root ) const
{
  string macro = "_include_";
  
  for (unsigned scan = 0; scan < root.length(); ++scan)
    if (root[ scan ] == '_' || isalnum( root[ scan ]))
      macro += root[ scan ];
    else
      macro += '_';

  macro += "_h_";
    
  out <<
    "#ifndef " << macro << "\n"
    "#define " << macro << "\n\n";
}
