#ifndef _INCLUDE_FlattenContext_H_
#define _INCLUDE_FlattenContext_H_


class TreeNode;


class TreeNode::FlattenContext // The static analysis context for flattenClasses
{
public:
  FlattenContext(llist<TreeNode*> *&);

  CompileUnitNode *toplevel;
  TypeDeclNode *outer;
  bool inStatic;
  bool inTemplate;
  llist<TreeNode*> *finals;
  BlockNode *crntBlock;
};

inline TreeNode::FlattenContext::FlattenContext(llist<TreeNode*> *&finals_)
  : toplevel(NULL), outer(NULL), inStatic(false), inTemplate(false), 
     finals(finals_), crntBlock(NULL) {}

#endif
