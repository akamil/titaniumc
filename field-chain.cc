#include "AST.h"


bool MethodCallAssignNode::fieldChainParent() const
{
  return false;
}


bool MethodCallNode::fieldChainParent() const
{
  return false;
}


bool ObjectFieldAccessNode::fieldChainParent() const
{
  return decl()->modifiers() & Decl::Method;
}


bool TreeNode::fieldChainParent() const
{
  return true;
}


////////////////////////////////////////////////////////////////////////


bool FieldAccessNode::fieldChainEnd() const
{
  return parent()->fieldChainParent();
}
