#ifndef _ArrayAccessSet_H_
#define _ArrayAccessSet_H_

#include "AST.h"
#include "MIVEset.h"
#include "optimize.h"

typedef map< TreeNode *, MIVEset *, less< TreeNode * > > map_tree_to_MIVEset;

class ArrayAccessSet {
 public:
  mutable map_tree_to_MIVEset m;  // SR
  mutable map_tree_to_cMIVElist q; // OSR
  mutable map_tree_to_treeSet c; // Loop invariant pointers
  mutable map_tree_to_tree u;
  mutable map_tree_tree_to_tree uu;

  class osr_iterator {
  public:
    osr_iterator(map_tree_to_cMIVElist &q_) :
      q(q_), i (q.begin()), l ((i == q.end()) ? NULL : (*i).second) {}
    TreeNode *array() { return (*i).first; }
    const MIVE *base() { return l->front(); }
    const MIVE *dest() { return l->tail()->front(); }
    bool isDone() { return i == q.end(); }
    void next() {
      assert(l != NULL && l->tail() != NULL);
      l = l->tail()->tail();
      while (l == NULL) {
	i++;
	if (isDone()) return;
	l = (*i).second;
      }
    }      
  private:
    map_tree_to_cMIVElist &q;
    map_tree_to_cMIVElist::const_iterator i;
    /* l is a list of base, dest, base, dest, base, dest, ... */
    const llist<const MIVE *> *l;
  };

  osr_iterator begin() const { return osr_iterator(q); }

  class lip_iterator {
  public:
    lip_iterator(map_tree_to_treeSet &c_) :
      c(c_), i(c.begin()), l ((i == c.end()) ? NULL : set_to_list((*i).second))
      {}
    TreeNode *array() { return (*i).first; }
    TreeNode *index() { return l->front(); }
    bool isDone() { return i == c.end(); }
    void next() {
      l = l->tail();
      while (l == NULL) {
	i++;
	if (isDone()) return;
	l = set_to_list((*i).second);
      }
    }
  private:
    map_tree_to_treeSet &c;
    map_tree_to_treeSet::const_iterator i;
    llist<TreeNode *> *l;
  };
  
  lip_iterator lip_begin() const { return lip_iterator(c); }

  const MIVEset * MIVEs(const TreeNode *arr) const;
  MIVEset * & MIVEs(const TreeNode *arr);
  MIVEset * allMIVEs(const TreeNode *arr) const;
  void adjoin(TreeNode *t, TreeNode *WRTloop);
  void adjoinConstant(TreeNode *t, TreeNode *WRTloop);
  void adjoinOSR(TreeNode *t, TreeNode *WRTloop,
		 const MIVE *x, const MIVE *y);
  const MIVE *baseMIVEForOSR(TreeNode *arr, const MIVE *m) const;
  bool containsOSR(TreeNode *t, TreeNode *WRTloop) const;
  bool containsConstant(TreeNode *t, TreeNode *WRTloop) const;
  bool contains(TreeNode *t, TreeNode *WRTloop) const;
  TreeNode * & use(const TreeNode *a) const { return u[(TreeNode *) a]; }
  TreeNode * & use(const TreeNode *a, const TreeNode *i) const {
    return uu[(TreeNode *) a][(TreeNode *) i];
  }
  string OSRTempname(TreeNode *WRTloop, TreeNode *arr, const MIVE *m);
  void seekUnitStride(map_int_to_treeSet &provablyUnitStride,
		      TreeNode *l);
  treeSet *SRarrays() const;
};

TreeNode *arrayVarDecl(const TreeNode *arr);

#endif /* _ArrayAccessSet_H_ */
