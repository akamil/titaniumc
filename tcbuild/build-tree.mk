backend_src_dir := $(top_srcdir)/runtime/backend/$(backend)
backend_build_dir := $(top_builddir)/runtime/backend/$(backend)
sequential_build_dir := $(top_builddir)/runtime/backend/sequential

backend_mk_dir := $(backend_build_dir)
backend_include_dir := $(backend_src_dir)
tlib_include_dir := $(sequential_build_dir)/tc-gen-$(prebuilt_tlib)

ifeq "$(TLIB_VERSION)" "1.0"
  tlib_ld_extraflags :=
else
  tlib_ld_extraflags := -L$(top_builddir)/runtime/zlib
endif

# these three are lazily expanded to allow the backend-provided variables, which are defined later
context_includes = -I$(backend_include_dir) -I$(top_builddir) -I$(top_builddir)/runtime -I$(top_srcdir)/runtime $(backend_includes_buildtree)
context_ld_flags = -L$(backend_build_dir) -L$(top_builddir)/runtime/gc-build $(tlib_ld_extraflags) $(backend_ld_flags_buildtree)
tlib_libdir = $(backend_build_dir)

makedepend := $(config_mk_dir)/makedepend
demangler := $(config_mk_dir)/tcbuild-demangler
refresh_cache := $(config_mk_dir)/refresh-cache
cleanup := $(config_mk_dir)/cleanup
