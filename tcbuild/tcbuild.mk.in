default : all

########################################################################
#
#  What's what, and what depends upon what
#
#  The order of the following lines is quite particular.
#

depends_mk := $(object_dir)/depends.mk

include $(config_mk_dir)/config.mk
include $(mk_dir)/$(context).mk
include $(mk_dir)/$(strategy).mk

objects := $(sources:$(generate_dir)/%.c=$(object_dir)/%.o)
assemblys := $(sources:$(generate_dir)/%.c=$(object_dir)/%.s)

ifeq "$(verbose)" "0"
  xargs := xargs
else
  xargs := xargs -t
endif

objlist = $(object_dir)/objlist
stamp_objlist = $(object_dir)/stamp-objlist

# never allow make to use it's implicit rule to run the assembler
%.o : %.s

########################################################################
#
#  Various and sundry supporting makefile fragments
#

makefile := $(object_dir)/GNUmakefile
ifeq "$(skip_tc)" "1"
makefile_dep :=
else
makefile_dep := $(makefile)
endif
include $(mk_dir)/$(goal).mk
include $(backend_mk_dir)/$(backend).mk


########################################################################
#
#  Prebuilt Titanium class library
#

ifneq "$(prebuilt_tlib)" ""
tlib_includes := -I$(tlib_include_dir)
tc_tlib_includes := -tlib-include-dir $(tlib_include_dir)

tlib_ld_libs  :=  -ltlib-$(backend)-$(prebuilt_tlib)
ifeq "@TLIB_SHARED@" "yes"
  tlib_ld_libs  += -ltlib-$(backend)-$(prebuilt_tlib)-a
endif

check_tlib_cmd := if test ! -f "$(tlib_include_dir)/T6Object4lang4java.h" ; then \
   echo "*** ERROR: Standard library headers are missing from: " ; \
   echo "***  $(tlib_include_dir)" ; \
   echo "*** This usually indicates a Titanium installation error." ; \
   echo "*** Most likely your admin needs to execute the following command:" ; \
   echo "***  cd $(top_builddir)/runtime/backend/sequential ; make tlib ; make install" ; \
   echo "*** As a temporary workaround, you can use \"tcbuild --rebuild-tlib\"" ; \
   echo "*** but note this workaround significantly increases compilation time." ; \
   exit 1 ; \
   fi
else
check_tlib_cmd :=
tc_tlib_includes :=
endif


########################################################################
#
#  Command options for Titanium compiler, C compiler, and linker
#
#  The "clean" versions avoid any mention of $(generate_dir), and
#  therefore are suitable for validating cache entries.
#

ifeq "$(verbose)" "0"
  time_cmd := 
else
  time_cmd := @TIMECMD@
endif

clean_tc_command := $(tc) $(tc_flags)
tc_args := $(tc_flags) -outdir $(generate_dir) $(tc_tlib_includes)
ifeq "$(gdb)" "1"
gdb_commandfile := $(object_dir)/gdb-commandfile
tc_command := gdb --command=$(gdb_commandfile) $(tc)
else
tc_command := $(time_cmd) $(tc) $(tc_args) $(ti_sources)
endif

clean_includes := $(tlib_includes) $(context_includes) $(backend_includes)
clean_cc_flags := $(cc_flags) $(backend_defines) $(clean_includes)
clean_cc_command := $(cc) $(clean_cc_flags)

unclean_includes := -I$(generate_dir)

all_includes := $(clean_includes) $(unclean_includes)
all_cc_flags := $(clean_cc_flags) $(unclean_includes)
cc_command   := $(time_cmd) $(clean_cc_command) $(unclean_includes)

ifeq "$(TLIB_VERSION)" "1.0"
  tlib_ld_extralibs := 
else
  tlib_ld_extralibs := -lz
endif

all_ld_flags := $(ld_flags) $(backend_ld_flags) $(context_ld_flags)
all_ld_libs  := $(ld_libs) $(tlib_ld_libs) $(backend_ld_libs) $(tlib_ld_extralibs) @LIBM@


########################################################################
#
#  Provide the requested level of feedback
#

export silent

ifeq "$(silent)" "0"
message := echo
pipe_demangler := | $(demangler)
else
message := :
progress :=
endif

ifeq "$(rawmsg)" "1"
pipe_demangler := 
endif

message_link = $(message) Linking $(outfile) ...


########################################################################
#
#  Targets
#

# AK: hack to get make to exit with an error if tc fails
set_tc_error := touch $(generate_dir)/tc_error
tc_error := [ -f $(generate_dir)/tc_error ]
error_exit := exit 1

$(object_dir)/gdb-commandfile: 
	[ -d $(object_dir) ] || mkdir -p $(object_dir)
	echo 'run $(tc_args) $(ti_sources)' > $@

$(stamp_tc) : $(makefile_dep) $(gdb_commandfile) $(ti_sources)
	[ -d $(generate_dir) -o -d $(object_dir) ] && $(cleanup) $(generate_dir) $(object_dir) 
	[ -d $(generate_dir) ] || mkdir -p $(generate_dir)
	[ -d $(object_dir) ] || mkdir -p $(object_dir)
	rm -f $@
	@$(message) Running Titanium compiler ...
	if !(TCBUILD_FLAGS='$(tcbuild_flags)' $(tc_command)); then $(set_tc_error); fi
	if [ -f $(generate_dir)/ti-tlib.c ]; then \
	    $(message) Adding library headers to initialization file ...;\
	    $(tc_collect) $(generate_dir)/ti-tlib.c $(generate_dir); \
	fi
	touch $@

# DOB: do not use $(generate_dir)/*.c below, because it could exceed arg limits on some OS's
$(depends_mk) : $(makedepend) $(stamp_cache)
	@$(message) Checking dependencies ...
	@$(check_tlib_cmd)
	$(time_cmd) $(makedepend) $(all_cc_flags) -- $(generate_dir) > $@

$(stamp_objlist):
	@> $(objlist)
	@touch $@

$(objlist) : $(stamp_objlist)
	@$(message) Building objlist ... 
	@echo $(foreach obj,$(filter-out %/ti-main.o $(nonshared_objects),$(objects)),$(shell echo $(obj) >> $(objlist))) > /dev/null

.PHONY: assembly force

force:

assembly : $(assemblys)

ifeq "$(preprocess_only)" "0"
all : $(outfile)
	if $(tc_error); then $(error_exit); else touch '$<'; fi
else
all : $(stamp_tc) 

endif

