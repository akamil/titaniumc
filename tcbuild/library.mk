$(outfile) $(nonshared_archive): $(objects) $(objlist)
	rm -f $(outfile) $(nonshared_archive)
	$(message_link)
	for file in XXX $(nonshared_objects); do \
	    if [ -r $$file ]; then \
		$(AR) cru $(nonshared_archive) $$file; \
		$(RANLIB) $(nonshared_archive); \
	    fi;\
	done;
	$(tc_libtool) --mode=link $(if $(verbose),--verbose) $(lib_type) $(objlist) $(outfile)
