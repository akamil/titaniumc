
-include $(stamp_tc)

sources := $(wildcard $(generate_dir)/*.c)

$(object_dir)/%.o : $(generate_dir)/%.c
	@$(message) Compiling $* ... $(pipe_demangler)
	$(cc_command) -o $@ -c $<

$(object_dir)/%.s : $(generate_dir)/%.c
	@$(message) Compiling $* to assembly... $(pipe_demangler)
	$(cc_command) -o $@ -S $<
