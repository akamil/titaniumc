
ifeq "$(preprocess_only)" "0"
-include $(depends_mk)
endif

$(cache_dir)/%.o : $(generate_dir)/%.c $(cache_dir)/%.cmd
	@$(message) Compiling $* ... $(pipe_demangler)
	$(cc_command) -o $@ -c $<

$(object_dir)/%.s : $(generate_dir)/%.c 
	@$(message) Compiling $* to assembly... $(pipe_demangler)
	$(cc_command) -o $@ -S $<

$(object_dir)/%.o : $(cache_dir)/%.o
	cp $< $@

$(stamp_cache) : $(refresh_cache) $(stamp_tc)
	@$(message) Checking and updating cache ...
	[ -d $(cache_dir) ] || mkdir -p $(cache_dir)
	tc_command=`echo $(clean_tc_command)` ; \
	cc_command=`echo $(clean_cc_command)` ; \
	$(time_cmd) $(refresh_cache) $(generate_dir) $(cache_dir) "$$tc_command" "$$cc_command"
	touch $@
