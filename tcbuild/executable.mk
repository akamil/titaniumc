ifneq "$(prebuilt_tlib)" ""
$(outfile) : $(objects)
	if [ -f $(generate_dir)/ti-main.o ]; then \
		$(message_link); \
		$(time_cmd) $(ld) $(all_ld_flags) $(tlib_rpath) -o '$@' $^ $(all_ld_libs); \
	fi
else
# too many objects may exceed command-line limits
tmp_tlib = $(object_dir)/libtlib-tmp.a
$(tmp_tlib) : $(objects) $(objlist)
	@$(message) Building tlib-tmp...
	rm -f $@
	cat $(objlist) | $(xargs) $(AR) cru $@
	$(RANLIB) $@

$(outfile) : $(tmp_tlib)
	if [ -f $(generate_dir)/ti-main.o ]; then \
		$(message_link); \
		$(time_cmd) $(ld) $(all_ld_flags) -o '$@' $(object_dir)/ti-main.o $(tmp_tlib) $(all_ld_libs); \
	fi
endif

.PHONY: link 
# a handy target used to manually force linking
link:
	rm -f $(outfile)
	$(MAKE) -f $(generate_dir)/GNUmakefile $(outfile)

