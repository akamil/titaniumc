config_include_dir := $(includedir)
shared_include_dir := $(datadir)/include

backend_mk_dir := $(sysconfdir)/tcbuild
backend_include_dir := $(shared_include_dir)/backend/$(backend)
tlib_include_dir := $(datadir)/tlib-include

# these three are lazily expanded to allow the backend-provided variables, which are defined later
context_includes = -I$(config_include_dir) -I$(shared_include_dir) -I$(backend_include_dir) $(backend_includes_installtree)
context_ld_flags = -L$(libdir) $(backend_ld_flags_installtree)
tlib_libdir = $(libdir)

makedepend := $(libexecdir)/makedepend
demangler := $(libexecdir)/tcbuild-demangler
refresh_cache := $(libexecdir)/refresh-cache
cleanup := $(libexecdir)/cleanup
