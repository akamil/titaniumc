#ifndef _TITANIUM_CODE_MIVE_H_
#define _TITANIUM_CODE_MIVE_H_

#include "AST.h"
#include "Poly.h"
#include "code.h"
#include "types.h"

class MIVE;
class MIVEcontext;

bool equalMIVE(const MIVE *x, const MIVE *y);
bool sameInfo(const MIVE *x, const MIVE *y);
MIVEcontext *& MIVEcontextOfLoop(TreeNode *l);

#define SCALAR -1

class MIVEcontext {
private:
  // These two together tell us how to compute a given PolyFactor at run-time
  // An index of -1 means the treenode is scalar valued; for point valued
  // treenodes we use 1-based indexing.
  map_polyfactor_to_treenode PolyFactorToVarDeclNode;
  map_polyfactor_to_int PolyFactorToIndexWithinVarDeclNode;

  // table of expr to polynomial mappings, for expressions with known poly.
  map_void_int_to_poly _exprToPoly;

  Poly * introduceVar(string name, TreeNode *t, int index);

public:
  MIVE * introduceInv(TreeNode *t, TreeNode *l);

  // d[a][b] is the derivative of a with respect to b
  derivtable d;

  TreeNode *& PolyFactorToTreeNode(PolyFactor f) {
    return PolyFactorToVarDeclNode[f.varNum];
  }
  int & PolyFactorToIndexWithinTreeNode(PolyFactor f) {
    return PolyFactorToIndexWithinVarDeclNode[f.varNum];
  }
  Poly *& exprToPoly(const TreeNode *t, int i) {
    return _exprToPoly[((TreeNode *)t)][i];
  }
  MIVE * adjoin(const string *var, int arity, TreeNode *t);

  void set0Derivs(PolyFactor f, TreeNode *l);
  Poly * deriv(const Poly *p, PolyFactor v);
  Poly * deriv(const PolyTerm *t, PolyFactor v);
  const Poly *& derivtab(PolyFactor f, PolyFactor g);
};

MIVE * getMIVE(TreeNode *t, TreeNode *l);
void setMIVE(TreeNode *t, TreeNode *l, MIVE *p);

class MIVE {
public:
  TypeNode *exprType;

  /* If exprType is Point<N> p is an array of N polynomials; if
     exprType is scalar then p is a single polynomial. */
  Poly **p;

  MIVEcontext *e;

  MIVE(TypeNode *t, MIVEcontext *context, Poly **poly) {
    exprType = t; e = context; p = poly;
  }

  MIVE(MIVEcontext *context, Poly *poly) {
    exprType = theIntType; e = context; p = new Poly * [1]; *p = poly;
  }

  int arity() const {
    return (exprType != NULL && exprType->isPointType()) ? exprType->tiArity()
      : SCALAR;
  }

  bool unknown() const { return p == NULL; }

  void print(ostream &) const;

  bool allPolysPresent() const;
  bool noPolysPresent() const;
  bool equals(const MIVE *o) const { return equalMIVE(this, o); }
  bool sameInfo(const MIVE *o) const { return ::sameInfo(this, o); }
  MIVE * find(llist<MIVE *> * & l) const;
  const MIVE * find(llist<const MIVE *> * & l) const;

  MIVE * best(MIVE *) const;

  MIVE * mult(const MIVE *) const;
  MIVE * add(const MIVE *) const;
  MIVE * mult(const Poly *) const;
  MIVE * add(const Poly *) const;
  MIVE * negate() const;
  MIVE * deriv(PolyFactor v);
};

string stringify(const MIVE *m);
void setDerivs(TreeNode *l);
int find_or_append_MIVE(const MIVE *m, llist<const MIVE *> * & l);

void reset_MIVE();

#include "MIVESort.h"

#endif /* _TITANIUM_CODE_MIVE_H_ */
