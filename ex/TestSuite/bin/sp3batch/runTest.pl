@programNames = (
		 "HelloWorld",
		 "PR187",
		 "arrayCopyTest",
		 "barriertest",
		 "broadcasttest",
		 "broadcasttest2",
		 "knapsack",
		 "staticsync",
		 "stressregions1",
		 "stressregions2",
		 "synctest",
		 "synctest2",
		 "test-scan-reduce",
		 "timedwait"
		 );

for ($i = 0; $i < $#programNames; $i++) {
    $prog = $programNames [$i];
    $srcFile = $prog.".ti";
    open (TCBUILDPIPE, "tcbuild --backend sp3 --verbose --cache-dir tc-cache-sp3 --ld-libs \"-L/usr/local/lib/gcc-lib/powerpc-ibm-aix4.3.1.0/2.95.2\" $srcFile |");
    while ($line = <TCBUILDPIPE>) { print "$line"; }
    close TCBUILDPIPE;

    open (LLSCRIPT, ">llscript.sh");
    print LLSCRIPT "\#!/bin/csh
\#\@environment=COPY_ALL;\"LL_JOB=TRUE\"; MP_EUILIB=us; \"TI_PFORP=2/2 2/2\"
\#\@node_usage=not_shared
\#\@network.LAPI=css0,not_shared,US
\#\@arguments=\"\"
\#\@input=/dev/null
\#\@notification=never
\#\@output=$prog.2.2.out
\#\@error=$prog.2.2.err
\#\@initialdir=/rmount/work/ux452200/tc/ex/TestSuite/sp3
\#\@job_type=parallel
\#\@node=2
\#\@tasks_per_node=1
\#\@wall_clock_limit=02:00:00
\#\@class=normal
\#\@queue
./$prog
";
    close LLSCRIPT;

    $result = `llsubmit llscript.sh`;
    print "$result";
    while (!(-e "$prog.2.2.out")) { sleep (20); }

}
