@programNames = (
		 "HelloWorld",
		 "PR187",
		 "arrayCopyTest",
		 "barriertest",
		 "broadcasttest",
		 "broadcasttest2",
		 "knapsack",
		 "staticsync",
		 "stressregions1",
		 "stressregions2",
		 "synctest",
		 "synctest2",
		 "test-scan-reduce",
		 "timedwait",
		 );

for ($i = 0; $i < $#programNames; $i++) {
    $prog = $programNames [$i];
    $outfile = $prog.".2.2.out";
    $backfile = $outfile.".bak";
    `mv $outfile $backfile`;
    open (INPUT, "$backfile");
    open (OUTPUT, ">$outfile");
    while ($line = <INPUT>) {
	if (!($line =~ /^Tic: /)) {
	    print OUTPUT $line;
	}
    }
    close INPUT;
    close OUTPUT;
}
