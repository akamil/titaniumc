@programNames = (
                 "HelloWorld",
                 "PR187",
                 "arrayCopyTest",
                 "barriertest",
                 "broadcasttest",
                 "broadcasttest2",
                 "knapsack",
                 "staticsync",
                 "stressregions1",
                 "stressregions2",
                 "synctest",
                 "synctest2",
                 "test-scan-reduce",
                 "timedwait",
                 );
for ($i = 0; $i < $#programNames; $i++) {
    $prog = $programNames [$i];
    $results = $prog.".2.2.out";
    $correct = $prog.".stdout";
    `diff $results $correct > $prog.diff`;
}
