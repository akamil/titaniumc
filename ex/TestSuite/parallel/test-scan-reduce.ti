// test scan/reduce
// Far from exhaustive---some methods are not tested at all.

class Hello implements ObjectOp, IntOp {
  static int single n = Ti.numProcs();
  static int me = Ti.thisProc();
  static long longme = me;
  static double doubleme = me;

  public Object eval(Object x, Object y) {
    return new Integer(((Integer) x).intValue() + ((Integer) y).intValue());
  }

  public int eval(int x, int y) {
    return (x + y) % 7;
  }

  public static sglobal void main(String[] args) {
    Hello local h = new Hello();
    String local log = "";

    log += "\n" + ("Generics\n--------\n");
    log += "\n" + ("Scan test:");
    log += "\n" + (me + ": generic int op (sum mod 7) " +
		   Scan.gen((IntOp) h, me));
    log += "\n" + (me + ": generic object op (sum) " +
		   Scan.gen((ObjectOp) h, (Object) new Integer(me)));
    log += "\n" + ("Reduce test:");
    log += "\n" + (me + ": generic int op (sum mod 7) " +
		   Reduce.gen((IntOp) h, me));
    log += "\n" + (me + ": generic object op (sum) " +
		   Reduce.gen((ObjectOp) h, new Integer(me)));

    log += "\n" + ("Ints\n----\n");
    log += "\n" + ("Scan test:");
    log += "\n" + (me + ": add " + Scan.add(me));
    log += "\n" + (me + ": mult " + Scan.mult(me));
    log += "\n" + (me + ": or " + Scan.or(me));
    log += "\n" + (me + ": and " + Scan.and(me));
    log += "\n" + (me + ": xor " + Scan.xor(me));
    log += "\n" + (me + ": min " + Scan.min(me));
    log += "\n" + (me + ": max " + Scan.max(me));
    log += "\n" + ("Reduce test:");
    log += "\n" + (me + ": add " + Reduce.add(me));
    log += "\n" + (me + ": mult " + Reduce.mult(me));
    log += "\n" + (me + ": or " + Reduce.or(me));
    log += "\n" + (me + ": and " + Reduce.and(me));
    log += "\n" + (me + ": xor " + Reduce.xor(me));
    log += "\n" + (me + ": min " + Reduce.min(me));
    log += "\n" + (me + ": max " + Reduce.max(me));
    log += "\n" + ("Reduce to 0 test:");
    log += "\n" + (me + ": add " + Reduce.add(me, 0));
    log += "\n" + (me + ": mult " + Reduce.mult(me, 0));
    log += "\n" + (me + ": or " + Reduce.or(me, 0));
    log += "\n" + (me + ": and " + Reduce.and(me, 0));
    log += "\n" + (me + ": xor " + Reduce.xor(me, 0));
    log += "\n" + (me + ": min " + Reduce.min(me, 0));
    log += "\n" + (me + ": max " + Reduce.max(me, 0));
    if (n > 1) {
      log += "\n" + ("Reduce to 1 test:");
      log += "\n" + (me + ": add " + Reduce.add(me,  1));
      log += "\n" + (me + ": mult " + Reduce.mult(me,  1));
      log += "\n" + (me + ": or " + Reduce.or(me,  1));
      log += "\n" + (me + ": and " + Reduce.and(me,  1));
      log += "\n" + (me + ": xor " + Reduce.xor(me,  1));
      log += "\n" + (me + ": min " + Reduce.min(me,  1));
      log += "\n" + (me + ": max " + Reduce.max(me,  1));
    }

    log += "\n" + ("Longs\n-----\n");
    log += "\n" + ("Scan test:");
    log += "\n" + (longme + ": add " + Scan.add(longme));
    log += "\n" + (longme + ": mult " + Scan.mult(longme));
    log += "\n" + (longme + ": or " + Scan.or(longme));
    log += "\n" + (longme + ": and " + Scan.and(longme));
    log += "\n" + (longme + ": xor " + Scan.xor(longme));
    log += "\n" + (longme + ": min " + Scan.min(longme));
    log += "\n" + (longme + ": max " + Scan.max(longme));
    log += "\n" + ("Reduce test:");
    log += "\n" + (longme + ": add " + Reduce.add(longme));
    log += "\n" + (longme + ": mult " + Reduce.mult(longme));
    log += "\n" + (longme + ": or " + Reduce.or(longme));
    log += "\n" + (longme + ": and " + Reduce.and(longme));
    log += "\n" + (longme + ": xor " + Reduce.xor(longme));
    log += "\n" + (longme + ": min " + Reduce.min(longme));
    log += "\n" + (longme + ": max " + Reduce.max(longme));
    log += "\n" + ("Reduce to 0 test:");
    log += "\n" + (longme + ": add " + Reduce.add(longme, 0));
    log += "\n" + (longme + ": mult " + Reduce.mult(longme, 0));
    log += "\n" + (longme + ": or " + Reduce.or(longme, 0));
    log += "\n" + (longme + ": and " + Reduce.and(longme, 0));
    log += "\n" + (longme + ": xor " + Reduce.xor(longme, 0));
    log += "\n" + (longme + ": min " + Reduce.min(longme, 0));
    log += "\n" + (longme + ": max " + Reduce.max(longme, 0));
    if (n > 1) {
      log += "\n" + ("Reduce to 1 test:");
      log += "\n" + (longme + ": add " + Reduce.add(longme,  1));
      log += "\n" + (longme + ": mult " + Reduce.mult(longme,  1));
      log += "\n" + (longme + ": or " + Reduce.or(longme,  1));
      log += "\n" + (longme + ": and " + Reduce.and(longme,  1));
      log += "\n" + (longme + ": xor " + Reduce.xor(longme,  1));
      log += "\n" + (longme + ": min " + Reduce.min(longme,  1));
      log += "\n" + (longme + ": max " + Reduce.max(longme,  1));
    }

    log += "\n" + ("Doubles\n-------\n");
    log += "\n" + ("Scan test:");
    log += "\n" + (doubleme + ": add " + Scan.add(doubleme));
    log += "\n" + (doubleme + ": mult " + Scan.mult(doubleme));
    log += "\n" + (doubleme + ": min " + Scan.min(doubleme));
    log += "\n" + (doubleme + ": max " + Scan.max(doubleme));
    log += "\n" + ("Reduce test:");
    log += "\n" + (doubleme + ": add " + Reduce.add(doubleme));
    log += "\n" + (doubleme + ": mult " + Reduce.mult(doubleme));
    log += "\n" + (doubleme + ": min " + Reduce.min(doubleme));
    log += "\n" + (doubleme + ": max " + Reduce.max(doubleme));
    log += "\n" + ("Reduce to 0 test:");
    log += "\n" + (doubleme + ": add " + Reduce.add(doubleme, 0));
    log += "\n" + (doubleme + ": mult " + Reduce.mult(doubleme, 0));
    log += "\n" + (doubleme + ": min " + Reduce.min(doubleme, 0));
    log += "\n" + (doubleme + ": max " + Reduce.max(doubleme, 0));
    if (n > 1) {
      log += "\n" + ("Reduce to 1 test:");
      log += "\n" + (doubleme + ": add " + Reduce.add(doubleme,  1));
      log += "\n" + (doubleme + ": mult " + Reduce.mult(doubleme,  1));
      log += "\n" + (doubleme + ": min " + Reduce.min(doubleme,  1));
      log += "\n" + (doubleme + ": max " + Reduce.max(doubleme,  1));
    }

    // Dump logs to stdout
    for (int single i = 0; i < n; i++) {
      if (me == i) {
	System.out.println(log);
	// extra time for GLUnix to flush streams
	try { Thread.sleep(2000); } catch (InterruptedException e) { }
      }
      Ti.barrier();
    }
  }
}
