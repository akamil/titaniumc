class A { }

class B extends A { }

class s7 extends java.util.Vector {
  static java.util.Vector[] w = new s7[2];
  static Object[] x = new String[3]; 
  static Object[] barf;
  public static boolean FALSE = false;
  public static void main(String[] args) {
    // PR711
    String[] o = (String[]) x;  
    s7[] p = (s7[]) w;  
    Object o2 = p;
    Object q = (Object) args; 
    String[] r = (String[]) q;
    Object[] s = (Object[]) args; 
    Object nl = null;
    Object[] nla = null;
    Object obj = new Object();
    try {
      barf = (Object[])obj;
      if (FALSE) throw new ClassCastException(); // prevent bad opts due to erroneous exception
      System.out.println("error at " + __LINE__);
    } catch (ClassCastException exn) {}
    try {
      char [] c = new char[10];
      barf = (Object[])(Object)c;
      if (FALSE) throw new ClassCastException(); // prevent bad opts due to erroneous exception
      System.out.println("error at " + __LINE__);
    } catch (ClassCastException exn) {}

    // PR340
    A [] a = new B[10];
    B [] b = (B[]) a;
    if (b == null) System.out.println("test failed");

    // other misc checks
    assert x instanceof Object[];
    assert x instanceof String[];
    assert o instanceof Object[];
    assert o instanceof String[];
    assert q instanceof Object[];
    assert q instanceof String[];
    assert s instanceof Object[];
    assert s instanceof String[];
    assert w instanceof Object[];
    assert w instanceof java.util.Vector[];
    assert w instanceof s7[];
    assert p instanceof Object[];
    assert p instanceof java.util.Vector[];
    assert p instanceof s7[];
    assert o2 instanceof Object[];
    assert o2 instanceof java.util.Vector[];
    assert o2 instanceof s7[];
    assert !(nl instanceof Object);
    assert !(nl instanceof Object[]);
    assert !(nla instanceof Object);
    assert !(nla instanceof Object[]);
    assert !(obj instanceof Object[]);
    assert !(obj instanceof String[]);
    System.out.println("done.");
  }
}
