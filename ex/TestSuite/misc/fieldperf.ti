// field access performance tester, Dan Bonachea

public class fieldperf {
  public static single void report(String desc, Timer t, int iters) {
    Ti.barrier();
    if (Ti.thisProc() == 0)
    System.out.println(desc + ": " + t.micros()/iters*1000.0 + " ns per iteration ("
                       + t.secs() + " secs total)");
    Ti.barrier();
  }
 #define TEST(msg, code) do { \
    t.reset(); \
    t.start(); \
    for (int i=0; i < iters; i++) { \
      code; \
    } \
    t.stop(); \
    report(msg, t, iters); \
  } while (false)

  public static int staticvar = 0;
  public int instancevar = 0;
  public static void snoop() {}
  public local void lnoop() {}
  public void gnoop() {}

  public static single void main(String[]args) {
    int iters0 = 1000000;
    if (args.length > 0) 
      try { iters0 = Integer.parseInt(args[0]); } catch (Throwable exn) {}
    int single iters = broadcast iters0 from 0;
    if (Ti.thisProc()==0) 
      System.out.println("Running " + iters + " iterations of field access performance test on " 
                        + Ti.numProcs() + " threads");
    PrivateRegion pr = new PrivateRegion();
    SharedRegion sr = new SharedRegion();
    fieldperf single local lthis = new fieldperf();
    fieldperf single gthis = (fieldperf single)(broadcast lthis from 0); // defeat LQI
    gthis = lthis;
    fieldperf single local lthispr = new (pr) fieldperf();
    fieldperf single gthispr = (fieldperf single)(broadcast lthispr from 0); // defeat LQI
    gthispr = lthispr;
    fieldperf.doitstatic(iters);
    lthis.doitlocal(iters,"GC");
    gthis.doitglobal(iters,"GC");
    lthispr.doitlocal(iters,"PrivateRegion");
    gthispr.doitglobal(iters,"PrivateRegion");
  }

  public static single void doitstatic(int single iters) {
    Timer t = new Timer();
    int x;
    if (Ti.thisProc() == 0) System.out.println("--------------------------------------");
    Ti.barrier();
    TEST("loop overhead           ", snoop(); );
    TEST("static read             ", x += staticvar; snoop(); );
    TEST("static write            ", staticvar = x; snoop(); );
    TEST("static RMW              ", staticvar += x; snoop(); );

  }
  public single void doitglobal(int single iters,String type) {
    Timer t = new Timer();
    int x;

    if (Ti.thisProc() == 0) System.out.println("--------------------------------------");
    Ti.barrier();
    TEST("loop overhead          ", gnoop(); );
    TEST("global "+type+" instance read ", x += instancevar; gnoop(); );
    TEST("global "+type+" instance write", instancevar = x; gnoop(); );
    TEST("global "+type+" instance RMW  ", instancevar += x; gnoop(); );

  }
  public single local void doitlocal(int single iters,String type) {
    Timer t = new Timer();
    int x;

    if (Ti.thisProc() == 0) System.out.println("--------------------------------------");
    Ti.barrier();
    TEST("loop overhead           ", lnoop(); );
    TEST("local "+type+" instance read  ", x += instancevar; lnoop(); );
    TEST("local "+type+" instance write ", instancevar = x; lnoop(); );
    TEST("local "+type+" instance RMW   ", instancevar += x; lnoop(); );

  }
}
