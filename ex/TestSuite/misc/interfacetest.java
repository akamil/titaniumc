import java.util.*;

interface A {
  public void f1();
}
interface B extends A {
  public void f2();
}
interface C extends A {
  public void f3();
}
interface D extends B,C {
  public void f4();
}

interface E {
  public void f5();
}

interface F {
  public void f6();
}

interface G {
  public void f7();
}

interface H extends G {
  public void f8();
}
interface I extends H {}
interface J extends H {}
interface K extends I,J {}
interface L extends K {
  public void f9();
}

class C1 implements E {
  public void f5() { System.out.println("f5"); }
}

class C2 extends C1 implements F {
  public void f6() { System.out.println("f6"); }
}

class C3 implements D {
  public void f1() { System.out.println("f1"); }
  public void f2() { System.out.println("f2"); }
  public void f3() { System.out.println("f3"); }
  public void f4() { System.out.println("f4"); }
}

class C4 extends C3 {}

class C5 extends C4 implements L {
  public void f7() { System.out.println("f7"); }
  public void f8() { System.out.println("f8"); }
  public void f9() { System.out.println("f9"); }
}

interface M {
  public void f10();
}
interface N {}
interface O extends N {
  public void f11();
}
interface P extends O {}

abstract class C6 extends C5 implements M, P {
  public abstract void f10();
  public abstract void f11();
}

class C7 extends C6 implements O {
  public void f10() { System.out.println("f10"); }
  public void f11() { System.out.println("f11"); }
}

interface Q {
  public void f12();
}
interface R extends Q {}
interface S {
  public void f13();
  public void f14();
}
abstract class C8 implements R {
  public abstract void f12();
}
abstract class C9 extends C8 implements S {
  public abstract void f13();
  public void f14() { System.out.println("f14"); }
}

class C10 extends C9 {
  public void f12() { System.out.println("f12"); }
  public void f13() { System.out.println("f13"); }
}
class C11 extends C10 {
}

interface T {
  public void f15();
}
class C12 implements T {
  public void f15() {
    System.out.println("f15");
  }
}

public class interfacetest {
  public static void C3Test(C3 c3) {
    ((A)c3).f1();
    ((B)c3).f1();
    ((C)c3).f1();
    ((D)c3).f1();

    ((B)c3).f2();
    ((D)c3).f2();

    ((C)c3).f3();
    ((D)c3).f3();

    ((D)c3).f4();
  }
  public static void C5Test(C5 c5) {
    ((G)c5).f7();
    ((H)c5).f7();
    ((I)c5).f7();
    ((J)c5).f7();
    ((K)c5).f7();
    ((L)c5).f7();

    ((H)c5).f8();
    ((I)c5).f8();
    ((J)c5).f8();
    ((K)c5).f8();
    ((L)c5).f8();

    ((L)c5).f9();
  }
  public static void C10Test(C10 c10) {
    ((Q)c10).f12();
    ((R)c10).f12();
    ((C8)c10).f12();
    ((C9)c10).f12();
    ((S)c10).f13();
    ((S)c10).f14();
    ((C9)c10).f13();
    ((C9)c10).f14();
    ((C10)c10).f13();
    ((C10)c10).f14();
  }
  public static single void main(String [] args) {
    disalignHeap();

    C1 c1 = new C1();
    ((E)c1).f5();
    System.out.println("---");

    C2 c2 = new C2();
    ((E)c2).f5();
    ((F)c2).f6();
    System.out.println("---");

    C3 c3 = new C3();
    C3Test(c3);
    System.out.println("---");

    C4 c4 = new C4();
    C3Test(c4);
    System.out.println("---");

    C5 c5 = new C5();
    C3Test(c5);
    C5Test(c5);
    System.out.println("---");

    C7 c7 = new C7();
    C3Test(c7);
    C5Test(c7);
    ((M)c7).f10();
    ((O)c7).f11();
    ((P)c7).f11();

    ((C6)c7).f1();
    ((C6)c7).f2();
    ((C6)c7).f3();
    ((C6)c7).f4();
    ((C6)c7).f7();
    ((C6)c7).f8();
    ((C6)c7).f9();
    ((C6)c7).f10();
    ((C6)c7).f11();
    System.out.println("---");

    C10 c10 = new C10();
    C10Test(c10);
    System.out.println("---");


    C11 c11 = new C11();
    C10Test(c11);
    ((C11)c11).f14();
    System.out.println("---");

    disalignHeap();
    T t = broadcast ((T) new C12()) from 0;
    t.f15();
  }

  public static single void disalignHeap() {
    Stack s = new Stack(); // dis-align the heaps
    for (int i=0;i<Ti.thisProc()*100;i++) {
      s.push(new String("yo mama"));
      s.push(new Integer(5));
      s.push(new byte[i]);
    }
    Ti.barrier();
  }
}

