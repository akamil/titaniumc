import java.io.*;

public class TestOut {

  static int MaxLines = 10000;
  static String outputfile = "outfile";

  public static void main(String args[]) {

    OutputStream fout;
    DataOutputStream out;

    try {
      System.out.println("Opening file " + outputfile + " for output");
      fout = new FileOutputStream(outputfile);
      out = new DataOutputStream(fout);
    } catch (IOException x) {
      System.out.println("Error in opening " + outputfile);
      System.exit(0);
    }

    String outputline;
    Format LongE = new Format("%26.15e");

    try {
      for (int k = 1; k <= MaxLines; k++) {
	outputline = "[" + k + "]:  " + LongE.form(Math.sqrt((double) k))+'\n';
	out.writeBytes(outputline);
      }
      out.close();
    } catch (IOException x) {
      System.out.println("Error in writing to " + outputfile);
      System.exit(0);
    }
  }
}
