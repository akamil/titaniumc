import java.util.*;
import java.io.*;
import java.util.zip.*;

public class DeflateInflateTest {
    public static void main(String[] args) {
	// Encode a String into bytes
	String inputString = "this is the input string!!";
	byte[] input = new byte[inputString.length()];
	inputString.getBytes(0, inputString.length(), input, 0);
	System.out.println(new String(input));

	// Compress the bytes
	byte[] output = new byte[100];
	System.out.println("Deflater created...");
	Deflater compresser = new Deflater(Deflater.BEST_COMPRESSION);
	compresser.setInput(input);
	compresser.finish();
	System.out.println("Adler: " + compresser.getAdler());
	System.out.println("getTotalIn: " + compresser.getTotalIn());
	System.out.println("getTotalOut: " + compresser.getTotalOut());

	int compressedDataLength = compresser.deflate(output);
	System.out.println("String deflated...");
	System.out.println("compressedDataLength: " + compressedDataLength);
	System.out.println("getTotalOut: " + compresser.getTotalOut());
	System.out.println("needsInput? (true) : " + compresser.needsInput());

	// Decompress the bytes
	Inflater decompresser = new Inflater();
	System.out.println("Inflater created...");
	System.out.println("getRemaining: " + decompresser.getRemaining());
	decompresser.setInput(output, 0, compressedDataLength);
	System.out.println("getTotalIn: " + decompresser.getTotalIn());
	System.out.println("getTotalOut: " + decompresser.getTotalOut());
	byte[] result = new byte[100];
	try {
	    int resultLength = decompresser.inflate(result);
	    System.out.println("String inflated...");
	    System.out.println("getTotalOut: " + decompresser.getTotalOut());
	    System.out.println("needsInput? (true) : " + decompresser.needsInput());

	    // Decode the bytes into a String
	    String outputString = new String(result, 0, resultLength);
	    System.out.println("decompressed string: " + outputString);
	    decompresser.end();
	} catch(DataFormatException e) {
	    System.out.println(e.getMessage());
	}
    }
}
