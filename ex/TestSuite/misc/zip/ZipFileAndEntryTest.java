import java.util.*;
import java.io.*;
import java.util.zip.*;

public class ZipFileAndEntryTest {
    public static void main(String[] args) {
	//Test ZipEntry values extraction made from real zip unix command
	try {
	    //path specially for this testsuite directory structure
	    String currdir = System.getenv("TS_HOME_DIRECTORY");
            if (currdir == null) currdir = System.getProperties().getProperty("user.dir");
            else currdir += "/misc/zip/";
	    System.out.println("opening ZipFileAndEntryTest.txt.zip...");
	    ZipFile zf = new ZipFile(currdir + "ZipFileAndEntryTest.txt.zip");
	    System.out.println("getting ZipEntry ZipFileAndEntry.txt");
	    ZipEntry ze = zf.getEntry("ZipFileAndEntryTest.txt");
	    System.out.println("comment for this ZipEntry: " + ze.getComment());
	    System.out.println("compressed size for this ZipEntry: " + ze.getCompressedSize());
	    System.out.println("crc for this ZipEntry: " + ze.getCrc());
	    System.out.println("method for this ZipEntry (0==STORED, 8==DEFLATED): " + ze.getMethod());
	    System.out.println("name for this ZipEntry: " + ze.getName());
	    System.out.println("decompressed size for this ZipEntry: " + ze.getSize());
	    System.out.println("modification time for this ZipEntry: " + ze.getTime());
	    System.out.println("is this ZipEntry a directory? (false) : " + ze.isDirectory());
	    System.out.println("testing getInputStream... (uncompressed output should be printed after this)");
	    System.out.println();
	    System.out.println("----------------------------------------------------------------------------");
	    System.out.println();
	    InputStream is = zf.getInputStream(new ZipEntry("ZipFileAndEntryTest.txt"));
	    OutputStream os = System.out;
	    byte[] buffer = new byte[100];
	    int read = is.read(buffer);
	    while(read >= 0) {
		os.write(buffer, 0, read);
		read = is.read(buffer);
	    }
	    is.close();
	    os.close();
	} catch(IOException e) {
	    System.out.println(e.getMessage());
	}
    }
}
