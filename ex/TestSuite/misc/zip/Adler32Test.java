import java.io.*;
import java.util.*;
import java.util.zip.*;

public class Adler32Test {
	public static void main(String[] args) {
		Adler32 a = new Adler32();
		System.out.println("updated-value : new-checksum");
		for(int i = 0; i < 10; i++) {
			a.update((byte)i);
			System.out.println(i + " : " + a.getValue());
		}
		byte[] b = new byte[100];
		for(int i = 0; i < 10; i++) {
			b[i] = (byte) (i | 201);
		}
                System.out.println("updating array b...");
		a.update(b);
		System.out.println("new-checksum: " + a.getValue());
		System.out.println("resetting...");
		a.reset();
                System.out.println("updating array b again...");
		a.update(b);
                System.out.println("new-checksum: " + a.getValue());
	}
}
