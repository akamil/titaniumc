public class assertTest {
/* must be run with assertions enabled */
public static boolean flag1 = false;
public static boolean flag2 = false;
public static boolean returnsTrue() { flag1 = true; return true; }
public static boolean returnsFalse() { flag1 = true; return false; }
public static Object foo() { flag2 = true; return "assert failed"; }

public static void main(String[]args) {

  assert true;
  assert true : 1;
  assert true : "hi";
  assert true : 3.14;
  System.out.println("good1");
  assert returnsTrue() : foo();
  if (!flag1 || flag2)  System.out.println("err1");
  flag1 = false;
  try {
    assert false;
    System.out.println("err2");
  } catch (AssertionError exn) { System.out.println("good2"); }
  try {
    assert returnsFalse() : foo();
    System.out.println("err3");
  } catch (AssertionError exn) { System.out.println("good3"); }
  if (!flag1 || !flag2)  System.out.println("err4");
  flag1 = flag2 = false;

  System.out.println("done.");
}}
