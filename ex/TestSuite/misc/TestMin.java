class TestMin {
  static final int N = 2;   // manifest constant

  public static void main( String[] args ) {
    Point<N> p = Point<N>.all(1);
    RectDomain<N> D = [p:p];
    Point<N> q = D.min();
    System.out.println("min " + q);
  }
}
