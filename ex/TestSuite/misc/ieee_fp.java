class Test {
   public static double d;
   public static double zero = 0.0;
	public static void main(String[] args) {
		// An example of overflow:
		d = 1e308;
		System.out.print("overflow produces infinity: ");
		System.out.println(d + "*10==" + d*10);

		System.out.print("1.0/0.0 produces +infinity: ");
                d = 1.0;
                d = d / zero;
		System.out.println(d);
                System.out.println(1.0/zero);

		System.out.print("-1.0/0.0 produces -infinity: ");
                d = -1.0;
                d = d / zero;
		System.out.println(d);
                System.out.println(-1.0/zero);

                d = Double.NaN;
                if (d == Double.NaN) System.out.println("bad NaN==NaN");
                if (!(d != Double.NaN)) System.out.println("bad NaN!=NaN");
                if (d == Double.POSITIVE_INFINITY) System.out.println("bad NaN==Inf");
                if (!(d != Double.POSITIVE_INFINITY)) System.out.println("bad NaN!=Inf");
                d = Double.POSITIVE_INFINITY;
                if (!(d == Double.POSITIVE_INFINITY)) System.out.println("bad Inf==Inf");
                if ((d != Double.POSITIVE_INFINITY)) System.out.println("bad Inf!=Inf");
                if (d == Double.NEGATIVE_INFINITY) System.out.println("bad Inf==-Inf");
                if (!(d != Double.NEGATIVE_INFINITY)) System.out.println("bad Inf!=-Inf");

		// An example of gradual underflow:
		d = 1e-305 * Math.PI;
		System.out.print("gradual underflow: " + d + "\n      ");
		for (int i = 0; i < 4; i++)
			System.out.print(" " + (d /= 100000));
		System.out.println();
		// An example of NaN:
		System.out.print("0.0/0.0 is Not-a-Number: ");
		d = 0.0;
                d = d/zero;
		System.out.println(d);
                //System.out.println(0.0/0.0);
		// An example of inexact results and rounding:
		System.out.print("inexact results with float:");
		for (int i = 0; i < 100; i++) {
			float z = 1.0f / i;
			if (z * i != 1.0f)
				System.out.print(" " + i);
		}
		System.out.println();
		// Another example of inexact results and rounding:
		System.out.print("inexact results with double:");
		for (int i = 0; i < 100; i++) {
			double z = 1.0 / i;
			if (z * i != 1.0)
				System.out.print(" " + i);
		}
		System.out.println();
		// An example of cast to integer rounding:
		System.out.print("cast to int rounds toward 0: ");
		d = 12345.6;
		System.out.println((int)d + " " + (int)(-d));
	}
}


