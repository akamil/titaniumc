/**
 * Use MeshRefine.refine() to return a new set of rectangles given
 * tagged domains at a number of levels.
 *
 * @version  7 Feb 2000
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public class MeshRefine {

  private static final int SPACE_DIM = Util.SPACE_DIM;

  /** process used for broadcasts */
  private static final int single SPECIAL_PROC = 0;

  private static final int single COARSENING_RATIO = 2;

  /**
   * Returns a new array of arrays of RectDomains, indexed by levels.
   *
   * Given an array of domains of tagged cells, indexed by [base:top],
   * the returned array of arrays of RectDomain is indexed by [base+1:top+1].
   * Need Amr object for boxes, physical domains and refinement ratios.
   * @param myRegion region to contain the result
   * @param amrAll parent Amr object
   * @param tagged array of tagged domains at different levels
   */
  single public static RectDomain<SPACE_DIM> [1d][1d]
    refine(
	   SharedRegion single myRegion, // REGION
	   Amr amrAll, Domain<SPACE_DIM> [1d] tagged) {

    RectDomain<1> levinds = tagged.domain();
    RectDomain<1> single levindsnew = broadcast (levinds+[1]) from SPECIAL_PROC;
    int base = levinds.min()[1], top = levinds.max()[1];
    RectDomain<SPACE_DIM> [1d][1d] boxes = new
      (myRegion) // REGION
      RectDomain<SPACE_DIM> [levindsnew][1d];

    SharedRegion single tempRegion = new SharedRegion(); // REGION
    { // scope for new objects using tempRegion

      RectDomain<SPACE_DIM> [1d][1d] newGrids;

      // System.out.println("going to SPECIAL_PROC");
      if (Ti.thisProc() == SPECIAL_PROC) {

	/*
	  General plan:
	  
	  Coarsen oldGrids[base:top] by 2.
	  Then run MeshRefineD with
	  dProblemBase = dProblem[base] / 2, and ref_ratio[base:top],
	  to get newGrids[base+1:top+1].
	  Then refine newGrids[base+1:top+1] by 2.
	*/

	// refinement ratio between this and next finer level
	// $4 = (int [1d]) 0xefffecd8 int[[[-279264380]:[-279264381]:[-279264380]]]
	// System.out.println("levinds = " + levinds);
	RectDomain<1> levindsAgain = levinds;
	// System.out.println("levindsAgain = " + levindsAgain);
	int [1d] ref_ratio = new
	  (tempRegion) // REGION
	  int[levindsAgain];
	// System.out.println("ref_ratio domain size = " + ref_ratio.domain().size());
	foreach (h in levindsAgain) ref_ratio[h] = amrAll.getRefRatio(h);

	// physical domain at base level
	RectDomain<SPACE_DIM> dProblemBase = amrAll.getDomain(base);
	// System.out.println("base domain = " + dProblemBase);

	// copy existing boxes to oldGrids
	RectDomain<SPACE_DIM> [1d][1d] oldGrids = new
	  (tempRegion) // REGION
	  RectDomain<SPACE_DIM>[levindsAgain][1d];
	foreach (h in levindsAgain) {
	  RectDomain<SPACE_DIM> [1d] levelBoxes =
	    amrAll.getLevelNonsingle(h).getBa().boxes();
	  oldGrids[h] = new
	    (tempRegion) // REGION
	    RectDomain<SPACE_DIM>[levelBoxes.domain()];
	  foreach (indbox in levelBoxes.domain())
	    oldGrids[h][indbox] = levelBoxes[indbox];
	}

	/*
	  Coarsen domains by COARSENING_RATIO.
	*/

	dProblemBase = Util.coarsen(dProblemBase, COARSENING_RATIO);
	// System.out.println("coarsened base domain = " + dProblemBase);

	foreach (h in levindsAgain) Util.coarsen(oldGrids[h], COARSENING_RATIO);
	Util.coarsen(tagged, COARSENING_RATIO);
	
	/*
	  Use MeshRefineD to retrieve newGrids.
	*/

	// System.out.println("getting MeshRefineD object");
	MeshRefineD mrd = new
	  (tempRegion) // REGION
	  MeshRefineD(
		      tempRegion, // REGION
		      oldGrids, tagged, dProblemBase, ref_ratio);

	// System.out.println("refining MeshRefineD object");
	newGrids = mrd.refine();
      
	/*
	  Refine newGrids by COARSENING_RATIO.
	*/

	// System.out.println("refineBoxes");
	foreach (h in levindsnew) Util.refine(newGrids[h], COARSENING_RATIO);

      } // if (Ti.thisProc() == SPECIAL_PROC)
      Logger.barrier();
      Logger.append("got new grids");

      // Need to set up newGrids on other procs so that newGrids[h] exists.
      if (Ti.thisProc() != SPECIAL_PROC)
	newGrids = new
	  (tempRegion) // REGION
	  RectDomain<SPACE_DIM>[levindsnew][0:-1];

      foreach (h in levindsnew) {
	RectDomain<1> single boxesIndices =
	  broadcast newGrids[h].domain() from SPECIAL_PROC;
	boxes[h] = new
	  (myRegion) // REGION
	  RectDomain<SPACE_DIM> [boxesIndices];
	foreach (indbox in boxesIndices)
	  boxes[h][indbox] = broadcast newGrids[h][indbox] from SPECIAL_PROC;
      }
    } // scope for new objects using tempRegion
    // System.out.println("deleting tempRegion in MeshRefine");
    RegionUtil.delete(tempRegion); // REGION
    // System.out.println("done with MeshRefine");
    return boxes;
  }
}
