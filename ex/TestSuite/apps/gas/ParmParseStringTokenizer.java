import java.io.*;

public class ParmParseStringTokenizer extends ParmParseTokenizer {
    private String argString;
    private int currentIndex = 0;  // index into string being parsed

    /** 
     * Creates a stream tokenizer that parses the specified input
     * stream.
     * By default, it recognizes numbers, Strings quoted with 
     * single and double quotes, and all the alphabetics.
     * @param I the input stream 
     */
    public ParmParseStringTokenizer (String argStringGiven) {
        super();
	LINENO = 0;
        argString = argStringGiven;
    }

    protected int readChar() throws IOException {
      // currentIndex is pointer to a location in argString
      if (currentIndex >= argString.length()) return TT_EOF;
      int c = argString.charAt(currentIndex);
      currentIndex++;
      return c;
    }
}
