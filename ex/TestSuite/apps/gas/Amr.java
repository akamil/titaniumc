import java.io.*;

/**
 * The Amr class is designed to manage parts of the computation
 * which do not belong on a single level, like establishing and updating
 * the hierarchy of levels, global timestepping, and managing the different
 * AmrLevels.
 *
 * @version  1 Sep 2000
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public class Amr {

  /*
    constants
  */

  private static final int single SPACE_DIM = Util.SPACE_DIM;

  /** process used for broadcasts */
  private static final int single SPECIAL_PROC = 0;
  
  /*
    fields that are set in constructor
  */

  /** dimensions of coarsest-level box */
  private Point<SPACE_DIM> single extent;

  /** current finest level */
  private int single finest_level;

  /** number of iterations between regriddings */
  private int single regrid_int;

  /** number of iterations between writes to plotfiles */
  private int single plot_int;

  /**
   * array of refinement ratios [0:finest_level-1];
   * This is the ratio between this level and the next FINER level.
   * dt_level[k] = dt_level[k-1]/REAL(ref_ratio[k-1]);
   */
  private int single [1d] ref_ratio;

  /** coordinates of the box ends */
  private double single [1d][1d] ends; // [Util.DIRECTIONS][Util.DIMENSIONS]

  private AmrLevel single [1d] amr_level;

  /** physical time step at each level */
  private double single [1d] dt_level;

  /** grid spacing at each level */
  private double single [1d] dx_level;

  /** physical domain at each level */
  private RectDomain<SPACE_DIM> single [1d] dProblem;

  /** number of steps made at each level */
  private int single [1d] level_steps;

  private String outputFilePrefix;

  /** maximum number of coarse-level steps */
  private double single maxSteps;

  /** maximum physical time to run the simulation */
  private double single maxCumtime;

  private boolean single printElapsed, printAdvance;

  private ParmParse pp;

  /*
    other fields
  */

  private boolean single saved = false;

  /** physical time variables.  initialized in getSaved() or run(). */
  private double single cumtime;
  private double single finetime;

  /**
   * constructor
   */
  single public Amr() {
    Util.set();

    // System.out.println("get pp for amr");
    pp = new ParmParse("amr");
    // System.out.println("read pp for amr");

    int [1d] extentRead = pp.getIntegerArray("n_cell");
    Point<SPACE_DIM> extentLocal = Util.ZERO;
    foreach (pdim in Util.DIMENSIONS) {
      int dim = pdim[1];
      extentLocal = extentLocal +
	Point<SPACE_DIM>.direction(dim, extentRead[dim-1]);
    }
    extent = broadcast extentLocal from SPECIAL_PROC;

    finest_level = broadcast pp.getInteger("max_level") from SPECIAL_PROC;

    // Change ParmParse so that you can call it with reference to an array
    // that gets modified?
    int [1d] inRefRatio = pp.getIntegerArray("ref_ratio");
    if (inRefRatio.domain().size() < finest_level) {
      System.out.println("Amr:  amr.ref_ratio needs refinement ratios " +
			 "up to level " + finest_level);
      System.exit(0);
    }
    ref_ratio = new int single[0:finest_level-1];
    foreach (ind in ref_ratio.domain()) {
      ref_ratio[ind] = broadcast inRefRatio[ind] from SPECIAL_PROC;
    }

    // System.out.println("getting prob_lo");
    double [1d] prob_lo = pp.getDoubleArray("prob_lo");
    if (prob_lo.domain().size() != SPACE_DIM) {
      System.out.println("Amr:  See amr.prob_lo.  Problem is " +
			 SPACE_DIM + "-dimensional");
    }

    // System.out.println("getting prob_hi");
    double [1d] prob_hi = pp.getDoubleArray("prob_hi");
    if (prob_hi.domain().size() != SPACE_DIM) {
      System.out.println("Amr:  See amr.prob_hi.  Problem is " +
			 SPACE_DIM + "-dimensional");
    }

    // System.out.println("getting ends");
    ends = new double single[Util.DIRECTIONS][Util.DIMENSIONS];
    foreach (pdim in Util.DIMENSIONS) {
      ends[-1][pdim] = broadcast prob_lo[pdim - [1]] from SPECIAL_PROC;
      ends[+1][pdim] = broadcast prob_hi[pdim - [1]] from SPECIAL_PROC;
      // System.out.println("ends" + pdim + ":  " + ends[-1][pdim] + " to " +
      //					ends[+1][pdim]);
    }

    // System.out.println("got pp stuff for amr");
    // System.out.println("finest_level = " + finest_level);
    /*
      Global arrays of Amr parameters.
    */

    // System.out.println("defining amr_level, dt_level");
    RectDomain<1> single levs = [0 : finest_level];
    amr_level = new AmrLevel single[levs];
    dt_level = new double single[levs];
    dx_level = new double single[levs];
    dProblem = new RectDomain<SPACE_DIM> single[levs];

    /*
      Find domain at each level.
    */
    Point<SPACE_DIM> single extentLevel = extent;
    dProblem[0] = [Util.ZERO : (extentLevel - Util.ONE)];
    for (int single lev = 1; lev <= finest_level; lev++) {
      extentLevel = extentLevel * ref_ratio[lev-1];
      dProblem[lev] = [Util.ZERO : (extentLevel - Util.ONE)];
    }

    /*
      Find grid spacing at each level.
    */
    int single dim = 1;
    dx_level[0] = (ends[+1][dim] - ends[-1][dim]) / (double) extent[dim];
    // Check that other dimensions agree.
    for (dim = 2; dim <= SPACE_DIM; dim++) {
      double single dxdim = (ends[+1][dim] - ends[-1][dim]) / (double) extent[dim];
      if (dxdim != dx_level[0]) {
	System.out.println("Amr:  Aspect ratio of problem box is incorrect.");
        System.out.println("Ratio " + dx_level[0] + " in dimension 1 and " +
                           dxdim + " in dimension " + dim);
	System.exit(0);
      }
    }
    for (int single lev = 1; lev <= finest_level; lev++) {
      dx_level[lev] = dx_level[lev-1] / (double) ref_ratio[lev-1];
    }

    level_steps = new int single[levs];
    foreach (lev in levs) level_steps[lev] = 0;

    /*
      Variables listed below are not saved in plotfile.
    */

    regrid_int = broadcast pp.getInteger("regrid_int") from SPECIAL_PROC;
    plot_int = broadcast pp.getInteger("plot_int") from SPECIAL_PROC;
    String plot_file = pp.getString("plot_file");

    // System.out.println("getting outputfileprefix");
    outputFilePrefix = plot_file + '.';
    for (int d = 1; d <= SPACE_DIM; d++) 
      outputFilePrefix = outputFilePrefix + extent[d] + ".";
    outputFilePrefix = outputFilePrefix + finest_level + '.' +
      Ti.numProcs() + '.';

    // System.out.println("getting max_step, stop_time");
    maxSteps = broadcast pp.getDouble("max_step") from SPECIAL_PROC;
    maxCumtime = broadcast pp.getDouble("stop_time") from SPECIAL_PROC;
    // System.out.println("got max_step, stop_time");

    printElapsed = broadcast pp.contains("print_elapsed") from SPECIAL_PROC;
    printAdvance = broadcast pp.contains("print_advance") from SPECIAL_PROC;
  }


  /*
    accessor methods
  */

  single public AmrLevel single getLevel(int single lev) {
    if (0 <= lev && lev <= finest_level)
      return amr_level[lev];
    else
      return null;
  }

  single public AmrLevel single getLevel(Point<1> single lev) {
    return getLevel(lev[1]);
  }
  
  public double single [1d][1d] getEnds() {
    return ends;
  }

  public double [1d][1d] getEndsNonsingle() {
    double [1d][1d] endsNonsingle = new double[Util.DIRECTIONS][Util.DIMENSIONS];
    foreach (pdir in Util.DIRECTIONS) foreach (pdim in Util.DIMENSIONS) {
      endsNonsingle[pdir][pdim] = ends[pdir][pdim];
    }
    return endsNonsingle;
  }

  public AmrLevel getLevelNonsingle(int lev) {
    if (0 <= lev && lev <= finest_level)
      return amr_level[lev];
    else
      return null;
  }

  public AmrLevel getLevelNonsingle(Point<1> lev) {
    return getLevelNonsingle(lev[1]);
  }

  single public void setLevel(int single lev, AmrLevel single amrlev)
    { amr_level[lev] = amrlev; }

  public Point<SPACE_DIM> single getExtent() { return extent; }

  public int getLevelSteps(int lev) {
    if (0 <= lev && lev <= finest_level) {
      return level_steps[lev];
    } else {
      System.out.println("Amr:  Trying to get steps for level " + lev);
      return 0;
    }
  }

  public int getLevelSteps(Point<1> lev) {
    return getLevelSteps(lev[1]);
  }

  public int single finestLevel() { return finest_level; }

  public double single getCumtime() { return cumtime; }

  public double single getFinetime() { return finetime; }

  public int single getRefRatio(int single lev) {
    if (lev >= ref_ratio.domain().size()) {
      System.out.println("Amr:  No refinement ratio given up to level " +
			 lev);
      System.exit(0);
    }
    return ref_ratio[lev];
  }

  public int single getRefRatio(Point<1> single lev) {
    return getRefRatio(lev[1]);
  }

  public double single getDx(int single lev) { return dx_level[lev]; }

  public double single getDx(Point<1> single lev) { return dx_level[lev]; }

  public double single getDt(int single lev) { return dt_level[lev]; }

  public double single getDt(Point<1> single lev) { return dt_level[lev]; }

  public RectDomain<SPACE_DIM> single getDomain(int single lev) { return dProblem[lev]; }

  public RectDomain<SPACE_DIM> single getDomain(Point<1> single lev) { return dProblem[lev]; }

  /*
    methods
  */

  /**
   * Read data from saved plotfile.
   * (1) Number of states (int)
   * (2) Name of state (aString)
   * (3) Number of dimensions (int)
   * (4) Time (REAL)
   * (5) Finest level written (int)
   * (6) Loside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
   * (7) Hiside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
   * (8) Refinement ratio ((numLevels-1)*(int))
   * (9) Domain of each level (numLevels* (BOX) )
   * (10) Steps taken on each level (numLevels*(int) )
   * (11) Grid spacing (numLevels*(BL_SPACEDIM*(int))) [\n at each level]
   * (12) Coordinate flag (int)
   * (13) Boundary info (int)
   */
  single public void getSaved(MyInputStream in)
    throws IOException, EOFException {
    saved = true;
    System.out.println("in getSaved");

    // (1) Number of states (int)
    int savedStates = in.readInt1();
    System.out.println("saved states: " + savedStates);

    // (2) Name of state (aString)
    for (int i = 0; i < savedStates; i++) in.ignoreLine();

    // (3) Number of dimensions (int)
    int savedDims = in.readInt1();
    if (savedDims != SPACE_DIM) {
      System.out.println("Plot file is for " + savedDims +
			 "-dimensional problem instead of " + SPACE_DIM);
      System.exit(0);
    }
    System.out.println("saved dimensions: " + savedDims);

    // (4) Time (REAL)
    // double single savedTime = broadcast in.readDouble1() from SPECIAL_PROC;
    // cumtime = savedTime;
    // finetime = savedTime;
    double savedTime = in.readDouble1();
    System.out.println("saved time: " + savedTime);
    // FIX cumtime = broadcast savedTime from SPECIAL_PROC;
    // FIX finetime = cumtime;

    // (5) Finest level written (int)
    int savedFinest = in.readInt1();
    System.out.println("saved finest level: " + savedFinest);
    if (savedFinest != finest_level) {
      System.out.println("Plot file has finest level " + savedFinest +
			 " instead of " + finest_level);
      System.exit(0);
    }

    // (6) Loside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
    double [1d] savedProbLo = in.readDoubles();
    foreach (pdim in Util.DIMENSIONS) {
      double edge = savedProbLo[pdim-[1]];
      if (edge != ends[-1][pdim]) {
	System.out.println("Plot file has edge at " + edge +
			   " instead of " + ends[-1][pdim]);
	System.exit(0);
      }
    }

    // (7) Hiside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
    double [1d] savedProbHi = in.readDoubles();
    foreach (pdim in Util.DIMENSIONS) {
      double edge = savedProbHi[pdim-[1]];
      if (edge != ends[+1][pdim]) {
	System.out.println("Plot file has edge at " + edge +
			   " instead of " + ends[+1][pdim]);
	System.exit(0);
      }
    }

    // (8) Refinement ratio ((numLevels-1)*(int))
    int [1d] savedRefine = in.readInts();
    foreach (ind in ref_ratio.domain()) {
      if (savedRefine[ind] != ref_ratio[ind]) {
	System.out.println("Plot file has refinement ratio of " +
			   savedRefine[ind] +
			   " instead of " + ref_ratio[ind]);
	System.exit(0);
      }
    }

    // (9) Domain of each level (numLevels*(BOX) )
    RectDomain<SPACE_DIM> [1d] savedDomains = in.readDomains();
    Point<SPACE_DIM> extentLevel = extent;
    if (savedDomains[0] != [Util.ZERO : extentLevel - Util.ONE]) {
      System.out.println("Plot file has level 0 extent " +
			 savedDomains[0] +
			 " instead of " + extentLevel);
      System.exit(0);
    }
    for (int h = 1; h <= finest_level; h++) {
      extentLevel = extentLevel * Point<SPACE_DIM>.all(ref_ratio[h-1]);
      if (savedDomains[h] != [Util.ZERO : extentLevel - Util.ONE]) {
	System.out.println("Plot file has level " + h + " extent " +
			   savedDomains[h] +
			   " instead of " + extentLevel);
	System.exit(0);
      }
    }

    // (10) Steps taken on each level (numLevels*(int) )
    int [1d] savedSteps = in.readInts();
    /* FIX
    for (int single h = 0; h <= finest_level; h++)
      level_steps[h] = broadcast savedSteps[h] from SPECIAL_PROC;
    */

    // (11) Grid spacing (numLevels*(BL_SPACEDIM*(int))) [\n at each level]
    double dxLevel = 0.0; // dx;
    for (int h = 0; h <= finest_level; h++) {
      double [1d] savedDx = in.readDoubles();
      if (savedDx.domain().size() != SPACE_DIM) {
	System.out.println("Not right number of dimensions with grid spacing");
	System.exit(0);
      }
      for (int dim = 1; dim <= SPACE_DIM; dim++) {
	System.out.println("grid spacing " + savedDx[dim]);
	if (savedDx[dim] != dxLevel) {
	  System.out.println("Plot file has level " + h + " grid spacing " +
			     savedDx[dim] +
			     " instead of " + dxLevel);
	  System.exit(0);
	}
      }
      if (h < finest_level) dxLevel /= (double) ref_ratio[h];
    }

    // (12) Coordinate flag (int)
    in.ignoreLine();

    // (13) Boundary info (int)
    in.ignoreLine();

    // Now getSaved for each level.
    // for (int lev = 0; lev <= finest_level; lev++)
    // amr_level[height].getSaved(in);
    System.out.println("Now getSaved for each level.");
  }


  /**
   * Get initial BoxArray at level 0.
   * Each process has one box at level 0.  Fill them in later.
   */
  single public BoxArray single baseBoxes() {
    int single numProcs = Ti.numProcs();
    boolean single useDefaultDist = true;
    Point<SPACE_DIM> single base = Util.ZERO;

    if (broadcast pp.contains("base") from SPECIAL_PROC) {
      int [1d] baseRead = pp.getIntegerArray("base");
      int single baseProduct = 1;
      foreach (pdim in Util.DIMENSIONS) {
	int single dim = pdim[1];
	int single length = broadcast baseRead[dim-1] from SPECIAL_PROC;
	baseProduct *= length;
	base = base + Point<SPACE_DIM>.direction(dim, length);
      }
      if (baseProduct == numProcs) {
	useDefaultDist = false;
      } else {
	if (numProcs == 1) {
	  System.out.println("Ignored amr.base for " + baseProduct +
			     " processes, because using only 1 process.");
	} else {
	  if (Ti.thisProc() == SPECIAL_PROC) {
	    System.out.println("Ignored amr.base for " + baseProduct +
			       " processes, because using " + numProcs +
			       " processes.");
	    System.out.println("Will split along longest dimension instead.");
	  }
	}
	useDefaultDist = true;
      }
    }

    if (useDefaultDist) {
      // Split along longest dimension.  First find which is longest.
      int single longestDim = 1;
      for (int single dim = 2; dim <= SPACE_DIM; dim++) 
	if (extent[dim] > extent[longestDim]) longestDim = dim;

      base = Point<SPACE_DIM>.direction(longestDim, numProcs - 1) + Util.ONE;
      // e.g. [4-1, 0] + [1, 1] == [4, 1]
    }

    Point<SPACE_DIM> single fracExtent = extent / base;
    // Check that each dimension can be split as requested.
    foreach (pdim in Util.DIMENSIONS) {
      int dim = pdim[1];
      if (base[dim] * fracExtent[dim] != extent[dim]) {
	System.out.println("Amr:  Length " + extent[dim] +
			   " in dimension " + dim +
			   " not divisible by " + base[dim]);
	System.exit(0);
      }
    }

    RectDomain<SPACE_DIM> fracDomain = [Util.ZERO : fracExtent - Util.ONE];

    /*
      Assign boxes to processes, and return BoxArray.
    */

    RectDomain<1> procTeam = [0 : numProcs - 1];
    RectDomain<SPACE_DIM> [1d] domainlist = new RectDomain<SPACE_DIM>[procTeam];
    int [1d] proclist = new int[procTeam];
    
    int proc = 0;
    foreach (section in [Util.ZERO : base - Util.ONE]) {
      proclist[proc] = proc;
      domainlist[proc] = fracDomain + fracExtent * section;
      proc++;
    }

    return new BoxArray(domainlist, proclist);
    // Can't set amr_level[0] = new AmrLevel() right here because 
    // you need to do new HSCLLevel().
  }


  /**
   * Advance a level in time using the level operator.
   *
   * @param level which level
   * @param time physical time before advancement
   */
  single public void timeStep(int single level, double single time) {
    if (Ti.thisProc() == SPECIAL_PROC && printAdvance) {
      String s = "";
      for (int space = 0; space < level; space++) s = s + " ";
      System.out.println(s + "ADVANCE grids at level " + level +
			 " with dt = " + dt_level[level] + " from " +
			 time);
    }
    double dt_new = amr_level[level].advance(time, dt_level[level]);
    // System.out.println("done advance");
    level_steps[level]++;

    if (level < finest_level) {
      for (int single i = 0; i < ref_ratio[level]; i++) {
	timeStep(level + 1, time);
	time += dt_level[level + 1];
      }

      // System.out.println("doing post_timestep");
      // Use new  amr_level[level + 1]  to update  amr_level[level].
      amr_level[level + 1].post_timestep();
      // System.out.println("done post_timestep");
    } else {
      finetime += dt_level[level];
    }
  }


  single public void run() {
    // int single MaxSteps, double single MaxCumtime
    if (!saved) {
      cumtime = 0.0;
      finetime = 0.0;
    }
    long oldSysTime = System.currentTimeMillis();

    for (int single step = 1;
	 step <= maxSteps || cumtime < maxCumtime;	 
	 step++) {
      coarseTimeStep();
      if (Ti.thisProc() == SPECIAL_PROC) {
	long curSysTime = System.currentTimeMillis();
	if (printElapsed) {
	  System.out.println("elapsed seconds:  " +
			     ((curSysTime - oldSysTime)/1000));
	}
	oldSysTime = curSysTime;
      }
    }
  }


  /**
   * Make a time step on the coarsest grid.
   * Called by run().
   */
  single private void coarseTimeStep() {
    checkForRegrid();

    // if (level_steps[0] > 0)
    Logger.append("computeNewDt");
    computeNewDt();  // sets dt_level
    timeStep(0, cumtime);
    cumtime += dt_level[0];

    if (amr_level[0].isInitialStep()) amr_level[0].post_initial_step();

    checkForPlot();
  }


  single private void checkForRegrid() {
    if ((level_steps[0] > 0) && (level_steps[0] % regrid_int == 0)) regrid();
  }


  single private void checkForPlot() {
    if ((level_steps[0] > 0) && (level_steps[0] % plot_int == 0)) write();
  }


  single private void checkForPlotError() {
      // if ((level_steps[0] > 0) && (level_steps[0] % plot_int == 0)) writeError();
      if (level_steps[0] % plot_int == 0) writeError();
  }


  single public void write() {
    if (Ti.thisProc() == SPECIAL_PROC)
      amr_level[0].write(outputFilePrefix + level_steps[0]);
  }


  single public void writeError() {
    if (Ti.thisProc() == SPECIAL_PROC)
      amr_level[0].writeError("err" + outputFilePrefix + level_steps[0]);
  }


  single private void regrid() {
    // If finest_level == 0, you still want to reuse memory.
    // if (finest_level == 0) return;
    if (Ti.thisProc() == SPECIAL_PROC && printAdvance)
      System.out.println("Regridding after step " + level_steps[0]);

    SharedRegion single oldRegion = amr_level[0].getRegion(); // REGION
	// System.out.println("Amr.regrid():  new SharedRegion, Domain setRegion");
    SharedRegion single newRegion = new SharedRegion(); // REGION
    Domain<1>.setRegion(newRegion); // REGION

    { // scope to prevent Region problems
    RectDomain<1> single levinds = [0 : finest_level - 1];
    RectDomain<1> single levindsnew = levinds + [1];
    Domain<SPACE_DIM> [1d] single tags = new
      (newRegion) // REGION
      Domain<SPACE_DIM>[levinds];

    foreach (lev in levinds) tags[lev] = amr_level[lev].tag();
    checkForPlotError();

    // Doing MeshRefine.refine for levels levinds.
    // Note that you are sending one region for multiple levels.
    RectDomain<SPACE_DIM> [1d][1d] boxes =
      MeshRefine.refine(
			newRegion, // REGION
			this, tags);

    BoxArray single [1d] single baArray = new
      (newRegion) // REGION
      BoxArray single[0 : finest_level];
    baArray[0] = amr_level[0].getBa();
    foreach (lev in levindsnew) baArray[lev] =
      amr_level[lev].assignBoxes(
				 newRegion, // REGION
				 boxes[lev], dProblem[lev]);

    /*
      Reset hierarchy.
      Interpolate data from old hierarchy onto new hierarchy.
    */

    Logger.append("resetting hierarchy");

    /*
      At level 0, patches do not change, but you need to copy in order
      to put them into different regions.

      AmrLevel.regrid() returns old grids at the same level.
    */
    LevelArrayQuantities single UTempCoarse =
      amr_level[0].regrid(
			  newRegion, // REGION
			  baArray[0], BoxArray.Null(), LevelArrayQuantities.Null());

    for (int single lev = 1; lev <= finest_level; lev++) {
      // System.out.println("Regridding level " + lev);
      UTempCoarse =
	amr_level[lev].regrid(
			      newRegion, // REGION
			      baArray[lev], baArray[lev-1], UTempCoarse);
    }

    } // scope to prevent Region problems

	// System.out.println("Amr.regrid():  deleting oldRegion");
    RegionUtil.delete(oldRegion); // REGION

    Logger.append("done resetting hierarchy");
    // System.out.println("Done regridding");
  }


  /**
   * We are at the end of a coarse grid timecycle.
   * Compute the timesteps for the next iteration.
   * Called by coarseTimeStep().
   */
  single public void computeNewDt() {
    int single n_factor = 1;
    double single dt_0 = amr_level[0].estTimeStep();
    for (int single lev = 1; lev <= finest_level; lev++) {
      n_factor *= ref_ratio[lev-1];
      double single dt_scaled =
	(double) n_factor * amr_level[lev].estTimeStep();
      if (dt_scaled < dt_0) dt_0 = dt_scaled;
    }

    if (maxCumtime > 0.0 && cumtime + dt_0 > maxCumtime)
      dt_0 = maxCumtime - cumtime;

    n_factor = 1;
    dt_level[0] = dt_0;
    for (int single lev = 1; lev <= finest_level; lev++) {
      n_factor *= ref_ratio[lev-1];
      dt_level[lev] = dt_0 / (double) n_factor;
    }
  }
}
