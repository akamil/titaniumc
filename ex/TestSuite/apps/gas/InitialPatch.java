interface InitialPatch {

  public static final int SPACE_DIM = Util.SPACE_DIM;

  public void initData(RectDomain<SPACE_DIM> D, Quantities [SPACE_DIM d] U, double dx);
 
}
