// Copyright (C) The United States Department of
// Energy and the University of California.

// The ExecutionLog class.

/******************************************************************************
 CHANGE LOG.

 23 Nov 98:
  Creation. [pike]

******************************************************************************/

import java.io.PrintStream;
import java.io.FileOutputStream;

class ExecutionLog {
  BoxedList_String strings;
  PrintStream local out;
  boolean timestampByDefault;
  long time0;
  int loggingLevel;
  static final int defaultLevel = 1;

  public ExecutionLog()
  {
    init(defaultLevel);
  }

  public ExecutionLog(int ll)
  {
    init(ll);
  }

  private local void init(int ll)
  {
    loggingLevel = ll;
    strings = new BoxedList_String();
    timestampByDefault = true;
    time0 = System.currentTimeMillis();
    append("begin");
  }

  public void rawappend(int ll, String s)
  {
    if (ll <= loggingLevel)
      strings.push(s);
  }

  public void append(String s)
  {
    append(defaultLevel, s);
  }

  public void append(int ll, String s)
  {
    if (timestampByDefault) 
      s = (System.currentTimeMillis() - time0) + ": " + s;
    s = Ti.thisProc() + ": " + s;
    rawappend(ll, s);
  }

  public static void printArray(PrintStream local out, String [1d] a) {
    for (int i = a.domain().min()[1]; i <= a.domain().max()[1]; i++)
      out.println(a[i]);
  }

  public static void printReverseOrder(BoxedList_String s,
				       PrintStream local out) {
    printArray(out, s.toReversedArray());
  }

  public local void dump(BoxedList_String s)
  {
    // System.out.println("Dumping ExecutionLog from proc " + Ti.thisProc());
    printReverseOrder(s, out);
  }

  static single void end(ExecutionLog local e)
  {
    int single n = Ti.numProcs();
    int me = Ti.thisProc();
    boolean die = false;

    if ((broadcast e.loggingLevel from 0) == 0) return;

    e.append("end");

    if (Ti.thisProc() == 0 && e.out == null)
      try {
	e.out = new PrintStream(new FileOutputStream("log"));
      }
      catch (Throwable x) {
	die = true;
      }

    if (broadcast die from 0) return;

    for (int single i = 0; i < n; i++) {
      BoxedList_String s = broadcast e.strings from i;
      if (me == 0)
	e.dump(s);
    }
  }

  single local void end()
  {
    end(this);
  }
}
