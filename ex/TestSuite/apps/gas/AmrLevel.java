import java.io.*;

/**
 * AmrLevel functions both as a container for state data on a level
 * and also manages the advancement of data in time.
 *
 * @version  10 Feb 2000
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public abstract class AmrLevel {

  /*
    constants
  */

  protected static final int single SPACE_DIM = Util.SPACE_DIM;

  /** process used for broadcasts */
  protected static final int single SPECIAL_PROC = 0;

  /*
    fields that are arguments to constructor
  */

  /**
   * region holding objects for grid hierarchy.
   * it is deleted and reset after each regridding.
   */
  protected SharedRegion single myRegion; // REGION

  /** parent Amr object. */
  protected Amr single parent;

  /** height of this AmrLevel. */
  protected int single level;

  /** arrays of boxes for this level and next coarser level. */
  protected BoxArray single ba, baCoarse;

  /** physical domain in coordinates at this level. */
  protected RectDomain<SPACE_DIM> dProblem;

  /** refinement ratio between this level and next coarser level. */
  protected int single nRefineCoarser;

  /** grid spacing. */
  protected double single dx;

  /*
    other fields that are set in constructor
  */

  /** reference to AmrLevel at next coarser level. */
  protected AmrLevel single amrlevel_coarser;

  /** refinement ratio as point. */
  protected Point<SPACE_DIM> single nRefineCoarserP;

  /** are we on the initial timestep? */
  protected boolean single initial_step = true;

  protected double single oldTime = 0.0;
  protected double single newTime = 0.0;

  /** LevelArrayQuantities's of state data at old time and at new time. */
  protected LevelArrayQuantities single UOld, UNew, UInter;

  protected LevelArraydouble single truncErrorCopied, truncErrorCopied2;

  /** FluxRegister for interface with the coarser level. */
  protected FluxRegister fReg;

  /** these are NOT single */
  protected int myProc;
  protected Domain<1> myBoxes;


  /**
   * constructor
   */
  single public AmrLevel(
			 SharedRegion single MyRegion, // REGION
		  Amr single Parent,
		  int single Level,
		  BoxArray single Ba,
		  BoxArray single BaCoarse,
		  RectDomain<SPACE_DIM> DProblem,
		  int single NRefineCoarser,
		  double single Dx) {
    // System.out.println("constructing AmrLevel");
    // myRegion = new SharedRegion();  // gets deleted when regridding
    myRegion = MyRegion; // REGION
    parent = Parent;
    level = Level;
    ba = Ba;  // Ba lives in the region assigned to the next coarser level
    myProc = Ti.thisProc();
    myBoxes = ba.procboxes(myProc);
    baCoarse = BaCoarse;
    nRefineCoarser = NRefineCoarser;
    nRefineCoarserP = Point<SPACE_DIM>.all(nRefineCoarser);
    dx = Dx;
    dProblem = DProblem;

    amrlevel_coarser = getLevel(level-1);

    // Allocate states.
    UOld = new
      (myRegion) // REGION
      LevelArrayQuantities(
			   myRegion, // REGION
			   ba);
    UNew = new
      (myRegion) // REGION
      LevelArrayQuantities(
			   myRegion, // REGION
			   ba);

    // fReg is for interface between this level and next coarser level.
    fReg = ((boolean single) baCoarse.isNull()) ? null : new
      (myRegion) // REGION
      FluxRegister(
		   myRegion, // REGION
		   ba, baCoarse, dProblem, nRefineCoarser);
  }

  /*
    accessor methods
  */

  public boolean single isInitialStep() { return initial_step; }

  single public AmrLevel single getLevel(int single lev)
    { return parent.getLevel(lev); }

  public BoxArray single getBa() { return ba; }

  public LevelArrayQuantities single getUNew() { return UNew; }

  public LevelArrayQuantities single getUOld() { return UOld; }

  public LevelArrayQuantities single getUInter() { return UInter; }

  public FluxRegister getfReg() { return fReg; }

  public double single getOldTime() { return oldTime; }

  public double single getNewTime() { return newTime; }

  public double single getDx() { return dx; }

  public RectDomain<SPACE_DIM> getDproblem() { return dProblem; }

  public SharedRegion single getRegion() { return myRegion; } // REGION

  protected LevelArraydouble single getTruncErrorCopied() { return truncErrorCopied; }

  protected LevelArraydouble single getTruncErrorCopied2() { return truncErrorCopied2; }

  /*
    methods
  */

  single public void setRegion(SharedRegion single newRegion) { myRegion = newRegion; } // REGION

  /**
   * Read in saved plotfile.  This is incomplete.
   */
  abstract single public void getSaved(MyInputStream in);

  single public void initData(InitialPatch initializer) {
    foreach (lba in myBoxes) {
      // initializer.initData(ba[lba], UOld[lba], dx);
      initializer.initData(ba[lba], UNew[lba], dx);
    }
  }

  single public void post_initial_step() { initial_step = false; }

  /**
   * Average solution at this level to get new solution (UNew) at
   * next coarser level, on all coarse cells covered by this level.
   * Requires level > 0.
   * Called by post_timestep().
   */
  single protected void avgDown() {
    LevelArrayQuantities single UC = amrlevel_coarser.getUNew();
    foreach (lba in myBoxes)
      foreach (lbaC in baCoarse.indices()) {
      RectDomain<SPACE_DIM> intersect = (ba[lba] / nRefineCoarserP) * baCoarse[lbaC];
      foreach (cellC in intersect) {
	// UC[lbaC] may live on another process.
	UC[lbaC][cellC] =
	  Quantities.Average(UNew[lba].
			     restrict(Util.subcells(cellC, nRefineCoarser)));
      }
    }
  }


  /**
   * Compute estimated coarsest-level timestep for next iteration
   * -- must be defined in derived class.
   * Called by Amr.computeNewDt().
   *
   * @return timestep
   */
  abstract single public double single estTimeStep();


  /**
   * Operations to be done after a timestep (like refluxing)
   * -- must be defined in derived class.
   * Called by Amr.timeStep().
   */
  abstract single public void post_timestep();


  /**
   * Advance by a timestep at this level.
   * Called by Amr.timeStep().
   *
   * @param time physical time before advancement
   * @param dt physical timestep
   * @return CFL number
   */
  abstract single public double advance(double single time, double single dt);


  /**
   * swap time levels (called by advance())
   */
  single protected void swapTimeLevels(double single dt) {
    oldTime = newTime;
    newTime = oldTime + dt;

    UOld.copy(UNew);
  }


  /**
   * Tag cells at this level.
   */
  abstract single public Domain<SPACE_DIM> tag();


  /**
   * Return a copy of UNew on the original grids.
   */
  abstract single public LevelArrayQuantities single
    regrid(
	   SharedRegion single newRegion, // REGION
	   BoxArray single Ba,
	   BoxArray single BaCoarse,
	   LevelArrayQuantities single UTempCoarse);

  abstract single public BoxArray single
    assignBoxes(
		SharedRegion single assignRegion, // REGION
		RectDomain<SPACE_DIM> [1d] boxes,
		RectDomain<SPACE_DIM> dProblemLevel);

  abstract single public void
    initGrids(InitialPatch initializer);

  abstract public void write(String outputfile);

  abstract protected void writeOn(PrintStream out,
  			  Quantities [SPACE_DIM d] gridData,
  			  int height);

  abstract public void writeError(String outputfile);

  abstract protected void writeErrorOn(PrintStream out,
  			  double [SPACE_DIM d] gridData,
  			  int height);
}
