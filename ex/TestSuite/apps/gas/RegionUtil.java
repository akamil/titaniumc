public class RegionUtil {

  public static void delete(PrivateRegion r) {
    // System.out.println("Going to delete PrivateRegion");
    if (r != null) {
      try {
        r.delete();
      } catch (RegionInUse oops) {
        System.err.println("BUG: freed PrivateRegion that is in use.");
        System.exit(2);
      }
    } else {
      System.out.println("Attempting to delete null PrivateRegion");
    }
  }

  public static sglobal void delete(SharedRegion single r) {
    // System.out.println("Going to delete SharedRegion");
    Ti.barrier();
    if (r != null) {
      try {
        r.delete();
      } catch (RegionInUse oops) {
        System.err.println("BUG: freed SharedRegion that is in use.");
        System.exit(2);
      }
    } else {
      System.out.println("Attempting to delete null SharedRegion");
    }
  }
}
