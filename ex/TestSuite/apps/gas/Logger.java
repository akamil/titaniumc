public class Logger {

  static final String bb = "begin barrier";
  static final String eb = "end barrier";

  static boolean single logging = false;  // are you using the logger?

  static ExecutionLog local log;

  static int barrierCount;

  public static sglobal void barrier() { 
    if (logging) {
      barrierCount++;
      log.append(bb);
      Ti.barrier();
      log.append(eb);
    } else {
      Ti.barrier();
    }
  }

  single public static void begin() {
    log = new ExecutionLog();
    barrierCount = 0;
    logging = true;
  }

  single public static void end() {
    if (logging) {
      ExecutionLog.end(log);
    }
  }

  public static void append(String s) {
    if (logging) {
      log.append(s);
    }
  }
}
