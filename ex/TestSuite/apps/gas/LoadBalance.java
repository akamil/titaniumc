/**
 * int [1d] proclist = LoadBalance.balance(int [1d] load, int numProcs)
 *
 * Given task loads and the number of processes, assign tasks to processes.
 *
 * Input
 * int [1d] load:  loads of some tasks.
 * int numProcs:  number of processes to which load must be distributed.
 *
 * Returns:
 * int [1d] proclist:  same domain as load, range [0:numProcs-1];
 * proclist[i] gives the ID number of the process to which the i'th
 * task is assigned.
 * 
 * This algorithm comes from
 * William Y. Crutchfield, ``Load Balancing Irregular Algorithms''
 *
 * @version  15 Feb 2000
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public class LoadBalance {

  public static int [1d] balance(int [1d] load, int numProcs) {

    RectDomain<1> procTeam = [0 : numProcs - 1];

    RectDomain<1> taskindices = load.domain();
    // proclist is what is returned
    int [1d] proclist = new int[taskindices];
    if (taskindices.size() == 0) {
      System.out.println("LoadBalance:  no tasks");
      return proclist;
    }

    Domain<1> [1d] procTasks = new Domain<1>[procTeam];

    // procLoad[proc] is total load assigned to process proc.
    int [1d] procLoad = new int[procTeam];
    foreach (proc in procTeam) procLoad[proc] = 0;

    // foreach (task in taskindices)
    //   System.out.println("Task " + task + " has load " + load[task]);

    /*
      Figure out how to allocate to processes.
    */

    Domain<1> unassigned = taskindices;
    // procTasks[proc] containing ind
    // means that tasks[ind] is assigned to process proc.
    foreach (proc in procTeam) procTasks[proc] = [0 : -1];

    // Find heaviest task and assign it to the lightest process.
	// System.out.println("find heaviest task");
    while (! unassigned.isNull()) {
      // find heaviest task among those that are as yet unassigned
      int maxloadTask = 0;
      Point<1> heaviestTask;
      foreach (taskp in unassigned) {
	if (load[taskp] > maxloadTask) {
	  heaviestTask = taskp; maxloadTask = load[taskp];
	}
      }

      if (maxloadTask == 0) {
	System.out.println("LoadBalance:  no task with positive load");
	System.exit(0);
      }

      // find lightest process
      int lightestProc = 0;
      foreach (proc in procTeam)
	if (procLoad[proc] < procLoad[lightestProc]) lightestProc = proc[1];

      // assign heaviest task to lightest process
      Domain<1> heaviestTaskD = [heaviestTask : heaviestTask];
      procTasks[lightestProc] = procTasks[lightestProc] + heaviestTaskD;
      procLoad[lightestProc] += load[heaviestTask];
      unassigned = unassigned - heaviestTaskD;
    }

    // System.out.println("now finding heaviest");
      
    boolean unbalanced = true;
  findswaps:
    while (unbalanced) {
      unbalanced = false;

      // procTasksPrint(procTasks);

      // find heaviest process
      Point<1> heaviestProc = [0];
      foreach (proc in procTeam)
	if (procLoad[proc] > procLoad[heaviestProc]) heaviestProc = proc;
	
      // for each task in heaviest process
      foreach (taskph in procTasks[heaviestProc]) {
	int wth = load[taskph];
	  
	// for each task NOT in heaviest process
	// foreach (otherProc in procTeam - [heaviestProc:heaviestProc]) {
	foreach (otherProc in procTeam) if (otherProc != heaviestProc) {
	  Domain<1> procTasksOther = procTasks[otherProc];
	  foreach (taskpo in procTasksOther) {
	      
	    int wto = load[taskpo];  // weight of the other task
	    int loadhnew = procLoad[heaviestProc] - wth + wto;
	    int loadonew = procLoad[otherProc] - wto + wth;
	      
	    // if interchanging taskph and taskpo improves load balance
	    // (i.e., difference in process loads goes down)
	    if (Math.abs(loadhnew - loadonew) <
		procLoad[heaviestProc] - procLoad[otherProc]) {
		
	      // System.out.println("interchanging " + taskph[1] +
	      // " in [" + heaviestProc + "] with " +
	      //		 taskpo[1] + " in " + otherProc);
		
	      // perform interchange
	      procTasks[heaviestProc] =
		procTasks[heaviestProc] - [taskph:taskph] + [taskpo:taskpo];
	      procLoad[heaviestProc] = loadhnew;
	      procTasks[otherProc] =
		procTasks[otherProc] - [taskpo:taskpo] + [taskph:taskph];
	      procLoad[otherProc] = loadonew;
	      // find heaviest process again...
	      unbalanced = true;
	      continue findswaps;
	    }
	  }
	}
      }
    } // while (unbalanced)

    /*
      If input contains a line -balance.print,
      then print out loads.
    */

    ParmParse pp = new ParmParse("balance");
    if (pp.contains("print")) {
      // print out loads
      String s = "Loads:";
      for (int proc = 0; proc < numProcs; proc++) {
	int numTasks = procTasks[proc].size();
	s = s + "  " + procLoad[proc] +
	  " (" + numTasks + " task" + ((numTasks == 1) ? "" : "s") + ")";
      }
      System.out.println(s);
    }

    /*
      Now assign tasks to processes.
    */
      
    // RectDomain<1> taskindices = tasks.domain();
    foreach (proc in procTeam)
      foreach (indtask in procTasks[proc]) {
      proclist[indtask] = proc[1];
      // System.out.println("Task " + indtask + " assigned to " + proc[1]);
    }
    return proclist;
  }
}
