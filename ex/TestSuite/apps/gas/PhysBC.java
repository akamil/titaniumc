interface PhysBC {

  public static final int SPACE_DIM = Util.SPACE_DIM;

  /**
   * Calculate flux on physical boundary.
   * @param q (cell) primitive variables
   * @param coeffs (edge) artificial viscosity coefficients
   * @param sdim signed dimension
   * @param flux (edge) flux, output
   */
  void f(Quantities [SPACE_DIM d] q,
	 double [SPACE_DIM d] coeffs,
	 int sdim,
	 Quantities [SPACE_DIM d] flux,
         double dx);

  void slope(Quantities [SPACE_DIM d] q,
	     Domain<SPACE_DIM> D,
	     int sdim,
	     Quantities [SPACE_DIM d] dq,
             double dx);
}
