public class LevelArrayQuantities {
  private static final int single SPACE_DIM = Util.SPACE_DIM;

  private BoxArray single ba;
  private Quantities [1d] single [SPACE_DIM d] arrs;

  private static final LevelArrayQuantities single LevelArrayNull = new 
    LevelArrayQuantities(
			 null, // REGION
			 (BoxArray single) BoxArray.Null());

  /**
   * constructor
   */
  single public LevelArrayQuantities(
				 SharedRegion single reg, // REGION
				 BoxArray single Ba) {
    // set fields
    ba = Ba;
    RectDomain<1> single indices = (RectDomain<1> single) ba.indices();
    arrs = new
      (reg) // REGION
      Quantities[indices][SPACE_DIM d];

    // process number stuff
    int myProc = Ti.thisProc();
    int single numProcs = Ti.numProcs();
    RectDomain<1> single Dprocs = [0 : numProcs-1];

    {
      // set up arrays of references to grids, and exchange references
      Quantities [1d][SPACE_DIM d] arrlistloc = new
	(reg) // REGION
	Quantities[indices][SPACE_DIM d];

      Quantities [1d] single [1d][SPACE_DIM d] arrlist = new
	(reg) // REGION
	Quantities[Dprocs][1d][SPACE_DIM d];

      arrlist.exchange(arrlistloc);

      // define new grids living on process myProc
      foreach (ibox in ba.procboxes(myProc))
	arrlist[myProc][ibox] = new
	(reg) // REGION
	Quantities[ba[ibox]];

      Ti.barrier();

      // get references from the appropriate processes
      foreach (ibox in indices) arrs[ibox] = arrlist[ba.proc(ibox)][ibox];

      Ti.barrier();
    }
  }

  // methods

  public BoxArray single boxArray() { return ba; }

  public Quantities [SPACE_DIM d] op[] (Point<1> p) { return arrs[p]; }

  public Quantities [SPACE_DIM d] op[] (int ip) { return arrs[ip]; }

  public void op[]= (Point<1> p, Quantities [SPACE_DIM d] myarr) { arrs[p] = myarr; }

  public void op[]= (int ip, Quantities [SPACE_DIM d] myarr) { arrs[ip] = myarr; }

  public RectDomain<1> domain() { return ba.indices(); }

  // clone method:  replace old with new as UOld = UNew.clone();
  // copy method:  replace old data with UOld.copy(UNew);

  single public void copy(LevelArrayQuantities U) {
    foreach (lba in ba.procboxes(Ti.thisProc())) arrs[lba].copy(U[lba]);
  }

  single public void checkNaN(String str) {
    foreach (lba in ba.procboxes(Ti.thisProc()))
      foreach (p in arrs[lba].domain())
      arrs[lba][p].checkNaN(str + "" + lba + "" + p);
  }

  public static LevelArrayQuantities single Null() { return LevelArrayNull; }
}
