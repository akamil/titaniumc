class List_RectDomain {
  private static final int SPACE_DIM = Util.SPACE_DIM;

  public RectDomain<SPACE_DIM> car;
  public List_RectDomain cdr;

  /**
   * Main constructor.
   */
  public List_RectDomain(RectDomain<SPACE_DIM> x, List_RectDomain rest) {
    car = x;
    cdr = rest;
  }

  public List_RectDomain cons(
			      Region reg, // REGION
			      RectDomain<SPACE_DIM> x) {
    return new
      (reg) // REGION
      List_RectDomain(x, this);
  }        

  public /*inline*/ RectDomain<SPACE_DIM> first() {
    return car;
  }

  public /*inline*/ List_RectDomain rest() {
    return cdr;
  }

  public int length() {
    int i = 0;
    List_RectDomain r = this;
    while (r != null) {
      i++;
      r = r.rest();
    }
    return i;
  }
}
