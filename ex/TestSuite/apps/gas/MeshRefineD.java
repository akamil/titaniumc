/**
 * For internal use by MeshRefine.
 * This class contains only uniprocessor code.
 *
 * MeshRefineD mrd = new MeshRefineD(Region myRegion,
 *                                   RectDomain<SPACE_DIM> [1d][1d] oldGrids,
 *                                   Domain<SPACE_DIM> [1d] tagged,
 *                                   RectDomain<SPACE_DIM> DProblemBase,
 *                                   int [1d] Ref_Ratio);
 *
 * RectDomain<SPACE_DIM> [1d][1d] newgrids = mrd.refine();
 *
 * @version  13 Dec 1999
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public class MeshRefineD {

  /*
    constants
  */

  private static final int SPACE_DIM = Util.SPACE_DIM;

  /*
    non-constant fields
  */

  private int base, top;
  private Domain<SPACE_DIM> [1d] ProperNestingDomain; // [base:top]
  private RectDomain<SPACE_DIM> [1d] dProblem;  // [base:top]
  private int [1d] ref_ratio;           // [base:top]
  private Domain<SPACE_DIM> [1d] tagged;        // [base:top]

  private Region myRegion; // REGION

  /**
   * Constructor:
   * Given grids at levels base:top
   * and domains containing tagged cells at levels base:top.
   */
  public MeshRefineD(
		     Region MyRegion, // REGION
		     RectDomain<SPACE_DIM> [1d][1d] oldGrids,   // [base:top][1d]
		     Domain<SPACE_DIM> [1d] Tagged,             // [base:top]
		     RectDomain<SPACE_DIM> DProblemBase,        // at base
		     int [1d] Ref_Ratio) {              // [base:top]
    myRegion = MyRegion; // REGION
    RectDomain<1> levels = oldGrids.domain();
    base = levels.min()[1]; top = levels.max()[1];
    ref_ratio = Ref_Ratio;
    tagged = Tagged;

    getProblemDomains(DProblemBase);

    // (1) Compute proper nesting domains for levels base:top
    getPNDs(oldGrids);

    // (2) Turn off tags at points outside PND.
    clipTagged(tagged);
  }


  /**
   * Return arrays of disjoint rectangles indexed by [base+1:top+1]
   * such that the union of the disjoint rectangles indexed by [level+1]
   * covers the domains at level [level].
   */
  public RectDomain<SPACE_DIM> [1d][1d] refine() {
    // System.out.println("MeshRefineD.refine()");

    RectDomain<SPACE_DIM> [1d][1d] returnGrids = new
      (myRegion) // REGION
      RectDomain<SPACE_DIM>[base+1:top+1][1d];

    for (int h = top; h >= base; h--) {

      if (tagged[h].isNull()) {
	returnGrids[h+1] = null;
	break;
      }

      // (3) Find grids covering tagged points in patches at level h.

      // System.out.println("Getting GridGenerator for " + h);
      // System.out.println("Calling GridGenerator with " + tagged[h]);
      GridGenerator gg = new
	(myRegion) // REGION
	GridGenerator(
		      myRegion, // REGION
		      tagged[h], ProperNestingDomain[h]);
      // System.out.println("makeGrids");
      RectDomain<SPACE_DIM> [1d] newGrids = gg.makeGrids();

      // (4) Refine grids and store in returnGrids[h+1].
      
      // System.out.println("refineGrids for "+ h);
      Util.refine(newGrids, ref_ratio[h]);
      returnGrids[h+1] = new
	(myRegion) // REGION
	RectDomain<SPACE_DIM>[newGrids.domain()];
      foreach (indgrid in newGrids.domain())
	returnGrids[h+1][indgrid] = newGrids[indgrid];

      // (5) Expand grids by one cell in each direction (clipping at
      // physical boundaries) and then coarsen and union with tags
      // at level h-1.
      // Omit this step for the base level.

      if (h > base) {
	Util.coarsen(newGrids, ref_ratio[h]);  // back to level h

	// Extend each grid one cell in each direction.
	foreach (indgrid in newGrids.domain())
	  foreach (pdim in Util.DIMENSIONS) foreach (pdir in Util.DIRECTIONS) {
	  Point<SPACE_DIM> shift = Point<SPACE_DIM>.direction(pdim[1] * pdir[1], 1);
	  newGrids[indgrid] = (RectDomain<SPACE_DIM>)
	    (newGrids[indgrid] +
	     ((newGrids[indgrid] + shift) * dProblem[h]));
	}

	Util.coarsen(newGrids, ref_ratio[h-1]);  // to level h-1

	foreach (indgrid in newGrids.domain())
	  tagged[h-1] = tagged[h-1] + newGrids[indgrid];
      } // if (h > base)
    } // for (int h = top; h >= base; h--)

    // This method is called only once.
    // After it's called, all you retain is returnGrids.

    return returnGrids;
  }


  /**
   * Find physical domain for each level.
   */
  private void getProblemDomains(RectDomain<SPACE_DIM> DProblemBase) {
    dProblem = new
      (myRegion) // REGION
      RectDomain<SPACE_DIM>[base:top];
    Point<SPACE_DIM> extent = DProblemBase.max() + Util.ONE;
    for (int h = base; h <= top; h++) {
      dProblem[h] = [Util.ZERO : extent - Util.ONE];
      extent = extent * Point<SPACE_DIM>.all(ref_ratio[h]);
    }
  }



  /**
   * Compute proper nesting domains.
   *
   * From Chombo:
   * The proper nesting domain (PND) of level h+1 is defined as the
   * interior of the PND for level h (the next coarser) except at the
   * boundaries of the problem domain (ie, the level 0 box), in which case
   * the PNDs of levels h and h+1 both contain the boundary.  The PND of
   * the base level is defined as the interior of the union of all the grid
   * boxes in the base level.  Given ProperNestingDomain[h], this function
   * computes ProperNestingDomain[h+1] by iterating over the coordinate
   * directions and computing the cells to remove from the proper nesting
   * domain due to the boundaries in that direction.  For each direction
   * the domain is shifted (first up, then down) and only the part that is
   * shifted _out_ of the PND but remains _inside_ the problem boundary is
   * kept and then shifted back into the PND and subtracted (removed) from
   * it.  So if the shift moves part of the PND outside the problem
   * boundary then no cells will be removed.  If the cells that get shifted
   * out of the PND remain inside the boundary then they will be removed
   * from the PND when they get shifted back.  When all the directions have
   * been handled, the PND is refined to make it correct for level h+1.
   * The PND of the base level is the union of the mesh boxes in the base
   * level and serves as the starting point.  In set notation the operation
   * is:
   *
   *       D - ( ( ( (D << d) - D ) * B ) >> d )
   *
   * where:
   *  d is the current direction (+i,-i,+j,etc)
   *  B is the boundary of the problem domain at the current level
   *  - is the subtraction (removal) operator
   *  * is the intersection operator
   * << is the down-shift operator (shift in negative direction)
   * >> is the up-shift operator (shift in positive direction)
   * 
   * In the following graphic, the PND for two mesh boxes with BufferSize=1
   * is shown.
   *
   *      ###################################################
   *      #     |   |   |   |   |   |                       #
   *      #     |   | o | o | o |   |                       #
   *      #     |   |   |   |   |   |                       #
   *      #     +---+---+---+---+---%---+---+---+---+---+---#
   *      #     |   |   |   |   |   %   |   |   |   |   |   #
   *      #     |   | o | o | o |   %   |   |   |   |   |   #
   *      #     |   |   |   |   |   %   |   |   |   |   |   #
   *      #     +---+---+---+---+---%---+---+---+---+---+---#
   *      #     |   |   |   |   |   %   |   |   |   |   |   #
   *      #     |   | o | o | o | o % o | o | o | o | o | o #
   *      #     |   |   |   |   |   %   |   |   |   |   |   #
   *      #     +---+---+---+---+---%---+---+---+---+---+---#
   *      #     |   |   |   |   |   %   |   |   |   |   |   #
   *      #     |   |   |   |   |   %   | o | o | o | o | o #
   *      #     |   |   |   |   |   %   |   |   |   |   |   #
   *      #     +---+---+---+---+---%---+---+---+---+---+---#
   *      #                         |   |   |   |   |   |   #
   *      #                         |   | o | o | o | o | o #
   *      #                         |   |   |   |   |   |   #
   *      ###################################################
   * 
   *  where:
   *   #    are physical boundaries
   *   | +  are cell boundaries of the two mesh boxes
   *   %    are box boundaries shared between boxes
   *   o    are cells in the proper nesting domain
   *
   */
  private void getPNDs(RectDomain<SPACE_DIM> [1d][1d] oldGrids) {
    ProperNestingDomain = new
      (myRegion) // REGION
      Domain<SPACE_DIM>[oldGrids.domain()];

    foreach (h in oldGrids.domain()) {

      // First find the domain covered by grids at level h.

      // System.out.println("getPND for " + h[1]);
      Domain<SPACE_DIM> levDomain = Util.EMPTY;
      foreach (indgrid in oldGrids[h].domain())
        levDomain = levDomain + oldGrids[h][indgrid];

      Domain<SPACE_DIM> PND = levDomain;
      foreach (pdim in Util.DIMENSIONS) foreach (pdir in Util.DIRECTIONS) {
        // extend one cell in each direction
        Point<SPACE_DIM> shift = Point<SPACE_DIM>.direction(pdim[1] * pdir[1], 1);

	Domain<SPACE_DIM> remove = (((PND + shift) - PND) * dProblem[h]) - shift;
	// System.out.println("removing " + remove);
        PND = PND - remove;
      }
    
      ProperNestingDomain[h] = PND;
    }
  }


  /**
   * At each level, remove points outside proper nesting domain
   * from the tagged domain.
   */
  private void clipTagged(Domain<SPACE_DIM> [1d] tagged) {
    foreach (h in tagged.domain()) {
      // System.out.println("Before clipping, tagged" + h + " = " + tagged[h]);
      Domain<SPACE_DIM> tagged_before = tagged[h];
      tagged[h] = tagged[h] * ProperNestingDomain[h];
      // System.out.println("After  clipping, tagged" + h + " = " + tagged[h]);
      // System.out.println("Clipped out " + (tagged_before - tagged[h]));
    }
  }
}
