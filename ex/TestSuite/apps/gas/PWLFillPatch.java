/**
 * PieceWise Linear Fill Patch.
 * Lives on LevelGodunov.
 *
 * @version  9 Aug 2000
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public class PWLFillPatch {

  /*
    constants
  */

  private static final int SPACE_DIM = Util.SPACE_DIM;

  /** array of boxes at this level and at the next coarser level */
  private BoxArray ba, baCoarse;

  /** physical domain */
  private RectDomain<SPACE_DIM> dProblem;

  private int nRefine, innerRadius, outerRadius;

  private Point<SPACE_DIM> nRefineP;

  private double dRefine;

  private Domain<1> [1d] adjacentBoxes, adjacentCoarseBoxes, coarserBoxes;

  /** box within inner radius (isotropically) */
  private RectDomain<SPACE_DIM> [1d] BInner;

  /** box within outer radius (isotropically) */
  private RectDomain<SPACE_DIM> [1d] BOuter;

  private SharedRegion single myRegion; // REGION

  /**
   * constructor
   */
  public PWLFillPatch(
		      SharedRegion single MyRegion, // REGION
		      BoxArray Ba,
		      BoxArray BaCoarse,
		      RectDomain<SPACE_DIM> DProblem,
		      int NRefine,
		      int InnerRadius,
		      int OuterRadius) {
    myRegion = MyRegion; // REGION
    ba = Ba;
    baCoarse = BaCoarse;
    dProblem = DProblem;
    nRefine = NRefine;
    nRefineP = Point<SPACE_DIM>.all(nRefine);
    dRefine = (double) nRefine;
    innerRadius = InnerRadius;
    outerRadius = OuterRadius;

    RectDomain<1> boxInds = ba.indices();
    RectDomain<1> boxCInds = baCoarse.indices();

    /*
      Calculate BInner, BOuter.
    */
    BInner = new
      (myRegion) // REGION
      RectDomain<SPACE_DIM>[boxInds];
    BOuter = new
      (myRegion) // REGION
      RectDomain<SPACE_DIM>[boxInds];
    foreach (ind in boxInds) {
      BInner[ind] = ba[ind].accrete(innerRadius) * dProblem;
      BOuter[ind] = BInner[ind].accrete(outerRadius) * dProblem;
    }

    /*
      adjacentBoxes:  set of other boxes within outer radius.
    */
    adjacentBoxes = new
      (myRegion)  // REGION
      Domain<1>[boxInds];
    foreach (ind in boxInds) {
      adjacentBoxes[ind] = [0:-1];
      // foreach (indOther in boxInds - [ind:ind])
      foreach (indOther in boxInds)
	if (indOther != ind)
	  if (! (ba[indOther] * BOuter[ind]).isNull())
	    adjacentBoxes[ind] = adjacentBoxes[ind] + [indOther:indOther];
    }

    /*
      adjacentCoarseBoxes:  set of other coarse boxes touching coarse boxes.
    */
    adjacentCoarseBoxes = new
      (myRegion) // REGION
      Domain<1>[boxCInds];
    foreach (ind in boxCInds) {
      adjacentCoarseBoxes[ind] = [0:-1];
      RectDomain<SPACE_DIM> accreted = baCoarse[ind].accrete(1);
      // foreach (indOther in boxCInds - [ind:ind])
      foreach (indOther in boxCInds)
	if (indOther != ind)
	  if (! (baCoarse[indOther] * accreted).isNull())
	    adjacentCoarseBoxes[ind] =
	      adjacentCoarseBoxes[ind] + [indOther:indOther];
    }

    /*
      coarserBoxes:  set of coarser boxes within inner radius.
    */
    coarserBoxes = new
      (myRegion) // REGION
      Domain<1>[boxInds];
    foreach (ind in boxInds) {
      // String slist = "coarserBoxes of " + ba[ind] + ":";
      coarserBoxes[ind] = [0:-1];
      foreach (indC in boxCInds)
	if (! (baCoarse[indC] * (BInner[ind] / nRefineP)).isNull()) {
	  coarserBoxes[ind] = coarserBoxes[ind] + [indC:indC];
	  // slist = slist + " " + baCoarse[indC];
	}
      // System.out.println(slist);
    }
  }


  // methods

  public RectDomain<SPACE_DIM> innerBox(Point<1> PatchIndex) { 
    return BInner[PatchIndex];
  }


  /**
   * Fill in UPatch, by using patches from U as much as possible,
   * and also interpolating from patches at next coarser level
   * within the inner radius.
   *
   * @param U (cell) patches at this level
   * @param UCOld (cell) patches at next coarser level at old time
   * @param UCNew (cell) patches at next coarser level at new time
   * @param UPatch (cell) patch to be filled
   * @param PatchIndex index in U of the core of UPatch
   */
  public void fill(LevelArrayQuantities U, 
		   LevelArrayQuantities UCOld, 
		   LevelArrayQuantities UCNew, 
		   Quantities [SPACE_DIM d] UPatch,  // Patch to be filled
		   Point<1> PatchIndex,
		   double Time, 
		   double TCOld, 
		   double TCNew) {

    RectDomain<SPACE_DIM> baNew = UPatch.domain();
    Domain<SPACE_DIM> unfilled = baNew;

    /*
      Fill core by copying U[PatchIndex].
    */

    UPatch.copy(U[PatchIndex]);
    unfilled = unfilled - ba[PatchIndex];

    /*
      Fill within _outer_ radius by copying from adjacent boxes,
      as much as possible.
    */

    foreach (indadj in adjacentBoxes[PatchIndex]) {
      if (! (baNew * ba[indadj]).isNull()) {
	// this may belong to another process
	Quantities [SPACE_DIM d] UAdj = U[indadj];
	UPatch.copy(UAdj);
	unfilled = unfilled - ba[indadj];
      }
    }

    // Sometimes get all.
    if (unfilled.isNull()) return;

    /*
      Fill all unfilled cells within _inner_ radius by interpolating
      from coarser data.
    */

    // System.out.println("PWLFillPatch.fill(" + baNew + "):  Must fill in " + unfilled);

    Domain<SPACE_DIM> unfilledC = (unfilled * BInner[PatchIndex]) / nRefineP;
    // BInner[PatchIndex] is contained in baNew
    RectDomain<SPACE_DIM> BInnerC = BInner[PatchIndex] / nRefineP;
    Quantities [SPACE_DIM d] UC = new
      (myRegion) // REGION
      Quantities[BInnerC];

    double wtNew = (TCNew == TCOld) ? 1.0 : (Time - TCOld) / (TCNew - TCOld);
    double wtOld = 1.0 - wtNew;
    // System.out.println("wtOld = " + wtOld + " and wtNew = " + wtNew);

    foreach (indc in coarserBoxes[PatchIndex]) {

      // UCOld[indc] and UCNew[indc] may belong to other processes

      foreach (cellC in unfilledC * baCoarse[indc]) {
	/*
	System.out.println("Filling " + cellC + " in " + baCoarse[indc]);
	System.out.println("UCOld" + indc + " domain = " + UCOld[indc].domain());
	System.out.println("UCNew" + indc + " domain = " + UCNew[indc].domain());
	System.out.println("UC domain = " + UC.domain());
	*/
	try {
	  UC[cellC] = UCOld[indc][cellC] * wtOld + UCNew[indc][cellC] * wtNew;
	
	  // Find slopes of UC in each direction at each coarse cell.

	  Quantities [1d] UCslopes;
	  try {
	    UCslopes =
	      coarseSlopes(UCOld, UCNew, indc, cellC, wtOld, wtNew);
	  } catch (Exception x) {
	    System.out.println("Error in PWLFillPatch.coarseSlopes()");
	    System.exit(0);
	  }
	  foreach (cell in unfilled * Util.subcells(cellC, nRefine)) {
	    // Fill in cell.
	    try {
	      UPatch[cell] = UC[cellC];
	      foreach (pdim in Util.DIMENSIONS) {
		int dim = pdim[1];
		UPatch[cell] = UPatch[cell] + UCslopes[pdim] *
		  (((double) cell[dim] + 0.5) / dRefine -
		   ((double) cellC[dim] + 0.5));
	      }
	    } catch (Exception xx) {
	      System.out.println("Error in filling in " + cell);
	      System.exit(0);
	    }
	  }
	} catch (Exception x) {
	  System.out.println("Error in getting UC" + cellC);
	  System.exit(0);
	}
      }
    }

    // There may still be unfilled cells within _outer_ radius.
    // These will be filled in with extend().
  }


  private final Quantities [1d] coarseSlopes(LevelArrayQuantities UCOld,
					     LevelArrayQuantities UCNew,
					     Point<1> indc,
					     Point<SPACE_DIM> cellC,
					     double wtOld,
					     double wtNew) {

    // From box indc of UCOld (weight wtOld) and UCNew (weight wtNew)
    // get slopes in cell cellC.
    Quantities [1d] UCslopes = new
      (myRegion) // REGION
      Quantities[Util.DIMENSIONS];
    Quantities [1d] UCadj = new
      (myRegion)  // REGION
      Quantities[Util.DIRECTIONS];
    boolean [1d] foundAdjC = new
      (myRegion)  // REGION
      boolean[Util.DIRECTIONS];

    // Check all directions for neighbors.

    Quantities UCme = UCOld[indc][cellC] * wtOld + UCNew[indc][cellC] * wtNew;

    foreach (pdim in Util.DIMENSIONS) {
      foreach (pdir in Util.DIRECTIONS) { // pdir in {-1, 1}
        Point<SPACE_DIM> adjC = cellC + Point<SPACE_DIM>.direction(pdir[1] * pdim[1]);

	Point<1> indAdj;

	foundAdjC[pdir] = false;
	// ERROR:  not baCoarse but UCNew.boxArray()
	if (baCoarse[indc].contains(adjC)) {
	  foundAdjC[pdir] = true;
	  indAdj = indc;
	} else {
          // Look in adjacent boxes.
	  foreach (indAdjSearch in adjacentCoarseBoxes[indc])
	    // ERROR:  not baCoarse but UCNew.boxArray()
	    // You should have retained adjacentCoarseBoxes of it...
	    if (baCoarse[indAdjSearch].contains(adjC)) {
	      foundAdjC[pdir] = true;
	      indAdj = indAdjSearch;
	      break;
	    }
	}

	if (foundAdjC[pdir])
	  UCadj[pdir] = UCOld[indAdj][adjC]*wtOld + UCNew[indAdj][adjC]*wtNew;

      } // end pdir

      if (!foundAdjC[-1] && !foundAdjC[+1]) {
        // This shouldn't happen.
	System.out.println("Error:  Couldn't find either neighbor of " +
			   cellC + " in dimension " + pdim[1]);
        UCslopes[pdim] = Quantities.zero();
      } else if (foundAdjC[-1] && !foundAdjC[+1]) {
        UCslopes[pdim] = UCme - UCadj[-1];
      } else if (!foundAdjC[-1] && foundAdjC[+1]) {
        UCslopes[pdim] = UCadj[+1] - UCme;
      } else {
        UCslopes[pdim] = Quantities.vanLeer(UCadj[-1], UCme, UCadj[+1]);
      }
    } // end pdim
    return UCslopes;
  }


  /**
   * Fill unfilled cells of UPatch by copying from BInner[PatchIndex].
   */
  public void extend(Quantities [SPACE_DIM d] UPatch,
		     Point<1> PatchIndex,
		     int Dim,
		     int FlapRadius) {
    Domain<SPACE_DIM> unfilled = UPatch.domain() - BInner[PatchIndex];

    foreach (indadj in adjacentBoxes[PatchIndex])
      unfilled = unfilled - ba[indadj];

    RectDomain<SPACE_DIM> flapBase =
      [Point<SPACE_DIM>.direction(Dim, 1) : Point<SPACE_DIM>.direction(Dim, FlapRadius)];
    // = [[1 : FlapRadius], 0] or [0, [1 : FlapRadius]]

    foreach (pdir in Util.DIRECTIONS) {
      int sDim = pdir[1] * Dim;
      RectDomain<SPACE_DIM> flap = flapBase * Point<SPACE_DIM>.direction(sDim, 1);
      // = [[1 : FlapRadius], 0] or [0, [1 : FlapRadius]]
      // or [[-FlapRadius : -1], 0] or [0, [-FlapRadius : -1]]
      foreach (borderCell in BInner[PatchIndex].border(1, sDim, 0))
	foreach (cell in unfilled * (flap + borderCell))
	UPatch[cell] = UPatch[borderCell];
    }
  }


  /**
   * Fill UNew by copying from UTemp and interpolating from UTempCoarse.
   *
   * @param UNew (cell) new patches at this level, to be filled
   * @param UTemp (cell) old patches at this level
   * @param UTempCoarse (cell) old patches at next coarser level
   */
  public void
    regrid(LevelArrayQuantities UNew,
	   LevelArrayQuantities UTemp,
	   LevelArrayQuantities UTempCoarse) {
    BoxArray baNew = UNew.boxArray();
    Domain<1> myBoxes = baNew.procboxes(Ti.thisProc());
    foreach (lba in myBoxes) {
      /*
	Fill UNew[lba].
      */
      Domain<SPACE_DIM> unfilledC = baNew[lba] / nRefineP;
      /*
	First try to copy from UTemp.
      */
      foreach (lbaTemp in ba.indices()) {
	RectDomain<SPACE_DIM> intersect = baNew[lba] * ba[lbaTemp];
	if (! intersect.isNull()) {
	  UNew[lba].copy(UTemp[lbaTemp]);
	  unfilledC = unfilledC - (intersect / nRefineP);
	}
      }
      // System.out.println("In " + baNew[lba] + " still unfilled:  refined " +
      // unfilledC);
      /*
	Now fill in the rest by interpolation from UTempCoarse.
      */
      foreach (cellC in unfilledC) {
	// Find the box in baCoarse that contains cellC.
	Domain<1> lbaTC = baCoarse.findCell(cellC);
	if (lbaTC.size() == 0) {
	  System.out.println("Could not find " + cellC + " in baCoarse");
	} else if (lbaTC.size() > 1) {
	  System.out.println("More than one box in baCoarse had " + cellC);
	} else {
	  Point<1> indbox = lbaTC.min();
	  // Find slopes of UC in each direction at each coarse cell.
	  Quantities [1d] UCslopes;
	  // System.out.println("Getting slopes for " + cellC);
	  try {
	    UCslopes = // you actually want the old PWLFillPatch for this...
	      coarseSlopes(UTempCoarse, UTempCoarse,
			   indbox, cellC, 1.0, 0.0);
	  } catch (Exception x) {
	    System.out.println("Error in PWLFillPatch.coarseSlopes");
	    System.exit(0);
	  }
	  // System.out.println("Filling in subcells of " + cellC);
	  foreach (cell in Util.subcells(cellC, nRefine)) {
	    // Fill in cell.
	    try {
	      UNew[lba][cell] = UTempCoarse[indbox][cellC];
	      foreach (pdim in Util.DIMENSIONS) {
		int dim = pdim[1];
		UNew[lba][cell] = UNew[lba][cell] + UCslopes[pdim] *
		  (((double) cell[dim] + 0.5) / dRefine -
		   ((double) cellC[dim] + 0.5));
	      }
	    } catch (Exception xx) {
	      System.out.println("Error in filling in " + cell);
	      System.exit(0);
	    }
	  }
	} // else:  lbaTC.size() == 1
      } // foreach (cellC in unfilledC)
    } // foreach (lba in myBoxes)
  }
}
