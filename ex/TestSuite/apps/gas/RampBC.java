/**
 * Ramp problem:  extends Ramp
 *
 * Left side:  flux[e] = q1flux[LENGTH_DIM]
 * Right side:  flux[e] = q0flux[LENGTH_DIM]
 * Bottom side:  flux[e] = flux from free boundary, if e[LENGTH_DIM] <= maxOffWall
 *                         flux from solid boundary, if e[LENGTH_DIM] > maxOffWall
 * Top side:  flux[e] = q1flux[HEIGHT_DIM], if e[LENGTH_DIM] * dx <= sloc
 *                      q0flux[HEIGHT_DIM], if e[LENGTH_DIM] * dx > sloc
 * sloc = shockLocTop + shockSpeed * time
 * maxOffWall = shockLocBottom / dx
 *
 * need LevelGodunov object to get fluxes and to convert quantities
 *
 * @version  27 Jan 2000
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public class RampBC extends Ramp implements PhysBC {

  private static final int single SPACE_DIM = Util.SPACE_DIM;

  /**
   * Constructor.
   * If you write it so that there is a different RampBC object
   * for each level, then you can set lg, dx, maxOffWall here.
   */
  public RampBC() {
  }


  public void f(Quantities [SPACE_DIM d] state,
		double [SPACE_DIM d] difCoefs,
		int sdim,
		Quantities [SPACE_DIM d] flux,
		double dx) {
    RectDomain<SPACE_DIM> sideDomain = state.domain(), fluxDomain = flux.domain();

    int dim = (sdim > 0) ? sdim : -sdim;
    int side = (sdim > 0) ? 1 : -1;

    // RectDomain<SPACE_DIM> fluxDomain = sideDomain + Util.cell2edge[sdim];

    // System.out.println("Setting flux at the boundary " + fluxDomain);

    if (dim == LENGTH_DIM) {
      if (side == -1) { // Left side
	foreach (e in fluxDomain) flux[e] = q1flux[dim];
      } else if (side == +1) { // Right side
	foreach (e in fluxDomain) flux[e] = q0flux[dim];
      }

    } else if (dim == HEIGHT_DIM) {

      int VELOCITY_N = velocityIndex(dim);
      if (side == -1) { // Bottom side:  solid wall to right of maxOffWall
	// change ceil to floor and then p[LENGTH_DIM] + 1 to p[LENGTH_DIM]?
	int maxOffWall = (int) Math.ceil(shockLocBottom / dx);
	foreach (e in fluxDomain) {
	  Quantities Ucell = state[e + Util.edge2cell[+dim]];
	  Quantities qcell = lg0.consToPrim(Ucell);

	  if (e[LENGTH_DIM] + 1 <= maxOffWall) {
	    flux[e] = lg0.interiorFlux(qcell, dim);
	  } else {
	    Quantities artViscosity =
	      new Quantities(VELOCITY_N, (Ucell[dim]/Ucell[0]) * difCoefs[e]);
	    flux[e] = lg0.solidBndryFlux(qcell, sdim) + artViscosity * side;
	  }
	}
      } else if (side == +1) { // Top side
	double sloc = shockLocTop + shockSpeed * amrAll.getFinetime();
	foreach (e in fluxDomain) {
	  double leftEnd = (double) e[LENGTH_DIM] * dx;
	  double rightEnd = leftEnd + dx;
	  if (rightEnd <= sloc) {
	    flux[e] = q1flux[dim];  // doesn't come out right?
	  } else if (leftEnd >= sloc) {
	    flux[e] = q0flux[dim];
	  } else {
	    double a1 = rightEnd - sloc;
	    double a0 = sloc - leftEnd;
	    flux[e] = (q1flux[dim] * a1 + q0flux[dim] * a0) / (a1 + a0);
	  }
	}
      }

    } else {
      System.out.println("Bogus dimension " + dim);
    }
  }


  public void slope(Quantities [SPACE_DIM d] q,
		    Domain<SPACE_DIM> domainSide,
		    int sdim,
		    Quantities [SPACE_DIM d] dq,
		    double dx) {

    int dim = (sdim > 0) ? sdim : -sdim;
    int side = (sdim > 0) ? 1 : -1;

    if (dim == LENGTH_DIM) {

      slope1(q, domainSide, sdim, dq);

    } else if (dim == HEIGHT_DIM) {

      if (side == -1) { // Bottom side:  solid wall to right of maxOffWall
	// change ceil to floor and then p[LENGTH_DIM] + 1 to p[LENGTH_DIM]?
	int maxOffWall = (int) Math.ceil(shockLocBottom / dx);

        Point<SPACE_DIM> unit = Point<SPACE_DIM>.direction(LENGTH_DIM);
        Point<SPACE_DIM> domainSideMax = (Util.ONE - unit) * domainSide.max() +
	  unit * maxOffWall;
	Domain<SPACE_DIM> domainFree = domainSide *
	  [domainSide.min() : domainSideMax];

	slope1(q, domainFree, sdim, dq);

	Domain<SPACE_DIM> domainWall = domainSide - domainFree;
	slopeWall(q, domainWall, sdim, dq);

      } else if (side == +1) { // Top side

	slope1(q, domainSide, sdim, dq);

      }

    } else {
      System.out.println("Bogus dimension " + dim);
    }
  }


  public void slope1(Quantities [SPACE_DIM d] q,
		     Domain<SPACE_DIM> domainSide,
		     int sdim,
		     Quantities [SPACE_DIM d] dq) {
    int dim = (sdim > 0) ? sdim : -sdim;
    int side = (sdim > 0) ? 1 : -1;

    Point<SPACE_DIM> unitDir = Point<SPACE_DIM>.direction(sdim);
    foreach (p in domainSide) {
      dq[p] = q[p] * (side * 1.5) +
	q[p - unitDir] * (-side * 2.0) +
	q[p - unitDir - unitDir] * (side * 0.5);
    }
  }


  public void slopeWall(Quantities [SPACE_DIM d] q,
			Domain<SPACE_DIM> domainSide,
			int sdim,
			Quantities [SPACE_DIM d] dq) {
    int dim = (sdim > 0) ? sdim : -sdim;
    int side = (sdim > 0) ? 1 : -1;
    Point<SPACE_DIM> unitDir = Point<SPACE_DIM>.direction(sdim);

    int VELOCITY_N = velocityIndex(dim);
    foreach (p in domainSide) {
      double u0 = q[p][VELOCITY_N];
      double u1 = q[p - unitDir][VELOCITY_N];
      double newterm = 0.0;
      if (u0 * (u1 - u0) > 0.0) {
	double sgnu0 = 0.0;
	if (u0 > 0.0) {
	  sgnu0 = 1.0;
	} else if (u0 < 0.0) {
	  sgnu0 = -1.0;
	}
	newterm = (-side * 2.0) * sgnu0 * Math.min(Math.abs(u1 - u0), Math.abs(u0));
      }
      dq[p] = new Quantities(VELOCITY_N, newterm);
    }
  }  
}
