import java.io.*;

/**
 * ParmParse parses input from the command line and input files.
 *
 * Doesn't raise exceptions, but could raise:
 * IOException, NumberFormatException,
 * exceptions for bad input and could not find?
 *
 * @version  27 Oct 1999
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public class ParmParse {

  /*
    constants representing states in parsing
  */

  private static final int STATE_START = 0;
  private static final int STATE_LHS = 1;
  private static final int STATE_RHS_FIRST = 2;
  private static final int STATE_RHS_REST = 3;

  private static boolean single initialized = false;
  private static BoxedList_ParmParseEntry single table;

  private BoxedList_ParmParseEntry myParmSubset;
  private String myPrefix;

  /*
    methods for initializing the table.
  */

  /**
   * Call init() first from main program with command-line args.
   */
  single public static void init(String args[]) {
    if (initialized) {
      System.out.println("ParmParse:  table already built.");
      return;
    }
    table = new BoxedList_ParmParseEntry();

    String argString = "";
    for (int i = 0; i < args.length; i++) argString += args[i] + ' ';
    readArgs(argString);

    initialized = true;
  }


  /**
   * Read input file.
   * Called by addEntry() when "FILE = inputfile"
   * @param inputfile name of input file to open
   */
  private static void readFile(String inputfile) {
    FileInputStream fin;
    ParmParseFileTokenizer tokenizer;

    try {
      fin = new FileInputStream(inputfile);
      tokenizer = new ParmParseFileTokenizer(fin);
    } catch (IOException iox) {
      System.out.println("ParmParse:  Error in opening " + inputfile);
      System.exit(0);
    }

    try {
      tokenize(tokenizer, inputfile);
    } catch (IOException iox) {
      System.out.println("ParmParse:  Error in tokenizing " + inputfile);
      System.exit(0);
    }

    try {
      fin.close();
    } catch (IOException iox) {
      System.out.println("ParmParse:  Error in closing " + inputfile);
      System.exit(0);
    }
  }


  /**
   * Parse string.
   * Called by init().
   */
  private static void readArgs(String argString) {
    ParmParseStringTokenizer tokenizer =
      new ParmParseStringTokenizer(argString);

    try {
      tokenize(tokenizer, "command line");
    } catch (IOException iox) {
      System.out.println("ParmParse:  Error in tokenizing command line");
      System.exit(0);
    }
  }


  /**
   * Tokenize using previously initialized tokenizer.
   * Called by readFile(), readArgs().
   * @param inputfile name of input file (or "command line") for error messages
   */
  public static void tokenize(ParmParseTokenizer tokenizer,
			      String inputfile) {
    int state = STATE_START;
    String head, temp;
    BoxedList_String list;
    int lineNumber;

    try {
      /*
	Let '"' pass through to WORD, since you get sval both ways.

	Can return with:
	WORD && option
	WORD && !option
	'='

	What if it's FILE?

	cases:
	STATE_START:
	- WORD && option:  addEntry(option, new()); START
	- WORD && !option:  head = word; list = new(); LHS
	- default:  "need to start with variable or option name"; ERROR
	LHS:
	- '=':  list = new(); RHS
	- default:  "no '=' sign given after " + head; ERROR
	RHS:
	- WORD && option:  if temp exists, list.push(temp);
	  addEntry(head, list); addEntry(option, new()); START
	- WORD && !option:  if temp exists, list.push(temp); temp = word; RHS
	- '=':  new entry(head, list); head = temp; RHS
      */

      while (tokenizer.nextToken() != ParmParseTokenizer.TT_EOF) {
	String sval = tokenizer.sval;
	lineNumber = tokenizer.lineno();
	// System.out.println("token is " + sval);

	switch (state) {

	case STATE_START:
	  // System.out.println("state STATE_START");
	  switch (tokenizer.ttype) {
	  case ParmParseTokenizer.TT_WORD: case '"':
	    if (isOption(sval)) {
	      addEntry(sval.substring(1), new BoxedList_String());
	      state = STATE_START;
	    } else {
	      head = sval;
	      list = new BoxedList_String();
	      state = STATE_LHS;
	    }
	    break;
	  default:   // case '='
	    System.out.println("ParmParse:  missing variable name" +
			       " in " + inputfile + " at line " + lineNumber);
	    System.exit(0);  // or exception
	  }
	  break;

	case STATE_LHS:
	  // System.out.println("state STATE_LHS");
	  switch (tokenizer.ttype) {
	  case '=':
	    state = STATE_RHS_FIRST;
	    break;
	  default:
	    System.out.println("ParmParse:  no '=' given after " + head +
			       " in " + inputfile + " at line " + lineNumber);
	    System.exit(0);  // or exception
	  }
	  break;

	case STATE_RHS_FIRST:
	  // System.out.println("state STATE_RHS_FIRST");
	  switch (tokenizer.ttype) {
	  case ParmParseTokenizer.TT_WORD: case '"':
	    // OK if sval starts with '-'.
	    // If someone says fname = -what, then they probably mean it.
	    temp = sval;
	    list = new BoxedList_String();
	    state = STATE_RHS_REST;
	    break;
	  case '=':
	    // Another '='; might as well let it pass.
	    state = STATE_RHS_FIRST;
	  }
	  break;

	case STATE_RHS_REST:
	  // System.out.println("state STATE_RHS_REST");
	  switch (tokenizer.ttype) {
	  case ParmParseTokenizer.TT_WORD: case '"':
	    if (isOption(sval)) {
	      list.push(temp);
	      addEntry(head, list);
	      addEntry(sval.substring(1), new BoxedList_String());
	      state = STATE_START;
	    } else {  // just another RHS word
	      list.push(temp);
	      temp = sval;
	      state = STATE_RHS_REST;
	    }
	    break;
	  case '=':
	    addEntry(head, list);
	    head = temp;
	    state = STATE_RHS_FIRST;
	  }
	  break;

	default:
	  System.out.println("ParmParse default state; something wrong" +
			     " in " + inputfile + " at line " + lineNumber);
	}
      }
      // Now wind down.
      switch (state) {
      case STATE_START:
	break;
      case STATE_LHS:
	System.out.println("ParmParse never assigned " + head + 
			   " in " + inputfile + " at line " + lineNumber);
	break;
      case STATE_RHS_FIRST:
	System.out.println("ParmParse never got value for " + head +
			   " in " + inputfile + " at line " + lineNumber);
	break;
      case STATE_RHS_REST:
	list.push(temp);
	addEntry(head, list);
	break;
      default:
	System.out.println("ParmParse default state; something wrong" +
			   " in " + inputfile + " at line " + lineNumber);
      }

    } catch (IOException iox) {
      System.out.println("ParmParse:  Error" +
			 " in " + inputfile + " at line " + lineNumber);
      System.exit(0);
    }
  }


  /**
   * Check whether a string has the form of an option:
   * -[string], where [string] does not begin with a digit.
   */
  private static boolean isOption(String word) {
    if (word.length() < 2) return false;
    if (word.charAt(0) != '-') return false;
    char first = word.charAt(1);
    for (int i = '0'; i <= '9'; i++) {
      if (first == i) return false;
    }
    return true;
  }


  /**
   * Add an entry to the table, or read a new file.
   */
  private static void addEntry(String head, BoxedList_String list) {
    if (head.equals("FILE")) {
      // Get the files in the right order.
      String [1d] fileArray = list.toReversedArray();
      foreach (ind in fileArray.domain()) {
	readFile(fileArray[ind]);
      }
    } else {
      ParmParseEntry ppEntry = new ParmParseEntry(head, list);
      table.push(ppEntry);
    } 
  }


  /**
   * constructor
   */
  public ParmParse(String prefix) {
    if (!initialized) {
      System.out.println("ParmParse:  table not initialized");
      System.exit(0);  // or throw exception
    }
    myPrefix = prefix;
    myParmSubset = findParmSubset(prefix, table);
    if (myParmSubset == null) {
      System.out.println("ParmParse warning:  no such prefix " + prefix);
    }
  }


  /*
    Methods for finding entries in myParmSubset or the table.
  */

  /**
   * Return list of variables, with the prefix removed.
   */
  private BoxedList_ParmParseEntry
    findParmSubset(String prefix, BoxedList_ParmParseEntry tab) {
    BoxedList_ParmParseEntry list = new BoxedList_ParmParseEntry();
    String prefixDot = prefix + '.';
    for (List_ParmParseEntry l = tab.toList(); l != null; l = l.rest()) {
      ParmParseEntry entry = l.first();
      String head = entry.head();
      if (head.startsWith(prefixDot)) {
	String suffix = head.substring(prefixDot.length());
	// Ignore it if it is already in the table.
	if (findEntry(suffix, list) == null) {
	  list.push(new ParmParseEntry(suffix, entry.list()));
	}
      }
    }
    return list;
  }


  /**
   * Return the first entry in tab with this head.
   * If there are duplicates, this is the one that was entered last.
   */
  private static ParmParseEntry
    findEntry(String head, BoxedList_ParmParseEntry tab) {
    for (List_ParmParseEntry l = tab.toList(); l != null; l = l.rest()) {
      ParmParseEntry entry = l.first();
      if (entry.head().equals(head)) return entry;
    }
    return null;
  }

  private static BoxedList_String 
    retrieveList(String head, BoxedList_ParmParseEntry tab, String name) {
    ParmParseEntry entry = findEntry(head, tab);
    if (entry == null) {
      System.out.println("ParmParse:  no entry for " + name);
      System.exit(0);  // or exception?
    }
    BoxedList_String list = entry.list();
    return list;
  }


  /**
   * Get a single String.
   */
  public String getString(String suffix) {
    return retrieveString(suffix, myParmSubset, myPrefix + '.' + suffix);
  }


  public static String getStringGeneral(String head) {
    return retrieveString(head, table, head);
  }


  private static String
    retrieveString(String head, BoxedList_ParmParseEntry tab, String name) {
    BoxedList_String list = retrieveList(head, tab, name);
    if (list.length() == 0) {
      System.out.println("ParmParse:  no value for " + name);
      System.exit(0);  // or exception?
    } else if (list.length() > 1) {
      System.out.println("ParmParse:  multiple values given for " + name);
      System.exit(0);  // or exception?
    }
    return list.first();
  }


  /**
   * Get a single integer.
  */
  public int getInteger(String suffix) {
    return retrieveInteger(suffix, myParmSubset, myPrefix + '.' + suffix);
  }


  public static int getIntegerGeneral(String head) {
    return retrieveInteger(head, table, head);
  }


  private static int
    retrieveInteger(String head, BoxedList_ParmParseEntry tab, String name) {
    String str = retrieveString(head, tab, name);
    int val = 0;
    try {
      val = Integer.parseInt(str);
    } catch (NumberFormatException nfx) {
      System.out.println("ParmParse:  " + name +
			 " should be an integer, can't convert " + str);
      System.exit(0);
    }
    return val;
  }

  /**
   * Get a single double.
  */
  public double getDouble(String suffix) {
    return retrieveDouble(suffix, myParmSubset, myPrefix + '.' + suffix);
  }


  public static double getDoubleGeneral(String head) {
    return retrieveDouble(head, table, head);
  }


  private static double
    retrieveDouble(String head, BoxedList_ParmParseEntry tab, String name) {
    String str = retrieveString(head, tab, name);
    double val = 0.0;
    try {
      val = new Double(str).doubleValue();
    } catch (NumberFormatException nfx) {
      System.out.println("ParmParse:  " + name +
			 " should be a double, can't convert " + str);
      System.exit(0);
    }
    return val;
  }

  /**
   * Get an array of Strings.
   */
  public String [1d] getStringArray(String suffix) {
    return retrieveStringArray(suffix, myParmSubset, myPrefix + '.' + suffix);
  }


  public static String [1d] getStringArrayGeneral(String head) {
    return retrieveStringArray(head, table, head);
  }


  private static String [1d]
    retrieveStringArray(String head, BoxedList_ParmParseEntry tab, String name) {
    BoxedList_String list = retrieveList(head, tab, name);
    return list.toReversedArray();
  }

  
  /**
   * Get an array of integers.
   */
  public int [1d] getIntegerArray(String suffix) {
    return retrieveIntegerArray(suffix, myParmSubset, myPrefix + '.' + suffix);
  }


  public static int [1d] getIntegerArrayGeneral(String head) {
    return retrieveIntegerArray(head, table, head);
  }


  private static int [1d]
    retrieveIntegerArray(String head, BoxedList_ParmParseEntry tab, String name) {
    String [1d] stringArray = retrieveStringArray(head, tab, name);
    RectDomain<1> dom = stringArray.domain();
    int [1d] integerArray = new int[dom];
    foreach (ind in dom) {
      int val;
      try {
	val = Integer.parseInt(stringArray[ind]);
      } catch (NumberFormatException nfx) {
	System.out.println("ParmParse:  " + name + ind +
			   " should be an integer, can't convert " + stringArray[ind]);
	System.exit(0);
      }
      integerArray[ind] = val;
    }
    return integerArray;
  }

  /**
   * Get an array of doubles.
   */
  public double [1d] getDoubleArray(String suffix) {
    return retrieveDoubleArray(suffix, myParmSubset, myPrefix + '.' + suffix);
  }


  public static double [1d] getDoubleArrayGeneral(String head) {
    return retrieveDoubleArray(head, table, head);
  }


  private static double [1d]
    retrieveDoubleArray(String head, BoxedList_ParmParseEntry tab, String name) {
    String [1d] stringArray = retrieveStringArray(head, tab, name);
    RectDomain<1> dom = stringArray.domain();
    double [1d] doubleArray = new double[dom];
    foreach (ind in dom) {
      double val;
      try {
	val = new Double(stringArray[ind]).doubleValue();
      } catch (NumberFormatException nfx) {
	System.out.println("ParmParse:  " + name + ind +
			   " should be a double, can't convert " + stringArray[ind]);
	System.exit(0);
      }
      doubleArray[ind] = val;
    }
    return doubleArray;
  }


  /**
   * Check whether the table contains a particular header.
   */
  public boolean contains(String suffix) {
    ParmParseEntry entry = findEntry(suffix, myParmSubset);
    if (entry == null) {
      return false;
    } else {
      return true;
    }
  }


  public static boolean containsGeneral(String head) {
    ParmParseEntry entry = findEntry(head, table);
    if (entry == null) {
      return false;
    } else {
      return true;
    }
  }


  /*
    output methods
  */

  public static void printTableGeneral() {
    printTableSpecified(table);
  }

  public void printTable() {
    System.out.println("Table for " + myPrefix + ":");
    printTableSpecified(myParmSubset);
  }

  private static void printTableSpecified(BoxedList_ParmParseEntry tab) {
    for (List_ParmParseEntry l = tab.toList(); l != null; l = l.rest()) {
      l.first().print();
    }
  }
}
