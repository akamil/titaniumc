import java.io.*;

/**
 * HSCLLevel is for hyperbolic systems.
 *
 * @version  25 Aug 2000
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public class HSCLLevel extends AmrLevel {
  
  /*
    constants
  */

  private static final int single SPECIAL_PROC = 0;

  public static final Point<SPACE_DIM> TWO = Point<SPACE_DIM>.all(2);

  /*
    fields that are arguments to constructor
  */

  /** physical boundary conditions */
  private PhysBC bc;

  /*
    other fields that are set in constructor
  */

  /** maximum Courant-Friedrichs-Levy number */
  private double single cflmax;

  /** conserved variable to use for gradient to decide tagging */
  private int tagvar;

  /** whether to use gradient to tag cells */
  private boolean single useGradient;

  /** whether to adjust at interfaces with coarser level */
  private boolean single useNoCoarseInterface;

  /** whether to adjust at interfaces with finer level */
  private boolean single useNoFineInterface;

  /** minimum relative difference to tag a cell when using gradient method */
  private double minRelDiff;

  /** minimum relative difference in truncation error to tag a cell */
  private double minRelTrunc;

  private LevelGodunov single lg;

  /*
    other fields
  */

  private boolean single evenParity = true;

  /** perimeter of grids at this level */
  private int coarsePerimeter = 0;

  /** perimeter of grids at next finer level */
  private int finePerimeter = 0;

  private Domain<SPACE_DIM> coarseInterface = Util.EMPTY;

  private Domain<SPACE_DIM> coarseInterfaceOff = Util.EMPTY;

  private Domain<SPACE_DIM> fineInterface = Util.EMPTY;

  private HSCLLevel single finerLevel = null;

  /**
   * constructor
   */
  single public HSCLLevel(
			  SharedRegion single MyRegion, // REGION
		   Amr single Parent,
		   int single Level,
		   BoxArray single Ba,
		   BoxArray single BaCoarse,
		   RectDomain<SPACE_DIM> DProblem,
		   int single NRefineCoarser,
		   double single Dx,
		   PhysBC BC) {
    super(
	  MyRegion, // REGION
	  Parent, Level, Ba, BaCoarse, DProblem, NRefineCoarser, Dx);
    bc = BC;
	Logger.append("getting LevelGodunov object for level " + Level);
    lg = new 
      (myRegion) // REGION
      LevelGodunov(
		   myRegion, // REGION
		   ba, baCoarse, dProblem, nRefineCoarser, dx, bc);
	Logger.append("got LevelGodunov object for level " + Level);
    ParmParse pp = new ParmParse("hscl");
    cflmax = broadcast pp.getDouble("cfl") from SPECIAL_PROC;
    useGradient = broadcast pp.contains("gradient") from SPECIAL_PROC;
    if (useGradient) {
      minRelDiff = pp.getDouble("tagthresh");
    } else {
      minRelTrunc = pp.getDouble("truncthresh");
    }
    tagvar = pp.getInteger("tagvar");
    computePerimeter();
    useNoFineInterface = broadcast pp.contains("no_fine_interface") from SPECIAL_PROC;
    useNoCoarseInterface = broadcast pp.contains("no_coarse_interface") from SPECIAL_PROC;
  }

  /*
    accessor methods
  */

  single private int getCoarsePerimeter() { return coarsePerimeter; }

  single private Domain<SPACE_DIM> getCoarseInterfaceOff() { return coarseInterfaceOff; }

  /*
    methods
  */

  single private void setFinerLevel(HSCLLevel single newLevel) {
    finerLevel = newLevel;
  }

  /**
   * Find coarsePerimeter and coarseInterface.
   */
  single private void computePerimeter() {
    if (level == 0) return;  // there is no coarser level

    /*
      Find union = domain of all cells in grids at this level
    */

    Domain<SPACE_DIM> unionLocal = Util.EMPTY;
    foreach (lba in myBoxes) unionLocal = unionLocal + ba[lba];

    // kludge
    RectDomain<1> single procTeam = [0 : Ti.numProcs() - 1];
    Domain<SPACE_DIM> [1d] single unionAll =
      (Domain<SPACE_DIM> [1d] single) new Domain<SPACE_DIM>[procTeam];
    unionAll.exchange(unionLocal);
    Domain<SPACE_DIM> unionSpecial = Util.EMPTY;
    if (myProc == SPECIAL_PROC) {
      foreach (proc in procTeam) {
        Domain<SPACE_DIM> dd = unionAll[proc];
        Domain<SPACE_DIM> dd_local = Domain<SPACE_DIM>.toDomain(dd.RectDomainList());
        unionSpecial = unionSpecial + dd_local;
      }
    }
    Logger.barrier();
    Domain<SPACE_DIM> single union = broadcast unionSpecial from SPECIAL_PROC;

    /*
      Find coarseInterface, which consists of the cells on this level that
      are adjacent (including diagonally) to cells on next coarser level.
      coarseInterface is everything in the domain that is NOT in the
      proper nesting domain.

      coarseInterfaceOff = domain just _outside_ coarseInterface
    */
    Domain<SPACE_DIM> PND = union, unionPlus = union;
    foreach (pdim in Util.DIMENSIONS) foreach (pdir in Util.DIRECTIONS) {
      // extend one cell in each direction
      Point<SPACE_DIM> shift = Point<SPACE_DIM>.direction(pdim[1]*pdir[1], 1);

      Domain<SPACE_DIM> remove = (((PND + shift) - PND) * dProblem) - shift;
      PND = PND - remove;

      Domain<SPACE_DIM> newSide = (unionPlus + shift) * dProblem;
      unionPlus = unionPlus + newSide;
    }
    coarseInterface = union - PND;
    coarseInterfaceOff = unionPlus - union;

    /*
    coarseInterface = Util.EMPTY;
    foreach (pdim in Util.DIMENSIONS) foreach (pdir in Util.DIRECTIONS) {
      // extend one cell in each direction
      Point<SPACE_DIM> shift = Point<SPACE_DIM>.direction(pdim[1]*pdir[1], 1);
      Domain<SPACE_DIM> newSide = 
	(((union + shift) - union) * dProblem) - shift;
      coarseInterface = coarseInterface + newSide;
    }
    */
    coarsePerimeter = coarseInterface.size();
  }


  single public void getSaved(MyInputStream in) {
    try {
    // (a) level number and # of grids per level (2*int)
    int [1d] savedLevelStuff = in.readInts();
    int numGrids = savedLevelStuff[1];

    RectDomain<SPACE_DIM> [1d] domainlist = new RectDomain<SPACE_DIM>[0:numGrids-1];
    /*
      The plot file doesn't specify where each grid is stored.
      (Just as well:  you shouldn't have to continue with the same
      number of processes.)
      You'll need to call the load balancer.
    */

    for (int ind = 0; ind < numGrids; ind++) {
      // For each grid,
      // (1) Grid indices (BOX)
      domainlist[ind] = in.readDomain();
      
      // (2) Level (int)
      int savedLevel = in.readInt1();
      if (savedLevel != level) {
	System.out.println("Plot file has miscounted grids.");
	System.exit(0);
      }

      // (3) Steps taken on this grid (int)
      in.ignoreLine();
      
      // (4) Time for this grid (REAL)
      in.ignoreLine();

      // (5) Physical location of grid corners (BL_SPACEDIM*(REAL))
      foreach (pdim in Util.DIMENSIONS) in.ignoreLine();

      // (6) writeOn data dump of each FAB of data on that grid
    }

    } catch (IOException iox) {
      System.out.println("Error in reading saved data");
      System.exit(0);
    }
  }


  single public double single estTimeStep() {
    double single lgest = lg.estTimeStep(UNew);
    // System.out.println("Level " + level + " max v = " + (dx/lgest) + 
    // " estimated timestep:  " + (cflmax*lgest));
    return (cflmax * lgest);
  }


  /**
   * Use new solution at this level to update solution at coarser level.
   */
  single public void post_timestep() {
    initial_step = false;
    if (level > 0) {
      Logger.barrier();
	Logger.append("reflux");
      // System.out.println("Trying reflux at level " + level);
      try {
	reflux();
      } catch (Exception x) {
	System.out.println("Error in reflux()");
      }
      Logger.barrier();
	Logger.append("avgDown");
      // System.out.println("Trying avgDown at level " + level);
      try {
	avgDown();
      } catch (Exception x) {
	System.out.println("Error in avgDown()");
      }
      Logger.barrier();
	Logger.append("end post_timestep");
    }
  }

  single public double advance(double single Time, double single dt) {
    swapTimeLevels(dt);
    // System.out.println("calling level " + level +
    // " lg.step from " + Time + " by " + dt);
    double single cflout;
    // Recall initialization evenParity = true;

    LevelArrayQuantities single UCOld = (level == 0) ?
      LevelArrayQuantities.Null() : amrlevel_coarser.getUOld();
    LevelArrayQuantities single UCNew = (level == 0) ?
      LevelArrayQuantities.Null() : amrlevel_coarser.getUNew();

    // If level == 0 then coarse times aren't used.
    double single TCOld = (level == 0) ? 0.0 : amrlevel_coarser.getOldTime();
    double single TCNew = (level == 0) ? 0.0 : amrlevel_coarser.getNewTime();

    FluxRegister Fr = (level == 0) ? null : fReg;
    FluxRegister FrFine = (level == parent.finestLevel()) ?
      null : parent.getLevel(level + 1).getfReg();

    cflout =
      lg.step(UNew, UCOld, UCNew,
	      Time, TCOld, TCNew, dt,
	      Fr, FrFine, evenParity, true, true);

    evenParity = !evenParity;
    return cflout;
  }

  /**
   * Update the coarser level using flux registers living on this level.
   * Called from post_timestep().
   */
  single private void reflux() {
    fReg.reflux(amrlevel_coarser.getUNew());
  }


  /**
   * Tag cells at this level.
   *
   * Called by:
   * initGrids() for initialization
   * Amr.regrid() for regridding
   * Amr.getInitialBoxes() for initialization -- obsolete
   */
  single public Domain<SPACE_DIM> tag() {
    Domain<SPACE_DIM> myTagged = Util.EMPTY;
    RectDomain<SPACE_DIM> baseSquare = [Util.MINUS_ONE : Util.ONE];
    Logger.append("tagging for " + myProc);
    // System.out.println("tagging for proc " + myProc + " at level " + level);

    // System.out.println("HSCLLevel.tag():  new tagRegion");
    SharedRegion single tagRegion = new SharedRegion(); // REGION
    // Make sure there are no problems with tagRegion vs. myRegion
    { // scope for new objects using tagRegion

      // System.out.println("repeat:  tagging for proc " + myProc + " at level " + level);
      if (useGradient) {
        // use gradient

	System.out.println("Proc " + myProc + " useGradient, level " + level);
        foreach (lba in myBoxes)
	  myTagged = myTagged + tagBoxGradient(UNew[lba], ba[lba]);

      } else /* not useGradient */ {

	// use truncation error estimate from Richardson extrapolation

	if (initial_step) {
	  // System.out.println("Proc " + myProc + " initial step, level " + level);
	  // Level 0 differs from higher levels in that there is no
	// coarser level.
	// Hence no interpolation from a coarser level.

	// if (level == 0) 
	// Should really set dt only at level 0, because otherwise
	// there may be inconsistencies.
	// If not initial step, then you can use Amr.computeNewDt(),
	// which goes through every level.
	double single dt = estTimeStep();

	/*
	  Run stepSymmetric three times:
	  1.  U(0) to U(dt), dx=h.
	  2.  U(dt/2) to U(3*dt/2), dx=h, where U(dt/2) ~ (U(0) + U(dt))/2.
	  3.  U(0) to U(2*dt), dx=2*h.

                                     2*dt  =
                                           |
                      1.5*dt  =            |
                              |            |
             dt  =            |            |
                 |            |            |
         0.5*dt  -  - - - ->  =            |
                 |            h            |
              0  =  - - - - - - - - - ->   =
                 h                        2h

	       UInter      UTempHalf     UTemp2
	 */

	// UOld has not been set yet.
	// Perhaps I should rewrite it so that UOld does get set,
	// and UNew gets set to UInter.
	LevelArrayQuantities single UCOld = (level == 0) ?
	  LevelArrayQuantities.Null() : amrlevel_coarser.getUNew();
	LevelArrayQuantities single UCNew = (level == 0) ?
	  LevelArrayQuantities.Null() : amrlevel_coarser.getUInter();

	/*
	  1.
	  UNew at this level has been initialized by initGrids().

	  Put copy of UNew into UInter, and
	  advance UInter symmetrically from 0 to dt.
	*/

	UInter = new
	  (myRegion) // REGION
	  LevelArrayQuantities(
			       myRegion, // REGION
			       ba);

	UInter.copy(UNew);

	LevelGodunov single lgTag = new
	  (tagRegion) // REGION
	  LevelGodunov(
		       tagRegion, // REGION
		       ba, baCoarse, dProblem, nRefineCoarser, dx, bc);

	// advance
	lgTag.stepSymmetric(UInter, UCOld, UCNew,
			    0.0, 0.0, nRefineCoarser * dt, dt);

	/*
	  2.
	  Interpolate from UNew (t = 0) and UInter (t = dt)
	  to UTempHalf (t = 0.5*dt).
	  Then advance UTempHalf from 0.5*dt to 1.5*dt.

	  We want dUdt = (U(1.5*dt) - U(0.5*dt)) / dt
	  instead of the new solution.
	  In LevelGodunov.update():
	  stateNew = stateOld - fluxDiff * dt/dx
	  (stateNew - stateOld) / dt = - fluxDiff / dx
	*/

	LevelArrayQuantities single UTempHalf = new
	  (tagRegion) // REGION
	  LevelArrayQuantities(
			       tagRegion, // REGION
			       ba);

	// interpolate
	foreach (lba in myBoxes) foreach (p in ba[lba])
	  UTempHalf[lba][p] = (UNew[lba][p] + UInter[lba][p]) * 0.5;
	
	lgTag = new
	  (tagRegion) // REGION
	  LevelGodunov(
		       tagRegion, // REGION
		       ba, baCoarse, dProblem, nRefineCoarser, dx, bc);

	LevelArrayQuantities single dUdt = new
	  (tagRegion) // REGION
	  LevelArrayQuantities(
			       tagRegion, // REGION
			       ba);

	// advance
	lgTag.stepSymmetricDiff(dUdt, UTempHalf, UCOld, UCNew,
				dt * 0.5, 0.0, dt * nRefineCoarser, dt);

	/*
	  3.
	  Average UNew to a coarsened version, UTemp2, and
	  then advance UTemp2 from 0 to 2*dt.
	*/

	RectDomain<SPACE_DIM> [1d] domainlist2 = new
	  (tagRegion) // REGION
	  RectDomain<SPACE_DIM>[ba.indices()];

	int [1d] proclist2 = new 
	  (tagRegion) // REGION
	  int[ba.indices()];

	proclist2.copy(ba.proclist());

	foreach (ind in ba.indices())
	  domainlist2[ind] = ba[ind] / TWO;

	BoxArray single ba2 = new
	  (tagRegion) // REGION
	  BoxArray(domainlist2, proclist2);

	// my2Boxes should be the same as myBoxes
	Domain<1> my2Boxes = ba2.procboxes(myProc);

	LevelArrayQuantities single UTemp2 = new
	  (tagRegion) // REGION
	  LevelArrayQuantities(
			       tagRegion, // REGION
			       ba2);
	  
	foreach (lba in my2Boxes) foreach (p in ba2[lba])
	  UTemp2[lba][p] =
	  Quantities.Average(UNew[lba].restrict(Util.subcells(p, 2)));

	// System.out.println("ba2 = " + ba2);

	lgTag = new
	  (tagRegion) // REGION
	  LevelGodunov(
		       tagRegion, // REGION
		       ba2, baCoarse,
		       dProblem / TWO,
		       nRefineCoarser / 2, dx * 2.0, bc);

	LevelArrayQuantities single dU2dt = new
	  (tagRegion) // REGION
	  LevelArrayQuantities(
			       tagRegion, // REGION
			       ba2);

	// advance
	lgTag.stepSymmetricDiff(dU2dt, UTemp2, UCOld, UCNew,
				0.0, 0.0, dt * nRefineCoarser, dt * 2.0);

      /*
	Find maximum of dU2dt
      */

      double maxdU2dtLocal = 0.0;
      foreach (lba in my2Boxes) foreach (p in ba2[lba]) {
	double thisnorm = Math.abs(dU2dt[lba][p][tagvar]);
	if (thisnorm > maxdU2dtLocal) maxdU2dtLocal = thisnorm;
      }
      // System.out.println("maxdU2dtLocal = " + maxdU2dtLocal);
      double single maxdU2dt = Reduce.max(maxdU2dtLocal);

      /*
	Truncation error estimate is average(dUdt) - dU2dt.
	We take the norm to make things simpler.
      */

      LevelArraydouble single truncError = new
	(tagRegion) // REGION
	LevelArraydouble(
			 tagRegion, // REGION
			 ba2);

      // mixing my2Boxes and myBoxes, but they should be the same
      foreach (lba in my2Boxes) foreach (p in ba2[lba])
	truncError[lba][p] =
	Math.abs((Quantities.Average(dUdt[lba].restrict(Util.subcells(p, 2))) -
		  dU2dt[lba][p])[tagvar]);

      Ti.barrier();
	
        /*
          Copy truncation error.
        */

        truncErrorCopied = new
            (myRegion) // REGION
            LevelArraydouble(
                             myRegion, // REGION
                             ba2);
        foreach (lba in myBoxes) foreach (p in ba2[lba])
            truncErrorCopied[lba][p] = truncError[lba][p];
        Ti.barrier();
        // System.out.println("level " + level +
	// 		   " err boxes:  " + truncErrorCopied.boxArray());


        /*
          Copy adjusted truncation error.
        */

        truncErrorCopied2 = new
            (myRegion) // REGION
            LevelArraydouble(
                             myRegion, // REGION
                             ba2);
        foreach (lba in myBoxes) foreach (p in ba2[lba])
            truncErrorCopied2[lba][p] = truncError[lba][p];
        Ti.barrier();
        // System.out.println("level " + level +
	//	   	" err boxes:  " + truncErrorCopied2.boxArray());


      /*
	Tag where truncError >= epsilon * maxdU2dt
      */

      foreach (lba in my2Boxes) foreach (p in ba2[lba])
	if (truncError[lba][p] > minRelTrunc * maxdU2dt)
	  foreach (p2 in (baseSquare + p))
	    myTagged = myTagged + Util.subcells(p2, 2);
      // myTagged = myTagged + Util.subcells(p, 2);

      } else /* not initial_step */ {

	// System.out.println("Proc " + myProc + " not initial step, level " + level);
	// Level 0 differs from higher levels in that there is no
	// coarser level.
	// Hence no interpolation from a coarser level.

	// if (level == 0) 
	// Should really set dt only at level 0, because otherwise
	// there may be inconsistencies.
	// If not initial step, then you can use Amr.computeNewDt(),
	// which goes through every level.
	// double single dt = estTimeStep();

	// if (level == 0) parent.computeNewDt();
	// double single dt = parent.getDt(level);

	// Use old dt, because you're using UOld and UNew.
	double single dt = parent.getDt(level);

        /*
          Run stepSymmetric twice:
          1.  U(t-dt/2) to U(t+dt/2), dx=h, where U(t-dt/2) ~ (U(t)+U(t-dt))/2.
          2.  U(t-dt) to U(t+dt), dx=2*h.

                                   t + dt  =
                                           |
                    t + dt/2  =            |
                              |            |
              t  =            |            |
                 |            |            |
       t - dt/2  -  - - - ->  =            |
                 |            h            |
         t - dt  =  - - - - - - - - - ->   =
                 h                        2h

           UOld -> UNew    UTempHalf     UTemp2
         */

	LevelArrayQuantities single UCOld = (level == 0) ?
	  LevelArrayQuantities.Null() : amrlevel_coarser.getUOld();
	LevelArrayQuantities single UCNew = (level == 0) ?
	  LevelArrayQuantities.Null() : amrlevel_coarser.getUNew();

	// If level == 0 then coarse times aren't used.
	double single TCOld = (level == 0) ?
	  0.0 : amrlevel_coarser.getOldTime();
	double single TCNew = (level == 0) ?
	  0.0 : amrlevel_coarser.getNewTime();

	/*
	  1.
	  Interpolate from UOld and UNew into UTempHalf.
	  Then advance UTempHalf from t - 0.5*dt to t + 0.5*dt.

	  We want dUdt = (U(t - 0.5*dt) - U(t + 0.5*dt)) / dt
	  instead of the new solution.
	  In LevelGodunov.update():
	  stateNew = stateOld - fluxDiff * dt/dx
	  (stateNew - stateOld) / dt = - fluxDiff / dx
	*/

	LevelArrayQuantities single UTempHalf = new
	  (tagRegion) // REGION
	  LevelArrayQuantities(
			       tagRegion, // REGION
			       ba);

	foreach (lba in myBoxes) foreach (p in ba[lba])
	  UTempHalf[lba][p] = (UOld[lba][p] + UNew[lba][p]) * 0.5;

	
	LevelGodunov single lgTag = new
	  (tagRegion) // REGION
	  LevelGodunov(
		       tagRegion, // REGION
		       ba, baCoarse, dProblem, nRefineCoarser, dx, bc);

	LevelArrayQuantities single dUdt = new
	  (tagRegion) // REGION
	  LevelArrayQuantities(
			       tagRegion, // REGION
			       ba);

        // advance
        lgTag.stepSymmetricDiff(dUdt, UTempHalf, UCOld, UCNew,
                                dt * 0.5, TCOld, TCNew, dt);

	/*
	  2.
	  Average UOld to a coarsened version, UTemp2, and
	  then advance UTemp2 from  t - dt  to  t + dt.
	*/


        RectDomain<SPACE_DIM> [1d] domainlist2 = new
          (tagRegion) // REGION
          RectDomain<SPACE_DIM>[ba.indices()];

        int [1d] proclist2 = new 
          (tagRegion) // REGION
          int[ba.indices()];

        proclist2.copy(ba.proclist());

        Point<SPACE_DIM> TWO = Point<SPACE_DIM>.all(2);

        foreach (ind in ba.indices())
          domainlist2[ind] = ba[ind] / TWO;

        BoxArray single ba2 = new
          (tagRegion) // REGION
          BoxArray(domainlist2, proclist2);

        // my2Boxes should be the same as myBoxes
        Domain<1> my2Boxes = ba2.procboxes(myProc);

        LevelArrayQuantities single UTemp2 = new
          (tagRegion) // REGION
          LevelArrayQuantities(
                               tagRegion, // REGION
                               ba2);
          
        foreach (lba in my2Boxes) foreach (p in ba2[lba])
          UTemp2[lba][p] =
          Quantities.Average(UOld[lba].restrict(Util.subcells(p, 2)));

        // System.out.println("ba2 = " + ba2);

        lgTag = new
          (tagRegion) // REGION
          LevelGodunov(
                       tagRegion, // REGION
                       ba2, baCoarse,
                       dProblem / TWO,
                       nRefineCoarser / 2, dx * 2.0, bc);

        LevelArrayQuantities single dU2dt = new
          (tagRegion) // REGION
          LevelArrayQuantities(
                               tagRegion, // REGION
                               ba2);

        // advance
        lgTag.stepSymmetricDiff(dU2dt, UTemp2, UCOld, UCNew,
				oldTime, TCOld, TCNew, dt * 2.0);
	// is oldTime correct?
				

	/*
	  Find maximum of dU2dt
	*/

	double maxdU2dtLocal = 0.0;
	foreach (lba in my2Boxes) foreach (p in ba2[lba]) {
	  double thisnorm = Math.abs(dU2dt[lba][p][tagvar]);
	  if (thisnorm > maxdU2dtLocal) maxdU2dtLocal = thisnorm;
	}
	// System.out.println("maxdU2dtLocal = " + maxdU2dtLocal);
	double single maxdU2dt = Reduce.max(maxdU2dtLocal);

	// find max of dUdt

	double maxdUdtLocal = 0.0;
	foreach (lba in myBoxes) foreach (p in ba[lba]) {
	  double thisnorm = Math.abs(dUdt[lba][p][tagvar]);
	  if (thisnorm > maxdUdtLocal) maxdUdtLocal = thisnorm;
	}
	// System.out.println("maxdUdtLocal = " + maxdUdtLocal);

	/*
	  Truncation error estimate is average(dUdt) - dU2dt.
	  We take the norm to make things simpler.
	*/

	LevelArraydouble single truncError = new
	  (tagRegion) // REGION
	  LevelArraydouble(
			   tagRegion, // REGION
			   ba2);
	
	// mixing my2Boxes and myBoxes, but they should be the same
	foreach (lba in my2Boxes) foreach (p in ba2[lba])
	  truncError[lba][p] =
	  Math.abs((Quantities.Average(dUdt[lba].restrict(Util.subcells(p, 2))) -
		    dU2dt[lba][p])[tagvar]);
	
	Ti.barrier();
	
        /*
          Copy truncation error.
        */

        truncErrorCopied = new
            (myRegion) // REGION
            LevelArraydouble(
                             myRegion, // REGION
                             ba2);
        foreach (lba in myBoxes) foreach (p in ba2[lba])
            truncErrorCopied[lba][p] = truncError[lba][p];
        Ti.barrier();
        // System.out.println("level " + level +
	// 		   " err boxes:  " + truncErrorCopied.boxArray());

	/*
	  At coarse-fine boundaries, multiply truncation error by 
	  perimeter / volume.
	  
	  Do the same at physical boundaries?
	*/
	
	// Find total number of cells in the coarsened grids.
	int volumeLocal = 0;
	foreach (lba in myBoxes) volumeLocal += ba[lba].size();
	int single volume = Reduce.add(volumeLocal);

	/*
	  Adjust truncation error at cells on interface with the next
	  _finer_ level, if any (level < finest_level).
	*/
      
	// System.out.println("Proc " + myProc + " fine adjust, level " + level);
	if (!useNoFineInterface && finerLevel != null) {
	  finePerimeter = finerLevel.getCoarsePerimeter();
	  // convert to ba2 scale
	  fineInterface = finerLevel.getCoarseInterfaceOff();
	  int single coarsenRatio = parent.getRefRatio(level);
	  Domain<SPACE_DIM> fineInterface2 =
	    Util.coarsen(fineInterface, coarsenRatio * 2);
	  double adjustment = ((double) finePerimeter) / ((double) volume);
	  if (myProc == SPECIAL_PROC)
	    System.out.println("level " + level + " fine adjustment == " + adjustment);

	  int affectedLocal = 0;
	  Domain<SPACE_DIM> affectedDomain = Util.EMPTY;
	  foreach (lba in my2Boxes) {
	      affectedDomain = affectedDomain + (fineInterface2 * ba2[lba]);
	      foreach (p in fineInterface2 * ba2[lba]) {
	    truncError[lba][p] = truncError[lba][p] * adjustment;
	    affectedLocal++;
	      }
	  }
	  int single affected = Reduce.add(affectedLocal);
	  /*
	  if (myProc == SPECIAL_PROC) {
	    System.out.println("level " + level + " fine adjustment on " + affected + " boxes:");
	    System.out.println(affectedDomain);
	  }
	  */

	} else {
	  // System.out.println("Finer level than " + level + " is null");
	}

	/*
	  Adjust truncation error at cells on interface with the next
	  _coarser_ level, if any (level > 0).
	*/

	// System.out.println("Proc " + myProc + " coarse adjust, level " + level);
	if (!useNoCoarseInterface && level > 0) {
	  // Should have coarsePerimeter, coarseInterface.
	  Domain<SPACE_DIM> coarseInterface2 =
	    Util.coarsen(coarseInterface, 2);
	  double adjustment = ((double) coarsePerimeter) / ((double) volume);
	  if (myProc == SPECIAL_PROC)
	    System.out.println("level " + level + " coarse adjustment == " + adjustment);

	  int affectedLocal = 0;
          Domain<SPACE_DIM> affectedDomain = Util.EMPTY;
	  foreach (lba in my2Boxes) {
              affectedDomain = affectedDomain + (coarseInterface2 * ba2[lba]);
	      foreach (p in coarseInterface2 * ba2[lba]) {
	    truncError[lba][p] = truncError[lba][p] * adjustment;
	    affectedLocal++;
	      }
	  }
	  int single affected = Reduce.add(affectedLocal);
	  /*
	  if (myProc == SPECIAL_PROC) {
	    System.out.println("level " + level + " coarse adjustment on " + affected + " boxes");
            System.out.println(affectedDomain);
          }
	  */
	}

	Ti.barrier();

        /*
          Copy adjusted truncation error.
        */

        truncErrorCopied2 = new
            (myRegion) // REGION
            LevelArraydouble(
                             myRegion, // REGION
                             ba2);
        foreach (lba in myBoxes) foreach (p in ba2[lba])
            truncErrorCopied2[lba][p] = truncError[lba][p];
        Ti.barrier();
        // System.out.println("level " + level +
	//	   	" err boxes:  " + truncErrorCopied2.boxArray());

	/*
	  Tag where truncError >= epsilon * maxdU2dt
	*/
	
	foreach (lba in my2Boxes) foreach (p in ba2[lba])
	  if (truncError[lba][p] > minRelTrunc * maxdU2dt)
	    foreach (p2 in (baseSquare + p))
	      myTagged = myTagged + Util.subcells(p2, 2);

      } // if (initial_step)
      
      } // if (useGradient)
    } // scope for new objects using tagRegion
    RegionUtil.delete(tagRegion); // REGION
    Logger.append("got tagged for " + myProc + ":  " +
		  myTagged.size() + " points");
    // System.out.println("got tagged for proc " + myProc + ":  " + myTagged.size() + " points");
    // System.out.println("-> " + myTagged);
    Logger.barrier();
    RectDomain<1> single procTeam = [0 : Ti.numProcs() - 1];
    Domain<SPACE_DIM> [1d] single taggedAll =
      (Domain<SPACE_DIM> [1d] single) new Domain<SPACE_DIM>[procTeam];
    taggedAll.exchange(myTagged);
    Domain<SPACE_DIM> taggedUnion = Util.EMPTY;
    if (myProc == SPECIAL_PROC) {
      foreach (proc in procTeam) {
	Domain<SPACE_DIM> dd = taggedAll[proc];
	Domain<SPACE_DIM> dd_local = Domain<SPACE_DIM>.toDomain(dd.RectDomainList());
	taggedUnion = taggedUnion + dd_local;
      }
    }
    Logger.barrier();
    Domain<SPACE_DIM> single tagged = broadcast taggedUnion from SPECIAL_PROC;

    Logger.append("got tagged");

    return tagged;
  }


  /**
   * Tag cells in a particular box at this level, using gradient.
   */
  private Domain<SPACE_DIM> tagBoxGradient(Quantities [SPACE_DIM d] U,
					   RectDomain<SPACE_DIM> box) {
    // System.out.println("Tagging cells in " + box);
    Domain<SPACE_DIM> taggedDomain = Util.EMPTY;
    RectDomain<SPACE_DIM> baseSquare = [Util.MINUS_ONE : Util.ONE];
    /*
      Look at every point  p  in every box.
      If the relative difference in variable tagvar
      between  p  and one of its neighbors (two in each direction)
      exceeds the threshold minRelDiff,
      then tag  p  and all of the neighbors of  p, including diagonal ones.
    */
    foreach (p in box) {
      double var0 = U[p][tagvar];  // conserved component tagvar=0 is density
      // System.out.println("density" + p + " = " + var0);
      foreach (pdim in Util.DIMENSIONS) foreach (pdir in Util.DIRECTIONS) {
	Point<SPACE_DIM> pnew = p + Point<SPACE_DIM>.direction(pdim[1] * pdir[1]);
	if (box.contains(pnew)) {
	  double var1 = U[pnew][tagvar];
	  double relDiff = 2.0 * Math.abs(var0 - var1) /
	    (Math.abs(var0) + Math.abs(var1));
	  // if (relDiff > 0.0) System.out.println("relDiff = " + relDiff);
	  if (relDiff > minRelDiff)
	    taggedDomain = taggedDomain + (baseSquare + p);
	}
      }
    }
    return taggedDomain;
  }


  /**
   * initGrids() is called from the HSCLLevel object at level 0 only.
   * It creates the HSCLLevel objects for all higher levels,
   * and calls the routines to fill them in.
   *
   * In main program:
   * amrAll = new Amr();
   * ba0 = amrAll.baseBoxes();
   * HSCLLevel single level0 = new HSCLLevel(amrAll, 0, ba0, ...);
   * amrAll.setLevel(0, level0);
   * level0.initGrids();
   */
  single public void initGrids(InitialPatch initializer) {
    initData(initializer);
    BoxArray single baNewCoarse = ba;
    Point<SPACE_DIM> single extentLevel = parent.getExtent();
    HSCLLevel single coarserLevel = this;
    for (int single lev = 1; lev <= parent.finestLevel(); lev++) {
      int single levelm1 = lev - 1;

      Domain<SPACE_DIM> [1d] tags = new
	(myRegion) // REGION
	Domain<SPACE_DIM>[levelm1 : levelm1];

      tags[levelm1] = parent.getLevel(levelm1).tag();
      RectDomain<SPACE_DIM> [1d][1d] boxes =
	MeshRefine.refine(
			  myRegion, // REGION
			  parent, tags);

      RectDomain<SPACE_DIM> single finerDomain = parent.getDomain(lev);
      BoxArray single baNew = assignBoxes(
					  myRegion, // REGION
					  boxes[lev], finerDomain);
      // System.out.println("************ Level " + lev + " boxes: " + baNew);
					  
      Logger.append("getting new HSCLLevel " + lev);
      // System.out.println("new HSCLLevel " + lev);
      HSCLLevel single newLevel =
	new HSCLLevel(
		      myRegion, // REGION
		      parent, lev, baNew, baNewCoarse,
		      finerDomain, parent.getRefRatio(lev-1),
		      parent.getDx(lev), bc);
      // System.out.println("initData");
      newLevel.initData(initializer);
      Logger.barrier();
      coarserLevel.setFinerLevel(newLevel);
      coarserLevel = newLevel; // for next iteration
      // System.out.println("set finerLevel for level " + lev);
      // System.out.println("parent.setLevel");
      parent.setLevel(lev, newLevel);
      Logger.barrier(); 
      baNewCoarse = baNew;
    }
    parent.writeError();
  }


  /**
   * Assign boxes to procs.
   * The assignment is for the next finer level, not this one.
   */
  single public BoxArray single
    assignBoxes(
		SharedRegion single assignRegion, // REGION
		RectDomain<SPACE_DIM> [1d] boxes,
		RectDomain<SPACE_DIM> dProblemLevel) {

    BoxArray single baNew;

    // System.out.println("HSCLLevel.assignBoxes():  new tempRegion");
    SharedRegion single tempRegion = new SharedRegion(); // REGION
    { // scope for new objects using tempRegion
      int single numProcs = Ti.numProcs();
      RectDomain<1> single procTeam = [0 : numProcs - 1];

      Domain<1> [1d] procBoxes = new
	(tempRegion) // REGION
	Domain<1>[procTeam];

      RectDomain<1> boxindices = boxes.domain();

      int [1d] proclist;
      if (myProc == SPECIAL_PROC) {
	/*
	  Compute estimated load of each new patch.
	  The load associated with a box is taken to be the size of
	  box.accrete(2) * dProblemLevel
	              ^
		      2 ghost cells in each direction
	*/
	int [1d] load = new
	  (tempRegion) // REGION
	  int[boxindices];
	foreach (indbox in boxindices) {
	  load[indbox] = (boxes[indbox].accrete(2) * dProblemLevel).size();
	  // System.out.println("load" + indbox + " = " + load[indbox]);
	}
	proclist = LoadBalance.balance(load, numProcs);
      }

      Logger.barrier();
      Logger.append("balanced load");

      RectDomain<1> single boxinds = broadcast boxindices from SPECIAL_PROC;

      if (myProc != SPECIAL_PROC) proclist = new
				    (tempRegion) // REGION
				    int[boxinds];
      // proclistAll will be used in BoxArray, so don't put in tempRegion
      int [1d] single proclistAll = new
	(assignRegion) // REGION
	int[boxinds];
      foreach (indbox in boxinds)
	proclistAll[indbox] = broadcast proclist[indbox] from SPECIAL_PROC;

      // RectDomain<SPACE_DIM> single [1d] single boxes,
      // int [1d] single proclistAll
      baNew = new
	(assignRegion) // REGION
	BoxArray(boxes, proclistAll);
    }
    // System.out.println("HSCLLevel.assignBoxes():  deleting tempRegion");
    RegionUtil.delete(tempRegion); // REGION
    return baNew;
  }


  /**
   * Return a copy of UNew on the original grids.
   * Called from Amr.regrid().
   *
   * @param Ba new BoxArray at this level
   * @param BaCoarse new BoxArray at next coarser level
   * @param UTempCoarse (cell) old patches at next coarser level
   * @return (cell) old patches at this level
   */
  single public LevelArrayQuantities single
    regrid(
	   SharedRegion single newRegion, // REGION
	   BoxArray single Ba,
	   BoxArray single BaCoarse,
	   LevelArrayQuantities single UTempCoarse) {
    // System.out.println("Entering HSCLLevel.regrid() for level " + level);

    // Save existing UNew in UTemp.
    // Note that UNew.boxArray() isn't stored in a region.
    // System.out.println("Making UTemp");
    LevelArrayQuantities single UTemp = new
      (newRegion) // REGION
      LevelArrayQuantities(
			   newRegion, // REGION
			   UNew.boxArray());
    // System.out.println("Copying UTemp"); 
    UTemp.copy(UNew);
    // System.out.println("Copied UTemp");

    ba = Ba;
    baCoarse = BaCoarse;
    // System.out.println("Level " + level + " new coarse grids " + baCoarse);
    // System.out.println("Level " + level + " new fine grids " + ba);

    // discard previous UOld
    UOld = new
      (newRegion) // REGION
      LevelArrayQuantities(
			   newRegion, // REGION
			   ba);
    // LevelArrayQuantities single UTemp = UNew;  // save UNew
    UNew = new
      (newRegion) // REGION
      LevelArrayQuantities(
			   newRegion, // REGION
			   ba);
    fReg = ((boolean single) baCoarse.isNull()) ? null :
      new
      (newRegion) // REGION
      FluxRegister(
		   newRegion, // REGION
		   ba, baCoarse, dProblem, nRefineCoarser);
    myBoxes = ba.procboxes(myProc);

    // Special to HSCL.

    /*
      To update lg, you need only ba and baCoarse.
      But we also use the old PWLFillPatch object in lg to get new UNew.
      Why?  Because it contains the CoarseSlopes method, which is
      in PWLFillPatch because it uses adjacentCoarseBoxes.
    */
    // System.out.println("lg.regrid()");
    if (level == 0) {
      UNew = UTemp;
    } else {
      lg.regrid(UNew, UTemp, UTempCoarse);  // fill in UNew
    }

    // System.out.println("setting new lg");
    lg = new
      (newRegion) // REGION
      LevelGodunov(
		   newRegion, // REGION
		   ba, baCoarse, dProblem, nRefineCoarser, dx, bc);

    // System.out.println("Level " + level + " reassigning myRegion to newRegion");
    myRegion = newRegion; // REGION

    computePerimeter();

    // System.out.println("Done with HSCLLevel.regrid()");
    return UTemp;
  }


  /*
    output routines
  */

  /**
   * Write the whole hierarchy of levels to a file.
   * Call from level 0.
   */
  public void write(String outputfile) {
      // System.out.println("HSCLLevel.write():  new writeRegion");
    PrivateRegion writeRegion = new PrivateRegion(); // REGION

    { // scope for new objects using writeRegion
    /*
      Open output file.
    */

    PrintStream out;

    try {
      System.out.println("Opening file " + outputfile + " for output");
      OutputStream fout = new
	(writeRegion) // REGION
	FileOutputStream(outputfile);

      BufferedOutputStream bout = new
	(writeRegion) // REGION
        BufferedOutputStream(fout);

      out = new
	(writeRegion) // REGION
	PrintStream(bout);
    } catch (IOException x) {
      System.out.println("Error in opening " + outputfile);
      System.exit(0);
    }

    try {

      /*
	Given an output stream, we dump the following
	(1 per line):
	(1) Number of states (int)
	(2) Name of state (aString)
	(3) Number of dimensions (int)
	(4) Time (REAL)
	(5) Finest level written (int)
	(6) Loside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
	(7) Hiside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
	(8) Refinement ratio ((numLevels-1)*(int))
	(9) Domain of each level (numLevels* (BOX) )
	(10) Steps taken on each level (numLevels*(int) )
	(11) Grid spacing (numLevels*(BL_SPACEDIM*(int))) [\n at each level]
	(12) Coordinate flag (int)
	(13) Boundary info (int)
	(14) For each level,
             (a) level number and # of grids per level (2*int)
	     For each grid,
	         (1) Grid indices (BOX)
	         (2) Level (int)
                 (3) Steps taken on this grid (int)
                 (4) Time for this grid (REAL)
                 (5) Physical location of grid corners (BL_SPACEDIM*(REAL))
                 (6) writeOn data dump of each FAB of data on that grid
      */

      // System.out.println("Writing output");

      // (1) Number of states (int)
      out.println(Quantities.length() + 1);
      // also have level as a state

      // (2) Name of state (aString) [for each state]
      out.println("density");
      if (SPACE_DIM >= 1) out.println("x_velocity");
      if (SPACE_DIM >= 2) out.println("y_velocity");
      if (SPACE_DIM >= 3) out.println("z_velocity");
      out.println("pressure");
      out.println("level"); // should be here?

      // (3) Number of dimensions (int)
      out.println(SPACE_DIM);

      // (4) Time (REAL)
      double time = parent.getCumtime();
      out.println(time);

      // (5) Finest level written (int)
      int finest_level = parent.finestLevel();
      out.println(finest_level);

      // (6) Loside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
      double [1d][1d] ends = parent.getEndsNonsingle();
      double [1d] endsLow = ends[-1];
      out.println(doubleArrayString(endsLow));

      // (7) Hiside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
      out.println(doubleArrayString(ends[+1]));

      // (8) Refinement ratio ((numLevels-1)*(int))
      for (int h = 0; h <= finest_level - 1; h++) {
	out.print(parent.getRefRatio(h));
	out.print(' ');
      }
      out.println();

      // (9) Domain of each level (numLevels* (BOX) )
      Point<SPACE_DIM> extentLevel = parent.getExtent();
      out.print(domainString(extentDomain(extentLevel)));
      for (int h = 1; h <= finest_level; h++) {
	extentLevel = extentLevel * Point<SPACE_DIM>.all(parent.getRefRatio(h-1));
	out.print(domainString(extentDomain(extentLevel)));
      }
      out.println();

      // (10) Steps taken on each level (numLevels*(int) )
      for (int h = 0; h <= finest_level; h++) {
	out.print(parent.getLevelSteps(h));
	out.print(' ');
      }
      out.println();

      // (11) Grid spacing (numLevels*(BL_SPACEDIM*(real))) [at each level]
      double dxLevel = dx;
      for (int h = 0; h < finest_level; h++) {
	for (int dim = 1; dim <= SPACE_DIM; dim++) {
	  out.print(dxLevel);
	  out.print(' ');
	}
	out.println();
	dxLevel /= (double) parent.getRefRatio(h);
      }
      // and now finest level
      for (int dim = 1; dim <= SPACE_DIM; dim++) {
	out.print(dxLevel);
	out.print(' ');
      }
      out.println();

      // (12) Coordinate flag (int)
      out.println('0');  // 0 = cartesian

      // (13) Boundary info (int)
      out.println('0');  // 0 = no boundary info

      // System.out.println("Going through all processes...");

      for (int height = 0; height <= finest_level; height++) {

	// (14) For each level,

	// AmrLevel myLevel = parent.getLevel(height);
	AmrLevel myLevel = parent.getLevelNonsingle(height);
	// AmrLevel myLevel = amr_level[height];
	LevelArrayQuantities U = myLevel.getUNew();
	BoxArray ba = myLevel.getBa();
	dxLevel = myLevel.getDx();
	int steps = parent.getLevelSteps(height);

	// (a) level number and # of grids per level (2*int)

	RectDomain<1> boxIndices = ba.indices();
	out.print(height);
	out.print(' ');
	int totpatches = boxIndices.size();
	out.println(totpatches);

	System.out.println("Writing patches at level " + height + ":");
	foreach (indbox in boxIndices) {
	  // For each grid,
	  Quantities [SPACE_DIM d] gridData = U[indbox];
	  RectDomain<SPACE_DIM> box = ba[indbox];

	  System.out.print(" " + domainStringShort(box));

	  // (1) Grid indices (BOX)
	  out.println(domainString(box));

	  // (2) Level (int)
	  out.println(height);

	  // (3) Steps taken on this grid (int)
	  out.println(steps);  // Chombo plot files write 0

	  // (4) Time for this grid (REAL)
	  out.println(time);

	  // (5) Physical location of grid corners (BL_SPACEDIM*(REAL))
	  for (int dim = 1; dim <= SPACE_DIM; dim++) {
	    double loside = endsLow[dim] +
	      dxLevel * (double) box.min()[dim];
	    double hiside = endsLow[dim] + 
	      dxLevel * (double) (box.max()[dim] + 1);
	    out.print(loside);
	    out.print(' ');
	    out.println(hiside);
	  }

	  // (6) writeOn data dump of each FAB of data on that grid
	  myLevel.writeOn(out, gridData, height);

	} // foreach (indbox in boxIndices)
	System.out.println();
      } // for (int height = 0; height <= finest_level; height++)

      out.close();
      
    } catch (IOException x) {
      System.out.println("Error in writing to " + outputfile);
      System.exit(0);
    }
    } // scope for new objects using writeRegion
    // System.out.println("HSCLLevel.write():  deleting writeRegion");
    RegionUtil.delete(writeRegion); // REGION
  }


  /**
   * Write out data for a particular grid.
   */
  protected void writeOn(PrintStream out,
			 Quantities [SPACE_DIM d] gridData,
			 int height) {
      // System.out.println("HSCLLevel.writeOn():  new writeOnRegion");
    PrivateRegion writeOnRegion = new PrivateRegion(); // REGION
    { // scope for new objects using writeOnRegion
    RectDomain<SPACE_DIM> D = gridData.domain();
    String header = domainString(D) + " 1";
    Point<SPACE_DIM> Dmin = D.min(), Dmax = D.max();
    Quantities [SPACE_DIM d] primData = new
      (writeOnRegion) // REGION
      Quantities[D];
    lg.consToPrim(gridData, primData);

    try {
      for (int field = 0; field < Quantities.length(); field++) {

	out.println(header);

	Point<SPACE_DIM> p = Dmin;
	while (true) {
	  out.println(primData[p][field]);
	  int dim;
	  for (dim = 1; dim <= SPACE_DIM; dim++) {
	    Point<SPACE_DIM> unit = Point<SPACE_DIM>.direction(dim);
	    if (p[dim] + 1 <= Dmax[dim]) {
	      p = p + unit;
	      break;
	    } else {
	      // set p[dim] to Dmin[dim]
	      p = (Util.ONE - unit) * p + unit * Dmin[dim];
	      continue;
	    }
	  }
	  if (dim == SPACE_DIM + 1) break;
	}
      }

      // height field is constant
      out.println(header);
      foreach (p in D)
	out.println(height);

    } catch (IOException iox) {
      System.out.println("Error in writing patch with domain "+ D);
      System.exit(0);
    }
    } // scope for new objects using writeOnRegion
    // System.out.println("HSCLLevel.writeOn():  deleting writeOnRegion");
    RegionUtil.delete(writeOnRegion); // REGION
  }

  private static String domainString(RectDomain<SPACE_DIM> D) {
    String s = '(' + pointString(D.min()) +
      ' ' + pointString(D.max()) + 
      ' ' + pointString(Util.ZERO) + ')';
    return s;
  }

  private static String domainStringShort(RectDomain<SPACE_DIM> D) {
    // format [0:127, 0:31]
    Point<SPACE_DIM> Dmin = D.min(), Dmax = D.max();
    String s = "[" + Dmin[1] + ":" + Dmax[1];
    for (int dim = 2; dim <= SPACE_DIM; dim++) {
      s = s + ", " + Dmin[dim] + ':' + Dmax[dim];
    }
    s = s + ']';
    return s;
  }

  private static String pointString(Point<SPACE_DIM> p) {
    String s = "(" + p[1];
    for (int dim = 2; dim <= p.arity(); dim++) {
      s = s + "," + p[dim];
    }
    s = s + ')';
    return s;
  }

  private static String doubleArrayString(double [1d] arr) {
    String s = "" + arr[1];
    for (int dim = 2; dim <= arr.domain().size(); dim++) {
      s = s + " " + arr[dim];
    }
    return s;
  }

  private static RectDomain<SPACE_DIM> extentDomain(Point<SPACE_DIM> ext) {
    return [Util.ZERO : ext - Util.ONE];
  }


  /**
   * Write the whole hierarchy of levels to a file.
   * Call from level 0.
   */
  public void writeError(String outputfile) {
      // System.out.println("HSCLLevel.write():  new writeRegion");
    PrivateRegion writeRegion = new PrivateRegion(); // REGION

    { // scope for new objects using writeRegion
    /*
      Open output file.
    */

    PrintStream out;

    try {
      System.out.println("Opening file " + outputfile + " for output");
      OutputStream fout = new
	(writeRegion) // REGION
	FileOutputStream(outputfile);

      BufferedOutputStream bout = new
	(writeRegion) // REGION
        BufferedOutputStream(fout);

      out = new
	(writeRegion) // REGION
	PrintStream(bout);
    } catch (IOException x) {
      System.out.println("Error in opening " + outputfile);
      System.exit(0);
    }

    try {

      /*
	Given an output stream, we dump the following
	(1 per line):
	(1) Number of states (int)
	(2) Name of state (aString)
	(3) Number of dimensions (int)
	(4) Time (REAL)
	(5) Finest level written (int)
	(6) Loside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
	(7) Hiside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
	(8) Refinement ratio ((numLevels-1)*(int))
	(9) Domain of each level (numLevels* (BOX) )
	(10) Steps taken on each level (numLevels*(int) )
	(11) Grid spacing (numLevels*(BL_SPACEDIM*(int))) [\n at each level]
	(12) Coordinate flag (int)
	(13) Boundary info (int)
	(14) For each level,
             (a) level number and # of grids per level (2*int)
	     For each grid,
	         (1) Grid indices (BOX)
	         (2) Level (int)
                 (3) Steps taken on this grid (int)
                 (4) Time for this grid (REAL)
                 (5) Physical location of grid corners (BL_SPACEDIM*(REAL))
                 (6) writeOn data dump of each FAB of data on that grid
      */

      // System.out.println("Writing output");

      // (1) Number of states (int)
      out.println(2);  // truncation error before and after adjustment

      // (2) Name of state (aString) [for each state]
      out.println("error");
      out.println("error2");

      // (3) Number of dimensions (int)
      out.println(SPACE_DIM);

      // (4) Time (REAL)
      double time = parent.getCumtime();
      out.println(time);

      // (5) Finest level written (int)
      int finest_level = parent.finestLevel() - 1;
      // -1 because no error calculated at finest level
      out.println(finest_level);  

      // (6) Loside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
      double [1d][1d] ends = parent.getEndsNonsingle();
      double [1d] endsLow = ends[-1];
      out.println(doubleArrayString(endsLow));

      // (7) Hiside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
      out.println(doubleArrayString(ends[+1]));

      // (8) Refinement ratio ((numLevels-1)*(int))
      for (int h = 0; h <= finest_level - 1; h++) {
	out.print(parent.getRefRatio(h));
	out.print(' ');
      }
      out.println();

      // (9) Domain of each level (numLevels* (BOX) )
      Point<SPACE_DIM> extentLevel = parent.getExtent() / TWO;
      out.print(domainString(extentDomain(extentLevel)));
      for (int h = 1; h <= finest_level; h++) {
	extentLevel = (extentLevel * Point<SPACE_DIM>.all(parent.getRefRatio(h-1)));
	out.print(domainString(extentDomain(extentLevel)));
      }
      out.println();

      // (10) Steps taken on each level (numLevels*(int) )
      for (int h = 0; h <= finest_level; h++) {
	out.print(parent.getLevelSteps(h));
	out.print(' ');
      }
      out.println();

      // (11) Grid spacing (numLevels*(BL_SPACEDIM*(real))) [at each level]
      double dxLevel = dx * 2.0;  // multiply by TWO
      for (int h = 0; h < finest_level; h++) {
	for (int dim = 1; dim <= SPACE_DIM; dim++) {
	  out.print(dxLevel);
	  out.print(' ');
	}
	out.println();
	dxLevel /= (double) parent.getRefRatio(h);
      }
      // and now finest level
      for (int dim = 1; dim <= SPACE_DIM; dim++) {
	out.print(dxLevel);
	out.print(' ');
      }
      out.println();

      // (12) Coordinate flag (int)
      out.println('0');  // 0 = cartesian

      // (13) Boundary info (int)
      out.println('0');  // 0 = no boundary info

      // System.out.println("Going through all processes...");

      for (int height = 0; height <= finest_level; height++) {

	// (14) For each level,

	// AmrLevel myLevel = parent.getLevel(height);
	AmrLevel myLevel = parent.getLevelNonsingle(height);
	// AmrLevel myLevel = amr_level[height];
        LevelArraydouble err = myLevel.getTruncErrorCopied();
        LevelArraydouble err2 = myLevel.getTruncErrorCopied2();
	BoxArray ba = myLevel.getBa();
	dxLevel = myLevel.getDx() * 2.0;  // mult by TWO
	int steps = parent.getLevelSteps(height);

	// (a) level number and # of grids per level (2*int)

	RectDomain<1> boxIndices = ba.indices();
	out.print(height);
	out.print(' ');
	int totpatches = boxIndices.size();
	out.println(totpatches);

	System.out.println("Writing error patches at level " + height + ":");
	foreach (indbox in boxIndices) {
	  // For each grid,
	  double [SPACE_DIM d] errorData = err[indbox];
	  double [SPACE_DIM d] errorData2 = err2[indbox];
	  RectDomain<SPACE_DIM> box = ba[indbox] / TWO;

	  System.out.print(" " + domainStringShort(box));

	  // (1) Grid indices (BOX)
	  out.println(domainString(box));

	  // (2) Level (int)
	  out.println(height);

	  // (3) Steps taken on this grid (int)
	  out.println(steps);  // Chombo plot files write 0

	  // (4) Time for this grid (REAL)
	  out.println(time);

	  // (5) Physical location of grid corners (BL_SPACEDIM*(REAL))
	  for (int dim = 1; dim <= SPACE_DIM; dim++) {
	    double loside = endsLow[dim] +
	      dxLevel * (double) box.min()[dim];
	    double hiside = endsLow[dim] + 
	      dxLevel * (double) (box.max()[dim] + 1);
	    out.print(loside);
	    out.print(' ');
	    out.println(hiside);
	  }

	  // (6) writeOn data dump of each FAB of data on that grid
	  myLevel.writeErrorOn(out, errorData, height);
	  myLevel.writeErrorOn(out, errorData2, height);

	} // foreach (indbox in boxIndices)
	System.out.println();
      } // for (int height = 0; height <= finest_level; height++)

      out.close();
      
    } catch (IOException x) {
      System.out.println("Error in writing to " + outputfile);
      System.exit(0);
    }
    } // scope for new objects using writeRegion
    // System.out.println("HSCLLevel.write():  deleting writeRegion");
    RegionUtil.delete(writeRegion); // REGION
  }


  /**
   * Write out data for a particular grid.
   */
  protected void writeErrorOn(PrintStream out,
			 double [SPACE_DIM d] errorData,
			 int height) {
      // System.out.println("HSCLLevel.writeOn():  new writeOnRegion");
    PrivateRegion writeOnRegion = new PrivateRegion(); // REGION
    { // scope for new objects using writeOnRegion
    RectDomain<SPACE_DIM> D = errorData.domain();
    String header = domainString(D) + " 1";
    Point<SPACE_DIM> Dmin = D.min(), Dmax = D.max();

    try {

	out.println(header);

	Point<SPACE_DIM> p = Dmin;
	while (true) {
	  out.println(errorData[p]);
	  int dim;
	  for (dim = 1; dim <= SPACE_DIM; dim++) {
	    Point<SPACE_DIM> unit = Point<SPACE_DIM>.direction(dim);
	    if (p[dim] + 1 <= Dmax[dim]) {
	      p = p + unit;
	      break;
	    } else {
	      // set p[dim] to Dmin[dim]
	      p = (Util.ONE - unit) * p + unit * Dmin[dim];
	      continue;
	    }
	  }
	  if (dim == SPACE_DIM + 1) break;
	}

    } catch (IOException iox) {
      System.out.println("Error in writing patch with domain "+ D);
      System.exit(0);
    }
    } // scope for new objects using writeOnRegion
    // System.out.println("HSCLLevel.writeOn():  deleting writeOnRegion");
    RegionUtil.delete(writeOnRegion); // REGION
  }
}
