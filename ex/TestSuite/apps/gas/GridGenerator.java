/**
 * For internal use by MeshRefineD, which in turn is for internal use
 * by MeshRefine.
 * This class contains only uniprocessor code.
 * 
 * Usage:  GridGenerator object must be created in a region.
 * GridGenerator gg = new (Region myRegion) 
 *   GridGenerator(myRegion, Domain<SPACE_DIM> tagged, Domain<SPACE_DIM> pnd);
 * RectDomain<SPACE_DIM> [1d] grids = gg.makeGrids(myRegion);
 *
 * Given a set of tagged cells within a specified domain,
 * return an array of rectangles that cover the tagged cells
 * and are all contained in the domain.
 *
 * Input
 * Domain<SPACE_DIM> tagged:  tagged cells
 * Domain<SPACE_DIM> pnd:  proper nesting domain to contain all rectangles
 * tagged must be a subset of pnd.
 *
 * Output
 * RectDomain<SPACE_DIM> [1d] grids:  set of covering rectangles.
 *
 * Uses algorithm of Berger & Rigoutsos,
 * ``An Algorithm for Point Clustering and Grid Generation'',
 * IEEE Trans. Syst., Man, Cybern., vol. 21, pp. 1278--1286 (1991).
 *
 * @version  15 Dec 1999
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public class GridGenerator {

  /*
    constants
  */

  private static final int SPACE_DIM = Util.SPACE_DIM;

  /** minimum fraction of tagged cells for a rectangle to be accepted */
  private static double MIN_FILL_RATIO = 0.7;

  /** minimum inflection to split a rectangle */
  private static int MIN_INFLECTION = 3;

  /*
    non-constant fields
  */

  /** domain that must contain all rectangles returned */
  private Domain<SPACE_DIM> properDomain;

  /** domain of tagged cells */
  private Domain<SPACE_DIM> taggedDomain;

  /** list of rectangles to be returned */
  private BoxedList_RectDomain recsGood;

  private Region myRegion; // REGION

  /**
   * constructor
   */
  public GridGenerator(
		       Region MyRegion, // REGION
		       Domain<SPACE_DIM> tagged, Domain<SPACE_DIM> pnd) {
    myRegion = MyRegion; // REGION
    properDomain = pnd;
    taggedDomain = tagged;
    // System.out.println("tagged:  " + tagged);
    // System.out.println("There are " + tagged.size() + " tagged cells");
  }

  /*
    methods
  */


  /**
   * Return a set of rectangles covering taggedDomain,
   * all contained in properDomain.
   */
  public RectDomain<SPACE_DIM> [1d] makeGrids() {
    // Initialize recsGood with an empty list.
    // System.out.println("getting recsGood");
    recsGood = new
      (myRegion) // REGION
      BoxedList_RectDomain();

    // System.out.println("makeGridsRecur");
    makeGridsRecur(taggedDomain);

    // System.out.println("toArray");
    RectDomain<SPACE_DIM> [1d] recsArray = recsGood.toArray(
                                                    myRegion // REGION
                                                    );

    // Don't forget recsGood = new (myRegion) BoxedList_RectDomain();
    recsGood = null;
    // System.out.println("done with makeGrids");
    return recsArray;
  }


  private void makeGridsRecur(Domain<SPACE_DIM> tagged) {
    // System.out.println("get rec");
    RectDomain<SPACE_DIM> rec = tagged.boundingBox();
    // System.out.println("bounding box = " + rec);
    Point<SPACE_DIM> recMin = rec.min(), recMax = rec.max();

    // Check if rectangle is full enough.
    // System.out.println("fillRatio " + tagged.size() + " / " + rec.size());
    double fillRatio = (float) tagged.size() / (float) rec.size();

    // Need to check also that rectangle does not contain points outside
    // properDomain.
    if (fillRatio >= MIN_FILL_RATIO && (rec - properDomain).isNull()) {
      // Full enough.  We'll use it.
      // System.out.println("push " + rec + " ratio " + tagged.size() + " / " + rec.size() + " = " + fillRatio);
      recsGood.push(
		    myRegion, // REGION
		    rec);
      Point<SPACE_DIM> ext = recMax - recMin + Util.ONE;
    } else {
      // Compute signatures and then cut somewhere.
      // System.out.println("get sig");

      int [1d][1d] sig = signature(tagged);
      // System.out.println("cut");
      if (!cutAtHole(tagged, sig, recMin, recMax))
	if (!cutAtInflection(tagged, sig, recMin, recMax))
	  cutAtHalf(tagged, sig, recMin, recMax);
    }
  }

    
  /**
   * Look for a hole in the tagged domain.
   * Split at the first hole in the first dimension where you find one.
   * Return true if split at hole, false if not.
   */
  private boolean cutAtHole(Domain<SPACE_DIM> tagged, int [1d][1d] sig,
			    Point<SPACE_DIM> recMin, Point<SPACE_DIM> recMax) {
    foreach (pdim in Util.DIMENSIONS) {
      int dim = pdim[1];
      for (int hole = recMin[dim]; hole <= recMax[dim]; hole++) {
	if (sig[dim][hole] == 0) {
	  cut(tagged, dim, hole-1, hole+1);
	  return true;
	}
      }
    }
    return false;
  }


  /**
   * Look for a strong enough inflection point (set MIN_INFLECTION).
   * Split at the point of greatest inflection.
   * Return true if split at inflection point, false if not.
   */
  private boolean cutAtInflection(Domain<SPACE_DIM> tagged, int [1d][1d] sig,
				  Point<SPACE_DIM> recMin, Point<SPACE_DIM> recMax) {
    boolean [1d] existInfl = new
      (myRegion) // REGION
      boolean[Util.DIMENSIONS];
    boolean existInflAny = false;
    int [1d] bestLoc = new
      (myRegion) // REGION
      int[Util.DIMENSIONS];
    int [1d] bestVal = new
      (myRegion) // REGION
      int[Util.DIMENSIONS];
    foreach (pdim in Util.DIMENSIONS) {
      int dim = pdim[1];
      RectDomain<1> Ddiff = [[recMin[dim] + 1]: [recMax[dim] - 2]];
      existInfl[pdim] = false;
      if (!Ddiff.isNull()) {
	bestVal[pdim] = 0;
	int [1d] sigdim = sig[pdim];
	foreach (p in Ddiff) {
	  int diffLaplacian =
	    Math.abs(sigdim[p + [2]] - 3 * sigdim[p + [1]]
		     + 3 * sigdim[p] - sigdim[p - [1]]);
	  if (diffLaplacian >= MIN_INFLECTION) {
	    existInfl[pdim] = true;
	    existInflAny = true;
	    if (diffLaplacian > bestVal[pdim]) {
	      bestVal[pdim] = diffLaplacian;
	      bestLoc[pdim] = p[1];
	    }
	  }
	}
      }
    }
    if (existInflAny) {
      // Find best inflection point, and split there.
      int dimSplit = 1;
      for (int dim = 2; dim <= SPACE_DIM; dim++)
	if (existInfl[dim] && bestVal[dim] > bestVal[dimSplit])
	  dimSplit = dim;

      cut(tagged, dimSplit, bestLoc[dimSplit], bestLoc[dimSplit] + 1);
      return true;
    }
    return false;
  }


  /**
   * Find longest side, and split there.
   */
  private void cutAtHalf(Domain<SPACE_DIM> tagged, int [1d][1d] sig,
			 Point<SPACE_DIM> recMin, Point<SPACE_DIM> recMax) {
    Point<SPACE_DIM> lengths = recMax - recMin;
    int dimSplit = 1;
    for (int dim = 2; dim <= SPACE_DIM; dim++)
      if (lengths[dim] > lengths[dimSplit]) dimSplit = dim;
    int splitpoint = (recMin[dimSplit] + recMax[dimSplit]) / 2;

    cut(tagged, dimSplit, splitpoint, splitpoint + 1);
  }


  /**
   * Cut the domain tagged along the dimension dim,
   * with the first part ending at coordinate end1 and the second
   * part beginning at coordinate start2.
   */
  private void cut(Domain<SPACE_DIM> tagged, int dim, int end1, int start2) {

    Point<SPACE_DIM> tagMin = tagged.min(), tagMax = tagged.max();
    Point<SPACE_DIM> unit = Point<SPACE_DIM>.direction(dim);
    Point<SPACE_DIM> wtlim = Util.ONE - unit;

    Point<SPACE_DIM> pend1 = wtlim * tagMax + unit * end1;
    Point<SPACE_DIM> pstart2 = wtlim * tagMin + unit * start2;

    makeGridsRecur(tagged * [tagMin : pend1]);
    makeGridsRecur(tagged * [pstart2 : tagMax]);
  }


  /**
   * Compute signature in each dimension for the domain of tagged cells:
   * this is the number of tagged cells in each cross-section.
   */
  private int [1d][1d] signature(Domain<SPACE_DIM> tagged) {
    int [1d][1d] sig = new
      (myRegion) // REGION
      int[Util.DIMENSIONS][1d];
    // sig[1:SPACE_DIM] returns an array of int counting the number of
    // filled-in cells in each cross-section.

    Point<SPACE_DIM> tagMin = tagged.min();
    Point<SPACE_DIM> tagMax = tagged.max();
    foreach (pdim in Util.DIMENSIONS) {
      int dim = pdim[1];
      RectDomain<1> tagProj = [tagMin[dim] : tagMax[dim]];
      sig[dim] = new
	(myRegion) // REGION
	int[tagProj];
      foreach (p1 in tagProj) sig[dim][p1] = 0;
    }

    foreach (p in tagged) foreach (pdim in Util.DIMENSIONS) sig[pdim][p[pdim[1]]]++;

    return sig;
  }
}
