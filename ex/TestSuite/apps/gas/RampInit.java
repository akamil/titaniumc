public class RampInit extends Ramp implements InitialPatch {

  private static final int single SPACE_DIM = Util.SPACE_DIM;

  private static final int nsub = 10;  // number of subintervals
  private static final double dnsubinv = 1.0 / (double) nsub;
  private static final double rn2 = dnsubinv * dnsubinv;

  public void initData(RectDomain<SPACE_DIM> D, Quantities [SPACE_DIM d] U, double dx) {
    double subdx = dx * dnsubinv;
    double leftBase = ends[-1][LENGTH_DIM];
    double bottomBase = ends[-1][HEIGHT_DIM];
    foreach (p in D) {
      double left = leftBase + (double) p[LENGTH_DIM] * dx;
      double right = left + dx;
      double bottom = bottomBase + (double) p[HEIGHT_DIM] * dx;
      double top = bottom + dx;
   
      if (right <= shockLocBottom + bottom / slope) {
	U[p] = U1;
      } else if (left >= shockLocBottom + top / slope) {
	U[p] = U0;
      } else {
	int iwts = 0;
	for (int jsub = 1; jsub <= nsub; jsub++) {
	  for (int ksub = 1; ksub <= nsub; ksub++) {
	    double xsub = left + ((double) jsub - 0.5) * subdx;
	    double ysub = bottom + ((double) ksub - 0.5) * subdx;
	    if (xsub <= shockLocBottom + ysub / slope) iwts++;
	  }
	}
	double wt = (double) iwts * rn2;
	U[p] = U1 * wt + U0 * (1.0 - wt);
      }
    }
  }
}
