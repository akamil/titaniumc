import java.io.*;

public class ParmParseFileTokenizer extends ParmParseTokenizer {
    private InputStream input;

    /** 
     * Creates a stream tokenizer that parses the specified input
     * stream.
     * By default, it recognizes numbers, Strings quoted with 
     * single and double quotes, and all the alphabetics.
     * @param I the input stream 
     */
    public ParmParseFileTokenizer (InputStream I) {
        super();
	LINENO = 1;
	input = I;
    }

    protected int readChar() throws IOException {
      return input.read();   // InputStream is = input; is.read();
    }
}
