public class LevelArraydouble {
  private static final int single SPACE_DIM = Util.SPACE_DIM;

  private BoxArray single ba;
  private double [1d] single [SPACE_DIM d] arrs;

  /**
   * constructor
   */
  single public LevelArraydouble(
				 SharedRegion single reg, // REGION
				 BoxArray single Ba) {
    // set fields
    ba = Ba;
    RectDomain<1> single indices = (RectDomain<1> single) ba.indices();
    arrs = new
      (reg) // REGION
      double[indices][SPACE_DIM d];

    // process number stuff
    int myProc = Ti.thisProc();
    int single numProcs = Ti.numProcs();
    RectDomain<1> single Dprocs = [0 : numProcs-1];

    {
      // set up arrays of references to grids, and exchange references
      double [1d][SPACE_DIM d] arrlistloc = new
	(reg) // REGION
	double[indices][SPACE_DIM d];

      double [1d] single [1d][SPACE_DIM d] arrlist = new
	(reg) // REGION
	double[Dprocs][1d][SPACE_DIM d];

      arrlist.exchange(arrlistloc);

      // define new grids living on process myProc
      foreach (ibox in ba.procboxes(myProc))
	arrlist[myProc][ibox] = new
	(reg) // REGION
	double[ba[ibox]];

      Ti.barrier();

      // get references from the appropriate processes
      foreach (ibox in indices) arrs[ibox] = arrlist[ba.proc(ibox)][ibox];

      Ti.barrier();
    }
  }

  // methods

  public BoxArray single boxArray() { return ba; }

  public double [SPACE_DIM d] op[] (Point<1> p) { return arrs[p]; }

  public double [SPACE_DIM d] op[] (int ip) { return arrs[ip]; }

  public void op[]= (Point<1> p, double [SPACE_DIM d] myarr) { arrs[p] = myarr; }

  public void op[]= (int ip, double [SPACE_DIM d] myarr) { arrs[ip] = myarr; }

  public RectDomain<1> domain() { return ba.indices(); }
}
