/**
 * FluxRegister contains flux registers between a level and the
 * next coarser level.  The object lives on the finer level.
 *
 * @version  15 Dec 1999
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public class FluxRegister {

  /*
    constants
  */

  private static final int single SPACE_DIM = Util.SPACE_DIM;

  /** array of boxes at this level and at the next coarser level */
  private BoxArray single ba, baCoarse;

  /** physical domain */
  private RectDomain<SPACE_DIM> dProblem;

  /** registers indexed by [Util.DIMENSIONS][Util.DIRECTIONS] */
  private LevelArrayQuantities [1d][1d] registers;

  /**
   * map into _coarsened_ problem domain,
   * indexed by [Util.DIMENSIONS][Util.DIRECTIONS][indbox]
   */
  private Domain<SPACE_DIM> [1d][1d][1d] domains;

  /**
   * fineBoxes[dim][dir][indcoarsebox] is the set of indices of all
   * fine boxes that cover the given coarse box.
   * indexed by [Util.DIMENSIONS][Util.DIRECTIONS][indcoarsebox]
   */
  private Domain<1> [1d][1d][1d] fineBoxes;

  /** refinement ratio between the two levels */
  private int nRefine;

  /** refinement ratio between the two levels, as point */
  private Point<SPACE_DIM> nRefineP;

  /** refinement ratio between the two levels, as double */
  private double dRefine;

  private double wtFace;

  private SharedRegion single myRegion; // REGION

  /**
   * constructor
  */
  single public FluxRegister(
			     SharedRegion single MyRegion, // REGION
			     BoxArray single Ba,
			     BoxArray single BaCoarse,
			     RectDomain<SPACE_DIM> DProblem,
			     int NRefine) {
    myRegion = MyRegion; // REGION
    ba = Ba;
    baCoarse = BaCoarse;
    dProblem = DProblem;

    nRefine = NRefine;
    nRefineP = Point<SPACE_DIM>.all(nRefine);
    dRefine = (double) nRefine;

    wtFace = 1.0;
    for (int d = 1; d < SPACE_DIM; d++) wtFace /= dRefine;

    RectDomain<1> single inds = (RectDomain<1> single) ba.indices();
    RectDomain<1> single indsC = (RectDomain<1> single) baCoarse.indices();
    int [1d] proclist = ba.proclist();

    registers = new
      (myRegion) // REGION
      LevelArrayQuantities[Util.DIMENSIONS][Util.DIRECTIONS];

    domains = new
      (myRegion) // REGION
      Domain<SPACE_DIM>[Util.DIMENSIONS][Util.DIRECTIONS][inds];

    fineBoxes = new
      (myRegion) // REGION
      Domain<1>[Util.DIMENSIONS][Util.DIRECTIONS][indsC];

    // segmentsDimDir will be reused.
    RectDomain<SPACE_DIM> [1d] segmentsDimDir = new
      (myRegion) // REGION
      RectDomain<SPACE_DIM>[inds];

    foreach (pdim in Util.DIMENSIONS) foreach (pdir in Util.DIRECTIONS) {

      Domain<SPACE_DIM> [1d] domainsDimDir = domains[pdim][pdir];

      Domain<1> [1d] fineBoxesDimDir = fineBoxes[pdim][pdir];
      foreach (indboxC in indsC) fineBoxesDimDir[indboxC] = [0:-1];

      int dimdir = pdim[1] * pdir[1];

      foreach (indbox in inds) {
	// Find the coarse cells that border ba[indbox] in this direction.
	// Put these in the RectDomain<SPACE_DIM> segmentsDimDir[indbox].
	segmentsDimDir[indbox] = (ba[indbox].border(dimdir) * dProblem) /
	  nRefineP;

	// Copy segmentsdimDir[indbox] to the Domain<SPACE_DIM> domainsDimDir[indbox].
	// Later, these domains will be modified by removing segments that
	// are common to two different boxes.
	domainsDimDir[indbox] = segmentsDimDir[indbox];

	// If this domain intersects a coarse box, record the index.
	foreach (indboxC in indsC)
	  if (! (domainsDimDir[indbox] * baCoarse[indboxC]).isNull())
	    fineBoxesDimDir[indboxC] = fineBoxesDimDir[indboxC] +
	      [indbox:indbox];
      }

      BoxArray single baDimDir = new
	(myRegion) // REGION
	BoxArray(segmentsDimDir, proclist);
      registers[pdim][pdir] = new
	(myRegion) // REGION
	LevelArrayQuantities(
			     myRegion, // REGION
			     baDimDir);
    }

    /*
      Now trim domains:  Remove segments common to two different boxes.

      box:     indL   indR
                 |      |
                 v      |
            __________  v
           |          |L__      Initially...
           |         R|L  |
           |         R|L__|     L:  domains[1][+1][indL]
           |          |L        R:  domains[1][-1][indR]
           |__________|L

      Check the intersection (cells on the right):
      (domains[dim][-1][indR] + unit) * domains[dim][+1][indL]
      where
      unit = Point<SPACE_DIM>.direction(dim)
    */

    foreach (pdim in Util.DIMENSIONS) {
      Point<SPACE_DIM> unit = Point<SPACE_DIM>.direction(pdim[1]);
      Domain<SPACE_DIM> [1d][1d] domDim = domains[pdim];
      foreach (indR in inds) foreach (indL in inds) {
	Domain<SPACE_DIM> intersect = (domDim[-1][indR] + unit) * domDim[+1][indL];
	if (! intersect.isNull()) {
	  domDim[-1][indR] = domDim[-1][indR] - (intersect - unit);
	  domDim[+1][indL] = domDim[+1][indL] - intersect;
	}
      }
    }
  }

  // methods

  /**
   * Initialize registers to be zero.
   */
  public void clear() {
    Domain<1> myBoxes = ba.procboxes(Ti.thisProc());
    foreach (pdim in Util.DIMENSIONS) foreach (pdir in Util.DIRECTIONS)
      foreach (lba in myBoxes)
      registers[pdim][pdir][lba].set(Quantities.zero());
  }

  /**
   * Increment (from zero) the register with data from CoarseFlux,
   * which contains fluxes for baCoarse[CoarsePatchIndex].
   *
   * Recall that the flux registers live on the finer patches, so
   * you use fineBoxes to find the finer patches that are affected.
   */
  public void incrementCoarse(Quantities [SPACE_DIM d] CoarseFlux,
			      double Scale,
			      Point<1> CoarsePatchIndex,
			      int Dim,
			      Point<1> Dir) {
    int dimdir = Dim * Dir[1];
    if (! fineBoxes[Dim][Dir].domain().contains(CoarsePatchIndex) ) {
      System.out.println("fineboxes of " + dimdir + " has domain " +
			 fineBoxes[Dim][Dir].domain() + " and you want " +
			 CoarsePatchIndex);
    }
    Domain<1> myfineBoxes = fineBoxes[Dim][Dir][CoarsePatchIndex];
    // System.out.println("incrementCoarse myfineBoxes = " + myfineBoxes);
    // Recall that CoarseFlux is edge-centered.
    Point<SPACE_DIM> fluxOffset = Util.cell2edge[-dimdir];
    // For pC in flux register domain, use coarse flux at edge pC+fluxOff.
    foreach (indF in myfineBoxes) try {
      Quantities [SPACE_DIM d] myregs = registers[Dim][Dir][indF];
      Domain<SPACE_DIM> myDomain = domains[Dim][Dir][indF];
      foreach (pC in myDomain * baCoarse[CoarsePatchIndex])
	myregs[pC] = myregs[pC] + CoarseFlux[pC + fluxOffset] * Scale;
    } catch (Exception x) {
      System.out.println("incrementCoarse error in " + indF + " in " +
			 dimdir + " side of " + CoarseFlux.domain());
    }
  }


  /**
   * Increment the registers with data from FineFlux, which contains
   * flux for ba[FinePatchIndex].
   */
  public void incrementFine(Quantities [SPACE_DIM d] FineFlux,
			    double Scale,
			    Point<1> FinePatchIndex,
			    int Dim,
			    Point<1> Dir) {
    // FineFlux.domain() == edgeBox:  outer edges of (ba[lba]) in sweepDim
    int dimdir = Dim * Dir[1];
    double avgwtScale = Scale * wtFace;
    // myregs:  particular side of a particular box
    Quantities [SPACE_DIM d] myregs = registers[Dim][Dir][FinePatchIndex];
    // domains[][][]:  the cells in myregs.domain() that are actually used
    foreach (pC in domains[Dim][Dir][FinePatchIndex])
      try {
	foreach (eF in fineEdges(pC, dimdir))
	  myregs[pC] = myregs[pC] + FineFlux[eF] * avgwtScale;
	// Recall that FineFlux is edge-centered.
      } catch (Exception x) {
	System.out.println("incrementFine error at domains" +
			   [Dim] + "" + Dir + "" + FinePatchIndex + "" +
			   " at " + pC + " in " + myregs.domain() +
			   " with " + fineEdges(pC, dimdir) + " in " +
			   FineFlux.domain());
      }
  }


  /**
   * Return indices of finer edges adjacent to cellC,
   * where cellC is in direction Dir from these edges.
   * (This may sound backward.)
   * Called by incrementFine.
   */
  private RectDomain<SPACE_DIM> fineEdges(Point<SPACE_DIM> cellC, int Dir) {
    return (Util.subcells(cellC, nRefine).border(-Dir) +
	    Util.cell2edge[Dir]);
  }


  /**
   * Change the conserved quantities at the coarser level
   * in accordance with flux registers.
   */
  public void reflux(LevelArrayQuantities UCoarse) {
    Domain<1> myBoxesC = baCoarse.procboxes(Ti.thisProc());
    foreach (pdim in Util.DIMENSIONS) foreach (pdir in Util.DIRECTIONS)
      foreach (lbaC in myBoxesC) foreach (lba in fineBoxes[pdim][pdir][lbaC]) 
      foreach (pC in (domains[pdim][pdir][lba] * baCoarse[lbaC]))
      UCoarse[lbaC][pC] = UCoarse[lbaC][pC] + registers[pdim][pdir][lba][pC];
  }
}
