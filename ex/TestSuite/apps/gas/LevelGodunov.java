/**
 * LevelGodunov implements Godunov method at one level.
 *
 * @version  7 Aug 2000
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public class LevelGodunov {

  /*
    constants
  */

  private static final int single SPACE_DIM = Util.SPACE_DIM;

  private static final int DENSITY=0, VELOCITY_X=1, VELOCITY_Y=2, PRESSURE=3;

  /** relative tolerance used in Riemann solver */
  private static final double RELATIVE_TOLERANCE = 1e-8;

  private static double GAMMA = 1.4;

  /** threshold for density */
  private static double MIN_DENSITY = 1e-6;

  /** threshold for pressure */
  private static double MIN_PRESSURE = 1e-6;

  /** coefficient of artificial viscosity */
  private static double ART_VISC_COEFF = 0.1;

  /*
    fields that are arguments to constructor
  */

  private SharedRegion single myRegion; // REGION

  /** array of boxes at this level and at the next coarser level */
  private BoxArray single ba, baCoarse;

  /** physical domain */
  private RectDomain<SPACE_DIM> dProblem;

  /** refinement ratio between this level and next coarser level */
  private int single nRefine;

  /** mesh spacing */
  private double single dx;

  private PhysBC bc;

  /*
    other fields that are set in constructor
  */

  private int innerRadius, outerRadius, flapSize;

  private int nCons, nPrim, nSlopes, nFluxes;

  private double single dRefine;

  /** edges at ends of physical domain in each dimension */
  private RectDomain<SPACE_DIM> [1d] dProblemEdges;

  /**
   * dProblemAbutters[dim1][dim2][dir] contains the edges in dimension dim2
   * that abut the face dir*dim1 of the physical domain.
   * indexed by [Util.DIMENSIONS][Util.DIMENSIONS][Util.DIRECTIONS]
   */
  private RectDomain<SPACE_DIM> [1d][1d][1d] dProblemAbutters;

  private PWLFillPatch fp;

  private LevequeCorrection lc;


  /**
   * constructor
   */
  single public LevelGodunov(
			     SharedRegion single MyRegion, // REGION
		      BoxArray single Ba,
		      BoxArray single BaCoarse,
		      RectDomain<SPACE_DIM> DProblem,
		      int single NRefine,
		      double single Dx,
		      PhysBC BC) {
    myRegion = MyRegion; // REGION
    innerRadius = 1;
    outerRadius = 3;
    flapSize = outerRadius;
    nCons = SPACE_DIM + 2;
    nFluxes = nCons;
    nPrim = SPACE_DIM + 3;
    nSlopes = nCons;
    nRefine = NRefine;
    dRefine = (double) nRefine;
    dProblem = DProblem;
    dProblemEdges = new
      (myRegion) // REGION
      RectDomain<SPACE_DIM>[Util.DIMENSIONS];
    dProblemAbutters = new
      (myRegion) // REGION
      RectDomain<SPACE_DIM>[Util.DIMENSIONS][Util.DIMENSIONS][Util.DIRECTIONS];

    foreach (pdim in Util.DIMENSIONS) {
      int dim = pdim[1];
      Point<SPACE_DIM> lower = dProblem.min() + Util.cell2edge[-dim];
      Point<SPACE_DIM> upper = dProblem.max() + Util.cell2edge[+dim];
      Point<SPACE_DIM> stride = Point<SPACE_DIM>.direction(dim, upper[dim] - lower[dim] - 1) +
	Util.ONE;
      dProblemEdges[dim] = [lower : upper : stride];
      foreach (pdir in Util.DIRECTIONS) {
	RectDomain<SPACE_DIM> faceCells = dProblem.border(1, pdir[1] * dim, 0);
	// foreach (pdimOther in Util.DIMENSIONS - [pdim:pdim])
	foreach (pdimOther in Util.DIMENSIONS) if (pdimOther != pdim)
	  dProblemAbutters[pdim][pdimOther][pdir] =
	  Util.innerEdges(faceCells, pdimOther[1]);
      }
    }

    dx = Dx;
    bc = BC;
    ba = Ba;
    baCoarse = BaCoarse;

    fp = new
      (myRegion) // REGION
      PWLFillPatch(
		   myRegion, // REGION
		   ba, baCoarse, dProblem, nRefine, innerRadius, outerRadius);

    lc = new
      (myRegion) // REGION
      LevequeCorrection(
			myRegion, // REGION
			ba, dProblem, dx, nCons);
  }

  /*
    methods
  */
  
  single public void stepSymmetric(LevelArrayQuantities single U,
				   LevelArrayQuantities single UCOld,
				   LevelArrayQuantities single UCNew,
				   double single Time,
				   double single TCOld,
				   double single TCNew,
				   double single Dt) {
    SharedRegion single tempRegion = new SharedRegion(); // REGION
    { // scope for new objects using tempRegion
      // Copy U to UTemp

      LevelArrayQuantities single UTemp = new
	(tempRegion) // REGION
	LevelArrayQuantities(
			     tempRegion, // REGION
			     U.boxArray());

      int myProc = Ti.thisProc();
      Domain<1> myBoxes = ba.procboxes(myProc);

      foreach (lba in myBoxes) UTemp[lba].copy(U[lba]);

      // Do step with dimension 1 first.
      step(UTemp, UCOld, UCNew, Time, TCOld, TCNew, Dt, null, null, true, false, false);

      // Do step with dimension 1 last.
      step(U, UCOld, UCNew, Time, TCOld, TCNew, Dt, null, null, false, false, false);

      // Average the two.
      foreach (lba in myBoxes) foreach (p in U[lba].domain())
	U[lba][p] = (U[lba][p] + UTemp[lba][p]) * 0.5;
    }
  }


  /**
   * One step of symmetrized Godunov method, from Time to Time + Dt,
   * TCOld <= Time < Time + Dt <= TCNew
   * Called from HSCLLevel.tag()
   *
   * @param U (cell) conserved variables at this level, input at Time.
   * @param UCOld (cell) conserved variables, next coarser level, at TCOld
   * @param UCNew (cell) conserved variables, next coarser level, at TCNew
   * @param dUdt (cell) estimate of dU/dt over the interval
   */
  single public void stepSymmetricDiff(LevelArrayQuantities single dUdt,
				       LevelArrayQuantities single U,
				       LevelArrayQuantities single UCOld,
				       LevelArrayQuantities single UCNew,
				       double single Time,
				       double single TCOld,
				       double single TCNew,
				       double single Dt) {
    /*
    System.out.println("stepSymmetricDiff Time = " + Time +
		       ", TCOld = " + TCOld + 
		       ", TCNew = " + TCNew + 
		       ", Dt = " + Dt);
    */
    SharedRegion single tempRegion = new SharedRegion(); // REGION
    { // scope for new objects using tempRegion
      int myProc = Ti.thisProc();
      Domain<1> myBoxes = ba.procboxes(myProc);

      // Copy U to UTemp1
      LevelArrayQuantities single UTemp1 = new
	(tempRegion) // REGION
	LevelArrayQuantities(
			     tempRegion, // REGION
			     U.boxArray());
      foreach (lba in myBoxes) UTemp1[lba].copy(U[lba]);

      // Copy U to UTemp2
      LevelArrayQuantities single UTemp2 = new
	(tempRegion) // REGION
	LevelArrayQuantities(
			     tempRegion, // REGION
			     U.boxArray());
      foreach (lba in myBoxes) UTemp2[lba].copy(U[lba]);

      /*
	{ // diagnostic
	LevelArraydouble single myNorm = new
	  (tempRegion) // REGION
	  LevelArraydouble(
			   tempRegion, // REGION
			   U.boxArray());

	foreach (lba in myBoxes) foreach (p in U[lba].domain())
	    myNorm[lba][p] = UTemp1[lba][p].norm();

	double myNormLocal = 0.0;
	foreach (lba in myBoxes) foreach (p in U[lba].domain())
	  if (myNorm[lba][p] > myNormLocal)
	    myNormLocal = myNorm[lba][p];
	System.out.println("myNormLocal for initial UTemp1 = " + myNormLocal);
	}

	{ // diagnostic
	LevelArraydouble single myNorm = new
	  (tempRegion) // REGION
	  LevelArraydouble(
			   tempRegion, // REGION
			   U.boxArray());

	foreach (lba in myBoxes) foreach (p in U[lba].domain())
	    myNorm[lba][p] = UTemp2[lba][p].norm();

	double myNormLocal = 0.0;
	foreach (lba in myBoxes) foreach (p in U[lba].domain())
	  if (myNorm[lba][p] > myNormLocal)
	    myNormLocal = myNorm[lba][p];
	System.out.println("myNormLocal for initial UTemp2 = " + myNormLocal);
	}
      */

      // No flux registers, no 4th-order slopes, no flattening.

      // Do step with dimension 1 first.
      step(UTemp1, UCOld, UCNew, Time, TCOld, TCNew, Dt, null, null,
	   true, false, false);

      // Do step with dimension 1 last.
      step(UTemp2, UCOld, UCNew, Time, TCOld, TCNew, Dt, null, null,
	   false, false, false);


      /*
	{ // diagnostic
	LevelArraydouble single myNorm = new
	  (tempRegion) // REGION
	  LevelArraydouble(
			   tempRegion, // REGION
			   U.boxArray());

	foreach (lba in myBoxes) foreach (p in U[lba].domain()) {
	  myNorm[lba][p] = UTemp1[lba][p].norm();
	  // System.out.println("final UTemp1" + lba + "" + p + " = " + UTemp1[lba][p]);
	} // almost all NaN for level == 1

	double myNormLocal = 0.0;
	foreach (lba in myBoxes) foreach (p in U[lba].domain())
	  if (myNorm[lba][p] > myNormLocal)
	    myNormLocal = myNorm[lba][p];
	System.out.println("myNormLocal for final UTemp1 = " + myNormLocal);
	}

	{ // diagnostic
	LevelArraydouble single myNorm = new
	  (tempRegion) // REGION
	  LevelArraydouble(
			   tempRegion, // REGION
			   U.boxArray());

	foreach (lba in myBoxes) foreach (p in U[lba].domain()) {
	  myNorm[lba][p] = UTemp2[lba][p].norm();
	  // System.out.println("final UTemp2" + lba + "" + p + " = " + UTemp2[lba][p]);
	} // almost all NaN for level == 1

	double myNormLocal = 0.0;
	foreach (lba in myBoxes) foreach (p in U[lba].domain())
	  if (myNorm[lba][p] > myNormLocal)
	    myNormLocal = myNorm[lba][p];
	System.out.println("myNormLocal for final UTemp2 = " + myNormLocal);
	}
      */

      // Average the two, then take the difference between that
      // and the original, and divide by Dt.
      foreach (lba in myBoxes) foreach (p in U[lba].domain())
	dUdt[lba][p] =
	((UTemp1[lba][p] + UTemp2[lba][p]) * 0.5 - U[lba][p]) / Dt;


      /*
	{ // diagnostic
	LevelArraydouble single myNorm = new
	  (tempRegion) // REGION
	  LevelArraydouble(
			   tempRegion, // REGION
			   U.boxArray());

	foreach (lba in myBoxes) foreach (p in U[lba].domain())
	    myNorm[lba][p] = dUdt[lba][p].norm();

	double myNormLocal = 0.0;
	foreach (lba in myBoxes) foreach (p in U[lba].domain())
	  if (myNorm[lba][p] > myNormLocal)
	    myNormLocal = myNorm[lba][p];
	System.out.println("myNormLocal for our dUdt = " + myNormLocal);
	}
      */

    }
    RegionUtil.delete(tempRegion); // REGION
  }


  /**
   * One step of Godunov method, from Time to Time + Dt.
   * TCOld <= Time < Time + Dt <= TCNew
   * Called from HSCLLevel.advance()
   *
   * @param U (cell) conserved variables at this level;
   *                 input at Time, output at Time + Dt.
   * @param UCOld (cell) conserved variables, next coarser level, at TCOld
   * @param UCNew (cell) conserved variables, next coarser level, at TCNew
   * @param Fr flux register between this level and next coarser level; modified
   * @param FrFine flux register between this level and next finer level; modified
   * @param EvenParity true if dimension 1 first, false if dimension 1 last
   */
  single public double single step(LevelArrayQuantities single U,
				   LevelArrayQuantities single UCOld,
				   LevelArrayQuantities single UCNew,
				   double single Time,
				   double single TCOld,
				   double single TCNew,
				   double single Dt,
				   FluxRegister Fr,
				   FluxRegister FrFine,
				   boolean single EvenParity,
				   boolean single doFourth,
				   boolean single doFlattening) {
    Logger.append("LevelGodunov step dx=" + dx);
    // System.out.println("In lg.step()");
    double single cfl = Dt / estTimeStep(U);
    // System.out.println("lg.step() got cfl");

    SharedRegion single tempRegion = new SharedRegion(); // REGION
    { // scope for new objects using tempRegion
    int myProc = Ti.thisProc();
    Domain<1> myBoxes = ba.procboxes(myProc);
    RectDomain<1> allBoxes = ba.indices();

    // System.out.println("step TCOld = " + TCOld + ", Time = " + Time + ", TCNew = " + TCNew + ", Dt = " + Dt + ", UCOld array = " + UCOld.boxArray());
    // U.checkNaN("step U");

    // System.out.println("*** " + myProc + " has boxes " + myBoxes +
    // " taking time " + Dt);

    /*
      Compute coefficients of artificial viscosity.
    */

    LevelArraydouble [1d] single difCoefs =
      (LevelArraydouble [1d] single) new
      (tempRegion) // REGION
      LevelArraydouble[Util.DIMENSIONS];

    foreach (pdim in Util.DIMENSIONS) {
      RectDomain<SPACE_DIM> [1d] single boxesVisc =
	(RectDomain<SPACE_DIM> [1d] single) new
	(tempRegion) // REGION
	RectDomain<SPACE_DIM>[allBoxes];

      foreach (lba in allBoxes)
	boxesVisc[lba] = Util.outerEdges(ba[lba], pdim[1]);

      Logger.barrier();

      BoxArray single baVisc = new
	(tempRegion) // REGION
	BoxArray(boxesVisc, ba.proclist());

      // difCoefs[dimVisc][lba]:  outerEdges(ba[lba], dimVisc)

      difCoefs[pdim] = new
	(tempRegion) // REGION
	LevelArraydouble(
			 tempRegion, // REGION
			 baVisc);
    }

    LevelArrayQuantities single UTemp = new
      (tempRegion) // REGION
      LevelArrayQuantities(
			   tempRegion, // REGION
			   U.boxArray());

    lc.clear();  // need this before lc is used
    if (FrFine != null) FrFine.clear();

    for (int single passDim = 1; passDim <= SPACE_DIM; passDim++) {

      // Use Strang alternation of ordering of fractional steps
      // to preserve second-order accuracy in time.
      int single sweepDim = EvenParity ? passDim : (SPACE_DIM+1) - passDim;

      /*
	Table shows sweepDim.    passDim
	                          1 2
	EvenParity == true:       1 2
	EvenParity == false:      2 1
      */

      Logger.barrier();
      // Loop through grids.  Not single!

      foreach (lba in myBoxes) {

	// System.out.println("****** " + myProc + " Look number " + passDim + " in dimension " + sweepDim + " at the box " + ba[lba]);
	// Form single patch variable, and fill it with data.

	RectDomain<SPACE_DIM> stateBox = dProblem * fp.innerBox(lba)
	  .accrete(flapSize, +sweepDim)
	  .accrete(flapSize, -sweepDim);

	// stateBox:  ba[lba] + 1 (isotropic) + 3 (sweepDim)

	Quantities [SPACE_DIM d] state = new
	  (tempRegion) // REGION
	  Quantities[stateBox];

	/*
	  Fill state with data from U as much as possible.
	  Then within inner radius, interpolate from UCOld and UCNew.
	  Some cells within outer radius may be left unfilled.
	*/
	try {
	  fp.fill(U, UCOld, UCNew, state, lba, Time, TCOld, TCNew);
	} catch (Exception x) {
	  System.out.println(myProc + " Error in fp.fill()");
	  System.exit(0);
	}
	// System.out.println(myProc + " Done fp.fill");

	/*
	  Apply correction to the state in the first row of ghost cells 
	  adjacent to ba[lba] in all of the normal directions
	  that have not yet been updated.  (LevequeCorrection keeps
	  track of that information internally.)
	 */
	try {
	  lc.correctBoundary(state, lba, sweepDim);
	} catch (Exception x) {
	  System.out.println(myProc +
			     " Error in lc.correctBoundary() for box " +
			     ba[lba] + " in dim " + sweepDim);
	  System.exit(0);
	}
	// System.out.println(myProc + " Done lc.correctBoundary");

	/*
	  Fill the unfilled cells (within outer radius) of state by copying.
	*/
	try {
	  fp.extend(state, lba, sweepDim, flapSize);
	} catch (Exception x) {
	  System.out.println(myProc + " Error in fp.extend()");
	  System.exit(0);
	}
	// System.out.println(myProc + " Done fp.extend");

	/*
	  If this is first time through the dimension loop,
	  form artificial viscosity coefficients for all dimensions.
	*/

	if (passDim == 1)
	  for (int dimVisc = 1; dimVisc <= SPACE_DIM; dimVisc++)
	    artVisc(state, difCoefs[dimVisc][lba], dimVisc);
	
	// difCoefs[dimVisc][lba]:  outerEdges(ba[lba], dimVisc)

	// System.out.println(myProc + " Done artVisc");

	/*
	  Compute primitive variables.
	*/

	RectDomain<SPACE_DIM> primBox = dProblem * ba[lba]
	  .accrete(flapSize + 1, +sweepDim)
	  .accrete(flapSize + 1, -sweepDim);

	// primBox:  ba[lba] + (3+1) (sweepDim)

	Quantities [SPACE_DIM d] q = new
	  (tempRegion) // REGION
	  Quantities[primBox];

	// System.out.println(myProc + " About to do consToPrim");
	try {
	  consToPrim(state, q);
	} catch (Exception x) {
	  System.out.println("Error in consToPrim()");
	  System.exit(0);
	}

	/*
	  Compute slopes.
	*/

	RectDomain<SPACE_DIM> slopeBox = dProblem * ba[lba]
	  .accrete(1, +sweepDim)
	  .accrete(1, -sweepDim);

	// slopeBox:  ba[lba] + 1 (sweepDim)

	Quantities [SPACE_DIM d] dq = new
	  (tempRegion) // REGION
	  Quantities[slopeBox];

	// q on primBox:  ba[lba] + (3+1) (sweepDim)
	// dq on slopeBox:  ba[lba] + 1 (sweepDim)
	try {
	  slope(
		tempRegion, // REGION
		q, dq, sweepDim, doFourth, doFlattening);
	} catch (Exception x) {
	  System.out.println("Error in slope()");
	  System.exit(0);
	}
	// System.out.println(myProc + " Done slope " + slopeBox);

	/*
	  Compute predictor step to obtain extrapolated edge values.
	*/

	Quantities [SPACE_DIM d] qlo = new
	  (tempRegion) // REGION
	  Quantities[slopeBox];
	Quantities [SPACE_DIM d] qhi = new
	  (tempRegion) // REGION
	  Quantities[slopeBox];

	try {
	  normalPred(q, dq, qlo, qhi, Dt / dx, sweepDim);
	} catch (Exception x) {
	  System.out.println("Error in normalPred()");
	  System.exit(0);
	}
	// System.out.println(myProc + " Done normalPred");

	/*
	  Shift qlo, qhi to edges, and call Riemann solver on interior edges.
	*/

	RectDomain<SPACE_DIM> riemannBox = Util.innerEdges(slopeBox, sweepDim);
	RectDomain<SPACE_DIM> edgeBox = Util.outerEdges(ba[lba], sweepDim);

	// riemannBox:  inner edges of (ba[lba] + 1 (sweepDim)) in sweepDim
	// edgeBox:  outer edges of (ba[lba]) in sweepDim
	// So riemannBox should be the same as edgeBox unless
	// ba[lba] has a physical boundary in sweepDim.

	Quantities [SPACE_DIM d] qGdnv = new
	  (tempRegion) // REGION
	  Quantities[riemannBox];
	try {
	  // (qlo.translate(t))[p + t] == qlo[p];
	  // (qlo.translate(t)) has domain qlo.domain() + t
	  // On left is qhi[-1/2], on right is qlo[+1/2].
	  riemann(qhi.translate(Util.cell2edge[+sweepDim]),
		  qlo.translate(Util.cell2edge[-sweepDim]),
		  qGdnv, sweepDim);
	  // call with qhi[e + uce[-sdim]], qlo[e + uce[+sdim]]
	} catch (Exception x) {
	  System.out.println("Error in riemann()");
	  System.exit(0);
	}
	// System.out.println(myProc + " Done riemann");

	/*
	  Compute fluxes, including effect of artificial viscosity and
	  boundary conditions.
	*/

	// edgeBox:  outer edges of (ba[lba]) in sweepDim
	Quantities [SPACE_DIM d] flux = new
	  (tempRegion) // REGION
	  Quantities[edgeBox];
	try {
	  fluxCalc(qGdnv, state, difCoefs[sweepDim][lba], flux, sweepDim);
	} catch (Exception x) {
	  System.out.println("Error in fluxCalc()");
	  System.exit(0);
	}
	// System.out.println(myProc + " Done fluxCalc");

	/*
	  Perform conservative update, incrementing LevequeCorrection
	  object.

	  The intermediate update is stored into UTemp, so as not to
	  write over data in U that might be required as ghost cell
	  data for other grid updates.
	*/

	try {
	  // get original U, copy into UTemp.
	  UTemp[lba].copy(U[lba]);
	} catch (Exception x) {
	  System.out.println("Error in copying");
	  System.exit(0);
	}

	try {
	  update(flux, UTemp[lba], Dt / dx, sweepDim);
	} catch (Exception x) {
	  System.out.println("Error in update()");
	  System.exit(0);
	}
	// System.out.println(myProc + " Done update");

	try {
	  lc.increment(flux, Dt, lba, sweepDim);
	} catch (Exception x) {
	  System.out.println("Error in lc.increment()");
	  System.exit(0);
	}
	// System.out.println(myProc + " Done lc.increment");

	/*
	  Increment flux registers in this dimension
	  for interface with next coarser level, if any.
	*/

	// System.out.println("Calling Fr.incrementFine for box " + ba[lba]);
	if (Fr != null)
	  foreach (pdir in Util.DIRECTIONS)
	    try {
	      // flux.domain() == edgeBox: outer edges of (ba[lba]) in sweepDim
	      Fr.incrementFine(flux, +pdir[1]*Dt/(dx*dRefine), lba, sweepDim, pdir);
	  } catch (Exception x) {
	    System.out.println("Error in Fr.incrementFine() " +
			       " for " + U[lba].domain() +
			       " in dir " + pdir[1]);
	    System.exit(0);
	  }

	// System.out.println(myProc + " Done Fr.incrementFine");

	/*
	  Increment (actually, fill in) flux registers in this dimension
	  for interface with next finer level, if any.
	*/

	// coarse:  fill in flux registers in this dimension

	if (FrFine != null)
	  foreach (pdir in Util.DIRECTIONS)
	    try {
	      FrFine.incrementCoarse(flux, -pdir[1]*Dt/dx, lba, sweepDim,pdir);
	    } catch (Exception x) {
	      System.out.println("Error in FrFine.incrementCoarse()");
	      System.exit(0);
	    }

	// System.out.println(myProc + " Done FrFine.incrementCoarse");

      } // end of loop over grids

      // System.out.println(myProc + " reached the end in dim " + sweepDim);
      Logger.barrier();

      try {
	foreach (lba in myBoxes) U[lba].copy(UTemp[lba]);
      } catch (Exception x) {
	System.out.println(myProc + " Error in copying UTemp to U");
	System.exit(0);
      }
      // System.out.println(myProc + " copied UTemp to U");

      Logger.barrier();

    } // end of loop over sweep dimensions

    // System.out.println(myProc + " ---------- done lg.step");
    Logger.append("Done LevelGodunov step dx=" + dx);
    } // scope for new objects using tempRegion
    RegionUtil.delete(tempRegion); // REGION
    return cfl;
  }

  
  /**
   * Get coefficients of artificial viscosity.
   */
  private void artVisc(Quantities [SPACE_DIM d] state,
		       double [SPACE_DIM d] difCoefs,
		       int dimVisc) {
    // for (int dimVisc = 1; dimVisc <= SPACE_DIM; dimVisc++)
    // artVisc(state, difCoefs[dimVisc][lba], dimVisc);
    // difCoefs[dimVisc][lba]:  outerEdges(ba[lba], dimVisc)
    // stateBox:  ba[lba] + 1 (isotropic) + 3 (sweepDim)
    RectDomain<SPACE_DIM> stateBox = state.domain();
    Quantities [SPACE_DIM d] statePrim = new
      (myRegion) // REGION
      Quantities[stateBox];
    consToPrim(state, statePrim);
    RectDomain<SPACE_DIM> outerE = difCoefs.domain();

    /*
      Normal component of divergence:
      calculate on all edges except on physical boundary.
    */
    int VELOCITY_N = velocityIndex(dimVisc);
    // foreach (e in outerE - dProblemEdges[dimVisc])
    foreach (e in outerE) if (! dProblemEdges[dimVisc].contains(e))
      difCoefs[e] = ART_VISC_COEFF *
      ((statePrim[e + Util.edge2cell[+dimVisc]][VELOCITY_N] -
	statePrim[e + Util.edge2cell[-dimVisc]][VELOCITY_N]));

    /*
      Tangential components of divergence:
      calculate on all edges except on physical boundary.
      Use one-sided differences if edge abuts physical boundary.
    */

    // foreach (pdimOther in Util.DIMENSIONS - [dimVisc:dimVisc])
    foreach (pdimOther in Util.DIMENSIONS) if (pdimOther[1] != dimVisc) {
      int VELOCITY_T = velocityIndex(pdimOther);
      Point<SPACE_DIM> unitT = Point<SPACE_DIM>.direction(pdimOther[1]);

      // abutters[dir]:  edges (in dimension dimVisc) that abut the side
      // dir*dimOther of the physical domain.
      RectDomain<SPACE_DIM> [1d] abutters = dProblemAbutters[pdimOther][dimVisc];
      
      /*
	Aliased versions of statePrim:
	-|----|----|-
	 |    |    |
	 | LU | RU |
	 |    |    |
	-|----|----|-
	 |    |    |
         | L  e  R |
	 |    |    |
	-|----|----|-
	 |    |    |
	 | LD | RD |
	 |    |    |
	-|----|----|-
      */
      Quantities [SPACE_DIM d] stateR =
	statePrim.translate(Util.ZERO - Util.edge2cell[+dimVisc]);
      Quantities [SPACE_DIM d] stateRU =
	stateR.translate(Util.ZERO - unitT);
      Quantities [SPACE_DIM d] stateRD =
	stateR.translate(Util.ZERO + unitT);
      Quantities [SPACE_DIM d] stateL =
	statePrim.translate(Util.ZERO - Util.edge2cell[-dimVisc]);
      Quantities [SPACE_DIM d] stateLU =
	stateL.translate(Util.ZERO - unitT);
      Quantities [SPACE_DIM d] stateLD =
	stateL.translate(Util.ZERO + unitT);

      // foreach (e in outerE -
      // (dProblemEdges[dimVisc] + abutters[-1] + abutters[+1]))
      foreach (e in outerE)
	if (! dProblemEdges[dimVisc].contains(e))
	  if (! abutters[-1].contains(e))
	    if (! abutters[+1].contains(e))
	      difCoefs[e] = difCoefs[e] + (ART_VISC_COEFF * 0.25) *
		((stateRU[e][VELOCITY_T] - stateRD[e][VELOCITY_T]) +
		 (stateLU[e][VELOCITY_T] - stateLD[e][VELOCITY_T]));

      // Use one-sided differences when edges abut physical boundaries.

      // Bottom:  stateLD, stateRD are off the grid.
      foreach (e in outerE * abutters[-1])
	difCoefs[e] = difCoefs[e] + (ART_VISC_COEFF * 0.5) *
	((stateRU[e][VELOCITY_T] - stateR[e][VELOCITY_T]) +
	 (stateLU[e][VELOCITY_T] - stateL[e][VELOCITY_T]));

      // Top:  stateLU, stateRU are off the grid.
      foreach (e in outerE * abutters[+1])
	difCoefs[e] = difCoefs[e] + (ART_VISC_COEFF * 0.5) *
	((stateR[e][VELOCITY_T] - stateRD[e][VELOCITY_T]) +
	 (stateL[e][VELOCITY_T] - stateLD[e][VELOCITY_T]));
    }

    /*
      Zero out difCoefs wherever positive.
    */
    // foreach (e in outerE - dProblemEdges[dimVisc])
    foreach (e in outerE) if (! dProblemEdges[dimVisc].contains(e))
      if (difCoefs[e] > 0.0) difCoefs[e] = 0.0;

    /*
      On physical boundary, copy from neighbor.
    */
    foreach (pdir in Util.DIRECTIONS) {
      int dimdir = pdir[1] * dimVisc;
      Point<SPACE_DIM> unitBack = Point<SPACE_DIM>.direction(-dimdir);
      foreach (e in outerE *
	       (dProblem.border(1, dimdir, 0) + Util.cell2edge[dimdir]))
	difCoefs[e] = difCoefs[e + unitBack];
    }
  }


  public void consToPrim(Quantities [SPACE_DIM d] state,
                         Quantities [SPACE_DIM d] q) {
    foreach (p in q.domain()) q[p] = consToPrim(state[p]);
  }

  public Quantities consToPrim(Quantities U) {
    double density = Math.max(U[0], MIN_DENSITY);
    double vX = U[1] / density;
    double vY = U[2] / density;

    double pressure =
      Math.max(MIN_PRESSURE,
	       (GAMMA - 1.0) * (U[3] - density * 0.5 * (vX * vX + vY * vY)));
    return new Quantities(density, vX, vY, pressure);
  }

  public void primToCons(Quantities [SPACE_DIM d] qarr,
			 Quantities [SPACE_DIM d] U) {
    foreach (p in U.domain()) U[p] = primToCons(qarr[p]);
  }

  public Quantities primToCons(Quantities q) {
    double rho = Math.max(q[DENSITY], MIN_DENSITY);
    return
      new Quantities(rho,
		  rho * q[VELOCITY_X],
		  rho * q[VELOCITY_Y],
		  q[PRESSURE] / (GAMMA - 1.0) + rho * 0.5 *
		  (q[VELOCITY_X]*q[VELOCITY_X] + q[VELOCITY_Y]*q[VELOCITY_Y]));
  }


  private void slope(
		     SharedRegion single tempRegion, // REGION
		     Quantities [SPACE_DIM d] q,
		     Quantities [SPACE_DIM d] dq,
		     int sweepDim,
		     boolean single doFourth,
		     boolean single doFlatten) {
    /*
      RectDomain<SPACE_DIM> slopeBox = dProblem * ba[lba]
      .accrete(1, +sweepDim)
      .accrete(1, -sweepDim);

      Quantities [SPACE_DIM d] dq = new (myRegion) Quantities[slopeBox];

      // q on primBox:  ba[lba] + (3+1) (sweepDim)
      // dq on slopeBox:  ba[lba] + 1 (sweepDim)

      slope(q, dq, sweepDim);
    */
    Point<SPACE_DIM> unit = Point<SPACE_DIM>.direction(sweepDim);
    RectDomain<SPACE_DIM> primBox = q.domain();
    RectDomain<SPACE_DIM> slopeBox = dq.domain();

    Quantities [SPACE_DIM d] dq2 = new
	(tempRegion) // REGION
	Quantities[slopeBox];

    // Two-sided differences in interior.
    RectDomain<SPACE_DIM> DInner =
      slopeBox * primBox.shrink(1, +sweepDim).shrink(1, -sweepDim);

    foreach (p in DInner) {
      dq2[p] = Quantities.vanLeer(q[p - unit], q[p], q[p + unit]);
    }

    // One-sided differences at ends where physical boundaries lie.
    // Do something different on solid wall boundaries.

    bc.slope(q, slopeBox.border(1, -sweepDim, 0) - DInner, -sweepDim, dq2, dx);

    bc.slope(q, slopeBox.border(1, +sweepDim, 0) - DInner, +sweepDim, dq2, dx);

    RectDomain<SPACE_DIM> DFourth;
    if (doFourth) {
	DFourth = DInner.shrink(1, +sweepDim).shrink(1, -sweepDim);
    } else {
	DFourth = Util.EMPTY;
    }

    // foreach (p in slopeBox - DFourth) dq[p] = dq2[p];
    foreach (p in slopeBox) if (! DFourth.contains(p)) dq[p] = dq2[p];
    foreach (p in DFourth) {
	dq[p] = Quantities.slope4(q[p - unit], q[p], q[p + unit],
				  dq2[p - unit] + dq2[p + unit]);
    }

    if (doFlatten) {
	flatten(
		tempRegion, // REGION
		q, dq, sweepDim);
    }
  }

    
  private void flatten(
		       SharedRegion single tempRegion, // REGION
		       Quantities [SPACE_DIM d] q,
		       Quantities [SPACE_DIM d] dq,
		       int sweepDim) {
    Point<SPACE_DIM> unit = Point<SPACE_DIM>.direction(sweepDim);
    RectDomain<SPACE_DIM> primBox = q.domain();
    RectDomain<SPACE_DIM> slopeBox = dq.domain();

    double DELTA = 0.33, Z0 = 0.75, Z1 = 0.85;

    RectDomain<SPACE_DIM> flattenedBox =
      primBox.shrink(2, +sweepDim).shrink(2, -sweepDim);

    double [SPACE_DIM d][1d] chiBar = new
      (tempRegion) // REGION
      double[flattenedBox][Util.DIMENSIONS];

    foreach (p in flattenedBox) {
      double pNext = q[p + unit][PRESSURE];
      double pNextNext = q[p + unit + unit][PRESSURE];
      double pPrev = q[p - unit][PRESSURE];
      double pPrevPrev = q[p - unit - unit][PRESSURE];
      if (Math.abs(pNext - pPrev) > DELTA * Math.min(pNext, pPrev)) {
	double ratio = Math.abs((pNext - pPrev) / (pNextNext - pPrevPrev));
	double eta = Math.min(1.0, Math.max(0.0, 1.0 - (ratio - Z0) / (Z1 - Z0)));
	foreach (pdim in Util.DIMENSIONS) {
	  int VELOCITY_N = velocityIndex(pdim);
	  if (q[p - unit][VELOCITY_N] > q[p + unit][VELOCITY_N]) {
	    chiBar[p][pdim] = eta;
	  } else {
	    chiBar[p][pdim] = 1.0;
	  }
	}
      } else {
	foreach (pdim in Util.DIMENSIONS) chiBar[p][pdim] = 1.0;
      }
    }

    foreach (p in slopeBox * flattenedBox) {
      double chi = 1.0;
      foreach (pdim in Util.DIMENSIONS) {
	chi = Math.min(chi, chiBar[p][pdim]);
      }
      Point<SPACE_DIM> adj =
	(q[p + unit][PRESSURE] > q[p - unit][PRESSURE]) ?
	(p - unit) : (p + unit);
      if (flattenedBox.contains(adj)) {
	foreach (pdim in Util.DIMENSIONS) {
	  chi = Math.min(chi, chiBar[adj][pdim]);
	}
      }
      dq[p] = dq[p] * chi;
    }
  }


  /**
   * Given q and dq, return qlo and qhi.
   * dq, qlo, qhi are all defined on slopeBox.
   */
  private void normalPred(Quantities [SPACE_DIM d] q,
			  Quantities [SPACE_DIM d] dq,
			  Quantities [SPACE_DIM d] qlo,
			  Quantities [SPACE_DIM d] qhi,
			  double DtDx,
			  int sweepDim) {
    double halfDtDx = 0.5 * DtDx;
    RectDomain<SPACE_DIM> slopeBox = dq.domain();

    int VELOCITY_N = velocityIndex(sweepDim);

    foreach (p in slopeBox) {
      double c = soundspeed(q[p]);
      double vN = q[p][VELOCITY_N];
      Quantities eig0;

      double dqpp;
      try {
	dqpp = dq[p][DENSITY] - dq[p][PRESSURE] / (c * c);
      } catch (Exception x) {
	System.out.println("Error in normalPred() dq"+p+" in "+slopeBox);
	System.exit(0);
      }

      try {
        eig0 = new Quantities(DENSITY, dqpp);
        // foreach (pdim in Util.DIMENSIONS - [sweepDim:sweepDim]) {
	foreach (pdim in Util.DIMENSIONS) if (pdim[1] != sweepDim) {
          int VELOCITY_T = velocityIndex(pdim);
          eig0 += new Quantities(VELOCITY_T, dq[p][VELOCITY_T]);
        }
      } catch (Exception x) {
	System.out.println("Error in normalPred() eig0 at "+p+" in "+slopeBox);
	System.exit(0);
      }

      try {
	qlo[p] = q[p] - dq[p] * (0.5 + halfDtDx * Math.min(vN - c, 0.0));
	if (vN < 0.0) {
	  qlo[p] = qlo[p] - eig0 * (halfDtDx * c);
	  if (vN < -c)
	    qlo[p] = qlo[p] - eigs(+sweepDim, q[p], dq[p], c) * halfDtDx * c;
	}
      } catch (Exception x) {
	System.out.println("Error in normalPred() qlo at "+p+" in "+slopeBox);
	System.exit(0);
      }

      try {
	qhi[p] = q[p] + dq[p] * (0.5 - halfDtDx * Math.max(vN + c, 0.0));
	if (vN > 0.0) {
	  qhi[p] = qhi[p] + eig0 * (halfDtDx * c);
	  if (vN > c)
	    qhi[p] = qhi[p] + eigs(-sweepDim, q[p], dq[p], c) * halfDtDx * c;
	}
      } catch (Exception x) {
	System.out.println("Error in normalPred() qhi at "+p+" in "+slopeBox);
	System.exit(0);
      }
    }
  }


  private static Quantities eigs(int sdim,
				 Quantities q,
				 Quantities dq,
				 double c) {
    int dim = (sdim > 0) ? sdim : -sdim;

    int VELOCITY_N = velocityIndex(dim);

    double sdensity = (sdim > 0) ? q[DENSITY] : -q[DENSITY];
    double dvN = dq[VELOCITY_N];
    double dp = dq[PRESSURE];
    Quantities eigDensity = new Quantities(DENSITY, (dvN * sdensity + dp / c) / c);
    Quantities eigVelocity = new Quantities(VELOCITY_N, dvN + dp / (sdensity * c));
    Quantities eigPressure = new Quantities(PRESSURE, dvN * sdensity * c + dp);
    return (eigDensity + eigVelocity + eigPressure);
  }

  /**
   * Solve Riemann problems, and store results in qGdnv.
   */
  private void riemann(Quantities [SPACE_DIM d] qleft,
		       Quantities [SPACE_DIM d] qright,
		       Quantities [SPACE_DIM d] qGdnv,
		       int sweepDim) {
    int VELOCITY_N = velocityIndex(sweepDim);
    int VELOCITY_T = velocityOther(sweepDim);

    foreach (e in qGdnv.domain()) {
      Quantities qL = qleft[e], qR = qright[e];
      double cL = soundspeed(qL), cR = soundspeed(qR);

      double rhoL = qL[DENSITY], rhoR = qR[DENSITY];
      double vNL = qL[VELOCITY_N], vNR = qR[VELOCITY_N];
      double vTL = qL[VELOCITY_T], vTR = qR[VELOCITY_T];
      double pL = qL[PRESSURE], pR = qR[PRESSURE];

      double wL = cL * rhoL, wR = cR * rhoR;

      double p = (pL * wR + pR * wL + wL * wR * (vNL - vNR)) / (wL + wR);
      p = Math.max(p, MIN_PRESSURE);

      double u = (vNR * wR + vNL * wL + pL - pR) / (wL + wR);

      double usgn, cside, rhoside, vNside, vTside, pside;
      Quantities qside;
      if (u >= 0.0) {
	usgn = +1.0; qside = qL; cside = cL;
	rhoside = rhoL; vNside = vNL; vTside = vTL; pside = pL;
      } else {
	usgn = -1.0; qside = qR; cside = cR;
	rhoside = rhoR; vNside = vNR; vTside = vTR; pside = pR;
      }

      double rho = rhoside + (p - pside) / (cside * cside);
      rho = Math.max(rho, MIN_DENSITY);

      Quantities qstar =
	new Quantities(DENSITY, rho,
		       VELOCITY_N, u,
		       VELOCITY_T, vTside,
		       PRESSURE, p);

      double ustar = u - usgn * soundspeed(qstar);  // hence |ustar| <= |u|
      double v = vNside - usgn * cside;

      Quantities qmid;
      if (p > pside) {
	// Shock at this side.
	if (usgn * (ustar + v) >= 0.0)
	  qmid = qside;
	else
	  qmid = qstar;
      } else {
	// Rarefaction at this side.
	if (usgn * v >= 0.0)
	  qmid = qside;
	else if (usgn * ustar <= 0.0)
	  qmid = qstar;
	else {
	  double vustardif = v - ustar;
	  // usgn * v < 0, usgn * ustar > 0
	  // hence usgn * (v - ustar) < 0
	  double frac;
	  if (-usgn * vustardif < RELATIVE_TOLERANCE * (-usgn * v))
	    frac = 1.0;
	  else
	    frac = Math.max(0.0, Math.min(v / vustardif, 1.0));
	  qmid = qstar * frac + qside * (1.0 - frac);
	}
      }
      qGdnv[e] = qmid;
    } // foreach (e in riemannBox)
  }


  /**
   * Calculate flux.
   * @param qGdnv (edge) primitive variables
   * @param state (cell) conserved variables
   * @param difCoefs (edge) artificial viscosity
   * @param flux (edge) flux, output
   */
  private void fluxCalc(Quantities [SPACE_DIM d] qGdnv,
			Quantities [SPACE_DIM d] state,
			double [SPACE_DIM d] difCoefs,
			Quantities [SPACE_DIM d] flux,
			int sweepDim) {
    // qGdnv.domain() ==
    // riemannBox:  inner edges of (ba[lba] + 1 (sweepDim)) in sweepDim
    // state.domain() ==
    // stateBox:  ba[lba] + 1 (isotropic) + 3 (sweepDim)
    // difCoefs.domain() == outer edges of (ba[lba]) in sweepDim
    // flux.domain() ==
    // edgeBox:  outer edges of (ba[lba]) in sweepDim

    // Hence flux.domain() - qGdnv.domain() ==
    // outer edges of (ba[lba]) in sweepDim that lie on physical boundary

    foreach (e in qGdnv.domain()) {
      flux[e] = interiorFlux(qGdnv[e], sweepDim) +
	(state[e + Util.edge2cell[+sweepDim]] -
	 state[e + Util.edge2cell[-sweepDim]]) * difCoefs[e];
    }

    // Get flux at physical boundaries, using PhysBC bc.

    // flux.domain() - qGdnv.domain() ==
    // outer edges of (ba[lba]) in sweepDim that lie on physical boundary.
    // foreach (e in flux.domain() - qGdnv.domain())
    // flux[e] = new Quantities.zero();

    // qGdnv is not defined on the physical boundary.  Use qlo, qhi?  Or state?
    foreach (pdir in Util.DIRECTIONS) {
      int dimdir = sweepDim * pdir[1];
      RectDomain<SPACE_DIM> physBorder = 
	dProblemEdges[sweepDim] * flux.domain().border(1, dimdir, 0);
      if (! physBorder.isNull())
	bc.f(state.restrict(physBorder + Util.edge2cell[-dimdir]),
	     difCoefs.restrict(physBorder),
	     dimdir,
	     flux.restrict(physBorder),
             dx);
    }
  }


  public Quantities interiorFlux(Quantities qedge, int sweepDim) {
    int VELOCITY_N = velocityIndex(sweepDim);
    int VELOCITY_T = velocityOther(sweepDim);

    double rho = qedge[DENSITY];
    double vN = qedge[VELOCITY_N];
    double vT = qedge[VELOCITY_T];
    double p = qedge[PRESSURE];
    double bigterm = p / (1.0 - 1.0 / GAMMA) + rho * 0.5 * (vN*vN + vT*vT);
    return new Quantities(DENSITY, rho * vN,
			  VELOCITY_N, rho * vN * vN + p,
			  VELOCITY_T, rho * vN * vT,
			  PRESSURE, vN * bigterm);
  }


  public Quantities solidBndryFlux(Quantities qedge, int signdim) {
    int dim = (signdim > 0) ? signdim : -signdim;

    int VELOCITY_N = velocityIndex(dim);

    double dir = (signdim > 0 ) ? 1.0 : -1.0;
    double pstar = qedge[PRESSURE] +
      dir * qedge[DENSITY] * soundspeed(qedge) * qedge[VELOCITY_N];
    return new Quantities(VELOCITY_N, pstar);
  }

  /**
   * Perform conservative update of state using flux.
   */
  private void update(Quantities [SPACE_DIM d] flux,
		      Quantities [SPACE_DIM d] state,
		      double DtDx,
		      int sweepDim) {
    // System.out.println("Updating " + state.domain() + " from " +
    // flux.domain() + " with dt/dx = " + DtDx);
    Point<SPACE_DIM> nextEdge = Util.cell2edge[+sweepDim];
    Point<SPACE_DIM> prevEdge = Util.cell2edge[-sweepDim];
    foreach (p in state.domain())
      try {
	Quantities fluxDiff = flux[p + nextEdge] - flux[p + prevEdge];
	state[p] = state[p] - fluxDiff * DtDx;
      } catch (Exception x) {
	System.out.println("Error in update at " + p);
	System.exit(0);
      }
  }


  /**
   * If q contains primitive variables, then 
   * q[velocityIndex(dim)] is velocity in dimension dim.
   */
  private static int velocityIndex(int dim) { return dim; }

  private static int velocityIndex(Point<1> pdim) { return pdim[1]; }

  private static int velocityOther(int dim) { return (3 - dim); }


  /**
   * If q contains primitive variables, then 
   * soundspeed2(q) returns squared speed of sound.
   */
  private static double soundspeed2(Quantities q) {
    return (GAMMA * Math.abs(q[PRESSURE] / q[DENSITY]));
  }


  /**
   * If q contains primitive variables, then 
   * soundspeed(q) returns speed of sound.
   */
  private static double soundspeed(Quantities q) {
    return Math.sqrt(soundspeed2(q));
  }


  /**
   * Return maximum possible timestep.
   */
  single public double single estTimeStep(LevelArrayQuantities single U) {
    double maxv = 0.0;  // local
    int myProc = Ti.thisProc();
    foreach (lba in ba.procboxes(myProc))
      foreach (p in ba[lba]) {
      Quantities qpt = consToPrim(U[lba][p]);
      double c = soundspeed(qpt);
      // Doesn't use constants VELOCITY_X, VELOCITY_Y to retrieve velocities.
      foreach (pdim in Util.DIMENSIONS)
	maxv = Math.max(maxv, Math.abs(qpt[pdim[1]]) + c);
    }
    return (dx / Reduce.max(maxv));
  }


  /**
   * Fill UNew by copying from UTemp and interpolating from UTempCoarse.
   * Use PWLFillPatch object fp.
   *
   * @param UNew (cell) new patches at this level, to be filled
   * @param UTemp (cell) old patches at this level
   * @param UTempCoarse (cell) old patches at next coarser level
   */
  single public void
    regrid(LevelArrayQuantities single UNew,
	   LevelArrayQuantities single UTemp,
	   LevelArrayQuantities single UTempCoarse) {
    fp.regrid(UNew, UTemp, UTempCoarse);
  }
}
