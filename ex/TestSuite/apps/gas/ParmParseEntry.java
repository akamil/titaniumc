public class ParmParseEntry {
  private String head;
  private BoxedList_String list;

  public ParmParseEntry(String h, BoxedList_String l) {
    head = h;
    list = l;
  }

  public String head() { 
    return head;
  }

  public BoxedList_String list() {
    return list;
  }

  public void print() {
    String output = head + " : ( ";
    for (List_String l = list.toList(); l != null; l = l.rest()) {
      output += l.first() + ' ';
    }
    output += ')';
    System.out.println(output);
  }
}
