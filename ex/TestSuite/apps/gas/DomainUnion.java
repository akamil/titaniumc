class DomainUnion implements ObjectOp {
  public Object eval(Object left, Object right) {
    Domain<2> leftDomain = (Domain<2>) left;
    Domain<2> rightDomain = (Domain<2>) right;
    return leftDomain + rightDomain;
  }
}
