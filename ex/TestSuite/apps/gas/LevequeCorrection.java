/**
 * LevequeCorrection:
 * Objects in this class manage the setting of intermediate boundary
 * conditions in the operator-split method.
 * 
 * A LevequeCorrection object is a field of LevelGodunov.
 * This class is used in LevelGodunov only.
 * 
 * Given a patch, ghost cells are set as follows:
 * - If covered by another grid at the _same_ level, then copy.
 * - Otherwise:
 *   (1) interpolate from grids at next coarser level;
 *   (2) correct by performing a partial update corresponding to
 *       dimensional sweeps that have already been taken.
 * 
 * The LevequeCorrection class is concerned with the correction in (2).
 * The correction is made using a piecewise constant extrapolation of the
 * flux differences from the nearest interior cell.
 *
 *      -------------+
 *                   |x
 *                   |x    <- do correction on these ghost cells
 *                   |x
 *        box        +-----------------
 *                   |
 *                   |   other box
 *                   |
 *      -------------+
 *                   |
 *
 *
 * @version  7 Aug 2000
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public class LevequeCorrection {

  /*
    constants
  */

  private static final int single SPACE_DIM = Util.SPACE_DIM;

  /*
    fields that are arguments to constructor
  */

  private SharedRegion single myRegion; // REGION

  private BoxArray ba;

  /** physical domain */
  private RectDomain<SPACE_DIM> dProblem;

  /** mesh spacing */
  private double dx;

  private int numFluxes;

  /*
    other fields that are set in constructor
  */

  /** indices of boxes in BoxArray ba */
  private RectDomain<1> indices;

  /** for each box, the set of dimensions already swept */
  private Domain<1> [1d] haveDims; // [indices]

  /** ghost cells indexed by dimension, direction, box index */
  private Domain<SPACE_DIM> [1d][1d][1d] ghosts; // [Util.DIMENSIONS][Util.DIRECTIONS][indices]

  /** corrections indexed by dimension, direction */
  private LevelArrayQuantities [1d][1d] correction; // [Util.DIMENSIONS][Util.DIRECTIONS]

  /**
   * constructor
   */
  single public LevequeCorrection(
				  SharedRegion single MyRegion, // REGION
			   BoxArray Ba,
			   RectDomain<SPACE_DIM> DProblem,
			   double Dx,
			   int NumFluxes) {
    myRegion = MyRegion; // REGION
    ba = Ba;
    indices = ba.indices();
    dProblem = DProblem;
    dx = Dx;
    numFluxes = NumFluxes;
    haveDims = new
      (myRegion) // REGION
      Domain<1>[indices];

    ghosts = new
      (myRegion) // REGION
      Domain<SPACE_DIM>[Util.DIMENSIONS][Util.DIRECTIONS][indices];

    correction = new
      (myRegion) // REGION
      LevelArrayQuantities[Util.DIMENSIONS][Util.DIRECTIONS];

    int [1d] proclist = ba.proclist();

    foreach (pdim in Util.DIMENSIONS) foreach (pdir in Util.DIRECTIONS) {
      Domain<SPACE_DIM> [1d] ghostsDimDir = ghosts[pdim][pdir];
      RectDomain<SPACE_DIM> [1d] RghostsDimDir = new
	(myRegion) // REGION
	RectDomain<SPACE_DIM>[indices];

      foreach (indbox in indices) {
	RghostsDimDir[indbox] = dProblem * ba[indbox].border(pdim[1]*pdir[1]);
	ghostsDimDir[indbox] = RghostsDimDir[indbox];

	// Remove cells belonging to other grids at this level.
	foreach (indother in indices)
	  ghostsDimDir[indbox] = ghostsDimDir[indbox] - ba[indother];
      }

      BoxArray single baDimDir = new
	(myRegion) // REGION
	BoxArray(RghostsDimDir, proclist);

      correction[pdim][pdir] = new
	(myRegion) // REGION
	LevelArrayQuantities(
			     myRegion, // REGION
			     baDimDir);
    }
  }


  /*
    methods
  */


  /**
   * Reinitialize internal state.
   * Must be called prior to first use of object in LevelGodunov.step().
   */
  public void clear() {
    foreach (pdim in Util.DIMENSIONS) foreach (pdir in Util.DIRECTIONS)
      foreach (indbox in indices)
      correction[pdim][pdir][indbox].set(Quantities.zero());

    foreach (indbox in indices) haveDims[indbox] = [0:-1];
  }


  /**
   * Increment registers corresponding to the part of the boundary
   * of ba[PatchIndex] in direction SweepDim.
   *
   * The increment is the divided difference of Flux in direction
   * SweepDim, multiplied by Dt.  The difference is taken across 
   * the NON-ghost cell on the boundary.
   */
  public void increment(Quantities [SPACE_DIM d] Flux,
			double Dt,
			Point<1> PatchIndex,
			int sweepDim) {
    // System.out.println("Entering lc.increment");
    double DtDx = Dt / dx;
    haveDims[PatchIndex] = haveDims[PatchIndex] + [sweepDim:sweepDim];

    /*
      Loop over the dimensions that have NOT yet been updated.
      In these dimensions, increment the corrections in the ghost cells,
      according to the change in flux in the dimension of the current sweep.

      e.g. forward in 3 dimensions:
      [1] increment sides in dimensions 2, 3
      [2] increment sides in dimension 3
      [3] nothing
    */
    // foreach (pdim in Util.DIMENSIONS - haveDims[PatchIndex])
    foreach (pdim in Util.DIMENSIONS) if (! haveDims[PatchIndex].contains(pdim))
      foreach (pdir in Util.DIRECTIONS) {
      Point<SPACE_DIM> unit = Point<SPACE_DIM>.direction(pdir[1] * pdim[1]);
      /*
	If  p  is a ghost cell, then  p-unit  is a non-ghost
	cell on the boundary.
	Also recall that LevelGodunov.update() does
	state[p] = state[p] - ((flux[p + Util.cell2edge[+sweepDim]] -
	                        flux[p + Util.cell2edge[-sweepDim]]) * DtDx);
      */
      foreach (p in ghosts[pdim][pdir][PatchIndex]) {
	correction[pdim][pdir][PatchIndex][p] =
	  correction[pdim][pdir][PatchIndex][p] -
	  (Flux[p-unit + Util.cell2edge[+sweepDim]] -
	   Flux[p-unit + Util.cell2edge[-sweepDim]]) * DtDx;
      }
    }
  }


  /**
   * Apply correction to the state in the first row of cells 
   * adjacent to ba[PatchIndex] in all of the normal directions
   * that have not yet been updated.  (LevequeCorrection keeps
   * track of that information internally.)
   */
  public void correctBoundary(Quantities [SPACE_DIM d] UPatch,
			      Point<1> PatchIndex,
			      int sweepDim) {
    /*
      UPatch.domain() ==
      stateBox:  ba[PatchIndex] + 1 (isotropic) + 3 (sweepDim)
    */
    foreach (pdir in Util.DIRECTIONS) {
      foreach (p in ghosts[sweepDim][pdir][PatchIndex]) try {
	UPatch[p] = UPatch[p] + correction[sweepDim][pdir][PatchIndex][p];
      } catch (Exception x) {
	System.out.println("Error in correctBoundary at " + p + " with " +
			   UPatch.domain() + " ghosts " +
			   ghosts[sweepDim][pdir][PatchIndex]);
      }
    }
  }
}
