import java.io.PrintStream;

 class BoxedList_ParmParseEntry {
    private List_ParmParseEntry l = null;

    public BoxedList_ParmParseEntry() {
    }

    public BoxedList_ParmParseEntry(ParmParseEntry x) {
        push(x);
    }

    public void push(ParmParseEntry item) {
        l = new List_ParmParseEntry(item, l);
    }

    public ParmParseEntry pop() {
        ParmParseEntry t = l.first();
	l = l.rest();
	return t;
    }

    public ParmParseEntry first() {
        return l.first();
    }

    public List_ParmParseEntry toList() {
        return l;
    }

    public ParmParseEntry [1d] toArray() {
	ParmParseEntry [1d] a = new ParmParseEntry [[0 : length() - 1]];
	int i = 0;
	for (List_ParmParseEntry r = l; r != null; r = r.rest()) {
	    a[i] = r.first();
	    i++;
	}
	return a;
    }

    public ParmParseEntry [1d] toReversedArray() {
	int i = length();
	ParmParseEntry [1d] a = new ParmParseEntry [[0 : i - 1]];
	for (List_ParmParseEntry r = l; r != null; r = r.rest()) {
	    a[--i] = r.first();
	}
	return a;
    }

    public boolean isEmpty() {
	return l == null;
    }

    public int length() {
	return l == null ? 0 : l.length();
    }
}
