import java.io.*;

/**
 * Ramp contains the main program.
 *
 * @version  17 Dec 1999
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public class Ramp {

  /*
    constants
  */

  protected static final int single SPACE_DIM = Util.SPACE_DIM;
  protected static final int DENSITY=0, VELOCITY_X=1, VELOCITY_Y=2, PRESSURE=3;

  /** process used for broadcasts */
  private static final int single SPECIAL_PROC = 0;

  /*
    protected fields may be used by RampBC, RampInit.
  */

  /** dimension along barrier, e.g. 1 */
  protected static int LENGTH_DIM = 1;

  /** dimension above barrier, e.g. 2 */
  protected static int HEIGHT_DIM = 2;

  /** shock location on the solid boundary */
  protected static double shockLocBottom;

  /** shock location on the free top */
  protected static double shockLocTop;

  /** speed of shock along the free top */
  protected static double shockSpeed;

  /** slope of the shock */
  protected static double slope;

  /** conserved variables on each side of the shock */
  protected static Quantities U0, U1;

  /** flux on each side of the shock */
  protected static Quantities [1d] q0flux, q1flux;

  protected static Amr single amrAll;

  protected static double single [1d][1d] ends;

  protected static double gamma = 1.4;

  protected static LevelGodunov single lg0;

  single public static void main(String args[]) {
    // System.out.println("starting program");
    int myProc = Ti.thisProc();
    // System.out.println(myProc + " running");

    ParmParse.init(args);
    // printOut:  whether to print out results
    boolean single printOut =
      broadcast ParmParse.containsGeneral("print") from SPECIAL_PROC;

    boolean single saved =
      broadcast ParmParse.containsGeneral("saved") from SPECIAL_PROC;

    ParmParse pp = new ParmParse("prob");

    /*
      Define Amr object.
    */

    // System.out.println(myProc + " defining Amr object");
    amrAll = new Amr();

    // MyInputStream extends DataInputStream with functions.
    // You could move the declaration inside Amr and then
    // use an accessor function in HSCLLevel.
    MyInputStream in;
    if (saved) {
      try {
	in = new MyInputStream(System.in);
	System.out.println("opening saved file");
	amrAll.getSaved(in);
      } catch (IOException iox) {
	System.out.println("Error opening input");
	System.exit(0);
      }
    }

    Ti.barrier();

    /*
      Define HSCLLevel object at level 0.
    */

    boolean single logging =
      broadcast ParmParse.containsGeneral("log") from SPECIAL_PROC;
    if (logging) Logger.begin();

    // System.out.println(myProc + " defining HSCLLevel 0");
    SharedRegion single myRegion = new SharedRegion(); // REGION

    // Need to construct HSCLLevel from Ramp, because you can't do it
    // from Amr, which recognizes only AmrLevel, not HSCLLevel.
    HSCLLevel single level0 =
      new HSCLLevel(
		    myRegion, // REGION
		    amrAll, 0, amrAll.baseBoxes(), BoxArray.Null(),
		    amrAll.getDomain(0), 0, amrAll.getDx(0), new RampBC());
    // System.out.println(myProc + " setLevel 0");
    Ti.barrier();
    amrAll.setLevel(0, level0);
    Ti.barrier();

    /*
      Get data.  Need LevelGodunov object to do primitive-conservative
      conversions.  Also need gamma.
    */

    // System.out.println(myProc + " getData");
    ends = amrAll.getEnds();

    // Don't delete lgRegion until after amrAll.run(),
    // because lg0 is used in RampBC.
    SharedRegion single lgRegion = new SharedRegion(); // REGION
    { // scope of lgRegion
    lg0 = new LevelGodunov(
			   lgRegion, // REGION
			   BoxArray.Null(), BoxArray.Null(),
                           amrAll.getDomain(0), 1, 0.0, null);
    // don't need ba, baCoarse, nRefine, dx, bc
    // lg0 = level0.getLevelGodunov();  // causes region problems

    getData(pp);

    /*
      Initialize patches at all levels.
      This will also define HSCLLevel objects at finer levels.
    */

    RampInit initializer = new RampInit();

    // System.out.println(myProc + " calling initGrids");
    level0.initGrids(initializer);
    // System.out.println(myProc + " called initGrids");

    // Throw away reference to HSCLLevel, to make deleting myRegion
    // easier.
    level0 = null;

    // System.out.println(myProc + " defined HSCLLevels");

    Logger.barrier();

    /*
      Process the patches.
    */

    // System.out.println(myProc + " *********** running ***********");
    // Logger.begin();
    long startTime = System.currentTimeMillis();

    amrAll.run();

    Logger.end();
    long endTime = System.currentTimeMillis();
    Ti.barrier();

    /*
      Output
    */

    System.out.println("time = " + (endTime - startTime) / 1000 + " sec");

    if (printOut) amrAll.write();

    } // scope of lgRegion
  }


  /**
   * Get shock data to be used by RampInit and RampBC.
   * For RampInit:  shockLocBottom, slope, U0, U1
   * For RampBC:  q0flux, q1flux, shockLocTop, shockSpeed
   * Need LevelGodunov lg0 for conversions and flux calculations.
   */
  private static void getData(ParmParse pp) {
    double shockang = pp.getDouble("shockang");
    double shockAngle = shockang * (Math.PI / 180.0);
    slope = 1.0 / Math.tan(shockAngle);
    
    double p0 = pp.getDouble("pressref");
    double shockMach = pp.getDouble("shockmach");
    double c0 = pp.getDouble("soundref");
    double rho0 = gamma * p0 / (c0 * c0);

    double s1 = shockMach * c0;
    double p1 = p0 * (2.0 * gamma * shockMach * shockMach - gamma + 1.0) /
      (gamma + 1.0);
    double v1 = (p1 - p0) / (s1 * rho0);
    double ux1 = v1 * Math.cos(shockAngle);
    double uy1 = -v1 * Math.sin(shockAngle);
    double rho1 = rho0 / (1.0 - v1 / s1);

    shockLocBottom = pp.getDouble("shockloc");

    /*
      Set initial primitive quantities.  Recall order rho, ux, uy, p.
      (We need lg0 for conversions and flux.)
    */

    Quantities q0 = new Quantities(DENSITY, rho0) +
      new Quantities(PRESSURE, p0);
    Quantities q1 = new Quantities(DENSITY, rho1) +
      new Quantities(velocityIndex(LENGTH_DIM), ux1) +
      new Quantities(velocityIndex(HEIGHT_DIM), uy1) +
      new Quantities(PRESSURE, p1);

    U0 = lg0.primToCons(q0);
    U1 = lg0.primToCons(q1);

    q0flux = new Quantities[Util.DIMENSIONS];
    q1flux = new Quantities[Util.DIMENSIONS];
    foreach (pdim in Util.DIMENSIONS) {
      q0flux[pdim] = lg0.solidBndryFlux(q0, pdim[1]);
      q1flux[pdim] = lg0.interiorFlux(q1, pdim[1]);
    }
    // ends[+1][HEIGHT_DIM] - ends[-1][HEIGHT_DIM] is height
    shockLocTop = shockLocBottom +
      (ends[+1][HEIGHT_DIM] - ends[-1][HEIGHT_DIM]) * Math.tan(shockAngle);
    shockSpeed = s1 / Math.cos(shockAngle);
  }

  /**
   * If q contains primitive variables, then 
   * q[velocityIndex(dim)] is velocity in dimension dim.
   */
  protected static int velocityIndex(int dim) { return dim; }

  protected static int velocityIndex(Point<1> pdim) { return pdim[1]; }
}

