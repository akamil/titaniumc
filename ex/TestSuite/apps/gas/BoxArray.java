public class BoxArray {

  private static final int single SPACE_DIM = Util.SPACE_DIM;

  private static final BoxArray single BoxArrayNull = new BoxArray(null, null);

  private RectDomain<SPACE_DIM> [1d] domains;

  // procs[i] is process where data for box i are kept
  private int [1d] procs;

  // pa[proc] is the set of indices of boxes living on process proc
  private Domain<1> [1d] pa;

  // constructor
  public BoxArray(RectDomain<SPACE_DIM> [1d] domainlist, int [1d] proclist) {
    domains = domainlist;
    procs = proclist;

    RectDomain<1> Dprocs = [0 : Ti.numProcs() - 1];

    // Set pa.
    pa = new Domain<1>[Dprocs];
    foreach (indproc in Dprocs) {
      pa[indproc] = [0 : -1];
    }

    if (domains != null) {
      foreach (indbox in domains.domain()) {
	int procbox = procs[indbox];
	pa[procbox] = pa[procbox] + [indbox:indbox];
      }
    }
  }

  // methods

  // Returns the domain of a particular box.
  public RectDomain<SPACE_DIM> op[] (Point<1> p) { return domains[p]; }

  public RectDomain<SPACE_DIM> op[] (int ip) { return domains[ip]; }

  // Returns all indices of boxes.
  public RectDomain<1> indices() { 
    if (domains != null) {
      return domains.domain();
    } else {
      return [0:-1];
    }
  }

  // Returns the number of the process where data for this box reside.
  public int proc(Point<1> index) { return procs[index]; }
  public int proc(int index) { return procs[index]; }

  // Returns the indices of boxes living on process.
  public Domain<1> procboxes(int process) { return pa[process]; }

  // Returns the mapping of boxes to processes.
  public int [1d] proclist() { return procs; }

  // Returns the boxes.
  public RectDomain<SPACE_DIM> [1d] boxes() { return domains; }

  public boolean isNull() { return (domains == null); }

  // return the indices of the boxes that contain the cell.
  public Domain<1> findCell(Point<SPACE_DIM> cell) {
    Domain<1> D = [0 : -1];
    foreach (indbox in indices()) if (domains[indbox].contains(cell))
      D = D + [indbox:indbox];
    return D;
  }

  public String toString() {
    String s = "";
    foreach (indbox in indices()) s = s + "  " + domains[indbox];
    return s;
  }

  public static BoxArray single Null() { return BoxArrayNull; }
}
