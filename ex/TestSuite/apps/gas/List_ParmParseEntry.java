 class List_ParmParseEntry {
    public ParmParseEntry car;
    public List_ParmParseEntry cdr;

    // Main constructor.
    public List_ParmParseEntry(ParmParseEntry x, List_ParmParseEntry rest) {
	car = x;
	cdr = rest;
    }

    public List_ParmParseEntry cons(ParmParseEntry x) {
	return new List_ParmParseEntry(x, this);
    }        

    public /*inline*/ ParmParseEntry first() {
	return car;
    }
    public /*inline*/ List_ParmParseEntry rest() {
	return cdr;
    }

    public int length() {
	int i = 0;
	List_ParmParseEntry r = this;
	while (r != null) {
	    i++;
	    r = r.rest();
	}
	return i;
    }
}
