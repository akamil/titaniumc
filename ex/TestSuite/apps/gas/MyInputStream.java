import java.io.*;
import java.util.*;

public class MyInputStream extends DataInputStream {

  private static final int SPACE_DIM = 2;

  /** number of integers used to specify a domain:  low, high, centering */
  private static final int DOMAIN_LENGTH = 3 * SPACE_DIM;

  private static final RectDomain<1> DIMENSIONS = [1:SPACE_DIM];
 
  /**
   * constructor
   */
  public MyInputStream(InputStream in) {
    super(in);
  }

  /*
    methods
  */

  public void ignoreLine() throws IOException, EOFException {
    String line = readLine();
    if (line == null) throw new EOFException();
  }


  public int readInt1() throws IOException, EOFException {
    String line = readLine();
    if (line == null) throw new EOFException();
    int val = 0;
    try {
      val = Integer.parseInt(line);
      // or new Integer(line).intValue();
    } catch (NumberFormatException nfx) {
      System.out.println(line);
      System.out.println("Bad file format:  need an integer");
      System.exit(0);
    }
    return val;
  }


  public int [1d] readInts() throws IOException, EOFException {
    String line = readLine();
    if (line == null) throw new EOFException();
    StringTokenizer tokens = new StringTokenizer(line);
    int n = tokens.countTokens();
    int [1d] vals = new int[0:n-1];
    try {
      for (int i = 0; i < n; i++) {
        vals[i] = Integer.parseInt(tokens.nextToken());
        // or new Integer(tokens.nextToken()).intValue();
      }
    } catch (NumberFormatException nfx) {
      System.out.println(line);
      System.out.println("Bad file format:  need integers");
      System.exit(0);
    }
    return vals;
  }


  public double readDouble1() throws IOException, EOFException {
    String line = readLine();
    if (line == null) throw new EOFException();
    double val = 0.0;
    try {
      val = new Double(line).doubleValue();
    } catch (NumberFormatException nfx) {
      System.out.println(line);
      System.out.println("Bad file format:  need a double");
      System.exit(0);
    }
    return val;
  }


  public double [1d] readDoubles() throws IOException, EOFException {
    String line = readLine();
    if (line == null) throw new EOFException();
    StringTokenizer tokens = new StringTokenizer(line);
    int n = tokens.countTokens();
    double [1d] vals = new double[0:n-1];
    try {
      for (int i = 0; i < n; i++) {
        vals[i] = new Double(tokens.nextToken()).doubleValue();
        // or new Integer(tokens.nextToken()).intValue();
      }
    } catch (NumberFormatException nfx) {
      System.out.println(line);
      System.out.println("Bad file format:  need doubles");
      System.exit(0);
    }
    return vals;
  }


  public RectDomain<2> readDomain() throws IOException, EOFException {
    String line = readLine();
    if (line == null) throw new EOFException();
    StringTokenizer tokens = new StringTokenizer(line, "( ,)");
    int n = tokens.countTokens();
    int [1d] vals = new int[1:n];
    try {
      for (int i = 1; i <= n; i++) {
        vals[i] = Integer.parseInt(tokens.nextToken());
        // or new Integer(tokens.nextToken()).intValue();
      }
    } catch (NumberFormatException nfx) {
      System.out.println(line);
      System.out.println("Bad file format:  need integers in domain");
      System.exit(0);
    }
    Point<2> lo = Util.ZERO, hi = Util.ZERO;
    foreach (pdim in DIMENSIONS) {
      int dim = pdim[1];
      lo = lo + Point<2>.direction(dim, vals[dim]);
      hi = hi + Point<2>.direction(dim, vals[dim + SPACE_DIM]);
    }
    return [lo : hi];
  }


  public RectDomain<2> [1d] readDomains() throws IOException, EOFException {
    String line = readLine();
    if (line == null) throw new EOFException();
    StringTokenizer tokens = new StringTokenizer(line, "( ,)");
    int n = tokens.countTokens();
    int [1d] vals = new int[1:n];
    try {
      for (int i = 1; i <= n; i++) {
        vals[i] = Integer.parseInt(tokens.nextToken());
        // or new Integer(tokens.nextToken()).intValue();
      }
    } catch (NumberFormatException nfx) {
      System.out.println(line);
      System.out.println("Bad file format:  need integers in domain");
      System.exit(0);
    }
    int numDomains = n / DOMAIN_LENGTH;
    RectDomain<2> [1d] domains = new RectDomain<2>[0:numDomains-1];
    for (int i = 0; i < numDomains; i++) {
      int base = i * DOMAIN_LENGTH;
      Point<2> lo = Util.ZERO, hi = Util.ZERO;
      foreach (pdim in DIMENSIONS) {
	int dim = pdim[1];
	lo = lo + Point<2>.direction(dim, vals[base + dim]);
	hi = hi + Point<2>.direction(dim, vals[base + dim + SPACE_DIM]);
      }
      domains[i] = [lo : hi];
    }
    return domains;
  }
}
