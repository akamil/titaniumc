import java.io.*;
import java.util.*;

/**
 * Read a plot file and print the number of grids and their total sizes
 * at each level.
 * Example:
 * 
 * ~/gas/amr/new/auto/timing/pp/noreg/Levels < outt.128.32.2.8.20
 * Level 0 has 8 grids, total size 4096 points (100% of 4096)
 * Level 1 has 9 grids, total size 2736 points (16% of 16384)
 * Level 2 has 15 grids, total size 7552 points (11% of 65536)
 *
 * @version  14 Aug 2000
 * @author   Peter McCorquodale, PWMcCorquodale@lbl.gov
 */

public class Levels {

  private static DataInputStream in;

  public static void main(String args[]) {

    try {
      in = new DataInputStream(System.in);
    } catch (IOException iox) {
      System.out.println("Error opening input");
      System.exit(0);
    }

    try {
      processFile();
    } catch (IOException iox) {
      System.out.println("Error in input");
      System.exit(0);
    } catch (EOFException eofx) {
      System.out.println("Premature end of input");
      System.exit(0);
    }
  }

  private static void processFile() 
    throws IOException, EOFException {
    int nStates = readInt();
    for (int i = 0; i < nStates; i++) ignoreLine();
    int nDims = readInt();
    ignoreLine(); // time
    int finestLevel = readInt();
    ignoreLine(); // loside
    ignoreLine(); // hiside
    ignoreLine(); // refinement ratios
    RectDomain<2> [1d] wholeDomains = readDomains();
    // 1: steps
    // finestLevel+1: grid spacings
    // 2: coordinate flag, boundary info
    // = finestLevel + 4: total
    for (int i = 0; i < finestLevel + 4; i++) ignoreLine();

    for (int level = 0; level <= finestLevel; level++) {
      int wholeSize = wholeDomains[level].size();
      int totalSize = 0;
      // For each level:
      int [1d] heightAndGrids = readInts();
      int levelCheck = heightAndGrids[0];
      if (levelCheck != level) System.out.println("Level number doesn't check");
      int numGrids = heightAndGrids[1];

      for (int grid = 0; grid < numGrids; grid++) {
	// For each grid:
	RectDomain<2> domain = readDomain();
	totalSize += domain.size();

	// 3: level, steps, time
	// nDims: corners
	// = nDims + 3: total
	for (int i = 0; i < nDims + 3; i++) ignoreLine();

	// writeOn
	for (int i = 0; i < nStates; i++) {
	  // domain again
	  ignoreLine();
	  // data
	  foreach (p in domain) ignoreLine();
	}
      }
      
      int pctFilled = (100 * totalSize) / wholeSize;
      String plural = (numGrids == 1) ? "" : "s";
      System.out.println("Level " + level +
			 " has " + numGrids + " grid" + plural + ", total size " +
			 totalSize + " points (" +
			 pctFilled + "% of " + wholeSize + ")");
    }
  }


  private static void ignoreLine()
    throws IOException, EOFException {
    String line = in.readLine();
    if (line == null) throw new EOFException();
  }


  private static int readInt()
    throws IOException, EOFException {
    String line = in.readLine();
    if (line == null) throw new EOFException();
    int val = 0;
    try {
      val = Integer.parseInt(line);
      // or new Integer(line).intValue();
    } catch (NumberFormatException nfx) {
      System.out.println("Bad file format:  need an integer");
      System.exit(0);
    }
    return val;
  }


  private static int [1d] readInts()
    throws IOException, EOFException {
    String line = in.readLine();
    if (line == null) throw new EOFException();
    StringTokenizer tokens = new StringTokenizer(line);
    int n = tokens.countTokens();
    int [1d] vals = new int[0:n-1];
    try {
      for (int i = 0; i < n; i++) {
        vals[i] = Integer.parseInt(tokens.nextToken());
        // or new Integer(tokens.nextToken()).intValue();
      }
    } catch (NumberFormatException nfx) {
      System.out.println("Bad file format:  need integers");
      System.exit(0);
    }
    return vals;
  }


  private static RectDomain<2> readDomain()
    throws IOException, EOFException {
    String line = in.readLine();
    if (line == null) throw new EOFException();
    StringTokenizer tokens = new StringTokenizer(line, "( ,)");
    int n = tokens.countTokens();
	System.out.println("line = " + line);
	System.out.println("readDomain countTokens = " + n);
    int [1d] vals = new int[1:n];
    try {
      for (int i = 1; i <= n; i++) {
        vals[i] = Integer.parseInt(tokens.nextToken());
        // or new Integer(tokens.nextToken()).intValue();
      }
    } catch (NumberFormatException nfx) {
      System.out.println("Bad file format:  need integers in domain");
      System.exit(0);
    }
    return [[vals[1], vals[2]] : [vals[3], vals[4]]];
  }


  private static RectDomain<2> [1d] readDomains()
    throws IOException, EOFException {
    String line = in.readLine();
    if (line == null) throw new EOFException();
    StringTokenizer tokens = new StringTokenizer(line, "( ,)");
    int n = tokens.countTokens();
    int [1d] vals = new int[1:n];
    try {
      for (int i = 1; i <= n; i++) {
        vals[i] = Integer.parseInt(tokens.nextToken());
        // or new Integer(tokens.nextToken()).intValue();
      }
    } catch (NumberFormatException nfx) {
      System.out.println("Bad file format:  need integers in domain");
      System.exit(0);
    }
    int numDomains = n / 6;
    RectDomain<2> [1d] domains = new RectDomain<2>[0:numDomains-1];
    for (int i = 0; i < numDomains; i++) {
      int base = i * 6;
      domains[i] = 
	[[vals[base+1], vals[base+2]] : [vals[base+3], vals[base+4]]];
    }
    return domains;
  }

}
