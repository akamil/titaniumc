/**
 * Utility methods and data for use with grids.
 */

public class Util {

  /*
    constants
  */

  public static final int single SPACE_DIM = 2;

  public static final RectDomain<1> single DIMENSIONS = [1:SPACE_DIM];
  public static final RectDomain<1> single DIRECTIONS = [-1:1:2];

  public static final Point<SPACE_DIM> single ZERO = Point<SPACE_DIM>.all(0);
  public static final Point<SPACE_DIM> single ONE = Point<SPACE_DIM>.all(1);
  public static final Point<SPACE_DIM> single MINUS_ONE = Point<SPACE_DIM>.all(-1);

  public static final RectDomain<SPACE_DIM> single EMPTY = [ZERO : MINUS_ONE];

  /**
   * cell2edge:
   * If  c  is a CELL, then
   * c + cell2edge[dir * dim]
   * is the EDGE in dimension  dim  (1:SPACE_DIM) in
   * direction  dir (-1, 1) from  c.
   *
   * If  A  is a cell-centered grid, then
   * B = A.translate(cell2edge[dir * dim])
   * will produce an edge-centered grid with
   * B[c + cell2edge[dir * dim]] = A[c].
   */
  public static Point<SPACE_DIM> [1d] cell2edge;

  /**
   * edge2cell:
   * If  e  is an EDGE in dimension  dim  (1:SPACE_DIM), then
   * e + edge2cell[dir * dim]
   * is the CELL in direction  dir (-1, 1) from  e.
   *
   * If  A  is an edge-centered grid, then
   * B = A.translate(edge2cell[dir * dim])
   * will produce a cell-centered grid with
   * B[e + edge2cell[dir * dim]] = A[e].
   */
  public static Point<SPACE_DIM> [1d] edge2cell;

  // Convention:
  // cell      1 2 3 4
  // edge     0 1 2 3 4

  public static void set() {
    cell2edge = new Point<SPACE_DIM>[-SPACE_DIM : +SPACE_DIM];
    edge2cell = new Point<SPACE_DIM>[-SPACE_DIM : +SPACE_DIM];
    foreach (pdim in DIMENSIONS) {
      int dim = pdim[1];

      cell2edge[-dim] = Point<SPACE_DIM>.direction(-dim);
      edge2cell[+dim] = Point<SPACE_DIM>.direction(+dim);

      cell2edge[+dim] = ZERO;
      edge2cell[-dim] = ZERO;
    }
  }

  /**
   * Return domain for inner edges of a box in a particular dimension.
   * According to our convention, this means removing the last row.
   */
  public static RectDomain<SPACE_DIM> innerEdges(RectDomain<SPACE_DIM> box, int dim) {
    return box.shrink(1, +dim);
  }


  /**
   * Return domain for outer edges of a box in a particular dimension.
   * According to our convention, this means appending the first row.
   */
  public static RectDomain<SPACE_DIM> outerEdges(RectDomain<SPACE_DIM> box, int dim) {
    return box.accrete(1, -dim);
  }


  /** Return indices of subcells of cellC at the next finer level. */
  public static RectDomain<SPACE_DIM> subcells(Point<SPACE_DIM> cellC, int nRefine) {
    RectDomain<SPACE_DIM> subBase = [ZERO : Point<SPACE_DIM>.all(nRefine - 1)];
    return (subBase + (cellC * nRefine));
  }


  /** Refine a RectDomain by a given ratio. */
  public static RectDomain<SPACE_DIM> refine(RectDomain<SPACE_DIM> grid, int nRefine) {
    Point<SPACE_DIM> nRefineP = Point<SPACE_DIM>.all(nRefine);
    return [grid.min() * nRefineP : (grid.max() + ONE) * nRefineP - ONE];
  }


  /** Coarsen a RectDomain by a given ratio. */
  public static RectDomain<SPACE_DIM> coarsen(RectDomain<SPACE_DIM> grid, int nRefine) {
    Point<SPACE_DIM> nRefineP = Point<SPACE_DIM>.all(nRefine);
    return (grid / nRefineP);
  }


  /** Coarsen a Domain by a given ratio. */
  public static Domain<SPACE_DIM> coarsen(Domain<SPACE_DIM> dom, int nRefine) {
    Point<SPACE_DIM> nRefineP = Point<SPACE_DIM>.all(nRefine);
    return (dom / nRefineP);
  }


  /** Refine all RectDomains in an array by the same ratio. */
  public static void refine(RectDomain<SPACE_DIM> [1d] grids, int nRefine) {
    Point<SPACE_DIM> nRefineP = Point<SPACE_DIM>.all(nRefine);
    foreach (ind in grids.domain())
      grids[ind] = [grids[ind].min() * nRefineP :
	(grids[ind].max() + ONE) * nRefineP - ONE];
  }


  /** Coarsen all RectDomains in an array by the same ratio. */
  public static void coarsen(RectDomain<SPACE_DIM> [1d] grids, int nRefine) {
    Point<SPACE_DIM> nRefineP = Point<SPACE_DIM>.all(nRefine);
    foreach (ind in grids.domain())
      grids[ind] = grids[ind] / nRefineP;
  }


  /** Coarsen all Domains in an array by the same ratio. */
  public static void coarsen(Domain<SPACE_DIM> [1d] doms, int nRefine) {
    Point<SPACE_DIM> nRefineP = Point<SPACE_DIM>.all(nRefine);
    foreach (ind in doms.domain())
      doms[ind] = doms[ind] / nRefineP;
  }
}
