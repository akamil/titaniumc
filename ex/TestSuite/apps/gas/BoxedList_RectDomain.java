class BoxedList_RectDomain {
  private static final int SPACE_DIM = Util.SPACE_DIM;

  private List_RectDomain l = null;

  public BoxedList_RectDomain() {
  }

  public BoxedList_RectDomain(
			      Region reg, // REGION
			      RectDomain<SPACE_DIM> x) {
    push(
	 reg, // REGION
	 x);
  }

  public void push(
		   Region reg, // REGION
		   RectDomain<SPACE_DIM> item) {
    l = new
      (reg) // REGION
      List_RectDomain(item, l);
  }

  public RectDomain<SPACE_DIM> pop() {
    RectDomain<SPACE_DIM> t = l.first();
    l = l.rest();
    return t;
  }

  public RectDomain<SPACE_DIM> first() {
    return l.first();
  }

  public List_RectDomain toList() {
    return l;
  }

  public RectDomain<SPACE_DIM> [1d] toArray(
					    Region reg // REGION
					    ) {
    RectDomain<SPACE_DIM> [1d] a = new
      (reg) // REGION
      RectDomain<SPACE_DIM> [[0 : length() - 1]];
    int i = 0;
    for (List_RectDomain r = l; r != null; r = r.rest()) {
      a[i] = r.first();
      i++;
    }
    return a;
  }

  public RectDomain<SPACE_DIM> [1d] toReversedArray(
						    Region reg // REGION
						    ) {
    int i = length();
    RectDomain<SPACE_DIM> [1d] a = new
      (reg) // REGION
      RectDomain<SPACE_DIM> [[0 : i - 1]];
    for (List_RectDomain r = l; r != null; r = r.rest()) {
      a[--i] = r.first();
    }
    return a;
  }

  public boolean isEmpty() {
    return l == null;
  }

  public int length() {
    return l == null ? 0 : l.length();
  }
}
