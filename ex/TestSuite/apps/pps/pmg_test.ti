import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.io.IOException;
import java.io.StreamTokenizer;

class pmg_test {
  static ExecutionLog local log;  // stuff to track execution.
  static int barrierCount;
  static final String bb = "begin barrier";
  static final String eb = "end barrier";

  public static sglobal void barrier() { 
    barrierCount++;
    log.append(2, bb);
    Ti.barrier();
    log.append(2, eb);
  }
  public static single void main(String[] args) {
    String phase;
    log = new ExecutionLog(2);
    phase = "Read inputs";
    log.begin(phase);

    /** Various defaults. */
    int reporting = 0;
    int C = 1;
    int star_shaped = 0;
    int Nref = 4;
    int id_border = 10;
    int LongDim = 128;
    int S = 9;
    int P = 0;
    int NumPatches = 1;
    int M = 0;
    boolean from_file = false;
    String filename;
    boolean solve = true;
    boolean report = false;
    double H;

    // System.out.println(Ti.numProcs() + " processors");

    barrier();

    /*if (Ti.thisProc() == 0) {*/
    try {
      for (int i = 0; (i < args.length && args[i].startsWith("-", 0)); 
	   i++) {
	if (args[i].startsWith("-C", 0)) {
	  C = Integer.parseInt(args[++i], 10);
	}
	else if (args[i].startsWith("-Nr", 0)) {
	  Nref = Integer.parseInt(args[++i], 10);
	}
	else if (args[i].startsWith("-Np", 0)) {
	  NumPatches = Integer.parseInt(args[++i], 10);
	}
	else if (args[i].startsWith("-I", 0)) {
	  id_border = Integer.parseInt(args[++i], 10);
	}
	else if (args[i].startsWith("-L", 0)) {
	  LongDim = Integer.parseInt(args[++i], 10);
	}
	else if (args[i].startsWith("-W", 0)) {
	  M = Integer.parseInt(args[++i], 10);
	}
	else if (args[i].startsWith("-st", 0)) {
	  star_shaped = 1;
	}
	else if (args[i].startsWith("-S", 0)) {
	  S = Integer.parseInt(args[++i], 10);
	}
	else if (args[i].startsWith("-P", 0)) {
	  P = Integer.parseInt(args[++i], 10);
	}
	else if (args[i].startsWith("-r", 0)) {
	  reporting = Integer.parseInt(args[++i], 10);
	}
	else if (args[i].startsWith("-no", 0)) {
	  solve = false;
	}
	else if (args[i].startsWith("-f", 0)) {
	  filename = args[++i];
	  from_file = true;
	}
	else if (args[i].startsWith("-R", 0)) {
	  report = true;
	}
      }
    }
    catch (NumberFormatException x) {
      System.err.println("There appear to be some poorly formatted arguments"
			 + " in the command line.");
      System.err.println("Generally, except for toggles, -n <int> is the kind"
			 + " of thing you want.");
      System.exit(-1);
    }

    if (report) {
      System.out.println("Processor " + Ti.thisProc());
    }
    log.end(phase);
      
    barrier();

    phase = "PMG setup";
    log.begin(phase);

    if (Ti.thisProc() == 0) {
      switch (S) {
      case 9:
	System.out.println("Doing 9-point method through coarse solve...");
	break;
      case 95:
	System.out.println("Using 5-point MG in higher-accuracy infinite domain solver...");
	break;
      case 59:
	System.out.println("Using 9-point MG in lower-accuracy infinite domain solver...");
	break;
      case 5:
	System.out.println("Using 5-point MG and lower-accuracy infinite domain solver for the local solves...");
	break;
      default:
	S = 9;
	System.out.println("Defaulting back to 9-point method: in future please use -S 9, 5, 59, or 95...");
	break;
      }
      System.out.println("Size: " + LongDim + "x" + LongDim 
			 + ", Total Patches: " + NumPatches*NumPatches
			 + ", Refinement Ratio: " + Nref
			 + ", Correction Radius: " + C);
    }

    RectDomain<2> interior = [ [0, 0] : [LongDim, LongDim] ];

    H = 1.0/LongDim;
    int single npatches = broadcast NumPatches from 0;

    charge [1d] charges;

    if (broadcast from_file from 0) {
      try { charges = parse_input_file(filename); }
      catch (Throwable x) {}
    }
    else {
      charges = new charge [1 : 1];
      charges[1] = new charge(0.48, 0.48, 0.42, M, star_shaped);
    }
    
    log.end(phase);

    phase = "PMG constructor";
    log.begin(phase);

    pmg single local myProblem = 
      new pmg(interior, npatches, C, Nref, id_border, H, S, charges, 
	      broadcast reporting from 0);

    log.end(phase);

    barrier();

    Timer totalTime = new Timer();
    if (Ti.thisProc() == 0) {
      totalTime.start();
    }

    if (broadcast solve from 0) {
      //      barrier();
      myProblem.solve();
      //      barrier();
    }

    barrier();

    if (Ti.thisProc() == 0) {
      totalTime.stop();
      System.out.println("Time taken: " + ( (double) totalTime.millis() 
					    / 1000.0 ));
    }

    barrier();
    myProblem.wrap_up();
    barrier();
    ExecutionLog.end(log);
    ExecutionLog.histDump();
  }

  public single static charge [1d] parse_input_file(String filename) {
    charge [1d] charges;
    if (Ti.thisProc() == 0) {
      try {
	FileInputStream in = new FileInputStream(filename); 
	StreamTokenizer st = new StreamTokenizer(in);

	int token, i, m, s;
	double c1, c2, r0;
	token = st.nextToken();
	i = (int) st.nval;

	if (i > 0) {
	  charges = new charge [1 : i];
	  foreach (p in charges.domain()) {
	    token = st.nextToken();
	    c1 = st.nval;
	    token = st.nextToken();
	    c2 = st.nval;
	    token = st.nextToken();
	    r0  = st.nval;
	    token = st.nextToken();
	    m = (int) st.nval;
	    token = st.nextToken();
	    s = (int) st.nval;
	    charges[p] = new charge(c1, c2, r0, m, s);
	  }
	}
      }
      catch (Throwable x) {}
    }
    return broadcast charges from 0;
  }
}











