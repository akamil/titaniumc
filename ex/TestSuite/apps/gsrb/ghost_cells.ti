class CF_ghost_cells {
  /* coarse_ghost_cells holds the set of points for which we will need 
     coarse grid data to set boundary conditions; the array is for the set
     of directions N, S, E, W (and T and B in 3d). */
  public coarse_ghost_face [1d] coarse_ghost_cells;

  public CF_ghost_cells() {}

  public CF_ghost_cells(

  /* Fill the coarse_ghost_cells' data. */
  /* This involves COMMUNICATION */
  public fill() {
    foreach (i in coarse_ghost_cells) {
      foreach (j in coarse_ghost_cells[i].coarse_soln.domain()) {
	coarse_ghost_cells[i].data.copy(coarse_ghost_cells[i].coarse_soln);
      }
    }
  }


}

immutable class coarse_ghost_face {
  /* face_domain only needs to be nDim - 1, but I think that would make 
     things only slightly more efficient and much more complicated. */
  public Domain<2> face_domain;
  public int dir;
  public double [1d] [2d] coarse_soln;
  public double [2d] data;

  /* Set up ghost_face given a direction d, the initial dimensions of
     the face, and an array of neighboring coarse solution values. */
  public ghost_face(int d, RectDomain<2> face, 
		    double [1d] [2d] coarse_neighbor_soln) {
    dir = d;
    coarse_soln = coarse_neighbor_soln;
    data = new double [face];

    /* Calculate the domain for this face. */
    Domain<2> temp_domain = face;
    foreach (i in coarse_neighbor_soln.domain()) {
      face -= coarse_neighbor_soln[i].domain().shrink(1);
    }
    face_domain = face;
}
