# -----------------------------------------------------------------------------
#
# Name: d*.tst
#
# Version: 1.0
#
# Creator: miyamoto
#
# Date: 980205
#
# Purpose:
#   Sanity test for domains.  It will create the necessary files for an
#   upper level tester to create an executable, run it, and compare the
#   output to an expected output.
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
class RectTest
{
  public static void main( String[] argv )
  {
    RectDomain<2> y;
    RectDomain<2> R = [1 : 9, 1 : 19];
    Point<2> mip = R.min(), mxp = R.max();
    int mi0 = mip[1], mx0 = mxp[1], mi1 = mip[2], mx1 = mxp[2];

    System.out.println("R = [1 : 10, 1 : 20]");
    System.out.print("R.min() = [");
    System.out.print(mi0);
    System.out.print(", ");
    System.out.print(mi1);
    System.out.println("]");
    System.out.print("R.max() = [");
    System.out.print(mx0);
    System.out.print(", ");
    System.out.print(mx1);
    System.out.println("]");

    System.out.println("All points in [1 : 3, 1 : 5]:");
    y = [1 : 2, 1 : 4];
    foreach (i in y) {
      System.out.print("(");
      System.out.print(i[1]);
      System.out.print(", ");
      System.out.print(i[2]);
      System.out.println(")");
    }
    System.out.println("All points in [[1,1] : [3,5]]:");
    y = [[1,1] : [2,4]];
    foreach (i in y) {
      System.out.print("(");
      System.out.print(i[1]);
      System.out.print(", ");
      System.out.print(i[2]);
      System.out.println(")");
    }
  }
}
@eof

cat > stdout.old << '@eof'
R = [1 : 10, 1 : 20]
R.min() = [1, 1]
R.max() = [9, 19]
All points in [1 : 3, 1 : 5]:
(1, 1)
(1, 2)
(1, 3)
(1, 4)
(2, 1)
(2, 2)
(2, 3)
(2, 4)
All points in [[1,1] : [3,5]]:
(1, 1)
(1, 2)
(1, 3)
(1, 4)
(2, 1)
(2, 2)
(2, 3)
(2, 4)
@eof

cat > stderr.old << '@eof'
@eof
