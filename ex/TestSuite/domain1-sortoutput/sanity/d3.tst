# -----------------------------------------------------------------------------
#
# Name: d*.tst
#
# Version: 1.0
#
# Creator: miyamoto
#
# Date: 980205
#
# Purpose:
#   Sanity test for domains.  It will create the necessary files for an
#   upper level tester to create an executable, run it, and compare the
#   output to an expected output.
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
class RectCastTest
{
  public static void main( String[] argv )
  {
    RectDomain<3> rd;
    Domain<3> d, d2, derr;

    rd = [[1,1,1] : [2,2,2] : [1,1,1]];
    d = (Domain<3>) rd;
    foreach (p in d) {
      System.out.print("p = [");
      System.out.print(p[1]);
      System.out.print(",");
      System.out.print(p[2]);
      System.out.print(",");
      System.out.print(p[3]);
      System.out.println("]");
    }
    System.out.println("---");
    rd = (RectDomain<3>) d;
    foreach (p in rd) {
      System.out.print("p = [");
      System.out.print(p[1]);
      System.out.print(",");
      System.out.print(p[2]);
      System.out.print(",");
      System.out.print(p[3]);
      System.out.println("]");
    }
    System.out.println("---");
    derr = d + [[2,2,2] : [3,3,3] : [1,1,1]];
    d = d + [[1,1,1] : [3,3,3] : [1,1,1]];
    System.out.print("d isRectangular: ");
    System.out.println(d.isRectangular());
    rd = (RectDomain<3>) d;
    foreach (p in rd) {
      System.out.print("p = [");
      System.out.print(p[1]);
      System.out.print(",");
      System.out.print(p[2]);
      System.out.print(",");
      System.out.print(p[3]);
      System.out.println("]");
    }
    System.out.println("---");
    d2 = d + [[2,2,2] : [3,3,3] : [1,1,1]];
    System.out.print("d2 isRectangular: ");
    System.out.println(d2.isRectangular());
    rd = (RectDomain<3>) d2;
    foreach (p in rd) {
      System.out.print("p = [");
      System.out.print(p[1]);
      System.out.print(",");
      System.out.print(p[2]);
      System.out.print(",");
      System.out.print(p[3]);
      System.out.println("]");
    }
/*
    System.out.println("---");
    System.out.print("derr isRectangular: ");
    System.out.println(derr.isRectangular());
    // The next line should be a runtime error.
    rd = (RectDomain<3>) derr;
    foreach (p in rd) {
      System.out.print("p = [");
      System.out.print(p[1]);
      System.out.print(",");
      System.out.print(p[2]);
      System.out.print(",");
      System.out.print(p[3]);
      System.out.println("]");
    }
*/
  }
}
@eof

cat > stdout.old << '@eof'
p = [1,1,1]
p = [1,1,2]
p = [1,2,1]
p = [1,2,2]
p = [2,1,1]
p = [2,1,2]
p = [2,2,1]
p = [2,2,2]
---
p = [1,1,1]
p = [1,1,2]
p = [1,2,1]
p = [1,2,2]
p = [2,1,1]
p = [2,1,2]
p = [2,2,1]
p = [2,2,2]
---
d isRectangular: true
p = [1,1,1]
p = [1,1,2]
p = [1,1,3]
p = [1,2,1]
p = [1,2,2]
p = [1,2,3]
p = [1,3,1]
p = [1,3,2]
p = [1,3,3]
p = [2,1,1]
p = [2,1,2]
p = [2,1,3]
p = [2,2,1]
p = [2,2,2]
p = [2,2,3]
p = [2,3,1]
p = [2,3,2]
p = [2,3,3]
p = [3,1,1]
p = [3,1,2]
p = [3,1,3]
p = [3,2,1]
p = [3,2,2]
p = [3,2,3]
p = [3,3,1]
p = [3,3,2]
p = [3,3,3]
---
d2 isRectangular: true
p = [1,1,1]
p = [1,1,2]
p = [1,1,3]
p = [1,2,1]
p = [1,2,2]
p = [1,2,3]
p = [1,3,1]
p = [1,3,2]
p = [1,3,3]
p = [2,1,1]
p = [2,1,2]
p = [2,1,3]
p = [2,2,1]
p = [2,2,2]
p = [2,2,3]
p = [2,3,1]
p = [2,3,2]
p = [2,3,3]
p = [3,1,1]
p = [3,1,2]
p = [3,1,3]
p = [3,2,1]
p = [3,2,2]
p = [3,2,3]
p = [3,3,1]
p = [3,3,2]
p = [3,3,3]
@eof
#---
#derr isRectangular: false
#Runtime: (Error) not a RectDomain in cast.

cat > stderr.old << '@eof'
@eof
