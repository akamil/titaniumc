# -----------------------------------------------------------------------------
#
# Name: d*.tst
#
# Version: 1.0
#
# Creator: miyamoto
#
# Date: 980205
#
# Purpose:
#   Sanity test for domains.  It will create the necessary files for an
#   upper level tester to create an executable, run it, and compare the
#   output to an expected output.
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
// Rect Domain

// Uses fake templates.  Replace V_ARITY with arity you want.

// import ti.domains.*;

class DomainIntersectTest {
  public static void main(String[] args) {

    Domain<3> d1;
    Domain<3> d2;
    Domain<3> d3;
    RectDomain<3> r;

    d1 = [[4,4,4]:[6,6,6]] + [[6,6,6]:[8,8,8]];
    // d2 = [[3,5,6]:[6,8,9]] + [[5,7,8]:[8,10,11]];
    d2 = [[4,4,4]:[7,7,7]] + [[5,5,5]:[8,8,8]];

    d3 = d1 * d2;
    d3 = d3 * d2;
    d3 = d3 * d1;
    foreach (p in d3) {
      System.out.print("p = [");
      System.out.print(p[1]);
      System.out.print(",");
      System.out.print(p[2]);
      System.out.print(",");
      System.out.print(p[3]);
      System.out.print("]");
      System.out.println("");
    }
    System.out.println("-----");
    r = [[5,5,4] : [5,6,4] : [0,1,0]] * [[5,4,4] : [7,4,7] : [1,0,1]];
    foreach (p in r) {
      System.out.print("p = [");
      System.out.print(p[1]);
      System.out.print(",");
      System.out.print(p[2]);
      System.out.print(",");
      System.out.print(p[3]);
      System.out.print("]");
      System.out.println("");
    }
  }
}
@eof

cat > stdout.old << '@eof'
p = [4,4,4]
p = [4,4,5]
p = [4,4,6]
p = [4,5,4]
p = [4,5,5]
p = [4,5,6]
p = [4,6,4]
p = [4,6,5]
p = [4,6,6]
p = [5,4,4]
p = [5,4,5]
p = [5,4,6]
p = [5,5,4]
p = [5,6,4]
p = [5,5,5]
p = [5,5,6]
p = [5,6,5]
p = [5,6,6]
p = [6,4,4]
p = [6,4,5]
p = [6,4,6]
p = [6,5,4]
p = [6,5,5]
p = [6,5,6]
p = [6,6,4]
p = [6,6,5]
p = [6,6,6]
p = [6,6,7]
p = [6,6,8]
p = [6,7,6]
p = [6,7,7]
p = [6,7,8]
p = [6,8,6]
p = [6,8,7]
p = [6,8,8]
p = [7,6,6]
p = [7,6,7]
p = [7,6,8]
p = [7,7,6]
p = [7,7,7]
p = [7,7,8]
p = [7,8,6]
p = [7,8,7]
p = [7,8,8]
p = [8,6,6]
p = [8,6,7]
p = [8,6,8]
p = [8,7,6]
p = [8,7,7]
p = [8,7,8]
p = [8,8,6]
p = [8,8,7]
p = [8,8,8]
-----
@eof

cat > stderr.old << '@eof'
@eof
