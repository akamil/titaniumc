# -----------------------------------------------------------------------------
#
# Name: pd_list.tst
#
# Version: 1.0
#
# Creator: miyamoto
#
# Date: 980205
#
# Purpose:
#   Test PointList and RectDomainList
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
// Test RectDomainList and PointList

class Main {

  public static void main(String[] args) {
    Domain<3> d1 = [ Point<3>.all(0) : Point<3>.all(-1) ];
    RectDomain<3> [1d] da1;
    Point<3> [1d] pa1;

    d1 = d1 + [ Point<3>.all(1) : Point<3>.all(3) ];
    d1 = d1 + [ Point<3>.all(2) : Point<3>.all(4) ];
    d1 = d1 + [ Point<3>.all(3) : Point<3>.all(5) ];

    da1 = d1.RectDomainList();
    pa1 = d1.PointList();

    System.out.println("da1:");
    foreach (p in da1.domain()) {
      System.out.println(da1[p].toString());
    }
    System.out.println("pa1:");
    foreach (p in pa1.domain()) {
      System.out.println(pa1[p].toString());
    }
  }
}
@eof

cat > stdout.old << '@eof'
da1:
[[1,1,1]:[1,3,3]:[1,1,1]]
[[2,1,1]:[3,1,3]:[1,1,1]]
[[2,2,1]:[3,3,1]:[1,1,1]]
[[2,2,2]:[2,4,4]:[1,1,1]]
[[3,2,2]:[4,2,4]:[1,1,1]]
[[3,3,2]:[4,4,2]:[1,1,1]]
[[3,3,3]:[5,5,5]:[1,1,1]]
pa1:
[1,1,1]
[1,1,2]
[1,1,3]
[1,2,1]
[1,2,2]
[1,2,3]
[1,3,1]
[1,3,2]
[1,3,3]
[2,1,1]
[2,1,2]
[2,1,3]
[3,1,1]
[3,1,2]
[3,1,3]
[2,2,1]
[2,3,1]
[3,2,1]
[3,3,1]
[2,2,2]
[2,2,3]
[2,2,4]
[2,3,2]
[2,3,3]
[2,3,4]
[2,4,2]
[2,4,3]
[2,4,4]
[3,2,2]
[3,2,3]
[3,2,4]
[4,2,2]
[4,2,3]
[4,2,4]
[3,3,2]
[3,4,2]
[4,3,2]
[4,4,2]
[3,3,3]
[3,3,4]
[3,3,5]
[3,4,3]
[3,4,4]
[3,4,5]
[3,5,3]
[3,5,4]
[3,5,5]
[4,3,3]
[4,3,4]
[4,3,5]
[4,4,3]
[4,4,4]
[4,4,5]
[4,5,3]
[4,5,4]
[4,5,5]
[5,3,3]
[5,3,4]
[5,3,5]
[5,4,3]
[5,4,4]
[5,4,5]
[5,5,3]
[5,5,4]
[5,5,5]
@eof

cat > stderr.old << '@eof'
@eof
