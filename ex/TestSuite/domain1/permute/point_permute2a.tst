# -----------------------------------------------------------------------------
#
# Name: *permute*.tst
#
# Version: 1.0
#
# Creator: miyamoto
#
# Date: 990209
#
# Purpose:
#   Test the permute method on domains, rectdomans and points.
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
// Permute on points

class Test {

  public static void main(String[] args) {
    System.out.print("[1,2] permuted [1,1] = ");
    System.out.println(([1,2]).permute([1,1]).toString());
  }
}
@eof

cat > stdout.old << '@eof'
@eof

cat > stderr.old << '@eof'
tc-gen-X/domains.h:335: failed assertion `(("Point<2> does not result in a valid permute operation"),( 0))'
@eof
