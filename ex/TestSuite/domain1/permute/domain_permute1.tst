# -----------------------------------------------------------------------------
#
# Name: *permute*.tst
#
# Version: 1.0
#
# Creator: miyamoto
#
# Date: 990209
#
# Purpose:
#   Test the permute method on domains, rectdomans and points.
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
// Permute on domains

class Test {

  public static void main(String[] args) {
    System.out.println("RectDomain:");
    System.out.print("[[1,2]:[20,30]:[1,2]] permuted [1,2] = ");
    System.out.println(([[1,2]:[20,30]:[1,2]]).permute([1,2]).toString());
    System.out.print("[[1,2]:[20,30]:[1,2]] permuted [2,1] = ");
    System.out.println(([[1,2]:[20,30]:[1,2]]).permute([2,1]).toString());

    Domain<2> d = [[1,2]:[20,30]:[1,2]];
    System.out.println("Domain:");
    System.out.print("[[1,2]:[20,30]:[1,2]] permuted [1,2] = ");
    System.out.println(d.permute([1,2]).toString());
    System.out.print("[[1,2]:[20,30]:[1,2]] permuted [2,1] = ");
    System.out.println(d.permute([2,1]).toString());
  }
}
@eof

cat > stdout.old << '@eof'
RectDomain:
[[1,2]:[20,30]:[1,2]] permuted [1,2] = [[1,2]:[20,30]:[1,2]]
[[1,2]:[20,30]:[1,2]] permuted [2,1] = [[2,1]:[30,20]:[2,1]]
Domain:
[[1,2]:[20,30]:[1,2]] permuted [1,2] = [[1,2]:[20,30]:[1,2]]
[[1,2]:[20,30]:[1,2]] permuted [2,1] = [[2,1]:[30,20]:[2,1]]
@eof

cat > stderr.old << '@eof'
@eof
