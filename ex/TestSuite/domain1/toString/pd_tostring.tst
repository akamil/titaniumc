# -----------------------------------------------------------------------------
#
# Name: pd_tostring.tst
#
# Version: 1.0
#
# Creator: miyamoto
#
# Date: 990209
#
# Purpose:
#   Test the toString method on domains, rectdomans and points.
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
// Test toString on points, domains, and rectdomains

class Test {

  public static void main(String[] args) {
    System.out.println(([1,2]).toString());
    System.out.println(([[1,1]:[2,2]]).toString());
    System.out.println(([[1,1]:[2,2]] + [[2,2]:[3,3]]).toString());
    System.out.println("[1,2] = " + ([1,2]));
  }
}
@eof

cat > stdout.old << '@eof'
[1,2]
[[1,1]:[2,2]:[1,1]]
[[1,1]:[1,2]:[1,1]] + [[2,1]:[2,1]:[1,1]] + [[2,2]:[3,3]:[1,1]]
[1,2] = [1,2]
@eof

cat > stderr.old << '@eof'
@eof
