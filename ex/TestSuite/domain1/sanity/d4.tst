# -----------------------------------------------------------------------------
#
# Name: d*.tst
#
# Version: 1.0
#
# Creator: miyamoto
#
# Date: 980205
#
# Purpose:
#   Sanity test for domains.  It will create the necessary files for an
#   upper level tester to create an executable, run it, and compare the
#   output to an expected output.
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
// Rect Domain

// Uses fake templates.  Replace V_ARITY with arity you want.

// import ti.domains.*;

class RectDomainSliceTest {

  public static void main(String[] args) {

    RectDomain<3> rd3;
    RectDomain<2> rd2;
    RectDomain<1> rd1;
    Point<3> p3;

    p3 = [7,8,9];
    rd3 = [p3 : [ 9,12,15 ] : [ 1,2,3 ]];
    rd2 = rd3.slice(2);
    foreach (p in rd2) {
      System.out.print("p = [");
      System.out.print(p[1]);
      System.out.print(",");
      System.out.print(p[2]);
      System.out.println("]");
    }
    System.out.println("---");
    rd1 = rd3.slice(2).slice(1);
    foreach (p in rd1) {
      System.out.print("p = [");
      System.out.print(p[1]);
      System.out.println("]");
    }
  }
}
@eof

cat > stdout.old << '@eof'
p = [7,9]
p = [7,12]
p = [7,15]
p = [8,9]
p = [8,12]
p = [8,15]
p = [9,9]
p = [9,12]
p = [9,15]
---
p = [9]
p = [12]
p = [15]
@eof

cat > stderr.old << '@eof'
@eof
