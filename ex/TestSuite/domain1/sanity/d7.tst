# -----------------------------------------------------------------------------
#
# Name: d*.tst
#
# Version: 1.0
#
# Creator: miyamoto
#
# Date: 980205
#
# Purpose:
#   Sanity test for domains.  It will create the necessary files for an
#   upper level tester to create an executable, run it, and compare the
#   output to an expected output.
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
// Rect Domain

// Uses fake templates.  Replace V_ARITY with arity you want.

// import ti.domains.*;

class RectDomainPointDivTest {
  public static void main(String[] args) {

    RectDomain<3> rd;
    RectDomain<3> rd2;
    Point<3> p_div;
    Point<3> p_lwb;
    Point<3> p_upb;
    Point<3> p_stride;

    Domain<3> x = (Domain<3>)([[0, 0, 0]:[-1, -1, -1]]);

    rd = [[4,3,3] : [4,4,4] : [0,1,1]];
    p_div = [2,2,2];
    rd = rd / p_div;
    p_lwb = rd.lwb();
    p_upb = rd.upb();
    p_stride = rd.stride();
    System.out.print("rd = [[");
    System.out.print(p_lwb[1]); System.out.print(",");
    System.out.print(p_lwb[2]); System.out.print(",");
    System.out.print(p_lwb[3]); System.out.print("]:[");
    System.out.print(p_upb[1]); System.out.print(",");
    System.out.print(p_upb[2]); System.out.print(",");
    System.out.print(p_upb[3]); System.out.print("]:[");
    System.out.print(p_stride[1]); System.out.print(",");
    System.out.print(p_stride[2]); System.out.print(",");
    System.out.print(p_stride[3]); System.out.print("]]");
    System.out.println("");
    foreach (p in rd) {
      System.out.print("p = [");
      System.out.print(p[1]);
      System.out.print(",");
      System.out.print(p[2]);
      System.out.print(",");
      System.out.print(p[3]);
      System.out.print("]");
      System.out.println("");
    }
    System.out.println("-----");
    rd2 = [[4,3,3] : [4,8,8] : [0,2,2]] / [3,3,3];
    foreach (p in rd2) {
      System.out.print("p = [");
      System.out.print(p[1]);
      System.out.print(",");
      System.out.print(p[2]);
      System.out.print(",");
      System.out.print(p[3]);
      System.out.print("]");
      System.out.println("");
    }
    System.out.println("-----");
    rd2 = [[4,0,0] : [4,8,8] : [0,4,4]] / [2,2,2];
    foreach (p in rd2) {
      System.out.print("p = [");
      System.out.print(p[1]);
      System.out.print(",");
      System.out.print(p[2]);
      System.out.print(",");
      System.out.print(p[3]);
      System.out.print("]");
      System.out.println("");
    }
  }
}
@eof

cat > stdout.old << '@eof'
rd = [[2,1,1]:[3,3,3]:[1,1,1]]
p = [2,1,1]
p = [2,1,2]
p = [2,2,1]
p = [2,2,2]
-----
p = [1,1,1]
p = [1,1,2]
p = [1,2,1]
p = [1,2,2]
-----
p = [2,0,0]
p = [2,0,2]
p = [2,0,4]
p = [2,2,0]
p = [2,2,2]
p = [2,2,4]
p = [2,4,0]
p = [2,4,2]
p = [2,4,4]
@eof

cat > stderr.old << '@eof'
@eof
