# -----------------------------------------------------------------------------
#
# Name: d*.tst
#
# Version: 1.0
#
# Creator: miyamoto
#
# Date: 980205
#
# Purpose:
#   Sanity test for domains.  It will create the necessary files for an
#   upper level tester to create an executable, run it, and compare the
#   output to an expected output.
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
class RectTest
{
  public static void main( String[] argv )
  {
    RectDomain<1> R = [1 : 3];
    Point<1> mip = R.min(), mxp = R.max();
    int mi = mip[1], mx = mxp[1];

    System.out.println("R = [1 : 4]");
    System.out.println(mi);
    System.out.println(mx);
    foreach (i in R) {
      System.out.println(i[1]);
    }
  }
}

@eof

cat > stdout.old << '@eof'
R = [1 : 4]
1
3
1
2
3
@eof

cat > stderr.old << '@eof'
@eof
