# -----------------------------------------------------------------------------
#
# Name: d*.tst
#
# Version: 1.0
#
# Creator: miyamoto
#
# Date: 980205
#
# Purpose:
#   Sanity test for domains.  It will create the necessary files for an
#   upper level tester to create an executable, run it, and compare the
#   output to an expected output.
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
// Rect Domain

// Uses fake templates.  Replace V_ARITY with arity you want.

// import ti.domains.*;

immutable class ThisIsImmutable {
  int x;
  public ThisIsImmutable(int new_x) {
    x = new_x;
  }
  public ThisIsImmutable() {
    x = 1;
  }
  public int get() {
    return x;
  }
}

class Tester {

  public static void main(String[] args) {
    // ThisIsImmutable t;
    ThisIsImmutable t = new ThisIsImmutable(2);
    // ThisIsImmutable t = ThisIsImmutable(2);

    System.out.print("Number is ");
    System.out.println(t.get());
  }
}
@eof

cat > stdout.old << '@eof'
Number is 2
@eof

cat > stderr.old << '@eof'
@eof
