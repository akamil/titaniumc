# -----------------------------------------------------------------------------
#
# Name: literals.tst
#
# Version: 1.0
#
# Creator: liblit
#
# Date: 981001
#
# Purpose:
#   Sanity test for domain literal syntaxes.  It will create the
#   necessary files for an upper level tester to create an executable,
#   run it, and compare the output to an expected output.
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
class LiteralSyntaxTest
{
    static public String literate( Point<3> point )
    {
	return "[ "
	    + point[1] + ", "
	    + point[2] + ", "
	    + point[3]
	    + " ]";
    }
    
    static public String literate( RectDomain<3> domain )
    {
	return literate( domain.min() ) + " : "
	    +  literate( domain.max() ) + " : "
	    +  literate( domain.stride() );
    }

    static public void show( RectDomain<3> domain )
    {
	System.out.println( literate( domain ) );
    }

    static public void main( String[] argv )
    {
	show( [ [ 1, 2, 3 ] : [ 101, 202, 303 ] : [ 10, 20, 30 ] ] );
	show( [ [ 1, 2, 3 ] : [ 101, 202, 303 ]                  ] );
	show( [ 1 : 101 : 10, 2 : 202 : 20, 3 : 303 : 30 ] );
	show( [ 1 : 101,      2 : 202,      3 : 303      ] );
    }
}

@eof

cat > stdout.old << '@eof'
[ 1, 2, 3 ] : [ 101, 202, 303 ] : [ 10, 20, 30 ]
[ 1, 2, 3 ] : [ 101, 202, 303 ] : [ 1, 1, 1 ]
[ 1, 2, 3 ] : [ 101, 202, 303 ] : [ 10, 20, 30 ]
[ 1, 2, 3 ] : [ 101, 202, 303 ] : [ 1, 1, 1 ]
@eof

cat > stderr.old << '@eof'
@eof
