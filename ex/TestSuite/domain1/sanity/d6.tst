# -----------------------------------------------------------------------------
#
# Name: d*.tst
#
# Version: 1.0
#
# Creator: miyamoto
#
# Date: 980205
#
# Purpose:
#   Sanity test for domains.  It will create the necessary files for an
#   upper level tester to create an executable, run it, and compare the
#   output to an expected output.
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
// Rect Domain

// Uses fake templates.  Replace V_ARITY with arity you want.

// import ti.domains.*;

class RectDomainNullTest {
  public static void main(String[] args) {

    RectDomain<3> rd1;
    RectDomain<3> rd2;
    Domain<3> d1;
    Domain<3> d2;

    rd1 = [[7,8,9] : [ 9,12,15 ] : [ 1,2,3 ]];
    rd2 = [[7,8,9] : [ 5,6,7 ] : [ 1,2,3 ]];
    d1 = rd1 - rd1;
    System.out.print("rd1.isEmpty() = ");
    System.out.println(rd1.isEmpty());
    System.out.print("rd1.size = ");
    System.out.println(rd1.size());
    System.out.print("rd2.isEmpty() = ");
    System.out.println(rd2.isEmpty());
    System.out.print("rd2.size = ");
    System.out.println(rd2.size());
    System.out.print("d1.isEmpty() = ");
    System.out.println(d1.isEmpty());
    System.out.print("d1.size = ");
    System.out.println(d1.size());

    foreach (p in rd1) {
      System.out.print("p = [");
      System.out.print(p[1]);
      System.out.print(",");
      System.out.print(p[2]);
      System.out.print(",");
      System.out.print(p[3]);
      System.out.print("]");
      System.out.print(" rd1.contains(p) = ");
      System.out.println(rd1.contains(p));
    }

    d2 = (Domain<3>) rd1;
    foreach (p in d2) {
      System.out.print("p = [");
      System.out.print(p[1]);
      System.out.print(",");
      System.out.print(p[2]);
      System.out.print(",");
      System.out.print(p[3]);
      System.out.print("]");
      System.out.print(" d2.contains(p) = ");
      System.out.println(d2.contains(p));
    }
  }
}
@eof

cat > stdout.old << '@eof'
rd1.isEmpty() = false
rd1.size = 27
rd2.isEmpty() = true
rd2.size = 0
d1.isEmpty() = true
d1.size = 0
p = [7,8,9] rd1.contains(p) = true
p = [7,8,12] rd1.contains(p) = true
p = [7,8,15] rd1.contains(p) = true
p = [7,10,9] rd1.contains(p) = true
p = [7,10,12] rd1.contains(p) = true
p = [7,10,15] rd1.contains(p) = true
p = [7,12,9] rd1.contains(p) = true
p = [7,12,12] rd1.contains(p) = true
p = [7,12,15] rd1.contains(p) = true
p = [8,8,9] rd1.contains(p) = true
p = [8,8,12] rd1.contains(p) = true
p = [8,8,15] rd1.contains(p) = true
p = [8,10,9] rd1.contains(p) = true
p = [8,10,12] rd1.contains(p) = true
p = [8,10,15] rd1.contains(p) = true
p = [8,12,9] rd1.contains(p) = true
p = [8,12,12] rd1.contains(p) = true
p = [8,12,15] rd1.contains(p) = true
p = [9,8,9] rd1.contains(p) = true
p = [9,8,12] rd1.contains(p) = true
p = [9,8,15] rd1.contains(p) = true
p = [9,10,9] rd1.contains(p) = true
p = [9,10,12] rd1.contains(p) = true
p = [9,10,15] rd1.contains(p) = true
p = [9,12,9] rd1.contains(p) = true
p = [9,12,12] rd1.contains(p) = true
p = [9,12,15] rd1.contains(p) = true
p = [7,8,9] d2.contains(p) = true
p = [7,8,12] d2.contains(p) = true
p = [7,8,15] d2.contains(p) = true
p = [7,10,9] d2.contains(p) = true
p = [7,10,12] d2.contains(p) = true
p = [7,10,15] d2.contains(p) = true
p = [7,12,9] d2.contains(p) = true
p = [7,12,12] d2.contains(p) = true
p = [7,12,15] d2.contains(p) = true
p = [8,8,9] d2.contains(p) = true
p = [8,8,12] d2.contains(p) = true
p = [8,8,15] d2.contains(p) = true
p = [8,10,9] d2.contains(p) = true
p = [8,10,12] d2.contains(p) = true
p = [8,10,15] d2.contains(p) = true
p = [8,12,9] d2.contains(p) = true
p = [8,12,12] d2.contains(p) = true
p = [8,12,15] d2.contains(p) = true
p = [9,8,9] d2.contains(p) = true
p = [9,8,12] d2.contains(p) = true
p = [9,8,15] d2.contains(p) = true
p = [9,10,9] d2.contains(p) = true
p = [9,10,12] d2.contains(p) = true
p = [9,10,15] d2.contains(p) = true
p = [9,12,9] d2.contains(p) = true
p = [9,12,12] d2.contains(p) = true
p = [9,12,15] d2.contains(p) = true
@eof

cat > stderr.old << '@eof'
@eof
