# -----------------------------------------------------------------------------
#
# Name: neg_stride.tst
#
# Version: 1.0
#
# Creator: miyamoto
#
# Date: 990626
#
# Purpose:
#   Test the that using negative stride values generates an error
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
// Test for negative strides

class Main {

  public static void main(String[] args) {
    Domain<1> D1 = [1:10:-1];
    //No error on D2 - it's empty, so stride is irrelevant
    Domain<1> D2 = [10:1:-1];
    Domain<2> D3 = [1:10:-1, 1:10:+1];
    //Ditto for D4 and 5
    Domain<2> D4 = [10:1:+1, 10:1:-1];
    Domain<2> D5 = [10:1:-1, 10:1:-1];

    // prevent above from being optimized away by tc
    Domain<1> D6 = D1+D2;
    Domain<2> D7 = D3+D4+D5;
    String s = "" + D6 + D7;
  }
}
@eof

cat > stdout.old << '@eof'
Runtime: (Error) stride may not contain a negative Point<N> value.
Runtime: (Error) stride may not contain a negative Point<N> value.
@eof

cat > stderr.old << '@eof'
@eof
