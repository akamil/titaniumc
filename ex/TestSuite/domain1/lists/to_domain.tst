# -----------------------------------------------------------------------------
#
# Name: to_domain.tst
#
# Version: 1.0
#
# Creator: miyamoto
#
# Date: 980205
#
# Purpose:
#   Test R.toDomain
#
# Change Log:
#
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
// Test toDomain from a RectDomainList or a PointList

class Main {

  public static void main(String[] args) {
    Domain<3> d1 = [ Point<3>.all(0) : Point<3>.all(-1) ];
    RectDomain<3> [1d] da1;
    Point<3> [1d] pa1;

    d1 = d1 + [ Point<3>.all(1) : Point<3>.all(3) ];
    d1 = d1 + [ Point<3>.all(2) : Point<3>.all(4) ];
    d1 = d1 + [ Point<3>.all(3) : Point<3>.all(5) ];

    da1 = d1.RectDomainList();
    pa1 = d1.PointList();

    Domain<3> d2 = Domain<3>.toDomain(da1);
    Domain<3> d3 = Domain<3>.toDomain(pa1);

    if (!d2.equals(d3) || !d1.equals(d2) || !d1.equals(d3)) {
      System.out.println("Error. Domains do not match.");
    }
  }
}
@eof

cat > stdout.old << '@eof'
@eof

cat > stderr.old << '@eof'
@eof
