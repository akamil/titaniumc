# -----------------------------------------------------------------------------
#
# Name: dotEquals.tst
#
# Version: 1.1
#
# Creator: ihaque
#
# Date: 040713
#
# Purpose:
#   Test the .equals method on domains and rectdomains.
#
# Change Log:
#
# 1.1: 7-16-04: removed calls to Domain<N>.op== and RectDomain<N>.op==(Domain<N>)
# -----------------------------------------------------------------------------

cat > test.ti << '@eof'
// Test .equals on Domains and RectDomains

#define PL System.out.println
class testDotEquals {
public static void main(String[] args) {

RectDomain<1> rd1=[1:5];
RectDomain<1> rd2=[0:-1];
PL("RD false equality: "+(((rd1==rd2)==(rd1.equals(rd2)))?"PASS":"FAIL"));
PL("RD false equality reversed: "+(((rd1==rd2)==(rd2.equals(rd1)))?"PASS":"FAIL"));
PL("RD true nonnull equality: "+(((rd1==rd1)==(rd1.equals(rd1)))?"PASS":"FAIL"));
PL("RD true null equality: "+(((rd2==rd2)==(rd2.equals(rd2)))?"PASS":"FAIL"));

Domain<1> d1=rd1;
Domain<1> d2=rd2;
Domain<1> d3=rd1+[7:10];
Domain<1> d4=[1:3]+[2:5];

Object o1=d1;
Object o2=d2;

PL("D false nonnull-null equality: "+((d1.equals(d2)==false)?"PASS":"FAIL"));
PL("D false null-nonnull equality: "+((d2.equals(d1)==false)?"PASS":"FAIL"));
PL("D true nonnull equality: "+      ((d1.equals(d1)==true)?"PASS":"FAIL"));
PL("D true null equality: "+         ((d2.equals(d2)==true)?"PASS":"FAIL"));
PL("D true multirect nonnull equality: "+((d3.equals(d3)==true)?"PASS":"FAIL"));
PL("D false multirect nonnull equality: "+((d3.equals(d4)==false)?"PASS":"FAIL"));
PL("D true RD-multirect equality: "+((rd1.equals(d4)==true)?"PASS":"FAIL"));
PL("RD true RD-multirect equality: "+((rd1.equals(d4)==true)?"PASS":"FAIL"));
PL("D false rect-multi equality: "+((d1.equals(d3)==false)?"PASS":"FAIL"));
PL("D false multi-rect equality: "+((d3.equals(d1)==false)?"PASS":"FAIL"));

PL("RD-nonRD Object equality: "+((rd1.equals("hello world"))?"FAIL":"PASS"));
PL("RD-null Object equality: "+((rd1.equals((Object)null))?"FAIL":"PASS"));
PL("empty RD-nonRD Object equality: "+((rd2.equals("hello world"))?"FAIL":"PASS"));
PL("empty RD-null Object equality: "+((rd2.equals((Object)null))?"FAIL":"PASS"));

PL("D-nonD Object equality: "+((d3.equals("hello world"))?"FAIL":"PASS"));
PL("D-null Object equality: "+((d3.equals((Object)null))?"FAIL":"PASS"));
PL("empty D-nonRD Object equality: "+((d2.equals("hello world"))?"FAIL":"PASS"));
PL("empty D-null Object equality: "+((d2.equals((Object)null))?"FAIL":"PASS"));

PL("RD-RD Object equality: "+((rd1.equals(o1))?"PASS":"FAIL"));
PL("RD-RD Object inequality: "+((rd1.equals(o2))?"FAIL":"PASS"));
PL("D Object-D object equality: "+((o1.equals((Object)d1))?"PASS":"FAIL"));
PL("D Object-D object inequality: "+((o1.equals(o2))?"FAIL":"PASS"));

return;
}
}
@eof

cat > stdout.old << '@eof'
RD false equality: PASS
RD false equality reversed: PASS
RD true nonnull equality: PASS
RD true null equality: PASS
D false nonnull-null equality: PASS
D false null-nonnull equality: PASS
D true nonnull equality: PASS
D true null equality: PASS
D true multirect nonnull equality: PASS
D false multirect nonnull equality: PASS
D true RD-multirect equality: PASS
RD true RD-multirect equality: PASS
D false rect-multi equality: PASS
D false multi-rect equality: PASS
RD-nonRD Object equality: PASS
RD-null Object equality: PASS
empty RD-nonRD Object equality: PASS
empty RD-null Object equality: PASS
D-nonD Object equality: PASS
D-null Object equality: PASS
empty D-nonRD Object equality: PASS
empty D-null Object equality: PASS
RD-RD Object equality: PASS
RD-RD Object inequality: PASS
D Object-D object equality: PASS
D Object-D object inequality: PASS
@eof

cat > stderr.old << '@eof'
@eof

