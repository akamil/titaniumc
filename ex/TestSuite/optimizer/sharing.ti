/*
This is a contrived example demonstrating the use of sharing and local 
inference to improve def-use analysis results, leading to more 
aggressive loop-invariant code motion.

To observe the change, run:
tcbuild -O --tc-flags "-stats -O{no}sharing -O{no}local" sharing.ti
and watch the "lifting" lines of the loop stats output with and without
the various inferences
*/


public class sharing {
  public static single void main(String[]args) {
    am(); bm(); cm();
    }


// here we get lifting with no help from inference
  public static single void am() {
    int [] b = null;
    b = broadcast new int[50] from 0; // force b to become global, shared pointer
    int [] local nonshared a = new int[50] nonshared;
    int ind = -1;
    foreach (p in [0:49]) {
      b[p[1]] = p[1];
      ind = a[25];  
        // this assignment should get lifted, because the use of a can 
        // be disambiguated from b based on sharing information
      }
   if (ind != 0) System.out.println("test failed");
   else System.out.println("test succeeded");
   }

// here we get lifting if -Osharing enabled
  public static single void bm() {
    int [] b = null;
    b = broadcast new int[50] from 0; // force b to become global, shared pointer
    int [] local a = new int[50];
    int ind = -1;
    foreach (p in [0:49]) {
      b[p[1]] = p[1];
      ind = a[25];  
        // this assignment should get lifted, because the use of a can 
        // be disambiguated from b based on sharing information
      }
   if (ind != 0) System.out.println("test failed");
   else System.out.println("test succeeded");
   }

// here we get lifting if -Olocal and -Osharing enabled
  public static single void cm() {
    int [] b = null;
    b = broadcast new int[50] from 0; // force b to become global, shared pointer
    int [] a = new int[50];
    int ind = -1;
    foreach (p in [0:49]) {
      b[p[1]] = p[1];
      ind = a[25];  
        // this assignment should get lifted, because the use of a can 
        // be disambiguated from b based on sharing information
      }
   if (ind != 0) System.out.println("test failed");
   else System.out.println("test succeeded");
   }
 }
 
