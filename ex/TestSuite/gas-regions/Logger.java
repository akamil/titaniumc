public class Logger {

  static ExecutionLog local log;

  static int barrierCount;
  static final String bb = "begin barrier";
  static final String eb = "end barrier";

  public static sglobal void barrier() { 
    barrierCount++;
    log.append(bb);
    Ti.barrier();
    log.append(eb);
  }

  single public static void begin() {
    log = new ExecutionLog();
    barrierCount = 0;
  }

  single public static void end() {
    ExecutionLog.end(log);
  }

  public static void append(String s) {
    log.append(s);
  }
}
