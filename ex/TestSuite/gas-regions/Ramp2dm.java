import java.io.*;

public class Ramp2dm {

  static final int dimensions = 2;
  static final RectDomain<1> Ddims = [1:dimensions], Ddirs = [-1:1:2];

  protected static double [1d][1d] ends = new double[Ddims][Ddirs];

  protected static double shockloc = 0.2;  // originally 0.1
  protected static double shockangle = 30.0 * (Math.PI / 180.0);
  protected static double slope = 1.0 / Math.tan(shockangle);
  protected static double shockloctop, shockspeed;

  protected static double gamma = 1.4;
  protected static double smallthresh = 1e-6;

  protected static double p0 = 1.0, c0 = 1.0, rho0 = gamma * p0 / (c0 * c0);
  protected static double mach = 10.0, s1 = mach * c0;

  protected static double p1 = p0 * (2.0 * gamma * mach * mach - gamma + 1.0) /
    (gamma + 1.0);
  protected static double v1 = (p1 - p0) / (s1 * rho0);
  protected static double ux1 = v1 * Math.cos(shockangle);
  protected static double uy1 = -v1 * Math.sin(shockangle);
  protected static double rho1 = rho0 / (1.0 - v1 / s1);

  protected static Quantities2 q0, q1;

  protected static Vector4 [1d] q0flux, q1flux;

  single public static void main(String args[]) {

    int single numProcs = Ti.numProcs();
    RectDomain<1> single Dprocs = [0 : numProcs-1];
    int myProc = Ti.thisProc();

    /*
      Get command-line arguments:  inputfile, s, totaltime, outputfile
    */

    boolean myprintOut, myreadFile;
    String myinputfile, myoutputfile, mygridhfile;
    int mys;
    double mytotaltime;

    try {
      int myarglen = args.length;
      if (myarglen < 3) exitinfo();
      myinputfile = args[0];
      mys = Integer.parseInt(args[1]);
      mytotaltime = new Double(args[2]).doubleValue();
      myprintOut = (myarglen >= 4);
      if (myprintOut) myoutputfile = args[3];
      myreadFile = (myarglen >= 5);
      if (myreadFile) mygridhfile = args[4];
    } catch (Exception e) {
      exitinfo();
    }

    int single s = broadcast mys from 0;
    double single totaltime = broadcast mytotaltime from 0;
    boolean single printOut = broadcast myprintOut from 0;
    boolean single readFile = broadcast myreadFile from 0;

    /*
      Set up Amr2 with global parameters in myinputfile.
    */

    DataInputStream in =
      Amr2.SetupInput(null, myinputfile, new Ramp2dmBoundaryFlux());

    /*
      Initialize processes.
    */

    System.out.println("initializing process " + myProc); 

    // The "single" modifier makes it look as if this sets all the
    // processes to be the same.
    Amr2.myself = new Amr2Process(null);
    Amr2.processes = new Amr2Process[Dprocs];
    Amr2.processes.exchange(Amr2.myself);

    /*
      Real coordinates of sides of computational domain.
    */

    ends[1][-1] = 0.0;
    ends[1][+1] = 1.0;
    ends[2][-1] = 0.0;
    ends[2][+1] = 0.25;

    /*
      Set initial primitive quantities.
    */

    Quantities2.gammaSet(gamma);
    Quantities2.setThresholds(smallthresh, smallthresh);

    // recall order rho, ux, uy, p

    q0 = new Quantities2(rho0, 0.0, 0.0, p0);
    q1 = new Quantities2(rho1, ux1, uy1, p1);

    /*
      These are used for finding boundary fluxes while solving.
    */

    q0flux = new Vector4[Ddims];
    q1flux = new Vector4[Ddims];
    foreach (pd in Ddims) {
      q0flux[pd] = q0.solidBndryFlux(pd[1]);
      q1flux[pd] = q1.getFlux(pd[1]);
    }
    shockloctop = shockloc +
      (ends[2][+1] - ends[2][-1]) * Math.tan(shockangle);
    shockspeed = s1 / Math.cos(shockangle);

    /*
      Read in patches.
    */

    double single time0 = 0.0;

    if (readFile) {
      time0 = ReadHierarchy.read(mygridhfile);
      // Read in hierarchy.
      // Need to run load balancing for levels > 0.
      // Idea:
      // For level 0, assign as before.
      // For levels > 0, put ALL patches into proc 0.
      // Then get U from q.  Also find adjacent, coarser.
      // Then regrid to get something more balanced.
      double timediscard = Amr2.myself.Process(s, totaltime, time0, true);
    } else {
      Amr2.myself.GetPatches(new Ramp2dmInit());
    }

    /*
      Process the patches.
    */

    // start logging
    Logger.begin();

    long startTime = System.currentTimeMillis();

    double timesim = Amr2.myself.Process(s, totaltime, time0, false);

    // end logging
    Logger.end();

    long endTime = System.currentTimeMillis();
    Ti.barrier();

    /*
      Output
    */

    System.out.println("time = " + (endTime - startTime) / 1000 + " sec");

    if (myProc == 0 && printOut) WriteHierarchy.write(myoutputfile, timesim);
  }


  private final static void exitinfo() {
    if (Ti.thisProc() == 0) {
      System.out.println("Usage:  ./Ramp2dm [inputfile] [s] [t] [[outputfile]] [[[gridhierarchyfile]]]");
      System.out.println("        [s] is number of time steps, or 0 if using [t]")
;
      System.out.println("        [t] is total time, or 0 if using [s]");
      System.exit(0);
    }
  }
}

