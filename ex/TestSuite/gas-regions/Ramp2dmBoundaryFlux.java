public class Ramp2dmBoundaryFlux extends Ramp2dm implements BoundaryFlux2 {

  // Perhaps use the physical boundary information from level?
  // Right now, all I use from level is ds.

  public final void f(Quantities2 [2d] q,
		      Level level, int sdim, double time,
                      Vector4 [2d] flux) {

    RectDomain<2> Dside = q.domain();
    int dim = (sdim > 0) ? sdim : -sdim;
    int side = (sdim > 0) ? 1 : -1;

    RectDomain<2> Dflux = Dside + Amr2.edge[dim][side];

    // System.out.println("Setting flux at the boundary " +
    // Format.toString(Dflux));
//    System.out.println("Setting flux at the boundary " +
//		       Format.toString(Dflux, dim));
    
    if (sdim == -1) {  // Left side
      foreach (e in Dflux) flux[e] = q1flux[dim];

    } else if (sdim == +1) {  // Right side
      foreach (e in Dflux) flux[e] = q0flux[dim];

    } else if (sdim == -2) {  // Bottom side
      double dx = level.ds[1];
      // change ceil to floor and then p[1] + 1 to p[1]?
      int maxoffwall = (int) Math.ceil(shockloc / dx);
      foreach (e in Dflux) flux[e] = (e[1] + 1 <= maxoffwall) ?
	q[e + Amr2.face[dim][+1]].getFlux(-sdim) :
	q[e + Amr2.face[dim][+1]].solidBndryFlux(sdim);

    } else if (sdim == +2) {  // Top side
      double sloc = shockloctop + shockspeed * time;
      double dx = level.ds[1];
      foreach (e in Dflux) {
	if ((double) (e[1] + 1) * dx <= sloc) {
	  flux[e] = q1flux[dim];
	} else if ((double) e[1] * dx >= sloc) {
	  flux[e] = q0flux[dim];
	} else {
	  double a1 = (double) (e[1] + 1) * dx - sloc;
	  double a0 = sloc - (double) e[1] * dx;
	  flux[e] = (q1flux[dim] * a1 + q0flux[dim] * a0) / (a1 + a0);
	}
      }
    }
  }
}
