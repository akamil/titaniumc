import java.io.*;

public class ReadHierarchy {

  private static final int dimensions = 2;
  private static final RectDomain<1> Ddims = [1:dimensions], Ddirs = [-1:1:2];

  private static DataInputStream in;

  public static double single read(String inputfile) {
    PrivateRegion temp = new PrivateRegion();

    // returns time
    double ttime;
    int single numProcs = Ti.numProcs();
    int myProc = Ti.thisProc();

    /*
      Open input file.
    */

    try {
      System.out.println("Opening saved grid hierarchy file " + inputfile);
      InputStream fin = new FileInputStream(inputfile);
      in = new DataInputStream(fin);
    } catch (IOException x) {
      System.out.println("Error in opening " + inputfile);
      System.exit(0);
    }

    String discard; // used for things that we throw away

    try {

      /*
	Given an output stream, we dump the following
	(1 per line):
	(1) Number of states (int)
	(2) Name of state (aString)
	(3) Number of dimensions (int)
	(4) Time (REAL)
	(5) Finest level written (int)
	(6) Loside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
	(7) Hiside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
	(8) Refinement ratio ((numLevels-1)*(int))
	(9) Domain of each level (numLevels* (BOX) )
	(10) Steps taken on each level (numLevels*(int) )
	(11) Grid spacing (numLevels*(BL_SPACEDIM*(int))) [\n at each level]
	(12) Coordinate flag (int)
	(13) Boundary info (int)
	(14) For each level,
             (a) level number and # of grids per level (2*int)
	     For each grid,
	         (1) Grid indices (BOX)
	         (2) Level (int)
                 (3) Steps taken on this grid (int)
                 (4) Time for this grid (REAL)
                 (5) Physical location of grid corners (BL_SPACEDIM*(REAL))
                 (6) writeOn data dump of each FAB of data on that grid
      */

      // (1) Number of states (int)
      discard = in.readLine();

      // (2) Name of state (aString) [for each state]
      foreach (pp in [1:5]) discard = in.readLine();

      // (3) Number of dimensions (int)
      discard = in.readLine();

      // (4) Time (REAL)
      ttime = Format.readDouble(in);
      System.out.println("time = " + ttime);

    } catch (IOException x) {
      System.out.println("Error in reading grid hierarchy");
      System.exit(0);
    }

    int myfinestLevel;
    try {
      // (5) Finest level written (int)
      myfinestLevel = Format.readInt(in);
    } catch (IOException x) {
      System.out.println("Error in reading grid hierarchy");
      System.exit(0);
    }

    Amr2.finestLevel = broadcast myfinestLevel from 0;
    // Define the Levels later, when you find out how many patches
    // are in each.
    System.out.println("Amr2.finestLevel = " + Amr2.finestLevel);

    double [1d][1d] ds = new (temp) double[0:Amr2.finestLevel][Ddims];
    try {
      // (6) Loside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
      discard = in.readLine();

      // (7) Hiside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
      discard = in.readLine();

      // (8) Refinement ratio ((numLevels-1)*(int))
      discard = in.readLine();
      
      // (9) Domain of each level (numLevels* (BOX) )
      discard = in.readLine();

      // (10) Steps taken on each level (numLevels*(int) )
      discard = in.readLine();

      // (11) Grid spacing (numLevels*(BL_SPACEDIM*(real))) [at each level]
      for (int h = 0; h <= Amr2.finestLevel; h++) {
	double[] readds = Format.readDoubles(in);  // returns [0:1]
	foreach (pdim in Ddims) ds[h][pdim] = readds[pdim[1] - 1];
	System.out.println("Level " + h + " ds: " + ds[h][1] + "," + ds[h][2]);
      }

      // (12) Coordinate flag (int)
      discard = in.readLine();

      // (13) Boundary info (int)
      discard = in.readLine();

    } catch (IOException x) {
      System.out.println("Error in reading grid hierarchy");
      System.exit(0);
    }

    System.out.println("Going through all levels...");
    RectDomain<2> D = Amr2.domain;
    Point<2> extent = D.max() - D.min() + Point<2>.all(1);
    // expand D and extent at end of this loop!!

    for (int single height = 0; height <= Amr2.finestLevel; height++) {

      System.out.println("Height " + height);

      int numpatches;
      try {
	// (14) For each level,
	// (a) level number and # of grids per level (2*int)
	int[] readhn = Format.readInts(in);
	if (readhn[0] != height) {
	  System.out.println("Huh... should be at level " + height + " now");
	}
	numpatches = readhn[1];
      } catch (IOException x) {
	System.out.println("Error in reading grid hierarchy");
	System.exit(0);
      }

      Level single myLevel;
      SharedRegion single myLevelRegion = new SharedRegion();
      Patch2 patch0;
      if (height == 0) {
	// height == 0:  divide among processes
	if (numpatches != numProcs)
	  System.out.println("Changing from " + numpatches + " to "
			     + numProcs + " processes");
	
	myLevel = new (myLevelRegion) Level(myLevelRegion, [0:0], D, height,
					    ds[height]);

	Point<2> fracextent = extent / [numProcs, 1];  // e.g. [32, 32]
	Point<2> shifter = [fracextent[1], 0];  // e.g. [32, 0]
	RectDomain<2> myD = [Point<2>.all(0) : fracextent - Point<2>.all(1)] +
	  shifter * myProc;
	patch0 = new Patch2(myLevelRegion, myLevel, myD);
	myLevel.patches[0] = patch0;
	patch0.q = new (myLevelRegion) Quantities2[myD];
        System.out.println("Level 0 patch " + patch0.domain);

      } else {
	// height > 0:  put all patches on process 0
	myLevel = new (myLevelRegion)
	  Level(myLevelRegion,
		myProc == 0 ? [0:numpatches-1] : [0:-1],
		D, height, ds[height]);
      }
      Amr2.myself.levels[height] = myLevel;
      
      for (int ipatch = 0; ipatch < numpatches; ipatch++) {

	String domainstr;
	try {
	  // (1) Grid indices (BOX)
	  domainstr = in.readLine();
	} catch (IOException x) {
	  System.out.println("Error in reading grid hierarchy");
	  System.exit(0);
	}
	RectDomain<2> thisdomain = getDomain(temp, domainstr);
	// System.out.println("domain " + thisdomain);
	Point<2> Dmin = thisdomain.min(), Dmax = thisdomain.max();
	double [2d][1d] qq = new (temp) double[thisdomain][1:5];
	// System.out.println("set qq for domain " + thisdomain);
	try {
	  // (2) Level (int)
	  discard = in.readLine();  // had better == height
	  // (3) Steps taken on this grid (int)
	  discard = in.readLine();
	  // (4) Time for this grid (REAL)
	  discard = in.readLine();
	  // (5) Physical location of grid corners (BL_SPACEDIM*(REAL))
	  foreach (pdim in Ddims) discard = in.readLine();
	  // (6) writeOn data dump of each FAB of data on that grid
	  for (int fab = 1; fab <= 5; fab++) {
	    discard = in.readLine();  // domain again
	    for (int i2 = Dmin[2]; i2 <= Dmax[2]; i2++)
	      for (int i1 = Dmin[1]; i1 <= Dmax[1]; i1++)
		qq[i1, i2][fab] = Format.readDouble(in);
	  }
	} catch (IOException x) {
	  System.out.println("Error in reading patch " + thisdomain +
			     " at height " + height);
	  System.exit(0);
	}
	// System.out.println("read qq for domain " + thisdomain);

	if (height == 0) {
	  RectDomain<2> intersect = patch0.domain * thisdomain;
	  foreach (p in intersect)
	    patch0.q[p] = 
	    new Quantities2(qq[p][1], qq[p][2], qq[p][3], qq[p][4]);
	  // need to get patch.U later.

	} else {
	  if (myProc == 0) {
	    System.out.println("Putting patch " + thisdomain +
			       " at level " + height + " into proc " + myProc);
	    Patch2 patch = new Patch2(myLevelRegion, myLevel, thisdomain);
	    myLevel.patches[ipatch] = patch;

	    patch.q = new Quantities2[thisdomain];
	    foreach (p in thisdomain)
	      patch.q[p] = 
	      new Quantities2(qq[p][1], qq[p][2], qq[p][3], qq[p][4]);
	    patch.U = Quantities2.QtoU(myLevelRegion, patch.q);
	  }  // if (myProc == 0)
	} // if (height == 0)
      } // for ipatch

      if (height == 0) patch0.U = Quantities2.QtoU(myLevelRegion, patch0.q);

      Ti.barrier();
      myLevel.FindAdjacent();
      myLevel.FindCoarserOld();
      Ti.barrier();

      extent = extent * Amr2.nRefine;
      D = [Point<2>.all(0) : extent - Point<2>.all(1)];
    } // for height

    try {
      in.close();
    } catch (IOException x) {
      System.out.println("Error in closing grid hierarchy file");
      System.exit(0);
    }
    Util.deleteRegion(temp);
    double single time = Reduce.max(ttime);
    if (time != ttime) {
      System.out.println("inconsistent start times. Problem reading file ?\n");
      System.exit(0);
    }
    return time;
  }


/*
	foreach (indproc in Amr2.processes.domain()) {
	  Amr2Process A2proc = Amr2.processes[indproc];
	  // All processes contain the same number of Levels,
	  // even though there may be no patches in a particular Level.
	  Level lev = A2proc.levels[height];
	  foreach (indpatch in lev.indices) {
	    // For each grid,
	    Patch2 patch = lev.patches[indpatch];

	    System.out.println("Writing patch with height " + height +
			       " in process " + indproc[1] +
			       " with domain " + patch.domain);

	    // (1) Grid indices (BOX)

	    out.writeBytes(domainString(patch.domain) + '\n');

	    // (2) Level (int)
	    out.writeBytes("" + height + '\n');

	    // (3) Steps taken on this grid (int)
	    out.writeBytes("0\n");  // current plot files write 0

	    // (4) Time for this grid (REAL)
	    out.writeBytes("" + time + '\n');

	    // (5) Physical location of grid corners (BL_SPACEDIM*(REAL))
	    for (int dim = 1; dim <= dimensions; dim++) {
	      double loside = ds[dim] * (double) patch.domain.min()[dim];
	      double hiside = ds[dim] * (double) (patch.domain.max()[dim] + 1);
	      out.writeBytes("" + loside + ' ' + hiside + '\n');
	    }

	    // (6) writeOn data dump of each FAB of data on that grid

	    writeOn(patch);

	  } // foreach (indpatch in lev.indices)
	} // foreach (indproc in Amr2.processes.domain())
      } // for (int height = 0; height <= Amr2.finestLevel; height++)

      out.close();
      
    } catch (IOException x) {
      System.out.println("Error in writing to " + outputfile);
      System.exit(0);
    }
  }
*/


  private static RectDomain<2> getDomain(Region temp, String s) {
    // converts a string of form ((96,16) (115,31) (0,0))
    // to a RectDomain<2> [[96, 16] : [115, 31]]

    // s.charAt(i):  character at i
    int [1d] nums = new (temp) int[1:6];
    int nc;
    int curnum = 0;
    boolean ondigits = false;
    for (int i = 0; i < s.length(); i++) {
      char c = s.charAt(i);
      switch (c) {
      case '(':
	break;
      case ')':  // done with curnum
	if (ondigits) ondigits = false;
	break;
      case ',':  // ondigits must be true
	if (ondigits) ondigits = false;
	break;
      case ' ':  // nothing
	break;
      default:   // digit
	if (ondigits) {
	  nums[curnum] = 10*nums[curnum] + (c - '0');
	} else {
	  ondigits = true;
	  curnum++;
	  nums[curnum] = (c - '0');
	}
      }
    }
    return [[nums[1], nums[2]] : [nums[3], nums[4]]];
  }
}
