public class FluxRegisterSegment {
  // a strip of flux registers
  public RectDomain<2> domain;
  /*private*/ public Vector4 [2d] fluxRegs;

  public FluxRegisterSegment(Region r, RectDomain<2> D) {
    domain = D;
    fluxRegs = new (r) Vector4[D];
  }

  public Vector4 op[] (Point<2> e) {
    return fluxRegs[e];
  }

  public Vector4 op[] (int i1, int i2) {
    return fluxRegs[[i1, i2]];
  }

  public void copyNew(Region r, FluxRegisterSegment frs) {  // set up a new copy
    domain = frs.domain;
    fluxRegs = new (r) Vector4[domain];
    fluxRegs.copy(frs.fluxRegs);
  }

  public void Set(Point<2> e, Vector4 v, String s) {
    fluxRegs[e] = v;
    /*
    System.out.println(s + " " + Format.toString(e) + ": " +
		       v.elem(1) + " " +
		       v.elem(2) + " " +
		       v.elem(3) + " " +
		       v.elem(4));
    */
  }
}
