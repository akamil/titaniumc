// Temporary class.

// Temporary.
import ti.domains.*;
// Temporary end.

class Fixme2 {
  public static RectDomain<2>[1d] RectDomainList(Region r, Domain<2> d) {
    // tiMultiRectADomain2 mrd = (tiMultiRectADomain2) d;
    Object fake_d = d;
    tiMultiRectADomain2 mrd = (tiMultiRectADomain2) fake_d;

    int numRectangles = mrd.getRectangles().getNumItems();
    RectDomain<2>[1d] result = new (r) RectDomain<2>[[1:numRectangles:1]];
    int numRectsAdded = 0;
    for (tiRectDomainListIterator2 iter_x = mrd.getRectangles().newIterator();
	 !iter_x.isEnd();
	 iter_x.advance()) {
      numRectsAdded++;
      result[[numRectsAdded]] = [iter_x.dereference().getLowerBound() :
				iter_x.dereference().getUpperMaxPoint() :
				iter_x.dereference().getLoopingStride()];
    }
    // assert (numRectsAdded == numRectangles);
    return result;
  }
}

// In your program, call it like:
// RectDomain<2>[1d] rr = Fixme2.RectDomainList(domain);

// Note that this does not always give the decomposition with the smallest
// number of segments.
// [[0:15], 15.5] + [[16:31], 15.5] stays as two segments, not one.
