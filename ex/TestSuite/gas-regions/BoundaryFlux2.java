interface BoundaryFlux2 {
  void f(Quantities2 [2d] q,
	 Level level,
	 int sdim,
	 double totaltime,
	 Vector4 [2d] flux);   // to be filled in
}
