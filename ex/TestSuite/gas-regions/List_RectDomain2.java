 class List_RectDomain2 {
    public RectDomain<2> car;
    public List_RectDomain2 cdr;

    // Main constructor.
    public List_RectDomain2(RectDomain<2> x, List_RectDomain2 rest) {
	car = x;
	cdr = rest;
    }

    public List_RectDomain2 cons(Region r, RectDomain<2> x) {
	return new (r) List_RectDomain2(x, this);
    }        

    public /*inline*/ RectDomain<2> first() {
	return car;
    }
    public /*inline*/ List_RectDomain2 rest() {
	return cdr;
    }

    public int length() {
	int i = 0;
	List_RectDomain2 r = this;
	while (r != null) {
	    i++;
	    r = r.rest();
	}
	return i;
    }
}
