public class Util {

  public static void deleteRegion(PrivateRegion r) {
    if (r != null)
      try {
	r.delete();
      }
      catch (RegionInUse oops) {
	System.err.println("BUG: freed memory that is in use.");
	System.exit(2);
      }
  }

  public static sglobal void deleteRegion(SharedRegion single r) {
    if (r != null)
      try {
	r.delete();
      }
      catch (RegionInUse oops) {
	System.err.println("BUG: freed memory that is in use.");
	System.exit(2);
      }
  }
}
