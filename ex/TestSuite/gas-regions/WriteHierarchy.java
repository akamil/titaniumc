import java.io.*;

public class WriteHierarchy {

  private static final int dimensions = 2;
  private static final RectDomain<1> Ddims = [1:dimensions], Ddirs = [-1:1:2];

  private static DataOutputStream out;

  public static void write(String outputfile, double time) {
    // need time

    /*
      Open output file.
    */

    try {
      System.out.println("Opening file " + outputfile + " for output");
      OutputStream fout = new FileOutputStream(outputfile);
      out = new DataOutputStream(fout);
    } catch (IOException x) {
      System.out.println("Error in opening " + outputfile);
      System.exit(0);
    }

    try {

      /*
	Given an output stream, we dump the following
	(1 per line):
	(1) Number of states (int)
	(2) Name of state (aString)
	(3) Number of dimensions (int)
	(4) Time (REAL)
	(5) Finest level written (int)
	(6) Loside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
	(7) Hiside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
	(8) Refinement ratio ((numLevels-1)*(int))
	(9) Domain of each level (numLevels* (BOX) )
	(10) Steps taken on each level (numLevels*(int) )
	(11) Grid spacing (numLevels*(BL_SPACEDIM*(int))) [\n at each level]
	(12) Coordinate flag (int)
	(13) Boundary info (int)
	(14) For each level,
             (a) level number and # of grids per level (2*int)
	     For each grid,
	         (1) Grid indices (BOX)
	         (2) Level (int)
                 (3) Steps taken on this grid (int)
                 (4) Time for this grid (REAL)
                 (5) Physical location of grid corners (BL_SPACEDIM*(REAL))
                 (6) writeOn data dump of each FAB of data on that grid
      */

      System.out.println("Writing output");

      // (1) Number of states (int)
      out.writeBytes("" + (dimensions + 2 + 1) + '\n');
      // also have level as a state

      // (2) Name of state (aString) [for each state]
      out.writeBytes("density\n");
      out.writeBytes("x_velocity\n");
      out.writeBytes("y_velocity\n");
      out.writeBytes("pressure\n");
      out.writeBytes("level\n"); // should be here?

      // (3) Number of dimensions (int)
      out.writeBytes("" + dimensions + '\n');

      // (4) Time (REAL)

      out.writeBytes("" + time + '\n');

      // (5) Finest level written (int)
      out.writeBytes("" + Amr2.finestLevel + '\n');

      // (6) Loside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
      out.writeBytes("0.0  0.0\n");

      // (7) Hiside prob domain loc in each direction (BL_SPACEDIM*(REAL*))
      double [1d] ds = Amr2.processes[0].levels[0].ds;
      double [1d] hi = new double[Ddims];
      for (int dim = 1; dim <= dimensions; dim++)
	hi[dim] = ds[dim] * (double)
	(Amr2.domain.max()[dim] - Amr2.domain.min()[dim] + 1);
      out.writeBytes("" + hi[1] + ' ' + hi[2] + '\n');

      // (8) Refinement ratio ((numLevels-1)*(int))
      for (int h = 0; h <= Amr2.finestLevel - 1; h++) {
	out.writeBytes("" + Amr2.nRefine + ' ');
      }
      out.writeBytes("\n");

      // (9) Domain of each level (numLevels* (BOX) )
      for (int h = 0; h <= Amr2.finestLevel; h++)
	out.writeBytes(domainString(Amr2.processes[0].levels[h].domain));

      out.writeBytes("\n");

      // (10) Steps taken on each level (numLevels*(int) )
      for (int h = 0; h <= Amr2.finestLevel; h++) {
	out.writeBytes("0 ");
      }
      out.writeBytes("\n");  // current plot files write all 0

      // (11) Grid spacing (numLevels*(BL_SPACEDIM*(real))) [at each level]
      for (int h = 0; h <= Amr2.finestLevel; h++) {
	double [1d] levds = Amr2.processes[0].levels[h].ds;
	for (int dim = 1; dim <= dimensions; dim++) {
	  out.writeBytes("" + levds[dim] + ' ');
	}
	out.writeBytes("\n");
      }

      // (12) Coordinate flag (int)
      out.writeBytes("0\n");  // 0 = cartesian

      // (13) Boundary info (int)
      out.writeBytes("0\n");  // 0 = no boundary info

      System.out.println("Going through all processes...");

      for (int height = 0; height <= Amr2.finestLevel; height++) {

	// (14) For each level,

	// (a) level number and # of grids per level (2*int)

	out.writeBytes("" + height);
	int totpatches = 0;
	foreach (indproc in Amr2.processes.domain())
	  totpatches += Amr2.processes[indproc].levels[height].indices.size();
	out.writeBytes(" " + totpatches + '\n');

	foreach (indproc in Amr2.processes.domain()) {
	  Amr2Process A2proc = Amr2.processes[indproc];
	  // All processes contain the same number of Levels,
	  // even though there may be no patches in a particular Level.
	  Level lev = A2proc.levels[height];
	  foreach (indpatch in lev.indices) {
	    // For each grid,
	    Patch2 patch = lev.patches[indpatch];

	    System.out.println("Writing patch with height " + height +
			       " in process " + indproc[1] +
			       " with domain " + patch.domain);

	    // (1) Grid indices (BOX)

	    out.writeBytes(domainString(patch.domain) + '\n');

	    // (2) Level (int)
	    out.writeBytes("" + height + '\n');

	    // (3) Steps taken on this grid (int)
	    out.writeBytes("0\n");  // current plot files write 0

	    // (4) Time for this grid (REAL)
	    out.writeBytes("" + time + '\n');

	    // (5) Physical location of grid corners (BL_SPACEDIM*(REAL))
	    for (int dim = 1; dim <= dimensions; dim++) {
	      double loside = ds[dim] * (double) patch.domain.min()[dim];
	      double hiside = ds[dim] * (double) (patch.domain.max()[dim] + 1);
	      out.writeBytes("" + loside + ' ' + hiside + '\n');
	    }

	    // (6) writeOn data dump of each FAB of data on that grid

	    writeOn(patch);

	  } // foreach (indpatch in lev.indices)
	} // foreach (indproc in Amr2.processes.domain())
      } // for (int height = 0; height <= Amr2.finestLevel; height++)

      out.close();
      
    } catch (IOException x) {
      System.out.println("Error in writing to " + outputfile);
      System.exit(0);
    }
  }


  private static String domainString(RectDomain<2> D) {
    String s =
      "((" + D.min()[1] + ',' + D.min()[2] + ')' +
      " (" + D.max()[1] + ',' + D.max()[2] + ')' +
      " (0,0))";
    return s;
  }


  private static void writeOn(Patch2 patch) {

    int h = patch.level.height;
    try {

      Point<2> Dmin = patch.domain.min(), Dmax = patch.domain.max();

      out.writeBytes(domainString(patch.domain) + " 1\n");
      for (int i2 = Dmin[2]; i2 <= Dmax[2]; i2++)
	for (int i1 = Dmin[1]; i1 <= Dmax[1]; i1++)
	  out.writeBytes("" + patch.q[i1, i2].density() + '\n');
    
      out.writeBytes(domainString(patch.domain) + " 1\n");
      for (int i2 = Dmin[2]; i2 <= Dmax[2]; i2++)
	for (int i1 = Dmin[1]; i1 <= Dmax[1]; i1++)
	  out.writeBytes("" + patch.q[i1, i2].velocity(1) + '\n');
    
      out.writeBytes(domainString(patch.domain) + " 1\n");
      for (int i2 = Dmin[2]; i2 <= Dmax[2]; i2++)
	for (int i1 = Dmin[1]; i1 <= Dmax[1]; i1++)
	  out.writeBytes("" + patch.q[i1, i2].velocity(2) + '\n');

      out.writeBytes(domainString(patch.domain) + " 1\n");
      for (int i2 = Dmin[2]; i2 <= Dmax[2]; i2++)
	for (int i1 = Dmin[1]; i1 <= Dmax[1]; i1++)
	  out.writeBytes("" + patch.q[i1, i2].pressure() + '\n');

      out.writeBytes(domainString(patch.domain) + " 1\n");
      for (int i2 = Dmin[2]; i2 <= Dmax[2]; i2++)
	for (int i1 = Dmin[1]; i1 <= Dmax[1]; i1++)
	  out.writeBytes("" + h + '\n');

    } catch (IOException x) {
      System.out.println("Error in writing patch with domain "+ patch.domain);
      System.exit(0);
    }
  }
}
