interface InitialPatch2 {

  // returns ds[1:dimensions]
  double [1d] dsGet(Point<2> extent);

  // returns initial primitive quantities for cells of the given patch
  Quantities2 [2d] qInitial(Region r, RectDomain<2> Dlocal, double [1d] ds);
}
