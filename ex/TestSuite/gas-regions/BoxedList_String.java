import java.io.PrintStream;

 class BoxedList_String {
    private List_String l = null;

    public BoxedList_String() {
    }

    public BoxedList_String(String x) {
        push(x);
    }

    public void push(String item) {
        l = new List_String(item, l);
    }

    public String pop() {
        String t = l.first();
	l = l.rest();
	return t;
    }

    public String first() {
        return l.first();
    }

    public List_String toList() {
        return l;
    }

    public String [1d] toArray() {
	String [1d] a = new String [[0 : length() - 1]];
	int i = 0;
	for (List_String r = l; r != null; r = r.rest()) {
	    a[i] = r.first();
	    i++;
	}
	return a;
    }

    public String [1d] toReversedArray() {
	int i = length();
	String [1d] a = new String [[0 : i - 1]];
	for (List_String r = l; r != null; r = r.rest()) {
	    a[--i] = r.first();
	}
	return a;
    }

    public boolean isEmpty() {
	return l == null;
    }

    public int length() {
	return l == null ? 0 : l.length();
    }
}
