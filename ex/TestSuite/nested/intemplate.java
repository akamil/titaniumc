// Tests static nested classes amd inner classes inside templates.
// Expected result: PASS
template<int x, int y> class R3 {
    static class R3b {
	{System.out.println(x + " " + y);}
    }
    class R3e {
      { System.out.println(y + " " + x); }
    }
}

template<class x> class R3f {
  static class R3b {
    { System.out.println(R3b.class); }
  }
  class R3e {
    { System.out.println(R3e.class); }
  }
}

class R3c {
    public static void main(String[] args) {
	new R3<1,2>.R3b();
	new R3<3,R3d.x>.R3b();
	new R3<1,2>().new R3e();
	new R3<3,R3d.x>().new R3e();
        new R3f<Object>.R3b();
        new R3f<Throwable>.R3b();
        new R3f<Object>().new R3e();
        new R3f<Throwable>().new R3e();
    }
}

class R3d {
  public static final int x = 4;
}


