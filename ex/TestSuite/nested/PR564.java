// Tests unqualified inner class allocation where encloser is subclass of 
// required class.
// Relevant PR: 564
// Expected result: PASS

class D2 extends D2b {
  class D2a {
    D2a() {
      System.out.println(new D2c());
    }
  }
  public static void main(String[] args) {
    new D2().new D2a();
    new D2().new D2c();
  }
}

class D2b {
  class D2c {
    { System.out.println(D2b.this); }
    public String toString() {
      return "D2b.D2c";
    }
  }
  public String toString() {
    return "D2b";
  }
}

