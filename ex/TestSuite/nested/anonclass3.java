// Tests anonymous classes in multiple classes.
// Expected result: PASS

class H2 {
  public static void main(String[] args) {
    System.out.println(getName(new Object() {}));
    System.out.println(getName(new Object() {}));
    new H2a();
  }
  static String getName(Object o) {
    String s = o.toString();
    StringBuffer sb = new StringBuffer(s.substring(0, s.indexOf('@')));
    for (int i = 0; i < sb.length(); i++) {
      if (sb.charAt(i) == '$') sb.setCharAt(i, '.');
    }
    return sb.toString();
  }
}

class H2a {
  { System.out.println(H2.getName(new Object() {})); }
  { System.out.println(H2.getName(new Object() {})); }
}
