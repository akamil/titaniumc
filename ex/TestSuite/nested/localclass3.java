// Test legal name collision of local classes.
// Expected result: PASS

class k3 {
  public static void main(String[] args) {
    class x { {System.out.println("x1");} }
    new x();
    foo();
  }
  static void foo() {
    class x { {System.out.println("x2");} }
    new x();
    class z {
      { class x { {System.out.println("x3");} }
        new x(); }
    }
    new z();
  }
}

