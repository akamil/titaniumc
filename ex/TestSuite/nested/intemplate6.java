// Tests using templates and template qualified names with qualified this
// and super.
// Expected result: PASS

class n4 {
  class n4b {
  }
  public static void main(String[] args) {
    println(new n4a<5>().new n4b().new n4c());
  }
  static void println(Object o) { println(o.toString()); }
  static void println(String s) {
    int a = s.indexOf('@');
    a = (a < 0 ? s.length() : a);
    System.out.println(s.substring(0, a));
  }
}


template<int x> class n4a {
  static void println(Object o) { n4.println(o); }
  static void println(String s) { n4.println(s); }
  public String toString() { return n4a.class.toString(); }
  class n4b {
    public String toString() { return n4b.class.toString(); }
    class n4c {
      { 
        println(n4a.this);
        println(n4a<x>.this);
        println(n4a<x>.n4b.this);
        println(n4a.n4b.this);
        println(n4a.super.toString());
        println(n4a<x>.super.toString());
        println(n4a<x>.n4b.super.toString());
        println(n4a.n4b.super.toString());
      }
    }
  }
}
