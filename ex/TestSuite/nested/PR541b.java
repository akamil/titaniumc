// Tests using enclosing instance in super call.
// Relevant PR: 541
// Expected result: PASS

class b2 {
    class b2a extends b2b {
	b2a() {
	    super(x);
	}
    }
    int x = 3;
    public static void main(String[] args) {
	new b2().new b2a();
    }
}

class b2b {
    b2b(int x) {
	System.out.println(x);
    }
}
