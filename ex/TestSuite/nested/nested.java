// Simple nested class tests.
// Expected result: PASS

class Y2 {
  static class Y2a {
    public String toString() { return "Y2.Y2a"; }
    { System.out.println(x); }
    { System.out.println(new Y2b()); }
    private int y = 4;
  }
  static class Y2b {
    public String toString() { return "Y2.Y2b"; }
  }
  public static void main(String[] args) {
    System.out.println(new Y2a());
    System.out.println(new Y2a().y);
  }
  static int x = 2;
}
