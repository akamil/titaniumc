// Test local/param var accessibilty from local class.
// Expected result: PASS

class d3 {
  public static void main(final String[] args) {
    final int y = 4;
    class x {
      x(String s) {
        System.out.println(s + " " + y + " " + args.length);
      }
    };
    new x("asdf");
  }
}
