// Tests using enclosing instance in instance initializer and constructor.
// Relevant PR: 541
// Expected result: PASS

class z2 {
  class z2a {
    z2a() {
      System.out.println(z2.this);
    }
    { System.out.println(z2.this); }
  }
  public static void main(String[] args) {
    new z2().new z2a();
  }
  public String toString() {
    return "z2";
  }
}

