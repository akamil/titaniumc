// Tests qualified super.
// Expected result: PASS

class r2 extends r2a {
  public static void main(String[] args) {
    new r2().new r2b();
  }
  class r2b extends r2c {
    r2b() {
      System.out.println(r2.super.blah());
      System.out.println(r2.super.blah);
      System.out.println(r2b.super.blah());
    }
    public String toString() {
      return "r2.r2b";
    }
  }
  public String blah() {
    return ("r2.blah(): " + toString());
  }
  public String toString() {
    return "r2";
  }
  public int blah = 1;
}

class r2a {
  public String blah() {
    return ("r2a.blah(): " + toString());
  }
  public String toString() {
    return "r2a";
  }
  public int blah = 2;
}

class r2c {
  public String blah() {
    return ("r2c.blah(): " + toString());
  }
  public String toString() {
    return "r2c";
  }
}

