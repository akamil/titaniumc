// Test local and anonymous classes in templates.
// Expected result: PASS

class T3 {
  public static void main(String[] args) {
    new T3a<Object>();
    new T3a<Throwable>();
  }
}

template<class x> class T3a {
  static String strip(String s) {
    int a = s.indexOf('@');
    a = (a < 0 ? s.length() : a);
    return s.substring(0, a);
  }
  { 
    final String z = strip(new x().toString()); 
    class T3b { 
      {System.out.println(z);} 
      public String toString() { return strip(super.toString()); }
    } 
    System.out.println(new x() {
      { System.out.println(z);
        System.out.println(new T3b()); }
      public String toString() { return strip(super.toString()); }
     });
  }
}
