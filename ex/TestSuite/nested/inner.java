// Simple inner class tests.
// Expected result: PASS

class Z2 {
  class Z2a {
    public String toString() { return "Z2.Z2a"; }
    { System.out.println(x); }
    { System.out.println(new Z2b()); }
    private int y = 4;
  }
  class Z2b {
    public String toString() { return "Z2.Z2b"; }
  }
  public static void main(String[] args) {
    System.out.println(new Z2().new Z2a());
    System.out.println(new Z2().new Z2a().y);
  }
  private int x = 2;
}
