// Test demangling of anonymous and local classes.
// Expected result: PASS

class q3 {
  public static void main(String[] args) {
    print(new Object() { { print(new Object() {}); } });
    class q3a {
      { print(new Object() { { print(new Object() {}); } }); }
    }
    print(new q3a());
    foo();
  }
  static void foo() {
    print(new Object() { { print(new Object() {}); } });
    class q3a {
      { print(new Object() { { print(new Object() {}); } }); }
    }
    print(new q3a());
  }
  static void print(Object o) {
    System.out.println(o.toString().substring(0, o.toString().indexOf('@')));
  }
}
