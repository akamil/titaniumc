// Tests multiple instances of an anonymous class with different final 
// parameter values.
// Expected result: PASS

class B2 {
  public static void main(String[] args) {
    B2 a = bah(1);
    B2 b = bah(2);
    a.nah();
    b.nah();
    a.nah();
  }
  void nah() {}
  public static B2 bah(final int x) {
    System.out.println(x);
    return new B2() {
        void nah() { System.out.println(x); }
    };
  }
}

