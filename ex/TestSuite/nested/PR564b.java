// Make sure collision between method of encloser and superclass gets resolved
// correctly.
// Relevant PR: 565
// Expected result: PASS

class X2 {
  class X2b extends X2a {
    X2b() { foo(); bar(); }
  }
  private void foo() { System.out.println("X2.foo()"); }
  void bar() { System.out.println("X2.bar()"); }
  public static void main(String[] args) {
    new X2().new X2b();
  }
}

class X2a {
  private void foo() { System.out.println("X2a.foo()"); }
  void bar() { System.out.println("X2a.bar()"); }
}
