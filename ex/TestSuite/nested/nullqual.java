// Tests qualified allocation with null qualifier.
// Expected result: EXCEPTION (caught)
class J1 {
    class J1a {
      //{System.out.println(y);}
    }
    public static void main(String[] args) {
	J1 x = null;
	try {
	    System.out.println(x.new J1a());
	} catch (NullPointerException e) {
	  System.out.println("caught exception");
	}
    }
  int y = 4;
}
