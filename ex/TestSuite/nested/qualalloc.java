// Tests qualified allocations with regions.
// Expected result: PASS

class b3 {
  class b3a {
  }
  public static void main(String[] args) {
    PrivateRegion pr = new PrivateRegion();
    b3a x = new b3().new (pr) b3a();
    assert x.regionOf() == pr;
  }
}
