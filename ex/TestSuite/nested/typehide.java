// Test hiding of inherited nested type.
// Expected result: PASS

class z3 extends z3b {
  static class z3a {
    public String toString() {
      return "z3.z3a";
    }
  }
  public static void main(String[] args) {
    System.out.println(new z3a());
  }
}

class z3b {
  static class z3a {
    public String toString() {
      return "z3b.z3a";
    }
  }
}

