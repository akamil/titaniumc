// Tests inheritance of field by nested class when it extends encloser.
// Relevant PR: 565
// Expected result: PASS

class C2 {
  int xxx = 5;
  static class C2a extends C2 {
    C2a() {
      System.out.println(xxx);
      System.out.println(toString());
    }
  }
  public static void main(String[] args) {
    new C2a();
  }
  public String toString() {
    return "C2";
  }
}
