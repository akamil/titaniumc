// Tests use of enclosing instance in field initializer.
// Relevant PR: 541
// Expected result: PASS

class a2 {
  int y = 4;
  class a2a {
    int x = y;
  }
  public static void main(String[] args) {
    new a2().new a2a();
  }
}

