#!/usr/bin/env ksh
#@ ----------------------------------------------------------------------------
#@
#@ Name: bad_expr1.sh
#@
#@ Version: 1.0
#@
#@ Creator: miyamoto
#@
#@ Date: 990430
#@
#@ Purpose:
#@   Test that the compiler catches and fails on some bad expressions.  This
#@   tests for arity mismatches when using a point to build a RectDomain.
#@
#@ Change Log:
#@
#@ ----------------------------------------------------------------------------

# Clear files
> test.tis

# Create files
cat > test.tis << '@eof'
class Test {
  public static void main(String[] args) {
    Point<P_ARITY> p = Point<P_ARITY>.all(3);
    // Error if p is of the wrong arity
    RectDomain<D_ARITY> rd = [p:p];
  }
}
@eof

# we're looking at tc's output, so make sure it keeps quiet
SS_DEFAULT_COMPILER_FLAGS="${SS_DEFAULT_COMPILER_FLAGS} --silent --noverbose"

for p_arity in 1 2 3 ; do
  for d_arity in 1 2 3 ; do
    rm -f test.ti
    sed -e "s/P_ARITY/${p_arity}/g" \
        -e "s/D_ARITY/${d_arity}/g" test.tis > test.ti
    > stdout.new
    > stderr.new

    # Compile
    ${SS_COMPILER} ${SS_DEFAULT_COMPILER_FLAGS} ${SS_COMPILER_FLAGS} test.ti > stdout.new 2> stderr.new
    rc=$?

    # Normalize
    ${SS_NORMALIZER} < stdout.new > stdout.new.norm || exit 10
    ${SS_NORMALIZER} < stderr.new > stderr.new.norm || exit 20

    # Validate
    if [[ "${p_arity}" != "${d_arity}" ]] ; then
      # Arities mismatched, an error
      if [[ "${rc}" = "0" ]] ; then
        echo "ERROR: Arity mismatch not detected"
        cat stdout.new.norm stderr.new.norm
	exit 20
      fi
      grep "rd is not assignable with tiRectDomain<..*>" stdout.new.norm stderr.new.norm > /dev/null || exit 30
    else
      if [[ "${rc}" != "0" ]] ; then
	exit 40
      fi
      # Arities matched, no error
      if [[ -s stdout.new.norm ]] ; then
	# stdout should have nothing
        echo "ERROR: stdout non-empty:"
        cat stdout.new.norm
	exit 50
      fi
      if [[ -s stderr.new.norm ]] ; then
	# stderr should have nothing
        echo "ERROR: stderr non-empty:"
        cat stderr.new.norm
	exit 60
      fi
    fi
  done
done

# Done and Passed
exit 0
