class Cut {
  int dim, value, thickness;

  /* sample: dim = 1, value = 3, thickness = k
	      applied to [[0, 0, 0] : [N, N, N]]
	      yields [[0, 0, 0] : [N, 3, N]] and [[0, 3 + k, 0] : [N, N, N]] */
  public Cut(int dim, int value, int thickness) {
    this.dim = dim;
    this.value = value;
    this.thickness = thickness;
  }

  // Apply the given cut to the given box, and cons the result(s) onto out.
  // If the given cut doesn't pass through the given box, should be
  // equivalent to out.push(box).
  private static final void apply1(Cut cut, RectDomain<3> box,
				   BoxedList_RectDomain_3 out) {
    int loU[] = new int[3];
    int hiL[] = new int[3];
    Point<3> boxU = box.max() + [1, 1, 1], boxL = box.min();
    RectDomain<3> c;

    for (int i = 0; i < 3; i++)
      if (i == cut.dim) {
	  loU[i] = cut.value;
	  hiL[i] = cut.value + cut.thickness;
      }
      else {
	  loU[i] = boxU[i];
	  hiL[i] = boxL[i];
      }

    c = [boxL : [loU[0], loU[1], loU[2]] - [1, 1, 1]] * box;
    if (!c.isEmpty()) out.push(c);
    c = [[hiL[0], hiL[1], hiL[2]] : boxU - [1, 1, 1]] * box;
    if (!c.isEmpty()) out.push(c);
  }

  // Apply the given cuts to the given box, resulting in a list of boxes.
  public static final BoxedList_RectDomain_3 apply(BoxedList_Cut cuts,
						  RectDomain<3> box) {
    BoxedList_RectDomain_3 boxes = new BoxedList_RectDomain_3(box);
    while (!cuts.isEmpty()) {
      Cut cut = cuts.pop();
      BoxedList_RectDomain_3 newboxes = new BoxedList_RectDomain_3();
      while (!boxes.isEmpty()) apply1(cut, boxes.pop(), newboxes);
      boxes = newboxes;
    }
    return boxes;
  }
}
