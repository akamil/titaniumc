import java.io.*;
import java.lang.*;

public class filetst {
  public static long starttime;
  public static void MSG(String s) {
    System.out.println((System.currentTimeMillis() - starttime) / 1000.0 + ": " + s);
    }

  public static void main (String [] args) {
    if (args.length != 1) {
      System.out.println("Usage: filetst <infilename>");
      System.exit(1);
      }
    String infilename = args[0];
    File infile = new File(infilename);
    starttime = System.currentTimeMillis();

    try {
      MSG("exists(): " + infile.exists());
      MSG("isDirectory(): " + infile.isDirectory());
      MSG("isFile(): " + infile.isFile());
      MSG("canRead(): " + infile.canRead());
      MSG("canWrite(): " + infile.canWrite());
      MSG("isAbsolute(): " + infile.isAbsolute());
      MSG("length(): " + infile.length());
      MSG("lastModified(): " + infile.lastModified());

      String tstfilename1 = ".filetst.tmp1"; // neither file should exist
      String tstfilename2 = ".filetst.tmp2";
      File file1 = new File(tstfilename1);
      File file2 = new File(tstfilename2);

      { // create file1
        RandomAccessFile raf = new RandomAccessFile(file1, "rw");
        raf.close();
        }

      if (file1.exists() && !file2.exists())  // test existence
        MSG("passed exists() test.");
      else 
        throw new InternalError("exists test failed");

      file1.renameTo(file2);
      if (!file1.exists() && file2.exists())  // test rename
        MSG("passed renameTo() test.");
      else 
        throw new InternalError("exists test failed");

      if (!file1.delete() && file2.delete())  // delete file1
        MSG("passed delete() test.");
      else 
        throw new InternalError("delete test failed");

      String tstdirname = ".filetst.dir";
      String tstdirname2 = ".filetst.dir/a/b";
      File newdir = new File(tstdirname);
      File newdir2 = new File(tstdirname2);
      if (newdir.exists()) throw new InternalError("can't perform mkdir test - remove " + newdir);
      if (!newdir.mkdir()) throw new InternalError("mkdir test failed (2)");
      if (newdir2.mkdir()) throw new InternalError("mkdir test failed (3)");
      if (!newdir2.mkdirs()) throw new InternalError("mkdir test failed (4)");
      if (!newdir2.exists()) throw new InternalError("mkdir test failed (5)");
      if (!newdir2.delete() ||
          !(new File(newdir2.getParent())).delete() ||
          !newdir.delete()) throw new InternalError("mkdir test failed (6)");

      MSG("passed mkdir() test.");

      String [] lst = infile.list();
      MSG("number of entries: " + lst.length);
      for (int i = 0; i < lst.length; i++) 
        MSG("dir(" + i + "): " + lst[i]);

      }
    catch (Throwable exn) {
      MSG("caught " + /*ExnType.gettype(exn) + */ " : "  + exn.getMessage());
      }
		}

	}
