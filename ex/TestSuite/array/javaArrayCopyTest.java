public class javaArrayCopyTest {
 public static void main(String[]args) {
   int [] x = new int [20];
   int [] y = new int [40];
   for (int i=0; i < x.length; i++) x[i] = i;
   
   System.arraycopy(x, 0, y, 20, 20);
   for (int i=20; i < y.length; i++) {
     if (y[i] != i-20) { System.out.println("non-overlapping arraycopy test failed"); System.exit(1); }
   }

   System.arraycopy(x, 0, x, 5, 10);
   for (int i=0; i < 5; i++) {
     if (x[i] != i) { System.out.println("overlapping arraycopy test failed"); System.exit(1); }
   }
   for (int i=5; i < 15; i++) {
     if (x[i] != i-5) { System.out.println("overlapping arraycopy test failed"); System.exit(1); }
   }
   for (int i=15; i < 20; i++) {
     if (x[i] != i) { System.out.println("overlapping arraycopy test failed"); System.exit(1); }
   }
   System.out.println("tests passed.");
 }
}
