import java.io.PrintStream;

 class BoxedList_RectDomain2 {
    private List_RectDomain2 l = null;

    public BoxedList_RectDomain2() {
    }

    public BoxedList_RectDomain2(RectDomain<2> x) {
        push(x);
    }

    public void push(RectDomain<2> item) {
        l = new List_RectDomain2(item, l);
    }

    public RectDomain<2> pop() {
        RectDomain<2> t = l.first();
	l = l.rest();
	return t;
    }

    public RectDomain<2> first() {
        return l.first();
    }

    public List_RectDomain2 toList() {
        return l;
    }

    public RectDomain<2> [1d] toArray() {
	RectDomain<2> [1d] a = new RectDomain<2> [[0 : length() - 1]];
	int i = 0;
	for (List_RectDomain2 r = l; r != null; r = r.rest()) {
	    a[i] = r.first();
	    i++;
	}
	return a;
    }

    public RectDomain<2> [1d] toReversedArray() {
	int i = length();
	RectDomain<2> [1d] a = new RectDomain<2> [[0 : i - 1]];
	for (List_RectDomain2 r = l; r != null; r = r.rest()) {
	    a[--i] = r.first();
	}
	return a;
    }

    public boolean isEmpty() {
	return l == null;
    }

    public int length() {
	return l == null ? 0 : l.length();
    }
}
