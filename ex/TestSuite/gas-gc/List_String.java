 class List_String {
    public String car;
    public List_String cdr;

    // Main constructor.
    public List_String(String x, List_String rest) {
	car = x;
	cdr = rest;
    }

    public List_String cons(String x) {
	return new List_String(x, this);
    }        

    public /*inline*/ String first() {
	return car;
    }
    public /*inline*/ List_String rest() {
	return cdr;
    }

    public int length() {
	int i = 0;
	List_String r = this;
	while (r != null) {
	    i++;
	    r = r.rest();
	}
	return i;
    }
}
