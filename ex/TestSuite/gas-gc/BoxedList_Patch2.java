import java.io.PrintStream;

 class BoxedList_Patch2 {
    private List_Patch2 l = null;

    public BoxedList_Patch2() {
    }

    public BoxedList_Patch2(Patch2 x) {
        push(x);
    }

    public void push(Patch2 item) {
        l = new List_Patch2(item, l);
    }

    public Patch2 pop() {
        Patch2 t = l.first();
	l = l.rest();
	return t;
    }

    public Patch2 first() {
        return l.first();
    }

    public List_Patch2 toList() {
        return l;
    }

    public Patch2 [1d] toArray() {
	Patch2 [1d] a = new Patch2 [[0 : length() - 1]];
	int i = 0;
	for (List_Patch2 r = l; r != null; r = r.rest()) {
	    a[i] = r.first();
	    i++;
	}
	return a;
    }

    public Patch2 [1d] toReversedArray() {
	int i = length();
	Patch2 [1d] a = new Patch2 [[0 : i - 1]];
	for (List_Patch2 r = l; r != null; r = r.rest()) {
	    a[--i] = r.first();
	}
	return a;
    }

    public boolean isEmpty() {
	return l == null;
    }

    public int length() {
	return l == null ? 0 : l.length();
    }
}
