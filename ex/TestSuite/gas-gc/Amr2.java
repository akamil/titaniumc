import java.io.*;

class Amr2 {

  public static int dimensions = 2;
  private static final RectDomain<1> Ddims = [1:dimensions], Ddirs = [-1:1:2];

  public static int single nRefine = 2;      // refinement ratio
  public static double dRefine = (double) nRefine;
  public static Point<2> nRefineP = Point<2>.all(nRefine);

  public static double single maxcfl = 0.8;

  /*
    set these once:  used for converting between edge coordinates
    and face coordinates.
    [p + edge[dim][dir]] for edge in direction dir in dimension dim from face p
    [e + face[dim][dir]] for face in direction dir in dimension dim from edge e
  */

  public static Point<2> [1d][1d] face, edge;

  /*
    non-constant fields
  */

  public static Amr2Process [1d] single processes;

  public static int single finestLevel, finestLevelMax;

  public static RectDomain<2> domain;  // the whole domain in level-0 coords

  public static BoundaryFlux2 boundaryFluxFn;

  // number of steps between regriddings, and
  // number of steps between outputs
  public static int single regridintvl, plotintvl;

  /*
    methods
  */

  public single static void SetProb(Point<2> extent,
				    int single finest,
				    BoundaryFlux2 fluxFn,
				    int single regridsteps,
				    int single plotsteps) {
    // Initialize fields.  Called by SetupInput.
    // System.out.println("entering SetProb");
    face = new Point<2>[Ddims][Ddirs];
    edge = new Point<2>[Ddims][Ddirs];
    foreach (pdim in Ddims) {
      Point<2> unit = Point<2>.direction(pdim[1]);
      face[pdim][-1] = Point<2>.all(0);
      face[pdim][+1] = unit;
      edge[pdim][-1] = unit * (-1);
      edge[pdim][+1] = Point<2>.all(0);
    }
    finestLevelMax = finest;
    domain = [Point<2>.all(0) : extent - Point<2>.all(1)];
    boundaryFluxFn = fluxFn;
    regridintvl = regridsteps;
    plotintvl = plotsteps;
    // System.out.println("leaving SetProb");
  }


  public static RectDomain<2> subcells(Point<2> cellC) {
    // returns indices of subcells of cellC at the next finer level.
    RectDomain<2> subBase = [Point<2>.all(0) : Point<2>.all(nRefine - 1)];
    return (subBase + (cellC * nRefine));
  }


  public static RectDomain<2> subedges(Point<2> edgeC, int dim) {
    // returns indices of subedges of edgeC at the next finer level.
    // in dimension dim:  (edgeC[dim] + 1) * nRefine - 1
    // in other dimensions:  edgeC[d] * nRefine + [0:nRefine - 1]
    // (just like subcells)
    int dimc = 3 - dim;
    Point<2> subBase = (edgeC + face[dim][+1]) * nRefine + edge[dim][-1];
    RectDomain<2> extent =
      [Point<2>.all(0) : Point<2>.direction(dimc, nRefine - 1)];
    return (extent + subBase);
  }


  public static Point<2> supercell(Point<2> cellF) {
    // returns index of supercell of cellF at the next coarser level.
    return (cellF / nRefineP);
  }


  public static single DataInputStream SetupInput(String inputfile,
						  BoundaryFlux2 fluxFn) {
    // Set up Amr2 with global data from inputfile.

    int myProc = Ti.thisProc();
    /*
      Opening inputfile
    */
    System.out.println(myProc + " Opening input file " + inputfile);
    InputStream fin;
    DataInputStream in;
    try {
      fin = new FileInputStream(inputfile);
      in = new DataInputStream(fin);
    } catch (IOException iox) {
      System.out.println("Error in process " +myProc+ " opening " +inputfile);
      System.exit(0);
    }

    System.out.println(myProc + " Reading global parameters");
    /*
      Reading global parameters from inputfile
    */
    Point<2> myextent;
    int myfinest, myregridsteps, myplotsteps;
    try {
      // Line 1:  extent of computational domain
      int[] extentRead = Format.readInts(in);
      if (extentRead.length != 2) throw new IOException();
      myextent = [extentRead[0], extentRead[1]];

      // Line 2:  finest level of patches to be used
      myfinest = Format.readInt(in);

      // Line 3:  number of steps between regriddings
      myregridsteps = Format.readInt(in);

      // Line 4:  number of steps between writing out hierarchy
      myplotsteps = Format.readInt(in);

      in.close();

    } catch (IOException x) {
      // includes EOFException
      System.out.println("Error in reading global parameters in " + inputfile);
      System.exit(0);
    }
    Point<2> extent = broadcast myextent from 0;
    int single finest = broadcast myfinest from 0;
    int single regridsteps = broadcast myregridsteps from 0;
    int single plotsteps = broadcast myplotsteps from 0;

    // System.out.println(myProc + " going to SetProb");
    SetProb(extent, finest, fluxFn, regridsteps, plotsteps);

    return in;
  }
}
