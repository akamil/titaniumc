 class List_Patch2 {
    public Patch2 car;
    public List_Patch2 cdr;

    // Main constructor.
    public List_Patch2(Patch2 x, List_Patch2 rest) {
	car = x;
	cdr = rest;
    }

    public List_Patch2 cons(Patch2 x) {
	return new List_Patch2(x, this);
    }        

    public /*inline*/ Patch2 first() {
	return car;
    }
    public /*inline*/ List_Patch2 rest() {
	return cdr;
    }

    public int length() {
	int i = 0;
	List_Patch2 r = this;
	while (r != null) {
	    i++;
	    r = r.rest();
	}
	return i;
    }
}
