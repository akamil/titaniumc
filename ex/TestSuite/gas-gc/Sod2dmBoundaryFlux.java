public class Sod2dmBoundaryFlux extends Sod2dm implements BoundaryFlux2 {

  // Perhaps use the physical boundary information from level?
  // Right now, all I use from level is ds.

  public final void f(Quantities2 [2d] q,
		      Level level, int sdim, double time,
                      Vector4 [2d] flux) {

    RectDomain<2> Dside = q.domain();
    int dim = (sdim > 0) ? sdim : -sdim;
    int side = (sdim > 0) ? 1 : -1;

    RectDomain<2> Dflux = Dside + Amr2.edge[dim][side];

    foreach (e in Dflux) flux[e] =
      q[e + Amr2.face[dim][-side]].solidBndryFlux(sdim);
  }
}
