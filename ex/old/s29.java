class a
{
  single void barrier() { }

  int b;
  single void f()
  {
    partition
    {
      b > 0 => barrier();
      b < 0 => { barrier(); barrier(); }
    }
  }
}
