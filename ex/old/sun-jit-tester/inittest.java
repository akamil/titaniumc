/*
   The inittest.java test checks if a JIT changes the order in which
   classes are initialized. Java semantics do not allow a class to be
   initialized until it is actually used.
*/

class inittest {
    public static void main(String args[]) {
	int i, x;
	for (i = 0; i < 10; i++) {
	    x = i * 10;
	    if (x < 0) {
		inittest2.foo(x);
	    }
	}
    }
}

class inittest2 {
    static { 
        System.out.println("\n\nInitializing inittest2, test FAILS\n"); 
    }

    public static void foo(int x) {
	System.out.println("foo value = " + x);
    }
}
