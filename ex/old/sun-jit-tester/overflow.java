/* 
   This test checks to see if a JIT can detect stack overflow. Method
   invocation overhead is expensive in Java and improving it is a
   nobel cause for a JIT -- it just has to be careful that it 
   doesn't loose some error handling ability in doing so.

*/

import java.lang.*;

class overflow {
    public static void main(String[] args) {
	try {
	   recurse(1);
	} catch (StackOverflowError e) {
	   System.out.println("Test PASSES");
	}
    }

    static int recurse(int n) {
	if (n != 0) {
	    return recurse(n+1);
	}
	return 0;
    }
}
