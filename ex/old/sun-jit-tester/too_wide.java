/*
     Check for intermediate results that are too wide.
     The too_wide.java test will fail if the intermediate result of 
     a float expression is maintained in greater-than-float precision, 
     or if the intermediate result of a double expression is maintained 
     in greater-than-double precision.
*/
class too_wide
{
   static double twoto(int n) {
       double res = 1.0;
       while(n > 0) {
	   res *= 2.0;
	   n -= 1;
       }
       return res;
   }

   public static void main(String[] arg) {
       float f0 = (float) twoto(0);
       float f24 = (float) twoto(24);
       if(f0 + f24 + f0 == f24)
	   System.out.println("Float test PASSES.");
       else
	   System.out.println("Float test FAILS");

       double d0 = twoto(0);
       double d53 = twoto(53);
       if(d0 + d53 + d0 == d53)
	   System.out.println("Double test PASSES.");
       else
	   System.out.println("Double test FAILS");
   }
}

