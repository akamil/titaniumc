/* 

   This JIT buster test checks to see if a JIT doing register allocation
   on a machine with a callees saves ABI for non-volatile registers can
   get the exception handling correct. Intel and PowerPC are both such
   machines. The problem is restoring the correct values of i and j in
   the catch block. If i and j are never put into registers, then the
   JIT won't have a problem with correctness because the catch block
   will load the correct values from memory. If the JIT puts i and j
   into registers, then restoring their correct values at the catch
   block gets a little bit tougher.

*/

import java.lang.*;

class exception {
    public static void main(String[] args) {
	int i, j;

	for (i=0,j=0; i<1000000; i++) {
	   j=j+1;
	   j=j+1;
	}
	try {
	   int k;
	   k = div(0);
	} catch (Exception e) {
	   if ((i != 1000000) || (j != 2000000)) {
	      System.out.println("Test FAILS");
	   } else {
	      System.out.println("Test PASSES");
	   }
	}
    }

    static int div(int n) {
	int m=15;
	m = m/n;
	return m;
    }
}
