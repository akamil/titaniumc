/*
 * Copyright (c) 1995, 1996  
 * Open Software Foundation, Inc. 
 *  
 * OSF DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
 * WITH RESPECT TO THIS SOFTWARE INCLUDING, WITHOUT LIMITATION, 
 * ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL OSF BE LIABLE FOR ANY 
 * SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES RESULTING FROM 
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN CONTRACT, TORT 
 * INCLUDING NEGLIGENCE, OR OTHER LEGAL THEORY ARISING OUT OF 
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS 
 * SOFTWARE. 
 */

/*
 * This file is part of an OSF RI software distribution. This software is 
 * distributed under the terms of a free license for non-commercial use. 
 * 
 * You should have received a copy of those terms along with this file.
 * Please see file COPYRIGHT.
 * 
 * If not, please write to: OSF RI, 2 Av. de Vignate, 38610 Gieres, France
 */

import java.io.*;

/* *********************************************************** *
 * Test JAVA exceptions :                                      *
 *   this test checks exception handling when creating our own *
 *   exceptions.                                               *
 * *********************************************************** */

// We will use predefined classes Exception and IOException and we
// will create new exception classes :
//   My_IO_exc (subclass of IOException)
//   My_exc_1 (subclass of Exception)
//   My_exc_1_1 and My_exc_1_2 (subclasses of My_exc_1)
//   My_exc_1_2_1 (subclass of My_exc_1_2)

class My_IO_exc extends IOException {}

class My_exc_1 extends Exception {}

class My_exc_1_1 extends My_exc_1 {}

class My_exc_1_2 extends My_exc_1 {}

class My_exc_1_2_1 extends My_exc_1_2 {}


class ExcUser extends validtest 
{
  int loop_count=0;		// Both variables are used to check
  int previous_except_number=0; // the order of exception occurences.

  protected void check_except(int i,int j) {
    if (loop_count!=i || previous_except_number!=j) {
      test_result=false;
    }
    previous_except_number++;
  }

  protected boolean execute (String argv[])
  {
    parse (argv);
    Exception all_except[]=new Exception[7];
    int exc_1_count=0;	// used in third test to check order of exceptions

    all_except[0]=new Exception();
    all_except[1]=new IOException();
    all_except[2]=new My_exc_1();
    all_except[3]=new My_IO_exc();
    all_except[4]=new My_exc_1_1();
    all_except[5]=new My_exc_1_2();
    all_except[6]=new My_exc_1_2_1();


/*  FIRST PART OF THE TEST : */
    // The exceptions handlers are ordered from handler for lower
    // classes exceptions to handler for upper classes.
    // So, each exception should be handled by its own handler.
    // Each exception handler checks that the exception has occured
    // at the right order.

    for (int i=0;i<7;i++) {
      try {
        loop_count++;
        throw all_except[i];
      }

      catch (My_exc_1_2_1 e) {
        check_except(7,6);
      }

      catch (My_exc_1_2 e) {
        check_except(6,5);
      }

      catch (My_exc_1_1 e) {
        check_except(5,4);
      }

      catch (My_IO_exc e) {
        check_except(4,3);
      }

      catch (My_exc_1 e) {
        check_except(3,2);
      }

      catch (IOException e) {
        check_except(2,1);
      }

      catch (Exception e) {
        check_except(1,0);
      }

    }

    check_except(7,7);	// Check that all exceptions have been raised.

/*  SECOND PART OF THE TEST : */
    // The handler of Exception class is in first position
    // so, each exception should be handled by this handler.

    previous_except_number = 0;

    for (int i=0;i<7;i++) {
      try {
        loop_count++;
        throw all_except[i];
      }

      catch (Exception e) {
        previous_except_number++;
      }

    }

    if (previous_except_number!=7) {	// Check that all exceptions have been raised.
      test_result=false;
    }

/*  THIRD PART OF THE TEST : */
    // The exceptions handlers have the following order :
    //   My_IO_exc handler    (A)
    //   IOException handler  (B)
    //   My_exc_1_1 handler   (C)
    //   My_exc_1 handler     (D)
    //   My_exc_1_2 handler   (E)
    //   Exception handler    (F)
    //   My_exc_1_2_1 handler (G)
    //
    // so :
    //   Exception should be handled by (F)
    //   IOException .................. (B)
    //   My_exc_1 ..................... (D)
    //   My_IO_exc .................... (A)
    //   My_exc_1_1 ................... (C)
    //   My_exc_1_2 ................... (D)
    //   My_exc_1_2_1 ................. (D)

    loop_count = 0;
    previous_except_number = 0;

    for (int i=0;i<7;i++) {
      try {
        loop_count++;
        throw all_except[i];
      }

      catch (My_IO_exc e) {
        check_except(4,3);
      }

      catch (IOException e) {
        check_except(2,1);
      }

      catch (My_exc_1_1 e) {
        check_except(5,4);
      }

      catch (My_exc_1 e) {
        exc_1_count++;
        switch (exc_1_count) {
          case 1 : check_except(3,2); break;
          case 2 : check_except(6,5); break;
          case 3 : check_except(7,6); break;
          default : test_result=false;
        }
      }

      catch (Exception e) {
        check_except(1,0);
      }

    }

    check_except(7,7);	// Check that all exceptions have been raised.

    WriteHTMLResults("ExcUser.html", "User defined exceptions test");
    return test_result;
  }
}
