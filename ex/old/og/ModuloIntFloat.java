/*
 * Copyright (c) 1995, 1996  
 * Open Software Foundation, Inc. 
 *  
 * OSF DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
 * WITH RESPECT TO THIS SOFTWARE INCLUDING, WITHOUT LIMITATION, 
 * ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL OSF BE LIABLE FOR ANY 
 * SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES RESULTING FROM 
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN CONTRACT, TORT 
 * INCLUDING NEGLIGENCE, OR OTHER LEGAL THEORY ARISING OUT OF 
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS 
 * SOFTWARE. 
 */

/*
 * This file is part of an OSF RI software distribution. This software is 
 * distributed under the terms of a free license for non-commercial use. 
 * 
 * You should have received a copy of those terms along with this file.
 * Please see file COPYRIGHT.
 * 
 * If not, please write to: OSF RI, 2 Av. de Vignate, 38610 Gieres, France
 */


/* ************************************************** *
 * This test checks that the modulo operator computes *
 *   the right result for every arithmetic type.      *
 * ************************************************** */

class modulo_int_data extends ModuloIntFloat {
byte ba, bb, br;
short sa, sb, sr;
int ia, ib, ir;
long la, lb, lr;
float fa, fb, fr;
double da, db, dr;

byte data_type = NONE;
boolean result = true;

    protected modulo_int_data(byte dt, long a, long b, long r) {
	data_type = dt;

	la=a; lb=b; lr=r;
	switch (data_type) {
	  case BYTE:
	    ba=(byte)a; bb=(byte)b; br=(byte)r;
	  case SHORT:
	    sa=(short)a; sb=(short)b; sr=(short)r;
	    fa=(short)a; fb=(short)b; fr=(short)r; // short can be converted to float
	  case INT:
	    ia=(int)a; ib=(int)b; ir=(int)r;
	    da=(int)a; db=(int)b; dr=(int)r;       // int can be converted to double
	  case LONG:
	    la=a; lb=b; lr=r;
	}
    }

    protected boolean check_modulo() {

	switch (data_type) {
	  case BYTE:
	    result = result & check(ba%bb,br, BYTE);
	  case SHORT:
	    result = result & check(sa%sb,sr, SHORT);
	    result = result & check((long)(fa%fb),(long)fr, FLOAT);
	  case INT:
	    result = result & check(ia%ib,ir, INT);
	    result = result & check((long)(da%db),(long)dr, DOUBLE);
	  case LONG:
	    result = result & check(la%lb,lr, LONG);
	}
	return result;
    }

    private boolean check(long left, long right, byte check_type) {
	boolean res = (left == right);

	if (!res) {
	    displayArea("   Error: "+la+" % "+lb+" != "+right+" ("+left+") for type "+type_name[check_type]+";");
	}
/*
// *** For debugging only
	if (check_type == data_type)
	    System.out.print(type_name[data_type].substring(0,1));
	System.out.print(type_name[check_type].substring(0,1).toLowerCase());
	System.out.flush();
// *** End of Debug
*/
	return res;
    }
}


class modulo_float_data extends ModuloIntFloat {
float fa, fb, fr;
double da, db, dr;

byte data_type = NONE;
boolean result = true;

    protected modulo_float_data(byte dt, double a, double b, double r) {
	data_type = dt;

	da=a; db=b; dr=r;
	switch (data_type) {
	  case FLOAT:
	    fa=(float)a; fb=(float)b; fr=(float)r;
	  case DOUBLE:
	    da=a; db=b; dr=r;
	}
    }

    protected boolean check_modulo() {

	switch (data_type) {
	  case FLOAT:
	    result = result & check(fa%fb,fr, FLOAT);
	  case DOUBLE:
	    result = result & check(da%db,dr, DOUBLE);
	}
	return result;
    }

    private boolean check(double left, double right, byte check_type) {
	boolean res = (left == right) || 
	  ((Double.isNaN(left)) && (Double.isNaN(right)));

	if (!res) {
	    displayArea("   Error: "+da+" % "+db+" != "+right+" ("+left+") for type "+type_name[check_type]+";");
	}
/*
// *** For debugging only
	if (check_type == data_type)
	    System.out.print(type_name[data_type].substring(0,1));
	System.out.print(type_name[check_type].substring(0,1).toLowerCase());
	System.out.flush();
// *** End of Debug
*/
	return res;
    }
}

class ModuloIntFloat extends validtest {
protected static final byte NONE = 0;
protected static final byte BYTE = 1;
protected static final byte SHORT = 2;
protected static final byte INT = 3;
protected static final byte LONG = 4;
protected static final byte FLOAT = 5;
protected static final byte DOUBLE = 6;
protected static final String[] type_name =
	{"None", "Byte", "Short", "Int", "Long", "Float", "Double"};

    protected boolean execute (String argv[]) 
    {
      boolean bApplet = false;
        parse (argv);
        if(m_bApplet)
	  bApplet = true; // as m_bApplet is static and inatialized to false
	                  // and we construct many GenericTestClass Object, store
	modulo_int_data data_int[] = {
	    new modulo_int_data(BYTE, 0,   2,   0),
	    new modulo_int_data(BYTE, 1,   2,   1),
	    new modulo_int_data(BYTE, -2,  -2,  0),
	    new modulo_int_data(BYTE, -3,  2,   -1),
	    new modulo_int_data(BYTE, 4,   2,   0),
	    new modulo_int_data(BYTE, 7,   -2,  1),
	    new modulo_int_data(BYTE, -8,  -2,  0),
	    new modulo_int_data(BYTE, -15, 2,   -1),
	    new modulo_int_data(BYTE, 16,  2,   0),
	    new modulo_int_data(BYTE, 31,  2,   1),
	    new modulo_int_data(BYTE, -32, 2,   0),
	    new modulo_int_data(BYTE, -63, -2,  -1),
	    new modulo_int_data(BYTE, 64,  -2,  0),
	    new modulo_int_data(BYTE, 127, -2,  1),
	    new modulo_int_data(BYTE, -128,-2,  0),
	    new modulo_int_data(BYTE, -128,-128,0),
	    new modulo_int_data(BYTE, -128,1,   0),
	    new modulo_int_data(BYTE, 37,  127, 37),
	    new modulo_int_data(BYTE, 97,  -127,97),
	    new modulo_int_data(BYTE, 17,  -128,17),

	    new modulo_int_data(SHORT, 128,   256,   128),
	    new modulo_int_data(SHORT, 129,   256,   129),
	    new modulo_int_data(SHORT, -255,  256,   -255),
	    new modulo_int_data(SHORT, -256,  256,   0),
	    new modulo_int_data(SHORT, 511,   -256,  255),
	    new modulo_int_data(SHORT, 512,   -256,  0),
	    new modulo_int_data(SHORT, 32767, 32767, 0),
	    new modulo_int_data(SHORT, 32767, -32768,32767),
	    new modulo_int_data(SHORT, -32768,32767, -1),
	    new modulo_int_data(SHORT, -32768,-32768,0),
	    new modulo_int_data(SHORT, 32767, 1,     0),
	    new modulo_int_data(SHORT, -32768,-1,    0),
	    new modulo_int_data(SHORT, 1999,  1789,  210),
	    new modulo_int_data(SHORT, 32765, 5,     0),
	    new modulo_int_data(SHORT, -32768,5,     -3),
	    new modulo_int_data(SHORT, 32766, 5,     1),

	    new modulo_int_data(INT, 0x00008000, 0x00010000, 0x00008000),
	    new modulo_int_data(INT, -0x00008000,-0x00010000,-0x00008000),
	    new modulo_int_data(INT, 0x00010000, 0x00010000, 0),
	    new modulo_int_data(INT, -0x000100001,0x00010000,-1),
	    new modulo_int_data(INT, 0x7fffffff, 0x7fffffff, 0),
	    new modulo_int_data(INT, 0x80000000, 0x7fffffff, -1),
	    new modulo_int_data(INT, 0x7fffffff, 0x80000000, 0x7fffffff),
	    new modulo_int_data(INT, 0x80000000, 0x80000000, 0),
	    new modulo_int_data(INT, 0x7fffffff, 1,          0),
	    new modulo_int_data(INT, 0x80000000, 1,          0),
	    new modulo_int_data(INT, 0x7fffffff, 2,          1),
	    new modulo_int_data(INT, 0x80000001, 2,          -1),

	    new modulo_int_data(LONG, 0x0000000080000000L, 0x000000100000000L, 0x0000000080000000L),
	    new modulo_int_data(LONG, -0x0000000080000000L,-0x000000100000000L,-0x0000000080000000L),
	    new modulo_int_data(LONG, 0x0000000100000000L, 0x0000000100000000L, 0L),
	    new modulo_int_data(LONG, -0x0000000100000001L, 0x0000000100000000L, -1L),
	    new modulo_int_data(LONG, 0x7fffffffffffffffL, 0x7fffffffffffffffL, 0L),
	    new modulo_int_data(LONG, 0x8000000000000000L, 0x7fffffffffffffffL, -1L),
	    new modulo_int_data(LONG, 0x7fffffffffffffffL, 0x8000000000000000L, 0x7fffffffffffffffL),
	    new modulo_int_data(LONG, 0x8000000000000000L, 0x8000000000000000L, 0L),
	    new modulo_int_data(LONG, 0x7fffffffffffffffL, 2L, 1L),
	    new modulo_int_data(LONG, 0x8000000000000001L, 2L, -1L) };
 

	modulo_float_data data_float[] = {
	    new modulo_float_data(FLOAT, 7717.F,     151.F,    16.F),
	    new modulo_float_data(FLOAT, 71.77e2F,   7.87E+2F, .94e+2F),
	    new modulo_float_data(FLOAT, 123.456e3F, 653.2e1F, 588E1F),
	    new modulo_float_data(FLOAT, 640.25F,    32.5F,    22.75F),
	    new modulo_float_data(FLOAT, 640.25F,    -32.5F,   22.75F),
	    new modulo_float_data(FLOAT, -640.25F,   32.5F,    -22.75F),
	    new modulo_float_data(FLOAT, -640.25F,   -32.5F,   -22.75F),
	    new modulo_float_data(FLOAT, 151.29e32F, 123e32F,  28.29e32F),

	    new modulo_float_data(DOUBLE, 1.e0,      1.e19,    1),
	    new modulo_float_data(DOUBLE, 1.0e19,    1.0e10,   0),  // a/b < 2^31
	    new modulo_float_data(DOUBLE, 1.0e20,    1.0e10,   0),  // a/b > 2^31
	    new modulo_float_data(DOUBLE, 1.0e28,    1.0e10,   0),  // a/b < 2^63
	    new modulo_float_data(DOUBLE, 1.0e29,    1.0e10,   0), // a/b > 2^63
	    new modulo_float_data(DOUBLE, 1.0e10,    0.0,   Double.NaN), // given in the spec
	    new modulo_float_data(DOUBLE, Double.POSITIVE_INFINITY,    1.0e10,   Double.NaN), // usual spec but not specified in the VM doc
	    new modulo_float_data(DOUBLE, 1.0e10, Double.POSITIVE_INFINITY, 1.0e10), //  usual spec but not specified in the VM doc
	    new modulo_float_data(DOUBLE, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY,   Double.NaN), //  usual spec but not specified in the VM doc

	    new modulo_float_data(DOUBLE, (double) power(2,100), (double) power(2,10), 0) };//  usual spec but not specified in the VM doc

	if(bApplet)
	  m_bApplet = true;   // recall
	for (int i=0; i<data_int.length; i++)
	    test_result = test_result & data_int[i].check_modulo();

	for (int i=0; i<data_float.length; i++)
	    test_result = test_result & data_float[i].check_modulo();

	WriteHTMLResults("ModuloIntFloat.html", "modulo operator test");
	return test_result;

    }

public double power(double base,int power) {
  double result=1;
  while (power > 0) {
    result *= base;
    power -= 1;
  }
  return (result); 
}
}
