/*
 * Copyright (c) 1995, 1996  
 * Open Software Foundation, Inc. 
 *  
 * OSF DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
 * WITH RESPECT TO THIS SOFTWARE INCLUDING, WITHOUT LIMITATION, 
 * ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL OSF BE LIABLE FOR ANY 
 * SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES RESULTING FROM 
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN CONTRACT, TORT 
 * INCLUDING NEGLIGENCE, OR OTHER LEGAL THEORY ARISING OUT OF 
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS 
 * SOFTWARE. 
 */

/*
 * This file is part of an OSF RI software distribution. This software is 
 * distributed under the terms of a free license for non-commercial use. 
 * 
 * You should have received a copy of those terms along with this file.
 * Please see file COPYRIGHT.
 * 
 * If not, please write to: OSF RI, 2 Av. de Vignate, 38610 Gieres, France
 */

import java.util.*;

/* ************************************************ *
 * Test write access in arrays of arrays of classes *
 * ************************************************ */

class item {
  static int counter = 0 ;
  public int val ;

  public item() {
    val = counter++ ;
  }
}

class ArrayClass extends validtest
{

  protected boolean execute(String argv[]) 
  {
    item.counter = 0;  // for appletests
    item[][]  t1 ;
    item[][]  t2 ;
    item[]    t ;
    int verif=0;

    parse (argv);    

    t1 = new item[4][4] ;
    t2 = new item[4][4] ;

    for ( int i = 0 ; i < 4 ; i++ ) {
      t = new item[4] ;
      for ( int j = 0 ; j < 4 ; j++ ) {
        t[j] = new item() ;
      }
      t1[i] = t ;
    }

    for ( int i = 0 ; i < 4 ; i++ ) {
      for ( int j = 0 ; j < 4 ; j++ ) {
        t2[i][j] = new item() ;
      }
    }

    for ( int i = 0 ; i < 4 ; i++ ) {
      for ( int j = 0 ; j < 4 ; j++ ) {
        test_result = test_result & (t1[i][j].val==verif);
        verif++;
      }
    }

    for ( int i = 0 ; i < 4 ; i++ ) {
      for ( int j = 0 ; j < 4 ; j++ ) {
        test_result = test_result & (t2[i][j].val==verif);
        verif++;
      }
    }

    WriteHTMLResults("ArrayClass.html", "Write access in arrays of arrays of classes test") ;
    return test_result;
  }
}
