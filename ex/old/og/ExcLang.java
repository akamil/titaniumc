/*
 * Copyright (c) 1995, 1996  
 * Open Software Foundation, Inc. 
 *  
 * OSF DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
 * WITH RESPECT TO THIS SOFTWARE INCLUDING, WITHOUT LIMITATION, 
 * ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL OSF BE LIABLE FOR ANY 
 * SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES RESULTING FROM 
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN CONTRACT, TORT 
 * INCLUDING NEGLIGENCE, OR OTHER LEGAL THEORY ARISING OUT OF 
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS 
 * SOFTWARE. 
 */

/*
 * This file is part of an OSF RI software distribution. This software is 
 * distributed under the terms of a free license for non-commercial use. 
 * 
 * You should have received a copy of those terms along with this file.
 * Please see file COPYRIGHT.
 * 
 * If not, please write to: OSF RI, 2 Av. de Vignate, 38610 Gieres, France
 */

import java.lang.*;
import java.util.*;

/* *********************************************************** *
 * Test JAVA exceptions :                                      *
 *   this test checks that all exceptions defined in java.lang *
 *   can be raised and handled.                                *
 * *********************************************************** */


class ExcLang extends validtest
{

    protected void check_except(int i, int n) {
	if (n != i) {
	    displayArea("Error "+i+" != "+n);
	    test_result=false;
	}
    }

    protected boolean execute(String argv[]) 
    {
	int i;
        
        parse (argv);

	Throwable all_except[] = {
	    new AbstractMethodError(),		//0
	    new ArithmeticException("arithm"),	//1
	    new ArrayIndexOutOfBoundsException(),//2
	    new ArrayStoreException(),		//3
	    new ClassCastException(),		//4
	    new ClassCircularityError(),	//5
	    new ClassFormatError(),		//6
	    new ClassNotFoundException(),	//7
	    new CloneNotSupportedException(),	//8
	    new Error(),			//9
	    new Exception(),			//10
	    new IllegalAccessError(),		//11
	    new IllegalAccessException(),	//12
	    new IllegalArgumentException(),	//13
	    new IllegalMonitorStateException(),	//14
	    new IllegalThreadStateException(),	//15
	    new IncompatibleClassChangeError(),	//16
	    new IndexOutOfBoundsException(),	//17
	    new InstantiationError(),		//18
	    new InstantiationException(),	//19
	    new InternalError(),		//20
	    new InterruptedException(),		//21
	    new LinkageError(),			//22
	    new NegativeArraySizeException(),	//23
	    new NoClassDefFoundError(),		//24
	    new NoSuchFieldError(),		//25
	    new NoSuchMethodError(),		//26
	    new NoSuchMethodException(),	//27
	    new NullPointerException(),		//28
	    new NumberFormatException(),	//29
	    new OutOfMemoryError(),		//30
	    new StackOverflowError(),		//31
	    new RuntimeException(),		//32
	    new SecurityException(),		//33
	    new StringIndexOutOfBoundsException(),//34
	    new ThreadDeath(),			//35
	    new UnknownError(),			//36
	    new UnsatisfiedLinkError(),		//37
	    new VerifyError() };		//38
//        new VirtualMachineError() };		//39 Is an abstract Class ???


/*  FIRST PART OF THE TEST : */
    // Exceptions are raised in alphabetical order.
    // The exceptions handlers are in random order.
    // The handler of superclass is added in last position
    // each exception should be handled by its own handler.
    // Each exception handler checks that the exception has occured
    // at the right order.

	for (i=0; i<all_except.length; i++) {
	    try {
		throw all_except[i];
	    }

	    catch (ClassNotFoundException e) {
		check_except(i, 7);
	    }

	    catch (CloneNotSupportedException e) {
		check_except(i, 8);
	    }

	    catch (IllegalAccessException e) {
		check_except(i, 12);
	    }

	    catch (InstantiationException e) {
		check_except(i, 19);
	    }

	    catch (InterruptedException e) {
		check_except(i, 21);
	    }

	    catch (NoSuchMethodException e) {
		check_except(i, 27);
	    }

	    catch (ArithmeticException e) {
		check_except(i, 1);
	    }

	    catch (ArrayStoreException e) {
		check_except(i, 3);
	    }

	    catch (ClassCastException e) {
		check_except(i, 4);
	    }
 
	    catch (IllegalThreadStateException e) {
		check_except(i, 15);
	    }

	    catch (NumberFormatException e) {
		check_except(i, 29);
	    }

	    catch (IllegalArgumentException e) {
		check_except(i, 13);
	    }

	    catch (IllegalMonitorStateException e) {
		check_except(i, 14);
	    }

	    catch (ArrayIndexOutOfBoundsException e) {
		check_except(i, 2);
	    }

	    catch (StringIndexOutOfBoundsException e) {
		check_except(i, 34);
	    }
 
	    catch (IndexOutOfBoundsException e) {
		check_except(i, 17);
	    }

	    catch (NegativeArraySizeException e) {
		check_except(i, 23);
	    }

	    catch (NullPointerException e) {
		check_except(i, 28);
	    }

	    catch (SecurityException e) {
		check_except(i, 33);
	    }

	    catch (RuntimeException e) {
		check_except(i, 32);
	    }

	    catch (Exception e) {
		check_except(i, 10);
	    }

	    catch (ClassCircularityError e) {
		check_except(i, 5);
	    }

	    catch (ClassFormatError e) {
		check_except(i, 6);
	    }

	    catch (AbstractMethodError e) {
		check_except(i, 0);
	    }

	    catch (IllegalAccessError e) {
		check_except(i, 11);
	    }

	    catch (InstantiationError e) {
		check_except(i, 18);
	    }

	    catch (NoSuchFieldError e) {
		check_except(i, 25);
	    }

	    catch (NoSuchMethodError e) {
		check_except(i, 26);
	    }

	    catch (IncompatibleClassChangeError e) {
		check_except(i, 16);
	    }

	    catch (NoClassDefFoundError e) {
		check_except(i, 24);
	    }

	    catch (UnsatisfiedLinkError e) {
		check_except(i, 37);
	    }

	    catch (VerifyError e) {
		check_except(i, 38);
	    }

	    catch (LinkageError e) {
		check_except(i, 22);
	    }

	    catch (ThreadDeath e) {
		check_except(i, 35);
	    }

	    catch (InternalError e) {
		check_except(i, 20);
	    }

	    catch (OutOfMemoryError e) {
		check_except(i, 30);
	    }

	    catch (StackOverflowError e) {
		check_except(i, 31);
	    }

	    catch (UnknownError e) {
		check_except(i, 36);
	    }

	    catch (VirtualMachineError e) {
		check_except(i, -1);
	    }


	    catch (Error e) {
		check_except(i, 9);
	    }

	    catch (Throwable e) {
		check_except(i, -1);
	    }
     
	}

    check_except(i, all_except.length);	// Check that all exceptions have been raised.

/*  SECOND PART OF THE TEST : */
   // Exceptions are raised in alphabetical order.
    // The only one exceptions handler is for Throwbale, which should
    // catch every exceptions.

	byte except_count = 0;

	for (i=0; i<all_except.length; i++) {
	    try {
		throw all_except[i];
	    }     

	    catch (Throwable e) {
		except_count ++;
	    }
	}

	check_except(except_count, all_except.length);
    
	WriteHTMLResults("ExcLang.html", "JAVA Exceptions Test") ;
	return test_result;
    }
}
