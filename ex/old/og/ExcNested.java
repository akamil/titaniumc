/*
 * Copyright (c) 1995, 1996  
 * Open Software Foundation, Inc. 
 *  
 * OSF DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
 * WITH RESPECT TO THIS SOFTWARE INCLUDING, WITHOUT LIMITATION, 
 * ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL OSF BE LIABLE FOR ANY 
 * SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES RESULTING FROM 
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN CONTRACT, TORT 
 * INCLUDING NEGLIGENCE, OR OTHER LEGAL THEORY ARISING OUT OF 
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS 
 * SOFTWARE. 
 */

/*
 * This file is part of an OSF RI software distribution. This software is 
 * distributed under the terms of a free license for non-commercial use. 
 * 
 * You should have received a copy of those terms along with this file.
 * Please see file COPYRIGHT.
 * 
 * If not, please write to: OSF RI, 2 Av. de Vignate, 38610 Gieres, France
 */

import java.util.*;

/* *********************************************************** *
 * Test JAVA exceptions :                                      *
 *   this test checks the nested raising of all exceptions     *
 *   defined in java.lang : exceptions are called during       *
 *   previous exception handling.                              *
 * *********************************************************** */


class ExcNested extends validtest
{
  int except_count;

  Throwable all_except[] = {
    new AbstractMethodError(),		//0
    new ArithmeticException(),		//1
    new ArrayIndexOutOfBoundsException(),//2
    new ArrayStoreException(),		//3
    new ClassCastException(),		//4
    new ClassCircularityError(),	//5
    new ClassFormatError(),		//6
    new ClassNotFoundException(),	//7
    new CloneNotSupportedException(),	//8
    new Error(),			//9
    new Exception(),			//10
    new IllegalAccessError(),		//11
    new IllegalAccessException(),	//12
    new IllegalArgumentException(),	//13
    new IllegalMonitorStateException(),	//14
    new IllegalThreadStateException(),	//15
    new IncompatibleClassChangeError(),	//16
    new IndexOutOfBoundsException(),	//17
    new InstantiationError(),		//18
    new InstantiationException(),	//19
    new InternalError(),		//20
    new InterruptedException(),		//21
    new LinkageError(),			//22
    new NegativeArraySizeException(),	//23
    new NoClassDefFoundError(),		//24
    new NoSuchFieldError(),		//25
    new NoSuchMethodError(),		//26
    new NoSuchMethodException(),	//27
    new NullPointerException(),		//28
    new NumberFormatException(),	//29
    new OutOfMemoryError(),		//30
    new StackOverflowError(),		//31
    new RuntimeException(),		//32
    new SecurityException(),		//33
    new StringIndexOutOfBoundsException(),//34
    new ThreadDeath(),			//35
    new UnknownError(),			//36
    new UnsatisfiedLinkError(),		//37
    new VerifyError() };		//38
//        new VirtualMachineError() };		//39 Is an abstract Class ???


  protected boolean execute(String argv[]) 
  {
    parse (argv);

    try {
	test_result = true;
	nested_except();
    }
    catch (Throwable e) {
	displayArea("Error: Exception "+except_count+" not handled;");
	test_result = false;
    }

    WriteHTMLResults("ExcNested.html", "JAVA exceptions when nested"); ;
    return test_result;

  }

  private void check_except(int i) throws Throwable {
    if (except_count != i) {
      displayArea("Error "+except_count+" != "+i+";");
      test_result=false;
    }
    throw all_except[++except_count]; 
  }

  private void nested_except() throws Throwable {

    // Nested exceptions are raised in alphabetical order.
    // The exceptions handlers are in random order.
    // Each exception handler check that the exception has occured
    // at the right order.


    try {
      except_count = 0;
      throw all_except[except_count];
    }
    catch (AbstractMethodError e0) {
      try {
	check_except(0);
      }
      catch (ArithmeticException e1) {
	try {
	  check_except(1);
	}
	catch (ArrayIndexOutOfBoundsException e2) {
	  try {
	    check_except(2);
	  }
	  catch (ArrayStoreException e3) {
	    try {
	      check_except(3);
	    }
	    catch (ClassCastException e4) {
	      try {
		check_except(4);
	      }
	      catch (ClassCircularityError e5) {
		try {
		  check_except(5);
		}
		catch (ClassFormatError e6) {
		  try {
		    check_except(6);
		  }
		  catch (ClassNotFoundException e7) {
		    try {
		      check_except(7);
		    }
		    catch (CloneNotSupportedException e8) {
		      try {
			check_except(8);
		      }
		      catch (Error e9) {
			try {
			  check_except(9);
			}
			catch (Exception e10) {
			  try {
			    check_except(10);
			  }
			  catch (IllegalAccessError e11) {
			    try {
			      check_except(11);
			    }
			    catch (IllegalAccessException e12) {
			      try {
				check_except(12);
			      }
			      catch (IllegalArgumentException e13) {
				try {
				  check_except(13);
				}
				catch (IllegalMonitorStateException e14) {
				  try {
				    check_except(14);
				  }
				  catch (IllegalThreadStateException e15) {
				    try {
				      check_except(15);
				    }
				    catch (IncompatibleClassChangeError e16) {
				      try {
					check_except(16);
				      }
				      catch (IndexOutOfBoundsException e17) {
					try {
					  check_except(17);
					}
					catch (InstantiationError e18) {
					  try {
					    check_except(18);
					  }
					  catch (InstantiationException e19) {
					    try {
					      check_except(19);
					    }
					    catch (InternalError e20) {
					      try {
						check_except(20);
					      }
					      catch (InterruptedException e21) {
						try {
						  check_except(21);
						}
						catch (LinkageError e22) {
						  try {
						    check_except(22);
						  }
						  catch (NegativeArraySizeException e23) {
						    try {
						      check_except(23);
						    }
						    catch (NoClassDefFoundError e24) {
						      try {
							check_except(24);
						      }
						      catch (NoSuchFieldError e25) {
							try {
							  check_except(25);
							}
							catch (NoSuchMethodError e26) {
							  try {
							    check_except(26);
							  }
							  catch (NoSuchMethodException e27) {
							    try {
							      check_except(27);
							    }
							    catch (NullPointerException e28) {
							      try {
								check_except(28);
							      }
							      catch (NumberFormatException e29) {
								try {
								  check_except(29);
								}
								catch (OutOfMemoryError e30) {
								  try {
								    check_except(30);
								  }
								  catch (StackOverflowError e31) {
								    try {
								      check_except(31);
								    }
								    catch (RuntimeException e32) {
								      try {
									check_except(32);
								      }
								      catch (SecurityException e33) {
									try {
									  check_except(33);
									}
									catch (StringIndexOutOfBoundsException e34) {
									  try {
									    check_except(34);
									  }
									  catch (ThreadDeath e35) {
									    try {
									      check_except(35);
									    }
									    catch (UnknownError e36) {
									      try {
										check_except(36);
									      }
									      catch (UnsatisfiedLinkError e37) {
										try {
										  check_except(37);
										}
										catch (VerifyError e38) {
										  ++except_count;
	} } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } }

    // Check that all exceptions have been raised.
    if (except_count != all_except.length) {
	test_result = false;
    }
  }
}
