/*
 * Copyright (c) 1995, 1996  
 * Open Software Foundation, Inc. 
 *  
 * OSF DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
 * WITH RESPECT TO THIS SOFTWARE INCLUDING, WITHOUT LIMITATION, 
 * ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL OSF BE LIABLE FOR ANY 
 * SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES RESULTING FROM 
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN CONTRACT, TORT 
 * INCLUDING NEGLIGENCE, OR OTHER LEGAL THEORY ARISING OUT OF 
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS 
 * SOFTWARE. 
 */

/*
 * This file is part of an OSF RI software distribution. This software is 
 * distributed under the terms of a free license for non-commercial use. 
 * 
 * You should have received a copy of those terms along with this file.
 * Please see file COPYRIGHT.
 * 
 * If not, please write to: OSF RI, 2 Av. de Vignate, 38610 Gieres, France
 */

import java.io.PrintStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

class floattest extends GenericTestClass
{
  final static int K_FLOATCONV_PAGE =1;
  final static int K_DOUBLECONV_PAGE =2;
  final static int K_FLOATCOMP_PAGE =3;
  final static int K_DOUBLECOMP_PAGE =4;
  

  static  String m_sIdentifier="Java";
  float   floatpzero;
  float   floatnzero;
  double  doublenzero;
  double  doublepzero;
  int     m_nResPage;   // this test generates 4 HTML pages, this is the page ident
  boolean m_bFloatConvert;
  boolean m_bDoubleConvert;
  boolean m_bFloatCompare;
  boolean m_bDoubleCompare;
  boolean m_bTestResult;

  protected void parse(String argv[]){
    if (argv != null)
      for(int i =0; i < argv.length; i++)
	if ((argv[i].equals("-ident")) && (++i < argv.length))
	  m_sIdentifier = argv[i];

  }
  public boolean execute(String argv[]) 
  {
    parse (argv);
    m_bFloatConvert = true;
    m_bDoubleConvert = true;
    m_bFloatCompare = true;
    m_bDoubleCompare = true;
    m_bTestResult  = true;
    floatpzero = 1/Float.POSITIVE_INFINITY;
    floatnzero = 1/Float.NEGATIVE_INFINITY;
    
    doublepzero = 1/Double.POSITIVE_INFINITY;
    doublenzero = 1/Double.NEGATIVE_INFINITY;

    m_nResPage = K_FLOATCONV_PAGE;
    if (!m_bApplet)
      {
        WriteMainResultFile("floattest.html");
	nLineRowSpan = 14; // member of GenericTestClass
	WriteHTMLResults("floattest1.html", "Float conversion");
	m_nResPage = K_DOUBLECONV_PAGE;
	WriteHTMLResults("floattest2.html", "Double conversion");
	nLineRowSpan = 6; // member of GenericTestClass
	m_nResPage = K_FLOATCOMP_PAGE;
	WriteHTMLResults("floattest3.html", "Float comparisons");
	m_nResPage = K_DOUBLECOMP_PAGE;
	WriteHTMLResults("floattest4.html", "Double comparisons");
      }
    else
      WriteHTMLResults(null, "Double and float convertions");
    return  m_bTestResult;
  }

  public String compare(float val1, float val2, String sResult, boolean bEqual, boolean bInf, boolean bSup, 
			     boolean bSupEq, boolean bInfEq , boolean bDiff) {
      boolean localCompare;

      localCompare=true;

      if ( ((val1 == val2) != bEqual) ||
	   ((val1< val2) !=  bInf) ||
	   ((val1 > val2) != bSup ) ||
	   ((val1 >= val2) != bSupEq ) ||
	   ((val1 <= val2) !=  bInfEq) ||
	   ((val1 != val2) !=  bDiff) )
	{
	  m_bFloatCompare = false;
	  localCompare = false;
	  TRACE("Float comparison failed");
	}

      if (m_bApplet)
	sResult += (val1 +"\t "+ val2 +"\t "+ 
		       (val1 == val2) +"\t "+ 
		       (val1< val2) +"\t "+ 
		       (val1 > val2) +"\t "+ 
		       (val1 >= val2) +"\t "+ 
		       (val1 <= val2)+"\t "+ 
		       (val1 != val2)+"\n");
      else
	{
	  sResult += "<TD><STRONG>" + val1 +" [op] "+ val2+"</STRONG>";
	  sResult += "<TD>" + (val1 == val2);
	  sResult += "<TD>" + (val1< val2);
	  sResult += "<TD>" + (val1 > val2);
	  sResult += "<TD>" + (val1 >= val2);
	  sResult += "<TD>" + (val1 <= val2);
	  sResult += "<TD>" + (val1 != val2);
	  if (localCompare) sResult += "<TD>SUCCESS";
	  else sResult += "<TD>FAILED";
	  sResult += "<TR><TH>\n";
	}
      return sResult;
  }

  public String compare(double val1, double val2, String sResult, boolean bEqual, boolean bInf, boolean bSup, 
			     boolean bSupEq, boolean bInfEq , boolean bDiff) {
      boolean localCompare;

      localCompare=true;

      if ( ((val1 == val2) != bEqual) ||
	   ((val1< val2) !=  bInf) ||
	   ((val1 > val2) != bSup ) ||
	   ((val1 >= val2) != bSupEq ) ||
	   ((val1 <= val2) !=  bInfEq) ||
	   ((val1 != val2) !=  bDiff) )
	{
	  m_bDoubleCompare = false;
	  localCompare = false;
	  TRACE("Double comparison failed");
	}

      if (m_bApplet)
	sResult += val1 +"\t "+ val2 +"\t "+ 
		       (val1 == val2) +"\t "+ 
		       (val1< val2) +"\t "+ 
		       (val1 > val2) +"\t "+ 
		       (val1 >= val2) +"\t "+ 
		       (val1 <= val2) +"\t "+ 
		       (val1 != val2)+"\n";
      else
	{
	  sResult += "<TD><STRONG>" + val1 +" [op] "+ val2+"</STRONG>";
	  sResult += "<TD>" + (val1 == val2);
	  sResult += "<TD>" + (val1< val2);
	  sResult += "<TD>" + (val1 > val2);
	  sResult += "<TD>" + (val1 >= val2);
	  sResult += "<TD>" + (val1 <= val2);
	  sResult += "<TD>" + (val1 != val2)+"\n";
	  if (localCompare) sResult += "<TD>SUCCESS";
	  else sResult += "<TD>FAILED";
	  sResult += "<TR><TH>\n";
	}
      return sResult;
  }

  public String floatprint(float val, String sResult, float fRef, double dRef, long lRef, int iRef ) 
  {
    boolean localStatus;
    
    localStatus=true;

    if ( ((float)val != fRef) &&  
	 (!Float.isNaN((float)val) || !Float.isNaN(fRef)) &&  
	 (! Float.toString((float)val).equals(Float.toString(fRef)))){
      TRACE("float conversion failed -->"); TRACE("Float "+(float)val+" != "+ fRef);
      localStatus=false;
      m_bFloatConvert = false;}   

    double double_val = (double) val;
    if ( (double_val != dRef) &&  
	 (!Double.isNaN(double_val) || !Double.isNaN(dRef)) &&  
	 (! Double.toString(double_val).equals(Double.toString(dRef)))){
      TRACE("float conversion failed -->"); TRACE("Double "+double_val+" != "+ dRef);
      localStatus=false;
      m_bFloatConvert = false;}

    int ival=(int)val;
    long lval=(long)val;
    if ( ((float)ival != (float)iRef) ||
	 ((float)lval != (float)lRef)){
      TRACE("float conversion failed -->"); 
      if((float)ival != (float)iRef)
	TRACE("Int "+(int)val+" != "+ iRef);
      if ((float)val != (float)lRef)
	TRACE("Long "+(long)val+" != "+ lRef);
      localStatus=false;;
      m_bFloatConvert = false;}

    if (m_bApplet)
      sResult +=(val+"\t"+
			 (float)val +"\t"+
			 (double)val+"\t"+
			 (int)val+ "\t\t"+
			 (long)val)+"\n";
    else
      {
	  sResult += "<TD><STRONG>" + val+"</STRONG>";
	  sResult += "<TD>" + (float)val;
	  sResult += "<TD>" + (double)val;
	  sResult += "<TD>" + (int)val;
	  sResult += "<TD>" + (long)val+"\n";
	  if (localStatus) sResult += "<TD>SUCCESS";
	  else sResult += "<TD>FAILED";
	  sResult += "<TR><TH>\n";
      }
    return sResult;
  }

  public String doubleprint(double val, String sResult, float fRef, double dRef, long lRef, int iRef) {
      boolean localStatus;

      localStatus=true;

    
    if ( ( (float)val != fRef) &&  
	 (!Float.isNaN((float)val) || !Float.isNaN(fRef)) &&  
	 (! Float.toString((float)val).equals(Float.toString(fRef)))){
      TRACE("double conversion failed -->"); TRACE("Float "+(float)val+" != "+ fRef);
      localStatus=false;
      m_bDoubleConvert = false;}

    double double_val = (double) val;
    if ( ( (double)val != dRef) &&  
	 (!Double.isNaN(double_val) || !Double.isNaN(dRef)) &&  
	 (! Double.toString(double_val).equals(Double.toString(dRef)))){
      TRACE("double conversion failed -->"); TRACE("Double "+(double)val+" != "+ dRef);
      localStatus=false;
      m_bDoubleConvert = false;}

    int ival=(int)val;
    long lval=(long)val;
    if ( ((double)ival != (double)iRef) ||
	 ((double)lval != (double)lRef)){
      TRACE("double conversion failed -->"); 
      if((double)ival != (double)iRef)
	TRACE("Int "+(int)val+" != "+ iRef);
      if ((double)val != (double)lRef)
	TRACE("Long "+(long)val+" != "+ lRef);
      localStatus=false;
      m_bDoubleConvert = false;}

    if (m_bApplet)
	sResult += (val+"\t"+
			 (float)val +"\t"+
			 (double)val+"\t"+
			 (int)val+"\t\t"+
			 (long)val)+"\n";
    else
	{
	  sResult += "<TD><STRONG>" + val+"</STRONG>";
	  sResult += "<TD>" + (float)val;
	  sResult += "<TD>" + (double)val;
	  sResult += "<TD>" + (int)val;
	  sResult += "<TD>" + (long)val+"\n";
	  if (localStatus) sResult += "<TD>SUCCESS";
	  else sResult += "<TD>FAILED";
	  sResult += "<TR><TH>\n";
	}
    return sResult;
  }
  
  public void setApplet()
  {
    Parameter pParam;
    m_sIdentifier="Java";
    if((m_pTest != null) && (m_pTest.sTestArgv != null))
      parse(m_pTest.sTestArgv);

    super.setApplet();
    //lParameters = new Vector();
    //pParam = new Parameter("identifier", "-ident", m_sIdentifier);
    //lParameters.addElement( pParam);
  }      

  public String getHTMLFileName() {
    // do not write in floattest.html
    // results are on separate pages
    return null;
  }

  protected String WriteSpecificResults(String sResult)
  { 
    m_bFloatConvert = true;
    m_bDoubleConvert = true;
    m_bFloatCompare = true;
    m_bDoubleCompare = true;    
    
    if (m_bApplet) {
      //sResult += "Identifier : " + m_sIdentifier+"\n";
    } else {
      sResult += "<TD ROWSPAN="+nLineRowSpan+">" + m_sIdentifier;
    }

    if (m_nResPage == K_FLOATCONV_PAGE)
      {
	if (m_bApplet){
	  sResult += "float conversions\n";
	  sResult += "none \tfloat \tdouble \tint \t\tlong\n";}

	sResult = floatprint(0.25F, sResult, 0.25F, 0.25D, 0L, 0);
	sResult = floatprint(1.2345F, sResult, 1.2345F, 1.2345D, 1L, 1);
	sResult = floatprint(((float) Integer.MAX_VALUE),sResult,((float)Integer.MAX_VALUE),
			     ((double)Integer.MAX_VALUE),((long)Integer.MAX_VALUE),((int)Integer.MAX_VALUE));
	sResult = floatprint(((float) Integer.MAX_VALUE)+1,sResult,((float)Integer.MAX_VALUE)+1,((double)Integer.MAX_VALUE)+1,((long)Integer.MAX_VALUE)+1,((int)Integer.MAX_VALUE));
	sResult = floatprint(((float) Integer.MIN_VALUE),sResult,((float)Integer.MIN_VALUE),((double)Integer.MIN_VALUE),((long)Integer.MIN_VALUE),((int)Integer.MIN_VALUE));
	sResult = floatprint(((float) Long.MAX_VALUE),sResult,((float)Long.MAX_VALUE),((double)Long.MAX_VALUE),((long)Long.MAX_VALUE),((int)Integer.MAX_VALUE));
	sResult = floatprint(((float) Long.MAX_VALUE)+1,sResult,((float)Long.MAX_VALUE)+1,((double)Long.MAX_VALUE)+1,((long)Long.MAX_VALUE),((int)Integer.MAX_VALUE));
	sResult = floatprint(((float) Long.MIN_VALUE),sResult,((float)Long.MIN_VALUE),((double)Long.MIN_VALUE),((long)Long.MIN_VALUE),((int)Integer.MIN_VALUE));
	sResult = floatprint(1e25F,sResult,1e25F,1e25D,((long)Long.MAX_VALUE),((int)Integer.MAX_VALUE));
	sResult = floatprint(Float.NaN, sResult, Float.NaN, Double.NaN, 0, 0);
	sResult = floatprint(Float.POSITIVE_INFINITY, sResult, Float.POSITIVE_INFINITY,Double.POSITIVE_INFINITY, 
			     Long.MAX_VALUE, Integer.MAX_VALUE);
	sResult = floatprint(Float.NEGATIVE_INFINITY, sResult, Float.NEGATIVE_INFINITY,Double.NEGATIVE_INFINITY, 
			     Long.MIN_VALUE, Integer.MIN_VALUE);
	sResult = floatprint(floatpzero, sResult, floatpzero, doublepzero, 0, 0);
	sResult = floatprint(floatnzero, sResult, floatnzero, doublenzero, 0, 0 );
        if (m_bFloatConvert) {
	  TRACE("Float conversion succeeded");
	  if (m_bApplet) 
	    sResult += "Float conversion succeeded\n";}
	else{
	  if (m_bApplet) sResult += "Float conversion failed\n";
 	  m_bTestResult = false;}
     }
    if ( (m_nResPage == K_DOUBLECONV_PAGE) || m_bApplet)
      {
	if (m_bApplet){
	  sResult += "double conversions\n";
	  sResult += "none \tfloat \tdouble \tint \t\tlong\n";}

	sResult = doubleprint(0.25F, sResult, 0.25F, 0.25D, 0L, 0);
	sResult = doubleprint(1.2345F, sResult, 1.2345F, 1.2345D, 1L, 1);
	sResult = doubleprint(((double) Integer.MAX_VALUE),sResult,((float)Integer.MAX_VALUE),((double)Integer.MAX_VALUE),((long)Integer.MAX_VALUE),((int)Integer.MAX_VALUE));
	sResult = doubleprint(((double) Integer.MAX_VALUE)+1,sResult,((float)Integer.MAX_VALUE)+1,((double)Integer.MAX_VALUE)+1,((long)Integer.MAX_VALUE)+1,((int)Integer.MAX_VALUE));
	sResult = doubleprint(((double) Integer.MIN_VALUE),sResult,((float)Integer.MIN_VALUE),((double)Integer.MIN_VALUE),((long)Integer.MIN_VALUE),((int)Integer.MIN_VALUE));
	sResult = doubleprint(((double) Long.MAX_VALUE),sResult,((float)Long.MAX_VALUE),((double)Long.MAX_VALUE),((long)Long.MAX_VALUE),((int)Integer.MAX_VALUE));
	sResult = doubleprint(((double) Long.MAX_VALUE)+1,sResult,((float)Long.MAX_VALUE)+1,((double)Long.MAX_VALUE)+1,((long)Long.MAX_VALUE),((int)Integer.MAX_VALUE));
	sResult = doubleprint(((double) Long.MIN_VALUE),sResult,((float)Long.MIN_VALUE),((double)Long.MIN_VALUE),((long)Long.MIN_VALUE),((int)Integer.MIN_VALUE));
	sResult = doubleprint(1e25D,sResult,1e25F,1e25D,((long)Long.MAX_VALUE),((int)Integer.MAX_VALUE));
	sResult = doubleprint(Double.NaN, sResult, Float.NaN, Double.NaN, 0, 0);
	sResult = doubleprint(Double.POSITIVE_INFINITY, sResult, Float.POSITIVE_INFINITY,Double.POSITIVE_INFINITY, 
			     Long.MAX_VALUE, Integer.MAX_VALUE);
	sResult = doubleprint(Double.NEGATIVE_INFINITY, sResult, 
			       Float.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Long.MIN_VALUE, Integer.MIN_VALUE);
	sResult = doubleprint(doublepzero, sResult, floatpzero, doublepzero, 0, 0);
	sResult = doubleprint(doublenzero, sResult, floatnzero, doublenzero, 0, 0);
        if (m_bDoubleConvert){
	  TRACE("Double conversion succeeded");
	  if (m_bApplet) 
	    sResult += "Double conversion succeeded\n";}
	else{
	  if (m_bApplet) sResult += "Double conversion failed\n";
	  m_bTestResult = false;}
      }
    if ( (m_nResPage == K_FLOATCOMP_PAGE) || m_bApplet)
      {	// comparisons 
	if (m_bApplet){
	  sResult += "float comparisons\n";
	  sResult += "val1 \t val2 \t == \t < \t > \t >= \t <= \t !=\n";}
	sResult = compare(Float.NaN, Float.NaN, sResult, 
	  false, false, false, false, false, true);
	sResult = compare(Float.NaN,Float.POSITIVE_INFINITY, sResult, 
	  false, false, false, false, false, true);
	sResult = compare(Float.NaN,floatpzero, sResult, 
	  false, false, false, false, false, true);
	sResult = compare(Float.NEGATIVE_INFINITY,Float.POSITIVE_INFINITY, sResult, 
	  false, true, false, false, true, true);
	sResult = compare(Float.NEGATIVE_INFINITY,floatpzero, sResult, 
	  false, true, false, false, true, true);
	sResult = compare(floatnzero,floatpzero, sResult, 
	  true, false, false, true, true, false);
        if (m_bFloatConvert){
	  TRACE("Float comparison succeeded");
	  if (m_bApplet) 
	    sResult += "Float comparison succeeded\n";}
	else{
	  if (m_bApplet) sResult += "Float comparison failed\n";
	  m_bTestResult = false;}
      }
    if ( (m_nResPage == K_DOUBLECOMP_PAGE) || m_bApplet)
      {
	if (m_bApplet){
	  sResult += "double comparisons\n";
	  sResult += "val1 \t val2 \t == \t < \t > \t >= \t <= \t !=\n";}
	sResult = compare(Double.NaN,Double.NaN, sResult, 
	  false, false, false, false, false, true);
	sResult = compare(Double.NaN,Double.POSITIVE_INFINITY, sResult, 
	  false, false, false, false, false, true);
	sResult = compare(Double.NaN,doublepzero, sResult, 
	  false, false, false, false, false, true);
	sResult = compare(Double.NEGATIVE_INFINITY,Double.POSITIVE_INFINITY, sResult, 
	  false, true, false, false, true, true);
	sResult = compare(Double.NEGATIVE_INFINITY,doublepzero, sResult, 
	  false, true, false, false, true, true);
	sResult = compare(doublenzero,doublepzero, sResult, 
	  true, false, false, true, true, false);
        if (m_bDoubleConvert){
	  TRACE("Double comparison succeeded");
	  if (m_bApplet) 
	    sResult += "Double comparison succeeded\n";}
	else{
	  if (m_bApplet) sResult += "Double comparison failed\n";
	  m_bTestResult = false;}
      }
     
    sResult += "\n";

    return sResult;
  }

  protected String WriteSpecificTableHeader(String sHeader)
  { 
    if ( (m_nResPage == K_DOUBLECONV_PAGE) || (m_nResPage == K_FLOATCONV_PAGE))
      {
        sHeader += "<COL><COLGROUP><COL><COLGROUP><COL><COL><COL><COL><COL><COL>\n";
        sHeader += "<THEAD>\n";
        sHeader += "<TR><TH><TH ROWSPAN=3>Date<TH COLSPAN=1>Parameters<TH COLSPAN=6>Results\n";
        sHeader += "<TR><TH><TH ROWSPAN=2>Identifier<TH ROWSPAN=2>Origin<TH COLSPAN=4>Conversion<TH ROWSPAN=2>Status\n";
        sHeader += "<TR><TH><TH>float<TH>double<TH>int<TH>long\n";
      }
    else
      {
        sHeader += "<COL><COLGROUP><COL><COLGROUP><COL><COL><COL><COL><COL><COL><COL><COL>\n";
        sHeader += "<THEAD>\n";
        sHeader += "<TR><TH><TH ROWSPAN=3>Date<TH COLSPAN=1>Parameters<TH COLSPAN=8>Results\n";
        sHeader += "<TR><TH><TH ROWSPAN=2>Identifier<TH ROWSPAN=2>Operands<TH COLSPAN=6>operators<TH ROWSPAN=2>Status\n";
        sHeader += "<TR><TH><TH>==<TH>&lt;<TH>&gt;<TH>&gt;=<TH>&lt;=<TH>!=\n";
      }
    return sHeader;
  }

  void WriteMainResultFile(String sFileName)
  {
      FileOutputStream fOutStream;    	// HTML file
      PrintStream      fResult; 	// data stream               
      sFileName = "../../html/res/"+sFileName;
      // test if the directory in filename exists
      if (!isValidPath(sFileName))
        {
          TRACE("Not a valid path");
	  return;
	}
      try 
        { fOutStream = new FileOutputStream(sFileName); }
      catch ( IOException e )
        { return ;}

      // link data stream and the file 
      fResult = new PrintStream(fOutStream);
    
      fResult.print("<head>\n<TITLE>\nOSF's Java Test Suite Results\n</TITLE>\n</head>\n<body>\n<P>\n<h1>float and double conversion and comparison</H1>\nlink to comments about <A HREF=\"../JavaValid.html#floattest\">floattest</A>\n<hr>\nResults are dispatched into four tables :\n<UL>\n<LI><A HREF=\"floattest1.html\"> Float conversions</A>.\n<LI><A HREF=\"floattest2.html\"> Double conversions</A>.\n<LI><A HREF=\"floattest3.html\"> Float Comparisons</A>\n<LI><A HREF=\"floattest4.html\"> Double Comparisons</A>\n</UL>\n</body>\n</html>");

      // flushes the stream
      try { 
	  fResult.flush();
	  fResult.close();
	  fOutStream.close();
 	}
      catch(IOException e ) {/* nothing to do */}
      fResult=null;  
}
}



