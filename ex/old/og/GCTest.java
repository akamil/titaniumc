/*
 * Copyright (c) 1995, 1996  
 * Open Software Foundation, Inc. 
 *  
 * OSF DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
 * WITH RESPECT TO THIS SOFTWARE INCLUDING, WITHOUT LIMITATION, 
 * ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL OSF BE LIABLE FOR ANY 
 * SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES RESULTING FROM 
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN CONTRACT, TORT 
 * INCLUDING NEGLIGENCE, OR OTHER LEGAL THEORY ARISING OUT OF 
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS 
 * SOFTWARE. 
 */

/*
 * This file is part of an OSF RI software distribution. This software is 
 * distributed under the terms of a free license for non-commercial use. 
 * 
 * You should have received a copy of those terms along with this file.
 * Please see file COPYRIGHT.
 * 
 * If not, please write to: OSF RI, 2 Av. de Vignate, 38610 Gieres, France
 */

import java.lang.*;
import java.util.Vector;

class GCTest extends GenericTestClass {
    static GCTestMemAlloc  memoryThread;
    static GCTestAlarm     alarm;
    int size ;
    int timeout;
    String m_sIdentifier;
    static String m_sExceptionCaught;
    static int m_nTotalBlocs;
    static int m_nOutMemExceptions;
    static long m_nFreeMemAtBegin;

  protected String WriteSpecificResults(String sResult)
  {
    if(m_bApplet)
      { 
       sResult += "Parameters :\n";
       //sResult+= "\tidentifier : "+m_sIdentifier+"\n";
	sResult += "\tSize of blocs : "+size+"bytes\n\t timeout : "+timeout+"s\n";;;
    	sResult+= " Results :\n    --> Free memory at begin : "+m_nFreeMemAtBegin+ "bytes";
    	sResult+= "\n    --> "+m_nTotalBlocs+" blocs allocated";
    	sResult+= "\n    --> "+m_nOutMemExceptions+" memory exceptions";
    	sResult+= "\n    --> Test stopped due to : "+m_sExceptionCaught+"\n";
        return sResult;
      }

    sResult += "<TD>"+	  m_sIdentifier;
    sResult += "<TD>"+    size + " bytes";
    sResult += "<TD>"+    timeout + " s";  
    sResult += "<TD>"+    m_nFreeMemAtBegin + " bytes";
    sResult += "<TD>"+    m_nTotalBlocs + " blocs";
    sResult += "<TD>"+    m_nOutMemExceptions;
    sResult += "<TD>"+    m_sExceptionCaught+"\n";
    return sResult;
  }

  protected String WriteSpecificTableHeader(String sHeader)
  { 
    sHeader += "<COL><COLGROUP><COL><COL><COL><COLGROUP><COL><COL><COL><COL>\n";
    sHeader += "<THEAD>\n";
    sHeader += "<TR><TH><TH ROWSPAN=2>Date<TH COLSPAN=3>Parameters<TH COLSPAN=4>Results\n";
    sHeader += "<TR><TH><TH>Identifier<TH>Size of blocs<TH>timeout<TH>Free Mem (start)<TH>total allocated"+
	       "<TH>Memory exceptions<TH>Exception stop\n";
    return sHeader;
  }

  public void setApplet()
  {
    Parameter pParam;
    
    super.setApplet();
    lParameters = new Vector();
 
    init();
    if((m_pTest != null) && (m_pTest.sTestArgv != null))
      parse(m_pTest.sTestArgv);

    //pParam = new Parameter("identifier", "-ident", m_sIdentifier);
    //lParameters.addElement( pParam);
    pParam = new Parameter("blocs size", "-size", Integer.toString(size));
    lParameters.addElement( pParam);
    pParam = new Parameter("timout", "-timeout", Integer.toString(timeout));
    lParameters.addElement( pParam);
  }  

  void init(){
    m_sIdentifier = "Java";
    size = 1000000;
    timeout=30;
    m_nTotalBlocs = 0;
    m_nOutMemExceptions = 0;
    m_nFreeMemAtBegin = 0;
    m_sExceptionCaught = "none";
   }

  protected void parse(String argv [])
  {
    init();
    if (argv == null)
        return;

    int i;
    for (i=0; i<argv.length; i++ ) 
      {
        if (argv[i].equals("-ident")) 
	 {  if (++i <argv.length) 
	  	m_sIdentifier = argv[i];
	    continue;
	 }
        if (argv[i].equals("-size")) 
	 {  if (++i <argv.length) 
	  	size = Integer.valueOf(argv[i]).intValue();
	    continue;
	 }
        if (argv[i].equals("-timeout")) 
	   if (++i <argv.length) 
	  	timeout = Integer.valueOf(argv[i]).intValue();
	 
      }     
  } 
  public static void UpdateResults(String Exception, int total, int exceptions)
  {
    m_sExceptionCaught = Exception;
    m_nTotalBlocs = total;
    m_nOutMemExceptions = exceptions;
  }
 

  protected boolean execute (String argv[]) 
    {
        parse (argv);
        m_nFreeMemAtBegin = Runtime.getRuntime().freeMemory();
        TRACE("Starting off with "+m_nFreeMemAtBegin+" bytes of memory.");
        TRACE("   Allocating chunks of " + size + " bytes of memory during " + timeout + " seconds.");

        memoryThread = new GCTestMemAlloc(size);
	memoryThread.start();
        alarm = new GCTestAlarm(timeout * 1000);
	alarm.start();

        try {
	  //GenericTestClass.TRACE("Waiting alarm.");
	    alarm.safeJoin();
            //GenericTestClass.TRACE("End wait.");
            memoryThread.stop(new GCTestTimeoutExc());
            //GenericTestClass.TRACE("Waiting memory thread.");
            memoryThread.safeJoin();
            //GenericTestClass.TRACE("End wait.");
        } catch (InterruptedException e) {
            GenericTestClass.TRACE("Memory allocator thread already stopped.");
        }
        alarm = new GCTestAlarm(1000);
	alarm.start();
        try {
	  //GenericTestClass.TRACE("Waiting alarm.");
	  alarm.safeJoin();
	  //GenericTestClass.TRACE("End wait.");
        } catch (InterruptedException e) {
            GenericTestClass.TRACE("Memory allocator thread already stopped.");
        }
	return WriteHTMLResults("GCTest.html", "Garbage Collector Test");
    }

}  /* GCTest */

class GCTestTimeoutExc extends InterruptedException {

}  /* GCTestTimeoutExc */

class GCTestMemAlloc extends Thread {

  public void safeJoin() throws java.lang.InterruptedException {
    // Netscape hack : join hangs up
    while (isAlive()) {
      join(1000);
    }
  }

    int  total = 0;
    int  exceptions = 0;
    int  chunksize;
    byte memoryChunk[];

    GCTestMemAlloc(int size) {
        chunksize = size;
    } 
 
    public void run() {
        while (true) {
            try {
                memoryChunk = new byte[chunksize];
                total++;
                yield();
            } catch (OutOfMemoryError e) {
                GCTest.displayArea("Exception caught: " + e.toString()); 
                exceptions++;
            } catch (Exception e) {
                GCTest.TRACE("Exception caught: " + e.toString());
                GCTest.TRACE("Allocated " + total + " " + chunksize + " byte chunks");
                GCTest.TRACE("Caught " + exceptions + " OutOfMemory exceptions");
                GCTest.UpdateResults(e.toString(), total, exceptions);
                break;
            }
        }
    }

}  /* GCTestMemAlloc */

class GCTestAlarm extends Thread {

  public void safeJoin() throws java.lang.InterruptedException {
    // Netscape hack : join hangs up
    while (isAlive()) {
      join(1000);
    }
  }

    int timeout;

    public GCTestAlarm(int t) {
        timeout = t;
	//setPriority(MAX_PRIORITY-1);
    }

    public void run() {
	try {
	    sleep(timeout);
	}
	catch (InterruptedException e) {
	    GCTest.displayArea("GCTestAlarm thread interrupted.");
	}
	//GenericTestClass.TRACE("alarm went off.");
    }

}  /* GCTestAlarm */
