/*
 * Copyright (c) 1995, 1996  
 * Open Software Foundation, Inc. 
 *  
 * OSF DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
 * WITH RESPECT TO THIS SOFTWARE INCLUDING, WITHOUT LIMITATION, 
 * ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL OSF BE LIABLE FOR ANY 
 * SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES RESULTING FROM 
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN CONTRACT, TORT 
 * INCLUDING NEGLIGENCE, OR OTHER LEGAL THEORY ARISING OUT OF 
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS 
 * SOFTWARE. 
 */

/*
 * This file is part of an OSF RI software distribution. This software is 
 * distributed under the terms of a free license for non-commercial use. 
 * 
 * You should have received a copy of those terms along with this file.
 * Please see file COPYRIGHT.
 * 
 * If not, please write to: OSF RI, 2 Av. de Vignate, 38610 Gieres, France
 */

import java.util.*;

/* ************************************* *
 * Test write access in arrays of arrays *
 * ************************************* */

class ArrayWrite extends validtest
{
  protected boolean execute (String argv[]) 
  {
    byte count = 0 ;
    byte verif = 0;
    byte[] tbyte ; 
    byte[][] ttbyte ;
    short[] tshort ;
    short[][] ttshort ;
    char[] tchar ; 
    char[][] ttchar ;
    int[] tint ; 
    int[][] ttint ;
    long[] tlong ; 
    long[][] ttlong ;
    float[] tfloat ;
    float[][] ttfloat ;
    double[] tdouble ; 
    double[][] ttdouble ;
    int size = 4;

  // Arrays definition
    ttbyte = new byte[size][size] ;
    ttshort = new short[size][size] ;
    ttchar = new char[size][size] ;
    ttint = new int[size][size] ;
    ttlong = new long[size][size] ;
    ttfloat = new float[size][size] ;
    ttdouble = new double[size][size] ;

    parse(argv);
      
  // Arrays initialization
    for ( int i = 0 ; i < size ; i++ ) {
      tbyte = new byte[size] ;
      for ( int j = 0 ; j < size ; j++ ) {
        tbyte[j] = count++ ;
      }
      ttbyte[i] = tbyte ;
    }

    for ( int i = 0 ; i < size ; i++ ) {
      tshort = new short[size] ;
      for ( int j = 0 ; j < size ; j++ ) {
        tshort[j] = count++ ;
      }
      ttshort[i] = tshort ;
    }

    for ( int i = 0 ; i < size ; i++ ) {
      tchar = new char[size] ;
      for ( int j = 0 ; j < size ; j++ ) {
        tchar[j] = (char)count++ ;
      }
      ttchar[i] = tchar ;
    }

    for ( int i = 0 ; i < size ; i++ ) {
      tint = new int[size] ;
      for ( int j = 0 ; j < size ; j++ ) {
        tint[j] = count++ ;
      }
      ttint[i] = tint ;
    }

    for ( int i = 0 ; i < size ; i++ ) {
      tlong = new long[size] ;
      for ( int j = 0 ; j < size ; j++ ) {
        tlong[j] = count++ ;
      }
      ttlong[i] = tlong ;
    }

    for ( int i = 0 ; i < size ; i++ ) {
      tfloat = new float[size] ;
      for ( int j = 0 ; j < size ; j++ ) {
        tfloat[j] = count++ ;
      }
      ttfloat[i] = tfloat ;
    }

    for ( int i = 0 ; i < size ; i++ ) {
      tdouble = new double[size] ;
      for ( int j = 0 ; j < size ; j++ ) {
        tdouble[j] = count++ ;
      }
      ttdouble[i] = tdouble ;
    }

  // Arrays checking
    for ( int i = 0 ; i < size ; i++ ) {
      for ( int j = 0 ; j < size ; j++ ) {
        test_result = test_result & (ttbyte[i][j]==verif);
        verif++;
      }
    }

    for ( int i = 0 ; i < size ; i++ ) {
      for ( int j = 0 ; j < size ; j++ ) {
        test_result = test_result & (ttshort[i][j]==verif);
        verif++;
      }
    }

    for ( int i = 0 ; i < size ; i++ ) {
      for ( int j = 0 ; j < size ; j++ ) {
        test_result = test_result & (ttchar[i][j]==verif);
        verif++;
      }
    }

    for ( int i = 0 ; i < size ; i++ ) {
      for ( int j = 0 ; j < size ; j++ ) {
        test_result = test_result & (ttint[i][j]==verif);
        verif++;
      }
    }

    for ( int i = 0 ; i < size ; i++ ) {
      for ( int j = 0 ; j < size ; j++ ) {
        test_result = test_result & (ttlong[i][j]==verif);
        verif++;
      }
    }

    for ( int i = 0 ; i < size ; i++ ) {
      for ( int j = 0 ; j < size ; j++ ) {
        test_result = test_result & (ttfloat[i][j]==verif);
        verif++;
      }
    }

    for ( int i = 0 ; i < size ; i++ ) {
      for ( int j = 0 ; j < size ; j++ ) {
        test_result = test_result & (ttdouble[i][j]==verif);
        verif++;
      }
    }

    WriteHTMLResults("ArrayWrite.html", "Write access in arrays of arrays");
    return test_result;
  }
}
