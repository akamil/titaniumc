/*
 * Copyright (c) 1995, 1996  
 * Open Software Foundation, Inc. 
 *  
 * OSF DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
 * WITH RESPECT TO THIS SOFTWARE INCLUDING, WITHOUT LIMITATION, 
 * ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL OSF BE LIABLE FOR ANY 
 * SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES RESULTING FROM 
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN CONTRACT, TORT 
 * INCLUDING NEGLIGENCE, OR OTHER LEGAL THEORY ARISING OUT OF 
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS 
 * SOFTWARE. 
 */

/*
 * This file is part of an OSF RI software distribution. This software is 
 * distributed under the terms of a free license for non-commercial use. 
 * 
 * You should have received a copy of those terms along with this file.
 * Please see file COPYRIGHT.
 * 
 * If not, please write to: OSF RI, 2 Av. de Vignate, 38610 Gieres, France
 */

import java.util.*;

/* ************************************ *
 * Test read access in arrays of arrays *
 * ************************************ */

class ArrayRead extends validtest
{

  protected boolean execute(String argv[]) 
  {
    byte[][] ttbyte ;
    short[][] ttshort ;
    char[][] ttchar ;
    int[][] ttint ;
    long[][] ttlong ;
    float[][] ttfloat ;
    double[][] ttdouble ;
    int size = 4;

    ttbyte = new byte[size][size] ;	// by default ttbyte is initialized with 0.
    ttshort = new short[size][size] ;	// by default ttshort is initialized with 0.
    ttchar = new char[size][size] ;	// by default ttchar is initialized with 0.
    ttint = new int[size][size] ;	// by default ttint is initialized with 0.
    ttlong = new long[size][size] ;	// by default ttlong is initialized with 0.
    ttfloat = new float[size][size] ;	// by default ttfloat is initialized with 0.0
    ttdouble = new double[size][size] ;	// by default ttdouble is initialized with 0.0

    parse(argv);
      
    for ( int i = 0 ; i < size ; i++ ) {
      for ( int j = 0 ; j < size ; j++ ) {
        test_result = test_result & (ttbyte[i][j]==0);
      }
    }

    for ( int i = 0 ; i < size ; i++ ) {
      for ( int j = 0 ; j < size ; j++ ) {
        test_result = test_result & (ttshort[i][j]==0);
      }
    }

    for ( int i = 0 ; i < size ; i++ ) {
      for ( int j = 0 ; j < size ; j++ ) {
        test_result = test_result & (ttchar[i][j]==0);
      }
    }

    for ( int i = 0 ; i < size ; i++ ) {
      for ( int j = 0 ; j < size ; j++ ) {
        test_result = test_result & (ttint[i][j]==0);
      }
    }

    for ( int i = 0 ; i < size ; i++ ) {
      for ( int j = 0 ; j < size ; j++ ) {
        test_result = test_result & (ttlong[i][j]==0);
      }
    }

    for ( int i = 0 ; i < size ; i++ ) {
      for ( int j = 0 ; j < size ; j++ ) {
        test_result = test_result & (ttfloat[i][j]==0.0);
      }
    }

    for ( int i = 0 ; i < size ; i++ ) {
      for ( int j = 0 ; j < size ; j++ ) {
        test_result = test_result & (ttdouble[i][j]==0.0);
      }
    }

    WriteHTMLResults("ArrayRead.html", "Read access in arrays of arrays");
    return test_result;
  }
}
