/*
 * Copyright (c) 1995, 1996  
 * Open Software Foundation, Inc. 
 *  
 * OSF DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
 * WITH RESPECT TO THIS SOFTWARE INCLUDING, WITHOUT LIMITATION, 
 * ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL OSF BE LIABLE FOR ANY 
 * SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES RESULTING FROM 
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN CONTRACT, TORT 
 * INCLUDING NEGLIGENCE, OR OTHER LEGAL THEORY ARISING OUT OF 
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS 
 * SOFTWARE. 
 */

/*
 * This file is part of an OSF RI software distribution. This software is 
 * distributed under the terms of a free license for non-commercial use. 
 * 
 * You should have received a copy of those terms along with this file.
 * Please see file COPYRIGHT.
 * 
 * If not, please write to: OSF RI, 2 Av. de Vignate, 38610 Gieres, France
 */

import java.util.*;

/* ***************** *
 * INTERFACE testing *
 * ***************** */

class Interfaces extends validtest {

  private int test1(Inter1 interf,int i) {
    return (interf.meth2(i));
  }

  private int test2(Inter2 interf,int i) {
    return (interf.meth2(i));
  }

  private int test3(int i) {
    return (meth2(i));
  }

  private int meth2(int i) {
    return i-1;
  }

  protected boolean execute(String argv[]) 
  {
    parse (argv);
    Inter1 inter1,inter1_bis,inter;
    Inter2 inter2;
    Inter1 inter1_3_a;
    Inter3 inter1_3_b;
    Inter4 inter4,inter4_bis_a,inter4_bis_b,inter4_bis_c;
    Vector v1=new Vector();
    int i1=6;
    Implem1 implem1=new Implem1();
    Implem1bis implem1_bis=new Implem1bis();
    float f1=11.0F;

    inter1 = new Implem1();
    inter1_bis = new Implem1bis();
    inter2 = new Implem2();

    // Check that two interfaces with methods having the same name
    // and with different implementation can be defined.
    test_result = test_result & (inter1.meth1(v1)==1);
    test_result = test_result & (inter1.meth2(i1)==7);
    test_result = test_result & (test1(inter1,10)==11);

    test_result = test_result & (inter2.meth1(v1)==2);
    test_result = test_result & (inter2.meth2(i1)==8);
    test_result = test_result & (test2(inter2,10)==12);


    // Check that an interface can have several implementations.
    test_result = test_result & (inter1_bis.meth1(v1)==100);
    test_result = test_result & (inter1_bis.meth2(i1)==106);
    test_result = test_result & (test1(inter1_bis,10)==110);

    // Check that a method can be defined in the current class
    // with the same name as a method defined in an interface
    test_result = test_result & (meth2(20)==19);
    test_result = test_result & (test3(30)==29);

    // Check that an object of type interface can de declared and
    // assigned with an object of the interface implementation.
    inter = implem1;
    test_result = test_result & (test1(inter,50)==51);
    inter = implem1_bis;
    test_result = test_result & (test1(inter,50)==150);


    // Check when two interfaces declaring a method (i.e. meth1) with the
    // same name and the same parameters type have a common implementation.
    inter1_3_a = new Implem1_3();
    inter1_3_b = new Implem1_3();

    test_result = test_result & (inter1_3_a.meth1(v1)==1313);
    test_result = test_result & (inter1_3_b.meth1(v1)==1313);

    // Check overloading between two methods (i.e. meth2) having the same
    // name and implemented in the same implementation class.
    test_result = test_result & (inter1_3_a.meth2(i1)==1319);
    test_result = test_result & (inter1_3_b.meth2(i1)==19.14F);
    test_result = test_result & (inter1_3_b.meth2(f1)==24.14F);

    // Check when two interfaces have a common implementation with a method
    // (i.e. meth3) defined only in one of the two interfaces.
    test_result = test_result & (inter1_3_b.meth3()==3131);

    // Check extends clause on interface :
    inter4 = new Implem4();
    test_result = test_result & (inter4.meth1(v1)==44);
    test_result = test_result & (inter4.meth2(i1)==50);
    test_result = test_result & (inter4.meth3(f1,i1)==17.44F);

    // Check when interface methods are implemented in separate classes
    // (there is an "extends" link between these classes).
    inter4_bis_c = new Implem4bis_c();
    test_result = test_result & (inter4_bis_c.meth1(v1)==9876);
    test_result = test_result & (inter4_bis_c.meth2(i1)==9882);
    test_result = test_result & (inter4_bis_c.meth3(f1,i1)==17.9876F);

    WriteHTMLResults("Interfaces.html", "Interfaces implementation test");
    return test_result;
  }
}
