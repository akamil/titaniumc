/*
 * Copyright (c) 1995, 1996  
 * Open Software Foundation, Inc. 
 *  
 * OSF DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
 * WITH RESPECT TO THIS SOFTWARE INCLUDING, WITHOUT LIMITATION, 
 * ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL OSF BE LIABLE FOR ANY 
 * SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES RESULTING FROM 
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN CONTRACT, TORT 
 * INCLUDING NEGLIGENCE, OR OTHER LEGAL THEORY ARISING OUT OF 
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS 
 * SOFTWARE. 
 */

/*
 * This file is part of an OSF RI software distribution. This software is 
 * distributed under the terms of a free license for non-commercial use. 
 * 
 * You should have received a copy of those terms along with this file.
 * Please see file COPYRIGHT.
 * 
 * If not, please write to: OSF RI, 2 Av. de Vignate, 38610 Gieres, France
 */


/* ************************************************** *
 * This test checks that a large case with not        *
 *   consecutive values executes properly             *
 * ************************************************** */

class LargeCase extends validtest {
private static int LastValue = 999;
private static int LastPrime = 10007;

    protected boolean execute (String argv[]) 
    {
	parse (argv);
	for (int i=0; i<5; i++) {
	    test_result = test_result & SearchValue(random(LastValue));
	}
        LastPrime = 10007;
	for (int i=0; i<5; i++) {
	    test_result = test_result & SearchNextPrime(random(LastPrime));
	}

	WriteHTMLResults("LargeCase.html", "'case' execution test");
	return test_result;
    }

    private int random(int Max) {

	return (int)(Max*Math.random());
    }

    private boolean SearchValue(int Val) {

	switch(Val) {
	  case 0:	case 1:		case 2:		case 3:		case 4:
	  case 5:	case 6:		case 7:		case 8:		case 9:
	  case 10:	case 11:	case 12:	case 13:	case 14:
	  case 15:	case 16:	case 17:	case 18:	case 19:
	  case 20:	case 21:	case 22:	case 23:	case 24:
	  case 25:	case 26:	case 27:	case 28:	case 29:
	  case 30:	case 31:	case 32:	case 33:	case 34:
	  case 35:	case 36:	case 37:	case 38:	case 39:
	  case 40:	case 41:	case 42:	case 43:	case 44:
	  case 45:	case 46:	case 47:	case 48:	case 49:
	  case 50:	case 51:	case 52:	case 53:	case 54:
	  case 55:	case 56:	case 57:	case 58:	case 59:
	  case 60:	case 61:	case 62:	case 63:	case 64:
	  case 65:	case 66:	case 67:	case 68:	case 69:
	  case 70:	case 71:	case 72:	case 73:	case 74:
	  case 75:	case 76:	case 77:	case 78:	case 79:
	  case 80:	case 81:	case 82:	case 83:	case 84:
	  case 85:	case 86:	case 87:	case 88:	case 89:
	  case 90:	case 91:	case 92:	case 93:	case 94:
	  case 95:	case 96:	case 97:	case 98:	case 99:
	  case 100:	case 101:	case 102:	case 103:	case 104:
	  case 105:	case 106:	case 107:	case 108:	case 109:
	  case 110:	case 111:	case 112:	case 113:	case 114:
	  case 115:	case 116:	case 117:	case 118:	case 119:
	  case 120:	case 121:	case 122:	case 123:	case 124:
	  case 125:	case 126:	case 127:	case 128:	case 129:
	  case 130:	case 131:	case 132:	case 133:	case 134:
	  case 135:	case 136:	case 137:	case 138:	case 139:
	  case 140:	case 141:	case 142:	case 143:	case 144:
	  case 145:	case 146:	case 147:	case 148:	case 149:
	  case 150:	case 151:	case 152:	case 153:	case 154:
	  case 155:	case 156:	case 157:	case 158:	case 159:
	  case 160:	case 161:	case 162:	case 163:	case 164:
	  case 165:	case 166:	case 167:	case 168:	case 169:
	  case 170:	case 171:	case 172:	case 173:	case 174:
	  case 175:	case 176:	case 177:	case 178:	case 179:
	  case 180:	case 181:	case 182:	case 183:	case 184:
	  case 185:	case 186:	case 187:	case 188:	case 189:
	  case 190:	case 191:	case 192:	case 193:	case 194:
	  case 195:	case 196:	case 197:	case 198:	case 199:
	  case 200:	case 201:	case 202:	case 203:	case 204:
	  case 205:	case 206:	case 207:	case 208:	case 209:
	  case 210:	case 211:	case 212:	case 213:	case 214:
	  case 215:	case 216:	case 217:	case 218:	case 219:
	  case 220:	case 221:	case 222:	case 223:	case 224:
	  case 225:	case 226:	case 227:	case 228:	case 229:
	  case 230:	case 231:	case 232:	case 233:	case 234:
	  case 235:	case 236:	case 237:	case 238:	case 239:
	  case 240:	case 241:	case 242:	case 243:	case 244:
	  case 245:	case 246:	case 247:	case 248:	case 249:
	  case 250:	case 251:	case 252:	case 253:	case 254:
	  case 255:	case 256:	case 257:	case 258:	case 259:
	  case 260:	case 261:	case 262:	case 263:	case 264:
	  case 265:	case 266:	case 267:	case 268:	case 269:
	  case 270:	case 271:	case 272:	case 273:	case 274:
	  case 275:	case 276:	case 277:	case 278:	case 279:
	  case 280:	case 281:	case 282:	case 283:	case 284:
	  case 285:	case 286:	case 287:	case 288:	case 289:
	  case 290:	case 291:	case 292:	case 293:	case 294:
	  case 295:	case 296:	case 297:	case 298:	case 299:
	  case 300:	case 301:	case 302:	case 303:	case 304:
	  case 305:	case 306:	case 307:	case 308:	case 309:
	  case 310:	case 311:	case 312:	case 313:	case 314:
	  case 315:	case 316:	case 317:	case 318:	case 319:
	  case 320:	case 321:	case 322:	case 323:	case 324:
	  case 325:	case 326:	case 327:	case 328:	case 329:
	  case 330:	case 331:	case 332:	case 333:	case 334:
	  case 335:	case 336:	case 337:	case 338:	case 339:
	  case 340:	case 341:	case 342:	case 343:	case 344:
	  case 345:	case 346:	case 347:	case 348:	case 349:
	  case 350:	case 351:	case 352:	case 353:	case 354:
	  case 355:	case 356:	case 357:	case 358:	case 359:
	  case 360:	case 361:	case 362:	case 363:	case 364:
	  case 365:	case 366:	case 367:	case 368:	case 369:
	  case 370:	case 371:	case 372:	case 373:	case 374:
	  case 375:	case 376:	case 377:	case 378:	case 379:
	  case 380:	case 381:	case 382:	case 383:	case 384:
	  case 385:	case 386:	case 387:	case 388:	case 389:
	  case 390:	case 391:	case 392:	case 393:	case 394:
	  case 395:	case 396:	case 397:	case 398:	case 399:
	  case 400:	case 401:	case 402:	case 403:	case 404:
	  case 405:	case 406:	case 407:	case 408:	case 409:
	  case 410:	case 411:	case 412:	case 413:	case 414:
	  case 415:	case 416:	case 417:	case 418:	case 419:
	  case 420:	case 421:	case 422:	case 423:	case 424:
	  case 425:	case 426:	case 427:	case 428:	case 429:
	  case 430:	case 431:	case 432:	case 433:	case 434:
	  case 435:	case 436:	case 437:	case 438:	case 439:
	  case 440:	case 441:	case 442:	case 443:	case 444:
	  case 445:	case 446:	case 447:	case 448:	case 449:
	  case 450:	case 451:	case 452:	case 453:	case 454:
	  case 455:	case 456:	case 457:	case 458:	case 459:
	  case 460:	case 461:	case 462:	case 463:	case 464:
	  case 465:	case 466:	case 467:	case 468:	case 469:
	  case 470:	case 471:	case 472:	case 473:	case 474:
	  case 475:	case 476:	case 477:	case 478:	case 479:
	  case 480:	case 481:	case 482:	case 483:	case 484:
	  case 485:	case 486:	case 487:	case 488:	case 489:
	  case 490:	case 491:	case 492:	case 493:	case 494:
	  case 495:	case 496:	case 497:	case 498:	case 499:
	  case 500:	case 501:	case 502:	case 503:	case 504:
	  case 505:	case 506:	case 507:	case 508:	case 509:
	  case 510:	case 511:	case 512:	case 513:	case 514:
	  case 515:	case 516:	case 517:	case 518:	case 519:
	  case 520:	case 521:	case 522:	case 523:	case 524:
	  case 525:	case 526:	case 527:	case 528:	case 529:
	  case 530:	case 531:	case 532:	case 533:	case 534:
	  case 535:	case 536:	case 537:	case 538:	case 539:
	  case 540:	case 541:	case 542:	case 543:	case 544:
	  case 545:	case 546:	case 547:	case 548:	case 549:
	  case 550:	case 551:	case 552:	case 553:	case 554:
	  case 555:	case 556:	case 557:	case 558:	case 559:
	  case 560:	case 561:	case 562:	case 563:	case 564:
	  case 565:	case 566:	case 567:	case 568:	case 569:
	  case 570:	case 571:	case 572:	case 573:	case 574:
	  case 575:	case 576:	case 577:	case 578:	case 579:
	  case 580:	case 581:	case 582:	case 583:	case 584:
	  case 585:	case 586:	case 587:	case 588:	case 589:
	  case 590:	case 591:	case 592:	case 593:	case 594:
	  case 595:	case 596:	case 597:	case 598:	case 599:
	  case 600:	case 601:	case 602:	case 603:	case 604:
	  case 605:	case 606:	case 607:	case 608:	case 609:
	  case 610:	case 611:	case 612:	case 613:	case 614:
	  case 615:	case 616:	case 617:	case 618:	case 619:
	  case 620:	case 621:	case 622:	case 623:	case 624:
	  case 625:	case 626:	case 627:	case 628:	case 629:
	  case 630:	case 631:	case 632:	case 633:	case 634:
	  case 635:	case 636:	case 637:	case 638:	case 639:
	  case 640:	case 641:	case 642:	case 643:	case 644:
	  case 645:	case 646:	case 647:	case 648:	case 649:
	  case 650:	case 651:	case 652:	case 653:	case 654:
	  case 655:	case 656:	case 657:	case 658:	case 659:
	  case 660:	case 661:	case 662:	case 663:	case 664:
	  case 665:	case 666:	case 667:	case 668:	case 669:
	  case 670:	case 671:	case 672:	case 673:	case 674:
	  case 675:	case 676:	case 677:	case 678:	case 679:
	  case 680:	case 681:	case 682:	case 683:	case 684:
	  case 685:	case 686:	case 687:	case 688:	case 689:
	  case 690:	case 691:	case 692:	case 693:	case 694:
	  case 695:	case 696:	case 697:	case 698:	case 699:
	  case 700:	case 701:	case 702:	case 703:	case 704:
	  case 705:	case 706:	case 707:	case 708:	case 709:
	  case 710:	case 711:	case 712:	case 713:	case 714:
	  case 715:	case 716:	case 717:	case 718:	case 719:
	  case 720:	case 721:	case 722:	case 723:	case 724:
	  case 725:	case 726:	case 727:	case 728:	case 729:
	  case 730:	case 731:	case 732:	case 733:	case 734:
	  case 735:	case 736:	case 737:	case 738:	case 739:
	  case 740:	case 741:	case 742:	case 743:	case 744:
	  case 745:	case 746:	case 747:	case 748:	case 749:
	  case 750:	case 751:	case 752:	case 753:	case 754:
	  case 755:	case 756:	case 757:	case 758:	case 759:
	  case 760:	case 761:	case 762:	case 763:	case 764:
	  case 765:	case 766:	case 767:	case 768:	case 769:
	  case 770:	case 771:	case 772:	case 773:	case 774:
	  case 775:	case 776:	case 777:	case 778:	case 779:
	  case 780:	case 781:	case 782:	case 783:	case 784:
	  case 785:	case 786:	case 787:	case 788:	case 789:
	  case 790:	case 791:	case 792:	case 793:	case 794:
	  case 795:	case 796:	case 797:	case 798:	case 799:
	  case 800:	case 801:	case 802:	case 803:	case 804:
	  case 805:	case 806:	case 807:	case 808:	case 809:
	  case 810:	case 811:	case 812:	case 813:	case 814:
	  case 815:	case 816:	case 817:	case 818:	case 819:
	  case 820:	case 821:	case 822:	case 823:	case 824:
	  case 825:	case 826:	case 827:	case 828:	case 829:
	  case 830:	case 831:	case 832:	case 833:	case 834:
	  case 835:	case 836:	case 837:	case 838:	case 839:
	  case 840:	case 841:	case 842:	case 843:	case 844:
	  case 845:	case 846:	case 847:	case 848:	case 849:
	  case 850:	case 851:	case 852:	case 853:	case 854:
	  case 855:	case 856:	case 857:	case 858:	case 859:
	  case 860:	case 861:	case 862:	case 863:	case 864:
	  case 865:	case 866:	case 867:	case 868:	case 869:
	  case 870:	case 871:	case 872:	case 873:	case 874:
	  case 875:	case 876:	case 877:	case 878:	case 879:
	  case 880:	case 881:	case 882:	case 883:	case 884:
	  case 885:	case 886:	case 887:	case 888:	case 889:
	  case 890:	case 891:	case 892:	case 893:	case 894:
	  case 895:	case 896:	case 897:	case 898:	case 899:
	  case 900:	case 901:	case 902:	case 903:	case 904:
	  case 905:	case 906:	case 907:	case 908:	case 909:
	  case 910:	case 911:	case 912:	case 913:	case 914:
	  case 915:	case 916:	case 917:	case 918:	case 919:
	  case 920:	case 921:	case 922:	case 923:	case 924:
	  case 925:	case 926:	case 927:	case 928:	case 929:
	  case 930:	case 931:	case 932:	case 933:	case 934:
	  case 935:	case 936:	case 937:	case 938:	case 939:
	  case 940:	case 941:	case 942:	case 943:	case 944:
	  case 945:	case 946:	case 947:	case 948:	case 949:
	  case 950:	case 951:	case 952:	case 953:	case 954:
	  case 955:	case 956:	case 957:	case 958:	case 959:
	  case 960:	case 961:	case 962:	case 963:	case 964:
	  case 965:	case 966:	case 967:	case 968:	case 969:
	  case 970:	case 971:	case 972:	case 973:	case 974:
	  case 975:	case 976:	case 977:	case 978:	case 979:
	  case 980:	case 981:	case 982:	case 983:	case 984:
	  case 985:	case 986:	case 987:	case 988:	case 989:
	  case 990:	case 991:	case 992:	case 993:	case 994:
	  case 995:	case 996:	case 997:	case 998:	case 999:
	    return true;

	  default:
	    displayArea("   Error: Failed to find value "+Val);
	    return false;
	}
    }

    protected boolean SearchNextPrime(int Val) {
	for (int i=Val; i<=LastPrime; i++) {
	    switch(i) {

	      case 2:		case 3:		case 5:		case 7:		case 11:
	      case 13:		case 17:	case 19:	case 23:	case 29:
	      case 31:		case 37:	case 41:	case 43:	case 47:
	      case 53:		case 59:	case 61:	case 67:	case 71:
	      case 73:		case 79:	case 83:	case 89:	case 97:
	      case 101:		case 103:	case 107:	case 109:	case 113:
	      case 127:		case 131:	case 137:	case 139:	case 149:
	      case 151:		case 157:	case 163:	case 167:	case 173:
	      case 179:		case 181:	case 191:	case 193:	case 197:
	      case 199:		case 211:	case 223:	case 227:	case 229:
	      case 233:		case 239:	case 241:	case 251:	case 257:
	      case 263:		case 269:	case 271:	case 277:	case 281:
	      case 283:		case 293:	case 307:	case 311:	case 313:
	      case 317:		case 331:	case 337:	case 347:	case 349:
	      case 353:		case 359:	case 367:	case 373:	case 379:
	      case 383:		case 389:	case 397:	case 401:	case 409:
	      case 419:		case 421:	case 431:	case 433:	case 439:
	      case 443:		case 449:	case 457:	case 461:	case 463:
	      case 467:		case 479:	case 487:	case 491:	case 499:
	      case 503:		case 509:	case 521:	case 523:	case 541:
	      case 547:		case 557:	case 563:	case 569:	case 571:
	      case 577:		case 587:	case 593:	case 599:	case 601:
	      case 607:		case 613:	case 617:	case 619:	case 631:
	      case 641:		case 643:	case 647:	case 653:	case 659:
	      case 661:		case 673:	case 677:	case 683:	case 691:
	      case 701:		case 709:	case 719:	case 727:	case 733:
	      case 739:		case 743:	case 751:	case 757:	case 761:
	      case 769:		case 773:	case 787:	case 797:	case 809:
	      case 811:		case 821:	case 823:	case 827:	case 829:
	      case 839:		case 853:	case 857:	case 859:	case 863:
	      case 877:		case 881:	case 883:	case 887:	case 907:
	      case 911:		case 919:	case 929:	case 937:	case 941:
	      case 947:		case 953:	case 967:	case 971:	case 977:
	      case 983:		case 991:	case 997:	case 1009:	case 1013:
	      case 1019:	case 1021:	case 1031:	case 1033:	case 1039:
	      case 1049:	case 1051:	case 1061:	case 1063:	case 1069:
	      case 1087:	case 1091:	case 1093:	case 1097:	case 1103:
	      case 1109:	case 1117:	case 1123:	case 1129:	case 1151:
	      case 1153:	case 1163:	case 1171:	case 1181:	case 1187:
	      case 1193:	case 1201:	case 1213:	case 1217:	case 1223:
	      case 1229:	case 1231:	case 1237:	case 1249:	case 1259:
	      case 1277:	case 1279:	case 1283:	case 1289:	case 1291:
	      case 1297:	case 1301:	case 1303:	case 1307:	case 1319:
	      case 1321:	case 1327:	case 1361:	case 1367:	case 1373:
	      case 1381:	case 1399:	case 1409:	case 1423:	case 1427:
	      case 1429:	case 1433:	case 1439:	case 1447:	case 1451:
	      case 1453:	case 1459:	case 1471:	case 1481:	case 1483:
	      case 1487:	case 1489:	case 1493:	case 1499:	case 1511:
	      case 1523:	case 1531:	case 1543:	case 1549:	case 1553:
	      case 1559:	case 1567:	case 1571:	case 1579:	case 1583:
	      case 1597:	case 1601:	case 1607:	case 1609:	case 1613:
	      case 1619:	case 1621:	case 1627:	case 1637:	case 1657:
	      case 1663:	case 1667:	case 1669:	case 1693:	case 1697:
	      case 1699:	case 1709:	case 1721:	case 1723:	case 1733:
	      case 1741:	case 1747:	case 1753:	case 1759:	case 1777:
	      case 1783:	case 1787:	case 1789:	case 1801:	case 1811:
	      case 1823:	case 1831:	case 1847:	case 1861:	case 1867:
	      case 1871:	case 1873:	case 1877:	case 1879:	case 1889:
	      case 1901:	case 1907:	case 1913:	case 1931:	case 1933:
	      case 1949:	case 1951:	case 1973:	case 1979:	case 1987:
	      case 1993:	case 1997:	case 1999:	case 2003:	case 2011:
	      case 2017:	case 2027:	case 2029:	case 2039:	case 2053:
	      case 2063:	case 2069:	case 2081:	case 2083:	case 2087:
	      case 2089:	case 2099:	case 2111:	case 2113:	case 2129:
	      case 2131:	case 2137:	case 2141:	case 2143:	case 2153:
	      case 2161:	case 2179:	case 2203:	case 2207:	case 2213:
	      case 2221:	case 2237:	case 2239:	case 2243:	case 2251:
	      case 2267:	case 2269:	case 2273:	case 2281:	case 2287:
	      case 2293:	case 2297:	case 2309:	case 2311:	case 2333:
	      case 2339:	case 2341:	case 2347:	case 2351:	case 2357:
	      case 2371:	case 2377:	case 2381:	case 2383:	case 2389:
	      case 2393:	case 2399:	case 2411:	case 2417:	case 2423:
	      case 2437:	case 2441:	case 2447:	case 2459:	case 2467:
	      case 2473:	case 2477:	case 2503:	case 2521:	case 2531:
	      case 2539:	case 2543:	case 2549:	case 2551:	case 2557:
	      case 2579:	case 2591:	case 2593:	case 2609:	case 2617:
	      case 2621:	case 2633:	case 2647:	case 2657:	case 2659:
	      case 2663:	case 2671:	case 2677:	case 2683:	case 2687:
	      case 2689:	case 2693:	case 2699:	case 2707:	case 2711:
	      case 2713:	case 2719:	case 2729:	case 2731:	case 2741:
	      case 2749:	case 2753:	case 2767:	case 2777:	case 2789:
	      case 2791:	case 2797:	case 2801:	case 2803:	case 2819:
	      case 2833:	case 2837:	case 2843:	case 2851:	case 2857:
	      case 2861:	case 2879:	case 2887:	case 2897:	case 2903:
	      case 2909:	case 2917:	case 2927:	case 2939:	case 2953:
	      case 2957:	case 2963:	case 2969:	case 2971:	case 2999:
	      case 3001:	case 3011:	case 3019:	case 3023:	case 3037:
	      case 3041:	case 3049:	case 3061:	case 3067:	case 3079:
	      case 3083:	case 3089:	case 3109:	case 3119:	case 3121:
	      case 3137:	case 3163:	case 3167:	case 3169:	case 3181:
	      case 3187:	case 3191:	case 3203:	case 3209:	case 3217:
	      case 3221:	case 3229:	case 3251:	case 3253:	case 3257:
	      case 3259:	case 3271:	case 3299:	case 3301:	case 3307:
	      case 3313:	case 3319:	case 3323:	case 3329:	case 3331:
	      case 3343:	case 3347:	case 3359:	case 3361:	case 3371:
	      case 3373:	case 3389:	case 3391:	case 3407:	case 3413:
	      case 3433:	case 3449:	case 3457:	case 3461:	case 3463:
	      case 3467:	case 3469:	case 3491:	case 3499:	case 3511:
	      case 3517:	case 3527:	case 3529:	case 3533:	case 3539:
	      case 3541:	case 3547:	case 3557:	case 3559:	case 3571:
	      case 3581:	case 3583:	case 3593:	case 3607:	case 3613:
	      case 3617:	case 3623:	case 3631:	case 3637:	case 3643:
	      case 3659:	case 3671:	case 3673:	case 3677:	case 3691:
	      case 3697:	case 3701:	case 3709:	case 3719:	case 3727:
	      case 3733:	case 3739:	case 3761:	case 3767:	case 3769:
	      case 3779:	case 3793:	case 3797:	case 3803:	case 3821:
	      case 3823:	case 3833:	case 3847:	case 3851:	case 3853:
	      case 3863:	case 3877:	case 3881:	case 3889:	case 3907:
	      case 3911:	case 3917:	case 3919:	case 3923:	case 3929:
	      case 3931:	case 3943:	case 3947:	case 3967:	case 3989:
	      case 4001:	case 4003:	case 4007:	case 4013:	case 4019:
	      case 4021:	case 4027:	case 4049:	case 4051:	case 4057:
	      case 4073:	case 4079:	case 4091:	case 4093:	case 4099:
	      case 4111:	case 4127:	case 4129:	case 4133:	case 4139:
	      case 4153:	case 4157:	case 4159:	case 4177:	case 4201:
	      case 4211:	case 4217:	case 4219:	case 4229:	case 4231:
	      case 4241:	case 4243:	case 4253:	case 4259:	case 4261:
	      case 4271:	case 4273:	case 4283:	case 4289:	case 4297:
	      case 4327:	case 4337:	case 4339:	case 4349:	case 4357:
	      case 4363:	case 4373:	case 4391:	case 4397:	case 4409:
	      case 4421:	case 4423:	case 4441:	case 4447:	case 4451:
	      case 4457:	case 4463:	case 4481:	case 4483:	case 4493:
	      case 4507:	case 4513:	case 4517:	case 4519:	case 4523:
	      case 4547:	case 4549:	case 4561:	case 4567:	case 4583:
	      case 4591:	case 4597:	case 4603:	case 4621:	case 4637:
	      case 4639:	case 4643:	case 4649:	case 4651:	case 4657:
	      case 4663:	case 4673:	case 4679:	case 4691:	case 4703:
	      case 4721:	case 4723:	case 4729:	case 4733:	case 4751:
	      case 4759:	case 4783:	case 4787:	case 4789:	case 4793:
	      case 4799:	case 4801:	case 4813:	case 4817:	case 4831:
	      case 4861:	case 4871:	case 4877:	case 4889:	case 4903:
	      case 4909:	case 4919:	case 4931:	case 4933:	case 4937:
	      case 4943:	case 4951:	case 4957:	case 4967:	case 4969:
	      case 4973:	case 4987:	case 4993:	case 4999:	case 5003:
	      case 5009:	case 5011:	case 5021:	case 5023:	case 5039:
	      case 5051:	case 5059:	case 5077:	case 5081:	case 5087:
	      case 5099:	case 5101:	case 5107:	case 5113:	case 5119:
	      case 5147:	case 5153:	case 5167:	case 5171:	case 5179:
	      case 5189:	case 5197:	case 5209:	case 5227:	case 5231:
	      case 5233:	case 5237:	case 5261:	case 5273:	case 5279:
	      case 5281:	case 5297:	case 5303:	case 5309:	case 5323:
	      case 5333:	case 5347:	case 5351:	case 5381:	case 5387:
	      case 5393:	case 5399:	case 5407:	case 5413:	case 5417:
	      case 5419:	case 5431:	case 5437:	case 5441:	case 5443:
	      case 5449:	case 5471:	case 5477:	case 5479:	case 5483:
	      case 5501:	case 5503:	case 5507:	case 5519:	case 5521:
	      case 5527:	case 5531:	case 5557:	case 5563:	case 5569:
	      case 5573:	case 5581:	case 5591:	case 5623:	case 5639:
	      case 5641:	case 5647:	case 5651:	case 5653:	case 5657:
	      case 5659:	case 5669:	case 5683:	case 5689:	case 5693:
	      case 5701:	case 5711:	case 5717:	case 5737:	case 5741:
	      case 5743:	case 5749:	case 5779:	case 5783:	case 5791:
	      case 5801:	case 5807:	case 5813:	case 5821:	case 5827:
	      case 5839:	case 5843:	case 5849:	case 5851:	case 5857:
	      case 5861:	case 5867:	case 5869:	case 5879:	case 5881:
	      case 5897:	case 5903:	case 5923:	case 5927:	case 5939:
	      case 5953:	case 5981:	case 5987:	case 6007:	case 6011:
	      case 6029:	case 6037:	case 6043:	case 6047:	case 6053:
	      case 6067:	case 6073:	case 6079:	case 6089:	case 6091:
	      case 6101:	case 6113:	case 6121:	case 6131:	case 6133:
	      case 6143:	case 6151:	case 6163:	case 6173:	case 6197:
	      case 6199:	case 6203:	case 6211:	case 6217:	case 6221:
	      case 6229:	case 6247:	case 6257:	case 6263:	case 6269:
	      case 6271:	case 6277:	case 6287:	case 6299:	case 6301:
	      case 6311:	case 6317:	case 6323:	case 6329:	case 6337:
	      case 6343:	case 6353:	case 6359:	case 6361:	case 6367:
	      case 6373:	case 6379:	case 6389:	case 6397:	case 6421:
	      case 6427:	case 6449:	case 6451:	case 6469:	case 6473:
	      case 6481:	case 6491:	case 6521:	case 6529:	case 6547:
	      case 6551:	case 6553:	case 6563:	case 6569:	case 6571:
	      case 6577:	case 6581:	case 6599:	case 6607:	case 6619:
	      case 6637:	case 6653:	case 6659:	case 6661:	case 6673:
	      case 6679:	case 6689:	case 6691:	case 6701:	case 6703:
	      case 6709:	case 6719:	case 6733:	case 6737:	case 6761:
	      case 6763:	case 6779:	case 6781:	case 6791:	case 6793:
	      case 6803:	case 6823:	case 6827:	case 6829:	case 6833:
	      case 6841:	case 6857:	case 6863:	case 6869:	case 6871:
	      case 6883:	case 6899:	case 6907:	case 6911:	case 6917:
	      case 6947:	case 6949:	case 6959:	case 6961:	case 6967:
	      case 6971:	case 6977:	case 6983:	case 6991:	case 6997:
	      case 7001:	case 7013:	case 7019:	case 7027:	case 7039:
	      case 7043:	case 7057:	case 7069:	case 7079:	case 7103:
	      case 7109:	case 7121:	case 7127:	case 7129:	case 7151:
	      case 7159:	case 7177:	case 7187:	case 7193:	case 7207:
	      case 7211:	case 7213:	case 7219:	case 7229:	case 7237:
	      case 7243:	case 7247:	case 7253:	case 7283:	case 7297:
	      case 7307:	case 7309:	case 7321:	case 7331:	case 7333:
	      case 7349:	case 7351:	case 7369:	case 7393:	case 7411:
	      case 7417:	case 7433:	case 7451:	case 7457:	case 7459:
	      case 7477:	case 7481:	case 7487:	case 7489:	case 7499:
	      case 7507:	case 7517:	case 7523:	case 7529:	case 7537:
	      case 7541:	case 7547:	case 7549:	case 7559:	case 7561:
	      case 7573:	case 7577:	case 7583:	case 7589:	case 7591:
	      case 7603:	case 7607:	case 7621:	case 7639:	case 7643:
	      case 7649:	case 7669:	case 7673:	case 7681:	case 7687:
	      case 7691:	case 7699:	case 7703:	case 7717:	case 7723:
	      case 7727:	case 7741:	case 7753:	case 7757:	case 7759:
	      case 7789:	case 7793:	case 7817:	case 7823:	case 7829:
	      case 7841:	case 7853:	case 7867:	case 7873:	case 7877:
	      case 7879:	case 7883:	case 7901:	case 7907:	case 7919:
	      case 7927:	case 7933:	case 7937:	case 7949:	case 7951:
	      case 7963:	case 7993:	case 8009:	case 8011:	case 8017:
	      case 8039:	case 8053:	case 8059:	case 8069:	case 8081:
	      case 8087:	case 8089:	case 8093:	case 8101:	case 8111:
	      case 8117:	case 8123:	case 8147:	case 8161:	case 8167:
	      case 8171:	case 8179:	case 8191:	case 8209:	case 8219:
	      case 8221:	case 8231:	case 8233:	case 8237:	case 8243:
	      case 8263:	case 8269:	case 8273:	case 8287:	case 8291:
	      case 8293:	case 8297:	case 8311:	case 8317:	case 8329:
	      case 8353:	case 8363:	case 8369:	case 8377:	case 8387:
	      case 8389:	case 8419:	case 8423:	case 8429:	case 8431:
	      case 8443:	case 8447:	case 8461:	case 8467:	case 8501:
	      case 8513:	case 8521:	case 8527:	case 8537:	case 8539:
	      case 8543:	case 8563:	case 8573:	case 8581:	case 8597:
	      case 8599:	case 8609:	case 8623:	case 8627:	case 8629:
	      case 8641:	case 8647:	case 8663:	case 8669:	case 8677:
	      case 8681:	case 8689:	case 8693:	case 8699:	case 8707:
	      case 8713:	case 8719:	case 8731:	case 8737:	case 8741:
	      case 8747:	case 8753:	case 8761:	case 8779:	case 8783:
	      case 8803:	case 8807:	case 8819:	case 8821:	case 8831:
	      case 8837:	case 8839:	case 8849:	case 8861:	case 8863:
	      case 8867:	case 8887:	case 8893:	case 8923:	case 8929:
	      case 8933:	case 8941:	case 8951:	case 8963:	case 8969:
	      case 8971:	case 8999:	case 9001:	case 9007:	case 9011:
	      case 9013:	case 9029:	case 9041:	case 9043:	case 9049:
	      case 9059:	case 9067:	case 9091:	case 9103:	case 9109:
	      case 9127:	case 9133:	case 9137:	case 9151:	case 9157:
	      case 9161:	case 9173:	case 9181:	case 9187:	case 9199:
	      case 9203:	case 9209:	case 9221:	case 9227:	case 9239:
	      case 9241:	case 9257:	case 9277:	case 9281:	case 9283:
	      case 9293:	case 9311:	case 9319:	case 9323:	case 9337:
	      case 9341:	case 9343:	case 9349:	case 9371:	case 9377:
	      case 9391:	case 9397:	case 9403:	case 9413:	case 9419:
	      case 9421:	case 9431:	case 9433:	case 9437:	case 9439:
	      case 9461:	case 9463:	case 9467:	case 9473:	case 9479:
	      case 9491:	case 9497:	case 9511:	case 9521:	case 9533:
	      case 9539:	case 9547:	case 9551:	case 9587:	case 9601:
	      case 9613:	case 9619:	case 9623:	case 9629:	case 9631:
	      case 9643:	case 9649:	case 9661:	case 9677:	case 9679:
	      case 9689:	case 9697:	case 9719:	case 9721:	case 9733:
	      case 9739:	case 9743:	case 9749:	case 9767:	case 9769:
	      case 9781:	case 9787:	case 9791:	case 9803:	case 9811:
	      case 9817:	case 9829:	case 9833:	case 9839:	case 9851:
	      case 9857:	case 9859:	case 9871:	case 9883:	case 9887:
	      case 9901:	case 9907:	case 9923:	case 9929:	case 9931:
	      case 9941:	case 9949:	case 9967:	case 9973:	case 10007:
	        return true;
	    }
	}
	displayArea("   Error: Failed to find prime number greater than or equal to "+Val);
	return false;
    }
}
