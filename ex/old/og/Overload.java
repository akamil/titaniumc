/*
 * Copyright (c) 1995, 1996  
 * Open Software Foundation, Inc. 
 *  
 * OSF DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
 * WITH RESPECT TO THIS SOFTWARE INCLUDING, WITHOUT LIMITATION, 
 * ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL OSF BE LIABLE FOR ANY 
 * SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES RESULTING FROM 
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN CONTRACT, TORT 
 * INCLUDING NEGLIGENCE, OR OTHER LEGAL THEORY ARISING OUT OF 
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS 
 * SOFTWARE. 
 */

/*
 * This file is part of an OSF RI software distribution. This software is 
 * distributed under the terms of a free license for non-commercial use. 
 * 
 * You should have received a copy of those terms along with this file.
 * Please see file COPYRIGHT.
 * 
 * If not, please write to: OSF RI, 2 Av. de Vignate, 38610 Gieres, France
 */

import java.util.*;
/* **************************************** *
 * Overload is used for OVERLOADING testing *
 * **************************************** */

class Class_sup {
  public int test_method (double d,float f) {
    return 1;
  }
  public int test_method (long l,float f,Object o) {
    return 2;
  }

  public int other_meth (OverClass_1 t,Object o) {
    return 7;
  }
}

class Class_mid extends Class_sup {
  public int test_method (Vector v) {
    return 3;
  }
  public int test_method (float f1,long l1) {
    return 4;
  }
  public int test_method (long l,byte b) {
    return 5;
  }

  public int other_meth (OverClass_2 t,Vector v) {
    return 8;
  }
}

class Class_inf extends Class_mid {
  public int test_method (short s,byte b) {
    return 6;
  }

  public int other_meth (OverClass_2 t,My_Vector v) {
    return 9;
  }
}

class OverClass_1 {}
class OverClass_2 extends OverClass_1 {}
class OverClass_3 extends OverClass_2 {}
class OverClass_4 extends OverClass_3 {}
class OverClass_5 extends OverClass_4 {}
class OverClass_6 extends OverClass_5 {}
class OverClass_7 extends OverClass_6 {}

class My_Vector extends Vector {}

class Overload extends validtest {
  Class_sup val_sup;
  Class_mid val_mid;
  Class_inf val_inf;

  protected boolean execute (String argv[])
  {
    parse (argv);

    Vector v1;
    long l1=1;
    float f1=1.0F,f2=1.0F;
    double d1=1.0;
    Object o1;
    int i1=1;
    byte b1=1,b2=1;
    short s1=1;
    char c1=1;
    My_Vector m1;
    OverClass_1 val1;
    OverClass_2 val2;
    OverClass_3 val3;
    OverClass_7 val7;

    val_sup = new Class_sup();
    val_mid = new Class_mid();
    val_inf = new Class_inf();
    v1 = new Vector();
    o1 = new Object();
    m1 = new My_Vector();
    val1 = new OverClass_1();
    val2 = new OverClass_2();
    val3 = new OverClass_3();
    val7 = new OverClass_7();


/* *************** Tests when arguments are of primitive types *********** */

  /* *************************************************** *
   * Test when argument types in method calls match      *
   * exactly arguments of one of the method declarations *
   * *************************************************** */

    /* Test for an element of Class_inf */

    //(float,long) -> returns 4
    test_result = test_result & (val_inf.test_method(f1,l1)==4);
    //(double,float) -> returns 1
    test_result = test_result & (val_inf.test_method(d1,f1)==1);
    //(long,byte) -> returns 5
    test_result = test_result & (val_inf.test_method(l1,b1)==5);
    //(Vector) -> returns 3
    test_result = test_result & (val_inf.test_method(v1)==3);
    //(short,byte) -> returns 6
    test_result = test_result & (val_inf.test_method(s1,b1)==6);
    //(float,long,Object) -> returns 2
    test_result = test_result & (val_inf.test_method(l1,f1,o1)==2);

    /* Test for an element of Class_mid */

    //(double,float) -> returns 1
    test_result = test_result & (val_mid.test_method(d1,f1)==1);
    //(long,byte) -> returns 5
    test_result = test_result & (val_mid.test_method(l1,b1)==5);
    //(float,long,Object) -> returns 2
    test_result = test_result & (val_mid.test_method(l1,f1,o1)==2);
    //(float,long) -> returns 4
    test_result = test_result & (val_mid.test_method(f1,l1)==4);
    //(Vector) -> returns 3
    test_result = test_result & (val_mid.test_method(v1)==3);

    /* Test for an element of Class_sup */

    //(float,long,Object) -> returns 2
    test_result = test_result & (val_sup.test_method(l1,f1,o1)==2);
    //(double,float) -> returns 1
    test_result = test_result & (val_sup.test_method(d1,f1)==1);

  /* ***************************************************** *
   * Test when argument types in method calls do not       *
   * match any arguments of one of the method declarations *
   * ***************************************************** */

    // (byte,byte)   -> returns 6 for a Class_inf element
    //               -> returns 5 for a Class_mid element
    //               -> returns 1 for a Class_sup element
    test_result = test_result & (val_inf.test_method(b1,b2)==6);
    test_result = test_result & (val_mid.test_method(b1,b2)==5);
    test_result = test_result & (val_sup.test_method(b1,b2)==1);

    // (int,byte)    -> returns 5 for a Class_inf element
    //               -> returns 5 for a Class_mid element
    //               -> returns 1 for a Class_sup element
    test_result = test_result & (val_inf.test_method(i1,b1)==5);
    test_result = test_result & (val_mid.test_method(i1,b1)==5);
    test_result = test_result & (val_sup.test_method(i1,b1)==1);

    // (float,char)  -> returns 4 for a Class_inf element
    //               -> returns 4 for a Class_mid element
    //               -> returns 1 for a Class_sup element
    test_result = test_result & (val_inf.test_method(f1,c1)==4);
    test_result = test_result & (val_mid.test_method(f1,c1)==4);
    test_result = test_result & (val_sup.test_method(f1,c1)==1);

    // (float,float) -> returns 1 for a Class_inf element
    //               -> returns 1 for a Class_mid element
    //               -> returns 1 for a Class_sup element
    test_result = test_result & (val_inf.test_method(f1,f2)==1);
    test_result = test_result & (val_mid.test_method(f1,f2)==1);
    test_result = test_result & (val_sup.test_method(f1,f2)==1);


/* *********** Tests when arguments are not of primitive types *********** */

    // val_sup.other_method is called :
    //     No overloading can occur.
    //     Whatever parameters, return value is 7.

    test_result = test_result & (val_sup.other_meth(val1,o1)==7);
    test_result = test_result & (val_sup.other_meth(val2,o1)==7);
    test_result = test_result & (val_sup.other_meth(val3,o1)==7);
    test_result = test_result & (val_sup.other_meth(val7,o1)==7);
    test_result = test_result & (val_sup.other_meth(val1,v1)==7);
    test_result = test_result & (val_sup.other_meth(val2,v1)==7);
    test_result = test_result & (val_sup.other_meth(val3,v1)==7);
    test_result = test_result & (val_sup.other_meth(val7,v1)==7);
    test_result = test_result & (val_sup.other_meth(val1,m1)==7);
    test_result = test_result & (val_sup.other_meth(val2,m1)==7);
    test_result = test_result & (val_sup.other_meth(val3,m1)==7);
    test_result = test_result & (val_sup.other_meth(val7,m1)==7);


    // val_mid.other_method is called :
    //     Overloading of val_sup.other_method by val_mid.other_method
    //     will occure when other_method parameters (resp. p1 and p2)
    //     are elements of classes that are equals or subclasses
    //     of respectively OverClass_2 and Vector.

    test_result = test_result & (val_mid.other_meth(val1,o1)==7);
    test_result = test_result & (val_mid.other_meth(val2,o1)==7);
    test_result = test_result & (val_mid.other_meth(val3,o1)==7);
    test_result = test_result & (val_mid.other_meth(val7,o1)==7);
    test_result = test_result & (val_mid.other_meth(val1,v1)==7);
    test_result = test_result & (val_mid.other_meth(val2,v1)==8);
    test_result = test_result & (val_mid.other_meth(val3,v1)==8);
    test_result = test_result & (val_mid.other_meth(val7,v1)==8);
    test_result = test_result & (val_mid.other_meth(val1,m1)==7);
    test_result = test_result & (val_mid.other_meth(val2,m1)==8);
    test_result = test_result & (val_mid.other_meth(val3,m1)==8);
    test_result = test_result & (val_mid.other_meth(val7,m1)==8);

    // val_inf.other_method is called :
    //     Overloading of val_sup.other_method by val_mid.other_method
    //     will occure when other_method parameters (resp. p1 and p2)
    //     are elements of classes that are equals or subclasses
    //     of respectively OverClass_2 and Vector.
    //     Overloading of val_mid.other_method by val_inf.other_method
    //     will occure when other_method parameters (resp. p1 and p2)
    //     are elements of classes that are equals or subclasses
    //     of respectively OverClass_2 and My_Vector.

    test_result = test_result & (val_inf.other_meth(val1,o1)==7);
    test_result = test_result & (val_inf.other_meth(val2,o1)==7);
    test_result = test_result & (val_inf.other_meth(val3,o1)==7);
    test_result = test_result & (val_inf.other_meth(val7,o1)==7);
    test_result = test_result & (val_inf.other_meth(val1,v1)==7);
    test_result = test_result & (val_inf.other_meth(val2,v1)==8);
    test_result = test_result & (val_inf.other_meth(val3,v1)==8);
    test_result = test_result & (val_inf.other_meth(val7,v1)==8);
    test_result = test_result & (val_inf.other_meth(val1,m1)==7);
    test_result = test_result & (val_inf.other_meth(val2,m1)==9);

// These 2 tests will generate a compil error with alpha version of Java
//   but will compile with beta version (-> returns 9)
    test_result = test_result & (val_inf.other_meth(val3,m1)==9);
    test_result = test_result & (val_inf.other_meth(val7,m1)==9);

    WriteHTMLResults("Overload.html", "Overloading test");
    return test_result;
  }
}
