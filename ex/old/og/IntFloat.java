/*
 * Copyright (c) 1995, 1996  
 * Open Software Foundation, Inc. 
 *  
 * OSF DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
 * WITH RESPECT TO THIS SOFTWARE INCLUDING, WITHOUT LIMITATION, 
 * ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL OSF BE LIABLE FOR ANY 
 * SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES RESULTING FROM 
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN CONTRACT, TORT 
 * INCLUDING NEGLIGENCE, OR OTHER LEGAL THEORY ARISING OUT OF 
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS 
 * SOFTWARE. 
 */

/*
 * This file is part of an OSF RI software distribution. This software is 
 * distributed under the terms of a free license for non-commercial use. 
 * 
 * You should have received a copy of those terms along with this file.
 * Please see file COPYRIGHT.
 * 
 * If not, please write to: OSF RI, 2 Av. de Vignate, 38610 Gieres, France
 */


/* ************************************************** *
 * This class defines IntFloat : Gauss elimination    *
 *   Test of integer and related operators            *
 * ************************************************** */

class Gauss {
    int system_size;
    float mat[][];
    float result[];
    Gauss(int input_array[][]) {
        system_size = input_array.length;
        mat = new float[system_size][system_size+1];
        for (int i=0;i<=system_size-1;i++)
          for (int j=0;j<=system_size;j++)
            mat[i][j]=input_array[i][j];
        result = new float[system_size];
    }
    Gauss(float input_array[][]) {
        system_size = input_array.length;
        mat = new float[system_size][system_size+1];
        for (int i=0;i<=system_size-1;i++)
          for (int j=0;j<=system_size;j++)
            mat[i][j]=input_array[i][j];
        result = new float[system_size];
    }
    private void eliminate () {
        int i,j,k;
        for (i=0;i<=system_size-1;i++)
          for (j=i+1;j<=system_size-1;j++)
            for (k=system_size;k>=i;k--)
              mat[j][k] -= mat[i][k]*mat[j][i]/mat[i][i];
    }
    private void substitute () {
        int j,k;
        float t;
        for (j=system_size-1;j>=0;j--) {
          t = 0.0F;
          for (k=j+1;k<=system_size-1;k++) t += mat[j][k]*result[k];
          result[j] = (mat[j][system_size] - t)/mat[j][j];
        }
    }
    public void resolve () {
        this.eliminate();
        this.substitute();
    }
    public boolean check (int expected_result[]) {
        for (int i=0;i<=system_size-1;i++)
          if (result[i]!=expected_result[i]) return false;
        return true;
    }
    public boolean check (float expected_result[]) {
        for (int i=0;i<=system_size-1;i++)
          if (result[i]!=expected_result[i]) return false;
        return true;
    }
}



class IntFloat extends validtest {
 
    Gauss test_values;
    protected boolean execute(String argv[]) 
    {
	parse (argv); 
        int data1[][] = {{1,3,-4,8},{1,1,-2,2},{-1,-2,5,-1}};
        int result1[] = {1,5,2};
        float data2[][] = {{1.F,3.F,-4.F,8.F},{1.F,1.F,-2.F,2.F},{-1.F,-2.F,5.F,-1.F}};
        float result2[] = {1.F,5.F,2.F};
        float data3[][] = {{2F,6.4F}};
        float result3[] = {3.2F};
        float data4[][] = {{0.25F,0.5F,1.1F},{2.5F,1.25F,0.8F}};
        float result4[] = {-1.04F,2.72F};
        test_values = new Gauss (data1);
        test_values.resolve();
        test_result = test_result & test_values.check(result1);
        test_values = new Gauss (data2);
        test_values.resolve();
        test_result = test_result & test_values.check(result2);
        test_values = new Gauss (data3);
        test_values.resolve();
        test_result = test_result & test_values.check(result3);
        test_values = new Gauss (data4);
        test_values.resolve();
        test_result = test_result & test_values.check(result4);
        WriteHTMLResults("IntFloat.html", "Integer and related operators test");
	return test_result;
    }
}
