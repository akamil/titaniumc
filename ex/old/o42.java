class a
{
  single void f()
  {
    partition { true => { } }
    partition x
    {
      true => { x = 2; }
      false => { }
    }
  }
}
