class Build
{
  int instance = 1;
  static int shared = 2; 
 
  Build()
  {
    if (instance == 1)
      System.out.println( "1" );
    else
      System.out.println( "*" );
  }

  public static void main( String[] argv )
  {
    new Build();
    if (shared == 2)
      System.out.println( "2" );
    else
      System.out.println( "*" );
  }
}
