class FormalParam
{
  static void go( int value )
  {
    if (value == 1)
      System.out.println( "1" );
    else
      System.out.println( "*" );
  }

  public static void main( String[] argv )
  {
    go( 1 );
  }
}
