class TryFinally
{
  public static void main( String argv[] )
  {
    try
      {
	try
	  {
	    throw new Exception();
	  }
	finally
	  {
	    System.out.println( "1" );
	  }
      }
    catch (Exception unused)
      {
	System.out.println( "2" );
      }
  }
}
