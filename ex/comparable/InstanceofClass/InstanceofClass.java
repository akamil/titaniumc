class InstanceofClass
{
  public static void main( String[] argv )
  {
    InstanceofClass known = new InstanceofClass();
    Object mystery = known;

    if (mystery instanceof InstanceofClass)
      System.out.println( "1" );
    else
      System.out.println( "*" );

    if (mystery instanceof Exception)
      System.out.println( "*" );
    else
      System.out.println( "2" );
  }
}
