interface Face
{
}

class CastInterface implements Face
{
  public static void main( String[] argv )
  {
    CastInterface known = new CastInterface();
    Object mystery = known;
    Face recovered = (Face) mystery;

    System.out.println( "1" );
  }
}
