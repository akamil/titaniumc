class Case
{
  static final int one = 1;
  
  static void go( int value )
  {
    switch (value)
      {
      case one:
	System.out.println( "1" );
      case 2:
	System.out.println( "2" );
	break;
      case one + 2:
	System.out.println( "3" );
	break;
      default:
	System.out.println( "4" );
      }
  }
	

  public static void main( String[] argv )
  {
    go( 1 );
    go( 3 );
    go( 4 );
  }
}
