class TryCatchBreak
{
  public static void main( String[] argv )
  {
  escape:
    {
      try
	{
	  throw new Exception();
	}
      catch (Exception unused)
	{
	  System.out.println( "1" );
	  break escape;
	}
    }
    System.out.println( "2" );
  }
}
