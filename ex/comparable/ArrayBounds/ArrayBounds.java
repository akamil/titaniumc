class ArrayBounds
{
  static int[] values = { 0 };

  private static void result( String operation, int offset, boolean caught )
    {
      System.out.print( operation );
      System.out.print( ": " );
      System.out.print( offset );
      System.out.print( ": " );
      System.out.println( caught ? "exception thrown" : "allowed" );
    }      

  private static void tryRead( int offset )
    {
      boolean caught = false;

      try
	{
	  int value = values[ offset ];

	  // make the value look like it is
	  // used, or Sun elides the read
	  ++value;
	}
      catch ( ArrayIndexOutOfBoundsException exception )
	{
	  caught = true;
	}

      result( "write", offset, caught );
    }
  
  private static void tryWrite( int offset )
    {
      boolean caught = false;
      
      try
	{
	  values[ offset ] = 0;
	}
      catch ( ArrayIndexOutOfBoundsException exception )
	{
	  caught = true;
	}

      result( "write", offset, caught );
    }
  
  public static void main( String[] argv )
    {
      for (int index = -1; index <= 1; ++index)
	{
	  System.out.println( index );
	  tryRead( index );
	  tryWrite( index );
	}
    }
}
