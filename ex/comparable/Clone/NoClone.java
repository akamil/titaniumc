class NoClone
{
  static public void test()
  {
    boolean forbidden = false;

    try
      {
	NoClone original = new NoClone();
	NoClone duplicate = (NoClone) original.clone();
      }
    catch (CloneNotSupportedException exception)
      {
	forbidden = true;
      }

    if (forbidden)
      System.out.println( "ok: NoClone forbade cloning" );
    else
      System.out.println( "err: NoClone allowed cloning" );
  }
}
