class ComplexClone extends SimpleClone
{
  public boolean cloned = false;

  public Object clone()
  {
    try
      {
	ComplexClone duplicate = (ComplexClone) super.clone();
	duplicate.cloned = true;
	return duplicate;
      }
    catch (CloneNotSupportedException exception)
      {
	System.out.println( "err: ComplexClone superclass forbade cloning" );
	return null;
      }
  }

  static public void test()
  {
    ComplexClone original = new ComplexClone();
    original.value = 12;

    ComplexClone duplicate = (ComplexClone) original.clone();

    if (original.value == 12 && !original.cloned)
      System.out.println( "ok: ComplexClone original uncorrupted" );
    else
      System.out.println( "err: ComplexClone original corrupted" );
      
    if (duplicate.value == 12)
      System.out.println( "ok: ComplexClone simple fields correct" );
    else
      System.out.println( "err: ComplexClone simple fields incorrect" );
      
    if (duplicate.cloned)
      System.out.println( "ok: ComplexClone specialized fields correct" );
    else
      System.out.println( "err: ComplexClone specialized fields incorrect" );
  }
}
