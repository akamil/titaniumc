class SimpleClone implements Cloneable
{
  public int value = 0;

  static public void test()
  {
    try
      {
	SimpleClone original = new SimpleClone();
	original.value = 5;
	
	SimpleClone duplicate = (SimpleClone) original.clone();
	if (duplicate.value == 5)
	  System.out.println( "ok: SimpleClone copied fields correctly" );
	else
	  System.out.println( "err: SimpleClone copied fields incorrectly" );
      }
    catch (CloneNotSupportedException exception)
      {
	System.out.println( "err: SimpleClone forbade cloning" );
      }
  }
}
