class TryCatchThrowFinally
{
  static int go()
  {
    try
      {
	try
	  {
	    throw new Exception();
	  }
	catch (Exception unused)
	  {
	    System.out.println( "1" );
	    throw new Exception();
	  }
	finally
	  {
	    System.out.println( "2" );
	  }
      }
    catch (Exception unused)
      {
	System.out.println( "3" );
      }
    finally
      {
	System.out.println( "4" );
      }
    return 5;
  }

  public static void main( String argv[] )
  {
    if (go() == 5)
      System.out.println( "5" );
    else
      System.out.println( "*" );
  }
}
