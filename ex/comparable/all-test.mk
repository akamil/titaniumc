SRCDIR	:= $(shell pwd)
TOP     := ../..

ALL_TESTS  := $(sort $(patsubst %/,%,$(dir $(wildcard $(SRCDIR)/*/*.java))))
FAIL_TESTS := $(patsubst %/,%,$(dir $(wildcard $(SRCDIR)/*/broken)))
TESTS := $(filter-out $(FAIL_TESTS),$(ALL_TESTS))

PASSED   := $(addsuffix /passed,$(TESTS))
FAILED   := $(addsuffix /broken,$(FAIL_TESTS))
CLEAN    := $(addsuffix /clean,$(ALL_TESTS))
BYTE     := $(addsuffix /byte,$(TESTS))
EXEC     := $(addsuffix /exec,$(TESTS))
OUT_BYTE := $(addsuffix /out.byte,$(TESTS))
OUT_EXEC := $(addsuffix /out.exec,$(TESTS))
TC_EXEC  := $(TOP)/tc
TLIBDIR  := $(TOP)/runtime/backend/sequential
TLIB     := $(TLIBDIR)/libtlib-sequential-O.a $(TLIBDIR)/libtitanium-sequential.a

# rebuild by default
#REBUILD = rebuild 
# do not force a rebuild by default
REBUILD = 
RECURSE = @DIRNAME=`basename "$(@D)"` ; \
          mkdir -p "$$DIRNAME" && \
          cd "$$DIRNAME" && $(MAKE) --no-print-directory -f "$(SRCDIR)/one-test.mk" SRCDIR="$(SRCDIR)/$$DIRNAME" $(@F)

all : test

hello : force
	@echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++ "
	@echo "Running tests: "
	@for file in $(TESTS) ; do echo "    "`basename $$file` ; done
	@echo "Ignoring known-broken tests: "
	@fails="$(FAIL_TESTS)" ; \
         for file in $$fails ; do echo "    "`basename $$file` ; done

test : $(TC_EXEC) $(TLIB) hello $(PASSED) $(FAILED)
	@echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++ "

%/passed : force $(REBUILD)
	@echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++ "
	@echo "Testing "`basename $(@D)`"..."
	$(RECURSE)

%/broken : force
	@echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++ "
	@echo "ex/comparable/"`basename $(@D)`": SKIPPED"

$(TC_EXEC):
	$(MAKE) -C $(TOP) tc

$(TLIB):
	$(MAKE) -C $(TLIBDIR) all tlib-O

clean : $(CLEAN)
%/clean : force ; $(RECURSE)

rebuild :
	$(MAKE) -C $(TOP)

force :

.PHONY: clean force rebuild hello test

