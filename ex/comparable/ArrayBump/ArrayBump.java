class ArrayBump
{
    public static void main( String[] argv )
    {
	int   index = 0;
	int[] values = { 0, 0, 0 };

	values[ index++ ] = 1;
	values[ ++index ] = 2;
	
	++values[ index ];
	values[ index ]++;

	System.out.println( index );
	System.out.println( values[ 0 ] + " " +
			    values[ 1 ] + " " +
			    values[ 2 ] );
    }
}
