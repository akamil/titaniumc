all : passed

SRCDIR = .
VPATH = $(SRCDIR)
JAVAC = javac
JAVA  = java
TEST  = $(notdir $(shell pwd))
EXEC  = $(TEST).exec
JAVAS = $(wildcard $(SRCDIR)/*.java)
BYTES = $(patsubst %.java,%.class,$(notdir $(JAVAS)))
OUTS  = out.byte out.exec

TOP     = ../../..
BACKEND = sequential
TCBUILD = $(TOP)/tcbuild/tcbuild --backend $(BACKEND) --generate-dir tc-gen --cache-dir tc-cache --silent


passed : $(OUTS)
	@echo diff $^
	@diff $^ || ( echo "ex/comparable/$(TEST): FAILED" ; exit 1 )
	@echo "ex/comparable/$(TEST): PASSED" ; touch $@

exec : $(EXEC)

out.exec : $(EXEC)
	TI_BACKEND_SILENT=1 ./$< >$@ 2>&1

$(EXEC) : $(JAVAS) $(TOP)/tc $(TOP)/runtime/backend/$(BACKEND)/libtitanium-$(BACKEND).a 
	$(TCBUILD) --outfile $(EXEC) $(SRCDIR)/$(TEST).java

byte : $(BYTES)

out.byte : $(BYTES)
	$(JAVA) $(TEST) >$@ 2>&1

%.class : %.java
	$(JAVAC) -d . -sourcepath "$(SRCDIR)" "$^"

clean :
	rm -Rf *.[oCH] *.class $(EXEC) $(OUTS) passed core tc-gen tc-cache

force :

.PHONY: clean force 
