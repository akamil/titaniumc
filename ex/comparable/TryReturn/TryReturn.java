class TryReturn
{
  static int go()
  {
    try
      {
	return 1;
      }
    catch (Exception ball)
      {
      }
    return 0;
  }

  public static void main( String argv[] )
  {
    if (go() == 1)
      System.out.println( "1" );
    else
      System.out.println( "*" );
  }
}
