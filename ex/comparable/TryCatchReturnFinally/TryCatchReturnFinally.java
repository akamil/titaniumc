class TryCatchReturnFinally
{
  static int go()
  {
    try
      {
	try
	  {
	    throw new Exception();
	  }
	catch (Exception unused)
	  {
	    return 3;
	  }
	finally
	  {
	    System.out.println( "1" );
	  }
      }
    finally
      {
	System.out.println( "2" );
      }
  }

  public static void main( String argv[] )
  {
    if (go() == 3)
      System.out.println( "3" );
    else
      System.out.println( "*" );
  }
}
