class CastClass
{
  public static void main( String[] argv )
  {
    CastClass known = new CastClass();
    Object mystery = known;

    if ((CastClass) mystery == known)
      System.out.println( "1" );
    else
      System.out.println( "*" );
  }
}
