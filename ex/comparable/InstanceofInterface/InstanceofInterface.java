class InstanceofInterface implements Face
{
  public static void main( String[] argv )
  {
    InstanceofInterface known = new InstanceofInterface();
    Object mystery = known;

    if (mystery instanceof Face)
      System.out.println( "1" );
    else
      System.out.println( "*" );
  }
}
