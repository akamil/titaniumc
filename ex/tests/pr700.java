// Tests assignment of char constants to bytes and shorts.
// Relevant PR: PR700
// Expected result: PASS

class M6 {
  public static void main(String[] args) {
    byte b = '+';
    short s = '-';
  }
}

