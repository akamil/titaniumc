// Test use of nested class in template that extends declarer of that class.
// Expected result: PASS

class W3 {
  static class W3a {
  }
  public static void main(String[] args) {
    new W3b<Object>();
  }
}

template<class x> class W3b extends W3 {
  { System.out.println(new W3a()); }
}
