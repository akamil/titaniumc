// Tests inner classes inside type-based templates parameterized with nested
// class.
// Expected result: PASS
template<class x> class S3 {
    class S3a {
	{System.out.println(new x());}
    }
}

class S3c {
    public static void main(String[] args) {
	System.out.println(new S3<S3d>().new S3a());
	System.out.println(new S3<S3c.S3b>().new S3a());
    }
    static class S3b {
    }
    static class S3d {
    }
}
