@< class Element > class Cons
{
    public final Element head;
    public final Cons tail;

    public Cons( Element head )
    {
	this( head, null );
    }

    public Cons( Element head, Cons tail )
    {
	this.head = head;
	this.tail = tail;
    }

    public String toString()
    {
	return
	    '('
	    + (head
	       + ('.'
		  + (tail == null ? "nil" : tail.toString())
		  + ')'));
    }
}


@< class Element > class Stack
{
    private Cons< Element > data;

    public boolean empty()
    {
	return data == null;
    }

    public Element top()
    {
	return data.head;
    }

    public Element pop()
    {
	Element chaff = data.head;
	data = data.tail;
	return chaff;
    }

    public void push( Element arrival )
    {
	data = new Cons< Element >( arrival, data );
    }

    public String toString()
    {
	String result = "[";
	boolean first = true;

	Cons< Element > cursor = data;
	while (cursor != null)
	    {
		if (!first) result += ", ";
		result += cursor.head;
		cursor = cursor.tail;
		first = false;
	    }

	result += ']';
	return result;
    }
}


class StackOfStack
{
    static public void main( String[] args )
    {
	Stack< int > primes = new Stack< int >();
	Stack< int > squares = new Stack< int >();
	Stack< Stack< int >> stacks = new Stack< Stack< int >>();

	primes.push( 7 );
	primes.push( 11 );
      
	stacks.push( primes );
	stacks.push( squares );
      
	stacks.top().push( 9 );
	stacks.top().push( 4 );

	System.out.println( stacks );
    }
}
