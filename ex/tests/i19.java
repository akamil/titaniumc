// test qualified allocation with null qualifier
// expected result: fail
class J1 {
    class J1a {
      //{System.out.println(y);}
    }
    public static void main(String[] args) {
	J1 x = null;
	System.out.println(x.new J1a());
    }
  int y = 4;
}
