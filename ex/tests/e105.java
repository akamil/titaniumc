template < class Element , class Bar > class Cons
{
}

template < boolean x, boolean y > class Tricky
{
}

template < int x, int y > class Tricky2
{
}

class foo {
  public static final int Cons = 1;
  public static final int x = 2;
  public static final int y = 3; 
public static void main(String[]args) {
  @Cons local<Object,String> c1;
  @Cons local [] <Object,String> c2;
  @Cons [] <Object,String> c3;
  @Cons <Object,String> local c4;
  @Cons <Object,String> local [] c5;
  @Tricky< (Cons < x), (y > 0)> t1;
  @Tricky< Cons < x, y > 0> t2;
  @Tricky< Cons > x, y > 0> t2a;
  @Tricky< Cons > x, y < 0> t2b;
  @Tricky< Cons <= x, y >= 0> t2c;
  @Tricky2< (Cons + x), (y + 0)> t3;
  @Tricky2< Cons + x, y + 0> t4;
  @Tricky2< Cons >> x, y >> 1> t5;
  @Tricky2< Cons >>> x, y >>> 1> t6;
  @Tricky2< (Cons >> x), (y >> 1)> t5;
  @Tricky2< (Cons >>> x), (y >>> 1)> t6;
}}

