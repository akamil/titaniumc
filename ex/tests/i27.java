// test ambiguous reference to multiply inherited typename
// expected result: fail 
class q1 implements q1a, q1b {
    public static void main(String[] args) {
	System.out.println(new q1c());
    }
}

interface q1a {
    class q1c {
    }
}

interface q1b {
    class q1c {
    }
}
