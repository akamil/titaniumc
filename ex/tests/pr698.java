// Check legal assignment of blank finals.
// Relevant PR: PR698
// Expected result: PASS

class G6 {
  public static void main(String[] args) {
    System.out.println(x);
  }
  static final int x;
  static {
    x = 3;
  }
  static final int v;
  static int u = v = 6;
  final int y;
  { y = 4; }
  final int z;
  int w = z = 5;
}
