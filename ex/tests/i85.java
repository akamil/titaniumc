// Test erroneous nested, local, and anonymous classes in templates.
// Expected result: FAIL

class U3 {
  public static void main(String[] args) {
    new U3a<int>();
    new U3d<4>();
  }
}

template<class x> class U3a {
  { System.out.println(new x() {}); }
  { class U3b extends x {} System.out.println(new U3b()); }
  static class U3c implements x.y {
  }
}

template<int x> class U3d {
  { System.out.println(new x() {}); }
  { class U3e extends x {} System.out.println(new U3e()); }
  static class U3f implements x.y {
  }
}

