// Test shadowing/redeclaration.
// Expected result: FAIL

class m5 {
  m5a<m5b.x + 1> x;
  public static void main(String[] args) {
  }
  void foo(int x, int x) {}
  void foo(int x) {
    int x;
  }
  void foo() {
    int x;
    int x;
    { int x; }
    for (int x = 0; ;) { break; }
    foreach (Point<1> x in [1:2]) {}
  }
}

template<int x> class m5a {
}

class m5b {
  static final int x = 3;
}
