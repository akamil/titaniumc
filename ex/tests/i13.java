// test qualified and unqualified allocation where this should be used as
// qualifier
// expected result: pass
class D1 {
  class D1a {
  }
  void foo() {
      System.out.println(new D1a());
  }
  void bar() {
    System.out.println(this.new D1a());
  }
  public static void main(String[] args) {
    new D1().foo();
    new D1().bar();
    //new D1a();
  }
}

