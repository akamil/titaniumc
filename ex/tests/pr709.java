// Test local class allocation in scope of final variable when encloser
// extends template.
// Relevant PR: PR709
// Expected result: PASS

class Q6 extends Q6a<1> 
{
  public static void main(String[] args) {
    final int y = 3;
    new Object() {};
  }
}

template<int x> class Q6a {
}
