// Test normal completion and reachability of switch.
// Relevant PR: PR696
// Expected result: FAIL

class c7 {
  public static void main(String[] args) {
  }
  static int foo(int x) {
    switch (x) {
    case 0:
      x += 3;
    default:
      return 0;
    }
    return 1;
  }
  static int bar(int x) {
    switch (x) {
      case 0:
        x += 3;
        return 0;
      case 2:
        x += 2;
      default:
        x -= 3;
    }
    return 2;
  }
  static int baz(int x) {
    switch (x) {
      case 0:
      case 2:
      default:
    }
    return 3;
  }
  static int foobar(int x) {
    switch (x) {
      case 0:  
        x += 3;
        return 0;
      case 2:  
        x -= 3;
        return 0;
      default:
    }
    return 4;
  }
  static int foobaz(int x) {
    switch (x) {
      case 0:  
        x += 3;
        break;
      case 2:  
        return 5;
      default:
        return 6;
    }
    return 7;
  }
  static int barbaz(int x) {
    switch (x) {
      case 0:  
        return 8;
        x += 3;
      case 2:  
        x -= 3;
    }
    return 9;
  }
  static int foobarbaz(int x) {
    OUTER: while (true) {
      switch (x) {
        case 0:  
          x += 3;
          break OUTER;
        case 2:  
          x -= 3;
      }
    }
    return 2;
  }
}

