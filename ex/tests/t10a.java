@< int Initial > class Base
{
    static final int baseValue = Initial;
}
  

@< int Initial > class Derived extends Base< Initial + 1 >
{
    static final int derivedValue = Initial;
}
  

class Main
{
    static public void main( String[] args )
    {
	System.out.println(  Derived< 4 >.derivedValue );
	System.out.println(  Derived< 4 >.baseValue );
    }
}
