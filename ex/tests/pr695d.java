// Test method call to abstract method from abstract class.
// Relevant PR: PR695
// Expected result: PASS

abstract class F6 {
  public static void main(String[] args) {
  }
  void foo() {
    bar();
  }
  abstract void bar();
}

