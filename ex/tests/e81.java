class a
{
  a local x;
  a g;
  a local [] y;
  a local [1d] z;

  void f()
  {
    x = null;
    this.x = null;
    g.x = null;
    y[0] = null;
    z[0] = null;
  }
}
