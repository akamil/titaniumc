interface i { }
class a implements i { }
class b extends a { }
final class d { }

class c {
  b x1[] = new a[10];
  i x2[] = new d[10];
  a x4[] = new i[5];
  d x5[] = (d[])(new i[5]);
}
