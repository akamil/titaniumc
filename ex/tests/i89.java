// Test disambiguation of template qualified names.
// Expected result: PASS

class c4 {
  public static void main(String[] args) {
    c4a<4>.c4b.c4c c = new c4a<4>.c4b.c4c();
    c4a<6>.c4b.c4c[] d = new c4a<6>.c4b.c4c[] { null, null, null };
    c4a<6>.c4b.c4c[] e = new c4a<6>.c4b.c4c[3];
    System.out.println(c4a<5>.c4b.c4c.x);
    System.out.println(c4a<3>.c4b.y.x);
    System.out.println(c4a<2>.w.y.x);
    System.out.println(c4a<c4a<11>.u>.c4b.y.x);
    System.out.println(c4a<c4a<7>.c4b.c4c.x>.c4b.y.x);
    System.out.println(c4d<c4a<1>.c4b.c4c>.x.x);
    System.out.println(c.x);
  }
}

template<int z> class c4a {
  static class c4b {
    static class c4c {
      final static int x = z + 1;
    }
    static c4c y = new c4c();
  }
  static c4b w = new c4b();
  final static int u = z * 2;
}

template<class v> class c4d {
  static v x = new v();
}
