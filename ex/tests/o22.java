interface i1 {
  void f();
}

abstract class i2 {
  void f() { }
  native void f2();
  abstract void f3();
}
