// test access checking for member classes
// expected result: fail
class Q1 {
    public static void main(String[] args) {
	Q1a.Q1b x = new Q1a.Q1b();
	System.out.println(x);
    }
}

class Q1a {
    private static class Q1b {
    }
}
