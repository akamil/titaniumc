// Test multiply nested qualified anonymous classes.
// Expected result: PASS

class A5 {
  public static void main(String[] args) {
    final A5b x = new A5b();
    x.new A5a(1) { 
      { x.new A5a(2) { 
          { new A5c<5>(); }
        }; 
      } 
    };
    new A5().foo();
  }
  void foo() {
    final A5b x = new A5b();
    x.new A5a(3) { 
      { x.new A5a(4) { 
          { new A5c<6>(); } 
        }; 
      } 
    };
  }
}

class A5b {
  class A5a {
    A5a(int x) { System.out.println(x); }
  }
}

template<int x> class A5c {
  { System.out.println(x); }
}
