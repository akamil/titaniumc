// Test singleness of array allocations.
// Relevant PR: PR797, PR817
// Expected result: PASS

class S9 {
  public static single void main(String[] args) {
    int[] single [] is = new int[1][Ti.thisProc()];
    int[1d] single [2d] ts = new int[0 : 0][[0,0] : [Ti.thisProc(),1]];
    int[] single js = { Ti.thisProc() };
    int[] local single ks = new int[] { Ti.thisProc() };
    if (is.length < 2) Ti.barrier();
    System.out.println("done.");
  }
}

