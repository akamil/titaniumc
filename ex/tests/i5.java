// test circularity with interface
// expected result: fail
class v1 extends v1a.v1b {
  interface v1c {
  }
}

interface v1a extends v1.v1c {
  class v1b {
  }
}
