// Tests inner classes inside immutables.
// Expected result: PASS

immutable class i2 {
    class i2a {
    }
    public static void main(String[] args) {
	System.out.println(new i2().new i2a());
    }
}
