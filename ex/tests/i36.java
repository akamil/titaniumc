// Tests immutable inner classes.
// Expected result: FAIL (PR561)

class l2 {
    immutable class l2a {
	int x;
    }
    public static void main(String[] args) {
	System.out.println(new l2.l2a().x);
    }
}
