// Test supertype's access to protected method of subtype in another package.
// Relevant PR: PR702
// Expected result: FAIL

import pack.pr702a2;
import pack2.pr702a3;

class W6 extends pr702a2 {
}

