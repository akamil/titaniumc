// overriding/hiding a final method
class a {
  void x() { }
  static void y() { }
}

class b extends a {
  static void x() { }
  void y() { }
}

