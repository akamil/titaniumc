// Test non-single broadcast in anonymous array init.
// Relevant PR: PR792
// Expected result: FAIL

class W8 {
  public static single void main(String[] args) {
    if (Ti.thisProc() != 0) {
      int a[] = new int[] {broadcast Ti.thisProc() from 0};
    }
  }
}

