// Conversions & assignment to interfaces
class A {
  // assignments
  A a1;
  Z z1 = 0;
  Z o1 = new A[10];
  C c1;
  Z z3 = a1;
  Z2 z2 = z1;

  // casts
  B b1;
  Object o3 = (Z)0;
  Object o4 = (Z)(new A[10]);
  Object o6 = (Z)c1;
}

class B extends A {
}

final class C {
  int f(int x) { return 0; }
}

interface Z {
  int f(int x);
}

interface Z2 extends Z {
}
