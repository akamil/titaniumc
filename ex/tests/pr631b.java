// Test unqualified access to an inherited field of an enclosing class.
// Expected result: PASS

class r4 {
  public static void main(String[] args) {
    new r4a().new r4b();
  }
  int xxx = 3;
  static int vvv = 9;
}

class r4a extends r4 {
  class r4b {
    { 
      System.out.println(xxx); 
      System.out.println(vvv); 
    }
  }
}
