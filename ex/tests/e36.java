class e1 extends Throwable { }
class e2 extends Throwable { }
class e3 extends Throwable { }
class e3a extends e3 { }

interface i1 {
  void f() throws e2;
}

interface i2 {
  void f() throws e1;
}

class b implements i1, i2
{
}

class c extends b
{
  public void f() throws e1 { }
}
