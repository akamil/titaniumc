class e1 extends Throwable { }
class e2 extends Throwable { }
class e3 extends Throwable { }
class e3a extends e3 { }

interface i1 {
  void f();
}

interface i2 {
  int f();
}

class b implements i1, i2
{
}
