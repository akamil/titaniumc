class e extends Throwable { }

class a {
  e e1;
  int single f(int single x, int single y) throws e
  {
    silly:
    try {
      if (y > 0) throw e1;
    }
    finally { break silly; }
    y++;
    return x;
  }
}
