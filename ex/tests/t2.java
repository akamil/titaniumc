public class Test {
  double[3d] x;
  RectDomain<3> d = x.domain();
  int i = x.arity();
  Point<3> p;
  double j = x[p];
  void f() { x[p] = j + x[0,0,0]; }
  double[3d] y = x.translate(p);
}

class B { }
