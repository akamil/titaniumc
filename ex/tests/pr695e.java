// Test binding to maximally specific method.
// Relevant PR: PR695
// Expected result: PASS

class q7 {
  public static void main(String[] args) {
    foo(args);
  }
  static void foo(Object[] args) {
    System.out.println(1);
  }
  static void foo(String[] args) {
    System.out.println(2);
  }
  static void foo(Object args) {
    System.out.println(3);
  }
}

