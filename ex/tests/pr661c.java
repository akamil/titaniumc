// Test folding of fields of class that is not method ready. See PR661.
// Expected result: PASS

class X5 {
  public static final int arity = X5a.val3;
  public static void foo(template X5b<arity> rd_result) { }
  public static void main(String[] args) { }
}

class X5a {
  public static final int val3 = 3;
}

template <int blah>
class X5b {
}
