// Test static accesses with template parameters.

class p4 {
  public static void main(String[] args) {
    new template p4a<p4b>();
  }
}

class p4b {
  static int y = 3;
}

template<class x> class p4a {
  static { System.out.println(x.y); }
}

