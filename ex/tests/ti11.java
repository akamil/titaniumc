// Test class passing type member of itself as actual to template superclass.
// Expected result: FAIL

class H4 extends H4a<H4.H4b> {
  public static void main(String[] args) {
  }
  static class H4b {}
}

template<class x> class H4a {
}

