// Test qualified anonymous class with template qualifier.
// Expected result: PASS

class x5 {
  static final int x = 3;
  public static void main(String[] args) {
    new x5a<x+1>().new x5b() { { System.out.println("anon"); } };
  }
}

template<int x> class x5a {
  class x5b {
    { System.out.println(x); }
  }
}
