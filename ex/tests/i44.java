// Tests anonyomus classes.
// Expected result: PASS

class A2 {
  public static void main(String[] args) {
    System.out.println(new A2() {
	public String toString() {
	  return "anon A2: " + super.toString(); 
	}});
    A2 zzz = new A2() {};
    System.out.println(zzz);
  }
  public String toString() { 
    Object a = new Object() { { System.out.println(x); } };
    System.out.println(a);
    return "A2: " + super.toString();
  }
  int x = 8;
}

