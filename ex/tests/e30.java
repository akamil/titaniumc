// Check restrictions on static code access to non-static things
class a {
  static a local polyshared s1 = this;
  a t1;
  static a f2 = t1;
  static a f3 = this.t1;

  static
  {
    s1 = (a local) this;
    f2 = t1;
    f3 = this.t1;
  }
}

class b extends a {
  static a b1 = super.s1;
  static a b2 = super.t1;
}

interface i {
  a x = this;
  a y = super.hmm;
  a z = this.x;
}
