// Test use of inner class with template that extends declarer of that class.
// Expected result: PASS

class Y3 {
  class Y3a {
    { System.out.println(Y3.this); }
  }
  public static void main(String[] args) {
    System.out.println(new Y3b<Object>().new Y3a());
  }
}

template<class x> class Y3b extends Y3 {
  { System.out.println(new Y3a()); }
}
