// Test typechecking rules for ? expressions.
// Relevant PR: PR713
// Expected result: FAIL

class K7 {
  public static void main(String[] args) {
    System.out.println(args.length > 0 ? new String() : new StringBuffer());
  }
}

