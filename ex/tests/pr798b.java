// Test return contravariance with respect to single.
// Expected result: FAIL

class y9 {
  public static single void main(String[] args) {
    y9 y = new y9a();
    int single i = y.foo();
    if (i < 5) Ti.barrier();
    System.out.println("done.");
  }
  int single foo() {
    return Ti.numProcs();
  }
}

class y9a extends y9 {
  int foo() {
    return Ti.thisProc();
  }
}
