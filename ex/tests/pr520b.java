// Test overloading with template arguments (PR520).
// Expected result: PASS

template<class T> public class o5a{
  public o5a(){
    T tt;
  }
}

class o5{
  public static single void main (String [] args){
    template o5a<double > t = new template o5a<double >();
    o5b superClass=new o5c(); 
    superClass.test(t);
    o5b superClass2=new o5b(); 
    superClass2.test(t);
  }
}



public class o5b{
  public void test(template o5a<double > temp) {
    System.out.println("This is o5b::test(o5a)");
  }
}


public class o5c extends o5b{
  public void test(template o5a<double > temp) {
    System.out.println("This is o5c::test(o5a)");
  }
}



