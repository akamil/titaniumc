// Test local and anonymous implementation of templates.
// Expected result: PASS

class Z4 {
  public static void main(String[] args) {
    class Z4b implements Z4a<3> { { System.out.println(foo); } }
    System.out.println(new Z4b());
    System.out.println(new Z4a<4>() {}.foo);
  }
}

template<int x> interface Z4a {
  static final int foo = x;
}
