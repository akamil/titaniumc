// Conversions & assignment to classes
public class A {
  // assignments
  A a1 = null;
  A o1 = new A[10];
  Z z1;
  A o2 = z1;
  B a2 = new A();
  A a3 = 0;

  // casts
  C c1;
  B b1;
  Object o4 = (A)(new A[10]);
  Object o6 = (C)z1;
  Object o7 = (C)a1;
  Object o8 = (C)0;
}

class B extends A {
}

final class C {
  int f(int x) { return 0; }
}

interface Z {
  int f(int x);
}
