// can't override final methods
class A
{
  final void f() { }
};

class B extends A
{
  final void f() { }
}

final class C { }

class D extends C { }

abstract final class E { }
