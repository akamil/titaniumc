// Test multiple inheritance of same nested class.
// Expected result: PASS

class B3 extends B3a implements B3b {
  public static void main(String[] args) {
    System.out.println(new B3c());
  }
}

class B3a implements B3b {
}

interface B3b {
  class B3c {
  }
}

