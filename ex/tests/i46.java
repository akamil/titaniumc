// Tests interface anonymous classes.
// Expected result: PASS

class E2 {
  public static void main(String[] args) {
    Runnable x = new Runnable() {
	public void run() {
	  System.out.println(-9);
	}
        class z {};
        {System.out.println(new z());}
      };
    System.out.println(x);
    x.run();
    //class z {};
    //System.out.println(new z());
  }
}

