// Test extension of nested types in multi-parametered templates.
// Expected result: PASS

class d5 extends d5b<d5a.y, d5a.y + 1>.d5c {
  public static void main(String[] args) {
  }
}

class d5a extends d5d<d5d<d5d<d5b<6, 7>.d5c, Object>.d5e, Throwable>.d5e, String>.d5e {
  final static int y = 4;
}

template<int x, int y> class d5b {
  static class d5c {
  }
}

template<class x, class y> class d5d {
  static class d5e {
  }
}
