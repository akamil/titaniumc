// Tests exception checking of this/super constructor calls.
// Expected result: FAIL

class M2 {
  M2() throws java.io.IOException {
    throw new java.io.IOException();
  }
  M2(int x) {
    this();
  }
  public static void main(String[] args) {
    new M2(3);
    new M2a();
    new M2b(); 
    new M2c();
 }
}

class M2a extends M2 {
  M2a() {
    super();
  }
}

class M2b extends M2 {
}

class M2c extends M2 {
  M2c() throws java.io.IOException {
  }
}
