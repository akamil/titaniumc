class a {
  void f() {
    final int x;

    x = 2; /* This is ok */
    x = 3;
  }

  void g(final int x) {
    x = 2;
  }
}
