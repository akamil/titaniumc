// Test normal completion and reachability of while.
// Relevant PR: PR696
// Expected result: FAIL

class D6 {
  public static void main(String[] args) {
  }
  static int foo(int x) {
    while (x < 2) {
      return x;
    }
    return x-1;
  }
  static int bar() {
    while (true) return 1;
  }
  static int baz() {
    while (true) break;
    return 0;
  }
  static int foobar(int x) {
    while (true) if (x < 3) return 3;
  }
  static int foobaz(int x) {
    while (false) x += 2;
    return x;
  }
}

