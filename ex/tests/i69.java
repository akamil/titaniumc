// Test nested class superclass resolution in anonymous class.
// Expected result: PASS

class v3 {
  public static void main(String[] args) { 
    class v3f {}
    new Object() {
      class v3a {
        class v3d {}
      }
      class v3b extends v3a {
      }
      class v3c extends v3a.v3d {
        v3c() { new v3a().super(); }
      } 
      class v3e extends v3f {
      }
      class v3g extends v3.v3h {
      }
      { System.out.println(new v3a()); }
      { System.out.println(new v3b()); }
      { System.out.println(new v3c()); }
      { System.out.println(new v3a().new v3d()); }
      { System.out.println(new v3e()); }
      { System.out.println(new v3f()); }
      { System.out.println(new v3g()); }
    };
  }
  static class v3h {
  }
}
