// Test subtype's access to protected method of supertype in another package.
// Relevant PR: PR702
// Expected result: PASS

import pack.pr702a;

class T6 extends pr702a {
  public static void main(String[] args) {
    System.out.println(baz());
    System.out.println(pr702a.baz());
  }
  void bar() {
    System.out.println(foo());
  }
}

