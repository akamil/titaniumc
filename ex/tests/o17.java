// Conversions & assignment to classes
class A {
  // assignments
  A a1 = null;
  Object o1 = new A[10];
  Z z1;
  Object o2 = z1;
  A a2 = new B();

  // casts
  C c1;
  B b1;
  Object o3 = (A)null;
  Object o4 = (Object)(new A[10]);
  Object o5 = (A)z1;
  Object o6 = (C)z1;
  Object o7 = (B)a1;
  Object o8 = (A)b1;
}

class B extends A {
}

final class C implements Z {
  public int f(int x) { return 0; }
}

interface Z {
  int f(int x);
}
