// Test import of nested type with non-canonical name.
// Relevant PR: PR689
// Expected result: FAIL

import pack.pr689a3.m6a;
import pack.pr689a3.*;

class s6 {
  public static void main(String[] args) {
    System.out.println(new m6a());
  }
}

