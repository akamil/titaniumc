// Tests immutable nested classes.
// Expected result: PASS

class h2 {
    static immutable class h2a {
	int x;
    }
    public static void main(String[] args) {
	System.out.println(new h2.h2a().x);
    }
}

immutable class h2b {
}
