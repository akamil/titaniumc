// Test method call to overloaded method where there are static and non-static
// candidates.
// Relevant PR: PR697
// Expected result: PASS

class E6 {
  public static void main(String[] args) {
  }
  void foo() {
    this.bar();
    bar();
  }
  static void bar(int x) {}
  void bar() {}
  static void bar(Object y) {}
}

