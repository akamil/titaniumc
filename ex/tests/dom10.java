public class copydom {
public static void foo(Domain<1> d) {}
public static void main(String[]args) {
Domain<1> d = [1:10] + [100:120];
foo(d);
PrivateRegion pr = new PrivateRegion();
Region r = pr;
Domain<1> d2 = new (r) Domain<1>(d);
Domain<1> d3 = new Domain<1>(d);
Domain<1> d4 = new Domain<1>(new (pr) Domain<1>(d));
Domain<1> d5 = new (pr) Domain<1>(d);
System.out.println(d);
System.out.println(d2);
System.out.println(d3);
System.out.println(d4);
System.out.println(d5);
System.out.println((Object)d == d2);
System.out.println((Object)d == d3);
System.out.println((Object)d == d4);
}}

