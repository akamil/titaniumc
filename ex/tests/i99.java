// Test qualified qualified this.
// Expected result: PASS

class m4 {
  class m4a {
    class m4b {
      { System.out.println(m4.m4a.this); }
    }
  }
  public static void main(String[] args) {
    new m4().new m4a().new m4b();
  }
}
