class a
{
  a x;
  a local y;
}

class b
{
  a xx;
  a local yy;

  a x1 = xx.x;
  a x2 = xx.y;

  a local [] local zz;

  local void f()
  {
    yy.x = null;
    yy.y = null;
    zz[0] = null;
  }
}

