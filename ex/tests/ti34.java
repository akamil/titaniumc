// Test template extending instantiation of itself.
// Expected result: FAIL                                   

class r5 {
  public static void main(String[] args) {
    new r5a<0>() {};
  }
}

template<int x> interface r5a extends r5a<x + 1> {
}
