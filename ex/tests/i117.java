// Test inner and anonymous class allocation from local method.
// Relevant PR: PR652
// Expected result: PASS

import java.io.*;

public class i6 {
    public class i6a {
	public i6a() {}
	public i6a(Object x, Throwable t, InputStream z, DataInput d) {}
    }
    public i6() {
	i6a i = this.new i6a();
	Object p = new Object() {};
	i6a i2 = this.new i6a(new String(), new IOException(), new
			      PushbackInputStream(null), new DataInputStream(null));
	i6a i3 = this.new i6a(new String(), new IOException(), new
			      PushbackInputStream(null), new DataInputStream(null)) {};
    }
    public void foo() {
	i6a i = this.new i6a();
	Object p = new Object() {};
	i6a i2 = this.new i6a(new String(), new IOException(), new
			      PushbackInputStream(null), new DataInputStream(null));
	i6a i3 = this.new i6a(new String(), new IOException(), new
			      PushbackInputStream(null), new DataInputStream(null)) {};
    }
    public local void bar() {
	i6a i = this.new i6a();
	Object p = new Object() {};
	i6a i2 = this.new i6a(new String(), new IOException(), new
	PushbackInputStream(null), new DataInputStream(null));
	i6a i3 = this.new i6a(new String(), new IOException(), new
			      PushbackInputStream(null), new DataInputStream(null)) {};	
	i6a i4 = this.new i6a() {};
	i6a i5 = new i6a() {};
    }
    public static void main(String[]args) {
	i6 bc = new i6();
	i6a i = bc.new i6a();
	Object p = new Object() {};
	i6a i2 = bc.new i6a(new String(), new IOException(), new
			    PushbackInputStream(null), new DataInputStream(null));
	i6a i3 = bc.new i6a(new String(), new IOException(), new
			    PushbackInputStream(null), new DataInputStream(null)) {};
	i6b myi6b = new i6b();
	i6a i5 = myi6b.new i6a(new String(), new IOException(), new
			       PushbackInputStream(null), new DataInputStream(null));
	i6a i6 = myi6b.new i6a(new String(), new IOException(), new
			       PushbackInputStream(null), new DataInputStream(null)) {};	
    }
}

class i6b extends i6 {}
