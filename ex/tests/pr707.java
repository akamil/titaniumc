// Test normal completion and reachability of try/catch.
// Relevant PR: PR696, PR707
// Currently passes (shouldn't)

class n7 {
  public static void main(String[] args) {
  }
  static int foo(int x) {
    try {
      bar();
      return 1;
    } catch (java.io.IOException e) {
      System.out.println(e);
      return 2;
    }
  }
  static void bar() {
  }
}

