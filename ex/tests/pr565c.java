// Tests inheritance of field by nested class when it extends encloser.
// Expected result: PASS

interface O5 {
  int xxx = 5;
  static class O5a implements O5 {
    O5a() {
      System.out.println(xxx);
    }
  }
  static class O5b {
    O5b() {
      System.out.println(xxx);
    }
  }
}

class O5b {
  public static void main(String[] args) {
    new O5.O5a();
    new O5.O5b();
  }
}
