public class domtest {
public static Object objmethod(Object arg) {
  return arg;
}
public static Domain<2> dommethod(Domain<2> arg) {
  return arg;
}
public static void main(String[]args) {
  RectDomain<2> r1 = [[1,10]:[20,30]];
  RectDomain<2> r2 = [[15,20]:[40,40]];
  Domain<2> d1 = r1 + r2;
  Domain<2> d1b = r1 + r2;
  Domain<2> d2 = r1 - r2;
  Domain<2> d2b = r1 - r2;
  Domain<2> d3 = r1 * r2;
  Domain<2> d3b = r1 * r2;
  Object o1 = new Object();
  Object o2 = new Object();

  o1 = d1;
  o2 = (Object)d2;
  d1 = (Domain<2>)o1;
  d1 = (Domain<2>)o2;

  d1 = dommethod(d1);
  d1 = (Domain<2>)dommethod(d1);
  o1 = dommethod((Domain<2>)o1);
  o1 = (Domain<2>)dommethod((Domain<2>)o1);

  d1 = (Domain<2>)objmethod(d1);
  d1 = (Domain<2>)objmethod((Domain<2>)d1);
  o1 = objmethod((Domain<2>)o1);
  o1 = (Domain<2>)objmethod((Domain<2>)o1);

  d1 = null;
  d1 = (Domain<2>)(Object)null;
}
}
