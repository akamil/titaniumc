// Test overriding with incompatible return types.
// Relevant PR: PR798
// Expected result: FAIL

class C9 implements C9a {
  public static void main(String[] args) {
    String s = new C9b().clone();
    C9b[] c = { new C9().clone() };
    System.out.println("done.");
  }
  public C9 local clone() {
    return this;
  }
  public C9 local localClone() {
    return new C9();
  }
}

interface C9a {
  public C9b clone();
}

class C9b extends C9 {
  public C9b clone() {
    return this;
  }
  public Object local localClone() {
    return new C9b();
  }
}
