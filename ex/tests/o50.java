class a { }
class b extends a { }

class C
{
  a local x1 = new a();
  a x2 = new b();
  a local x3 = new b();
  b y = new b();

  local polyshared a local f(a local x) { return x; }
  local polyshared a f(a x) { return x; }

  a local z1 = f(x1);
  a z2 = f(x2);

  a local x4 = (a local)x2;
  a x5 = (a)x4;
  a x6 = (b)x5;
  a local x7 = (b local)x5;
}
