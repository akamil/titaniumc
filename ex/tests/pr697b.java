// Test method call to overloaded method where there are static and non-static
// candidates.
// Relevant PR: PR697
// Expected result: PASS

class J6 {
  public static void main(String[] args) {
    bar();
  }
  static void foo() {
    J6.bar();
    bar();
  }
  void bar(int x) {}
  static void bar() {}
  void bar(Object y) {}
}

