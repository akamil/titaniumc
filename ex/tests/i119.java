// Test local inference of anonymous arrays.
// Expected result: PASS

class B7 {
  public static single void main(String[] args) {
    String[] x = new String[] {
      broadcast args[0] from 0
    };
  }
}

