// Test explicit throw of unchecked exception before global operation.
// Expected result: PASS
// Relevant PRs: PR824, PR356

class n10a extends Error { }

class n10 {
  static n10a single e1;
  int single f(int single x, int single y) throws n10a
  {
    if (y > 0) throw e1;
    return x;
  }
  public single static void main(String[] args) {
    if (Ti.thisProc() == 0) throw e1;
    else new n10().f(0, 1);
    Ti.barrier();
  }
}
