// Test anonymous classes in field initializers (PR646).
// Expected result: PASS

class H5 {
  static int v = 3;
  static H5 w = new H5() { { System.out.println(v); } };
  H5a<1> x = new H5a<1>() { { System.out.println(a); } };
  H5a<2>.H5b y = new H5a<2>.H5b() { { System.out.println(b); } };
  H5a<3>.H5c z = new H5a<3>().new H5c() { { System.out.println(c); } };
  public static void main(String[] args) {
    new H5();
  }
}

template<int x> class H5a {
  int a = x;
  static class H5b {
    int b = -x;
  }
  class H5c {
    int c = 2 * x;
  }
}
