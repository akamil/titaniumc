// Test static members in anonymous classes.
// Expected result: FAIL

class O2 {
  public static void main(String[] args) {
    System.out.println("here");
    new Object() {
      static { System.out.println("foo"); }
      static int x;
      static void baz() {}
    };
    new O2().bar();
  }
  void bar() {
    new Object() {
      static { System.out.println("bar"); }
      static int x;
      static void baz() {}
    };
  }
}

