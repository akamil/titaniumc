// Test global access of immutables with embedded locals.
// Relevant PR: PR427
// Expected result: FAIL

immutable class x10 {
  Object local x = new Object();
  public static void main(String[] args) {
    x10[1d] xs;
    x10a b;
    x10[] c;
    x10a[1d] single d;
    if (Ti.thisProc() == 0) {
      xs = new x10[0 : 1];
      xs[0] = new x10();
      b = new x10a();
      c = new x10[] { new x10() };
    }
    xs = broadcast xs from 0;
    b = broadcast b from 0;
    c = broadcast c from 0;
    d = new x10a[0 : Ti.numProcs() - 1];
    d.exchange(new x10a());
    x10 e = xs[0];
    e = b.a;
    e = foo().a;
    e = bar().b.a;
    e = new x10a().a;
    e = new x10b().b.a;
    System.out.println(Ti.thisProc() + " " + xs[0].x);
    System.out.println(Ti.thisProc() + " " + b.a.x);
    System.out.println(Ti.thisProc() + " " + c[0].x);
    System.out.println(Ti.thisProc() + " " + d[0].a.x);
    System.out.println(e.x);
    x10[1d] single f = new x10[0 : Ti.numProcs() - 1];
    f.exchange(new x10());
  }
  static x10a foo() {
    return new x10a();
  }
  static x10b bar() {
    return new x10b();
  }
}

class x10a {
  x10 a = new x10();
}

class x10b {
  x10a b = new x10a();
}
