// Test return covariance.
// Relevant PR: PR798
// Expected result: PASS

class B9 implements B9a {
  public static void main(String[] args) throws Exception {
    Object o = new B9();
    o = o.clone();
    B9 b = new B9b();
    b = b.clone();
    B9b c = new B9b();
    c = c.localClone();
    System.out.println("done.");
  }
  public B9 clone() {
    return this;
  }
  public B9 local localClone() {
    return new B9();
  }
}

interface B9a {
  public B9a clone();
}

class B9b extends B9 {
  public B9b local clone() {
    return new B9b();
  }
  public B9b local localClone() {
    return new B9b();
  }
}
