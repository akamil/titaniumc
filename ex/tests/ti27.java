// Test extension of abstract templates.
// Expected result: PASS

class a5 extends a5a<4> {
  public static void main(String[] args) {
    new a5().main();
  }
  public void main() { System.out.println(z); }
}

template<int x> abstract class a5a {
  abstract public void main();
  int z = x;
}
