// test unqualified allocation where enclosing instance should be used
// as qualifier
// expected result: pass
class z1 {
    class z1a {
    }
    class z1b {
	z1b() {
	    System.out.println(new z1a());
	}
    }
    public static void main(String[] args) {
	new z1().foo();
    }
    void foo() {
	System.out.println(new z1b());
    }
}
