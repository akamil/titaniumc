// Tests erroneous uses of qualified super.
// Expected result: FAIL

class v2 extends v2a {
  public static void main(String[] args) {
    new v2().new v2b();
  }
  class v2b {
    v2b() {
      System.out.println(v2.super.blah());
      System.out.println(v2.super.blah);
      System.out.println(v2.super.bah());
      System.out.println(v2.super.bah);
      System.out.println(v2a.super.blah());
    }
  }
  public String blah() {
    return ("v2.blah(): " + toString());
  }
  public int blah = 1;
}

class v2a {
}
