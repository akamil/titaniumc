// Tests qualified super where dynamic type of enclosing instance is subclass
// of required static type.
// Expected result: PASS

class e3 {
  class e3a {
    e3a() {
      System.out.println(e3.super.toString());
    }
  }
  public static void main(String[] args) {
    new e3().new e3a();
    new e3b().new e3a();
  }
  public String toString() {
    return "e3.toString()";
  }
}

class e3b extends e3 {
  public String toString() {
    return "e3b.toString()";
  }
}

