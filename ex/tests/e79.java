class a
{
  native int single f();
  native int f(); // error: illegal overloading

  native int single h(int single x);
  native int h(int x); // error: illegal overloading

  native int g(int single x);
  native int g(int x); // error: illegal overloading
}

class b
{
  native int single f();
  native int g(int single x);
}

class c extends b
{
  native int f(); // error: must be int single
  native int g(int x); // error: must be int single x
}

class e
{
  native single void f();
}

class f extends e
{
  native void f(); // error: must maintain singleness of method
}

class g
{
  native single void f();
  native void f(); // error: illegal overloading
}
