// Test normal completion and reachability of foreach.
// Relevant PR: PR696
// Expected result: FAIL

class k7 {
  public static void main(String[] args) {
  }
  static int foo(int x) {
    foreach (p in (Domain<1>) null) {
      System.out.println(p[0]);
    }
    return 0;
  }
  static int bar(int x) {
    foreach (p in [1:2]) {
      return 1;
    }
  }
}

