// Check array casts and assignment conversions
public class A {
  A[] x = new A[10];
  A[] y = null;
  A[] z = (A[])y;
  Object t1;
  A[] t2 = (A[])t1;
  Cloneable u1;
  A[] u2 = (A[])u1;
}

class B extends A {
}
