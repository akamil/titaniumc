// Test template extending instantiation of itself.
// Expected result: FAIL

class F4 {
  public static void main(String[] args) {
    new F4a<0>();
  }
}

template<int x> class F4a extends F4a<(x < 3 ? x + 1 : -1)> {
}
