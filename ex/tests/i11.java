// test allowed access to private field of class that is neither enclosed 
// or enclosing the accessing class
// expected result: pass
class B1 {
    class B1a {
	B1a() {
	    System.out.println(B1b.x);
	}
    }
    class B1b {
	private static final int x = 3;
    }
    public static void main(String[] args) {
	new B1().new B1a();
    }
}
