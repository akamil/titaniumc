// Test implementing interface that defines method that is incompatible with 
// protected method in Object.
// Relevant PR: PR691
// Expected result: FAIL

class b7 implements b7a {
  public static void main(String[] args) {
  }
}

interface b7a {
  public b7a clone();
}
