// Test normal completion and reachability of try/catch.
// Relevant PR: PR696, PR707
// Expected result: FAIL

class o7 {
  public static void main(String[] args) {
  }
  static void bar() {
  }
  static int baz() {
    try {
      bar();   
      return 1;
    } catch (RuntimeException e) {
      return 3;
    } catch (NumberFormatException e) {
      System.out.println(e);
      return 2;
    } catch (NumberFormatException e) {
      System.out.println(e);
      return 2;
    }
  }
}

