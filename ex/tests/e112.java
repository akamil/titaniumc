public class Number {
  public double val;
  public Number(double d) {
    val = d;
  }
  public Number op-(double d, Object c) { 
    assert this == c;
    return new Number(d - val);
  }
  public Number op-=(double d, Object c) { 
    assert this == c;
    return new Number(d - val);
  }
  public Number op-(double d) { 
    return new Number(val - d);
  }
  public String toString() { return ""+val; }
  public static void main(String[]args) {
    Number n1 = new Number(1.0);
    Number n2 = new dNumber(2.0);
    System.out.println(10 - n1);
    System.out.println(9.0 - n1);
    System.out.println(8.0f - n1);
    System.out.println(7L - n1);
    System.out.println(n1.op-(6L,n1));
    System.out.println(n1 - 0.5);
    System.out.println(n1 - 2);


    System.out.println(8.0f - n2);
    System.out.println(7 - n2);
    System.out.println(n2 - 2.5);
    System.out.println(n2 - 10);

    double d = 10.0;
    double e;
    d -= n1;
    System.out.println(d);
    System.out.println(d -= n1);
    System.out.println(d -= n1);
    System.out.println(e = (d -= n1));
    System.out.println(e);
    System.out.println(d -= n1);
  }
}
public class dNumber extends Number {
  public dNumber(double val) { super(val); }
  public Number op-(double d, Object c) { 
    assert this == c;
    System.out.println("hi");
    return new Number(d - val);
  }
  public Number op-(double d) { 
    System.out.println("there");
    return new Number(val - d);
  }
}

