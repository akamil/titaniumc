// Test class passing itself as actual to template superclass.
// Expected result: PASS

class Q5 extends Q5a<Q5> {
  Q5b<String> x;
  public static void main(String[] args) {
  }
}

template<class x> class Q5a {
}

template<class x> class Q5b {
}
