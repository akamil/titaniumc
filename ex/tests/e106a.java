template < boolean v, int x > class Cons
{
}


class StackOfInt
{
  static final int one = 1;
  static final int two = 2;
    static public void main( String[] args )
    {
        Cons < false, one+two > list1; // works
        Cons < one > two , 1 > list2; // fails - unavoidable
        Cons < one < two , 1 > list3; // fails - unavoidable
        Cons < false, one|two > list4; // fails - fixable

        // parenthesized expressions always work:
        Cons < false, (one+two) > list1b;
        Cons < (one > two), 1 > list2b;
        Cons < (one < two), 1 > list3b;
        Cons < false, (one|two) > list4b;
    }
}
