// test qualified allocation of inner class
// expected result: pass
class r1 {
  public static void main(String[] args) {
    System.out.println(new r1a().new r1b());
  }
}

class r1a {
  class r1b {
  }
}
