// Test extension of nested types in templates.
// Expected result: PASS

class y4 extends y4b<y4a.y>.y4c {
  public static void main(String[] args) {
  }
}

class y4a extends y4d<y4d<y4d<y4b<6>.y4c>.y4e>.y4e>.y4e {
  final static int y = 4;
}

template<int x> class y4b {
  static class y4c {
  }
}

template<class x> class y4d {
  static class y4e {
  }
}
