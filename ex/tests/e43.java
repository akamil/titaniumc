class e1 extends Throwable { }
class e2 extends e1 { }

class a
{
  void f()
  {
    try { throw new e1(); } catch (e2 x) { ; }
  }
}
