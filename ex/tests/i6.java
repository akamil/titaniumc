// test nested class inside interface
// expected result: pass
class w1 {
    public static void main(String[] args) {
	System.out.println(new w1a.w1b());
    }
}

interface w1a {
    class w1b {
    }
}
