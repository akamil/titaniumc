// Test folding of fields of class that is not method ready. See PR661.
// Expected result: PASS

class W5 {
  public static final int val2 = 3;
  public static final int arity = val2;
  public static void foo(template W5a<arity> rd_result) { }
  public static void main(String[] args) { }
}

template <int blah>
class W5a {
}
