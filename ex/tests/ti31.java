// Test local and anonymous extension of multi-parametered templates.
// Expected result: PASS

class e5 {
  public static void main(String[] args) {
    class e5b extends e5a<3, Throwable> { { foo(); } }
    System.out.println(new e5b());
    System.out.println(new e5a<4, Object>() {}.foo());
  }
}

template<int x, class y> class e5a {
  e5a foo() { System.out.println(new y() {}); return this; }
}
