// Lots of array index/decl/alloc parsing errors
class T {
  static final int N = 2;   // manifest constant

  int[1] b1;
  int[1,2] b2;
  int[1.d] b3;
  int[1e2] b4;
  int[1.] b5;

  void f() {
    int[] aa;
    
    aa[] = 1;
    aa local = 1;
    aa local [1] = 1;
    aa[1] local = 2;
    aa[1 e] = 3;
    aa[1 D] = 4;
    aa[1 d] = 5;
  }

  void g() {
    new int[1d];
    new int[1d][10];
    new int[5,3];
    new int[10][1.];
    new int[10][(T)d];
    new int[10][M d];
  }

}
