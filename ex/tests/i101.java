// Tests unqualified enclosing class access requiring disambiguation.
// Expected result: PASS

class t4 {
  static class t4b {
    { foo(); }
  }
  public static void main(String[] args) {
    new t4b();  
  }
  static void foo() {}
  static void foo(Object o) {}
}
