// Test implementing interface that has method with template arg (PR444).
// Expected result: PASS

public class i5 implements i5c {
 public void aMethod(@i5a<int> a_bld) {}
 public void aMethod(@i5a<int>.i5b a_bld) {}
 public static void main(String[]args) {
   i5 a = new i5();
 }
}

template <class T>
class i5a {
  static class i5b {}
}

interface i5c {
 public void aMethod(@i5a<int> a_bld);
 public void aMethod(@i5a<int>.i5b a_bld);
}
