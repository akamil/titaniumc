// Test illegal modifiers on nested and local classes.
// Expected result: FAIL

class u5 {
  single class u5a {}
  sglobal class u5b {}
  native class u5c {}
  synchronized class u5d {}
  transient class u5e {}
  volatile class u5f {}
  local class u5g {}
  nonshared class u5h {}
  polyshared class u5i {}
  inline class u5j {}
  public static void main(String[] args) {
    public class u5k {}
    immutable class u5l {}
  }
}
