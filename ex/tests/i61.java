// Test accessibilty of enclosing instance field from static local class.
// Expected result: FAIL

class i3 {
  public static void main(String[] args) {
    class x { {System.out.println(y);} }
    new x();
  }
  int y = 7;
}
