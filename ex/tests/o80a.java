class TestMin {
  static final int N = 2;   // manifest constant

  public static void main( String[] args ) {
    Point<2> p = Point<2>.all(1);
    RectDomain<2> D = [p:p];
    Point<2> q = D.min();
    System.out.println("min " + q);
  }
}
