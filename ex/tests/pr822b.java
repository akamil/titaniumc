// Test covariance of Domain<N> return types.
// Relevant PR: PR822
// Expected result: FAIL

class X9 extends X9a {
  Domain<1> foo() {
    return null;
  }
  Object bar() {
    return foo();
  }
  public static void main(String[] args) {
  }
}

class X9a {
  Domain<2> foo() {
    return null;
  }
  Domain<2> bar() {
    return foo();
  }
}

