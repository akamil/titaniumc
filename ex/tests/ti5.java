// Test template extending parameter.
// Expected result: PASS

class B4 {
  String y = "B4";
  public static void main(String[] args) {
    System.out.println(new B4a<B4>());
  }
}

template<class x> class B4a extends x {
  public String toString() { return y; }
}
