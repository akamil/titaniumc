// Test method calls on templates and nested classes in templates.
// Expected result: PASS

class f4 {
  public static void main(String[] args) {
    System.out.println(f4a<5>.f4b.f4c.x1());
    System.out.println(f4a<3>.f4b.y1().x1());
    System.out.println(f4a<2>.w1().y1().x1());
    System.out.println(f4a<f4a<11>.u>.f4b.y1().x1());
    System.out.println(f4a<f4a<7>.f4b.f4c.x>.f4b.y1().x1());
    System.out.println(f4d<f4a<1>.f4b.f4c>.x1().x1());
  }
}

template<int z> class f4a {
  static class f4b {
    static class f4c {
      final static int x = z + 1;
      static int x1() { return x; }
    }
    static f4c y = new f4c();
    static f4c y1() { return y; }
  }
  static f4b w = new f4b();
  static f4b w1() { return w; }
  final static int u = z * 2;
  static int u1() { return u; }
}

template<class v> class f4d {
  static v x = new v();
  static v x1() { return x; }
}
