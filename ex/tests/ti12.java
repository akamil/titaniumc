// Test class passing value member of itself as actual to template superclass.
// Expected result: FAIL

class I4 extends I4a<I4.x> {
  public static void main(String[] args) {
  }
  static final int x = 4;
}

template<int x> class I4a {
}

