immutable class ThisIsImmutable {
  int x;
  public ThisIsImmutable(int new_x) {
    x = new_x;
  }
  public ThisIsImmutable() {
    x = 1;
  }
  public int get() {
    return x;
  }
}

class Tester {

  public static void main(String[] args) {
    // ThisIsImmutable t;
    ThisIsImmutable t = new ThisIsImmutable(2);
    // ThisIsImmutable t = ThisIsImmutable(2);

    System.out.print("Number is ");
    System.out.println(t.get());
  }
}
