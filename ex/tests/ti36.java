// Test template extending instantiation of itself.
// Expected result: FAIL                                   

class t5 {
  public static void main(String[] args) {
    new t5a<0>();
  }
}

template<int x> class t5a extends t5b<t5a<x + 1>> {
}

template<class x> class t5b {
}
