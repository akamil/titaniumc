// Test inheriting incompatible methods with same parameters from different
// supertypes.
// Relevant PR: PR695
// Expected result: FAIL

abstract class C6 extends C6a implements C6b {
  public static void main(String[] args) {
  }
  void bar(String arg) {
    foo(arg);
  }
}

abstract class C6a {
  public abstract int foo(String s);
}

interface C6b {
  public void foo(String s);
}

