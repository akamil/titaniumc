// Test normal completion and reachability of for.
// Relevant PR: PR696
// Expected result: FAIL

class i7 {
  public static void main(String[] args) {
  }
  static int foo(int x) {
    for (; x < 0;) {
      return 0;
    }
    return 1;
  }
  static int bar() {
    for (; true;);
    return -1;
  }
  static int baz() {
    for (; true;) {
      System.out.println("hi");
      break;
    }
  }
  static int foobar(int x) {
    for (; false;) return 0;
    return 2;
  }
  static int foobaz(int x) {
    for (;;) if (x < 3) return 3;
  }
}

