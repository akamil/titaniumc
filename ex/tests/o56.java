immutable class A
{
  int x;

  A() { x = 2; }
}

class B
{
  A t;
  native local polyshared A f();

  int z = t.x + f().x;
}
