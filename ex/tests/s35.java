// Test single operation in non-single switch.
// Expected result: FAIL

class m10 {
  public single static void main(String[] args) {
    switch (Ti.thisProc()) {
    case 0: Ti.barrier();
    case 1: Ti.barrier();
    }
    switch (Ti.thisProc()) {
    case 0: Ti.barrier();
    }
  }
}
