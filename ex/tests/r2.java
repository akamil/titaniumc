// Test reachability of statements after continue and throw.
// Relevant PR: PR696
// Expected result: FAIL

class P6 {
  public static void main(String[] args) {
  }
  static int foo() {
    throw new Exception();
    return 2;
  }
  static void bar(int x) {
    while (x < 2) {
      System.out.println(x);
      continue;
      x += 1;
    }
  }
}

