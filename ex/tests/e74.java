class a
{
  local void x() { }
  local int x() { return 0; }
}

class b
{
  local void x() { }
}

class c extends b
{
  local int x() { return 0; }
}
