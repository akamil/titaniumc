// Tests anonymous arrays with mismatched types.
// Expected result: FAIL

class u2 {
  public static void main(String[] args) {
    System.out.println(new int[] {"a", "b"});
    System.out.println(new int[] {'a', 'b'});
    System.out.println(new Object[] {"a", "b"});
    System.out.println(new String[][] {{"a"},{args}});
  }
}
