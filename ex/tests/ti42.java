template<class C> 
class barf {
public static C c;
public static void foo(C c) {}
} 

template<int v> 
class blah {
public static final int x = v;
}

template<int x>
class foo {
public static barf<blah<x>> b;
}

public class badrd {
public static foo<1> z;
public static void main(String[]args) {}
}
