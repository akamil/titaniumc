@< int arg > class CycleA
{
    static  CycleB< arg > value;
}


@< int arg > class CycleB
{
    static  CycleA< arg > value;
}


class Main
{
    static public void main( String[] args )
    {
	System.out.println(  CycleA< 0 >.value );
    }
}
