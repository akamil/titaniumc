// Test unqualified inner class allocation from local method.
// Relevant PR: PR652
// Expected result: PASS

public class f6 {
  class f6a {}
  public local void bar() {
    new f6a();
    this.new f6a();
    Object p = new Object() {};
  }
  public static void main(String[]args) {
  }
}
