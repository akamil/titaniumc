// Test accessibility rules for zero-argument immutable constructors.
// Relevant PR: PR791
// Expected result: FAIL

public immutable class Z8 {
  Z8() {}
  public static void main(String[] args) {
  }
  private static immutable class Z8a {
    Z8a() {}
  }
  protected static immutable class Z8b {
    Z8b() {}
  }
  static immutable class Z8c {
    private Z8c() {}
  }
  protected static immutable class Z8d {
    public Z8d() {}
  }
  static immutable class Z8e {
    protected Z8e() {}
  }
  static immutable class Z8f {
    Z8f() {}
  }
}


