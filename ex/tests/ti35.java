// Test error messages in template interfaces.
// Expected result: FAIL

class s5 implements s5a<0> {
  public static void main(String[] args) {
  }
}

template<int x> interface s5a {
  static final int z = "z";
}

