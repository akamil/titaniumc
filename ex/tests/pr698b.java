// Check illegal assignment of blank finals.
// Relevant PR: PR698
// Expected result: FAIL

class I6 {
  public static void main(String[] args) {
    x = 6;
    System.out.println(x);
  }
  static final int x;
  final int y;
  { x = 4; }
  final static int z;
  int w = z = 5;
  I6() { x = 7; }
  void foo() { y = 8; }
}
