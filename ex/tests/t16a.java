class Derived implements Base< Derived.value >
{
    static final int value = 0;
    public int foo() { return bar; }
}


@< int val > interface Base
{
  public static int bar = val;
  public int foo();
}


class Main
{
    static public void main( String[] args )
    {
	System.out.println( Derived.value );
    }
}
