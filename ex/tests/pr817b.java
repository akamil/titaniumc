// Test singleness of array allocations.
// Relevant PR: PR797, PR817
// Expected result: FAIL

class T9 {
  public static single void main(String[] args) {
    int[] single [] is = new int[Ti.thisProc()][Ti.thisProc()];
    int[1d] single [2d] ts = new int[0 : Ti.thisProc()][[0,0] : [Ti.thisProc(),1]];
    int single [] js = { Ti.thisProc() };
    int single [] local single ks = new int single [] { Ti.thisProc() };
    if (js[0] < 1) Ti.barrier();
    if (is.length < 2) Ti.barrier();
    System.out.println("done.");
  }
}

