class a
{
  single void barrier() { }
  RectDomain<1> single d1;
  RectDomain<1> d2;

  single void f()
  {
    foreach (p1 in d1, p2 in d2) barrier();
  }
}
