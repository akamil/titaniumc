// Test template inheritance with interfaces.
// Expected result: PASS

class X4 {
  public static void main(String[] args) {
    System.out.println(new X4f());    
    System.out.println(new X4g());
    System.out.println(new X4h());
  }
}

template<class x> interface X4a {
  String v = "X4a.v";
}

template<int x> interface X4b {
  int u = x;
}

class X4c {
  class X4d {
    static final int y = 8;
  }
  static final int z = X4e.w;
}

class X4e {
  static final int w = X4c.X4d.y + 1;
}

class X4f implements X4a<X4c.X4d>, X4b<X4c.z + 3> {
  public String toString() {
    return super.toString() + " " + v + " " + u;
  }
}

class X4g implements X4b<X4c.z> {
  public String toString() {
    return super.toString() + " " + u;
  }
}

class X4h implements X4b<X4c.X4d.y> {
  public String toString() {
    return super.toString() + " " + u;
  }
}


