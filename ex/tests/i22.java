// test qualified allocation of inner class where qualifier is nested class
// expected result: pass
class N1 {
    public static void main(String[] args) {
	System.out.println(new N1a.N1b().new N1c());
    }
}

class N1a {
    static class N1b {
	class N1c {
	}
    }
}
