// Test normal completion and reachability of labeled statements.
// Relevant PR: PR696
// Expected result: FAIL

class l7 {
  public static void main(String[] args) {
  }
  static int foo(int x) {
    OUTER: while (true) {
      while (true) {
        break OUTER;
      }
    }
    return 0;
  }
  static int bar(int x) {
    OUTER: while (true) {
      while (true) {
        continue OUTER;
      }
    }
    return 1;
  }
  static int baz(int x) {
    return x;
    OUTER: while (true);
  }
}

