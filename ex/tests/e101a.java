immutable class EmbedsLocal
{
    Object local reference;
}


immutable class EmbedsGlobal
{
    Object reference;
}


immutable class EmbedsEmbedsLocal
{
    EmbedsLocal embedded;
}


immutable class EmbedsEmbedsGlobal
{
    EmbedsGlobal embedded;
}


template< class Element > class Test
{
    static Element element;
    static Element [1d] local proximate;
    static Element [1d] distant;

    static
    {
	proximate.copy( proximate );
	proximate.copy( distant );
	distant.copy( proximate );
	distant.copy( distant );
	
	proximate.exchange( element );
	distant.exchange( element );
    }
}


class Tests
{
    static single public void main( String[] args )
    {
	// basic elements
	{ Test< Object local > test; }
	{ Test< Object > test; }
	{ Test< EmbedsLocal > test; }
	{ Test< EmbedsGlobal > test; }
	{ Test< EmbedsEmbedsLocal > test; }
	{ Test< EmbedsEmbedsGlobal > test; }
	    
	// local array elements
	{ Test< Object local [1d] local > test; }
	{ Test< Object [1d] local > test; }
	{ Test< EmbedsLocal [1d] local > test; }
	{ Test< EmbedsGlobal [1d] local > test; }
	{ Test< EmbedsEmbedsLocal [1d] local > test; }
	{ Test< EmbedsEmbedsGlobal [1d] local > test; }
	    
	// global array elements
	{ Test< Object local [1d] > test; }
	{ Test< Object [1d] > test; }
	{ Test< EmbedsLocal [1d] > test; }
	{ Test< EmbedsGlobal [1d] > test; }
	{ Test< EmbedsEmbedsLocal [1d] > test; }
	{ Test< EmbedsEmbedsGlobal [1d] > test; }
    }
}
