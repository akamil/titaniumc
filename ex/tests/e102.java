immutable class Unboxed {
    Object local object;
}

public class Bad {
    public static single void main(String [] args) {
	Unboxed remote = broadcast new Unboxed() from 0;
    }
}
