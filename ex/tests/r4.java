// Test reachability of statements after break and return.
// Relevant PR: PR696
// Expected result: FAIL

class g7 {
  public static void main(String[] args) {
    return;
    int i = args.length;
    System.out.println(i);
    return;
  }
  static void foo(String[] args) {
    switch (args.length) {
    case 0:
      break;
      System.out.println(0);
      foo(null);
      return;
    default:
    }
  }
  static void bar(int x) {
    while (x < 2) {
      System.out.println(x);
      break;
      x += 1;
    }
  }
}
