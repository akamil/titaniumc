interface i { }
class a implements i { }
class b extends a { }

class c {
  a x1[] = new b[10];
  i x2[] = new b[10];
  i x3[] = new a[10];
  a x4[] = new a[5];
  a x5[] = (a[])(new i[5]);
}
