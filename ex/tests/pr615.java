// Test delayed type resolution for local class type.
// Expected result: PASS

class o3 {
  public static void main(String[] args) {
    class x { static final int y = 3; }
    System.out.println(x.y);
    { System.out.println(x.y); }
    class z { { System.out.println(x.y); } }
    new z();
  }
}

