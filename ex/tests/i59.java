// Test accessibilty of local class from anonymous class.
// Expected result: PASS

class g3 {
  public static void main(String[] args) {
    class x {}
    new Object() { {System.out.println(new x());} };
  }
}

