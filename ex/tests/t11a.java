@< int arg > class CycleA
{
  static final int value =  CycleB< arg >.basis + 1;
  static final int basis = arg;
}


@< int arg > class CycleB
{
  static final int value =  CycleA< arg >.basis + 1;
  static final int basis = arg;
}


class Main
{
  static public void main( String[] args )
    {
      System.out.println(  CycleA< 0 >.value );
    }
}
