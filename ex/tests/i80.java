// Tests inheritance of nested classes inside templates.
// Expected result: PASS
template<class x> class P3 {
    class P3a extends x {
	{System.out.println(new x());}
    }
    static class P3b extends x {
	{System.out.println(new x());}
    }
}

class P3d {
  public String toString() {
    return "P3d.toString()";
  }
}

class P3e {
  public String toString() {
    return "P3e.toString()";
  }
}

class P3c {
    public static void main(String[] args) {
	System.out.println(new P3<P3d>().new P3a());
	System.out.println(new P3<P3e>().new P3a());
	System.out.println(new P3<P3d>.P3b());
	System.out.println(new P3<P3e>.P3b());
    }
}
