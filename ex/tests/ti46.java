template <class D, class RD, class PT>
class td {
  public static final int Darity = D.arity;
  public static D d1;
  public static RectDomain<Darity> d2;
  public static RectDomain<D.arity> d3;
  public static Domain<Darity> d4;
  public static Domain<D.arity> d5;
  public static Point<Darity> d6;
  public static Point<D.arity> d7;
  public static void check(Domain<Darity> rd_result) { }
  public static void check2(Domain<D.arity> rd_result) { }

  public static final int RDarity = RD.arity;
  public static RD r1;
  public static RectDomain<RDarity> r2;
  public static RectDomain<RD.arity> r3;
  public static Domain<RDarity> r4;
  public static Domain<RD.arity> r5;
  public static Point<RDarity> r6;
  public static Point<RD.arity> r7;
  public static void check(RectDomain<RDarity> rd_result) { }
  public static void check2(RectDomain<RD.arity> rd_result) { }

  public static final int PTarity = PT.arity;
  public static PT p1;
  public static RectDomain<PTarity> p2;
  public static RectDomain<PT.arity> p3;
  public static Domain<PTarity> p4;
  public static Domain<PT.arity> p5;
  public static Point<PTarity> p6;
  public static Point<PT.arity> p7;
  public static void check(Point<PTarity> rd_result) { }
  public static void check2(Point<PT.arity> rd_result) { }
}
public class mainer {
  public static void main(String[]args) {
#define TI(N) \
  Domain<N> d##N; \
  RectDomain<N> r##N; \
  Point<N> p##N; \
        td<Domain<N>, RectDomain<N>, Point<N>>.check(d##N); \
        td<Domain<N>, RectDomain<N>, Point<N>>.check2(d##N); \
        td<Domain<N>, RectDomain<N>, Point<N>>.check(r##N); \
        td<Domain<N>, RectDomain<N>, Point<N>>.check2(r##N); \
        td<Domain<N>, RectDomain<N>, Point<N>>.check(p##N); \
        td<Domain<N>, RectDomain<N>, Point<N>>.check2(p##N); \

  TI(1)
  TI(2)
  TI(3)
  }
}
