// Test implementation of nested types in templates.
// Expected result: PASS

class Y4 implements Y4b<Y4a.y>.Y4c, Y4d<Y4a>.Y4e {
  public static void main(String[] args) {
  }
}

class Y4a implements Y4d<Y4d<Y4d<Y4b<6>.Y4c>.Y4e>.Y4e>.Y4e {
  final static int y = 4;
}

template<int x> class Y4b {
  interface Y4c {
  }
}

template<class x> class Y4d {
  interface Y4e {
  }
}
