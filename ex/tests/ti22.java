// Test extending template that requires qualified primitive as parameter.
// Expected result: PASS

class V4 extends V4a<4> {
  public static void main(String[] args) {
    System.out.println(foo());
  }
}

template<int single x> class V4a {
  static String foo() { return "" + x; }
}

