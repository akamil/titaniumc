// Test disambiguation of template qualified names.
// Expected result: FAIL

class e4 {
  public static void main(String[] args) {
    e4a<4>.e4b.e4c.x c = new e4a<4>.e4b.e4c.x();
    e4a<6>.e4b.e4c.x[] d = new e4a<6>.e4b.e4c.x[] { null, null, null };
    e4a<6>.e4b.e4c[] e = new e4a<19>.e4b.e4c[3];
    System.out.println(e4a<5>.e4b.e4c);
    System.out.println(e4a<3>.e4b);
    System.out.println(e4a<2>.w.e4c.x);
    System.out.println(e4a<e4a<11>.u>.e4b);
    System.out.println(e4a<e4a<7>.e4b.e4c>.e4b);
    System.out.println(e4d<e4a<1>.e4b.e4c.x>.x);
    System.out.println(c.x);
  }
}

template<int z> class e4a {
  static class e4b {
    static class e4c {
      final static int x = z + 1;
    }
    static e4c y = new e4c();
  }
  static e4b w = new e4b();
  final static int u = z * 2;
}

template<class v> class e4d {
  static v x = new v();
}
