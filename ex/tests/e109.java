template <int N, int P, class PTN>
class T {
public static void bar() {
Domain<N> D;
RectDomain<N> RD;
Point<N> p_;
Point<P> p2_;
Point<(P+0)*2/2+P-P> px;
foreach (p in D) { p_ = p; }
foreach (p in RD) { p_ = p; }
foreach (Point<P> p in D) { p_ = p; }
foreach (Point<P> p in RD) { p_ = p; }
// broken
//foreach (PTN p in D) { p_ = p; }
//foreach (PTN p in RD) { p_ = p; }
foreach (Point<(P+0)*2/2+P-P> p in D) { p_ = p; }
foreach (Point<(P+0)*2/2+P-P> p in RD) { p_ = p; }
}}

public class foo {
public static Point<1> p1;
public static Point<2> p2;
public static Point<3> p3;
public static void main(String[]args) {
Domain<1> D1;
RectDomain<1> RD1;
Domain<2> D2;
RectDomain<2> RD2;
Domain<3> D3;
RectDomain<3> RD3;

foreach (p in D1) { p1 = p; }
foreach (p in RD1) { p1 = p; }
foreach (Point<3> p in D1) { p1 = p; }
foreach (Point<3> p in RD1) { p1 = p; }

foreach (p in D2) { p2 = p; }
foreach (p in RD2) { p2 = p; }
foreach (Point<1> p in D2) { p2 = p; }
foreach (Point<1> p in RD2) { p2 = p; }

foreach (p in D3) { p3 = p; }
foreach (p in RD3) { p3 = p; }
foreach (Point<2> p in D3) { p3 = p; }
foreach (Point<2> p in RD3) { p3 = p; }

T<1,2,Point<3>> t1;
T<2,1,Point<3>> t2;
T<3,2,Point<1>> t3;
}}
