// Tests compile-time constants in anonymous classes.
// Currently fails (shouldn't)

class K2 {
  public static void main(String[] args) {
    final int z = 3;
    final int x = 2;
    //x = 1;
    new Object() {
      static final int y = x + 1;
      { System.out.println(x); }
      { System.out.println(y); }
    };
  }
}

