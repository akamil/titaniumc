template <class T, class S> class list {
      
   private T k;   
   private S d;
   private template list<T, S> t;

   list(T key, S data, list tail) {
      k = key;
      d = data;
      t = tail;
   }

   template list<T, S> tail() {
      return t;
   }

   T key() {
      return k;
   }

   S data() {
      return d;
   }

}

class test {
   int testFuntion() [] {
      template list<int, String> l =
         new list(1, "one",
                  new list(2, "two",
                           new list(3, "three", null)));
/*
      template list<int, String> l =
         new template list<int, String>(1, "one",
                  new template list<int, String>(2, "two",
                           new template list<int, String>(3, "three",
                                                          null)));
*/
      return new int[3];
   }

   static public single void main(String single [] single args) {
      System.out.println("Hello World!");
   }

}
