class Derived extends @Base< Derived.value >
{
    static final int value = 0;
}


@< int ignored > class Base
{
}


class Main
{
    static public void main( String[] args )
    {
	System.out.println( Derived.value );
    }
}
