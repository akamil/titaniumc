// test access to enclosing instance's field from static context
// expected result: fail
class y1 {
    class y2 {
	static final int x = xx;
    }
    int xx;
}
