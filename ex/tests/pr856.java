class w10 {
  public static void main(String[] args) {
    Object[1d] i = new Object[0 : 2];
    i.set(new Object()); // (1) fails with lqi  
    Object local[1d] j = new Object local[0 : 2]; 
    j.set(new Object()); // (2) fails with and without lqi - erroneous code
    Object local[1d] local k = new Object local[0 : 2];
    k.set(new Object()); // (3) works
    Object[1d] local l = new Object[0 : 2];
    l.set(new Object()); // (4) works
    Object[1d] m = broadcast new Object[0 : 2] from 0;
    m.set(new Object()); // also used to fail with lqi  
    System.out.println("done.");
  }
}
