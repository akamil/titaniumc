// Tests using array allocations as statements.
// Currently passes (shouldn't) (PR563)

class v2 {
  public static void main(String[] args) {
    new int[4];
    new int[] {1,2,3};
  }
}
