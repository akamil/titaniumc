// Test overriding a single/sglobal method with a non-sglobal method.
// Relevant PR: PR687
// Expected result: PASS (FAIL with -noinfer-sglobal)

class b8 {
  single void foo() {}
}

class b8a extends b8 {
  void foo() {
    Ti.barrier();
  }
}

class b8c extends b8 {
  void foo() {}
}

class b8d extends b8a {
  void foo() {}
}

class b8b {
  public static void main(String[] args) {
    b8 single x = new b8a();
    x.foo();
  }
}

