// Test field access with unqualified enclosing class access.
// Expected result: PASS

class s4 {
  public static void main(String[] args) {
    new s4a().new s4b();
  }
  s4c yyy = new s4c();
}

class s4c {
  int www = 4;
  void www() { System.out.println("www"); }
}

class s4a extends s4 {
  static s4c zzz = new s4c();
  class s4b {
    { 
      System.out.println(yyy.www); 
      yyy.www();
      System.out.println(zzz.www);
      zzz.www(); 
    }
  }
}
