immutable class foo
{
  final int x;

  foo() { x = 0; }
  foo(int y) { this.x = y; }
}
