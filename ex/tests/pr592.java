// Tests attempt to access field of template basis (PR592).
// Expected result: FAIL

class W2 {
  public static void main(String[] args) {
    System.out.println(W2a.xxx);
  }
}

template<class T> class W2a {
  int xxx = 0;
}
