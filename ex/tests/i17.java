// test qualified allocation with primitive type as qualifier
// expected result: fail
class H1 {
    class H1a {
    }
    public static void main(String[] args) {
	int xxx;
	xxx.new H1a();
    }
}
