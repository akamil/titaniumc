// Test interface defining method that is incompatible with public method 
// in Object.
// Relevant PR: PR691
// Expected result: FAIL

class Z6 {
  public static void main(String[] args) {
    Z6a x = null;
    x = x.equals(new Object());
  }
}

interface Z6a {
  public Z6a equals(Object o);
}
