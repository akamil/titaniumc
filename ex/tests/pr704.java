// Test a nested subtype calling a private method in its enclosing supertype.
// Relevant PR: PR704
// Expected result: PASS

class d7 {
  private Object foo() { return new Object(); }
  class d7a extends d7 {
    d7a() {
      Object o = foo();
      System.out.println(o);
    }
  }
  public static void main(String[] args) {
    new d7().new d7a();
  }
}

