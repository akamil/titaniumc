// Test typechecking rules for ? expressions.
// Relevant PR: PR713
// Expected result: FAIL

class L7 {
  public static void main(String[] args) {
    Object local [] x;
    Object [] y;
    Object z = args.length > 0 ? x : y;
  }
}
