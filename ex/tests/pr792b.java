// Test single broadcast in anonymous array init.
// Relevant PR: PR792
// Expected result: PASS

class X8 {
  public static single void main(String[] args) {
    int a[] = new int[] {broadcast Ti.thisProc() from 0};
    System.out.println(a[0]);
  }
}

