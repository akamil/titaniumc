// Test normal completion and reachability of do.
// Relevant PR: PR696
// Expected result: FAIL

class j7 {
  public static void main(String[] args) {
  }
  static int foo(int x) {
    do {
      x += 2;
    } while (x == -1);
    return 1;
  }
  static int bar(int x) {
    while (x != 2) {
      do {
        System.out.println(x);
        continue;
      } while (x == -1);
    }
    return 3;
  }
  static int baz(int x) {
    OUTER: do {
      while (true) {
        System.out.println(x);
        continue OUTER;
      }
    } while (x == -1);
    return 4;
  }
  static int foobar(int x) {
    do {
      x += 2;
      break;
    } while (true);
    return 5;
  }
  static int foobaz(int x) {
    do {
      while (true) {
        x += 2;
        continue;
      }
    } while (x == -1);
    return 6;
  }
}

