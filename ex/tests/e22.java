interface i1 {
  void f();
  static void g();
  final void h();
}

class i2 {
  abstract void f1() { }
  native void f2() { }
  native abstract void f3();
  void f4();

  abstract final void f5();
}
