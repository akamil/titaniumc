// Test qualfied super when enclosing class is immutable.
// Expected result: FAIL

immutable class n3 {
  class n3a { { System.out.println(n3.super.toString()); } }
  n3() { System.out.println(super.toString()); }
  public static void main(String[] args) {
    new n3().new n3a();
  }
  public String toString() { return "n3.toString()"; }
}

