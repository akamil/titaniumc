class a
{
  SharedRegion p1 = new SharedRegion();
  PrivateRegion p2 = new PrivateRegion();
  Region x1 = p1;
  Region x2 = p2;
  a v1 = new a();
  a v2 = new (p1) a();
  a v3 = new (p2) a();
  a[] va1 = new a[10];
  a[] va2 = new (p1) a[10];
  a[] va3 = new (p2) a[10];
  a[1d] vb1 = new a[0 : 10];
  a[1d] vb2 = new (p1) a[0 : 10];
  a[1d] vb3 = new (p2) a[0 : 10];
};
