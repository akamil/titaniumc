// test qualified allocation with qualified typename
// expected result: fail
class E1 {
    public static void main(String[] args) {
	new E1a().new E1b.E1c();
    }
}

class E1a {
    class E1b {
	class E1c {
	}
    }
}
