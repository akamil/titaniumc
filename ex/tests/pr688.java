// Test assigning anonymous class to final variable.
// Relevant PR: PR688
// Expected result: PASS

class l6 {
  public static void main(String[] args) {
    final l6 x = new l6() {};
  }
}

