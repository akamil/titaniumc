// test name disambiguation between field and inner class
// currently fails (shouldn't)
class t1 {
    class t1a {
	//final static int x = 3;
    }
    static t1b t1a = new t1b();
    public static void main(String[] args) {
	System.out.println(t1a.x);
    }
}

class t1b {
    int x = 4;
}
