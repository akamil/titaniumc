// Test template that extends nested class.
// Expected result: PASS

class X3 {
  static class X3a {
    static class X3c {}
  }
  public static void main(String[] args) {
    new X3b<Object>();
  }
}

template<class x> class X3b extends X3.X3a {
  { System.out.println(new X3c()); }
}
