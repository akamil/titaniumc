// Test inheritance of nested types.
// Expected result: PASS

class y3 extends y3a {
  public static void main(String[] args) {
    System.out.println(new y3b());
  }
}

class y3a {
  static class y3b {
  }
}

