// test access checking for member classes
// expected result: pass
class P1 {
    public static void main(String[] args) {
	P1a.P1b x = new P1a.P1b();
	System.out.println(x);
    }
}

class P1a {
    protected static class P1b {
    }
}
