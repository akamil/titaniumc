// Test qualified anonymous classes with templates inside.
// Expected result: PASS

class z5 {
  public static void main(String[] args) {
    new z5().new z5a() { { System.out.println(new z5b<4>()); } };
  }
  class z5a {
  }
}

template<int x> class z5b {
}
