// Error, cf PR#24
immutable class Immutable
{
  Object single reference;
  static Immutable single result = broadcast new Immutable() from 0;
}
