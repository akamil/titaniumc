// Tests static nested classes inside value-based templates.
// Expected result: PASS
template<int x, int y> class R3 {
    static class R3b {
	{System.out.println(x + " " + y);}
    }
}

class R3c {
    public static void main(String[] args) {
	System.out.println(new R3<1,2>.R3b());
	System.out.println(new R3<3,R3d.x>.R3b());
    }
}

class R3d {
  public static final int x = 4;
}
