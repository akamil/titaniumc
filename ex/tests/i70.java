// Test shadowing of field of enclosing class by final local var when in local 
// class.
// Expected result: PASS

class w3 {
  int y = 3;
  public static void main(String[] args) {
    final int y = 2;
    class w3a {
      { System.out.println(y); }
    }
    new w3a();
  }
}

