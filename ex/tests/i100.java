// Tests using templates and template qualified names with qualified this
// and super.
// Expected result: PASS

class n4 {
  class n4b {
  }
  public static void main(String[] args) {
    System.out.println(new n4a<5>().new n4b().new n4c());
    //System.out.println(new n4a<6>());
  }
}


template<int x> class n4a {
  public String toString() { return n4a.class.toString(); }
  class n4b {
    public String toString() { return n4b.class.toString(); }
    class n4c {
      { 
        System.out.println(n4a.this);
        System.out.println(n4a<x>.this);
        System.out.println(n4a<x>.n4b.this);
        System.out.println(n4a.n4b.this);
        System.out.println(n4a.super.toString());
        System.out.println(n4a<x>.super.toString());
        System.out.println(n4a<x>.n4b.super.toString());
        System.out.println(n4a.n4b.super.toString());
      }
    }
  }
}
