class a
{
  local void x() { }
  void x() { }
}

class b
{
  local void x() { }
  void x() { }
}

class c extends b
{
  local void x() { }
  void x() { }
}

class d
{
  local d x() { return null; }
  int x() { return 0; }

  d local l;
  d g;

  void f()
  {
    int xx = g.x();
    d yy = l.x();
  }

  local void g()
  {
    int xx = ((d)this).x();
    d yy = x();
  }

  void h()
  {
    int xx = x();
    d yy = ((d local)this).x();
  }
}
