// Test template extending instantiation of itself.
// Expected result: FAIL

class E4 {
  public static void main(String[] args) {
    new E4a<0>();
  }
}

template<int x> class E4a extends E4a<x + 1> {
}
