class a
{
  single void barrier() { }

  int b;
  void f()
  {
    boolean single x;

    partition
    {
      (x = b > 0) => barrier();
      b < 0 => { barrier(); barrier(); }
    }
  }
}
