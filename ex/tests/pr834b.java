// Test unchecked exceptions and global effects in static initializers.
// Relevant PR: PR834
// Expected result: FAIL

public class D10 {
  static {
    try {
      if (Ti.thisProc() == 0)
        throw new RuntimeException();
      Ti.barrier();
    } catch (RuntimeException re) {
    }
  }
  public static single void main(String[] args) {
    Ti.barrier();
  }
}

