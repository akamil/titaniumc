// Test folding of fields of class that is not method ready. See PR661.
// Expected result: PASS

class Z5 {
  public static final int xxx = 3;
  public static final int yyy = xxx;
  public int zzz = foo(null);
  public Z5 www = new Z5(null);
  public static int foo(Z5a<yyy> rd_result) { return 0; }
  public Z5(Z5a<yyy> rd_result) { }
  public static void main(String[] args) {}
}

template <int blah>
class Z5a {
}
