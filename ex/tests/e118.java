immutable class I1 {
    public boolean single op==(I1 single I) { return true; }
    public boolean single op!=(I1 single I) { return true; }
}

immutable class I2 {
    public boolean op==(I2 single I) { return true; }
    public boolean op!=(I2 single I) { return true; }
}

immutable class I3 {
    public boolean single op==(I3 I) { return true; }
    public boolean single op!=(I3 I) { return true; }
}

immutable class I4 {
    boolean single op==(I4 single I) { return true; }
    boolean single op!=(I4 single I) { return true; }
}

immutable class I5 {
    private boolean single op==(I5 single I) { return true; }
    private boolean single op!=(I5 single I) { return true; }
}

immutable class I6 {
    boolean single op==(Object I) { return true; }
    boolean single op!=(Object I) { return true; }
}

immutable class I7 {
    public Object single op==(I7 single I) { return null; }
    public Object single op!=(I7 single I) { return null; }
}

immutable class I8 {
    public boolean single op==(I8 single I) throws java.io.IOException { return true; }
    public boolean single op!=(I8 single I) throws Throwable { return true; }
}

immutable class I9 {
    public single boolean single op==(I9 single I) { return true; }
    public single boolean single op!=(I9 single I) { return true; }
}

class B {
  public static void main(String[]args) {}
}
