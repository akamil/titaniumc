// test qualified allocation with null literal as qualifier
// expected result: fail
class I1 {
    class I1a {
    }
    public static void main(String[] args) {
	null.new I1a();
    }
}
