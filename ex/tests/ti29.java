// Test multi-parametered template inheritance.
// Expected result: PASS

class c5 {
  public static void main(String[] args) {
    System.out.println(new c5f());    
    System.out.println(new c5g());
    System.out.println(new c5h());
  }
}

template<class x, int y> class c5a {
  String v = "c5a.v " + y;
}

template<int x, class y> class c5b {
  int u = x + y.class.toString().length();
}

class c5c {
  class c5d {
    static final int y = 8;
  }
  static final int z = c5e.w;
}

class c5e {
  static final int w = c5c.c5d.y + 1;
}

class c5f extends c5a<c5c.c5d, c5c.z + 3> {
  public String toString() {
    return super.toString() + " " + v;
  }
}

class c5g extends c5b<c5c.z, c5c> {
  public String toString() {
    return super.toString() + " " + u;
  }
}

class c5h extends c5b<c5c.c5d.y, c5c.c5d> {
  public String toString() {
    return super.toString() + " " + u;
  }
}


