// Test unqualifed qualified anonymous classes.
// Expected result: PASS

class y5 {
  class y5a {
  }
  public static void main(String[] args) {
    new y5().foo();
  }
  void foo() {
    y5a x = new y5a() {};
    System.out.println(x);
  }
}

