class UnsoundCopy
{
    static void transmit( Object local [1d] source,
                          Object local [1d] local dest )
    {
        dest.copy( source );
    }
}

