// Test normal completion and reachability of if.
// Relevant PR: PR696
// Expected result: FAIL

class h7 {
  public static void main(String[] args) {
  }
  static int foo() {
    if (true) return 0;
  }
  static int bar() {
    if (false) return foo();
  }
  static int baz() {
    if (true) return 0;
    else return 1;
  }
  static int foobar() {
    if (true) return 0;
    return 1;
  }
}

