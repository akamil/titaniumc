// Test qualified anonymous classes that take in arguments.
// Expected result: PASS

class B5 {
  public static void main(String[] args) {
    B5b x = new B5b();
    x.new B5a(1) { 
    };
    new B5().foo();
  }
  void foo() {
    B5b x = new B5b();
    x.new B5a(3) { 
    };
  }
}

class B5b {
  class B5a {
    B5a(int x) { System.out.println(x); }
  }
}
