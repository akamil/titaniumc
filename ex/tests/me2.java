class a
{
  PrivateRegion r1;
  SharedRegion r2 = new (r1) SharedRegion();
  PrivateRegion r3 = new (r1) PrivateRegion();
};
