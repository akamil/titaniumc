class a extends Throwable { }
class b extends Throwable { }

class c
{
  boolean x;

  int f() throws a
  {
    // complains about missing return (when it shouldn't, but javac
    // agrees with us, so...)
    try { if (x) throw new b(); throw new a(); }
    catch (b x) { }
  }

  int g() throws a
  {
    throw new a();
  }
}
