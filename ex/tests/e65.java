immutable class A
{
  int x = 2;
}

immutable class B
{
  int y = 3;

  void f(A z)
  {
    A t;

    if (z == t) y = z.x;
  }
}
