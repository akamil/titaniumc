// Tests qualified this.
// Expected result: PASS

class f2 {
    class f2a {
	f2a() {
	    System.out.println(f2.this);
	    System.out.println(x);
	    System.out.println(x());
	    System.out.println(f2.this.x);
	    System.out.println(f2.this.x());
	}
	int x = 2;
	int x() { return 1; }
    }
    public static void main(String[] args) {
	System.out.println(new f2().new f2a());
    }
    int x = 3;
    int x() { return 4; }
}
