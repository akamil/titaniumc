// Test passing template parameter as part of actual of superclass.
// Expected result: PASS

class D4 {
  static final int z = 2; 
  public static void main(String[] args) {
    System.out.println(new D4a<D4>());
  }
}

template<class x> class D4a extends D4b<x.z> {
  public String toString() { return y; }
}

template<int x> class D4b {
  String y = "D4b: " + x;
}
