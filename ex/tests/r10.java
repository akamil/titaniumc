// Test normal completion and reachability of try/catch/finally.
// Relevant PR: PR696
// Expected result: FAIL

class m7 {
  public static void main(String[] args) {
  }
  static int foo(int x) {
    try {
      bar();
      return 1;
    } catch (NumberFormatException e) {
      System.out.println(e);
      return 2;
    } finally {
      return 0;
    }
    return 3;
  }
  static void bar() {
  }
}

