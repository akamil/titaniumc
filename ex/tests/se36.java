// Test non-universal exceptions.
// Expected result: FAIL
// Relevant PRs: PR824, PR356

class p10 {
  public single static void main(String[] args) throws Throwable single, Throwable {
    try {
      if (Ti.numProcs() > 2) {
        throw new Throwable();
      }
      Ti.barrier();
    } catch (Exception single e) {
      Ti.barrier();
    } catch (Exception e) {
	Ti.barrier();
    }
    Ti.barrier();
    if (Ti.thisProc() == 0) {
      throw new Exception();
    }
  }
}
