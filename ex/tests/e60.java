class a { }
class b extends a { }

class C
{
  RectDomain<1> d1;
  RectDomain<2> d2;
  a local[] x1 = new a[10];
  a local[1d] x2 = new a[d1];
  a local[] local x3 = (a local[])new b local[10];
  a local [] [] y1 = new a local [10] [] [];
  a local [] [] y1a = new a local [10] [1d];
  a local [] [] y1b = new a local [10];
  a local [2d] [1d] local y2 = new a local [d2] [1d];
  a local [] [] local z1 = new a local [10] local [];
  a local [2d] local [1d] z2 = new a local [d2] [1d] local;
}
