// test unqualified allocation where enclosing instance should be used
// as qualifier
// expected result: pass
class x1 {
  class x1a {
    x1a() {
      System.out.println(new x1b());
    }
  }
  class x1b {
  }
  public static void main(String[] args) {
    new x1().new x1a();
  }
}

