// Test field access on built-in types, when the access can look like a
// qualified type name.
// Relevant PR: PR853
// Expected result: FAIL

public class I10 {	    
  public static void main(String[] args) {
    Point<1> i = [1];
    i.undefinedObject.undefinedMethod();
    i.arity.foo();
    RectDomain<1> j = [1:1];
    j.undefinedObject.undefinedMethod();
    j.arity.foo();
    int[1d] k = new int[j];
    k.undefinedObject.undefinedMethod();
    k.arity.foo();
    Domain<1> l = j;
    l.undefinedObject.undefinedMethod();
    l.arity.foo();
  }
}
