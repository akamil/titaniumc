// Test unqualified access to an inherited method of an enclosing class.
// Expected result: PASS

class o4 {
  public static void main(String[] args) {
    new o4a().new o4b();
  }
  static void foo() { System.out.println("foo"); }
  void bar() { System.out.println("bar"); }
}

class o4a extends o4 {
  class o4b {
    { foo();
      bar(); }
  }
}
