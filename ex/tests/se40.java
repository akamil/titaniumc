// Test mixing of universal and non-universal exceptions.
// Expected result: FAIL
// Relevant PRs: PR824, PR356

class t10 {
  public single static void main(String[] args) {
    try {
      single throw new Exception();
    } catch (Exception e) {
      System.out.println("caught #1");
    } catch (Exception single e) {
    }
  }
}
