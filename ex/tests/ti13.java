// Test class passing value that depends on a member of itself as actual to 
// template superclass.
// Expected result: FAIL

class J4 extends J4a<J4b.x> {
  public static void main(String[] args) {
  }
  static final int x = 4;
}

template<int x> class J4a {
}

class J4b {
  static final int x = J4.x + 1;
}

