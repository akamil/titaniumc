// Test ability of overriding method to declare unchecked exception that
// overridden method doesn't.
// Relevent PR: PR692
// Expected result: PASS

class q6 {
  public static void main(String[] args) {
  }
  void foo() {}
  void bar() {}
}

class q6a extends q6 {
  void foo() throws NullPointerException {
  }
  void bar() throws OutOfMemoryError {
  }
}

