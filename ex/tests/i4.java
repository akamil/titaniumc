// test name disambiguation between field and toplevel class
// expected result: fail
class u1 {
    u1b u1a = new u1b();
    public static void main(String[] args) {
	System.out.println(u1a.x);
    }
}

class u1a {
    final static int x = 3;
}

class u1b {
    int x = 4;
}
