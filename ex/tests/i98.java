// Test attempting to use qualified this with enclosing class of superclass.
// Expected result: FAIL

class l4 extends l4a.l4b {
  l4() {
    new l4a().super();
    System.out.println(l4a.this);
  }
  public static void main(String[] args) {
    new l4();
  }
}

class l4a {
  class l4b {
  }
}

