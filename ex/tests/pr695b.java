// Test inheriting abstract method with same signature from multiple
// supertypes.
// Relevant PR: PR695
// Expected result: PASS

abstract class A6 extends A6a implements A6b {
  public static void main(String[] args) {
  }
  public void bar() { foo(); }
}

abstract class A6a {
  public abstract void foo();
}

interface A6b {
  public void foo();
}

