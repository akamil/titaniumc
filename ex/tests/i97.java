// Tests using templates and template qualified names with qualified this.
// Expected result: PASS

class k4 {
  class k4b {
  }
  public static void main(String[] args) {
    System.out.println(new k4a<5>().new k4b().new k4c());
    //System.out.println(new k4a<6>());
  }
}


template<int x> class k4a {
  class k4b {
    class k4c {
      { 
        System.out.println(k4a.this);
        System.out.println(k4a<x>.this);
        System.out.println(k4a<x>.k4b.this);
        System.out.println(k4a.k4b.this);
      }
    }
  }
}
