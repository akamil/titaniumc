// check that multiple field defs produces error on reference

interface i1 { final int i = 1; }
interface i2 { final int i = 2; }
interface i3 extends i1 { final int j = 3; }

class Y { int i; }

class X1 extends Y implements i1 { 
  int x = i; // error
}

class X2 implements i1, i2 { 
  int x = i; // error
}

class X3 implements i1, i2 { 
  int x = i1.i; // ok, disambiguated
}

class X4 extends Y implements i1 { 
  int x = super.i; // ok, disambiguated
}

class X5 implements i1, i3 {
  int x = i; // ok, same field from both interfaces
}
