immutable class Inner
{
    Object local reference;
}


immutable class Outer
{
    Object local reference;
    Inner inner;
}


class Boxed
{
    Outer outer;
}


class Accessor
{
    static
    {
	Inner inner = new Inner();
	Object local reference = inner.reference;
    }
    
    static
    {
	Outer outer = new Outer();
	Inner inner = outer.inner;
	Object local reference = outer.inner.reference;
    }
	
    static
    {
	Boxed local boxed = new Boxed();
	Outer outer = boxed.outer;
	Inner inner = boxed.outer.inner;
	Object local reference = boxed.outer.inner.reference;
    }
	
    static
    {
	Boxed boxed = new Boxed();
	Outer outer = boxed.outer;
	Inner inner = boxed.outer.inner;
	Object reference = boxed.outer.inner.reference;
    }
}
