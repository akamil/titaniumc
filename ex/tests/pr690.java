// Test multiple imports of the same type.
// Relevant PR: PR690
// Expected result: PASS

import java.util.*;
import java.util.Vector;
import java.util.Vector;

class o6 {
  public static void main(String[] args) {
    new Vector();
  }
}

