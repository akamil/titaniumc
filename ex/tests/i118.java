// Test using qualified this with supertype of enclosing type.
// Expected result: FAIL

class e7 extends e7b {
  class e7a {
    { System.out.println(e7b.this); }
    { System.out.println(Object.this); }
  }
  public static void main(String[] args) {
    new e7().new e7a();
  }
}

class e7b {
}
