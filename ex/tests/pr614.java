// Test super constructor call from immutable (PR614).
// Expected result: FAIL

immutable class s3 {
  s3() {
    super();
  }
  public static void main(String[] args) {
    s3 x;
    s3a y;
  }
}

immutable class s3a {
  s3a() {
  }
}

