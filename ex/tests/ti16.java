// Test nested class passing a member of its enclosing class as a parameter 
// to a template superclass.
// Expected result: PASS

class M4 {
  static final int x = 3;
  static class M4a extends M4b<M4.x> {
  }
  public static void main(String[] args) {
    new M4a();
  }
}

template<int x> class M4b {
}

