// Test nested classes inside qualified anonymous classes in templates.
// Expected result: PASS

class F5 {
  class F5a {
    { System.out.println("F5.F5a"); }
  }
  public static void main(String[] args) {
    new F5b<1>();
    new F5b<2>();
  }
}

template<int x> class F5b {
  { new F5().new F5a() { { System.out.println(x); } }; }
}
