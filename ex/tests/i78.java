// Test nested type as template parameter.
// Expected result: PASS

class N3 {
  static class N3b {
  }
  public static void main(String[] args) {
    System.out.println(new N3a<N3.N3b>());
  }
}

template<class x> class N3a {
  { System.out.println(new x()); }
}
