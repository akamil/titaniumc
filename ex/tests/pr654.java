// Test various local qualification issues with anonymous arrays.
// Relevant PR: PR654
// Expected result: PASS

class j6 {
  public static void main(String[] args) {
    int[] b;
    b = new int[] {3,4};
    int[] local c = new int[] {9,0};
    c = new int[4];
    c = new int[] {5,6};
    foo(new int[] {1,2});
    bar(new int[] {7,8});
  }
  static void foo(int[] blah) {
    System.out.print("{");
    for (int i = 0; i < blah.length-1; i++)
      System.out.print(blah[i] + ", ");
    if (blah.length > 0)
      System.out.print(blah[blah.length-1]);
    System.out.println("}");
  }
  static void bar(int[] local blah) {
    foo(blah);
  }
}

