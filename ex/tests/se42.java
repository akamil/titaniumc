// Test globalness of single throw.
// Expected result: FAIL
// Relevant PR: PR824, PR356

class v10 {
  public static void main(String[] args) {
    if (Ti.thisProc() == 0) {
      single throw new Error();
    }
  }
  static void foo() throws Exception {
    if (Ti.thisProc() == 0) {
      single throw new Exception();
    }
  }
  static void bar() throws Exception single {
    if (Ti.thisProc() == 0) {
      single throw new Exception();
    }
  }
}
