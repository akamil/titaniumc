// Tests access to abstract method from nested class.
// Expected result: FAIL

interface U5 {
  int xxx = 5;
  static class U5a implements U5 {
    U5a() {
      System.out.println(xxx);
      foo();
    }
    public void foo() {}
  }
  static class U5b {
    U5b() {
      System.out.println(xxx);
      foo();
    }
  }
  class U5c {
    U5c() {
      System.out.println(xxx);
      foo();
    }
  }
  void foo();
}

class U5b {
  public static void main(String[] args) {
    new U5.U5a();
    new U5.U5b();
  }
}
