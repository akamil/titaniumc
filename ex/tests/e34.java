class a {
 public void m1() { }
 private void m2() { }
 protected void m3() { }
 void m4() { }
}

class b extends a {
  private void m1() { }
  private void m2() { } // ok
  private void m3() { } // ok
  private void m4() { }
}

class c extends a {
  protected void m1() { }
  protected void m2() { } // ok
  protected void m3() { } // ok
  protected void m4() { }
}

class d extends a {
  void m1() { }
  void m2() { } // ok
  void m3() { }
  void m4() { } // ok
}

class e extends a {
  public void m1() { } // ok
  public void m2() { } // ok
  public void m3() { } // ok
  public void m4() { } // ok
}

