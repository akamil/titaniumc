class A {
  static int a;
  static private int b;
  static public int c;
  protected int d;
}

class B extends A {
  int a0 = a;
  int b0 = A.b;
  int c0 = c;
  int d0 = d;
}

class C {
  int a1 = A.a;
  int b1 = A.b;
  int c1 = A.c;
  int d1 = A.d;
}
