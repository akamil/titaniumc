// Test qualified anonymous class allocation from local method.
// Relevant PR: PR652
// Expected result: PASS

import java.io.*;

public class h6 {
    public class h6a {
    }
    public local void bar() {
	h6a i4 = this.new h6a() {};
	h6a i5 = new h6a() {};
    }
    public static void main(String[]args) {
    }
}
