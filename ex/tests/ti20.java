// Test template passing instantiation of itself to template super class.
// Expected result: PASS

class Q4 {
  public static void main(String[] args) {
    System.out.println(new Q4a<3>());
  }
}

template<int x> class Q4a extends Q4b<Q4a<(x < 5 ? x + 1 : 0)>> {
}

template<class x> class Q4b {
}
