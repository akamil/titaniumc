// Tests empty last case/default in switch.
// Expected result: PASS

class p2 {
  public static void main(String[] args) {
    switch (args.length) {
      case 0:
    }
  }
}

