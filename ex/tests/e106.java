template < boolean v, int x > class Cons
{
}


class StackOfInt
{
  static final int one = 1;
  static final int two = 2;
    static public void main( String[] args )
    {
        template Cons < false, one+two > list1; // works
        template Cons < one > two , 1 > list2; // fails - unavoidable
        template Cons < one < two , 1 > list3; // fails - unavoidable
        template Cons < false, one|two > list4; // fails - fixable

        // parenthesized expressions always work:
        template Cons < false, (one+two) > list1b;
        template Cons < (one > two), 1 > list2b;
        template Cons < (one < two), 1 > list3b;
        template Cons < false, (one|two) > list4b;
    }
}
