interface i1 { void f(); }

abstract class A implements i1 { }

class B extends A { void f() { } }

class C extends A { }

class C2 implements i1 { }

final class D extends A { }

class E { abstract void f(); }

abstract class F { abstract void f(); }

class G extends F { }

class H extends F { void f() { } }
