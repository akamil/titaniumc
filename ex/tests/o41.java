class a
{
  int x[];
  RectDomain<1> d1;
  RectDomain<2> d2;
  int[1d] y = new int[d1];
  int[2d] z = new int[d2];
  Point<1> p;
  Point<2> q;

  void f()
  {
    x[0] = 1;
    y[0] = 2;
    y[p] = 3;
    z[0,1] = 4;
    z[q] = 5;
  }
}
