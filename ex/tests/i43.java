// Tests erroneous uses of qualified super.
// Expected result: FAIL

class w2 {
  class w2a {
  }
  static class w2b extends w2a {
  }
  public static void main(String[] args) {
  }
}

class w2c extends w2.w2a {
}

class w2d extends w2.w2a {
  w2d(int x) {
    x.super();
  }
  w2d(w2c x) {
    x.super();
  }
}


