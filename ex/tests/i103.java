// Test local immutables.
// Expected result: FAIL

class v4 {
  public static void main(String[] args) {
    immutable class v4a {
      int x = 4;
    }
    System.out.println(new v4a().x);
  }
  void foo() {
    immutable class v4a {
      int x = 5;
    }
    System.out.println(new v4a().x);
  }
}

