// Tests anonymous arrays with specified size.
// Expected result: FAIL

class t2 {
  public static void main(String[] args) {
    System.out.println(new int[2] {1,2});
    System.out.println(new int[2][] {{},{}});
    System.out.println(new int[]);
  }
}

