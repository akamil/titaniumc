// Test nested class passing a member of itself as a parameter to a template
// superclass.
// Expected result: FAIL

class L4 {
  static class L4a extends L4b<L4.L4a.x> {
    static final int x = 3;
  }
  public static void main(String[] args) {
    new L4a();
  }
}

template<int x> class L4b {
}

