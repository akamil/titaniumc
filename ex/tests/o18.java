// Conversions & assignment to interfaces
public class A {
  // assignments
  Z a1 = null;
  Cloneable o1 = new A[10];
  C c1;
  Z z1 = c1;
  Z2 z2;
  Z z3 = z2;

  // casts
  B b1;
  Object o3 = (Z)null;
  Object o4 = (Cloneable)(new A[10]);
  Object o5 = (Z)b1;
  Object o6 = (Z)c1;
  Object o7 = (Z2)z1;
}

class B extends A {
}

final class C implements Z {
  public int f(int x) { return 0; }
}

interface Z {
  int f(int x);
}

interface Z2 extends Z {
}
