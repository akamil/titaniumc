// Tests using template qualified names with qualified allocations and 
// super constructor calls.
// Expected result: PASS

class i4 {
  class i4b {
  }
  public static void main(String[] args) {
    System.out.println(i4a<4>.y.new i4b());
    System.out.println(i4a<i4a<11>.z>.y.new i4b());
    System.out.println(new i4c());
    System.out.println(new i4c(3));
  }
}


template<int x> class i4a {
  static i4 y = new i4();
  final static int z = x + 1;
}

class i4c extends i4.i4b {
  i4c() {
    i4a<3>.y.super();
  }
  i4c(int x) {
    i4a<i4a<7>.z>.y.super();
  }
}
