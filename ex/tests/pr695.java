// Test inheriting abstract and non-abstract method with same signature.
// Relevant PR: PR695
// Expected result: PASS

class y6 extends y6a implements y6b {
  public static void main(String[] args) {
    new y6().bar();
  }
  void bar() { foo(); }
}

class y6a {
  public void foo() {
    System.out.println(3);
  }
}

interface y6b {
  public void foo();
}

