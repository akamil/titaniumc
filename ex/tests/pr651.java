// Test error message for qualified anonymous class whose superclass is not
// inner class.
// Relevant PR: PR 651
// Expected result: FAIL

public class v7 {
  public static void main(String[] args) {
    v7 x = new v7();
    Object y = x.new Object() {};
    Object z = new v7a().new v7b() {};
  }
}

class v7a {
  static class v7b {
  }
}
