// Test template interfaces.
// Expected result: PASS

class b5 {
  public static void main(String[] args) {
    System.out.println(b5a<3>.x);
    System.out.println(b5a<5>.x);
    b5a<2> x = new b5a<2>.b5b();
    b5a<4> y = new b5a<4>.b5b();
    System.out.println(x);
    System.out.println(y);
  }
}

template<int y> interface b5a {
  void foo();
  class b5b implements b5a {
    public void foo() {}
  }
  static final int x = y;
}

