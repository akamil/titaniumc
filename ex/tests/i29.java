// Test qualified anonymous classes.
// Expected result: PASS

class A {
    public static void main(String[] args) {
	Aa x = new Aa();
	System.out.println(x.new Ab() {
		public String toString() {
		    return "blah";
		}
	    });
    }
}

class Aa {
    class Ab {
    }
}
