// Test nested class passing itself as actual to template superclass.
// Expected result: PASS

class O4 {
  static class O4b extends O4a<O4.O4b> {
  }
  public static void main(String[] args) {
  }
}

template<class x> class O4a {
}

