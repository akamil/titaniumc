// Test accessibilty of enclosing instance field from local class.
// Expected result: PASS

class h3 {
  public static void main(String[] args) {
    class z { { System.out.println(w);} }
    new h3().foo();
    new z();
  }
  void foo() {
    class x { {System.out.println(y);} }
    new x();
  }
  int y = 7;
  static int w = 8;
}
