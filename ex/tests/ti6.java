// Test passing template parameter as actual of superclass.
// Expected result: PASS

class C4 {
  String y = "C4";
  public static void main(String[] args) {
    System.out.println(new C4a<C4>());
  }
}

template<class x> class C4a extends C4b<x> {
  public String toString() { return y; }
}

template<class x> class C4b extends x {
}
