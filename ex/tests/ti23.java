// Test class passing itself as actual to template superinterface.
// Expected result: PASS

class W4 implements W4a<W4> {
  public static void main(String[] args) {
  }
}

template<class x> interface W4a {
}

