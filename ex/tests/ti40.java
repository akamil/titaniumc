// Test class that contains template instance passing value member of itself 
// as actual to template superclass.
// Expected result: FAIL

class T5 extends T5a<T5.x> {
  T5b<Object> y;
  public static void main(String[] args) {
  }
  static final int x = 4;
}

template<int x> class T5a {
}

template<class x> class T5b {
}
