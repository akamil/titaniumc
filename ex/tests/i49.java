// Tests anonymous classes in multiple classes.
// Expected result: PASS

class H2 {
  public static void main(String[] args) {
    System.out.println(new Object() {});
    System.out.println(new Object() {});
    new H2a();
  }
}

class H2a {
  { System.out.println(new Object() {}); }
  { System.out.println(new Object() {}); }
}
