// Test template passing itself as argument to template superclass.
// Expected result: PASS

class P4 {
  public static void main(String[] args) {
    System.out.println(new P4a<3>());
  }
}

template<int x> class P4a extends P4b<P4a<x>> {
}

template<class x> class P4b {
}
