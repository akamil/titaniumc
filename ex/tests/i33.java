// Tests qualified super constructor call.
// Expected result: PASS

class g2 {
    class g2a {
      g2a() {
	System.out.println(x);
      }
    }
    class g2d extends g2a {
      g2d() {
        g2.this.super();
      }
    }
    public static void main(String[] args) {
	System.out.println(new g2b());
	System.out.println(new g2c(new g2()));
	System.out.println(new g2().new g2d());
    }
    int x = 4;
}

class g2b extends g2.g2a {
    g2b() {
	new g2().super();
    }
}

class g2c extends g2.g2a {
  g2c(g2 x) {
    x.super();
  }
}
