// Test constant folding between files (PR649).
// Expected result: PASS

class L5 {
  public static final int x = pr649b.x;
  public static void main(String[] args) {
    Point<x> y;
    System.out.println(y[1]);
    new L5a<x+1>();
  }
}

template<int x> class L5a {
  { System.out.println(x); }
}
