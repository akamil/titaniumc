// Test qualified anonymous classes with templates inside that depend on
// the anonymous class's superclass.
// Expected result: PASS

class E5 {
  public static void main(String[] args) {
    new E5c().new E5a() { { new E5b<x>(); } };
  }
}

template<int x> class E5b {
  { System.out.println(E5b.class); }
}

class E5c {
  class E5a {
    final static int x = 2;
  }
}
