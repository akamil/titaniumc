// Test covariance of primitive return types.
// Relevant PR: PR798
// Expected result: FAIL

class C9 implements C9a {
  public static void main(String[] args) {
  }
  public short foo() {
    return (short) 10;
  }
  public float bar() {
    return 0.1f;
  }
}

interface C9a {
  public int foo();
  public double bar();
}

class C9b extends C9 {
  public byte foo() {
    return (byte) 0xf;
  }
  public byte bar() {
    return (byte) 0xa;
  }
}
