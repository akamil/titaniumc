class a extends Throwable { }
class b extends Throwable { }

class c
{
  boolean x;

  int f() throws a
  {
    try { if (x) throw new b(); throw new a(); } finally { }
  }

}
