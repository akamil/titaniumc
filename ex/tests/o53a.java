immutable class A
{
  B x = x;
}

immutable class B {
  C y = y;
}

immutable class C {
  B z = z;
}
