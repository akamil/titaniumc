class a { }
class b extends a { }

class C
{
  a x1;
  a local x2 = x1;
  a local[] x4;
  a local[] local x3 = x4;
  a local[1d] x5;
  a local[1d] local x6 = x5;
}
