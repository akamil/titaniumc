// Test class literals with templates and nested classes in templates.
// Expected result: PASS

class h4 {
  public static void main(String[] args) {
    System.out.println(h4a<5>.h4b.h4c.class);
    System.out.println(h4a<3>.h4b.class);
    System.out.println(h4a<2>.class);
    System.out.println(h4a<h4a<11>.u>.h4b.class);
    System.out.println(h4a<h4a<7>.h4b.h4c.x>.h4b.class);
    System.out.println(h4d<h4a<1>.h4b.h4c>.class);
  }
}

template<int z> class h4a {
  static class h4b {
    static class h4c {
      final static int x = z + 1;
    }
    static h4c y = new h4c();
  }
  static h4b w = new h4b();
  final static int u = z * 2;
}

template<class v> class h4d {
  static v x = new v();
}
