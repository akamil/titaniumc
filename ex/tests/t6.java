@< int A, int B > class Plus
{
    static final int value = A + B;
}


@< int Operand > class PlusOne
{
    static final int value =  @Plus< Operand, 1 >.value;
}


class Zero
{
    static final int value = 0;
}


class One
{
    static final int value =  @PlusOne< Zero.value >.value;
}


class Two
{
    static final int value =  @Plus< One.value, One.value >.value;
}


class Expressions
{
    static public void main( String[] args )
    {
	System.out.println( Two.value );
    }
}
