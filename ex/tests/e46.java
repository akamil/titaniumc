class a {
  int a[] = { 2, 3, 4, { 1 } };
  int b[][] = { null, a, { 2 },
	        { { 3 } } };
  int c = { 2, 34, "fun" };
}
