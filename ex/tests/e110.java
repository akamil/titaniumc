template <int x>
class foo {}

public class bar {
public static void main(String[]args) {
Domain<1> D1;
RectDomain<1> RD1;
Domain<2> D2;
RectDomain<2> RD2;
Domain<3> D3;
RectDomain<3> RD3;
Point<1> p1;
Point<2> p2;

foreach (Domain<3> p in D1) { p1 += p; }
foreach (RectDomain<3> p in RD1) { p1 += p; }

foreach (foo<1> p in D2) { p2 += p; }
foreach (foo<1> p in RD2) { p2 += p; }

}}
