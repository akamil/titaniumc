// test qualified allocation of nested class
// expected result: fail
class K1 {
    static class K1a {
    }
    public static void main(String[] args) {
	new K1().new K1a();
    }
}
