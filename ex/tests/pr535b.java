// Test illegal overloading using template instantiations (PR535).
// Expected result: FAIL

public class h5 {
 public h5() {
   System.out.println("This is my h5");
 }
 public void aMethod(@h5a<int> a_bld) {}
 public void aMethod(@h5a<int> a_ba) {}
 public void aMethod(@h5a<int>.h5b a_bld) {}
 public void aMethod(@h5a<int>.h5b a_ba) {}
 public void aMethod(int a) {}
 public void aMethod(int b) {}
 public void aMethod(double a) {}
 public static void main(String[]args) {
   h5 a = new h5();
 }
}

template <class T>
class h5a {
  static class h5b {}
}

