// Test inner and anonymous class allocation from local method.
// Relevant PR: PR652
// Expected result: PASS

public class g6 {
    public class g6a {
	public g6a() {}
    }
    public g6() {
	g6a i = this.new g6a();
	Object p = new Object() {};
    }
    public void foo() {
	g6a i = this.new g6a();
	Object p = new Object() {};
    }
    public local void bar() {
	g6a i = this.new g6a();
	Object p = new Object() {};
    }

    public static void main(String[]args) {
	g6 bc = new g6();
	g6a i = bc.new g6a();
	Object p = new Object() {};
    }
}

