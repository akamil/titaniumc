// Test hiding of nested interface by nested class.
// Expected result: PASS

class A3 extends A3a {
  static class A3b {
  }
  public static void main(String[] args) {
    System.out.println(new A3b());
  }
}

class A3a {
  interface A3b {
  }
}

