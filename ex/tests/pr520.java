// Test overriding using template instantiations (PR520).
// Expected result: PASS

public class j5 extends j5c {
 public j5() {
   System.out.println("This is my j5");
 }
 public void aMethod(@j5a<double> a_bld) {
   System.out.println("j5.aMethod 1");
 }
 public void bMethod(@j5a<int> a_ba) {
   System.out.println("j5.bMethod 1");
 }
 public void aMethod(@j5a<double>.j5b a_bld) {
   System.out.println("j5.aMethod 2");
 }
 public void bMethod(@j5a<int>.j5b a_ba) {
   System.out.println("j5.bMethod 2");
 }
 public static void main(String[]args) {
   j5 a = new j5();
   j5c b = new j5c();
   a.aMethod(new j5a<double>());
   a.aMethod(new j5a<double>.j5b());
   a.bMethod(new j5a<int>());
   a.bMethod(new j5a<int>.j5b());
   a.bMethod(new j5a<double>.j5b());
   b.aMethod(new j5a<double>());
   b.bMethod(new j5a<double>.j5b());
 }
}

template <class T>
class j5a {
  static class j5b {}
}

public class j5c {
 public void aMethod(@j5a<double> a_bld) {
   System.out.println("j5c.aMethod 1");
 }
 public void bMethod(@j5a<double>.j5b a_bld) {
   System.out.println("j5c.bMethod 1");
 }
}

