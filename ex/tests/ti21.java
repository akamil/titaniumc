// Test passing qualified type into template superclass.
// Expected result: PASS

class S4 extends S4a<String local> {
  public static void main(String[] args) {
    System.out.println(foo());
  }
}

template<class x> class S4a {
  static x foo() {
    return new x();
  }
}

