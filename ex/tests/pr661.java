// Test folding of fields of class that is not method ready. See PR661.
// Expected result: PASS

template <int val>
class V5a {
  public static final int arity = val;
  public static void foo(V5a<arity> rd_result) { }
}

template <class RD1>
class V5b {
   public static final int arity = V5a<1>.arity;
   public static void check(V5a<arity> rd_result) { }
}

public class V5 {
   public static void main(String[]args) {
       V5a<1> d;
       V5b<V5a<1>>.check(d);
   }
}

