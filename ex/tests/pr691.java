// Test calling Object's methods on interface.
// Relevant PR: PR691
// Expected result: PASS

class p6 {
  public static void main(String[] args) {
    java.util.Enumeration e = new java.util.Vector().elements();
    System.out.println(e.equals(e));
    e.notify();
    System.out.println(e.hasMoreElements());
  }
}

