// Tests anonymous allocation with arguments.
// Expected result: PASS

class J2 {
  J2(int x) {
    System.out.println(x);
  }
  public static void main(String[] args) {
    new J2(4) { { System.out.println(this); } };
  }
}

