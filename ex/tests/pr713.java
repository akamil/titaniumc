// Test typechecking rules for ? expressions.
// Relevant PR: PR713
// Expected result: PASS

class A7 {
  static boolean single b;
  public static single void main(String[] args) {
    Object o = b ? new String() : foo();
    Object p = b ? new Object() : bar();
    System.out.println(o + " " + p);
  }
  static single Object foo() {
    Object o = new Object();
    o = broadcast o from 0;
    return o;
  }
  static single String bar() {
    String o = new String();
    o = broadcast o from 0;
    return o;
  }
}

