// Test template inheritance.
// Expected result: PASS

class x4 {
  public static void main(String[] args) {
    System.out.println(new x4f());    
    System.out.println(new x4g());
    System.out.println(new x4h());
  }
}

template<class x> class x4a {
  String v = "x4a.v";
}

template<int x> class x4b {
  int u = x;
}

class x4c {
  class x4d {
    static final int y = 8;
  }
  static final int z = x4e.w;
}

class x4e {
  static final int w = x4c.x4d.y + 1;
}

class x4f extends x4a<x4c.x4d> {
  public String toString() {
    return super.toString() + " " + v;
  }
}

class x4g extends x4b<x4c.z> {
  public String toString() {
    return super.toString() + " " + u;
  }
}

class x4h extends x4b<x4c.x4d.y> {
  public String toString() {
    return super.toString() + " " + u;
  }
}


