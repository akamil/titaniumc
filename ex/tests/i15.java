// test partially qualified names
// expected result: pass
class F1 {
    static class F1a {
	{System.out.println(new F1b.F1c());}
    }
    static class F1b {
	static class F1c {
	}
    }
    public static void main(String[] args) {
	new F1a();
    }
}
