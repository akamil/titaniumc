// Test class passing type member of itself as actual to template qualified
// superclass.
// Expected result: FAIL

class K4 extends K4a<K4.K4b>.K4c {
  public static void main(String[] args) {
  }
  static class K4b {}
}

template<class x> class K4a {
  static class K4c {}
}

