// Test single returns for number methods.
// Relevant PR: PR418
// Expected result: PASS

class e9 {
  public static single void main(String single [] single args) {
    int single i = Integer.parseInt(args[0]);
    if (i > 0)
      Ti.barrier();
    Double single d = Double.valueOf(args[0]);
    float single f = Float.intBitsToFloat(i);
    if (d.doubleValue() + f > 1.0)
      Ti.barrier();
    byte single b = Byte.valueOf(args[0]).byteValue();
    short single s = Short.parseShort(args[0]);
    if (b + s < 2)
      Ti.barrier();
    long single l = Long.parseLong(args[0]) / Double.doubleToLongBits(d.doubleValue());
    if (l > 0)
      Ti.barrier();
  }
}

