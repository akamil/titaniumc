// Test nested immutable classes.
// Relevant PR: PR811
// Expected result: PASS

public class K9 {
  private K9b[1d] single allObjects;
  public static void main(String[] args) {
    K9b e = new K9b();
    e.key = new K9a();
  }	
  static immutable class K9a {
  }
}

class K9b {
  K9.K9a key;
}
