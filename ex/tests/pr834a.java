// Test unchecked exceptions and global effects in static initializers.
// Relevant PR: PR834
// Expected result: PASS

public class C10 {
  static {
    if (Ti.thisProc() == 0)
      throw new RuntimeException();
    Ti.barrier();
  }
  public static single void main(String[] args) {
    Ti.barrier();
  }
}

