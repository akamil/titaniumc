class a
{
  local void x() { }
  void x() { }
}

class b
{
  local void x() { }
  void x() { }
}

class c extends b
{
  local void x() { }
  void x() { }
}

class d
{
  local void x() { }
  int x() { return 0; }
}

class e extends d
{
  local void x() { }
}

