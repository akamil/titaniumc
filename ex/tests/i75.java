// Test name conflict.
// Expected result: PASS

class C3 {
  public static void main(String[] args) {
    new Object() {
      { new C3() {}.foo(); }
    };
  }
  void foo() {}
}

