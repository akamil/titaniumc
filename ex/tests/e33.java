class e1 extends Throwable { }
class e2 extends Throwable { }
class e3 extends Throwable { }
class e3a extends e3 { }

class a {
  void x() throws e1, e3 { }
}

class b extends a {
  void x() throws e1, e2 { }
}

class c extends a {
  void x() throws e3a { } // ok
}

class d extends a {
  void x() { } // ok
}
