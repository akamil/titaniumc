// Test folding of unqualfied non-static final fields.
// Relevant PR: PR827
// Expected result: FAIL

class f10 {
  final byte b = -40;
  static final byte c = b;
  public static void main(String[] args) {
    System.out.println(1 + b);
    System.out.println("" + b);
  }
  public void foo(byte d) {
    switch(d) {
    case this.b:
      foo((byte) (d - 1));
    }
  }
}
