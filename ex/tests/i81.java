// Tests static nested classes inside type-based templates.
// Expected result: PASS
template<class x> class Q3 {
    static class Q3b {
	{System.out.println(new x());}
    }
}

class Q3c {
    public static void main(String[] args) {
	System.out.println(new Q3<Object>.Q3b());
	System.out.println(new Q3<String>.Q3b());
    }
}
