public class Point {
public Point(Domain Domain, RectDomain RectDomain) {}
public static Domain Domain() { return new Domain(); }
public static RectDomain RectDomain() { return new RectDomain(); }
public static void foo(Point<1> Point, Domain<2> Domain, RectDomain<3> RectDomain) {};
public static void main(String[]args) {
RectDomain RectDomain = RectDomain();
Domain Domain = Domain();
Point Point = new Point(Domain, RectDomain);
Domain.Point(Point);
Point<1> p;
Domain<2> d;
RectDomain<3> r;
foo(p,d,r);
}}

class Domain {
public static void Point(Point Point) {}
}
class RectDomain {}

