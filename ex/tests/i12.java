// test disallowed access to private variable
// expected result: fail
class C1 {
    public static void main(String[] args) {
	System.out.println(C1a.x);
    }
}

class C1a {
    private static int x = 3;
}
