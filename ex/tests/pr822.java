// Test covariance of Domain<N> return types.
// Relevant PR: PR822
// Expected result: PASS

class W9 {
  Domain<1> foo() {
    return null;
  }
  Object bar() {
    return foo();
  }
  public static void main(String[] args) {
  }
}

class W9a extends W9 {
  Domain<1> foo() {
    return null;
  }
  Domain<1> bar() {
    return foo();
  }
}

