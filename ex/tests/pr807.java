// Test attempt to extend template base type.
// Relevant PR: PR807
// Expected result: FAIL

class F9 extends F9a {
  public static void main(String[] args) {
  }
}

template<class T> class F9a {
}
