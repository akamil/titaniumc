// Test import-on-demand of nested types.
// Relevant PR: PR689
// Expected result: PASS

import pack.pr689a.*;
import pack.pr689a2.u6a.*;

class t6 {
  public static void main(String[] args) {
    System.out.println(new m6a());
    System.out.println(new u6b());
  }
}

