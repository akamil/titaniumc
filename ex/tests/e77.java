class a
{
  a x;
  a local y;
}

class b
{
  a xx;
  a local yy;

  a local x1 = xx.x; // error: global  
  a local x2 = xx.y; // error: global

  a local [] zz;

  void f()
  {
    xx.x = null; // ok
    xx.y = null; // error: global.local
    zz[0] = null; // error: assign to local elem via global array ref
  }
}

