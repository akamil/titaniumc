// overriding/hiding a final method
class a {
  final void x() { }
  final static void y() { }
}

class b extends a {
  void x() { }
  static void y() { }
}

