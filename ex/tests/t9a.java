@< int ignored > class Base
{
    static final int value = 0;
}


class Derived extends  Base< Derived.value >
{
}


class Main
{
    static public void main( String[] args )
    {
	System.out.println( Derived.value );
    }
}
