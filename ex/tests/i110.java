// Test enclosing class access with qualified anonymous classes.
// Expected result: PASS

class D5 {
  int y = 8;
  static int z = 9;
  public static void main(String[] args) {
    D5b x = new D5b();
    x.new D5a(1) { 
      { System.out.println(z); }
    };
    new D5().foo();
  }
  void foo() {
    D5b x = new D5b();
    x.new D5a(3) { 
      { System.out.println(y); }
    };
  }
}

class D5b {
  class D5a {
    D5a(int x) { System.out.println(x); }
  }
}
