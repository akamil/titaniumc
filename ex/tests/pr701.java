// Test local allocations requiring rewriting in class that undergoes multiple
// type resolutions.
// Relevant PR: PR701
// Expected result: PASS

class R6 {
  public static void main(String[] args) {
    final int y = 3;
    new Object() {};
  }
}

template<int x> class R6a {
}

class R6b extends R6a<1> {
}

