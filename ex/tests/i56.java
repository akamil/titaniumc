// Tests final var usage in local classes.
// Expected result: PASS

class a3 {
  public static void main(String[] args) {
    final int x = 3;
    class a3a extends a3b {
      a3a() {
        super(x);
        System.out.println(x);
      }
    }
    new a3a();
  }
}

class a3b {
  a3b(int x) {
    System.out.println(x);
  }
}

