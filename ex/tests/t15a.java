@< class Element > class Wrapper
{
  Wrapper next;
  Wrapper< Element > prev;
  
  Element payload;
  Wrapper< int > users;

  Wrapper( Element payload )
    {
      this.payload = payload;
      next = this;
      prev = this;
    }
}


class UseSelf
{
  static public void main( String[] args )
    {
      Wrapper< int > inner = new Wrapper< int >( 1 );
      Wrapper< char > outer = new Wrapper< char >( 'a' );
      
      inner.users = inner;
      outer.users = inner;
    }
}
