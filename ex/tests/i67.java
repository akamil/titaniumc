// Test nested classes with the same name in different enclosing classes.
// Expected result: PASS

class p3 {
  static class p3a {
    public String toString() { return "p3.p3a"; }
  }
  public static void main(String[] args) {
    System.out.println(new p3a());
    p3b.foo();
  }
}

class p3b {
  static class p3a {
    public String toString() { return "p3b.p3a"; }
  }
  static void foo() {
    System.out.println(new p3a());
  }
}

