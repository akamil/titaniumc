// Test overriding a non-single method with a single method.
// Relevant PR: PR687
// Expected result: FAIL

class X7 {
  void foo() {}
}

class X7a extends X7 {
  single void foo() {
    Ti.barrier();
  }
}

class X7b {
  public static void main(String[] args) {
    X7 x = new X7a();
    if (Ti.thisProc() == 0) x.foo();
  }
}

