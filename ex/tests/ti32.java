// Test class passing itself as actual to multi-parametered template superclass.
// Expected result: PASS

class f5 extends f5a<f5, f5> {
  public static void main(String[] args) {
  }
}

template<class x, class y> class f5a {
}

