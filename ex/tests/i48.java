// Tests unqualified allocations of inner classes from inside a static
// nested class.
// Expected result: FAIL

class G2 {
  public static void main(String[] args) {
    new G2a();
  }
  static class G2a {
    class G2b {
    }
    {System.out.println(new G2b());}
    {System.out.println(new G2c());}
  }
  class G2c {
  }
}
