public class domtest {
public static Object objmethod(Object arg) {
  return arg;
}
public static Domain<2> dommethod(Domain<2> arg) {
  return arg;
}
public static void main(String[]args) {
  RectDomain<2> r1 = [[1,10]:[20,30]];
  RectDomain<2> r2 = [[15,20]:[40,40]];
  Domain<2> d1 = r1 + r2;
  Domain<2> d1b = r1 + r2;
  Domain<2> d2 = r1 - r2;
  Domain<2> d2b = r1 - r2;
  Domain<2> d3 = r1 * r2;
  Domain<2> d3b = r1 * r2;
  Object o1 = new Object();
  Object o2 = new Object();

  //o1 = r1;
  d1 = r1;
  o1 = r1 + r2;  
  //o1 = r1 * r2;  
  o1 = r1 - r2;  
  //o1 = [[1,10]:[20,30]];
  o1 = [[1,10]:[20,30]] + [[15,20]:[40,40]];
  //o1 = [[1,10]:[20,30]] * [[15,20]:[40,40]];
  o1 = [[1,10]:[20,30]] - [[15,20]:[40,40]];
  //o1 = objmethod(r1);
  o1 = objmethod(r1 + r2);  
  //o1 = objmethod(r1 * r2);  
  o1 = objmethod(r1 - r2);  
  //o1 = objmethod([[1,10]:[20,30]]);
  o1 = objmethod([[1,10]:[20,30]] + [[15,20]:[40,40]]);
  //o1 = objmethod([[1,10]:[20,30]] * [[15,20]:[40,40]]);
  o1 = objmethod([[1,10]:[20,30]] - [[15,20]:[40,40]]);
  d1 = dommethod(r1);
  d1 = dommethod(r1 + r2);  
  d1 = dommethod(r1 * r2);  
  d1 = dommethod(r1 - r2);  
  d1 = dommethod([[1,10]:[20,30]]);
  d1 = dommethod([[1,10]:[20,30]] + [[15,20]:[40,40]]);
  d1 = dommethod([[1,10]:[20,30]] * [[15,20]:[40,40]]);
  d1 = dommethod([[1,10]:[20,30]] - [[15,20]:[40,40]]);
}
}
