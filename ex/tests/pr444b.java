// Test implementing interface that has template throws clause.
// Relevant PR: PR444
// Expected result: PASS

public class w6 implements w6c {
 public void aMethod() throws w6a<int> {}
 public void bMethod() throws w6a<double>.w6b {}
 public static void main(String[]args) {
   w6 a = new w6();
 }
}

template <class T>
class w6a extends Exception {
  static class w6b extends w6a {}
}

interface w6c {
 public void aMethod() throws w6a<int>;
 public void bMethod() throws w6a<double>.w6b;
}
