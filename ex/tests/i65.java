// Test scope of local class.
// Expected result: FAIL

class m3 {
  public static void main(String[] args) {
    { class x {} }
    new x();
  }
}
