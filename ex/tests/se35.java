// Test single throw.
// Expected result: PASS
// Relevant PRs: PR824, PR356

class i10 {
  public single static void main(String[] args) throws Throwable single, Throwable {
    try {
      if (Ti.numProcs() > 2) {
        single throw new Exception();
      } else {
        foo();
      }
      Ti.barrier();
      if (Ti.numProcs() > 2) {
        throw new Throwable();
      }
    } catch (Exception single e) {
      Ti.barrier();
    } catch (Exception e) {
    } catch (Throwable e) {
    }
    Ti.barrier();
    if (Ti.thisProc() == 0) {
      throw new Exception();
    }
  }
  static single void foo() throws Throwable single {
    single throw new Throwable();
  }
}
