// Test mixing of universal and non-universal exceptions.
// Expected result: PASS
// Relevant PRs: PR824, PR356

class u10 {
  public single static void main(String[] args) {
    try {
      single throw new Exception();
    } catch (Exception e) {
      System.out.println("caught #1");
    }
    try {
      single throw new Exception();
    } catch (Exception single e) {
      System.out.println("caught #2");
    } catch (Exception e) {
      System.out.println("caught #3");
    }
    try {
      throw new Exception();
    } catch (Exception single e) {
      System.out.println("caught #4");
    } catch (Exception e) {
      System.out.println("caught #5");
    }
    try {
      try {
        throw new Exception();
      } catch (Exception single e) {
        System.out.println("caught #6");
      }
    } catch (Exception e) {
      System.out.println("caught #7");
    }
  }
}
