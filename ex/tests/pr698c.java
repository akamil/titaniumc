// Check assignment of blank finals of another class.
// Relevant PR: PR698
// Expected result: FAIL

class K6 {
  public static void main(String[] args) {
  }
  static {
    K6a.x = 3;
  }
  { K6a.y = 4; }
  static int w = K6a.z = 5;
}

class K6a {
  final static int z;
  final int y;
  static final int x;
}
