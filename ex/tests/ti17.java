// Test class whose ancestor extends a template superclass parameterized on it.
// Expected result: PASS

class N4 extends N4b {
  public static void main(String[] args) {
  }
}

template<class x> class N4a {
}

class N4b extends N4a<N4> {
}

