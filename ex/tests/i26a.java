// Tests inner classes inside templates.
// Expected result: PASS
template<class x> class R1 {
    class R1a {
	{System.out.println(new x());}
    }
}

class R1b {
    public static void main(String[] args) {
	System.out.println(new R1<Object>());//.new R1a());
	System.out.println(new R1<String>());//.new R1a());
    }
}
