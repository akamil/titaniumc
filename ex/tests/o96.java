public class in {
public static void within() {}
public static void from() {}
public static single void main(String[]args) {
foreach (p1 in [1:10]) {
System.out.println(""+p1);
}
#ifndef _CONCAT
#define _CONCAT_HELPER(a,b) a ## b
#define _CONCAT(a,b) _CONCAT_HELPER(a,b)
#endif

#define BTEST(expr) \
int single _CONCAT(x,__LINE__) = expr

int single x=0,y=0,z=0,a=0,b=0,c=0,from=0,in=0,within=0;
BTEST(broadcast 0 from 0);
BTEST(broadcast x+y+z*a*b*c from 0);
BTEST(broadcast true?x:y from (false?a:b));
BTEST(broadcast (int)x from 0);
BTEST(broadcast (int)(char)(short)x from (int)(char)(short)0);
BTEST(broadcast from from from);

#if 0
foreach (p1 in [1:10], p2 in [101:110]) {
System.out.println(""+p1+" " +p2);
}
#endif
}}
