// Test shadowing of local var with local var in local class.
// Expected result: PASS

class u3 {
  public static void main(String[] args) {
    final int x = 3;
    int y = 4;
    class u3a {
      u3a() {
        int x = 6;
        int y = 5;
      }
    }
  }
}

