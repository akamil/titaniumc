immutable class EmbedsLocal
{
    Object local reference;
}


immutable class EmbedsGlobal
{
    Object reference;
}


immutable class EmbedsEmbedsLocal
{
    EmbedsLocal embedded;
}


immutable class EmbedsEmbedsGlobal
{
    EmbedsGlobal embedded;
}


template< class Element > class Test
{
    static Element element;
    static Element [1d] local proximate;
    static Element [1d] distant;

    static
    {
	proximate.copy( proximate );
	proximate.copy( distant );
	distant.copy( proximate );
	distant.copy( distant );
	
	proximate.exchange( element );
	distant.exchange( element );
    }
}


class Tests
{
    static single public void main( String[] args )
    {
	// basic elements
	{ template Test< Object local > test; }
	{ template Test< Object > test; }
	{ template Test< EmbedsLocal > test; }
	{ template Test< EmbedsGlobal > test; }
	{ template Test< EmbedsEmbedsLocal > test; }
	{ template Test< EmbedsEmbedsGlobal > test; }
	    
	// local array elements
	{ template Test< Object local [1d] local > test; }
	{ template Test< Object [1d] local > test; }
	{ template Test< EmbedsLocal [1d] local > test; }
	{ template Test< EmbedsGlobal [1d] local > test; }
	{ template Test< EmbedsEmbedsLocal [1d] local > test; }
	{ template Test< EmbedsEmbedsGlobal [1d] local > test; }
	    
	// global array elements
	{ template Test< Object local [1d] > test; }
	{ template Test< Object [1d] > test; }
	{ template Test< EmbedsLocal [1d] > test; }
	{ template Test< EmbedsGlobal [1d] > test; }
	{ template Test< EmbedsEmbedsLocal [1d] > test; }
	{ template Test< EmbedsEmbedsGlobal [1d] > test; }
    }
}
