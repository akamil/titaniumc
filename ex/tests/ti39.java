// Stress test for extending templates parameterized by the extender.
// Expected result: PASS

class S5 extends S5a<S5> {
 S5b<S5> a;
 S5b<Object> b = new S5b<Object>();
 public static void main(String[] args) {
   new S5();
   System.out.println("done.");
 }
}

template<class x> class S5a {
 x x1;
 { System.out.println("in S5a: " + x.class); }
}

template<class x> class S5b extends S5c<x,S5b> implements S5d<S5b,x>, S5d<x,S5c<x,S5b>> {
 x x1;
 S5b a1;
 { System.out.println("in S5b: " + x.class); }
}

template<class x, class y> class S5c extends x {
 x x1;
 y y1;
 { System.out.println("in S5c: " + x.class); }
 { System.out.println("in S5c: " + y.class); }
}

template<class x, class y> interface S5d extends S5e<x,y,S5d> {
}

template<class x, class y, class z> interface S5e {
}
