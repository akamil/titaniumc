// Test class literals with primitives.
// Expected result: PASS

class M3 {
  public static void main(String[] args) throws Exception {
    System.out.println(boolean.class);
    System.out.println(char.class);
    System.out.println(byte.class);
    System.out.println(short.class);
    System.out.println(int.class);
    System.out.println(float.class);
    System.out.println(long.class);
    System.out.println(double.class);
    System.out.println(boolean.class.toString());
    System.out.println(char.class.toString());
    System.out.println(byte.class.toString());
    System.out.println(short.class.toString());
    System.out.println(int.class.toString());
    System.out.println(float.class.toString());
    System.out.println(long.class.toString());
    System.out.println(double.class.toString());
  }
}

