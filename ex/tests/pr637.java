// Test class field access on template parameter that is a primitive type.
// Currently fails (should it?)

class U4 {
  public static void main(String[] args) {
    System.out.println(new U4a<int>());
  }
}

template<class x> class U4a {
  public String toString() {
    return x.class.toString();
  }
}
