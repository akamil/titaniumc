// Test exception declaration.
// Expected result: FAIL
// Relevant PRs: PR824, PR356

class s10 {
  public static void main(String[] args) throws Exception single {
    throw new Exception();
  }
  static void foo() throws Error single {
    throw new Error();
  }
  static void bar() throws Error {
    single throw new Error();
  }
  static void baz() throws Exception {
    single throw new Exception();
  }
}
