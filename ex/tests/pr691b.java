// Test interface defining method that is incompatible with protected method
// in Object.
// Relevant PR: PR691
// Expected result: PASS

class Y6 {
  public static void main(String[] args) {
    Y6a x = null;
    x = x.clone();
  }
}

interface Y6a {
  public Y6a clone();
}
