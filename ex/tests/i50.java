// Tests defining constructors for anon classes.
// Expected result: FAIL

class I2 {
  public static void main(String[] args) {
    new Object() {
      Object() {}
    };
  }
}

