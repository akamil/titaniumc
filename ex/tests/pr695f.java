// Test illegal method overloading.
// Relevant PR: PR695
// Expected result: FAIL

class r7 {
  public static void main(String[] args) {
    foo(args);
  }
  static void foo(Object args) {
    System.out.println(1);
  }
  static void foo(Object args) {
    System.out.println(1);
  }
}
