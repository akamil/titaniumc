// Test name conflicts with local classes.
// Expected result: FAIL

class E3 {
  public static void main(String[] args) {
    class E3 {}
    class E3a {
      void foo() {
        class E3b {
          void bar() {
            class E3 {}
          }
        }
      }
    }
    class E3b {
    }
    class E3c {
      class E3c {
      }
    }
  }
}

