// Tests field in anonymous class with same name as final local var.
// Expected result: PASS

class N2 {
  public static void main(String[] args) {
    final int x = 3;
    new Object() {
      String x;
      { System.out.println(x = "blah"); }
    };
  }
}

