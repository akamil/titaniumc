// Tests modifying final var from anonymous class.
// Currently passes (shouldn't)

class F2 {
  public static void main(String[] args) {
    final int x = 3;
    new Object() {
      { x = 2;
        System.out.println(x);
      }
    };
    System.out.println(x);
  }
}

