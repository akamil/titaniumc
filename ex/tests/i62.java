// Test static initializer in local class.
// Expected result: FAIL

class j3 {
  public static void main(String[] args) {
    class z { static { } }
    new j3().foo();
    new z();
  }
  void foo() {
    class x { static { } }
    new x();
  }
  int y = 7;
}
