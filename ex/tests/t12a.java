@< int Dimensions > class Interval
{
  final Point< Dimensions > lower;
  final Point< Dimensions > upper;

  Interval( Point< Dimensions > lower, Point< Dimensions > upper )
    {
      this.lower = lower;
      this.upper = upper;
    }

  boolean contains( Point< Dimensions > probe )
    {
      return lower <= probe && probe < upper;
    }
}


class Main
{
  static final int two = 2;

  static void test(  Interval< two > interval,
		    Point< two > probe )
    {
      System.out.println( probe.toString() + ": " +
			  interval.contains( probe ) );
    }

  static public void main( String[] args )
    {
       Interval< two > interval =
	new  Interval< two >( [ 0, 0 ],
				      [ 4, 8 ] );
      test( interval, [ -1, -1 ] );
      test( interval, [ -1, 6 ] );
      test( interval, [ -1, 10 ] );
      test( interval, [ 2, -1 ] );
      test( interval, [ 2, 6 ] );
      test( interval, [ 2, 10 ] );
      test( interval, [ 6, -1 ] );
      test( interval, [ 6, 6 ] );
      test( interval, [ 6, 10 ] );
    }
}
