// test unqualified allocation of nested class
// expected result: pass
class L1 {
    static class L1a {
    }
    public static void main(String[] args) {
	System.out.println(new L1a());
    }
}
