// Test error messages for undefined names.
// Expected result: FAIL

class D3 {
  public static void main(String[] args) {
    D3a y = new D3a ();
    if (y.glorp()) { }
    if (z) {}
    if (D3b.x) {}
    if (D3a.a.b) {}
  }
  class D3b {
  }
}

class D3a {
  static D3.D3b a;
}
