// Tests inner classes inside value-based templates.
// Expected result: PASS
template<int x, int y> class O3 {
    class O3a {
	{System.out.println(x + " " + y);}
    }
}

class O3c {
    public static void main(String[] args) {
	System.out.println(new O3<1,2>().new O3a());
	System.out.println(new O3<3,4>().new O3a());
    }
}
