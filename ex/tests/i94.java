// Test method calls requiring disambiguation on template qualified names.
// Expected result: PASS

class g4 {
  public static void main(String[] args) {
    System.out.println(g4a<5>.g4b.g4c.x());
    System.out.println(g4a<3>.g4b.y().x());
    System.out.println(g4a<2>.w().y().x());
    System.out.println(g4a<g4a<11>.u>.g4b.y().x());
    System.out.println(g4a<g4a<7>.g4b.g4c.x>.g4b.y().x());
    System.out.println(g4d<g4a<1>.g4b.g4c>.x().x());
  }
}

template<int z> class g4a {
  static class g4b {
    static class g4c {
      final static int x = z + 1;
      static int x() { return x; }
    }
    static g4c y = new g4c();
    static g4c y() { return y; }
  }
  static g4b w = new g4b();
  static g4b w() { return w; }
  final static int u = z * 2;
  static int u() { return u; }
}

template<class v> class g4d {
  static v x = new v();
  static v x() { return x; }
}
