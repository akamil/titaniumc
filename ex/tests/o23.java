interface I { final int i = 2; }

class A implements I { int k = i; }

class B extends A { int j = i; }
