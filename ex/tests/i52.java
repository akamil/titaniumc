// Tests inheritance of throws clauses for anonymous classes.
// Expected result: FAIL

class L2 {
  L2() throws java.io.IOException {
    throw new java.io.IOException();
  }
  public static void main(String[] args) {
    new L2 () {};
 }
}

