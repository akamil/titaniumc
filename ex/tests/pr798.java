// Test return covariance with respect to single.
// Expected result: PASS

class x9 {
  public static single void main(String[] args) {
    int single i = new x9a().foo();
    if (i < 5) Ti.barrier();
    System.out.println("done.");
  }
  int foo() {
    return Ti.thisProc();
  }
}

class x9a extends x9 {
  int single foo() {
    return Ti.numProcs();
  }
}
