// Test overriding a non-sglobal method with an sglobal method.
// Relevant PR: PR687
// Expected result: FAIL

class Z7 {
  void foo() {}
}

class Z7a extends Z7 {
  void foo() {
    Ti.barrier();
  }
}

class Z7b {
  public static void main(String[] args) {
    Z7 x = new Z7a();
    if (Ti.thisProc() == 0) x.foo();
  }
}

