class a
{
  int x[];
  RectDomain<1> d1;
  RectDomain<2> d2;
  int[1d] y = new int[d2];
  int[2d] z = new int[10];
  Point<1> p;
  Point<2> q;

  void f()
  {
    x[0,0] = 1;
    x[p] = 1;
    y[0,0] = 2;
    y[q] = 3;
    z[0] = 4;
    z[p] = 4;
    z[0,1,2] = 4;
  }
}
