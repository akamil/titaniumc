// test allowed access of private fields of enclosed/enclosing type
// expected result: pass
class A1 {
    private int x = 3;
    class A1a {
	A1a() {
	    System.out.println(x);
	}
	private void foo() {
	    System.out.println("foo");
	}
    }
    public static void main(String[] args) {
	new A1().new A1a().foo();
    }
}
