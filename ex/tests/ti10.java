// Test class passing itself as actual to template superclass.
// Expected result: PASS

class G4 extends G4a<G4> {
  public static void main(String[] args) {
  }
}

template<class x> class G4a {
}

