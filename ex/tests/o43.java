class A {
  final static int a = A.b + 1;
  final static int b = a + 1;
  final static int a0 = A.b0 + 1;
  final static int b0 = 3;
  final static int c = 3;
  final static int d = c + 1;
  final static int e = B.a + 1;
  final static int f = B.c + 1;
  final int c0 = 4;
  //final static int c1 = c0;
  //byte t = e;
  final static byte u = (byte)(12+2);
  //byte u = f;
  //void g() { byte b = e; }
  void h() {
    switch (a)
    {
    case 2: break;
    case c: break;
    case c0: break;
    }
  }
}


class B extends A {
  final static int a = 12;
  final static int b = A.e + 1;
  final static int c = A.f + 1;

  void h() {
    switch (a)
    {
    case 2: break;
    case A.c: break;
    case A.c0: break;
    //case c: break;
    }
  }
}
