// Test shadowing of field of enclosing class by non-final local var when in 
// local class.
// Currently succeeds (shouldn't)

class x3 {
  static int y = 3;
  public static void main(String[] args) {
    int y = 2;
    class x3a {
      { System.out.println(y); }
    }
    new x3a();
  }
}

