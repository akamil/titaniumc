// Test class literals as primaries with templates and nested classes in 
// templates.
// Expected result: PASS

class j4 {
  public static void main(String[] args) {
    System.out.println(j4a<5>.j4b.j4c.class.toString());
    System.out.println(j4a<3>.j4b.class.toString());
    System.out.println(j4a<2>.class.toString());
    System.out.println(j4a<j4a<11>.u>.j4b.class.toString());
    System.out.println(j4a<j4a<7>.j4b.j4c.x>.j4b.class.toString());
    System.out.println(j4d<j4a<1>.j4b.j4c>.class.toString());
  }
}

template<int z> class j4a {
  static class j4b {
    static class j4c {
      final static int x = z + 1;
    }
    static j4c y = new j4c();
  }
  static j4b w = new j4b();
  final static int u = z * 2;
}

template<class v> class j4d {
  static v x = new v();
}
