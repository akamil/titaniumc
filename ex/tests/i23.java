// test unqualified allocation of qualified nested class
// expected result: pass
class O1 {
    public static void main(String[] args) {
	System.out.println(new O1a.O1b().toString());
    }
}

class O1a {
    static class O1b {
    }
}
