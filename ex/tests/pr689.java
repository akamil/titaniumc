// Test imports of nested types.
// Relevant PR: PR689
// Expected result: PASS

import pack.pr689a.m6a;
import pack.pr689a2.u6a.u6b;

class n6 {
  public static void main(String[] args) {
    System.out.println(new m6a());
    System.out.println(new u6b());
  }
}

