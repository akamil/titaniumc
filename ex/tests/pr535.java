// Test overloading using different template instantiations (PR535).
// Expected result: PASS

public class g5 implements g5c {
 public g5() {
   System.out.println("This is my g5");
 }
 public void aMethod(@g5a<double> a_bld) {
   System.out.println("method 1");
 }
 public void aMethod(@g5a<int> a_ba) {
   System.out.println("method 2");
 }
 public void aMethod(@g5a<double>.g5b a_bld) {
   System.out.println("method 3");
 }
 public void aMethod(@g5a<int>.g5b a_ba) {
   System.out.println("method 4");
 }
 public static void main(String[]args) {
   g5 a = new g5();
   a.aMethod(new g5a<double>());
   a.aMethod(new g5a<int>());
   a.aMethod(new g5a<double>.g5b());
   a.aMethod(new g5a<int>.g5b());
 }
}

template <class T>
class g5a {
  static class g5b {}
}

interface g5c {
 public void aMethod(@g5a<double> a_bld);
 public void aMethod(@g5a<int> a_ba);
 public void aMethod(@g5a<double>.g5b a_bld);
 public void aMethod(@g5a<int>.g5b a_ba);
}
