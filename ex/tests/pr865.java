// Test early sharing enforcement.
// Relevant PR: PR865
// Expected result: FAIL

public class N10 {
  static int x = 0;
  int y = x++;
  polyshared N10() {}
  public static void main(String[] args) {
    N10 local nonshared [] local a = new N10 local nonshared [1];
    N10 local nonshared [1d] local b = new N10 local nonshared [0:0];
    N10 local nonshared [] c;
    N10 local nonshared [1d] d;
    a[0] = new N10 local nonshared ();
    b[0] = new N10 local nonshared ();
    c = broadcast a from 0;
    d = broadcast b from 0;
    if (c.isLocal()) {
      a = (N10 local nonshared [] local) c;
      b = (N10 local nonshared [1d] local) d;
    }
    System.out.println(a[0].y);
    System.out.println(b[0].y);
    N10a local e = new N10a();
    N10a nonshared f;
    N10b g = new N10b();
    System.out.println(e.x.y);
  }
}

class N10a {
  polyshared N10a() {}
  N10 local nonshared x = new N10 local nonshared();
}

class N10b {
  polyshared N10b() {}
  N10c x;
}

immutable class N10c {
  Object local nonshared x = new Object local nonshared ();
}
