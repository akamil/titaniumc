// Test template usage in class that extends a template.
// Expected result: PASS

class R5 extends R5a<Object> {
  R5b<String> x;
  public static void main(String[] args) {
  }
}

template<class x> class R5a {
}

template<class x> class R5b {
}
