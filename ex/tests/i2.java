// test qualified allocation of nonexistent inner class
// test qualified allocation using nonexistent constructor
// expected result: fail
class s1 {
  public static void main(String[] args) {
    System.out.println(new java.util.Vector().new s1b());
    System.out.println(new s1a().new s1b("a", 3));
  }
    class s1b {
    }
}

class s1a {
  class s1b {
  }
}
