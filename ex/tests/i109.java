// Test qualified anonymous classes with final locals.
// Expected result: PASS

class C5 {
  public static void main(String[] args) {
    final C5b x = new C5b();
    x.new C5a(1) { 
      { System.out.println(x); }
    };
    new C5().foo();
  }
  void foo() {
    final C5b x = new C5b();
    x.new C5a(3) { 
      { System.out.println(x); }
    };
  }
}

class C5b {
  class C5a {
    C5a(int x) { System.out.println(x); }
  }
}
