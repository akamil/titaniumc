// Test local and anonymous classes in templates.
// Expected result: PASS

class T3 {
  public static void main(String[] args) {
    new T3a<Object>();
    new T3a<Throwable>();
  }
}

template<class x> class T3a {
  { 
    final int z = new x().toString().length(); 
    class T3b { {System.out.println(z);} } 
    System.out.println(new x() {
      { System.out.println(z);
        System.out.println(new T3b()); }
     });
  }
}
