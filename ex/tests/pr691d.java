// Test implementing interface that defines method that is incompatible with 
// protected method in Object.
// Relevant PR: PR691
// Expected result: FAIL

class a7 implements a7a {
  public static void main(String[] args) {
  }
  public int clone() {
    return 0;
  }
}

interface a7a {
  public int clone();
}
