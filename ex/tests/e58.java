class a { }
class b extends a { }

class C
{
  a x2;
  a local x1 = x2;

  a local z1 = (b)x2;
  b local z2 = (a local)x2;
}
