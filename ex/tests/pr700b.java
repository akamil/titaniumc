// Tests assignment of char constants to bytes.
// Relevant PR: PR700
// Expected result: PASS

class L6 {
  public static void main(String[] args) {
    System.out.println(foo(new byte[] {'+'}));
  }
  static int foo(byte[] x) {
    switch (x[0]) {
    case '+': return 0;
    case '-': return 1;
    default: return -1;
    }
  }
}
