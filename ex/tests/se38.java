// Test catching unchecked non-universal exceptions.
// Expected result: FAIL
// Relevant PR: PR824, PR356

class q10 {
  public single static void main(String[] args) {
    try {
      foo();
    } catch (Error e) {
    } catch (NullPointerException e) {
    }
    try {
      foo();
      Ti.barrier();
    } catch (Error single e) {
    } catch (NullPointerException single e) {
    }
    try {
      foo();
      Ti.barrier();
    } catch (Error e) {
    } catch (NullPointerException e) {
    }
  }
  static void foo() throws Error {
    throw new Error();
  }
}

