// Tests inner classes inside type-based templates.
// Expected result: PASS
template<class x> class L3 {
    class L3a {
	{System.out.println(new x());}
    }
}

class L3c {
    public static void main(String[] args) {
	System.out.println(new L3<Object>().new L3a());
	System.out.println(new L3<String>().new L3a());
    }
}
