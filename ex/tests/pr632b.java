// Test static field accesses on template parameter that is a qualified type.
// Expected result: PASS

class T4 {
  public static void main(String[] args) {
    System.out.println(new T4a<Object local>());
    System.out.println(new T4a<String local>());
  }
}

template<class x> class T4a {
  public String toString() {
    return x.class.toString();
  }
}
