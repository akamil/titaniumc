// Test local and anonymous extension of templates.
// Expected result: PASS

class z4 {
  public static void main(String[] args) {
    class z4b extends z4a<3> { { foo(); } }
    System.out.println(new z4b());
    System.out.println(new z4a<4>() {}.foo());
  }
}

template<int x> class z4a {
  z4a foo() { System.out.println(new z4a() {}); return this; }
}
