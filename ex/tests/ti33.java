// Test inheritance resolution readiness checks.
// Expected result: PASS

class p5 extends p5a {
  public static void main(String[] args) {
    p5c<5> z = new p5c<5>();
    new p5().foo(z);
    new p5a().foo(z);
    new p5b().foo(z);
  }
}

class p5a extends p5b {
  static final int x = 4;
}

class p5b {
  void foo(p5c<p5a.x + 1> y) {}
}

template<int x> class p5c {
}
