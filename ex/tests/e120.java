// Test error messages for unresolved methods.
// Expected result: FAIL

class I9 {
  void foo() {}
  nonshared I9() {}
  local void bar() {}
  local void baz(Class c) {}
  local polyshared void baz(String s) {}
  I9(Class c) {}
  I9(String s) {}
  local polyshared void narr(Object o, String s) {}
  void narr(String s, Object o) {}
  public static void main(String[] args) {
    I9 local nonshared i = new I9();
    i = new I9 nonshared();
    i.foo();
    i.bar();
    I9 local j = new I9(null);
    j.baz(null);
    j.narr("", "");
    j.narr(null, "");
    j.narr(null, null);
    j.narr(j, null);
  }
}
  
