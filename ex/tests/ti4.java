// Test local and anonymous extension of nested classes in templates.
// Expected result: PASS

class A4 {
  public static void main(String[] args) {
    class A4b extends A4a<3>.A4c {}
    System.out.println(new A4b());
    System.out.println(new A4a<4>.A4c() {});
  }
}

template<int x> class A4a {
  static class A4c {}
}
