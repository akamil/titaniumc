// test qualified allocation with void
// expected result: fail
class G1 {
    static void foo() {}
    class G1a {
    }
    public static void main(String[] args) {
	System.out.println(foo().new G1a());
    }
}
