// Test immutables without user-provided zero-argument constructors.
// Relevant PR: PR791
// Expected result: PASS

immutable class U8 {
  int x;
  int y = 4;
  U8(int o) { x = o; }
  public static void main(String[] args) {
    U8 a = new U8(2);
    U8 b;
    U8a c;
    System.out.println(a.x + " " + a.y + " " + b.x + " " + b.y + " " + c.x);
  }
}

immutable class U8a {
  int x = 3;
}

