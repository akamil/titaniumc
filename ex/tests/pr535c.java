// Test if inherited fields are accessible in types appearing in method/field
// signatures.
// Expected result: PASS

class k5 extends k5a {
  public static void main(String[] args) {
  }
  void foo(k5b<x> y) {}
  void foo(k5b<k5.x> y, int v) {}
  k5b<x> z;
  k5b<k5.x> w;
}

class k5a {
  static final int x = 3;
}

template<int x> class k5b {
}
