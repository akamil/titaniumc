// Tests anonymous arrays.
// Expected result: PASS

class s2 {
  public static void main(String[] args) {
    int a[] = {1, 2};
    int b[];
    b = new int[] {3, 4};
    System.out.println((new int[] {1,2}).length);
    System.out.println((new int[] {1,2})[0]);
    System.out.println((new int[] {1,2})[1]);
    System.out.println(b[1]);
    System.out.println((new int[][] {{5,6},{7,8,9}}).length);
    System.out.println((new int[][] {{5,6},{7,8,9}})[0].length);
    System.out.println((new int[][] {{5,6},{7,8,9}})[1][2]);
    System.out.println((new int[] {blah(1), blah(2)})[0]);
    System.out.println((new int[][] {new int[] {-1,-2}})[0][1]);
  }
  static int blah(int x) {
    System.out.println("blah: " + x);
    return -x;
  }
}

