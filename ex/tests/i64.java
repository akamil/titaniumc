// Test illegal name collision of local classes. 
// Expected result: FAIL

class l3 {
  public static void main(String[] args) {
    class x { {System.out.println("x1");} }
    new x();
    { 
      class x { {System.out.println("x2");} }
      new x();
    }
  }
}  

