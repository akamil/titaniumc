#!/usr/sww/bin/perl

unlink("core");
mkdir("out", 0777);

$nprocs = $ARGV[0];
$nprocs = 1 if ($nprocs <= 0);
print "Running $nprocs test(s) simultaneously\n";
@names = <*.java>;

select(STDOUT); $| = 1;
while ($#names >= 0)
{
  $ndo = $#names + 1;
  $ndo = $nprocs if ($ndo > $nprocs);
  for ($i = 0; $i < $ndo; $i++)
  {
      $fname = $names[0];
      shift @names;
      print "$fname\n";
      
      if (fork() == 0)
      {
	  exec("../tc -fonly -nomain $fname 2>&1 | sed 's:^\./::' >out/$fname.out");
      }
  }
  for ($i = 0; $i < $ndo; $i++)
  {
      wait;
  }
}
system("gdiff --recur ok out | more");
