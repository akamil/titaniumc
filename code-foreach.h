#ifndef _INCLUDE_TITANIUM_CODE_FOREACH_H_
#define _INCLUDE_TITANIUM_CODE_FOREACH_H_


string *& iterationPoint(TreeNode *l, int i);

/* If test_sr is true, then we generate code that verifies the
   foreach strength reduction optimizations when it is run. */
extern bool test_sr;

extern bool opt_foreach;
extern bool opt_lift;
extern bool opt_osr;
extern bool opt_dataflow;
extern bool debug_opt;
extern bool debug_defs;
extern bool use_goto;
extern bool use_nonTrivialSizeCheck;

string rectMin(const string &r, int dim, int arity);
string rectMax(const string &r, int dim, int arity);
string rectStride(const string &r, int dim, int arity);

// a few functions from sr.cc
string LIPTempname(TreeNode *WRTloop, TreeNode *arr, TreeNode *ind);
string SRTempname(TreeNode *WRTloop, llist<const MIVE *> * & l,
		  TreeNode *acc, const MIVE *m);
string SRvar(string ns, int arity);
string SRvar_at_depth(string ns, int d);
Poly *deriv(const MIVE *m, int i, TreeNode *WRTloop, int j);

#endif
