#ifndef _INCLUDE_TITANIUM_CODE_CALL_H_
#define _INCLUDE_TITANIUM_CODE_CALL_H_


extern bool opt_finalize;

const string callGridMethod(TypeNode &t, const string methodName,
			    const string args);


#endif /* _INCLUDE_TITANIUM_CODE_CALL_H_ */
