#ifndef _TC_BRIDGE_H_
#define _TC_BRIDGE_H_

/* You must include AST.h first */

class TouchSet;

class Bridge : public gc {
 public:
  Bridge() : l(NULL), finalized(false), stmt_list(NULL),
    r0(NULL), w0(NULL), r1(NULL), w1(NULL) {}
  
  inline void adjoin(TreeNode *t) { push(l, t); }
  bool must_come_from(TreeNode *t);
  string to_string() const { return long2hexstring((long) this); }

  TouchSet *reads(bool ignore_junk_method);
  TouchSet *writes(bool ignore_junk_method);

  void add_statements(set<StatementNode *> &s);
  void add_decls(treeSet &s);

  void finalize(llist<ForEachStmtNode *> *exclude);

  void push_contents(llist<TreeNode *> *& l);

 private:
  void next_stmt_is(StatementNode *t);
  bool stmt_is_included(StatementNode *t);

  // estimated contents of Bridge before finalization; NULL afterwards
  llist<TreeNode *> *l;

  bool finalized;
  llist<StatementNode *> *stmt_list; // created at finalization
  llist<TreeNode *> *decl_list; // created at finalization
  set<StatementNode *> stmt_set; // everything in stmt_list
  treeSet everything; // everything in the finalized Bridge
  TouchSet *r0, *w0, *r1, *w1;
};

#endif /* _TC_BRIDGE_H_ */
