#include "AST.h"
#include "CodeContext.h"


CodeLiteralNode TryStmtNode::popState( "*pexn_state = local_exnstate.prior;" );


void TryStmtNode::emitStatement( CodeContext &context )
{
    CodeContext subcontext( context );
    subcontext
               << "  EXCEPTION_STATE_DECLARE(local_exnstate);" << endCline
               << "  ti_ExceptionState **pexn_state = &PERPROC_REF(exception_state);" << endCline
	       << "  local_exnstate.prior = *pexn_state;" << endCline
	       << "  local_exnstate.handling = 0;" << endCline
	       << "  TO_LOCAL(local_exnstate.exn_instance) = NULL;" << endCline
	       << "  *pexn_state = &local_exnstate;" << endCline
	       << endCline
	       << "  if (!SETJMP( local_exnstate.handler ))" << endCline;
    {
	CodeContext blockContext( subcontext );
	block()->codeGen( blockContext );
    }
    catches()->codeGen( subcontext );
    popState.codeGen( subcontext );
    finally()->codeGen( subcontext );
    subcontext << "if (nonNull(local_exnstate.exn_instance)) retoss( &local_exnstate );" << endCline;
}
