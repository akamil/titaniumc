#include "AST.h"
#include "CodeContext.h"
#include "UniqueId.h"


void StatementNode::emitStatement( CodeContext & )
{
    undefined( "emitStatement" );
}


void StatementNode::codeGen( CodeContext &context )
{
  context.setPosition( position() );
  emitStatement( context );
}
