#include <cassert>
#include <iomanip>
#include <iostream>
#include "ClassDecl.h"
#include "CfHeader.h"
#include "CfSource.h"
#include "CtGlobal.h"
#include "CtLocal.h"
#include "CtPointInstance.h"
#include "PrimitiveDecl.h"
#include "code.h"
#include "AST.h"
#include "compiler.h"
#include "decls.h"
#include "types.h"
#include "domain-decls.h"
#include "utils.h"
#include "code-operator.h"

#define P(digits) MANGLE_TI_DOMAINS_POINT_ARG(+, digits)
#define M(name, arity, args) (string("")+MANGLE_TI_DOMAINS_POINT_RAWDISPATCH(+, arity, encodedMethodName(name, string("")), args))

/* Abstract away the fact that 1-d points are values whereas N-d points are structs */
string PtX(const char *base, int arity, string idx) { 
  if (arity == 1) {
    return string(base);
  } else return string(base) + ".x" + idx;
}
string PtX(const char *base, int arity, int idx) { 
  assert(idx >= 0 && idx < arity);
  return PtX(base, arity, int2string(idx));
}
const char *PtBeginVal(int arity) { return (arity == 1) ? "" : "{"; }
const char *PtEndVal(int arity) { return (arity == 1) ? "" : "}"; }
 

/* Define Pn_construct() and Pn_empty(). */
static void createPointConstruct(ostream &hFile, int arity, const string digits)
{
  hFile << "#define "
	<< MANGLE_TI_DOMAINS_POINT_CONSTRUCT(<<, digits)
	<< "p";
  for (int i = 0; i < arity; i++)
    hFile << ", v" << i;
  hFile << ") \\\n";
  hFile << "do { \\\n";
  for (int i = 0; i < arity; i++)
    hFile << "  " << PtX("p",arity,i) << " = v" << i << "; \\\n";
  hFile << "} while (0)\n\n";

  string empty = MANGLE_TI_DOMAINS_POINT_EMPTY(+, digits);
  string unit = MANGLE_TI_DOMAINS_POINT_UNIT(+, digits);
  string negunit = MANGLE_TI_DOMAINS_POINT_NEGUNIT(+, digits);
  hFile << "static " << P(digits) << " const _" << empty << " = " << PtBeginVal(arity);
  for (int i = 0; i < arity; i++)
    hFile << " 0" << (i < arity-1?",":"");
  hFile << " " << PtEndVal(arity) << ";\n"
        << "#define " << empty << "() _" << empty << "\n";

  hFile << "static " << P(digits) << " const _" 
        << unit << " = " << PtBeginVal(arity);
  for (int i = 0; i < arity; i++)
    hFile << " 1" << (i < arity-1?",":"");
  hFile << " " << PtEndVal(arity) << ";\n"
        << "#define " << unit << "() _" << unit << "\n";

  hFile << "static " << P(digits) << " const _" 
        << negunit << " = " << PtBeginVal(arity);
  for (int i = 0; i < arity; i++)
    hFile << " -1" << (i < arity-1?",":"");
  hFile << " " << PtEndVal(arity) << ";\n"
        << "#define " << negunit << "() _" << negunit << "\n";
}

/* Point op Point. */
static void createPointOp(ostream &hFile, int arity, const string digits,
			  const char *op, const char *name)
{
  string funcname = M(name, digits, P(digits));
  if (arity == 1) {
    hFile << "#define " << funcname << "(p1,p2) ((p1) " << op << " (p2))\n";
    return;
  }
  hFile << "TI_INLINE(" << funcname << ")\n" << P(digits) << " " <<
    funcname << "(" << P(digits) << " p1, " <<
    P(digits) << " p2)\n{\n";
  hFile << "  " << P(digits) << " ret;\n";
  for (int i = 0; i < arity; i++)
    hFile << "  ret.x" << i << " = p1.x" << i <<
      ' ' << op << " p2.x" << i << ";\n";
  hFile << "  return ret;\n}\n";
}

/* Point op Point. */
static void createPointUnaryOp(ostream &hFile, int arity, const string digits,
			  const char *op, const char *name)
{
  string funcname = M(name, digits, "");
  if (arity == 1) {
    hFile << "#define " << funcname << "(p1) (" << op << "(p1))\n";
    return;
  }
  hFile << "TI_INLINE(" << funcname << ")\n" << P(digits) << " " <<
    funcname << "(" << P(digits) << " p1)\n{\n";
  hFile << "  " << P(digits) << " ret;\n";
  for (int i = 0; i < arity; i++)
    hFile << "  ret.x" << i << " = " << op << " p1.x" << i << ";\n";
  hFile << "  return ret;\n}\n";
}

/* Point op int. */
static void createPointOpI(ostream &hFile, int arity, const string digits,
			   const char *op, const char *name)
{
  string funcname = M(name, digits, MANGLE_INT_ARG(+));
  if (arity == 1) {
    hFile << "#define " << funcname << "(p1,v) ((p1) " << op << " (v))\n";
    return;
  }
  hFile << "TI_INLINE(" << funcname << ")\n" << P(digits) << " " <<
    funcname << "(" << P(digits) << " p1, " <<
    "jint v)\n{\n";
  hFile << "  " << P(digits) << " ret;\n";
  for (int i = 0; i < arity; i++)
    hFile << "  ret.x" << i << " = p1.x" << i << ' ' << op << " v;\n";
  hFile << "  return ret;\n}\n";
}

/* int op Point. */
static void createPointOpIRev(ostream &hFile, int arity, const string digits,
			   const char *op)
{
  string funcname = M(op, digits, MANGLE_INT_ARG(+) + P(digits));
  hFile << "#define " << funcname << "(p1, v, pjunk) \\\n"
        << "       _" << funcname << "((p1), (v))\n";
  if (arity == 1) {
    hFile << "#define _" << funcname << "(p1,v) ((v) " << op << " (p1))\n";
    return;
  }
  hFile << "TI_INLINE(" << funcname << ")\n" << P(digits) << " _" <<
    funcname<< "(" << P(digits) << " p1, " <<
    "jint v)\n{\n";
  hFile << "  " << P(digits) << " ret;\n";
  for (int i = 0; i < arity; i++)
    hFile << "  ret.x" << i << " = v " << op << " p1.x" << i << ";\n";
  hFile << "  return ret;\n}\n";
}

static void createPointOpFunction(ostream &hFile, int arity, const string digits,
				const char *func, const char *name)
{
  string funcname = M(name, digits, P(digits));
  if (arity == 1) {
    hFile << "#define " << funcname << "(p1,p2) " << func << "((p1), (p2))\n";
    return;
  }
  hFile << "TI_INLINE(" << funcname << ")\n" << P(digits) << " " << 
    funcname << "(" << P(digits) << " p1, " <<
    P(digits) << " p2)\n{\n";
  hFile << "  " << P(digits) << " ret;\n";
  for (int i = 0; i < arity; i++)
    hFile << "  ret.x" << i << " = " << func <<
      "(p1.x" << i << ", p2.x" << i << ");\n";
  hFile << "  return ret;\n}\n";
}

/* ... is to createPointOpFunction as createPointOpI is to createPointOp. */
static void createPointOpIFunction(ostream &hFile, int arity, const string digits,
				 const char *func, const char *name)
{
  string funcname = M(name, digits, MANGLE_INT_ARG(+));
  if (arity == 1) {
    hFile << "#define " << funcname << "(p1,v) " << func << "((p1), (v))\n";
    return;
  }
  hFile << "TI_INLINE(" << funcname << ")\n" << P(digits) << " " <<
    funcname << "(" << P(digits) << " p1, " <<
    "jint v)\n{\n";
  hFile << "  " << P(digits) << " ret;\n";
  for (int i = 0; i < arity; i++)
    hFile << "  ret.x" << i << " = " << func << "(p1.x" << i << ", v);\n";
  hFile << "  return ret;\n}\n";
}

/* same but with args reversed */
static void createPointOpIFunctionRev(ostream &hFile, int arity, const string digits,
				 const char *func, const char *name)
{
  string funcname = M(name, digits, MANGLE_INT_ARG(+) + P(digits));
  hFile << "#define " << funcname << "(p1, v, pjunk) \\\n"
        << "       _" << funcname << "((p1), (v))\n";
  if (arity == 1) {
    hFile << "#define _" << funcname << "(p1,v) " << func << "((v), (p1))\n";
    return;
  }
  hFile << "TI_INLINE(" << funcname << ")\n" << P(digits) << " _" <<
    funcname << "(" << P(digits) << " p1, " <<
    "jint v)\n{\n";
  hFile << "  " << P(digits) << " ret;\n";
  for (int i = 0; i < arity; i++)
    hFile << "  ret.x" << i << " = " << func << "(v, p1.x" << i << ");\n";
  hFile << "  return ret;\n}\n";
}

static void createPointComparison(ostream &hFile, int arity, const string digits,
				  const char *op)
{
  string funcname = M(op, digits, P(digits));
  if (arity == 1) {
    hFile << "#define " << funcname << "(p1,p2) ((p1) " << op << " (p2))\n";
    return;
  }
  hFile << "TI_INLINE(" << funcname << ")\njboolean " <<
    funcname << "(" << P(digits) << " p1, " <<
    P(digits) << " p2)\n{\n";
  hFile << "  return ";
  for (int i = 0; i < arity; i++) {
    if (i) hFile << " && ";
    hFile << "p1.x" << i << ' ' << op << " p2.x" << i;
  }
  hFile << ";\n}\n";
}

static void createPointIsNotEqual(ostream &hFile, int arity, const string digits)
{
  hFile << "#define " << M("!=", digits, P(digits)) << "(x, y) "
    "(!" << M("==", digits, P(digits)) << "((x), (y)))\n";
  hFile << "#define " << M("equals", digits, P(digits)) << "(x, y) "
    "(" << M("==", digits, P(digits)) << "((x), (y)))\n";
}

static void createPointAll(ostream &hFile, int arity, const string digits)
{
  string funcname = M("all", digits, MANGLE_INT_ARG(+));
  if (arity == 1) {
    hFile << "#define " << funcname << "(x) " << "(x)\n";
    return;
  }
  hFile << "TI_INLINE(" << funcname << ")\n" << P(digits) << " "
	<< funcname << "(jint x)\n{\n"
	<< "  " << P(digits) << " ret;\n"
	<< "  " << MANGLE_TI_DOMAINS_POINT_CONSTRUCT(<<, digits) << "ret";
  for (int i = 0; i < arity; i++)
    hFile << ", x";
  hFile << ");\n  return ret;\n}\n";
}

static void createPointDirection(ostream &hFile, int arity, const string digits)
{
  string funcname1 = M("direction", digits, MANGLE_INT_ARG(+));
  string funcname2 = M("direction", digits, MANGLE_INT_ARG(+) + MANGLE_INT_ARG(+));
  hFile << "#define " << funcname1 << "(dir) " <<
    funcname2 << "((dir), 1)\n";

  if (arity == 1) {
    hFile << "#define " << funcname2 << "(dir,val) \\\n"
          << "  (pointAssert(((dir) == 1 || (dir) == -1), \"Point<1> accessed with out of range index\"), \\\n"
          << "   ((dir) > 0 ? (val) : -(val)))\n";
    return;
  }
  hFile << "TI_INLINE(" << funcname2 << ")\n" << P(digits) << " " << funcname2
    << "(jint dir, jint val)\n{\n";
  hFile << "  if (dir < 0) { val = -val; dir = -dir; }\n";
  hFile << "  return " <<
    M("set", digits, MANGLE_INT_ARG(+) + MANGLE_INT_ARG(+)) << "(" <<
    M("all", digits, MANGLE_INT_ARG(+)) << "(0), dir-1, val);\n}\n";

}

#if 0
/* should never be called - .arity() is always removed in lowering */
static void createPointArity(ostream &hFile, int arity, const string digits)
{
  hFile << "#define " <<
    M("arity", digits, "") << "() (" << digits << ")\n";
}
#endif

static void createPointGet(ostream &hFile, int arity, const string digits, const char *name, int dimbase)
{
  string funcname = M(name, digits, MANGLE_INT_ARG(+));
  if (!strcmp(name,"get")) hFile << "#define POINT" << digits << "_GET(pt,idx) (" << PtX("(pt)",arity,"##idx") << ")\n";
  if (arity == 1) {
    hFile << "#define " << funcname << "(p, x) \\\n"
          << "    ( pointAssert((x) == " << dimbase << ", \"Point<1> accessed with out of range index\"), (p) )\n";
    return;
  } 
  hFile << "TI_INLINE(" << funcname << ")\njint " << funcname
	<< "(" << P(digits) << " p, jint dim)\n{\n"
        << "  pointAssert(dim >= " << dimbase << " && dim <= " << (dimbase+arity-1)
        << ", \"Point<" << digits << "> accessed with out of range index\");\n"
	<< "  switch (dim) {\n";
  
  for (int dimension = 0; dimension < arity; ++dimension)
    hFile << "    case " << dimbase+dimension << ":  return p.x"
	  << dimension << ";\n";
  
  hFile << "  }\n}\n";
}

static void createPointSet(ostream &hFile, int arity, const string digits, const char *name, int dimbase)
{
  string funcname = M(name, digits, MANGLE_INT_ARG(+) + MANGLE_INT_ARG(+));
  if (arity == 1) {
    hFile << "#define " << funcname << "(p, dim, val) \\\n"
          << "    ( pointAssert((dim) == " << dimbase << ", \"Point<1> accessed with out of range index\"), (val) )\n";
    return;
  }
  hFile << "TI_INLINE(" << funcname << ")\n" << P(digits) << ' ' << funcname
	<< "(" << P(digits) << " p, jint dim, jint val)\n{\n";

  hFile << "  pointAssert(dim >= " << dimbase << " && dim <= " << (dimbase+arity-1)
        << ", \"Point<" << digits << "> accessed with out of range index\");\n";
  hFile	<< "  switch (dim) {\n";
  
  for (int dimension = 0; dimension < arity; ++dimension)
    hFile << "    case " << dimbase+dimension << ":  p.x"
	  << dimension << " = val;  break;\n";
  
  hFile << "  }\n";

  hFile	<< "  return p;\n"
	<< "}\n\n";
}

static void createPointMaxOrMin(ostream &hFile, int arity, const string digits,
				const char *name, const char *op)
{
  string funcname = M(name, digits, P(digits));
  hFile << "TI_INLINE(" << funcname << ")\n" << P(digits) << " " <<
    funcname << "(" << P(digits) << " p1, " <<
    P(digits) << " p2)\n{\n";
  hFile << "  " << P(digits) << " ret;\n";
  for (int i = 0; i < arity; i++)
    hFile << "  " << PtX("ret",arity,i) << " = (" << PtX("p1",arity,i) << " "
          << op << " " << PtX("p2",arity,i) << " ? " << PtX("p1",arity,i)
          << " : " << PtX("p2",arity,i) << ");\n";
  hFile << "  return ret;\n}\n";
}

static void createPointMaxMin(ostream &hFile, int arity, const string digits)
{
  createPointMaxOrMin(hFile, arity, digits, "upperBound", ">");
  createPointMaxOrMin(hFile, arity, digits, "lowerBound", "<");
}

static void createPointPermute(ostream &hFile, int arity, const string digits)
{
  string funcnamecheck = M("checkPermute", digits, "");
  /* Permute checker */
  hFile << "TI_INLINE(" << funcnamecheck << ")\njboolean " <<
    funcnamecheck << "(" << P(digits) << " p)\n{\n";
  if (arity < 31) {
    /* if arity is less than 31, use a bit field to do sort */
    hFile << "  jint bitFlags = 0;\n"
	  << "  int err = 0;\n"
	  << "  int i;\n"
	  << "  for (i = 0; (i < " << digits << ") && (!err); i++) {\n"
	  << "    jint p_i = " << M("get", digits, MANGLE_INT_ARG(<<)) << "(p, i);\n"
	  << "    if ((p_i <= 0) || (p_i > " << digits << ")) {\n"
	  << "      err = 1;\n"
	  << "    } else {\n"
	  << "      jint mask = ((jint) 1) << (p_i-1);\n"
	  << "      if (bitFlags & mask) {\n"
	  << "        err = 1;\n"
	  << "      } else {\n"
	  << "        bitFlags = bitFlags | mask;\n"
	  << "      }\n"
	  << "    }\n"
	  << "  }\n"
	  << "  return ((bitFlags == ((((jint) 1) << " << digits << ") - ((jint) 1))) && (!err));\n";
  } else {
    /* else use an array to do O(n) sort */
    hFile << "  int flags[" << digits << "];\n"
	  << "  int count = 0;\n"
	  << "  int err = 0;\n"
	  << "  int i;\n"
	  << "  for (i = 0; i < " << digits << "; i++) flags[i] = 0;\n"
	  << "  for (i = 0; (i < " << digits << ") && (!err); i++) {\n"
	  << "    jint p_i = " << M("get", digits, MANGLE_INT_ARG(<<)) << "(p, i);\n"
	  << "    if ((p_i <= 0) || (p_i > " << digits << ")) {\n"
	  << "      err = 1;\n"
	  << "    } else {\n"
	  << "      if (flags[p_i-1] == 0) {\n"
	  << "        flags[p_i-1] = 1;\n"
	  << "        count++;\n"
	  << "      } else {\n"
	  << "        err = 1;\n"
	  << "      }\n"
	  << "    }\n"
	  << "  }\n"
	  << "  return ((count == " << digits << ") && (!err));\n";
  }
  hFile << "}\n";

  /* Permute routine */
  string funcname = M("permute", digits, P(digits));
  hFile << "TI_INLINE(" << funcname << ")\n" << P(digits) << " " <<
    funcname << "(" << P(digits) << " p1, " <<
    P(digits) << " p2)\n{\n";
  hFile << "  " << P(digits) << " ret;\n"
        << "  pointAssert(" << M("checkPermute", digits, "") << "(p2), \"Point<" << digits
	<< "> does not result in a valid permute operation\");\n";
  for (int i = 0; i < arity; i++)
    hFile << "  " << PtX("ret",arity,i) << " = " << M("[]", digits, MANGLE_INT_ARG(<<))
	  << "(p1, " << M("get", digits, MANGLE_INT_ARG(<<)) << "(p2, " << i << "));\n";
  hFile << "  return ret;\n}\n";
}

static void createUtils(ostream &hFile)
{
  hFile << "\n\
#include <runtime-options.h>\n\
#include <stdio.h>\n\
#include <java_string.h>\n\
\n\
#ifndef POINTS_BOUNDS_CHECKING\n\
#if BOUNDS_CHECKING\n\
#define POINTS_BOUNDS_CHECKING 1\n\
#else\n\
#define POINTS_BOUNDS_CHECKING 0\n\
#endif\n\
#endif\n\
#if POINTS_BOUNDS_CHECKING\n\
# define pointAssert(fact, msg) (!(fact)?pointAbort(msg):((void)0))\n\
#else /* no bounds checking */\n\
# define pointAssert(fact, msg) ((void) 0)\n\
#endif /* no bounds checking */\n\
#include <native-utils.h>\n\
static void pointAbort(const char *msg) {\n\
  fprintf(stderr, \"fatal error on processor %i in %s:\\n %s\\n\", MYPROC, __current_loc, msg); \\\n\
  fflush(stderr); \\\n\
  abort(); \\\n\
}\n\
TI_PLEASE_INLINE(ti_gcd)\n\
jint ti_gcd(jint a, jint b)\n\
{\n\
  return (b == 0) ? a : ti_gcd(b, a % b);\n\
}\n\
TI_INLINE(ti_lcm)\n\
jint ti_lcm(jint a, jint b)\n\
{\n\
  return ((a == 0) || (b == 0)) ? 0 : (a * b / ti_gcd(a, b));\n\
}\n\
TI_INLINE(ti_div_round_to_minus_inf)\n\
jint ti_div_round_to_minus_inf(jint a, jint b)\n\
{\n\
  /*if ((a > 0) == (b > 0)) return a / b;*/\n\
  if ((a^b) >= 0) return a / b;\n\
  else if (b > 0) return (a - (b - 1)) / b;\n\
  else return (a - (b + 1)) / b;\n\
}\n\
";
}

static void createPointToString(ostream &hFile, int arity, const string digits)
{                                                                                                          
  string funcname = M("toString", digits, "");
  string fmt = "[";
  string args = "";
  for (int i=0; i < arity; i++) {
    fmt += "%i";
    args += PtX("var_p",arity,i);
    if (i < arity-1) { fmt += ","; args += ", "; }
  }
  fmt += "]";
  hFile << "TI_INLINE(" << funcname << ")\nLP_JString " << funcname << "(" << P(digits) << " var_p) {\n"
        << "   char str[" << arity*15 << "];\n"
        << "   sprintf(str, \"" << fmt << "\", " << args << ");\n"
        << "   return java_string_build_8(str);\n"
        << "}\n";
}

static void createDomainMethods(ostream &hFile, int arity, const string digits) {
  const string Ldom = string(MANGLE_TYPE_LOCAL_MARKER) + MANGLE_DOMAIN_TYPE_UNBOXED(+, digits);
  const string mrad = MANGLE_MRAD_TYPE_UNBOXED(+, digits);
  const string Lmrad = string(MANGLE_TYPE_LOCAL_MARKER) + mrad;
  const string rd = MANGLE_RECTDOMAIN_TYPE(+, digits);
  const string rdjarr = string("T") + MANGLE_JAVA_ARRAY_TYPE_MARKER + rd;
  const string Lrdjarr = string(MANGLE_TYPE_LOCAL_MARKER) + rdjarr;
  const string region = RegionDecl->asType()->cType();
  hFile << "#include \"" << "layout!" << Ldom << ".h\"\n";
  hFile << "#include \"" << "layout!" << Lmrad << ".h\"\n";
  hFile << "#include \"" << "layout!" << rdjarr << ".h\"\n";
  hFile << "#include \"" << "layout!" << Lrdjarr << ".h\"\n";
  hFile << "#include \"" << "layout!" << region << ".h\"\n";
  hFile << "#include \"" << rd << ".h\"\n";
  hFile << "#include \"" << mrad << ".h\"\n";
  hFile << "#include \"" << "regions.h\"\n\n";

  const string rd_empty_cache = string("  _ti_empty_RD") + digits + "_cache";
  hFile << rd << rd_empty_cache << ";\n\n";

  hFile << "#define "
        << NEW_RECTDOMAIN_EMPTY(<<, digits) 
        << "  " << rd_empty_cache << "\n\n";

  const string mrad_empty_cache = string("  _ti_empty_MRAD") + digits + "_cache";
  hFile << Ldom << mrad_empty_cache << ";\n\n";
  const string empty_expr = string("")
        + "  (pointAssert(" + mrad_empty_cache + " != NULL, "
        + "\"bad " + mrad_empty_cache + "cache value\"), "
        + mrad_empty_cache + ")";

  hFile << "#define "
        << NEW_DOMAIN_EMPTY(<<, digits) 
        << "  " << empty_expr << "\n\n";

  const string funcname = MANGLE_TI_DOMAINS_MRAD_STATICDISPATCH(+, digits, "makeDomain", region + MANGLE_INT_ARG(+));
  hFile << "TI_INLINE(" << funcname << ")\n" << Lmrad << " " << funcname
        << "(" << region << " region, jint arrayLen) {\n"
        << "  " << Lmrad << " retval;\n"
        << "  java_array_header *arr;\n"
        << "  pointAssert(arrayLen > 0, \"bad arrayLen\");\n"
        << "  OBJECT_RALLOC_WITHEXTRA(retval, MyGRegion(region), " << mrad << ", \n"
        << "   1, sizeof(java_array_header) + arrayLen*sizeof(" << rd << "));\n"
        << "  arr = (java_array_header *)(retval+1);\n"
        << "  JAVA_ARRAY_INIT_FIELDS(arr, arrayLen, sizeof(" << rd << "), TI_ATOMIC_INDEX);\n"
        << "  retval->" << MANGLE_TI_DOMAINS_MRAD_FIELD_ACCESS(<<, "rects", digits) 
        << "    = (" << Lrdjarr << ")arr;\n"
        << "  retval->" << MANGLE_TI_DOMAINS_MRAD_FIELD_ACCESS(<<, "sizecache", digits) 
        << "    = -1;\n"
        << "  retval->" << MANGLE_TI_DOMAINS_MRAD_FIELD_ACCESS(<<, "bBoxcache", digits) << "."
        << MANGLE_TI_DOMAINS_RECTDOMAIN_FIELD_ACCESS(<<, "loopStride", digits)
        << PtX("", arity, 0) 
        << " = -1;\n"
        << "  return retval;\n"
        << "}\n";

  if (arity == MAX_TIARITY) {
    hFile << "#define _TI_INIT_MRAD_EMPTY_CACHE() \\\n";
    for (int i=1; i <= MAX_TIARITY; i++) {
      const string idigits = int2string(i);
      const string imrad = MANGLE_MRAD_TYPE_UNBOXED(+, idigits);
      const string iLmrad = string(MANGLE_TYPE_LOCAL_MARKER) + imrad;
      hFile << "  OBJECT_MALLOC((*(" << iLmrad << "*)&_ti_empty_MRAD" << idigits << "_cache), " << imrad << ", 1); \\\n" 
            << "  " << MANGLE_TI_DOMAINS_DOMAIN_CONSTRUCTORDISPATCH(<<, idigits, 
                          MANGLE_MRAD_CONSTRUCTOR(<<,idigits), "")
            << "((" << iLmrad << ")_ti_empty_MRAD" << idigits << "_cache); \\\n";
      hFile << "  _ti_empty_RD" << idigits << "_cache = " 
            << MANGLE_RECTDOMAIN_CONSTRUCTOR(<<, idigits) << "c" 
            << MANGLE_RECTDOMAIN_TYPE(<<, idigits) << "(); \\\n";
    }
    hFile << "\n\n";
  }
}
void createPointMethods(CfHeader &hFile, int arity)
{
  static bool inited = false;
  if (!inited) {
    createUtils(hFile);
    inited = true;
  }

  (new CtPointInstance( arity ))->define( hFile );

  const string digits(int2string(arity));

  hFile << endl;
  hFile << "/* --------------------------------------------------------------------- " << endl;
  hFile << " *                    Point ARITY = " << digits << endl;
  hFile << " * --------------------------------------------------------------------- */" << endl;
  hFile << endl;

  hFile << endl << " /* ----- Point<" << digits << "> Constructors ----- */" << endl << endl;
  createPointConstruct(hFile, arity, digits);
  hFile << endl << " /* ----- UNARYOP Point<" << digits << "> ----- */" << endl << endl;
  createPointUnaryOp(hFile, arity, digits, "-", "-");
  hFile << endl << " /* ----- Point<" << digits << "> ARITHOP Point<" << digits << "> ----- */" << endl << endl;
  createPointOp(hFile, arity, digits, "+", "+");
  createPointOp(hFile, arity, digits, "+", "+=");
  createPointOp(hFile, arity, digits, "-", "-");
  createPointOp(hFile, arity, digits, "-", "-=");
  createPointOp(hFile, arity, digits, "*", "*");
  createPointOp(hFile, arity, digits, "*", "*=");
  createPointOpFunction(hFile, arity, digits, "ti_div_round_to_minus_inf", "/");
  createPointOpFunction(hFile, arity, digits, "ti_div_round_to_minus_inf", "/=");
  hFile << endl << " /* ----- Point<" << digits << "> ARITHOP int ----- */" << endl << endl;
  createPointOpI(hFile, arity, digits, "+", "+");
  createPointOpI(hFile, arity, digits, "+", "+=");
  createPointOpI(hFile, arity, digits, "-", "-");
  createPointOpI(hFile, arity, digits, "-", "-=");
  createPointOpI(hFile, arity, digits, "*", "*");
  createPointOpI(hFile, arity, digits, "*", "*=");
  createPointOpIFunction(hFile, arity, digits, "ti_div_round_to_minus_inf", "/");
  createPointOpIFunction(hFile, arity, digits, "ti_div_round_to_minus_inf", "/=");
  hFile << endl << " /* ----- int ARITHOP Point<" << digits << "> ----- */" << endl << endl;
  createPointOpIRev(hFile, arity, digits, "+");
  createPointOpIRev(hFile, arity, digits, "-");
  createPointOpIRev(hFile, arity, digits, "*");
  createPointOpIFunctionRev(hFile, arity, digits, "ti_div_round_to_minus_inf", "/");
  hFile << endl << " /* ----- Point<" << digits << "> COMPAREOP Point<" << digits << "> ----- */" << endl << endl;
  createPointComparison(hFile, arity, digits, "==");
  createPointComparison(hFile, arity, digits, ">=");
  createPointComparison(hFile, arity, digits, "<=");
  createPointComparison(hFile, arity, digits, ">");
  createPointComparison(hFile, arity, digits, "<");
  createPointIsNotEqual(hFile, arity, digits);
  //createPointArity(hFile, arity, digits);
  hFile << endl << " /* ----- Point<" << digits << "> Accessors ----- */" << endl << endl;
  createPointGet(hFile, arity, digits, "get", 0);
  createPointGet(hFile, arity, digits, "[]", 1);
  createPointSet(hFile, arity, digits, "set", 0);
  createPointSet(hFile, arity, digits, "replace", 1);
  hFile << endl << " /* ----- Point<" << digits << "> Named methods ----- */" << endl << endl;
  createPointAll(hFile, arity, digits);
  createPointMaxMin(hFile, arity, digits);
  createPointPermute(hFile, arity, digits);
  createPointDirection(hFile, arity, digits);
  createPointOpFunction(hFile, arity, digits, "ti_lcm", "getLcm");
  createPointToString(hFile, arity, digits);
  hFile << endl << " /* ----- Domain<" << digits << "> native methods ----- */" << endl << endl;
  createDomainMethods(hFile, arity, digits);
  hFile << endl << endl;
}
