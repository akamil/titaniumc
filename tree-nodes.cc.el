(defun initialize-buffer (nodes builder)
  (insert-c++-comment-prefix nodes builder)
  (insert "#include <iostream>\n"
	  "\n"
	  "#include \"AST.h\"\n"
	  "#include \"c-types/CtType.h\"\n"
          "#if !defined(TREE1) && !defined(TREE2) && !defined(TREE3) && !defined(TREE4)\n"
	  "  #define TREE1 1\n"
	  "  #define TREE2 1\n"
	  "  #define TREE3 1\n"
	  "  #define TREE4 1\n"
	  "#endif\n"
	  "\n"
	  "\n"))

(defun initialized-states (states)
  (and states
       (let ((state (car states))
	     (rest (initialized-states (cdr states))))
	 (if (caddr state)
	     (cons state rest)
	   rest))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *filenum* 1 "current filenum for class")
(defvar *filenumcnt* 4 "How many filenums to use")

(defun defnode* (name parents fields methods states)
  (let* ((details (resolve-fields fields))
	 (flds (aref details 2))
	 (kids (aref details 0))
	 (attrs (aref details 1))
	 (states (resolve-states states))
	 (init-states (initialized-states states)))
    (insert
     "\n\n"
     (format "#if TREE%i\n" *filenum*)
     (format "\t/* Class %s */\n" name)
     (format "int %s::arity () const { return %d; }\n\n"
	     name (length kids))
     (format "const char* %s::oper_name () const { return \"%s\"; }\n\n"
	     name name)
     (format "%s::%s(" name name))
    (loop for f in flds do
	  (insert (format "%s __%s, "
			  (to-c++-type (cadr f))
			  (basic-field-name (car f)))))
    (insert "SourcePosn _posn)\n")
    (insert-initializers attrs init-states t)
    (insert "{\n")
    (if kids 
	(insert "    children = _children;\n")
        (insert "    children = NULL;\n"))
    (loop for i = 0 then (1+ i)
	  for k in kids do
	  (if (list-type (cadr k))
	      (insert 
	       (format "    child(%d, new %s(__%s));\n"
		       i (cadr k) (car k)))
	    (insert (format "    child(%d, __%s);\n" i (car k)))))
    (insert "    setWhere (_posn);\n")
    (insert "}\n\n")
    (when init-states
      (insert (format "%s::%s ()\n" name name))
      (insert-initializers attrs init-states nil)
      (insert "{\n}\n\n"))
    (insert "#endif\n")
    (setq *filenum* (+ (% *filenum* *filenumcnt*) 1))
))
	 
(defun insert-initializers (attrs states withargs)
  (and (or attrs states)
       (progn (insert "    : ")
	      (let ((sep ""))
		(loop for f in attrs do
		      (let ((field-name (basic-field-name (car f))))
			(if (list-type (cadr f))
			    (insert (format "%s_%s(new %s(__%s))\n"
					    sep field-name
					    (cadr f) field-name))
			(insert
			 (if withargs
			     (format "%s_%s(__%s)" sep field-name field-name)
			   (format "%s_%s(%s)" sep field-name (caddr f))))))
		      (setq sep ",\n"))
		(loop for f in states do
		      (insert (format "%s%s(%s)\n"
				      sep (basic-field-name (car f))
				      (caddr f)))
		      (setq sep ","))))))
