#ifndef _TITANIUM_STATS_H
#define _TITANIUM_STATS_H

#include <iostream>

class TreeNode;
class TypeNode;

extern bool stat_foreach;
void loopStats(TreeNode *t, TypeNode *d, bool partialDomain, bool needPoint,
	       int usi, int lifted, ostream &os);

#endif /* _TITANIUM_STATS_H */
