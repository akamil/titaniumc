#include "AST.h"
#include "CfHeader.h"
#include "CfSource.h"
#include "CodeContext.h"
#include "CtLocal.h"
#include "FieldDecl.h"
#include "MethodDecl.h"
#include "MethodSet.h"
#include "code-util.h"
#include "compiler.h"
#include "ctBox.h"
#include "decls.h"
#include "errors.h"
#include "string-utils.h"
#include "utils.h"

string atomicIndex(const TypeNode *t) {
  if (t->isAtomic())
    return "TI_ATOMIC_INDEX";
  else if (!t->isImmutable()) {
    if (t->isLocal())
      return "TI_LOCAL_OBJECT_INDEX";
    else
      return "TI_GLOBAL_OBJECT_INDEX";
  } else if (t->isTitaniumArrayType()) {
    ostringstream s;
    s << "_ti_";
    if (!t->isLocal())
      s << "global_";
    s << "INDEX(" << t->elementType()->cType() << ", " << t->tiArity() << ")";
    return s.str();
  } else {
    ClassDecl *d = (ClassDecl *) t->decl();
    return d->cDescriptorName() + ".index"; 
  }
}

static const string *register_string = intern("register");
static const string *dump_string = intern("dump");
static const string *local_unswizzle_string = intern("unswizzle_local");
static const string *global_unswizzle_string = intern("unswizzle_global");

static Decl *registerDecl = NULL;
static Decl *dumpDecl = NULL;
static Decl *unswizzleLocalDecl = NULL;
static Decl *unswizzleGlobalDecl = NULL;

static TypeNode *cplType = NULL;
static TypeNode *cpgType = NULL;
static TypeNode *objlType = NULL;
static TypeNode *objgType = NULL;

static bool hasNovelInstanceFields(ClassDecl &cl, bool checkpoint) {
  TreeNode *members = cl.source()->members();
  for (int i = 0; i < members->arity(); i++) {
    TreeNode *mem = members->child(i);
    if (isFieldDeclNode(mem) && !(mem->flags() & Common::Static)) {
      if (checkpoint && !(mem->flags() & Common::Transient) && !mem->dtype()->isAtomic())
	return true;
      else if (!checkpoint && ((mem->flags() & Common::Transient) || !mem->dtype()->isAtomic()))
	return true;
    }
  }
  return false;
}

static void emitNonAtomicImmutableMethods(ClassDecl &cl, CfHeader &hFile, CfSource &cFile) {
  const string checkpoint = cl.cArrayCheckpointName();
  const string restore = cl.cArrayRestoreName();
  TypeNode *cpType = TiLangCheckpointDecl->asType()->addModifiers(Common::Local);
  TypeNode *rsType = cpType;
  TypeNode *imType = cl.asType();
  hFile << "void " << checkpoint << "(void *l, " << cpType->cType() << ");\n";
  hFile << "void " << restore << "(void *, " << rsType->cType() << ");\n";
  cFile << "void " << checkpoint << "(void *loc, " << cpType->cType() << " cp) {\n"
	<< "  " << imType->cType() << " *tmp = (" << imType->cType() << " *) loc;\n"
	<< "  " << cl.cCheckpointName() << "(*tmp, cp, (jboolean) 1);\n"
	<< "}\n";
  cFile << "void " << restore << "(void *loc, " << rsType->cType() << " cp) {\n"
	<< "  " << imType->cType() << " *tmp = (" << imType->cType() << " *) loc;\n"
	<< "  *tmp = " << cl.cRestoreName() << "(*tmp, cp);\n"
	<< "}\n";
}

static void emitCheckpointString(TreeNode *field, string me, string cp, CodeContext &cFile) {
  TypeNode *tn = field->dtype();
  cFile << "    ";
  if (tn->isTitaniumArrayType()) {
    cFile << "    if (!_ti_" << (tn->isLocal() ? "" : "global_") << "arraymethod_isnull("
	  << tn->elementType()->cType() << ", " << tn->tiArity() << ")("
	  << me << "))\n";
    cFile << "      _ti_";
    if (!tn->isLocal())
      cFile << "global_";
    cFile << "CHECKPOINT(" << tn->elementType()->cType() << ", " << tn->tiArity() << ")"
      << "(" << me << ", " << cp << ", (jboolean) 1);\n";
  } else if (tn->isImmutable()) {
    cFile << ((ClassDecl *)tn->decl())->cCheckpointName() << "(" << me << ", " << cp << ", (jboolean) 1);\n";
  } else {
    char *ws = "      ";
    cFile.depend(ctBox(*new CtLocal(ObjectDecl->cDescriptorType()), true));
    cFile.depend(ctBox(*new CtLocal(ObjectDecl->cDescriptorType()), false));
    cFile << "{\n" << ws << objgType->cType() << " " << me << "o;\n";
    cFile << ws << CtLocal(ObjectDecl->cDescriptorType()) << " " << me << "c;\n";
    cFile << ws << me << "o = *(" << objgType->cType() << " *) &" << me << ";\n";
    cFile << ws << "if (!ISNULL_GLOBAL(" << me << ")) {\n";
    cFile << ws << "  CLASS_INFO_GLOBAL(" << me << "c, " << ObjectDecl->cDescriptorType() << ", " << me << "o);\n";
    cFile << ws << "  " << me << "c->checkpoint(" << me << "o, " << cp << ", (jboolean) 1);\n";
    cFile << ws << "}\n";
    cFile << "    }\n";
  }
}

static void emitCheckpoint(ClassDecl &cl, CodeContext &cFile) {
  TypeNode *imType = cl.asType()->removeModifiers(Common::Local);
  bool imm = imType->isImmutable();
  bool novel = hasNovelInstanceFields(cl, true);
  if (imm && !novel) {
    return;
  }
  if (!imm) {
    ClassDecl *super = cl.superClass();
    cFile.depend(imType->addModifiers(Common::Local)->cType());
    cFile << "  " << TiLangCheckpointDecl->asType()->addModifiers(Common::Local)->cType() << " lcp;\n";
    if (novel) {
      cFile.depend(imType->cType());
      cFile << "  " << imType->cType() << " me = *(" << imType->cType() << " *) &obj;\n";
      cFile << "  " << imType->addModifiers(Common::Local)->cType() << " lme;\n";
    }
    cFile << "  if (reg && !" << registerDecl->cMethodNameStatic() << "(cp, obj)) return;\n";
    cFile << "  lcp = TO_LOCAL(cp); /* must be local */\n";
    cFile << "  if (reg) " << dumpDecl->cMethodNameStatic() << "(lcp, obj, (jint) sizeof(" << cl.cType() << "));\n";
    if (super != ObjectDecl) // slight optimization
      cFile << "  " << super->cCheckpointName() << "(obj, cp, (jboolean) 0);\n";
    if (!novel)
      return;
    cFile << "  lme = TO_LOCAL(me); /* must be local */\n";
  } else {
    cFile << "  " << TiLangCheckpointDecl->asType()->removeModifiers(Common::Local)->cType() << " cp;\n";
    cFile << "  globalize(cp, lcp);\n";
  }
  TreeNode *members = cl.source()->members();
  for (int i = 0; i < members->arity(); i++) {
    TreeNode *field = members->child(i);
    if (!isFieldDeclNode(field) || (field->flags() & (Common::Static | Common::Transient)) || field->dtype()->isAtomic())
      continue;
    bool fimm = field->dtype()->isImmutable();
    bool flocal = (field->dtype()->isReference() && field->dtype()->isLocal() && !field->dtype()->isTitaniumArrayType());
    TypeNode *fgtype = (flocal ? field->dtype()->removeModifiers(Common::Local) : field->dtype());
    cFile.depend(fgtype->cType());
    cFile << "  {\n";
    cFile << "    " << fgtype->cType() << " tmp" << i << ";\n";
    if (flocal) {
      cFile.depend(field->dtype()->cType());
      cFile << "    " << field->dtype()->cType() << " tmpl" << i << ";\n";
    }
    cFile << "    tmp" << (flocal ? "l" : "") << i << " = lme" << (imm ? "." : "->") << field->decl()->cFieldName() << ";\n";
    if (flocal)
      cFile << "    globalize(tmp" << i << ", tmpl" << i << ");\n";
    emitCheckpointString(field, "tmp" + int2string(i), string(fimm ? "lcp" : "cp"), cFile);
    cFile << "  }\n";
  }
}

static void emitClearString(TreeNode *field, string lhs, CodeContext &cFile) {
  TypeNode *tn = field->dtype();
  cFile << "    ";
  if (tn->isTitaniumArrayType()) {
    cFile << lhs << " = _ti_" << (tn->isLocal() ? "" : "global_") 
	  << "arraymethod_empty(" << tn->elementType()->cType() << ", " 
	  << tn->tiArity() << ");\n";
  } else if (tn->isImmutable()) {
    // call default constructor
    extern TreeNode *findNoArgConstructor(TreeNode *);
    Decl *d = findNoArgConstructor(tn->decl()->source())->decl();
    cFile << lhs << " = " << d->cMethodNameStatic() << "()";
  } else if (tn->isPrimitive()) {
    cFile << lhs << " = " << "(" << tn->decl()->cType() << ") 0";
  } else if (tn->isLocal()) {
    cFile << lhs << " = " << "NULL;";
  } else {
    cFile << "globalize(" << lhs << ", NULL);";
  }
  cFile << ";\n";
}

static void emitRestoreString(TreeNode *field, string me, string cp, CodeContext &cFile) {
  TypeNode *tn = field->dtype();
  if (field->flags() & Common::Transient) {
    emitClearString(field, me, cFile);
  } else if (tn->isTitaniumArrayType()) {
    cFile << "    " << me << " = "
	  << "_ti_";
    if (!tn->isLocal())
      cFile << "global_";
    cFile << "RESTORE(" << tn->elementType()->cType() << ", " << tn->tiArity() << ")"
      << "(" << me << ", " << cp << ");\n";
  } else if (tn->isImmutable()) {
    cFile << "    " << me << " = " << ((ClassDecl *)tn->decl())->cRestoreName() << "(" << me << ", " << cp << ");\n";
  } else if (tn->isLocal()) {
    char *ws = "      ";
    cFile << "    {\n";
    cFile << ws << ObjectDecl->asType()->addModifiers(Common::Local)->cType() << " " << me << "o;\n";
    cFile << ws << me << "o = *(" << ObjectDecl->asType()->addModifiers(Common::Local)->cType() << " *) &" << me << ";\n";
    cFile << ws << me << "o = " << unswizzleLocalDecl->cMethodNameStatic() << "(" << cp << ", " << me << "o);\n";
    cFile << ws << me << " = *(" << tn->cType() << " *) &" << me << "o;\n";
    cFile << "    }\n";
  } else {
    char *ws = "      ";
    cFile << "    {\n";
    cFile << ws << ObjectDecl->asType()->removeModifiers(Common::Local)->cType() << " " << me << "o;\n";
    cFile << ws << me << "o = *(" << ObjectDecl->asType()->removeModifiers(Common::Local)->cType() << " *) &" << me << ";\n";
    cFile << ws << me << "o = " << unswizzleGlobalDecl->cMethodNameStatic() << "(" << cp << ", " << me << "o);\n";
    cFile << ws << me << " = *(" << tn->cType() << " *) &" << me << "o;\n";
    cFile << "    }\n";
  }
}

static void emitRestore(ClassDecl &cl, CodeContext &cFile) {
  TypeNode *imType = cl.asType()->addModifiers(Common::Local);
  bool imm = imType->isImmutable();
  if (!hasNovelInstanceFields(cl, false)) {
    if (!imm && 
	cl.superClass() != ObjectDecl) // slight optimization
      cFile << "  " << cl.superClass()->cRestoreName() << "(lobj, lcp);\n";
    return;
  }
  TreeNode *members = cl.source()->members();
  if (!imm) {
    ClassDecl *super = cl.superClass();
    cFile << "  " << imType->cType() << " lme = *(" << imType->cType() << " *) &lobj;\n";
    if (super != ObjectDecl) // slight optimization
      cFile << "  " << super->cRestoreName() << "(lobj, lcp);\n";
  }
  for (int i = 0; i < members->arity(); i++) {
    TreeNode *field = members->child(i);
    if (!isFieldDeclNode(field) || (field->flags() & Common::Static))
      continue;
    if (field->dtype()->isAtomic() && !(field->flags() & Common::Transient))
      continue;
    cFile.depend(field->dtype()->cType());
    cFile << "  {\n";
    cFile << "    " << field->dtype()->cType() << " tmp" << i << ";\n";
    cFile << "    tmp" << i << " = lme" << (imm ? "." : "->") << field->decl()->cFieldName() << ";\n";
    emitRestoreString(field, "tmp" + int2string(i), string("lcp"), cFile);
    cFile << "    lme" << (imm ? "." : "->") << field->decl()->cFieldName() << " = tmp" << i << ";\n";
    cFile << "  }\n";
  }
}

void emitCheckpointMethods(ClassDecl &cl, CfHeader &hFile, CfSource &cFile) {
  const string checkpoint = cl.cCheckpointName();
  const string restore = cl.cRestoreName();
  TypeNode *imType = cl.asType()->removeModifiers(Common::Local);
  bool imm = imType->isImmutable();
  if (registerDecl == NULL) {
    registerDecl = TiLangCheckpointDecl->environ()->lookup(register_string, Decl::Method);
    dumpDecl = TiLangCheckpointDecl->environ()->lookup(dump_string, Decl::Method);
    unswizzleLocalDecl = TiLangCheckpointDecl->environ()->lookup(local_unswizzle_string, Decl::Method);
    unswizzleGlobalDecl = TiLangCheckpointDecl->environ()->lookup(global_unswizzle_string, Decl::Method);
    cplType = TiLangCheckpointDecl->asType()->addModifiers(Common::Local);
    cpgType = TiLangCheckpointDecl->asType()->removeModifiers(Common::Local);
    objlType = ObjectDecl->asType()->addModifiers(Common::Local);
    objgType = ObjectDecl->asType()->removeModifiers(Common::Local);
  }
  hFile << "void " << checkpoint << "(" << (imm ? imType : objgType)->cType() << ", " << (imm ? cplType : cpgType)->cType() << ", jboolean);\n";
  if (imm) hFile << imType->cType();
  else hFile << "void";
  hFile << " " << restore << "(" << (imm ? imType : objlType)->cType() << ", " << cplType->cType() << ");\n";
  if (cl.fullName() == "java.lang.Object")
    return; // methods implemented in native code
  cFile << "void " << checkpoint << "(" << (imm ? imType : objgType)->cType() << (imm ? " lme, " : " obj, ") << (imm ? cplType : cpgType)->cType() << (imm ? " l" : " ") << "cp, jboolean reg) {\n";
  {
    CodeContext cpCtx(cFile);
    emitCheckpoint(cl, cpCtx);
  }
  cFile << "}\n";
  if (imm) cFile << imType->cType();
  else cFile << "void";
  cFile << " " << restore << "(" << (imm ? imType : objlType)->cType() << (imm ? " lme, " : " lobj, ") << cplType->cType() << " lcp) {\n";
  {
    CodeContext rsCtx(cFile);
    emitRestore(cl, rsCtx);
  }
  if (imm)
    cFile << "  return lme;\n";
  cFile << "}\n";
  if (imm && !imType->isAtomic())
    emitNonAtomicImmutableMethods(cl, hFile, cFile);
}
