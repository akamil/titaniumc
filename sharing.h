#ifndef INCLUDE_sharing_h
#define INCLUDE_sharing_h

#include <string>
#include "common.h"

enum SharingEnforcement { Late, Early };

enum Sharing {
  Unconstrained,
  Shared,
  Nonshared = Common::NonsharedQ,
  Polyshared = Common::PolysharedQ
};


extern SharingEnforcement sharingEnforcement;

Sharing modifiersToSharing( Common::Modifiers );
Common::Modifiers sharingToModifiers( Sharing );

Sharing lub( Sharing, Sharing );
bool conformsTo( Sharing, Sharing );
bool conformsTo( Common::Modifiers, Common::Modifiers );

ostream &operator << ( ostream &, Sharing );
string &operator += ( string &, Sharing );
string operator + ( const string &, Sharing );
string operator + ( const char [], Sharing );
string operator + ( Sharing, const string & );
string operator + ( Sharing, const char [] );


#endif // INCLUDE_sharing_h
