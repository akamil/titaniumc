#ifndef _FREE_MAP_H_
#define _FREE_MAP_H_

// You must #include <map> before including this file.

#define generic template <class K, class V>

generic void free_map_first(const map<K, V> &m)
{
  for (TYPENAME map<K, V>::const_iterator i = m.begin(); i != m.end(); i++)
    delete (*i).first;
}

generic void free_map_second(const map<K, V> &m)
{
  for (TYPENAME map<K, V>::const_iterator i = m.begin(); i != m.end(); i++)
    delete (*i).second;
}

generic void free_map(const map<K, V> &m, bool free_first = false,
		      bool free_second = true);

generic void free_map(const map<K, V> &m, bool free_first, bool free_second)
{
  if (free_first)
    free_map_first(m);
  if (free_second)
    free_map_second(m);
}

#undef generic

#endif
