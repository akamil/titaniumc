# To use these commands in your own gdb sessions, use the "source"
# command:
#
#    (gdb) source /path/to/titaniumc/gdbinit
#
# You can also add that to a suitable ".gdbinit", so that these
# commands will sourced automatically.



# If we're debugging, it's a good bet that we are interested in
# diagnostic messages.

break Error
break Warning


# C++ strings are ubiquitous in the Titanium compiler.  This defines a
# "pstr" command for printing C++ strings correctly without spurious
# trailing garbage.

define pstr
print ($arg0).dat[0] @ (((Rep *) ($arg0).dat - 1)->len)
end
document pstr
print a C++ string
end
