#include "AST.h"
#include "CodeContext.h"
#include "UniqueId.h"
#include "pseudocode.h"

UniqueId ExprNode::temporary( "temp" );


void ExprNode::codeGen( CodeContext & )
{
  fatal("attempted to call codeGen() on an expression (" +
	pseudocode(this) + ")");
}


const string ExprNode::declareTemporary( LocalVars &vars )
{
  return declareTemporary( vars, *type() );
}


const string ExprNode::declareTemporary( LocalVars &vars, const TypeNode &type )
{
  return declareTemporary( vars, type.cType() );
}


const string ExprNode::declareTemporary( LocalVars &vars, const CtType &type )
{
  const string result = getTemporary();
  vars.declare( result, type );
  return result;
}


const string ExprNode::getTemporary()
{
  const string result = temporary.next();
  return result;
}


void ExprNode::resetTemporary()
{
  temporary.reset();
}
