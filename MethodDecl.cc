#define __SINST__		// g++ header bug
#include "AST.h"
#include "CtType.h"
#include "MethodDecl.h"
#include "code-operator.h"
#include "code.h"
#include "compiler.h"

TypeNode *MethodDecl::thisType() const
{
  if (modifiers() & Common::Static)
    return 0;
  else
    {
      TypeNode * const global = container()->asType();
      Modifiers mods = Common::None;
      
      if (global->hasReference() && (type()->isLocal() || (category() & Constructor)))
	mods = (Modifiers) (mods | Common::Local );
      
      mods = (Modifiers) (mods | sharingToModifiers( type()->sharing() ));

      return global->addModifiers( mods );
    }
}


////////////////////////////////////////////////////////////////////////

const string MethodDecl::cMethodNameBare() const {
  string result;
  if (category() != Constructor) {
    const TypeNode *self = thisType();
    if (self && self->isReference())
      {
	if (methodType()->isLocalInferred())
	  result += MANGLE_LQI_NAMESPACE_MARKER;
	else if (methodType()->isLocal())
	  result += MANGLE_LOCAL_NAMESPACE_MARKER;
	
	switch (methodType()->sharing()) {
	case Nonshared:
	  result += MANGLE_NONSHARED_NAMESPACE_MARKER;
	  break;
	case Polyshared:
	  result += MANGLE_POLYSHARED_NAMESPACE_MARKER;
	  break;
	default:
	  break;
	}
      }
  }

  return encodedMethodName(*name(), result);
} 

const string MethodDecl::cMethodNameDynamic() const
{
  string result = cMethodNameBare();

  foriter (param, type()->paramTypes()->allTypes(), TreeNode::TypeIter)
    {
      if ((*param)->hasFormalSignature())
	result += (*param)->formalSignature();
      else {
	if ((*param)->isLocalInferred())
	  result += MANGLE_LQI_FORMAL_MARKER;
	
	if ((*param)->isSharingInferred())
	  result += MANGLE_SHARING_INFERENCE_FORMAL_MARKER;

	switch ((*param)->sharing())
	  {
	  case Nonshared:
	    result += MANGLE_TYPE_NONSHARED_MARKER;
	    break;
	  case Polyshared:
	    result += MANGLE_TYPE_POLYSHARED_MARKER;
	    break;
	  default: 
	    break;
	  }

	result += (*param)->cType();
      }
    }

  return result;
}


const string MethodDecl::cMethodNameStatic() const
{
  // AK: PR459 -- use different end markers for methods and constructors
  // to avoid name collision
  return cMethodNameDynamic() + 
    (category() == Constructor ? MANGLE_CONSTRUCTOR_NAME_END_MARKER :
     MANGLE_METHOD_NAME_END_MARKER) +
    container()->cType();
}


const string Decl::cMethodNameDynamic() const
{
    invalidOperation( "cMethodNameDynamic" );
    return "";
}


const string Decl::cMethodNameStatic() const
{
    invalidOperation( "cMethodNameStatic" );
    return "";
}


////////////////////////////////////////////////////////////////////////


MethodDecl::MethodDecl( const string *name, TypeNode *type,
			Category _category, ClassDecl *classOrIntf,
			Modifiers mods, TreeNode *source )
  : MemberDecl( name, type, classOrIntf, mods, source ),
    _overrides( 0 ),
    _category( _category ),
    _isDead( false )
{
}


MethodTypeNode *MethodDecl::methodType() const
{
  TypeNode * const result = type();
  assert( result->kind() == Common::MethodKind );
  return static_cast< MethodTypeNode * >(result);
}
