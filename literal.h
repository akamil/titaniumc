#ifndef _LITERAL_H_
#define _LITERAL_H_

#include "ti_config.h"
#include <string>
#include "common.h"

// Java constants, and operations for constant folding

#include "runtime/gasnet/other/portable_inttypes.h"

typedef int64_t  int64; 
typedef uint64_t uint64; 
typedef int32_t  int32; 
typedef uint32_t uint32;
typedef int16_t  int16; 
typedef uint16_t uint16; 
typedef int8_t   int8;
typedef uint8_t  uint8;

class Literal {
public:
  Literal() { _kind = (Common::Kind) 0; }
  Literal(uint16 x) { _kind = Common::CharKind; value.i = x; }
  Literal(int32 x) { _kind = Common::IntKind; value.i = x; }
  Literal(int64 x) { _kind = Common::LongKind; value.i = x; }
  Literal(float x) { _kind = Common::FloatKind; value.f = x; }
  //Literal(double x) { _kind = Common::DoubleKind; d = x; }
  Literal(double x, string o) { _kind = Common::DoubleKind; value.d = x; orig = o; }
  Literal(bool x) { _kind = Common::BoolKind; value.b = x; }

  Common::Kind kind() const { return _kind; }
  Literal cast(Common::Kind to);

  Literal operator~();
  Literal operator!();
  Literal operator-();
  Literal operator*(Literal arg);
  Literal operator/(Literal arg);
  Literal operator%(Literal arg);
  Literal operator+(Literal arg);
  Literal operator-(Literal arg);
  Literal lsl(Literal arg); // logical shift left
  Literal rsl(Literal arg); // logical shift right
  Literal rsa(Literal arg); // arighmetic shift right
  // the damn g++ compiler feels like instantiating function.h on
  // Literal, and gets silly errors for <, <=, >, >= (in fold.cc)
  Literal lt(Literal arg);
  Literal le(Literal arg);
  Literal gt(Literal arg);
  Literal ge(Literal arg);
  Literal eq(Literal arg);
  Literal ne(Literal arg);
  Literal operator&(Literal arg);
  Literal operator|(Literal arg);
  Literal operator^(Literal arg);
  Literal operator&&(Literal arg);
  Literal operator||(Literal arg);

  // operators to extract info on the types of nodes
  bool boolValue() const { return value.b; }
  int64 intValue() const;
  double doubleValue() const { return kind() == Common::FloatKind ? value.f : value.d; }

  // Return a printed representation of this literal
  const string asString() const;
  const string javaStringValue() const;

  bool isNd() const;

protected:
  Literal arithPromote();
  Literal arithPromote(Common::Kind to);
  Common::Kind _kind;
  union {
    int64 i;
    float f;
    double d;
    bool b;
  } value;
  string orig;
};

ostream& operator<<(ostream& os, Literal l);

// make literal from java token
Literal intLiteral(const string &);
Literal longLiteral(const string &);
Literal floatLiteral(const string &);
Literal doubleLiteral(const string &);

#define IntegerKind Common::ByteKind: case Common::ShortKind: case Common::CharKind: case Common::IntKind: case Common::LongKind

#endif
