#ifndef INCLUDE_Subfield_h
#define INCLUDE_Subfield_h

#include <iosfwd>
#include "FieldDecl.h"

class SourcePosn;


class Subfield
{
public:
  Subfield( const FieldDecl &, const Subfield * = 0 );
  
  const TreeNode &source() const;
  string name() const;

private:
  const FieldDecl &decl;
  const Subfield * const parent;
};


////////////////////////////////////////////////////////////////////////


inline Subfield::Subfield( const FieldDecl &decl, const Subfield *parent )
  : decl( decl ),
    parent( parent )
{
}


inline const TreeNode &Subfield::source() const
{
  return *decl.source();
}


#endif // !INCLUDE_Subfield_h
