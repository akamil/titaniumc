#ifndef _TYPES_H
#define _TYPES_H

#include "ti_config.h"
#include "sharing.h"

class ClassDecl;
class CtType;
class EnactContext;
class Decl;
class QualSolver;
class TypeDecl;


/* A TypeNode represents a type. */

class TypeNode : public TreeNode
{
public:
  /*****************************************************************/

  /* For TreeNodes that represent types */

  // sanity check type qualifiers
  virtual void checkQualifiers() const;

  /* True for all types. */
  bool isTypeNode() const;
  /* True for the primitive types. */
  virtual bool isPrimitive() const;
  /* True for the reference types. */
  bool isReference() const;
  // true for reference types, arrays, and methods
  virtual bool hasReference() const;
  /* True for arithmetic types */
  bool isArithType() const;
  /* True for FP types */
  bool isFloatType() const;
  /* True for integral types */
  bool isIntegralType() const;
  /* True for immutable types */
  bool isImmutable() const;
  /* True for TypeNameNode based types */
  virtual bool isNamedType() const { return false; }
  /* True for the null/point/domain/rectdomain types. */
  virtual bool isNullType() const { return false; }
  virtual bool isPointType() const { return false; }
  virtual bool isDomainType() const { return false; }
  virtual bool isRectDomainType() const { return false; }
  virtual bool isDomainOrRectDomainType() const { 
    return isDomainType() || isRectDomainType();
  }
  bool isTitaniumBuiltinType() const {
    return isPointType() || isDomainType() || isRectDomainType();
  }
  /* True for all indexable types */
  virtual bool isArrayType() const { return false; }
  /* True for Java arrays */
  virtual bool isJavaArrayType() const { return false; }
  /* True for Titanium arrays */
  virtual bool isTitaniumArrayType() const { return false; }
  /* True for strings */
  bool isStringType() const;
  /* True for 'single' types */
  bool isSingle() const { return (modifiers() & Single) != 0; }
  /* True for 'local' types */
  bool isLocal() const { return (modifiers() & Local) != 0; }
  /* True for types inferred to be 'local' by LQI */
  bool isLocalInferred() const { return modifiers() & LocalInferred; }
  /* True for types inferred to be 'nonshared' or 'polyshared' by sharing inference */
  bool isSharingInferred() const { return modifiers() & SharingInferred; }
  /* True for types that contain no references. */
  virtual bool isAtomic() const;

  // cross-process visibility control
  Sharing sharing() const { return modifiersToSharing( modifiers() ); }
  void checkShared( const TreeNode &attribution, const char thing[], const char action[] ) const;
  virtual bool isSharedField() const { return sharing() == Shared; }

  /* The category of this type. */
  virtual Kind kind() const;

  /* Assuming THIS and T1 represent types, true iff T1 represents */
  /* a type that is assignment compatible with THIS (that is, iff */
  /* values of type T1 may be assigned (statically) to values of */
  /* type THIS.) */
  /* The special hack for integer constants is not done here */
  bool isAssignableFromType (const TypeNode* T1) const;

  /* Assuming THIS and T1 represent types, true iff T1 represents */
  /* a type that is castable to THIS (that is, iff values of type */
  /* T1 may be cast (statically) to values of type THIS.) */
  bool isCastableFrom (const TypeNode* T1) const;
 
  bool isComparableTo (const TypeNode* T1) const;
 
  Decision isCastableFromWithoutSharing (const TypeNode* T1) const;
 
 /* Assuming that THIS represents a method type and T1 is a list of */
  /* types, true iff T1 represents the types of a set of valid */
  /* arguments to a method whose type is THIS. */
  virtual bool isCallableWith (Modifiers actualModifiers,
			       const TreeNode* T1) const;

  /* True for all array types that are assignable (points are not).
     Default is true to avoid spurious error messages */
  virtual bool isWriteableArrayType() const { return true; }
  /* Return type of indexes for all array types */
  virtual TypeNode *indexType() const;
  /* The arity of a titanium type - 0 if undefined/bad */
  virtual int tiArity() const;

  void checktiArity(const char *name);

  /* Finds global pointers inside a type and returns a list of their names. 
     "toplevel_p" defaults to true...not declared here because it messes up
     KC++. */
  llist <string> *findGptrs(string myname, bool toplevel_p);

  /* True if two method types conflict (same parameter types and
     'local' specification for 'this') */
  bool methodsConflict(TypeNode *with);

  bool isAssignableFromExpr(TreeNode *with) const;
  virtual bool isAssignableFromConstant(const TreeNode *with) const;

  Common::Kind constantType() const;

  // overridden methods
  void print (ostream& os, unsigned depth) const;
  void pseudoprint (ostream& os, unsigned depth) const;
  bool typeIdent(const TreeNode* T1) const;
  bool typeIdentNM(const TreeNode* T1) const;
  bool typeIdentNS(const TreeNode* T1) const;

  // covariant restrictions
  TypeNode *clone() const = 0;
  TypeNode *deepClone() const = 0;
  TypeDecl *decl() const;

  virtual Modifiers modifiers() const;
  void modifiers (Modifiers v);
  // make a copy of this type with modifiers 'v'
  TypeNode *newModifiers(Modifiers v) const;

  // Make a copy of this type with some set of modifiers.  Will only
  // make a copy if the set of modifiers actually changed.  The "add"
  // versions add to the existing modifier set.  The "remove" versions
  // remove from the existing modifier set.
  
  TypeNode       *  withModifiers(Modifiers);
  TypeNode const *  withModifiers(Modifiers) const;
  TypeNode       *   addModifiers(Modifiers);
  TypeNode const *   addModifiers(Modifiers) const;
  TypeNode       *removeModifiers(Modifiers);
  TypeNode const *removeModifiers(Modifiers) const;

  TypeNode *componentAccessType( bool );

  void setElementType(TypeNode *to);

  // user-presentable name for this qualified type
  string typeName() const;

  // typeName() helper for dealing with arrays
  virtual void buildArrayTypeName( string & ) const;

  // Type name in C
  const CtType &cType() const;

  // code generation helpers
  virtual const string emitGlobalize( CodeContext &, const string &, const string & ) const;
  virtual const string emitLocalize( CodeContext &, const string & ) const;

  // cloning helpers
  virtual void emitNullifyLocal( ostream &, const string & ) const;

  // sequential consistency helpers
  const char *fencePreRead() const;
  const char *fencePostRead() const;
  const char *fencePreWrite() const;
  const char *fencePostWrite() const;
  ostream &fencePreRead( ostream & ) const;
  ostream &fencePostRead( ostream & ) const;
  ostream &fencePreWrite( ostream & ) const;
  ostream &fencePostWrite( ostream & ) const;

  // Any typenode can appear in the IF.
  bool checkIF(bool) const { return true; }

  virtual CfgExtent makeCfg();

  // mangled name is simply C type name, from CtTypes library
  string mangle();

  // template helpers
  TypeNode *fixTemplateActual( const string & );
  int templateDepth() const;

  /*****************************************************************/

  virtual TypeNode *unshareNode( const TreeNode *coparent );
  
  virtual bool isInferable() const;

  virtual void conservative( QualSolver & ) const;
  virtual bool enactType( const EnactContext & );
  virtual void identicalTo( const TypeNode &, QualSolver & ) const;
  virtual void protect( QualSolver & ) const;
  virtual void similarTo( const TypeNode &, QualSolver & ) const;
  virtual void subsimilarTo( const TypeNode &, QualSolver & ) const;
  virtual void stronglySubsumedBy( const TypeNode &, QualSolver & ) const;
  virtual void subcastTo( const TypeNode &, QualSolver &solver ) const;
  virtual void weaklySubsumedBy( const TypeNode &, QualSolver & ) const;
  virtual bool needsProtection() const;

  // Local qualification inference
  virtual void localCastTo( const TypeNode &, LocalSolver & ) const;
  
  // Sharing qualification inference
  virtual void sharingCastTo( const TypeNode &, SharingSolver & ) const;
  virtual void inferSharingEarly( SharingSolver & ) const;

  /*****************************************************************/
  
protected:
  TypeNode() :
    _modifiers((Modifiers)0),
    _cType(0)
    { }
  
  /* The set of types to which THIS can be converted by */
  /* assignment (a logical OR of Kinds). */
  /* (for primitive types only) */
  virtual Kind kindsAssignableTo() const { return (Kind) 0; }
  /* and by a cast */
  virtual Kind kindsCastableTo() const { return (Kind) 0; }

  /* The modifiers (only single so far) applied to this type */
  Modifiers _modifiers;

  // add single modifier to typenames
  string singleName(string s) const;

  // build the C type representation
  virtual const CtType &cTypeCreateBoxed() const;
  virtual const CtType &cTypeCreateUnboxed() const;
  virtual const CtType &cDescriptorTypeCreate() const;
  friend class TypeDecl;

  // Types are never freed.
  void free();

private:
  mutable const CtType *_cType;
};

/* An array type with given ELEMENTTYPE and DIMS dimensions.  When */
/* DIMS=0, equals ELEMENTTYPE. */
TypeNode* makeArrayType (TypeNode* elementType, int dims);
// Return a new Point<n> type
TypeNode *makePointType(int n);
// Return a new Domain<n> type
TypeNode *makeDomainType(int n);
// Return a new RectDomain<n> type
TypeNode *makeRectDomainType(int n);
// Return a new T array<n> type
TypeNode *makeTiArrayType(TypeNode *T, TreeNode *dims, int i);

/* The TypeNode corresponding to signature S. */
TypeNode* signatureToType (string s, llist<TypeNode *> *throws = NULL);

/* Return the type of METHOD, assuming that its parameter list and */
/* return type have been resolved. */
TypeNode* typeFromMethodDeclNode (TreeNode* method);

/* Useful types */
extern TypeNode
    *theIntType,
    *theBoolType,
    *theShortType,
    *theByteType,
    *theCharType,
    *theLongType,
    *theVoidType,
    *theFloatType,
    *theDoubleType,
    *theNullType,
    *theArrayInitializerType;

// The java type promotion rules for primitive types
TypeNode* arithPromoteType (TypeNode* type);
TypeNode* arithPromoteType (TypeNode* type0, TypeNode* type1);


// Return the Point<n> decl
ClassDecl *getPointDecl(const TypeNode *type);
// Return the Domain<n> decl
ClassDecl *getDomainDecl(const TypeNode *type);
// Return the RectDomain<n> decl
ClassDecl *getRectDomainDecl(const TypeNode *type);
// Return the T Array<n> decl
ClassDecl *getTiArrayDecl(const TypeNode *type);
// Return the T[] decl
ClassDecl *getJavaArrayDecl(const TypeNode *type);

/* Creates a comma separated list of type names.  Used for error messages. */
string TypeListToString(const TreeNode *);

TypeNode *arraySpecifiers2Type(llist<TreeNode *> *aspecs, TypeNode *base);

inline const CtType &TypeNode::cType() const
{
  if (!_cType) _cType = &cTypeCreateBoxed();
  return *_cType;
}

#endif
