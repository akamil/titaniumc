#ifndef _CLASS_DECL_H_
#define _CLASS_DECL_H_

#include <list>
#include "Decision.h"
#include "TypeDecl.h"

class CfCode;
class Instantiations;
class TreeListNode;
class TypeDeclNode;


class ClassDecl : public TypeDecl {
public:
  ClassDecl (const string* name, Decl* container, Modifiers mods, TreeNode* source);
  ClassDecl (const string* name, Decl* container, Modifiers mods, TreeNode* source, TypeNode* as_type);
  ClassDecl (const string* name, Decl* container);
  ClassDecl (const string* name, const string* declaredName, Decl* container, Modifiers mods, TreeNode* source);
  ClassDecl (const string* name, const string* declaredName, Decl* container, Modifiers mods, TreeNode* source, TypeNode* as_type);
  ClassDecl (const string* name, const string* declaredName, Decl* container);
  ClassDecl (ClassDecl& basis, TreeListNode& actuals, int depth );

  virtual bool isTypeReady() const;
  virtual bool typesResolved() const;
  virtual bool isFieldReady() const;
  virtual bool isMethodReady() const;

  bool hasEnviron() const { return true; }
  Environ* environ() {
    if (visits == 0) buildEnviron();
    return _environ;
  }
  Environ* environ(bool build) {
    if (build && visits == 0) buildEnviron();
    return _environ;
  }
  void environ (Environ* env) { _environ = env; }
    
  void drequire();

  bool hasContainer() const { return true; }
  Decl* container() const { return _container; }
  void container (Decl* decl) { _container = decl; }

  bool hasSource() const { return true; }
  TreeNode* source() const { return _source; }
  void source (TreeNode* tree) { _source = tree; }

  bool hasModifiers() const { return true; }
  Modifiers modifiers() const { return _modifiers; }
  void modifiers (Modifiers mods) { _modifiers = mods; }

  ClassDecl* superClass() const { return _superClass; }
  void superClass(ClassDecl* decl) { _superClass = decl; }
  bool hasSuperClass() { return category() == Class && _superClass; }

  llist<Decl*>* interfaces() { return _interfaces; }
  void interfaces (llist<Decl*>* ints) { _interfaces = ints; }
  bool hasInterfaces() const { return true; }

  bool allFieldsShared();
  void collectEmbeddedLocals( list< BadSubfield > &, const Subfield * );
  bool containsEmbeddedLocals();

  typedef set< Decl *, less< Decl * > > DeclSet;
  DeclSet &requires() { return _requirements; }
  void requires(Decl &);
  void requires(TypeNode &);

  typedef set< pair< Decl *, int > > DeclIntSet;
  DeclIntSet *initDepends() { return &_initDependencies; }
  void initDepends(Decl *, int);
  DeclIntSet *dependsOnInit() { return &_dependenciesOnInit; }
  void dependsOnInit(Decl *, int);

  bool valid() const { return _valid; }
  virtual void valid (bool now) { _valid = now; }

  string signature() const;

  const char* thisClassName() const;
  Category category() const;
  Common::Kind kind() const;

  string fullName( char = '.' );
  string mangledFullName( bool first = true );

  string mangledName( bool first = true );

  // declared name (relevant to nested classes)
  const string* declaredName() const { return _declaredName ? _declaredName : name(); }

  // names for error reporting
  string errorName();

  // is this an instantiation of an array pseudo-template?
  bool isArrayInstance() const;

  // is this a template basis?
  bool isTemplateBasis() const { return instantiations; }

  // Information on template instances
  Subst *templateArgs() { return _templateArgs; }
  void templateArgs(Subst *args) { _templateArgs = args; }

  // instantiate a template at some nesting depth;
  // assumes all error checking has already been done;
  // abort if we are not a template
  ClassDecl *instantiateDecl( TreeListNode &actuals, int depth );

  // generate the source for this instantiation by cloning the basis;
  // abort if we are not a template instantiation
  void instantiateSource();

  // instantiations of this template;
  // null if we are not a template
  Instantiations *instantiations;
  int numInstantiations;

  // arguments with which we are instantiated;
  // null if we are not a template instantiation
  TreeListNode *templateActuals;

  // template of which we are an instantiation;
  // null if we are not a template instantiation
  ClassDecl *templateBasis;

  // depth of nested template instantiations, for infinite instantiation pruning
  // zero if we are not a template instantiation
  int templateDepth;

  // the finalizer for this class
  Decl *finalizeMethod();

  // names for code generation
  const string cDescriptorName();
  const string cStaticFieldsStructName();
  const string cIntfMTName();
  const string cNativeName();
  const string cNullifyLocalName();
  const string cCheckpointName();
  const string cRestoreName();
  const string cArrayCheckpointName();
  const string cArrayRestoreName();
  string cFinalizerName();

  // code fragment generators
  const char *emitInterfaceList( ostream & );
  void includeSelf( CfCode & );
  void includeSupers( CfCode & );
  void includeRequirements( CfCode & );

protected:
  Decl* _container;
  Modifiers _modifiers;
  TreeNode* _source;
  Environ* _environ;
  ClassDecl* _superClass;
  llist<Decl*>* _interfaces;
  DeclSet _requirements;
  DeclIntSet _initDependencies;
  DeclIntSet _dependenciesOnInit;
  bool _valid;
  Subst *_templateArgs;
  Decision _allFieldsShared;
  const string* _declaredName;
  Decl *_finalizeMethod;

  void buildEnviron();

  void dumpAttributes( ostream & ) const;

private:
  TypeNameNode *invalid( const string * );
};


#endif // !_CLASS_DECL_H_


// Local Variables:
// c-file-style: "gnu"
// End:
