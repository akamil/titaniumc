#ifndef _INCLUDE_TITANIUM_CONFIG_H_
#define _INCLUDE_TITANIUM_CONFIG_H_

@TOP@

/* self-identification */
#undef PACKAGE
#undef VERSION

#undef TI_SYSTEM_NAME
#undef TI_SYSTEM_TUPLE
#undef TI_CONFIGURE_ARGS
#undef TI_BUILD_ID
#undef TOP_SRCDIR
#undef TOP_BUILDDIR
#undef INSTALL_PREFIX

/* the PAPI library */
#undef USE_PAPI

/* PAPI_multiplex_init() function */
#undef HAVE_PAPI_MULTIPLEX_INIT

/* Track compiler's use (and misuse) of memory */
#undef DEBUG_MEMORY_USAGE

/* Use conservative garbage collector in compiler */
#undef USE_TC_GC

/* Enable Java finalization */
#undef USE_FINALIZERS

/* Build and use our version of Omega library */
#undef BUILD_OMEGA

/* Sglobal inference */
#undef USE_SGLOBAL_INFERENCE

/* Disable GC */
#undef USE_GC_NONE

/* OS executable suffix */
#undef EXESUFFIX

/* Variable sized stack arrays at runtime */
#undef USE_RUNTIME_VARARRAY

/* MAXPATHLEN is defined in sys/param.h */
#undef USE_RUNTIME_SYS_PARAM_H_MAXPATHLEN

/* PATHSIZE and not MAXPATHLEN is defined in sys/param.h */
#undef USE_RUNTIME_SYS_PARAM_H_PATHSIZE

/* Using the GNU C compiler for the Cray at runtime */
#undef USE_RUNTIME_CRAY_GNU_C

/* Using the Cray T3E C compiler at runtime */
#undef USE_RUNTIME_CRAY_T3E_C

/* Use 64-bit point coordinates (workaround for PR291, see c-types/CtPointInstance.cc)*/
#undef USE_64BIT_POINTS

/* MAX_TIARITY size from --with-max-tiarity= */
#undef MAX_TIARITY

/* tlib version in use, as an unquoted string, eg: 1.4 */
#undef TLIB_VERSION

/* tlib version in use, as a formatted integer, eg: 010400 */
#undef TLIB_VERSION_VAL

/* C++ string class has erase() methods */
#undef HAVE_STRING_ERASE

/* C++ queue and stack templates take two type parameters */
#undef HAVE_BINARY_ADAPTORS

/* C++ template specialization syntax */
#undef HAVE_TEMPLATE_SPECIALIZATION

/* C++ string class requires inclusion of <std/bastring.cc> */
#undef HAVE_STD_BASTRING_CC

/* C++ character traits template class is called "char_traits" */
#undef HAVE_CHAR_TRAITS

/* C++ character traits template class can be specialized implicitly */
#undef IMPLICIT_CHAR_TRAITS

/* C++ stream state set using ios::setstate() */
#undef HAVE_IOS_SETSTATE

/* has usleep() */
#undef HAVE_USLEEP

/* has nanosleep() */
#undef HAVE_NANOSLEEP

/* has nsleep() */
#undef HAVE_NSLEEP

/* has sched_yield() */
#undef HAVE_SCHED_YIELD

/* has __builtin_expect */
#undef HAVE_BUILTIN_EXPECT

/* has pthread_kill_other_threads_np() */           
#undef HAVE_PTHREAD_KILL_OTHER_THREADS_NP

/* has pthread_setconcurrency() */           
#undef HAVE_PTHREAD_SETCONCURRENCY

/* has pthread_attr_setguardsize */
#undef HAVE_PTHREAD_ATTR_SETGUARDSIZE

/* has fmod() */
#undef HAVE_FMOD

/* has remainder() */
#undef HAVE_REMAINDER

/* has drem() */
#undef HAVE_DREM

/* has __func__ function name defined */
#undef HAVE_FUNC

/* have working __swab16(), __swab32(), and __swab64() macros */
#undef HAVE_SWABN_MACROS

/* support for glibc backtracing */
#undef HAVE_EXECINFO_H
#undef HAVE_BACKTRACE
#undef HAVE_BACKTRACE_SYMBOLS
#undef ADDR2LINE_PATH

/* check for a certain broken sstream implementation (early GNU g++ release) */
#undef SSTREAM_BROKEN

/* Define if "long-long-support.h" should be included */
#undef USE_LONG_LONG_SUPPORT_H

/* getrusage compatilibity */
#undef HAVE_GETRUSAGE_CONTEXT
#undef HAVE_GETRUSAGE_MAXRSS
#undef HAVE_GETRUSAGE_MEM
#undef HAVE_GETRUSAGE_PAGE

/* Define if stat64 contains long longs */
#undef STAT64_CONTAINS_LONG_LONGS

/* Define if you have an enameserver.h file that implements a global endpoint na
ming service */
#undef HAVE_ENAMESERVER_H

/* Define to a string in order to use a custom spawn function by default for udp-* backends */
#undef TI_CSPAWN_CMD

/* C Preprocessor command */
#undef CPP

/* Perl interpreter */
#undef PERL

/* portable inttypes support */
#undef HAVE_INTTYPES_H
#undef HAVE_STDINT_H
#undef HAVE_SYS_TYPES_H
#undef COMPLETE_INTTYPES_H
#undef COMPLETE_STDINT_H
#undef COMPLETE_SYS_TYPES_H

/* the host versions, when cross compiling */
#undef HOST_HAVE_INTTYPES_H
#undef HOST_HAVE_STDINT_H
#undef HOST_HAVE_SYS_TYPES_H
#undef HOST_COMPLETE_INTTYPES_H
#undef HOST_COMPLETE_STDINT_H
#undef HOST_COMPLETE_SYS_TYPES_H
#undef HOST_SIZEOF_CHAR
#undef HOST_SIZEOF_SHORT
#undef HOST_SIZEOF_INT
#undef HOST_SIZEOF_LONG
#undef HOST_SIZEOF_LONG_LONG
#undef HOST_SIZEOF_VOID_P

@BOTTOM@

#define TLIB_VERSION_VAL_1_0 010000
#define TLIB_VERSION_VAL_1_4 010400

/* these get us 64-bit file declarations under several Unixen */
/* they must come before the first include of features.h */
/* define them even on platforms lacking features.h */
#define _LARGEFILE64_SOURCE 1
#define _LARGEFILE_SOURCE 1
#ifdef HAVE_FEATURES_H
# include <features.h>
#endif

/* special GCC features */
#if ! defined (__GNUC__) && ! defined (__attribute__)
#define __attribute__(flags)
#endif

/* some related system-wide defs that cannot appear directly 
   in this file or autoconf will tamper with them */
#include <ti_extra_config.h>

#endif
