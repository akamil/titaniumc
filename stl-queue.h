#ifndef _INCLUDE_STL_QUEUE_H_
#define _INCLUDE_STL_QUEUE_H_

// portability helper for STL queues

#include <queue>
#include "ti_config.h"

#ifdef HAVE_BINARY_ADAPTORS	// ANSI standard
#define STL_QUEUE( element, container )  queue< element, container >
#else  // older libraries, like g++ 2.7.x
#define STL_QUEUE( element, container )  queue< container >
#endif

#endif // !_INCLUDE_STL_QUEUE_H_
