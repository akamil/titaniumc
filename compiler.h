// -*-C++-*-
// compiler.h: general compiler definitions

#ifndef _COMPILER_H
#define _COMPILER_H

#include <string>
#include <cstdio>
#include "llist.h"
#include "stats.h"

class ClassDecl;
class CompileUnitNode;
class Decl;
class TreeNode;

extern int DEBUG;
// Set to 1 to get some debugging output

extern int debugger_level;
// Set to 1 to output information for a debugger

extern bool line_directives;
// Set to true to emit "#line" directives in generated C code

extern char *dump_ast;
// Set to a filename to dump AST there

extern char *dump_phase;
// The compiler phase where the AST should be dumped

extern llist<CompileUnitNode *> *allFiles;
// The list of all CompileUnitNodes loaded during compilation

extern char *currentFilename;
// The name of the current file being worked on

void initTheSystem();
// Effects: initialise the compiler. Call before doing anything else

void load(const string name, bool titaniumFile, bool libraryFile);
// Effects: Opens and parses a file called 'name', and performs
// 'nameResolution' (build the file-level environment, load() all
// types explicitly named)
// If titaniumFile is true, the file is assumed to contain titanium source code
// if it is false, only Java code is allowed.

void load(const char * const name, bool titaniumFile, bool libraryFile);
// Effects: Opens and parses a file called 'name', and performs
// 'nameResolution' (build the file-level environment, load() all
// types explicitly named)
// If titaniumFile is true, the file is assumed to contain titanium source code
// if it is false, only Java code is allowed.

void load(const string name, FILE *file, bool titaniumFile, bool libraryFile);
// Effects: Parses file, assuming it is a file called 'name', and
//   performs 'nameResolution' (build the file-level environment,
//   load() all types explicitly named)
// If titaniumFile is true, the file is assumed to contain titanium source code
// if it is false, only Java code is allowed.

void staticSemantics();
// Effects: Run the static semantics on the currently loaded files.
//   Note that this may cause more files to be loaded...

void doMethodInlining();
// perform method inlining

bool isTitanium(string fileName);
// Returns: true if fileName ends in ".ti"

void backend();
// Backend


// Some phases of the front end

void buildEnvironments();
// Effects: builds the class-level environments for all the classes loaded
// since the last call to buildEnvironments

void verifyCircularity();
// Effects: Report errors for all classes or interfaces that are in a circular
//   inheritance dependency

void foldConstants(bool fold_opt_pass = false);
// Effects: Perform global constant folding

void foldInstantiations();
// Effects: Perform global constant folding on template instantiations

void singleAnalysis(CompileUnitNode *file);
// Effects: Perform single analysis on the CompileUnitNode file

				/* Standard Declarations */

extern Decl *unnamedPackage;	/* The unnamed package */

extern ClassDecl
   *ObjectDecl,			/* java.lang.Object */
   *RuntimeExceptionDecl,	/* java.lang.RuntimeException */
   *ThrowableDecl,		/* java.lang.Throwable */
   *JavaArrayDecl,		/* ti.internal.tiJArray */
   *TiArrayDecl,		/* ti.internal.tiArray */
   *TiArrayLDecl,		/* ti.internal.tiArray */
   *TiArrayM1Decl,		/* ti.internal.tiArrayM1 */
   *TemplateArgDecl,		/* ti.internal.tiTemplateArgument */
   *PointDecl,			/* ti.internal.tiPoint */
   *DomainDecl,			/* ti.internal.tiDomain */
   *RectDomainDecl,		/* ti.internal.tiRectDomain */
   *RectDomainM1Decl,		/* ti.internal.tiRectDomainM1 */
   *ErrorDecl,			/* java.lang.Error */
   *ExceptionDecl,		/* java.lang.Exception */
   *AssertionErrorDecl,		/* java.lang.AssertionError */
   *CloneableDecl,		/* java.lang.Cloneable */
   *StringDecl,			/* Java.lang.String */
   *StringBufferDecl,		/* java.lang.StringBuffer */
   *JavaLangClassDecl,		/* java.lang.Class */
   *TiLangCheckpointDecl,       /* ti.lang.Checkpoint */
   *RegionDecl,			/* ti.lang.Region */
   *SharedRegionDecl,		/* ti.lang.SharedRegion */
   *PrivateRegionDecl,		/* ti.lang.PrivateRegion */
   *NativeUtilsDecl;		/* ti.lang.NativeUtils */

extern Decl *TiArityDecl;	/* ti.internal.tiTemplateArgument.tiArity */


// Miscellaneous strings
extern const string
  *ObjectString,
  *UnknownString,
  *StringString,
  *StringBufferString,
  *JavaLangClassString,
  *ErrorString,
  *ExceptionString,
  *RuntimeExceptionString,
  *ThrowableString,
  *JavaArrayString,
  *RectDomainString,
  *RectDomainM1String,
  *DomainString,
  *CheckpointString,
  *TiArrayString,
  *TiArrayLString,
  *TiArrayM1String,
  *TemplateArgString,
  *PointString,
  *CloneableString,
  *TitaniumString,
  *InternalString,
  *JavaString,
  *LangString,
  *ArityString,
  *CopyString,
  *RegionString,
  *SharedRegionString,
  *PrivateRegionString;

#endif
