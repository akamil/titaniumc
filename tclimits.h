#ifndef INCLUDE_TCLIMITS_H
#define INCLUDE_TCLIMITS_H

/* Maximum depth of the inheritance hierarchy */
extern int maxInheritanceDepth;
/* Maximum nesting of template types */
extern int maxTemplateDepth;
/* Maximum number of instantiations of each template basis */
extern int maxTemplateInstantiations;

#endif
