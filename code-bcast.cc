#include <fstream>
#include "AST.h"
#include "CodeContext.h"
#include "CtType.h"
#include "code-assign.h"

const string IBroadcastNode::emitExpression( CodeContext &context )
{
  const string result = declareTemporary( context );
  const string process = proc()->emitExpression( context );
  const CtType &typeName = expr()->type()->cType();
  const string suffix = assignSuffix(assignType(expr()->type()), false);
  assert( !suffix.empty() );

  CodeContext subcontext( context );
  subcontext << "BROADCAST_BEGIN( " << typeName << ", " << process << " );" << endCline;
  

  const string value = expr()->emitExpression( subcontext );

  // everybody checks the processor number
  subcontext << "if (" << process << " == MYPROC) { " << endCline;
    checkPointerEscape(expr(), value, "", true, "broadcast", subcontext);
  subcontext << "}" << endCline;

  subcontext << "BROADCAST" << suffix << "( " << result 
	     << ", " << typeName << ", " << process << ", " 
	     << value << " );" << endCline;
  /*
  subcontext << "else" << endCline
	     << "  BROADCAST_RECEIVE( " << result << ", " << typeName << ", " << process << " );" << endCline;
  */  	     
  subcontext << "BROADCAST_END( " << result << ", " << typeName << ", " << process << " );" << endCline;
  
  return result;
}
