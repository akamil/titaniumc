#include "AST.h"
#include "PolyToCode.h"
#include "buildexpr.h"

CodeContext * current_context;
static MIVEcontext * current_MIVEcontext;
static treeSet * current_treeSet;

static string PolyFactor_to_string(PolyFactor f)
{
  // in code-MIVE.cc
  extern string PolyFactor_to_string(MIVEcontext *e, PolyFactor f,
				     CodeContext &context);

  return PolyFactor_to_string(current_MIVEcontext, f, *current_context);
} 

static const string nop(const string &a, const string &b) { return ""; }
static string insert_PolyFactor_in_current_treeSet(PolyFactor f)
{
  current_treeSet->insert(current_MIVEcontext->PolyFactorToTreeNode(f));
  return "";
}

/* Add to s a pointer to each TreeNode that will have emitExpression() call on it
   when (if) we later do PolyToCode(p, e, context). */
void usedInPoly(const Poly *p, MIVEcontext *e, treeSet *s)
{
  current_treeSet = s;
  current_MIVEcontext = e;
  (void) p->output(nop, nop, insert_PolyFactor_in_current_treeSet);
}

string PolyToCode(const Poly *p, MIVEcontext *e, CodeContext &context)
{
  current_context = &context;
  current_MIVEcontext = e;
  return p->output(sum, product, PolyFactor_to_string);
}

string PolyToCode(const Poly *p, TreeNode *WRTloop, CodeContext &context)
{
  return PolyToCode(p, MIVEcontextOfLoop(WRTloop), context);
}

const string product(const string &a, const string &b)
{
  return product(a, b, *current_context);
}
const string quotient(const string &a, const string &b)
{
  return quotient(a, b, *current_context);
}
const string quotient_with_check(const string &a, const string &b,
				 const string &y)
{
  return quotient_with_check(a, b, y, *current_context);
}
const string quotient_with_runtime_unitdivisor_opt(const string &a, const string &b)
{
  return quotient_with_runtime_unitdivisor_opt(a, b, *current_context);
}
const string sum(const string &a, const string &b)
{
  return sum(a, b, *current_context);
}
const string difference(const string &a, const string &b)
{
  return difference(a, b, *current_context);
}
