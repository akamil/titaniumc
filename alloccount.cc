#include "AST.h"
#include <cstdio>

#define N 10000
#define M (N * 100)

#if COUNT_ALLOCATION
extern const string int2string(int);

static map_cstring_to_int count;

void ALLOCCOUNT(const string &s)
{
  static int i = 0;
  ++i;
  ++count[s];
  if (i % N == 0)
    cout << "{{" << int2string(i) << "}}" << endl;
  if (i % M == 0) {
    cout << "{{" << endl;
    for (map_cstring_to_int::const_iterator j = count.begin();
	 j != count.end();
	 j++)
      cout << int2string((*j).second) << "	" << (*j).first << endl;
    cout << "}}" << endl;
  }
}

#endif
