#ifndef DEBUG_MEMORY_C_H
#define DEBUG_MEMORY_C_H

#if 1
/* __builtin_return_address(0) for the x86 */
#define RETADR1() ({ unsigned long CaLlEr;  asm("movl 4(%%ebp), %0" : "=r" (CaLlEr)); CaLlEr; })
#define RETADR2() ({ unsigned long oldEbP, CaLlEr; asm("movl (%%ebp), %0; movl 4(%0), %1" : "=r" (oldEbP), "=r" (CaLlEr)); CaLlEr; })
#else
#define RETADR1() ((unsigned long)__builtin_return_address(0))
#define RETADR2() ((unsigned long)__builtin_return_address(1))
#endif

/* #define this to save the return address of the caller to malloc/new/etc
   too (uses __builtin_return_address(1) which seems unreliable) - see
   PASS_RETADR
*/
#define GET_CALLERS_CALLER

#endif
