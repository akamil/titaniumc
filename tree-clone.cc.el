(defun initialize-buffer (nodes builder)
  (insert-c++-comment-prefix nodes builder)
  (insert "#include <iostream>\n"
	  "\n"
	  "#include \"AST.h\"\n"
	  "#include \"c-types/CtType.h\"\n"
	  "\n"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun defnode* (name parents fields methods states)
  (let* ((details (resolve-fields fields))
	 (kids (aref details 0)))
    (insert
     (format "\n%s *%s::clone () const\n" name name)
     "{\n"
     (format "  %s* copy = new %s(*this);\n" name name)
     (format "  copy->children = %s; return copy;\n" (if kids "copy->_children" "NULL"))
     "}\n"
     "\n"
     (format "%s *%s::deepClone () const\n" name name)
     "{\n"
     (format "  %s* const copy = clone();\n" name name)
     "  deepCloneChildren(*copy);\n"
     "  deepCloneSpecial(copy); \n"
     "  return copy;\n"
     "}\n\n")))


(defun deflistnode* (name methods)
  (let ((name (symbol-name name)))
    (insert "\n"
     name " *" name "::clone () const\n{\n"
     "  " name "* copy = new " name "; *copy = *this;\n"
     "  copy->children = new TreeNode*[_arity];\n"
     "  memcpy(copy->children, children, _arity * sizeof(*children));\n"
     "  return copy;\n}\n\n"
     name " *" name "::deepClone () const\n"
     "{\n"
     "  " name " * const copy = new " name ";\n"
     "  *copy = *this;\n"
     "  copy->children = new TreeNode*[_arity];\n"
     "  deepCloneChildren(*copy);\n"
     "  return copy;\n"
     "}\n\n")))
