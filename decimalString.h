#ifndef INCLUDE_DECIMALSTRING_H
#define INCLUDE_DECIMALSTRING_H

#include <cstdio>
#include "using-std.h"


static string inline decimalString(const string n) {
  if (n[0] == '0' && n[1] == 'x') {
    int hex;
    char z[10];
    sscanf((n.c_str()) + 2, "%x", &hex);
    sprintf(z, "%d", hex);
    return z;
  }
  return n;
}


#endif // !INCLUDE_DECIMALSTRING_H
