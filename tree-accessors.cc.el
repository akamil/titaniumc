(defun defattr* (name type value)
  (unless (string-match ":" (symbol-name name))
    (insert (format "%s TreeNode::%s () const\n" (to-c++-node-type type) name)
	    "{\n"
	    (format "    undefined (\"%s\");\n" name)
	    (format "    return %s;\n" value)
	    "}\n"
	    (format "void TreeNode::%s (%s)\n" name (to-c++-node-type type))
	    (format "{ undefined (\"%s\"); }\n" name))))

(defun defattr-default* (name type value)
  (insert (format "%s TreeNode::%s () const\n" type name)
	  "{\n"
	  (format "    return %s;\n" value)
	  "}\n"
	  (format "void TreeNode::%s (%s)\n" name type)
	  (format "{ undefined (\"%s\"); }\n" name)))

(defun defchild* (name type)
  (insert (format "%s TreeNode::%s () const\n" (to-c++-node-type type) name)
	  (format "{ undefined(\"%s\"); return 0; }\n" name)
	  (format "void TreeNode::%s (%s)\n" name (to-c++-node-type type))
	  (format "{ undefined(\"%s\"); }\n" name)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun initialize-buffer (nodes builder)
  (insert-c++-comment-prefix nodes builder)
  (insert "#include \"AST.h\"\n"
	  "\n"
	  "\n"))
