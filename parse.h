/* -*-C++-*-							*/
/* parse.h: Communication interface between the parser, the lexer, */
/* and the world. */

#ifndef _PARSER_H_
#define _PARSER_H_

#include <cstddef>
#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
#include "code.h"
#include "osstream.h"

class CompileUnitNode;


class File
{
public:
  File(const string name) : name(name), lineEndings(1, 0u) { }
  const string name;

  // line and column number for the given character offset
  typedef pair< unsigned, unsigned > Coordinates;
  Coordinates coordinates( unsigned long ) const;

  /* Record that N is the position of a newline in the source program */
  /* file.  It is assumed that this function is called with increasing */
  /* values of N. */
  void defineLineEnd (unsigned long N);
  
private:
  vector<unsigned long> lineEndings;
};

/* A SourcePosn is a character number (starting at 1) within a file. */
/* The position of a token is identified by the SourcePosn of its */
/* first character.  */
struct SourcePosn
{
  bool unknown() const { return file == NULL; }
  bool known() const { return !unknown(); }

  /* The line number on which character N of the input apppears. */
  size_t posnToLineNumber () const
  {
    return coordinates().first;
  }

  string asString() const;

  void dump( ostream & ) const;

  typedef File::Coordinates Coordinates;
  Coordinates coordinates() const
  {
    return unknown()
      ? Coordinates( 0, 0 )
      : file->coordinates( posn );
  }
  
  File *file;
  unsigned long posn;
};

/* The undefined position.  Used to as the position of nodes that */
/* have no meaningful position in the source. */
extern const SourcePosn NoSourcePosition;

/* Print a position in a format suitable for errors or warnings. */
ostream & operator << ( ostream &, const SourcePosn & );

// if true, error messages include column numbers
extern bool error_column;

/* The parser's interface to the rest of the compiler.  The */
/* root of the synthesized abstract syntax tree is to be passed to */
/* this function. */
void compileAST(CompileUnitNode* root);

/* The unique string* containing the current contents of TEXT. */
const string* intern(const char* text);
const string* intern(const string &);

void parse(FILE *fp, string name, string tmpname, bool titaniumFile);

class NameNode;
extern NameNode *buildName(const char *fullname, SourcePosn pos);

typedef struct {
  bool force_nobcheck; /* always disable bounds checking for this CUN */
  bool no_srcpos;      /* disable tracing srcpos #line info for the CUN */
} TiPragma;
static bool operator==(TiPragma a, TiPragma b) {
  return *&a == *&b;
}

typedef struct {
  CompileUnitNode *loaded;
  TiPragma pragma;
} CompileUnitNode_loadinfo_t;


////////////////////////////////////////////////////////////////////////


inline ostream & operator << ( ostream &os, const File &file )
{
  return os << file.name;
}


#endif
