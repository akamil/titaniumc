/* -*-C++-*-							*/
/* tokens.h: Communication interface between the parser and lexer */

#ifndef _TOKENS_H_
#define _TOKENS_H_

#include <cstddef>
#include <string>
#include "AST.h"
#include "common.h"
#include "parse.h"
#include "string16.h"

/* All tokens returned by the lexical analyzer have at least type as their */
/* semantic values (the value assigned to $1, $2, or etc.).  */
struct SimpTerminalInfo {
    SourcePosn posn;
};

/* Terminal types IDENTIFIER, INT_LITERAL, LONG_LITERAL, FLOAT_LITERAL, */
/* DOUBLE_LITERAL, CHARACTER_LITERAL, and STRING_LITERAL all have the */
/* text of the token (minus any surrounding quotes in the case of */
/* CHARACTER_LITERAL and STRING_LITERAL) pointed to by .val.  For all */
/* terminals, the .posn field contains the character number (starting */
/* at 1) of the first character of the token. */
struct TerminalInfo : public SimpTerminalInfo {
    const string *val;
};

struct StrTerminalInfo : public SimpTerminalInfo {
    const string16 *val;
};

struct CharTerminalInfo : public SimpTerminalInfo {
    uint16 val;
};

/* DeclaratorTuples allow you to pull information from declarators out to */
/* the declarations containing them. */
struct DeclaratorTuple {
    DeclaratorTuple() {} 
    DeclaratorTuple(long dims0, TreeNode* name0, TreeNode* initExpr0)
	: dims(dims0), name(name0), initExpr(initExpr0) {}
    long dims;			/* Number of []'s after name */
    TreeNode* name;	/* Name declared */
    TreeNode* initExpr;		/* Initializing expression (optional) */
};

llist<TreeNode*>* makeVarDeclNodes(bool isfinal, TypeNode *type,
				   llist<DeclaratorTuple> *decls);

/* The lexical analyzer. Returns syntactic category of next token */
/* input stream and puts other information about the token in the */
/* global variable yylval. */

#ifndef yylex
  #define yylex ti_yylex
#endif
#ifndef yyin
  #define yyin ti_yyin
#endif
#ifndef yychar
  #define yychar ti_yychar
#endif
#ifndef yylval
  #define yylval ti_yylval
#endif
#ifndef yynerrs
  #define yynerrs ti_yynerrs
#endif
#ifndef yyleng
  #define yyleng ti_yyleng
#endif
#ifndef yytext
  #define yytext ti_yytext
#endif

extern FILE *yyin;
extern int yylex();

/* The lexer's current position (a kludge). */
extern SourcePosn lexerPosition();

/* The parser.  The return value is not relevant.  In the absence of */
/* syntactic errors (at least), converts the contents of stdin into an */
/* abstract syntax tree and processes it with compileAST, below. */
extern int yyparse();

#endif
