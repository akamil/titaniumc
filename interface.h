#ifndef _INCLUDE_INTERFACE_H_
#define _INCLUDE_INTERFACE_H_

#include <iosfwd>

class ClassDecl;
class MethodDecl;


void CreateIntfMethods();
void EmitIntfMT(ClassDecl &, ostream& os);
int FindIntfMethodId(const MethodDecl &);


#endif
