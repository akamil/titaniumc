#include "AST.h"
#include "lgMacro.h"


const string ExprNode::emitPrefixOp( CodeContext &context, const char op )
{
    const string operand = child(0)->emitExpression( context );
    return string( 1, op ) + '(' + operand + ')';
}


const string UnaryPlusNode::emitExpression( CodeContext &context )
{
    return emitPrefixOp( context, '+' );
}


const string UnaryMinusNode::emitExpression( CodeContext &context )
{
    return emitPrefixOp( context, '-' );
}


const string ComplementNode::emitExpression( CodeContext &context )
{
    return emitPrefixOp( context, '~' );
}


const string NotNode::emitExpression( CodeContext &context )
{
    return emitPrefixOp( context, '!' );
}
