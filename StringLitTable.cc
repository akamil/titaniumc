#include "StringLitTable.h"
#include "ClassDecl.h"
#include "CodeContext.h"
#include "c-types/CtLocal.h"
#include "compiler.h"
#include "code.h"


const char StringLitTable::name[]      = "StringLiteral";
const char StringLitTable::tableName[] = "StringLitTable";

const CtLocal *StringLitTable::stringType = 0;


StringLitTable::StringLitTable()
{
  if (!stringType)
    stringType = new CtLocal( StringDecl->cType() );
}



size_t StringLitTable::operator[]( const string16 &text )
{
  const iterator hint = lower_bound( text );
  size_t id;
  
  if (hint != end() && hint->first == text)
    id = hint->second;
  else
    {
      id = size();
      insert( hint, make_pair( text, id ) );
    }

  return id;
}


void StringLitTable::declare( ostream &out ) const
{
  out << "static " << *stringType << " *" << name << "[MAX_BOX_PROCS];\n";
}


void StringLitTable::internAll( CodeContext &tableInit ) const
{
  if (empty())
    tableInit << name << "[MYBOXPROC] = 0;" << endCline;
  else
    {
      const_iterator entry;
      for (entry = begin(); entry != end(); ++entry)
	{
	  const string16 &text = entry->first;
	  const unsigned index = entry->second;
      
	  if (!text.empty())
	    {
	      tableInit << "static jchar chars_" << index << "[] = ";
	      printEscaped( tableInit, text );
	      tableInit << ';' << endCline;
	    }
        }
      tableInit << endCline;
      tableInit << name << "[MYBOXPROC] = (" << *stringType << " *)ti_malloc(" 
                << size() << "*sizeof(" << *stringType << "));" << endCline;
      tableInit << endCline;
      
      for (entry = begin(); entry != end(); ++entry) {
	  const string16 &text = entry->first;
	  const unsigned index = entry->second;

	  tableInit << name << "[MYBOXPROC][" << index << "] = "
                  << "java_string_localintern("
                  << "java_string_build_16("
		  << text.length() << ", " ;
                  if (text.empty()) tableInit << "0";
                  else tableInit << "chars_" << index;
          tableInit << "));" << endCline;
	}
    }
}
