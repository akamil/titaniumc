#ifndef _MEMBER_DECL_H_
#define _MEMBER_DECL_H_


#include "ClassDecl.h"
#include "decls.h"

class CtType;
class TreeNode;
class TypeNode;


class MemberDecl : public Decl {
public:
  bool hasType() const;
  TypeNode* type() const;
  void type( TypeNode* );

  bool hasContainer() const;
  ClassDecl* container() const;
  void container( Decl* decl );

  bool hasModifiers() const;
  Modifiers modifiers() const;
  void modifiers( Modifiers mods );

  bool hasSource() const;
  TreeNode* source() const;
  void source( TreeNode* tree );
  
  bool isStatic() const { return modifiers() & Common::Static; }

protected:
  MemberDecl( const string *, TypeNode *,
	      ClassDecl *, Modifiers, TreeNode *);

private:
  TypeNode *_type;
  ClassDecl *_container;
  Modifiers _modifiers;
  TreeNode *_source;
};


#endif // !_MEMBER_DECL_H_


// Local Variables:
// c-file-style: "gnu"
// End:
