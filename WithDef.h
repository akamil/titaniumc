#ifndef _include_WithDef_h_
#define _include_WithDef_h_


#include <iosfwd>
#include <string>
#include "using-std.h"


class WithDef {
public:
  WithDef( ostream &, const char [], const string & = "" );
  ~WithDef();

private:
  ostream &out;
  const char * const macro;
};


#endif // !_include_WithDef_h_
