#ifndef _INCLUDE_TypeContext_H_
#define _INCLUDE_TypeContext_H_


class TreeNode;


class TreeNode::TypeContext // The static analysis context for type resolution
{
public:
  TypeContext(bool &);
  TypeContext(const TypeContext &, bool &);

  Decl *package;
  TypeNode *cclass;
  Environ *fileEnv;
  Environ *typeEnv;
  bool noRecurse;
  bool resolveAsType;
  int depth;
  bool &postponed;
};

inline TreeNode::TypeContext::TypeContext(bool &_postponed)
  : package(0), cclass(0), fileEnv(0), typeEnv(0), noRecurse(0), 
     resolveAsType(1), depth(0), postponed(_postponed) {}

inline TreeNode::TypeContext::TypeContext(const TypeContext &ctx, bool &_postponed)
  : package(ctx.package), cclass(ctx.cclass), fileEnv(ctx.fileEnv), 
     typeEnv(ctx.typeEnv), noRecurse(ctx.noRecurse), 
     resolveAsType(ctx.resolveAsType), depth(ctx.depth), postponed(_postponed) {}

#endif
