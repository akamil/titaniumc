#ifndef _TI_GARBAGE_LIST_H_
#define _TI_GARBAGE_LIST_H_

#include <vector>
#include "using-std.h"

extern vector<char *> garbageVector1, garbageVectorN;

template <class T> T * garbageListAlloc(size_t n)
{
  T * p;
  if (n == 1) {
    p = new T;
    garbageVector1.push_back((char *) p);
  } else {
    p = new T [n];
    garbageVectorN.push_back((char *) p);
  }
  return p;
}

static inline size_t garbageListSize()
{
  return garbageVector1.size() + garbageVectorN.size();
}

static inline void garbageListFree(char *v) { }
extern void garbageListClear();

#endif /* _TI_GARBAGE_LIST_H_ */
