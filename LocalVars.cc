#include <iostream>
#include <cstddef>
#include "AST.h"
#include "CtRegistry.h"
#include "CtType.h"
#include "LocalVars.h"
#include "parse.h"


LocalVars::LocalVars( CtRegistry &dependencies )
  : dependencies( dependencies )
{
}


LocalVars::LocalVars( const LocalVars &parent )
  : dependencies( parent.dependencies )
{
}


void LocalVars::depend( const CtType &type )
{
  dependencies.add( type );
}


void LocalVars::declare( const string &name, const CtType &type )
{
  insert( value_type( type, name ) );
  depend( type );
}


void LocalVars::codeGen( ostream &sink, int lineNumber ) const
{
  const_iterator scan = begin();
  while (scan != end())
    {
      const_iterator last = upper_bound( (*scan).first );
      
      sink << (*scan).first << ' ' << (*scan).second;      
      while (++scan != last)
	sink << ", " << (*scan).second;

      sink << ";\n";

      if (lineNumber)
	sink << "#line " << lineNumber << '\n';
    }
}


#if defined(__KCC) || defined(__PGI) || defined(__HP_aCC)
void *LocalVars::operator new( size_t )
{
  return 0;
}
#endif
