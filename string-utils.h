#ifndef INCLUDE_STRING_UTILS_H
#define INCLUDE_STRING_UTILS_H

#include <string>
#include "using-std.h"


inline bool hasPrefix( const string &complete, const string &prefix )
{
  return prefix == complete.substr( 0, prefix.length() );
}


inline bool hasSuffix( const string &complete, const string &suffix )
{
  return complete.length() >= suffix.length()
    && suffix == complete.substr( complete.length() - suffix.length() );
}


#endif // !!INCLUDE_STRING_UTILS_H
