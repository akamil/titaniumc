#ifndef _INCLUDE_ST_FIELD_H_
#define _INCLUDE_ST_FIELD_H_


class TreeNode;
class Decl;
class TypeNode;
class TreeListNode;


class TreeNode::FieldContext // The static analysis FieldContext for resolveField
{
public:
  FieldContext();
  Decl *currentPackage;
  CompileUnitNode *currentCompileUnit;
  TypeNode *currentClass;
  TreeListNode *methodArgs;
  bool inStatic; // true while in static code
  Modifiers thisModifiers;
  Modifiers constructorSharingLUB;
};


inline TreeNode::FieldContext::FieldContext()
  : currentPackage(NULL),
    currentCompileUnit(NULL),
    currentClass(NULL),
    methodArgs(NULL),
    inStatic(false),
    thisModifiers(None),
    constructorSharingLUB(None)
{
}


#endif
