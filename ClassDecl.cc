#define __SINST__		// g++ header bug
#include "AST.h"
#include "CfCode.h"
#include "code.h"
#include "code-util.h"
#include "compiler.h"
#include "ctBox.h"
#include "decls.h"
#include "errors.h"
#include "string-erase.h"
#include "string-utils.h"


#define BASE_TO_HDR(basevar, base, header, end_str) if ((basevar) == base) return string(header) + end_str


const string ClassDecl::cDescriptorName()
{ 
    return MANGLE_CLASS_DESC(+, cType());
}

const string ClassDecl::cStaticFieldsStructName() 
{
  return MANGLE_CLASS_DESC(+, cType()) + "_static_fields";
}


const string ClassDecl::cIntfMTName()
{
    return MANGLE_CLASS_INTF(+, cType());
}

const string ClassDecl::cNativeName()
{
  return "native/" + cType() + ".c";
}


const string ClassDecl::cNullifyLocalName()
{
  return MANGLE_CLASS_NULLLOC(+, cType());
}


const string ClassDecl::cCheckpointName()
{
  return MANGLE_CLASS_CHECKPOINT(+, cType());
}


const string ClassDecl::cRestoreName()
{
  return MANGLE_CLASS_RESTORE(+, cType());
}


const string ClassDecl::cArrayCheckpointName()
{
  if (asType()->isImmutable() && !asType()->isAtomic())
    return MANGLE_CLASS_ARRAY_CHECKPOINT(+, cType());
  return "NULL";
}


const string ClassDecl::cArrayRestoreName()
{
  if (asType()->isImmutable() && !asType()->isAtomic())
    return MANGLE_CLASS_ARRAY_RESTORE(+, cType());
  return "NULL";
}


static const string *finalize_string = intern("finalize");
static Decl *object_finalizer = NULL;


Decl *ClassDecl::finalizeMethod() {
  /* AK (PR832): make sure only finalizer that overrides Object's is used */
  if (!object_finalizer) {
    object_finalizer = ObjectDecl->environ()->lookupProper(finalize_string, Decl::Method);
  }
  if (!_finalizeMethod) {
    EnvironIter methods = environ()->lookupFirstProper(finalize_string, Decl::Method);
    foriter (method, methods, EnvironIter) {
      if (method->type()->methodsConflict(object_finalizer->type())) {
	_finalizeMethod = &*method;
	break;
      } else {
	method->source()->warning("finalize-qualifiers") 
	  << method->errorName() 
	  << " will not be used for finalization in "
	  << errorName() << ";"
	  << " required signature is "
	  << "\"[public/protected] void finalize()\"\n";
      }
    }
  }
  return _finalizeMethod;
}

string ClassDecl::cFinalizerName()
{
  Decl *finalizer = finalizeMethod();
  if (finalizer)
    return MANGLE_CLASS_FINALIZER(+, ((ClassDecl *) finalizer->container())->cType());
  else
    return "NULL";
}


void ClassDecl::includeSelf( CfCode &os )
{
  if (asType()->isPointType())
    os.include( "domains" );
  
  else if (asType()->isTitaniumArrayType())
    {
      assert( !asType()->isLocal() );
      os.include( asType()->cType() );
    }

  else if (asType()->isJavaArrayType())
    {
    /* we don't currently generate type headers for each Java array type */
    }

  else
    os.include( cType() );
}


void ClassDecl::includeSupers( CfCode &os )
{
    Decl * const super = (this == ObjectDecl ? 0 : superClass());
    if (super) super->includeSelf( os );
    
    foriter (interface, elements( interfaces() ), ListIterator< Decl * >)
	(*interface)->includeSelf( os );
}


const char *ClassDecl::emitInterfaceList( ostream &os )
{
    if (interfaces()) {
  	os << "static const interface_header *interfaces[] = {\n";
	foriter (interface, elements( interfaces() ), ListIterator< Decl * >) {
	    ClassDecl &decl = static_cast< ClassDecl & >(**interface);
	    assert( decl.category() == Decl::Interface );
	    os << "  &" << decl.cDescriptorName() << ",\n";
	}
	os << "  0\n};\n\n";
	return "interfaces";
    } else
	return "0";
}


void ClassDecl::includeRequirements( CfCode &os )
{
    DeclSet &classes = requires();
    
    for (DeclSet::iterator dependency = classes.begin();
	 dependency != classes.end(); ++dependency)
	(*dependency)->includeSelf( os );

    os << '\n';
}


Common::Kind ClassDecl::kind() const
{
  if (category() == Interface)
    return Common::InterfaceKind;
  else
    return superClass()
      ? Common::ClassKind
      : Common::ImmutableKind;
}


bool ClassDecl::isArrayInstance() const
{
  if (isInSystemPackage( this, "ti", "internal"))
    {
      if (hasPrefix( *name(), "tiArray<"  )) return true;
      if (hasPrefix( *name(), "tiArrayL<" )) return true;
      if (hasPrefix( *name(), "tiJArray<" )) return true;
    }

  return false;
}


// stubs

const string Decl::cIntfMTName()
{
    invalidOperation("cIntfMTName");
    return "";
}


const string Decl::cNativeName()
{
    invalidOperation("cNativeName");
    return "";
}


void Decl::includeSupers(CfCode &)
{
    invalidOperation("includeSupers");
}


void Decl::includeSelf(CfCode &)
{
    invalidOperation("includeSelf");
}
