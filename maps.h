#ifndef _tc_map_h_
#define _tc_map_h_

// This file, maps.h, is normally included from AST.h, and nowhere else.

#include <string>
#include <map>
#include <set>

#ifdef __xlC__
/* bug workaround for xlC STL bug */
#define _CONST_ 
#else
#define _CONST_ const 
#endif

typedef map< TreeNode *, TreeNode * > map_tree_to_tree;
typedef map< Decl *, Decl * > map_decl_to_decl;
typedef map< TreeNode *, map_tree_to_tree > map_tree_tree_to_tree; 
typedef map< int, treeSet * > map_int_to_treeSet;
typedef map< TreeNode *, treeSet * > map_tree_to_treeSet;
typedef map< TreeNode *, map_tree_to_treeSet > map_tree_tree_to_treeSet;
typedef map< int, const Poly * > map_polyfactor_to_poly;
typedef map< int, map_polyfactor_to_poly > map_polyfactor_polyfactor_to_poly;
typedef map< void *, string > map_void_to_string;
typedef map< void *, int > map_void_to_int;
typedef map< void *, map_void_to_int > map_void_void_to_int;
typedef map< int, TreeNode * > map_polyfactor_to_treenode;
typedef map< int, int > map_polyfactor_to_int;
typedef map< int, Poly * > map_int_to_poly;
typedef map< void *, map_int_to_poly > map_void_int_to_poly;
typedef map< TreeNode *, MIVEcontext * > map_tree_to_MIVEcontext;
typedef map< TreeNode *, MIVE * > map_tree_to_MIVE;
typedef map< TreeNode *, map_tree_to_MIVE > map_tree_tree_to_MIVE;
typedef map< TreeNode *, CFGset * > map_tree_to_CFGset;
typedef map< CfgNode *, CFGset * > map_CFGnode_to_CFGset;
typedef map< TreeNode *, const CFGuset * > map_tree_to_CFGuset;
typedef map< CfgNode *, const CFGuset * > map_CFGnode_to_CFGuset;
typedef map< int, string > map_int_to_string;
typedef map< TreeNode *, llist< MIVE * > * > map_tree_to_MIVElist;
typedef map< TreeNode *, llist< const MIVE * > * > map_tree_to_cMIVElist;
typedef map< int, Poly * > map_int_to_Poly;
typedef map< TreeNode *, map_int_to_Poly > map_tree_int_to_Poly;
typedef map< int, map_tree_int_to_Poly > map_int_tree_int_to_Poly;
typedef map< TreeNode *, map_int_tree_int_to_Poly >
    map_tree_int_tree_int_to_Poly;
typedef map< const MIVE *, map_int_tree_int_to_Poly >
    map_cMIVE_int_tree_int_to_Poly;
typedef map< int, int > map_int_to_int;
typedef map< TreeNode *, int > map_tree_to_int;
typedef map< TreeNode *, string > map_tree_to_string;
typedef map< TreeNode *, map_int_to_int > map_tree_int_to_int;
typedef map<_CONST_ string, int > map_cstring_to_int;
typedef map< string, string > stringmap;
typedef map< string, llist<TreeNode * > * > map_string_to_list;
typedef map< string, map_string_to_list > map_string_string_to_list;
typedef map< string, void * > map_string_to_void;
typedef map< TreeNode *, map_string_to_void > map_tree_string_to_void;
typedef map< MIVE *, int > map_MIVE_to_int; 
typedef map< const MIVE *, int > map_cMIVE_to_int; 
typedef map< int, string * > map_int_to_pstring;
typedef map< TreeNode *, map_int_to_pstring > map_tree_int_to_pstring;


#define generic template <class K, class V>

/* Apply f to every value in the map. */
generic void apply_to_all(const map<K, V> &m, void (*f)(V))
{
  for (TYPENAME map<K, V>::const_iterator i = m.begin(); i != m.end(); i++)
    (*f)((*i).second);
}

/* Apply delete to every value in the map. */
generic void delete_values_in_map(const map<K, V> &m)
{
  for (TYPENAME map<K, V>::const_iterator i = m.begin(); i != m.end(); i++)
    delete (*i).second;
}

/* Apply f to every key/value pair in the map. */
generic void apply_to_all(const map<K, V> &m, void (*f)(K, V))
{
  for (TYPENAME map<K, V>::const_iterator i = m.begin(); i != m.end(); i++)
    (*f)((*i).first, (*i).second);
}

#undef generic

#endif // _tc_map_h_
