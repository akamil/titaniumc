#ifndef _INCLUDE_TITANIUM_OPTIMIZE_H_
#define _INCLUDE_TITANIUM_OPTIMIZE_H_

extern bool opt_lift;
extern bool opt_sr;
extern bool opt_osr;
extern bool opt_uae;
extern bool opt_deadvar;
extern bool opt_unreachable;
extern bool opt_copy;
extern bool opt_foldextra;
extern bool opt_subexpr;
extern bool opt_stoptifu;
extern bool opt_use_new_dom;
extern bool sequential_consistency;
extern bool opt_local;
extern bool opt_sharing;

extern bool debug_sr;
extern bool debug_lift;
extern bool debug_qual;

#include "AST.h"
#include "code-util.h"
#include "pseudocode.h"
#include "cfg.h"

#ifndef DEBUG_DEFAULT
#define DEBUG_DEFAULT 0
#endif

/* too verbose for most purposes */
#define DEBUG_DOM_VERBOSE (DEBUG_PHASE_ENABLED("domv", currentFilename))

/* dumpphase opt turns on lots of debugging stuff. */
#define DEBUG_OPT DEBUG_PHASE_ENABLED("opt", currentFilename)
#define DEBUG_DEPS (DEBUG_OPT || DEBUG_PHASE_ENABLED("deps", currentFilename))
#define DEBUG_STOPTIFU (DEBUG_OPT || DEBUG_PHASE_ENABLED("stoptifu", currentFilename))
#define DEBUG_TOUCH (DEBUG_OPT || DEBUG_PHASE_ENABLED("touch", currentFilename))
#define DEBUG_BC (DEBUG_OPT || DEBUG_PHASE_ENABLED("bc", currentFilename))
#define DEBUG_SR (DEBUG_OPT || DEBUG_PHASE_ENABLED("sr", currentFilename))
#define DEBUG_OSR (DEBUG_OPT || DEBUG_PHASE_ENABLED("osr", currentFilename))
#define DEBUG_UAE (DEBUG_OPT || DEBUG_PHASE_ENABLED("uae", currentFilename))
#define DEBUG_DEADVAR (DEBUG_OPT || DEBUG_PHASE_ENABLED("deadvar", currentFilename))
#define DEBUG_UNREACHABLE (DEBUG_OPT || DEBUG_PHASE_ENABLED("unreachable", currentFilename))
#define DEBUG_COPY (DEBUG_OPT || DEBUG_PHASE_ENABLED("copy", currentFilename))
#define DEBUG_SUBEXPR (DEBUG_OPT || DEBUG_PHASE_ENABLED("subexpr", currentFilename))
#define DEBUG_LOOP_INVAR (DEBUG_OPT || DEBUG_PHASE_ENABLED("invar", currentFilename))
#define DEBUG_LOOP_ANAL (DEBUG_OPT || DEBUG_PHASE_ENABLED("loopanal", currentFilename))
#define DEBUG_LIFT (DEBUG_OPT || DEBUG_PHASE_ENABLED("lift", currentFilename))
#define DEBUG_UPDATEPOINTBEFORESTMTNODE (DEBUG_OPT || DEBUG_PHASE_ENABLED("upbs", currentFilename))
#define DEBUG_DOM \
    (DEBUG_DOM_VERBOSE || DEBUG_OPT || DEBUG_PHASE_ENABLED("dom", currentFilename))

#define LOOP_ARITY(l) (dynamic_cast<ForEachStmtNode *>((l))->tiArity())

#include "Worklist.h"

TreeNode *CloneTree(const TreeNode *t, bool cleanUseDef = false);
PrimitiveLitNode *newBoolLit(bool b, SourcePosn p = NoSourcePosition);

bool isFullDomainLoop(TreeNode *t);
TreeNode *replaceNode(TreeNode *t, TreeNode *x, TreeNode *y);
TreeNode *replaceOneNode(TreeNode *t, TreeNode *x, TreeNode *y);

treeSet *allDefs(treeSet *vars);
treeSet *allUses(const TreeNode *t);
treeSet *LvalueUses(const TreeNode *t);
treeSet *RvalueUses(const TreeNode *t);
treeSet *ObjectNodes(TreeNode *e);
treeSet *treeSetAdjoin(treeSet *s, const TreeNode *t);
treeSet *adjoin(treeSet *s, const TreeNode *t);
treeSet *destructively_intersect(treeSet *s, treeSet *t);
treeSet *destructively_union(treeSet *s, treeSet *t);
treeSet *copy(treeSet *s);

map_tree_to_treeSet *
    destructive_convert_cfgmap_to_astmap(map_CFGnode_to_CFGset *r);
treeSet *
    destructive_convert_cfgset_to_treeset(CFGset *s);
map_tree_to_treeSet *
    destructive_convert_cfgmap_to_astmap(map_CFGnode_to_CFGuset *r);
treeSet *
    convert_cfguset_to_treeset(const CFGuset *s);

treeSet *exprset_to_stmtset(treeSet *t);

bool isSingletonContaining(TreeNode *e, treeSet *s);

CFGset *destructively_intersect(CFGset *s, CFGset *t);
bool contains(const CFGset *s, const CfgNode *t);
CFGset *adjoin(CFGset *s, const CfgNode *t);

bool set_contains_any(const treeSet *s, const llist<TreeNode *> *l);
bool noDefsFromSet(const TreeNode *t, const treeSet *s);

void initInvar(TreeNode *l);
void foundInvariant(const TreeNode *inv, TreeNode *WRTloop);
bool isInvar(const TreeNode *t, const TreeNode *WRTloop);
bool isLiftedInvar(const TreeNode *t, const TreeNode *WRTloop);
void liftedInvar(const TreeNode *t, const TreeNode *WRTloop);
void freeInvarInfo(const TreeNode *);
void cloneMIVEandInvarInfo(const TreeNode *src, const TreeNode *dest);

TreeNode *valueTransferred(TreeNode *def, TreeNode *use);

void propagateChanges(Worklist *w, TreeNode *t);

void lazyOptimize();

CFGset * list_to_CFGset(llist<CfgNode *> *l);
treeSet * list_to_set(llist<TreeNode *> *l);
llist<TreeNode *> * set_to_list(const treeSet *s);
bool isSubset(const treeSet &s, const treeSet &t);
bool isSubset(const treeSet *s, const treeSet *t);
bool treeSetContains(const treeSet *s, const TreeNode *t);
bool treeSetContains(const treeSet &s, const TreeNode *t);
bool contains(const treeSet *s, const TreeNode *t);
bool contains(const treeSet &s, const TreeNode *t);
bool block_contains(TreeNode *t, BlockNode *b);
void print(llist<TreeNode *> *s, ostream &os, int m);
void printTreeSet(const treeSet *u, ostream &os, int indent);
void printTreeSetWithParents(const treeSet *s, ostream &os, int m);
treeSet *filter(bool f(const TreeNode *t), const treeSet *s);
treeSet *destructively_filter(bool f(const TreeNode *t), treeSet *s);

BlockNode *SortVarDeclsInBlock(BlockNode *b);
SwitchBranchNode *SortVarDeclsInSwitch(SwitchBranchNode *b);

// TreeNode *removeUselessVarDecls(TreeNode *t, treeSet *useless);

TreeNode *deleteDeclsFromBlocks(TreeNode *t, treeSet *s);

#define PCODE2(os, t) ((os) << pseudocode((t)), "")
#define PLCODE2(os, t) ((os).flush(),					\
			((os) << "{" << (t)->oper_name() << " at " <<	\
			 (t)->position().asString() << "} "),		\
			(os) << pseudocode((t)),			\
			"")
#define POPER2(os, t) (((os) << "{" << (t)->oper_name() << " at " <<	\
			(t)->position().asString() << "}"),		\
		       "")
#define SHOWMIVE2(os, m) ((m)->print((os)), "")

#define PCODE(t) PCODE2(cout, t)
#define SAFEPCODE(t) (((t) == NULL) ? "NULL" : PCODE(t))
#define SAFEPLCODE2(os, t) (((t) == NULL) ? "NULL" : PLCODE2(os, t))
#define POPER(t) POPER2(cout, t)
#define PLCODE(t) PLCODE2(cout, t)
#define SHOWMIVE(t) SHOWMIVE2(cout, t)

#define QCFG(x) (unique((void *) (x)))

int doCopyPropagation(TreeNode *t);
int doCommonSubexprElim(TreeNode *t);
TreeNode *doDeadVariableRemoval(TreeNode *t);
TreeNode *removeUnreachableStmts(TreeNode *t);

void inferLocal();
void inferSharing();

void invalidateCFG();
void redoDefUse(TreeNode *t);

extern void computeMethodDominators (TreeNode* body, bool ignoreNSE);

#endif /* _INCLUDE_TITANIUM_OPTIMIZE_H_ */
