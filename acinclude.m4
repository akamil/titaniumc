dnl TI_CHECK_BACKEND(backend-name, test-pattern, [1=disabledbydefault])
AC_DEFUN([TI_CHECK_BACKEND],[
GASNET_FUN_BEGIN([$0($1,...,$3)])
pushdef([enable_backend],[enable_[]patsubst([$1], -, _)])
pushdef([check_pat],[patsubst([$2], [\$[^\$|]*], yes)])
AC_MSG_CHECKING(for $1 backend)
reason=
GASNET_IF_ENABLED_WITH_AUTO($1,[Enable/disable use of the $1 backend (if available)], [
  if test "$2" = "check_pat"; then
    enable_backend=yes 
    reason=" (enabled by configure arguments)"
  else
    AC_MSG_RESULT(failed)
    name='$2'
    val="$2"
    AC_MSG_ERROR([Passed --enable-$1, but required support not found: $name=$val])
  fi
],[
  enable_backend=no
  reason=" (disabled by configure arguments)"
],[
  if test "$2" = "check_pat"; then
    if test "$3" = "1"; then
      enable_backend=no 
      reason=" (disabled by default)"
    else 
      enable_backend=yes 
      reason=" (auto-detected)"
    fi
  else
    enable_backend=no
  fi
])
if test $enable_backend = yes; then
  BACKENDS="$BACKENDS $1"
fi
AC_MSG_RESULT($enable_backend$reason)
popdef([enable_backend])
GASNET_FUN_END([$0($1,...,$3)])
])

dnl TI_TCBUILD_CFLAGS(name, description, action-if-not-set)
AC_DEFUN([TI_TCBUILD_CFLAGS],[
  GASNET_FUN_BEGIN([$0($1,$2,...)])
  AC_MSG_CHECKING(tcbuild $2 flags)
  AC_ARG_WITH(tcbuild-cflags-$1,
    GASNET_OPTION_HELP(with-tcbuild-cflags-$1=FLAGS,$2 flags used by tcbuild),
    ,
    [$3])
  TCBUILD_CFLAGS_[]translit($1, [a-z], [A-Z])="$withval"
  AC_SUBST(TCBUILD_CFLAGS_[]translit($1, [a-z], [A-Z]))
  AC_MSG_RESULT($withval)
  GASNET_FUN_END([$0($1,$2,...)])
])

