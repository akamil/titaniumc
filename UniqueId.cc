#include "UniqueId.h"
#include "osstream.h"


const string UniqueId::next()
{
  ostringstream assemble;
  assemble << prefix << '_' << sequence++;

  return assemble.str();
}
