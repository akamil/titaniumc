#include <iostream>
#include "Poly.h"

bool debug_poly = true;

void showsum(Poly p, Poly q)
{
  const Poly * sum = p.add(&q);
  cout << "The sum of "; 
  p.print(cout);
  cout << " and ";
  q.print(cout);
  cout << " is ";
  sum->print(cout);
  cout << '\n';
}

void showproduct(Poly p, Poly q)
{
  const Poly * product = p.mult(&q);
  cout << "The product of "; 
  p.print(cout);
  cout << " and ";
  q.print(cout);
  cout << " is ";
  product->print(cout);
  cout << '\n';
}

main()
{
  PolyFactor a('A');
  PolyFactor b(11, "beta");
  PolyFactor c('c');
  
  Poly pa(a);
  Poly pb(b);
  Poly pc(c);
  Poly p1(1);
  Poly pa_1(pa.add(&p1));
  Poly pb_1(pb.add(&p1));
  Poly pc_1(pc.add(&p1));
  Poly pa_b_2(pb_1.add(&pa_1));
  Poly pb_a_2(pa_1.add(&pb_1));
  Poly zip(pa_b_2.sub(&pb_a_2));
  Poly pzero = Poly(0);
  
  showsum(pa_1, pb_1);
  showsum(pa_1, *pa_1.negate());
  showproduct(pa_1, pa_1);
  showproduct(pa_1, pb_1);
  showproduct(pa_1, zip);
  showproduct(pa_1, pzero);
  showproduct(pa_1, p1);
  showproduct(*pa_1.mult(&pa_1), pb_1);
  showproduct(*pa_1.mult(&pa_1), pa_1);
  showproduct(*pa_1.mult(&pc_1), pb_1);
}
