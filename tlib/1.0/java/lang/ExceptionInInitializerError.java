package java.lang;

public class ExceptionInInitializerError extends LinkageError {
  private Throwable cause = null;

  public ExceptionInInitializerError(Throwable thrown) {
    super((String)null);
    cause = thrown; 
  }
  public Throwable getException() { return cause; }
  public Throwable getCause() { return cause; }
}

