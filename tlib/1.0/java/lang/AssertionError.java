package java.lang;

/* simple implementation by Dan Bonachea */

public class AssertionError extends Error {
     public AssertionError() {}
     public AssertionError(boolean detailMessage){ super(""+detailMessage); }
     public AssertionError(char detailMessage)   { super(""+detailMessage); }
     public AssertionError(double detailMessage) { super(""+detailMessage); }
     public AssertionError(float detailMessage)  { super(""+detailMessage); }
     public AssertionError(int detailMessage)    { super(""+detailMessage); }
     public AssertionError(long detailMessage)   { super(""+detailMessage); }
     public AssertionError(Object detailMessage) { super(detailMessage.toString()); }
}
