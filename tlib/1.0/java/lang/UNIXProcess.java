/*
 * @(#)UNIXProcess.java	1.18 97/02/24
 * 
 * Copyright (c) 1995, 1996 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * This software is the confidential and proprietary information of Sun
 * Microsystems, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Sun.
 * 
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
 * SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 * 
 * CopyrightVersion 1.1_beta
 * 
 */

package java.lang;

import java.io.*; 
import java.util.Hashtable;
import java.util.Vector;
import java.util.Enumeration;

/* java.lang.Process subclass in the UNIX environment.
 * 
 * @author Dave Brown
 */

public class UNIXProcess extends Process {
    /* hastable of subprocesses, indexed by pid */
    static Hashtable subprocs = null;

    private boolean isalive = false;
    private int exit_code = 0;
    private FileDescriptor local stdin_fd;
    private FileDescriptor local stdout_fd;
    private FileDescriptor local stderr_fd;
    private FileDescriptor local sync_fd;
    int pid;

    /* stdin of the process */
    private OutputStream stdin_stream;

    /* streams directly to the process */

    private InputStream stdout_stream;
    private InputStream stderr_stream;

    static {
	subprocs = broadcast new Hashtable() from 0;
    }

    private UNIXProcess() {}

    private native local int forkAndExec(String cmd[], String env[]) throws java.io.IOException;

    /*
    This method is called in response to receiving a signal
    */
  private static synchronized void deadChild(int pid, int exitcode) {
    System.out.println(pid + " is now dead.");
    for(Enumeration e = subprocs.elements(); e.hasMoreElements(); ) {
      System.out.print("Hashtable contains: ");
      System.out.println(e.nextElement());
    }
    System.out.println((subprocs == null) ? "subprocs is null" : "true"); 
    Object o = subprocs.get(new Integer(pid));
    System.out.println((o == null) ? "unixprocess is null" : o);
    UNIXProcess p = (UNIXProcess) o;
    if (p != null) {
      synchronized (p) {
	p.isalive = false;
	subprocs.remove(new Integer(pid));
	p.exit_code = exitcode;
	/* if there is anyone doing io from the process - wake them up*/
      }
    }
  }


    UNIXProcess(String cmdarray[], String env[]) throws java.io.IOException {

	stdin_fd = new FileDescriptor();
	stdout_fd = new FileDescriptor();
	stderr_fd = new FileDescriptor();
	sync_fd = new FileDescriptor();
	
	pid = forkAndExec(cmdarray, env);
	System.out.println("Forked PID: " + pid);
	/* parent process */
	isalive = true;
	stdin_stream = new BufferedOutputStream(new FileOutputStream(stdin_fd));
	stdout_stream = new FileInputStream(stdout_fd);
	stderr_stream = new FileInputStream(stderr_fd);
	
	subprocs.put(new Integer(pid), this);

	/* notfiy child to start */
	FileOutputStream f = new FileOutputStream(sync_fd);
	f.write('A'); // for Audrey.
	f.close();
    }

    public OutputStream getOutputStream() {
	return stdin_stream;
    }

    public InputStream getInputStream() {
	return stdout_stream;
    }

    public InputStream getErrorStream() {
	return stderr_stream;
    }

    public synchronized int waitFor() throws InterruptedException {
        if (isalive) {
	    waitUntilDeath();
	}
	return exit_code;
    }

    public synchronized int exitValue() { 
	if (isalive) {
	    throw new IllegalThreadStateException("process hasn't exited");
	}
	return exit_code; 
    }

  private native void waitUntilDeath();
  
  public native void destroy();

  public void finalize() {
    try {
      stdin_stream.close();
    } catch (IOException e){}
    
    /* for now we just swallow exceptions here */
    try {
      stdout_stream.close();
    } catch (IOException e){}
    try {
      stderr_stream.close();
    } catch (IOException e){}
  }
  
  public String toString() {
    return "unixprocess: " + pid;
  }

}

/* The current problem w/ UNIXProcess is that the file descriptors
 * associated with stdout and stderr must be closed when the process
 * exits.  Ideally, we'd close these in the Process's finalize(), but
 * practice shows that the finalizer doesn't get run quickly enough:
 * under stress-testing we run out of file descriptors and the whole
 * runtime dies (ugly).  
 * Closing the fd's after the exec'd process exits creates the race
 * condition that the caller of exec() must read() the entire stream
 * before exit, which doesn't work reliably.
 * As a workaround, we create a thread each to read from stdout/stderr
 * and save the data in a buffer, and Process.getInputStream()/getErrorStream()
 * read from these buffers.  It doesn't matter in this case that the fd's
 * are closed.  The process's output can be read long after it exits.  
 * The code that closes the streams is synchronized around the readers
 * finishing.
 */



