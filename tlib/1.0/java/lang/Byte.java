/* Copyright 2003, Dan Bonachea <bonachea@cs.berkeley.edu> */

package java.lang;

public final class Byte extends Number {
  public static final Class TYPE = byte.class;

  public static final byte MIN_VALUE = (byte)0x80;
  public static final byte MAX_VALUE = (byte)0x7F;

  public Byte(byte single value) { this.value = value; }

  public static Byte single valueOf(String single s) { return new Byte(parseByte(s,10)); }
  public static Byte single valueOf(String single s, int single radix) { return new Byte(parseByte(s,radix)); }

  public static Byte decode(String nm) {
    int radix = 10;
    boolean neg = false;
    if (nm.charAt(0) == '-') { neg = true; nm = nm.substring(1); }
    if (nm.startsWith("0x"))      { radix = 16; nm = nm.substring(2); }
    else if (nm.startsWith("0X")) { radix = 16; nm = nm.substring(2); }
    else if (nm.startsWith("#"))  { radix = 16; nm = nm.substring(1); }
    else if (nm.startsWith("0"))  { radix = 8;  nm = nm.substring(1); }

    if (neg) nm = "-" + nm;

    return Byte.valueOf(nm, radix);
  }  

  public static String toString(byte b) { return String.valueOf(b); }
  public String toString() { return toString(value); }
  public int hashCode() { return value; }

  public boolean equals(Object obj) {
    return (obj != null) && (obj instanceof Byte) && ((Byte)obj).value == value;
  }
  public int compareTo(Byte other) {
    return (int)(other.value - value);
  }
  public int compareTo(Object other) { return compareTo((Byte)other); }

  public static byte single parseByte(String single s) {
    return parseByte(s,10);
  }
  public static byte single parseByte(String single s, int single radix) {
    long single val = Long.parseLong(s,radix);
    if (val < MIN_VALUE || val > MAX_VALUE) throw new NumberFormatException();
    else return (byte single)val;
  }

  public byte single byteValue() { return (byte single)value; }
  public short single shortValue() { return (short single)value; }
  public int single intValue() { return (int single)value; }
  public long single longValue() { return (long single)value; }
  public float single floatValue() { return (float single)value; }
  public double single doubleValue() { return (double single)value; }

  private byte single value; 
}
