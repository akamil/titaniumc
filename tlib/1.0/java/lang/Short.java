/* Copyright 2003, Dan Bonachea <bonachea@cs.berkeley.edu> */

package java.lang;

public final class Short extends Number {
  public static final Class TYPE = short.class;

  public static final short MIN_VALUE = (short)0x8000;
  public static final short MAX_VALUE = (short)0x7FFF;

  public Short(short single value) { this.value = value; }

  public static Short single valueOf(String single s) { return new Short(parseShort(s,10)); }
  public static Short single valueOf(String single s, int single radix) { return new Short(parseShort(s,radix)); }

  public static Short decode(String nm) {
    int radix = 10;
    boolean neg = false;
    if (nm.charAt(0) == '-') { neg = true; nm = nm.substring(1); }
    if (nm.startsWith("0x"))      { radix = 16; nm = nm.substring(2); }
    else if (nm.startsWith("0X")) { radix = 16; nm = nm.substring(2); }
    else if (nm.startsWith("#"))  { radix = 16; nm = nm.substring(1); }
    else if (nm.startsWith("0"))  { radix = 8;  nm = nm.substring(1); }
     
    if (neg) nm = "-" + nm;

    return Short.valueOf(nm, radix);
  }                                                                                            

  public static String toString(short b) { return String.valueOf(b); }
  public String toString() { return toString(value); }
  public int hashCode() { return value; }

  public boolean equals(Object obj) {
    return (obj != null) && (obj instanceof Short) && ((Short)obj).value == value;
  }
  public int compareTo(Short other) {
    return (int)(other.value - value);
  }
  public int compareTo(Object other) { return compareTo((Short)other); }

  public static short single parseShort(String single s) {
    return parseShort(s,10);
  }
  public static short single parseShort(String single s, int single radix) {
    long single val = Long.parseLong(s,radix);
    if (val < MIN_VALUE || val > MAX_VALUE) throw new NumberFormatException();
    else return (short)val;
  }

  public byte single byteValue() { return (byte single)value; }
  public short single shortValue() { return (short single)value; }
  public int single intValue() { return (int single)value; }
  public long single longValue() { return (long single)value; }
  public float single floatValue() { return (float single)value; }
  public double single doubleValue() { return (double single)value; }

  private short single value; 
}
