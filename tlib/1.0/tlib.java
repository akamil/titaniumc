// Everything you could ever want in a runtime!

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.LineNumberInputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.io.PushbackInputStream;
import java.io.RandomAccessFile;
import java.io.SequenceInputStream;
import java.io.StreamTokenizer;
import java.io.StringBufferInputStream;
import java.io.UTFDataFormatException;
import java.lang.AbstractMethodError;
import java.lang.ArithmeticException;
import java.lang.ArrayIndexOutOfBoundsException;
import java.lang.ArrayStoreException;
import java.lang.AssertionError;
import java.lang.Boolean;
import java.lang.Byte;
import java.lang.Character;
import java.lang.Class;
import java.lang.ClassCastException;
import java.lang.ClassCircularityError;
import java.lang.ClassFormatError;
import java.lang.ClassLoader;
import java.lang.ClassNotFoundException;
import java.lang.CloneNotSupportedException;
import java.lang.Cloneable;
import java.lang.Compiler;
import java.lang.Double;
import java.lang.Error;
import java.lang.Exception;
import java.lang.ExceptionInInitializerError;
import java.lang.Float;
import java.lang.IllegalAccessError;
import java.lang.IllegalAccessException;
import java.lang.IllegalArgumentException;
import java.lang.IllegalMonitorStateException;
import java.lang.IllegalThreadStateException;
import java.lang.IncompatibleClassChangeError;
import java.lang.IndexOutOfBoundsException;
import java.lang.InstantiationError;
import java.lang.InstantiationException;
import java.lang.Integer;
import java.lang.InternalError;
import java.lang.InterruptedException;
import java.lang.LinkageError;
import java.lang.Long;
import java.lang.Math;
import java.lang.NegativeArraySizeException;
import java.lang.NoClassDefFoundError;
import java.lang.NoSuchFieldError;
import java.lang.NoSuchMethodError;
import java.lang.NoSuchMethodException;
import java.lang.NullPointerException;
import java.lang.Number;
import java.lang.NumberFormatException;
import java.lang.Object;
import java.lang.OutOfMemoryError;
import java.lang.Process;
import java.lang.Runnable;
import java.lang.Runtime;
import java.lang.RuntimeException;
import java.lang.SecurityException;
import java.lang.SecurityManager;
import java.lang.Short;
import java.lang.StackOverflowError;
import java.lang.String;
import java.lang.StringBuffer;
import java.lang.StringIndexOutOfBoundsException;
import java.lang.System;
import java.lang.Thread;
import java.lang.ThreadDeath;
import java.lang.ThreadGroup;
import java.lang.Throwable;
import java.lang.UNIXProcess;
import java.lang.UnknownError;
import java.lang.UnsatisfiedLinkError;
import java.lang.VerifyError;
import java.lang.VirtualMachineError;
import java.lang.Void;
import java.util.BitSet;
import java.util.Date;
import java.util.Dictionary;
import java.util.EmptyStackException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import java.util.Random;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.Vector;
// arity-dependent classes in ti.domains are loaded by hardcoding in static.cc
import ti.io.BulkDataInput;
import ti.io.BulkDataInputStream;
import ti.io.BulkDataOutput;
import ti.io.BulkDataOutputStream;
import ti.io.BulkRandomAccessFile;
import ti.lang.Checkpoint;
import ti.lang.Complex;
import ti.lang.CompileSettings;
import ti.lang.DoubleOp;
import ti.lang.Handle;
import ti.lang.IntOp;
import ti.lang.LongOp;
import ti.lang.ObjectOp;
import ti.lang.Param;
import ti.lang.PrivateRegion;
import ti.lang.Reduce;
import ti.lang.Region;
import ti.lang.RegionInUse;
import ti.lang.Scan;
import ti.lang.Serialize;
import ti.lang.SharedRegion;
// import ti.lang.TickCounter;
import ti.lang.Timer;
import ti.lang.Ti;
import ti.lang.Trace;
import ti.lang.CounterNotLocalException;
import ti.lang.EventNotAvailException;
import ti.lang.PAPICounter;
import ti.lang.PAPIException;
import ti.lang.PAPINotAvailException;
import ti.lang.TooManyCounterException;
import ti.util.Demangle;

// The following are disabled because of parsing problems with
// "Point".  No great loss ... actually getting AWT to work under
// Titanium would be a tremendously difficult task.

// import java.applet.*;
// import java.awt.*;


// The following is disabled because java.net.InetAddress invokes the
// clone() method on a Java array.  Method invocation on Java arrays
// is not yet supported by the backend.

// import java.net.*;

class tlib {
  // force certain popular array types to appear in the tlib because they occur 
  // heavily in applications (save compile time)

Point<1> [1d]		x1;
Point<2> [1d]		x2;
Point<3> [1d]		x3;
RectDomain<1> [1d]	x4;
RectDomain<2> [1d]	x5;
RectDomain<3> [1d]	x6;
boolean [1d]		x7;
boolean [2d]		x8;
boolean [3d]		x9;
byte [1d]		x10;
byte [2d]		x11;
byte [3d]		x12;
char [1d]		x13;
char [2d]		x14;
char [3d]		x15;
short [1d]		x16;
short [2d]		x17;
short [3d]		x18;
int [1d]		x19;
int [2d]		x20;
int [3d]		x21;
long [1d]		x22;
long [2d]		x23;
long [3d]		x24;
float [1d]		x25;
float [2d]		x26;
float [3d]		x27;
double [1d]		x28;
double [2d]		x29;
double [3d]		x30;

}

