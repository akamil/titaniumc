package ti.lang;

public class PrivateRegion extends Region
{
  public native void delete() throws RegionInUse;
};
