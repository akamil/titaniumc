package ti.lang;

public interface LongOp
{
  long eval(long x, long y);
}
