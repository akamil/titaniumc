package ti.lang;

public interface ObjectOp
{
  Object eval(Object x, Object y);
}
