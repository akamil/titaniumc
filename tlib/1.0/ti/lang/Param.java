package ti.lang;

final public class Param {
    static public int unsigned(String p, String description, int def) {
	return def;
    }
    static public int unsigned(String p, int def) {
	return def;
    }
    static public int integer(String p, String description, int def) {
	return def;
    }
    static public int integer(String p, int def) {
	return def;
    }
    static public boolean bool(String p, String description,
			       boolean def) {
	return def;
    }
    static public boolean bool(String p, boolean def) {
	return def;
    }
    static public int range(String p, String description,
			    int lo, int hi, int def) {
	return def;
    }
    static public int range(String p, int lo, int hi, int def) {
	return def;
    }
}
