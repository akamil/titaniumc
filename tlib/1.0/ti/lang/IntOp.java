package ti.lang;

public interface IntOp
{
  int eval(int x, int y);
}
