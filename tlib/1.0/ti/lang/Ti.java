package ti.lang;

// Titanium standard functions
public class Ti {
  native public static int thisProc();
  native public static int single numProcs();
  native public static single void barrier();
  native public static int single [1d] single local myTeam();
  native public static void poll();
  
  private static int autogc_cnt = 0;
  native private static void disable_autogc();
  native private static void enable_autogc();
  public static void suspend_autogc() {
    if (autogc_cnt == 0) disable_autogc();
    autogc_cnt++;
  }
  public static void resume_autogc() {
    if (autogc_cnt == 0) 
      throw new RuntimeException("mismatched call to Ti.resume_autogc() on proc "+Ti.thisProc());
    autogc_cnt--;
    if (autogc_cnt == 0) enable_autogc();
  }
}
