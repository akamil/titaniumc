package ti.lang;

public class PAPIException extends RuntimeException{
    
    public PAPIException(String s){
	super(s);
    }
}
