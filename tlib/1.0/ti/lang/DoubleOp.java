package ti.lang;

public interface DoubleOp
{
  double eval(double x, double y);
}
