package ti.lang;

public immutable class Handle {
  private long data;
  public native void syncNB(); /* sync a single NB op */
  public static native void syncNBI(); /* sync all outstanding NBI ops */
}
