package ti.lang;

public class CompileSettings {
  // these are replaced with the correct values during compilation
  public static final boolean optimize = false;
  public static final boolean debug = false;
  public static final boolean bcheck = false;
}
