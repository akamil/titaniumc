package ti.lang;

// ensure correct attribution for ti_malloc calls in Region native code
#pragma TI nosrcpos

abstract public class Region
{
  native static private void init();
};
