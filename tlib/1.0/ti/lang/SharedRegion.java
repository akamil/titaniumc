package ti.lang;

final public class SharedRegion extends Region
{
  public inline single SharedRegion() { } /* exists solely for the single */
  public single native void delete() throws RegionInUse single;
};
