package ti.lang;

public class PAPINotAvailException extends PAPIException{

    public PAPINotAvailException(){
	super("PAPI is not available on this machine");
    }
}
