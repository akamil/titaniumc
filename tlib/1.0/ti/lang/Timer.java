package ti.lang;

public class Timer {
  public static final double MAX_SECS = 1.8446744073709551616e13;
  private long curTick, elapTick;

  public Timer() { /* curTick = 0; elapTick = 0; DOB- field clear is redundant, already guaranteed by Java */ }
  
  public inline void start() {
    curTick = TickCounter.current();
  }

  public inline void stop() {
    elapTick += TickCounter.current() - curTick;
  }

  public inline void reset() { elapTick = 0; }
  
  public inline double secs()   { return TickCounter.ticksToNanos(elapTick) / 1.0E9; }
  public inline double millis() { return TickCounter.ticksToNanos(elapTick) / 1.0E6; }
  public inline double micros() { return TickCounter.ticksToNanos(elapTick) / 1.0E3; }
  public inline double nanos()  { return TickCounter.ticksToNanos(elapTick); }

}

