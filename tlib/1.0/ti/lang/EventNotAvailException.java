package ti.lang;

public class EventNotAvailException extends PAPIException{

    public EventNotAvailException(int event){
	super("Event " + event + " is not available on this platform");
    }
}
