package ti.lang;
import java.io.IOException;


final public class NativeUtils
{
    // These utility methods are used by native code.  As much of the
    // logic as possible has been pulled out of the native code so
    // that local qualification inference can see what is going on.

    // Build a string out of an array of Unicode characters.  This is
    // used by native utility function java_string_build_16().

    public static String local buildString( char[] local chars )
    {
	return new String( chars );
    }

    
    // Build a string out of an array of ASCII bytes.  Used by native
    // utility function java_string_build_8().

    public static String local buildString( byte[] local bytes )
    {
	return new String( bytes, 0 );
    }


    // Extract an ASCII byte array out of a string.  Used by native
    // utility functions jstring2Cstring(), stringToDouble(), and
    // jopen().

    public static byte[] local getBytes( String string )
    {
	int length = string.length();
	byte[] local result = new byte[ length ];
	
	string.getBytes( 0, length, result, 0 );
	return result;
    }
    

    // Intern a local string.  Used by literal string table
    // initialization code.
    
    public static String local intern( String local string )
    {
	return (String local) string.intern();
    }

    // Throw an array bounds exception because the proposed access 
    // strays outside of array bounds  
    // Uses position information if available (otherwise pass NULL)
    // Used by native utility macros JAVA_ARRAY_CHECK_GLOBAL() and
    // JAVA_ARRAY_CHECK_LOCAL().

    public static void throwArrayIndexOutOfBoundsException( 
      int arraylength, int offset, int length, String local where) 
      throws ArrayIndexOutOfBoundsException {
      String mesg;
      
      if (length < 0 || arraylength < 0) // sanity check
        throw new InternalError("inconsistent information in throwArrayIndexOutOfBoundsException()");

      if (offset < 0) 
        mesg = "Java array index out of range (too low): " + offset +  " < 0";
      else 
        mesg = "Java array index out of range (too high): " + 
               (offset+length-1) +  " >= " + arraylength;

      if (where != null) mesg += ", (in " + where + ")";

      throw new ArrayIndexOutOfBoundsException(mesg);
    }


    // Throw an illegal argument exception.  Used by native method
    // java.lang.UNIXProcess.forkAndExec().

    public static void throwIllegalArgumentException() throws IllegalArgumentException
    {
	throw new IllegalArgumentException();
    }

    // Throw an ClassNotFoundException with the given message.  
    // Used by native utility function tossClassNotFoundException_str()

    public static void throwClassNotFoundException( String local message ) throws ClassNotFoundException
    {
	throw new ClassNotFoundException( message );
    }

    // Throw an illegal argument exception with the given message.  
    // Used by native utility function tossIllegalArgumentException_str()

    public static void throwIllegalArgumentException( String local message ) throws IllegalArgumentException
    {
	throw new IllegalArgumentException( message );
    }

    // Throw an I/O exception with the given message.  Used by native
    // utility function tossIOException().

    public static void throwIOException( String local message ) throws IOException
    {
	throw new IOException( message );
    }

    // Throw an illegal monitor state exception. Used by native method
    // java.lang.Object.wait(), notify(), and notifyAll(). 

    public static void throwIllegalMonitorStateException( String local message ) throws IllegalMonitorStateException
    {
	throw new IllegalMonitorStateException( message );
    }


    // Throw an out-of-memory error.  Used by native method
    // java.lang.UNIXProcess.forkAndExev().
    
    public static void throwOutOfMemoryError() throws OutOfMemoryError
    {
	throw new OutOfMemoryError();
    }
    

    // Throw a not-cloneable exception.  Use by native utility
    // function allocateClone().

    public static void throwCloneNotSupportedException() throws CloneNotSupportedException
    {
        // !! In Java, the exception has a message -- the class name.
 	throw new CloneNotSupportedException();
    }
    
    
    // Throw a class cast exception.  Used by native utility
    // functions castToClass() and castToInterface().  Also
    // used by code generated for certain cast expressions.

    public static void throwClassCastException() throws ClassCastException
    {
	// !! In Java, the exception has a message -- the name of the
	// dynamic type of the instance being cast.
	
 	throw new ClassCastException();
    }
    
    
    // Throw a null pointer exception.  Used by native utility macros
    // CHECK_NULL_GLOBAL() and CHECK_NULL_LOCAL().

    public static void throwNullPointerException(String local where) 
      throws NullPointerException
    {
        String mesg = "in ";
        /* be careful about using string literals here, because null-checks may occur 
           very early, before the string literal tables are loaded */
        if (where != null && mesg != null) {
          mesg = mesg + where;
          throw new NullPointerException(mesg);
        } else throw new NullPointerException();
    }


    // Throw an array store exception.  Used by System.arraycopy

    public static void throwArrayStoreException(String local mesg) 
      throws ArrayStoreException
    {
	throw new ArrayStoreException(mesg);
    }


    // Throw a number format exception.  Used by native utility
    // function stringToDouble().

    public static void throwNumberFormatException( String message ) throws NumberFormatException
    {
	throw new NumberFormatException( message );
    }


    // Add a property to a property table.  Used by native utility
    // function addProperty().

    public static void addProperty( java.util.Properties props,
				    String local key,
				    String local value )
    {
	props.put( key, value );
    }

    // Output an exception to stderr. used by top-level exception "handler"

    public static void dumpException( Throwable exn ) {
        /* don't assume anything about state of system */
        String desc = exn.toString();
        if (desc == null) desc = "unidentified exception";
        if (desc != null && System.err != null) System.err.println(desc);
        if (System.err != null) exn.printStackTrace();
    }

    // Force instantiation of certain classes used by native code
    
    private static void forceInstantiation() {
      // used by runtime/primitive_desc.c
      java.lang.Void v;
      java.lang.Boolean b;
      java.lang.Character c;
      java.lang.Byte b2;
      java.lang.Short s;
      java.lang.Integer i;
      java.lang.Long l;
      java.lang.Float f;
      java.lang.Double d;
    }

}
