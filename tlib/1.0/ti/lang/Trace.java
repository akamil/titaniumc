package ti.lang;

/* ti.lang.Trace provides a language-level interface to the 
   runtime's tracing & statistical collection system, 
   which may only be available on some configurations
 */
public class Trace {
  /* output an arbitrary message into the trace log, if it exists 
   */
  public static native void print(String s);

  /* fetch the mask of currently enabled trace/stat events,
     in a configuration-dependent format
   */
  public static native String getTraceMask();
  public static native String getStatsMask();

  /* set the mask for the currently enabled trace/stat events,
     in a configuration-dependent format
     Note the effective trace mask setting may be shared 
     between several Titanium threads
   */
  public static native void setTraceMask(String mask);
  public static native void setStatsMask(String mask);

  /* fetch/set whether or not local tracing is enabled */
  public static native boolean getTraceLocal();
  public static native void setTraceLocal(boolean newval);
}
