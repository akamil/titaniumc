package ti.lang;

// Complex number immutable class (rectangular)
// Kaushik Datta and Dan Bonachea {kdatta,bonachea}@cs.berkeley.edu

public immutable class Complex {
    public double re;
    public double im;

    private static final double NaN = Double.NaN;
    private static final Complex i = new Complex(0, 1);

    /* No-argument constructor returns a Complex equal to 0 */
    public inline Complex() {
	re = 0.0;
	im = 0.0;
    }

    /* Constructor */
    public inline Complex(double re, double im) {
	this.re = re;
	this.im = im;
    }

    /* returns a string in the form: a, bi, a+bi, or a-bi */
    public String toString() {
	String s = new String("");
	if ((re == 0) || (im == 0)) {
	    if (im == 0) {
		s = s + re;
	    }
	    else {
		s = s + im + "i";
	    }
	}
	else {
	    if ((im > 0) || (Double.isNaN(im))) {
		s = s + re + "+" + im + "i";
	    }
	    else {
		s = s + re + im + "i";
	    }
	}
	return s;
    }

    /* String s must be in the form: a, bi, a+bi, a-bi, or (a, b), where a and b can be parsed into doubles */
    public static Complex parseComplex(String s) throws NumberFormatException {
	String realString, imagString;
	double real, imag;

	// the extra end space is added to avoid a StringIndexOutOfBoundsException
	s = s.trim() + " ";

	// s is in the form: (a, b)
	if (s.charAt(0) == '(') {
	    int commaIndex = s.indexOf(',');
	    int rightParenIndex = s.indexOf(')');

	    // both the comma and the right paren are required
	    if ((commaIndex == -1) || (rightParenIndex == -1)) {
		throw new NumberFormatException();
	    }
	    realString = s.substring(1, commaIndex).trim();
	    imagString = s.substring(commaIndex+1, rightParenIndex).trim();
	}
	else {
	    int iIndex = s.indexOf('i');

	    // s is in the form: a
	    if (iIndex == -1) {
		realString = s;
		imagString = "0";
	    }
	    else {
		int complexPlusIndex = s.indexOf('+', 1);
		int complexMinusIndex = s.indexOf('-', 1);

		// confirm that the "i" is the last non-whitespace character
		if (iIndex != (s.length()-2)) {
		    throw new NumberFormatException();
		}
		// s is in the form: a+bi
		else if (complexPlusIndex != -1) {
		    realString = s.substring(0, complexPlusIndex);
		    imagString = s.substring(complexPlusIndex+1, iIndex).trim();
		}
		// s is in the form: a-bi
		else if (complexMinusIndex != -1) {
		    realString = s.substring(0, complexMinusIndex);
		    imagString = "-" + s.substring(complexMinusIndex+1, iIndex).trim();
		}
		// s is in the form: bi
		else {
		    realString = "0";
		    imagString = s.substring(0, iIndex);
		}
	    }
	}

	// convert from strings to doubles
	real = Double.valueOf(realString).doubleValue();
	imag = Double.valueOf(imagString).doubleValue();
	
	return new Complex(real, imag);
    }

    /* Basic Arithmetic */

    // -this
    public inline Complex op-() {
	return new Complex(-re, -im);
    }

    // this + d1
    public inline Complex op+(double d1) {
	return new Complex(re + d1, im);
    }

    // d1 + c1
    public inline Complex op+(double d1, Complex c1) {
	return new Complex(d1 + re, im);
    }

    // this + c1
    public inline Complex op+(Complex c1) {
	return new Complex(re + c1.re, im + c1.im);
    }

    // this += d1
    public inline Complex op+=(double d1) {
	return (this + d1);
    }

    // this += c1
    public inline Complex op+=(Complex c1) {
	return (this + c1);
    }

    // this - d1
    public inline Complex op-(double d1) {
	return new Complex(re - d1, im);
    }

    // d1 - c1
    public inline Complex op-(double d1, Complex c1) {
	return new Complex(d1 - re, -im);
    }

    // this - c1
    public inline Complex op-(Complex c1) {
	return new Complex(re - c1.re, im - c1.im);
    }

    // this -= d1
    public inline Complex op-=(double d1) {
	return (this - d1);
    }

    // this -= c1
    public inline Complex op-=(Complex c1) {
	return (this - c1);
    }

    // this * d1
    public inline Complex op*(double d1) {
	return new Complex(re * d1, im * d1);
    }

    // d1 * c1
    public inline Complex op*(double d1, Complex c1) {
	return new Complex(d1 * re, d1 * im);
    }

    // this * c1
    public inline Complex op*(Complex c1) {
	return new Complex(re * c1.re - im * c1.im, im * c1.re + re * c1.im);
    }

    // this *= d1
    public inline Complex op*=(double d1) {
	return (this * d1);
    }

    // this *= c1
    public inline Complex op*=(Complex c1) {
	return (this * c1);
    }

    // this / d1
    public inline Complex op/(double d1) {
	if (im != 0.0) {
	    return new Complex(re / d1, im / d1);
	}
	else return new Complex(re / d1, 0.0);
    }

    // d1 / c1
    public inline Complex op/(double d1, Complex c1) {
	if (im != 0.0) {
	    double magnitudeC1Squared = re * re + im * im;
	    return new Complex((d1 * re) / magnitudeC1Squared, -(d1 * im) / magnitudeC1Squared);
	}
	else {
	    return new Complex(d1 / re, 0.0);
	}
    }

    // this / c1
    public inline Complex op/(Complex c1) {
	if (c1.im != 0.0) {
	    double magnitudeC1Squared = c1.re * c1.re + c1.im * c1.im;
	    return new Complex((re * c1.re + im * c1.im) / magnitudeC1Squared, (im * c1.re - re * c1.im) / magnitudeC1Squared);
	}
	else {
	    return (this / c1.re);
	}
    }

    // this /= d1
    public inline Complex op/=(double d1) {
	return (this / d1);
    }

    // this /= c1
    public inline Complex op/=(Complex c1) {
	return (this / c1);
    }

    /* Equality / Inequality */

    // this == d1
    public inline boolean single op==(double single d1) {
	return (boolean single)((re == d1) && (im == 0.0));
    }

    // d1 == c1
    public inline boolean single op==(double single d1, Complex single c1) {
	return (boolean single)((re == d1) && (im == 0.0));
    }

    // this == c1
    public inline boolean single op==(Complex single c1) {
	return (boolean single)((re == c1.re) && (im == c1.im));
    }

    // this != d1
    public inline boolean single op!=(double single d1) {
	return (boolean single)((re != d1) || (im != 0.0));
    }

    // d1 != c1
    public inline boolean single op!=(double single d1, Complex single c1) {
	return (boolean single)((re != d1) || (im != 0.0));
    }

    // this != c1
    public inline boolean single op!=(Complex single c1) {
	return (boolean single)((re != c1.re) || (im != c1.im));
    }

    /* Complex Conjugate */

    // ~c1 (complex conjugate)
    public inline Complex op~() {
	return new Complex(re, -im);
    }

    // this.conj() (complex conjugate)
    public inline Complex conj() {
	return (~this);
    }

    /* Exponentiation / Logarithms */

    // this^d1 (raising to a power)
    public inline Complex op^(double d1) {
	if (this != 0.0) {
	    return (log() * d1).exp();
	}
	else {
	    if (d1 != 0.0) {
		return new Complex(0.0, 0.0);
	    }
	    else {
		return new Complex(NaN, 0.0);
	    }
	}
    }

    // d1^c1 (raising to a power)
    public inline Complex op^(double d1, Complex c1) {
	if (d1 != 0.0) {
	    return (Math.log(d1) * this).exp();
	}
	else {
	    if (this != 0.0) {
		return new Complex(0.0, 0.0);
	    }
	    else {
		return new Complex(NaN, 0.0);
	    }
	}
    }

    // this^c1 (raising to a power)
    public inline Complex op^(Complex c1) {
	if (this != 0.0) {
	    return (log() * c1).exp();
	}
	else {
	    if (c1 != 0.0) {
		return new Complex(0.0, 0.0);
	    }
	    else {
		return new Complex(NaN, 0.0);
	    }
	}
    }

    // this ^= d1
    public inline Complex op^=(double d1) {
	return (this ^ d1);
    }

    // this ^= c1
    public inline Complex op^=(Complex c1) {
	return (this ^ c1);
    }

    // e raised to "this" power
    public inline Complex exp() {
	double magnitude = Math.exp(re);
	return new Complex(magnitude * Math.cos(im), magnitude * Math.sin(im));
    }

    // log base e of "this"
    public inline Complex log() {
	return new Complex(Math.log(abs()), arg());
    }

    // square root of "this"
    public Complex sqrt() {
	if (this == 0.0) {
	    return this;
	}
	double magnitude = abs();
	double cosArg = re/magnitude;

	// uses trig half-angle formulas
	double cosHalfArgSquared = 0.5 * (1.0 + (cosArg));
	double sinHalfArgSquared = 0.5 * (1.0 - (cosArg));

	if (im >= 0.0) {
	    return new Complex(Math.sqrt(magnitude * cosHalfArgSquared), Math.sqrt(magnitude * sinHalfArgSquared));
	}
	else {
	    return new Complex(Math.sqrt(magnitude * cosHalfArgSquared), -Math.sqrt(magnitude * sinHalfArgSquared));
	}
    }

    /* Trigonometric Functions */

    // sine
    public inline Complex sin() {
	double sinRe = Math.sin(re);
	double cosRe = Math.cos(re);
	double expIm = Math.exp(im);
	double expMinusIm = 1.0/expIm;

	return new Complex(0.5 * sinRe * (expMinusIm + expIm), 0.5 * cosRe * (expIm - expMinusIm));
    }

    // cosine
    public inline Complex cos() {
	double sinRe = Math.sin(re);
	double cosRe = Math.cos(re);
	double expIm = Math.exp(im);
	double expMinusIm = 1.0/expIm;

	return new Complex(0.5 * cosRe * (expMinusIm + expIm), 0.5 * sinRe * (expMinusIm - expIm));
    }

    // tangent
    public Complex tan() {
	double sinRe = Math.sin(re);
	double cosRe = Math.cos(re);
	double expIm = Math.exp(im);
	double expMinusIm = 1.0/expIm;
	double sum = expIm + expMinusIm;
	double diff = expIm - expMinusIm;
	double x1 = sinRe * sum;
	double y1 = cosRe * diff;
	double x2 = cosRe * sum;
	double y2 = sinRe * diff;
	double magnitudeSquared = x2 * x2 + y2 * y2;

	return new Complex((x1 * x2 - y1 * y2)/magnitudeSquared, (x2 * y1 + x1 * y2)/magnitudeSquared);
    }

    // arcsine
    public inline Complex asin() {
	return (-i * (i * this + (-this * this + 1.0).sqrt()).log());
    }

    // arccosine
    public inline Complex acos() {
	return (-i * (this + i * (-this * this + 1.0).sqrt()).log());
    }

    // arctangent
    public inline Complex atan() {
	return ((0.5 * i) * ((i + this)/(i - this)).log());
    }

    // hyperbolic sine
    public inline Complex sinh() {
	double sinIm = Math.sin(im);
	double cosIm = Math.cos(im);
	double expRe = Math.exp(re);
	double expMinusRe = 1.0/expRe;

	return new Complex(0.5 * cosIm * (expRe - expMinusRe), 0.5 * sinIm * (expRe + expMinusRe));
    }

    // hyperbolic cosine
    public inline Complex cosh() {
	double sinIm = Math.sin(im);
	double cosIm = Math.cos(im);
	double expRe = Math.exp(re);
	double expMinusRe = 1.0/expRe;

	return new Complex(0.5 * cosIm * (expRe + expMinusRe), 0.5 * sinIm * (expRe - expMinusRe));
    }

    // this.tanh() (hyperbolic tangent)
    public Complex tanh() {
	double sinIm = Math.sin(im);
	double cosIm = Math.cos(im);
	double expRe = Math.exp(re);
	double expMinusRe = 1.0/expRe;
	double sum = expRe + expMinusRe;
	double diff = expRe - expMinusRe;
	double x1 = cosIm * diff;
	double y1 = sinIm * sum;
	double x2 = cosIm * sum;
	double y2 = sinIm * diff;
	double magnitudeSquared = x2 * x2 + y2 * y2;

	return new Complex((x1 * x2 + y1 * y2)/magnitudeSquared, (x2 * y1 - x1 * y2)/magnitudeSquared);
    }

    // this.asinh() (hyperbolic arcsine)
    public inline Complex asinh() {
	return (this + ((this * this) + 1.0).sqrt()).log();
    }

    // this.acosh() (hyperbolic arccosine)
    public inline Complex acosh() {
	return (this + (((this + 1.0).sqrt()) * ((this - 1.0).sqrt()))).log();
    }

    // this.atanh() (hyperbolic arctangent)
    public inline Complex atanh() {
	return ((this + 1.0)/(-this + 1.0)).log() / 2.0;
    }

    /* Polar Conversion Methods */

    // this.abs() (returns the absolute value, also called modulus)
    public inline double abs() {
	return Math.sqrt(absSquare());
    }

    // this.absSquare() (returns abs()^2- used to compare magnitudes without Math.sqrt())
    public inline double absSquare() {
	return (re * re + im * im);
    }

    // this.arg() (returns the argument, also called phase angle, in radians between -pi and pi)
    public inline double arg() {
	return Math.atan2(im, re);
    }
}
