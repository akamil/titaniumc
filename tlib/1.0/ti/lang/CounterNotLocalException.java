package ti.lang;

public class CounterNotLocalException extends PAPIException{

    public CounterNotLocalException(){
	super("PAPI counter is not local to this processor");
    }
}
