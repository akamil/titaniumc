package ti.lang;

public class TooManyCounterException extends PAPIException{

    public TooManyCounterException(){
	super("There are too many counters in use");
    }
}
