package ti.lang;


/**
 * Provides direct access to machine tick counter.  A TickCounter
 * instance is a self-contained stopwatch: it may be started and
 * stopped, and measures the number of ticks that elapse while
 * running.  Additionally, the instantaneous raw machine tick count
 * may be read directly, for programmers who wish to do their own
 * interval management.
 *
 * <p>Very little sanity checking is performed, in order to avoid the
 * distortion caused by running extra code.  The caller is responsible
 * for ensuring that each {@link #start} call is matched up with
 * exactly one {@link #stop} call.  As long as the calls are matched,
 * one may start and stop a TickCounter several times; the {@link
 * #elapsed} field accumulates elapsed ticks between successive starts
 * and stops.
 *
 * <p>Ticks have <em>no</em> universal, portable interpretation.  The
 * length of time consumed by a tick may vary from architecture to
 * architecture, or from chip to chip within a single architecture.
 * Ticks may wrap around at different maximal values.  On some
 * platforms, the tick count may not be accessible at all, in which
 * case {@link #current} will always return -1.
 * 
 * @see Timer
 */

public class TickCounter {

    /**
     * Net elapsed ticks measured by this stopwatch.  Ticks accumulate
     * between matched calls to {@link #start} and {@link #stop}.
     */
    public long elapsed = 0;
    
    /**
     * Raw machine tick count at the moment this stopwatch was started.
     */
    private long started = 0;

    /**
     * Creates a new tick-counting stopwatch.  The stopwatch is
     * initially stopped; it may be started by calling {@link #start}.
     */
    public TickCounter()
    {
	// This could just as well have been a default constructor,
	// but then we would not have any place to hang the Javadoc
	// documentation.
    }

    /**
     * Reads the current raw machine tick count.
     * 
     * @return the current raw machine tick count, or -1 if the count
     *         is not available on this platform
     */
    public static native long current();

    /** 
     * Converts a tick count into microseconds, nanoseconds
     */
    public static native double ticksToNanos(long ticks);
    public static inline double ticksToMicros(long ticks) { 
      return ticksToNanos(ticks)/1000.0;
    }
 
    /** 
     * Returns the approximate overhead, in microseconds,
     * for each current() call, and the observable
     * granularity - the shortest non-zero difference 
     * between two current() calls, in microseconds
     */ 
    public static native double granularity();
    public static native double overhead();

    /**
     * Starts counting ticks.
     * 
     * @see #stop
     */
    public inline void start() {
	started = current();
    }

    /**
     * Stops counting ticks.
     * 
     * @see #start
     * @see #elapsed
     */
    public inline void stop() {
	elapsed += current() - started;
    }

    /**
     * Resets a tick counter stopwatch, as though it had just been
     * created.  The stopwatch is stopped, and its {@link #elapsed}
     * tick accumulator is reset to zero.
     */
    public inline void reset()
    {
	elapsed = 0;
	started = 0;
    }
}
