package ti.internal;
final public immutable class tiPoint {
  native public static tiPoint single all(int single x);
  native public static tiPoint single direction(int single k, int single x);
  native public static tiPoint single direction(int single k);

  native public tiPoint single op -();
  native public tiPoint single op +(tiPoint single p);
  native public tiPoint single op +=(tiPoint single p);
  native public tiPoint single op -(tiPoint single p);
  native public tiPoint single op -=(tiPoint single p);
  native public tiPoint single op *(tiPoint single p);
  native public tiPoint single op *=(tiPoint single p);
  native public tiPoint single op /(tiPoint single p);
  native public tiPoint single op /=(tiPoint single p);
  native public tiPoint single op +(int single n);
  native public tiPoint single op +(int single n, tiPoint single p);
  native public tiPoint single op +=(int single n);
  native public tiPoint single op -(int single n);
  native public tiPoint single op -(int single n, tiPoint single p);
  native public tiPoint single op -=(int single n);
  native public tiPoint single op *(int single n);
  native public tiPoint single op *(int single n, tiPoint single p);
  native public tiPoint single op *=(int single n);
  native public tiPoint single op /(int single n);
  native public tiPoint single op /(int single n, tiPoint single p);
  native public tiPoint single op /=(int single n);
  native public boolean single equals(tiPoint single p);
  native public boolean single op ==(tiPoint single p);
  native public boolean single op !=(tiPoint single p);
  native public boolean single op <(tiPoint single p);
  native public boolean single op <=(tiPoint single p);
  native public boolean single op >(tiPoint single p);
  native public boolean single op >=(tiPoint single p);

  native public static int single arity();
  public final static int single arity = 0; // replaced with correct value during instantiation

  native public tiPoint single permute(tiPoint single p);
  native public String single local toString();

  native public int single op[](int single index);

  native public tiPoint single lowerBound(tiPoint single p);
  native public tiPoint single upperBound(tiPoint single p);
  native public tiPoint single replace(int single index, int single value); 

  // non-public, undocumented methods
  native public int single get(int single index); // 0-based indexing!!!
  native public tiPoint single set(int single index, int single value); // 0-based indexing!!!
   // P.set(i,v) returns new Point NP with NP[i+1] == v, 
   // and other components the same as P
  native public tiPoint single getLcm(tiPoint single p);
   // P.getLcm(P2) returns new Point NP with NP[i] == leastCommonMultiple(P[i], P2[i])
}
