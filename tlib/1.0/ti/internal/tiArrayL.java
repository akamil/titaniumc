// this file describes a titanium array whose elements local references,
// or some type with embedded locals - we need to restrict operations that
// would move these elements across processors to prevent unsoundness
// (e.g. we prohibit exchange(), bulk I/O, and some kinds of copies)

package ti.internal;

final public immutable class tiArrayL {
  // view queries

  public final static int single arity = 0; // replaced with correct value during instantiation
  native public static int single arity();

  native public tiRectDomain single domain();
  native public local tiRectDomain single domain();
  native public local nonshared tiRectDomain single domain();
  native public local polyshared tiRectDomain single domain();

  // equiv to: this.domain().size()
  native public int single size();
  native public local int single size();
  native public local nonshared int single size();
  native public local polyshared int single size();
        
  // equiv to: this.domain().isEmpty()
  native public boolean single isEmpty();
  native public local boolean single isEmpty();
  native public local nonshared boolean single isEmpty();
  native public local polyshared boolean single isEmpty();

  // array view operators

  native public tiArrayL single translate(tiPoint single p);
  native public local tiArrayL local single translate(tiPoint single p);
  native public local nonshared tiArrayL local nonshared single translate(tiPoint single p);
  native public local polyshared tiArrayL local polyshared single translate(tiPoint single p);

  native public tiArrayL single restrict(tiRectDomain single d);
  native public local tiArrayL local single restrict(tiRectDomain single d);
  native public local nonshared tiArrayL nonshared local single restrict(tiRectDomain single d);
  native public local polyshared tiArrayL polyshared local single restrict(tiRectDomain single d);

  native public tiArrayL single inject(tiPoint single p);
  native public local tiArrayL local single inject(tiPoint single p);
  native public local nonshared tiArrayL local nonshared single inject(tiPoint single p);
  native public local polyshared tiArrayL local polyshared single inject(tiPoint single p);

  native public tiArrayL single project(tiPoint single p);
  native public local tiArrayL local single project(tiPoint single p);
  native public local nonshared tiArrayL local nonshared single project(tiPoint single p);
  native public local polyshared tiArrayL local polyshared single project(tiPoint single p);

  native public tiArrayL single permute(tiPoint single p);
  native public local tiArrayL local single permute(tiPoint single p);
  native public local nonshared tiArrayL local nonshared single permute(tiPoint single p);
  native public local polyshared tiArrayL local polyshared single permute(tiPoint single p);

  native public tiArrayM1 single slice(int single k, int single j);
  native public local tiArrayM1 local single slice(int single k, int single j);
  native public local nonshared tiArrayM1 local nonshared single slice(int single k, int single j);
  native public local polyshared tiArrayM1 local polyshared single slice(int single k, int single j);

  // equiv to: this.restrict(this.domain().shrink(k,dir))
  native public tiArrayL single shrink(int single k, int single dir);
  native public local tiArrayL local single shrink(int single k, int single dir);
  native public local nonshared tiArrayL local nonshared single shrink(int single k, int single dir);
  native public local polyshared tiArrayL local polyshared single shrink(int single k, int single dir);

  // equiv to: this.restrict(this.domain().shrink(k))
  native public tiArrayL single shrink(int single k);
  native public local tiArrayL local single shrink(int single k);
  native public local nonshared tiArrayL local nonshared single shrink(int single k);
  native public local polyshared tiArrayL local polyshared single shrink(int single k);

  // equiv to: this.restrict(this.domain().border(k,dir,shift))
  native public tiArrayL single border(int single k, int single dir, int single shift);
  native public local tiArrayL local single border(int single k, int single dir, int single shift);
  native public local nonshared tiArrayL local nonshared single border(int single k, int single dir, int single shift);
  native public local polyshared tiArrayL local polyshared single border(int single k, int single dir, int single shift);

  // equiv to: this.restrict(this.domain().border(k,dir))
  native public tiArrayL single border(int single k, int single dir);
  native public local tiArrayL local single border(int single k, int single dir);
  native public local nonshared tiArrayL local nonshared single border(int single k, int single dir);
  native public local polyshared tiArrayL local polyshared single border(int single k, int single dir);

  // equiv to: this.restrict(this.domain().border(dir))
  native public tiArrayL single border(int single dir);
  native public local tiArrayL local single border(int single dir);
  native public local nonshared tiArrayL local nonshared single border(int single dir);
  native public local polyshared tiArrayL local polyshared single border(int single dir);

  // element initialization

  // native public void set(tiTemplateArgument value); // PR856 - this combination is unsound
  native public local void set(tiTemplateArgument value);
  native public local nonshared void set(tiTemplateArgument value);
  native public local polyshared void set(tiTemplateArgument value);

  // Object-like queries

  native public boolean isLocal();
  native public local boolean isLocal();
  native public local nonshared boolean isLocal();
  native public local polyshared boolean isLocal();

  native public int creator();
  native public local int creator();
  native public local nonshared int creator();
  native public local polyshared int creator();

  native public Region local regionOf();
  native public local Region local regionOf();
  native public local nonshared Region local regionOf();
  native public local polyshared Region local regionOf();

  //------------------------------------------------------------------------------------
  // methods involving more than one array - the qualifiers grow our declarations combinatorially 
  // boy this is yucky... I hope Titanium never grows any more array qualifiers...

  // Dense (rectangular) array copy
  // note - we never copy these arrays over a global reference
  native public local void copy(tiArrayL local x);
  native public local void copy(tiArrayL local nonshared x);
  native public local void copy(tiArrayL local polyshared x);
  native public local nonshared void copy(tiArrayL local x);
  native public local nonshared void copy(tiArrayL local nonshared x);
  native public local nonshared void copy(tiArrayL local polyshared x);
  native public local polyshared void copy(tiArrayL local x);
  native public local polyshared void copy(tiArrayL local nonshared x);
  native public local polyshared void copy(tiArrayL local polyshared x);  

  native public local void copyNBI(tiArrayL local x);
  native public local void copyNBI(tiArrayL local nonshared x);
  native public local void copyNBI(tiArrayL local polyshared x);
  native public local nonshared void copyNBI(tiArrayL local x);
  native public local nonshared void copyNBI(tiArrayL local nonshared x);
  native public local nonshared void copyNBI(tiArrayL local polyshared x);
  native public local polyshared void copyNBI(tiArrayL local x);
  native public local polyshared void copyNBI(tiArrayL local nonshared x);
  native public local polyshared void copyNBI(tiArrayL local polyshared x);  

  native public local Handle copyNB(tiArrayL local x);
  native public local Handle copyNB(tiArrayL local nonshared x);
  native public local Handle copyNB(tiArrayL local polyshared x);
  native public local nonshared Handle copyNB(tiArrayL local x);
  native public local nonshared Handle copyNB(tiArrayL local nonshared x);
  native public local nonshared Handle copyNB(tiArrayL local polyshared x);
  native public local polyshared Handle copyNB(tiArrayL local x);
  native public local polyshared Handle copyNB(tiArrayL local nonshared x);
  native public local polyshared Handle copyNB(tiArrayL local polyshared x);    

  // junk "pseudo-method"
  native public void junk();
  native public local void junk();
  native public local nonshared void junk();
  native public local polyshared void junk();

  //------------------------------------------------------------------------------------
  // Sparse array copy (over general domain)
  // note - we never copy these arrays over a global reference
  native public local void copy(tiArrayL local x, tiDomain d);
  native public local void copy(tiArrayL local nonshared x, tiDomain d);
  native public local void copy(tiArrayL local polyshared x, tiDomain d);
  native public local nonshared void copy(tiArrayL local x, tiDomain d);
  native public local nonshared void copy(tiArrayL local nonshared x, tiDomain d);
  native public local nonshared void copy(tiArrayL local polyshared x, tiDomain d);
  native public local polyshared void copy(tiArrayL local x, tiDomain d);
  native public local polyshared void copy(tiArrayL local nonshared x, tiDomain d);
  native public local polyshared void copy(tiArrayL local polyshared x, tiDomain d);

  //------------------------------------------------------------------------------------
  // Sparse array copy (over rect domain)
  // note - we never copy these arrays over a global reference
  native public local void copy(tiArrayL local x, tiRectDomain d);
  native public local void copy(tiArrayL local nonshared x, tiRectDomain d);
  native public local void copy(tiArrayL local polyshared x, tiRectDomain d);
  native public local nonshared void copy(tiArrayL local x, tiRectDomain d);
  native public local nonshared void copy(tiArrayL local nonshared x, tiRectDomain d);
  native public local nonshared void copy(tiArrayL local polyshared x, tiRectDomain d);
  native public local polyshared void copy(tiArrayL local x, tiRectDomain d);
  native public local polyshared void copy(tiArrayL local nonshared x, tiRectDomain d);
  native public local polyshared void copy(tiArrayL local polyshared x, tiRectDomain d);

  //------------------------------------------------------------------------------------
  // Sparse array copy (over point list)
  // note - we never copy these arrays over a global reference
  native public local      /*shared*/ void copy(tiArrayL local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void copy(tiArrayL local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void copy(tiArrayL local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void copy(tiArrayL local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void copy(tiArrayL local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void copy(tiArrayL local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void copy(tiArrayL local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void copy(tiArrayL local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void copy(tiArrayL local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void copy(tiArrayL local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void copy(tiArrayL local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void copy(tiArrayL local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void copy(tiArrayL local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void copy(tiArrayL local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void copy(tiArrayL local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void copy(tiArrayL local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void copy(tiArrayL local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void copy(tiArrayL local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void copy(tiArrayL local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void copy(tiArrayL local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void copy(tiArrayL local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void copy(tiArrayL local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void copy(tiArrayL local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void copy(tiArrayL local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void copy(tiArrayL local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void copy(tiArrayL local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void copy(tiArrayL local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void copy(tiArrayL local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void copy(tiArrayL local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void copy(tiArrayL local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void copy(tiArrayL local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void copy(tiArrayL local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void copy(tiArrayL local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void copy(tiArrayL local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void copy(tiArrayL local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void copy(tiArrayL local      polyshared x, tiPoint[1d] local      polyshared ptArray);

  //------------------------------------------------------------------------------------
  // Gather
  // note - we never gather to/from these arrays over a global reference, 
  // because both this and x have element type with embedded locals
  // ptArray is permitted to be global because the elements have no embedded locals
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      polyshared ptArray);

  //------------------------------------------------------------------------------------
  // Scatter
  // note - we never scatter to/from these arrays over a global reference,
  // because both this and x have element type with embedded locals
  // ptArray is permitted to be global because the elements have no embedded locals
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      polyshared ptArray);

  private polyshared tiArrayL() { }
}

