package ti.internal;
public final class tiDomain {

  // Domain set relationships
  native public tiDomain single op +(tiDomain single d);
  native public tiDomain single op +=(tiDomain single d);
  native public tiDomain single op -(tiDomain single d);
  native public tiDomain single op -=(tiDomain single d);
  native public tiDomain single op *(tiDomain single d);
  native public tiDomain single op *=(tiDomain single d);

  // Domain boolean relationships
  native public boolean single op <(tiDomain single d);
  native public boolean single op <=(tiDomain single d);
  native public boolean single op >(tiDomain single d);
  native public boolean single op >=(tiDomain single d);
  native public boolean single equals(tiDomain single d);

  // RectDomain set relationships
  native public tiDomain single op +(tiRectDomain single d);
  native public tiDomain single op +=(tiRectDomain single d);
  native public tiDomain single op -(tiRectDomain single d);
  native public tiDomain single op -=(tiRectDomain single d);
  native public tiDomain single op *(tiRectDomain single d);
  native public tiDomain single op *=(tiRectDomain single d);

  // RectDomain boolean relationships
  native public boolean single op <(tiRectDomain single d);
  native public boolean single op <=(tiRectDomain single d);
  native public boolean single op >(tiRectDomain single d);
  native public boolean single op >=(tiRectDomain single d);
  native public boolean single equals(tiRectDomain single d);

  // Point set relationships
  native public tiDomain single op +(tiPoint single p);
  native public tiDomain single op +=(tiPoint single p);
  native public tiDomain single op -(tiPoint single p);
  native public tiDomain single op -=(tiPoint single p);
  native public tiDomain single op *(tiPoint single p);
  native public tiDomain single op *=(tiPoint single p);
  native public tiDomain single op /(tiPoint single p);
  native public tiDomain single op /=(tiPoint single p);

  // Shape
  native public tiDomain single permute(tiPoint single p);

  // Shape information
  public final static int single arity = 0; // replaced with correct value during instantiation
  native public static int single arity();
  native public tiPoint single lwb();
  native public tiPoint single upb();
  native public tiPoint single min();
  native public tiPoint single max();
  native public int single size();
  native public long single size64();
  native public boolean single contains(tiPoint single p);
  native public tiRectDomain single boundingBox();
  native public boolean single isNull();
  native public boolean single isNotNull();
  native public boolean single isEmpty();
  native public boolean single isNotEmpty();
  native public boolean single isADR();
  native public boolean single isRectangular();
  native public tiRectDomain [1d] single RectDomainList();
  native public tiPoint [1d] single PointList();

  native public static tiDomain local single toDomain(tiRectDomain single [1d] rd);
  native public static tiDomain local toDomain(tiRectDomain [1d] rd);
  native public static tiDomain local single toDomain(tiPoint single [1d] p);
  native public static tiDomain local toDomain(tiPoint [1d] p);

  // Object overrides
  native public String toString();
  native public int hashCode();
  native public boolean equals(Object obj);

  // dynamic optimization (undocumented)
  native public void optimize();

  public tiDomain(tiDomain copy) {}
  // private methods
  native public static tiDomain local single copy_constructor(Region r, tiDomain single d);
  native        tiRectDomain[] isRectangularEx();
  // Hack for memory allocation - sets region used for all domain allocations and returns
  // old one. Use null for GCed allocation.
  native public static Region setRegion(Region r);
}
