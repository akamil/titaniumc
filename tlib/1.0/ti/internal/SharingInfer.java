package ti.internal;


class SharingInfer
{
    // Native code may access certain fields and methods in ways that
    // are invisible to sharing qualification inference analysis.  The
    // code in this class will cause additional constraints to be
    // applied during sharing qualification inference.  These extra
    // constraints prevent sharing qualfications from being added in
    // places that would break native code.

    static private native char[] local chars();
    static private native byte[] local bytes();

    static {
	NativeUtils.buildString( chars() );
	NativeUtils.buildString( bytes() );
    }
}
