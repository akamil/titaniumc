package ti.internal;

final public class tiJArray implements Cloneable {
  private native tiTemplateArgument get(int location);  
  private native void set(int location, tiTemplateArgument value);
  private native static tiJArray allocate(int length);
  private tiJArray() { }
  
  public final int single length;

  public native Object clone(); 
  public native local Object local localClone(); 
   // DOB: note array cloning is handled using a special-case dispatch 
   // to the analogous methods in java.lang.Object
}
