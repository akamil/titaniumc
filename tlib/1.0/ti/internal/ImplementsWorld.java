package ti.internal;

import java.io.*;
import java.lang.*;
import java.util.*;

class ImplementsWorld implements DataInput,
				 DataOutput,
				 FilenameFilter,
				 Cloneable,
				 Runnable,
				 Enumeration,
				 Observer,
				 ObjectOp,
				 IntOp,
				 LongOp,
				 DoubleOp
{
    public void readFully(byte b[]) throws IOException { }
    public void readFully(byte b[], int off, int len) throws IOException { }
    public int skipBytes(int n) throws IOException { return 0; }
    public boolean readBoolean() throws IOException { return true; }
    public byte readByte() throws IOException { return 0; }
    public int readUnsignedByte() throws IOException { return 0; }
    public short readShort() throws IOException { return 0; }
    public int readUnsignedShort() throws IOException { return 0; }
    public char readChar() throws IOException { return 'a'; }
    public int readInt() throws IOException { return 0; }
    public long readLong() throws IOException { return 0; }
    public float readFloat() throws IOException { return (float)0.0; }
    public double readDouble() throws IOException { return 0.0; }
    public String readLine() throws IOException { return "bad"; }
    public String readUTF() throws IOException { return "bad"; }

    public void write(int b) throws IOException { }
    public void write(byte b[]) throws IOException { }
    public void write(byte b[], int off, int len) throws IOException { }
    public void writeBoolean(boolean v) throws IOException { }
    public void writeByte(int v) throws IOException { }
    public void writeShort(int v) throws IOException { }
    public void writeChar(int v) throws IOException { }
    public void writeInt(int v) throws IOException { }
    public void writeLong(long v) throws IOException { }
    public void writeFloat(float v) throws IOException { }
    public void writeDouble(double v) throws IOException { }
    public void writeBytes(String s) throws IOException { }
    public void writeChars(String s) throws IOException { }
    public void writeUTF(String str) throws IOException { }

    public boolean accept(File dir, String name) { return true; }

    public void run() { }

    public boolean hasMoreElements() { return true; }
    public Object nextElement() { return null; }

    public void update(Observable o, Object arg) { }

    public Object eval(Object x, Object y) { return x; }
    public int eval(int x, int y) { return x; }
    public long eval(long x, long y) { return x; }
    public double eval(double x, double y) { return x; }
}
