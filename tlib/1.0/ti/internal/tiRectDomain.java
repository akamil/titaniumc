package ti.internal;
public immutable class tiRectDomain {
  native public boolean isRectangular();

  native public tiDomain single op +(tiRectDomain single d);
  native public tiDomain single op -(tiRectDomain single d);
  native public tiRectDomain single op *(tiRectDomain single d);
  native public tiRectDomain single op *=(tiRectDomain single d);

  native public tiRectDomain single op +(tiPoint single p);
  native public tiRectDomain single op +=(tiPoint single p);
  native public tiRectDomain single op -(tiPoint single p);
  native public tiRectDomain single op -=(tiPoint single p);
  native public tiRectDomain single op *(tiPoint single p);
  native public tiRectDomain single op *=(tiPoint single p);
  native public tiRectDomain single op /(tiPoint single p);
  native public tiRectDomain single op /=(tiPoint single p);

  native public boolean single equals(tiRectDomain single d);
  native public boolean single op ==(tiRectDomain single d);
  native public boolean single op !=(tiRectDomain single d);
  native public boolean single op <(tiRectDomain single d);
  native public boolean single op <=(tiRectDomain single d);
  native public boolean single op >(tiRectDomain single d);
  native public boolean single op >=(tiRectDomain single d);

  native public tiDomain single op +(tiDomain single d);
  native public tiDomain single op -(tiDomain single d);
  native public tiDomain single op *(tiDomain single d);

  native public boolean single equals(tiDomain single d);
  native public boolean single op <(tiDomain single d);
  native public boolean single op <=(tiDomain single d);
  native public boolean single op >(tiDomain single d);
  native public boolean single op >=(tiDomain single d);

  native public boolean equals(Object d);

  // Shape
  native public tiRectDomain single permute(tiPoint single p);

  // Shape information
  native public static int single arity();
  public final static int single arity = 0; // replaced with correct value during instantiation
  native public tiPoint single lwb();
  native public tiPoint single upb();
  native public tiPoint single min();
  native public tiPoint single max();
  native public tiPoint single stride();
  native public int single size();
  native public long single size64();
  native public boolean single isNull();
  native public boolean single isNotNull();
  native public boolean single isEmpty();
  native public boolean single isNotEmpty();
  native public boolean single isADR();
  native public tiRectDomain single accrete(int single k, int single dir, int single s);
  native public tiRectDomain single accrete(int single k, int single dir); // s = 1
  native public tiRectDomain single accrete(int single k, tiPoint single S);
  native public tiRectDomain single accrete(int single k); // S = all(1)
  native public tiRectDomain single shrink(int single k, int single dir);
  native public tiRectDomain single shrink(int single k);
  native public tiRectDomain single border(int single k, int single dir, int single shift);
  native public tiRectDomain single border(int single k, int single dir);
  native public tiRectDomain single border(int single dir);
  native public boolean single contains(tiPoint single p);
  native public tiRectDomain single boundingBox();

  native public tiRectDomainM1 single slice(int single k);

  native public String single local toString();

  private tiRectDomain() { }
}
