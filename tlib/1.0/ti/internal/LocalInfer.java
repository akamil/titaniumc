package ti.internal;


class LocalInfer
{
    // Native code may access certain fields and methods in ways that
    // are invisible to local qualification inference analysis.  The
    // code in this class will cause additional constraints to be
    // applied during local qualification inference.  These extra
    // constraints prevent local qualfications from being added in
    // places that would break native code.

    static private native String string();
    static private native java.util.Properties properties();
    static private native java.io.RandomAccessFile file();
    static private native java.io.DataInputStream stream();
    static private native java.io.PrintStream pstream();
    static private native Throwable throwable();
    static private native byte[] bytes();
    static private native LocalInfer localinfer();

    static {
	try {
	    NativeUtils.getBytes( string() );
	    NativeUtils.addProperty( properties(), null, null );
	    NativeUtils.throwNumberFormatException( string() );
	    NativeUtils.dumpException( throwable() );
            throwable().fillInStackTrace();
	    file().getFD();
	    file().readFully( bytes(), 0, 0 );
	    stream().readFully( bytes(), 0, 0 );
            pstream().println( string() );
            ti.util.Demangle.demangle(string());
	} catch ( java.io.IOException exception ) { }
    }
    private void foo() {
      try {
        localinfer().finalize();
      } catch (Throwable exn) {}
    }
}
