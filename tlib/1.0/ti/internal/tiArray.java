// this file describes a general titanium array 
// whose elements don't contain any local references (also see tiArrayL.java)


// why we have 4 versions of each method with different qualifiers:
// global arrays can only be accessed if they have a "shared" qualifier
// local arrays can be used regardless of sharing qualifier, but we need to 
//  list all 3 varieties in order for overloading resolution to find an 
//  unambiguous match (not good enough to just list a polyshared version)

package ti.internal;

final public immutable class tiArray {
  // view queries

  public final static int single arity = 0; // replaced with correct value during instantiation
  native public static int single arity();

  native public tiRectDomain single domain();
  native public local tiRectDomain single domain();
  native public local nonshared tiRectDomain single domain();
  native public local polyshared tiRectDomain single domain();

  // equiv to: this.domain().size()
  native public int single size();
  native public local int single size();
  native public local nonshared int single size();
  native public local polyshared int single size();
        
  // equiv to: this.domain().isEmpty()
  native public boolean single isEmpty();
  native public local boolean single isEmpty();
  native public local nonshared boolean single isEmpty();
  native public local polyshared boolean single isEmpty();

  // array view operators

  native public tiArray single translate(tiPoint single p);
  native public local tiArray local single translate(tiPoint single p);
  native public local nonshared tiArray local nonshared single translate(tiPoint single p);
  native public local polyshared tiArray local polyshared single translate(tiPoint single p);

  native public tiArray single restrict(tiRectDomain single d);
  native public local tiArray local single restrict(tiRectDomain single d);
  native public local nonshared tiArray nonshared local single restrict(tiRectDomain single d);
  native public local polyshared tiArray polyshared local single restrict(tiRectDomain single d);

  native public tiArray single inject(tiPoint single p);
  native public local tiArray local single inject(tiPoint single p);
  native public local nonshared tiArray local nonshared single inject(tiPoint single p);
  native public local polyshared tiArray local polyshared single inject(tiPoint single p);

  native public tiArray single project(tiPoint single p);
  native public local tiArray local single project(tiPoint single p);
  native public local nonshared tiArray local nonshared single project(tiPoint single p);
  native public local polyshared tiArray local polyshared single project(tiPoint single p);

  native public tiArray single permute(tiPoint single p);
  native public local tiArray local single permute(tiPoint single p);
  native public local nonshared tiArray local nonshared single permute(tiPoint single p);
  native public local polyshared tiArray local polyshared single permute(tiPoint single p);

  native public tiArrayM1 single slice(int single k, int single j);
  native public local tiArrayM1 local single slice(int single k, int single j);
  native public local nonshared tiArrayM1 local nonshared single slice(int single k, int single j);
  native public local polyshared tiArrayM1 local polyshared single slice(int single k, int single j);

  // equiv to: this.restrict(this.domain().shrink(k,dir))
  native public tiArray single shrink(int single k, int single dir);
  native public local tiArray local single shrink(int single k, int single dir);
  native public local nonshared tiArray local nonshared single shrink(int single k, int single dir);
  native public local polyshared tiArray local polyshared single shrink(int single k, int single dir);

  // equiv to: this.restrict(this.domain().shrink(k))
  native public tiArray single shrink(int single k);
  native public local tiArray local single shrink(int single k);
  native public local nonshared tiArray local nonshared single shrink(int single k);
  native public local polyshared tiArray local polyshared single shrink(int single k);

  // equiv to: this.restrict(this.domain().border(k,dir,shift))
  native public tiArray single border(int single k, int single dir, int single shift);
  native public local tiArray local single border(int single k, int single dir, int single shift);
  native public local nonshared tiArray local nonshared single border(int single k, int single dir, int single shift);
  native public local polyshared tiArray local polyshared single border(int single k, int single dir, int single shift);

  // equiv to: this.restrict(this.domain().border(k,dir))
  native public tiArray single border(int single k, int single dir);
  native public local tiArray local single border(int single k, int single dir);
  native public local nonshared tiArray local nonshared single border(int single k, int single dir);
  native public local polyshared tiArray local polyshared single border(int single k, int single dir);

  // equiv to: this.restrict(this.domain().border(dir))
  native public tiArray single border(int single dir);
  native public local tiArray local single border(int single dir);
  native public local nonshared tiArray local nonshared single border(int single dir);
  native public local polyshared tiArray local polyshared single border(int single dir);

  // element initialization

  native public void set(tiTemplateArgument value);
  native public local void set(tiTemplateArgument value);
  native public local nonshared void set(tiTemplateArgument value);
  native public local polyshared void set(tiTemplateArgument value);

  // Object-like queries 

  native public boolean isLocal();
  native public local boolean isLocal();
  native public local nonshared boolean isLocal();
  native public local polyshared boolean isLocal();

  native public boolean isContiguous();
  native public local boolean isContiguous();
  native public local nonshared boolean isContiguous();
  native public local polyshared boolean isContiguous();

  native public int creator();
  native public local int creator();
  native public local nonshared int creator();
  native public local polyshared int creator();

  native public Region local regionOf();
  native public local Region local regionOf();
  native public local nonshared Region local regionOf();
  native public local polyshared Region local regionOf();

  // collective communication
  native public single void exchange(tiTemplateArgument myValue);
  native public single local void exchange(tiTemplateArgument myValue);
  native public single local nonshared void exchange(tiTemplateArgument myValue);
  native public single local polyshared void exchange(tiTemplateArgument myValue);

  // junk "pseudo-method"
  native public void junk();
  native public local void junk();
  native public local nonshared void junk();
  native public local polyshared void junk();

  // bulk I/O methods 
  native public void readFrom(java.io.RandomAccessFile RAF) throws java.io.IOException;
  native public void readFrom(java.io.DataInputStream DIS) throws java.io.IOException;
  native public void writeTo(java.io.RandomAccessFile RAF) throws java.io.IOException;
  native public void writeTo(java.io.DataOutputStream DOS) throws java.io.IOException;

  //------------------------------------------------------------------------------------
  // methods involving more than one array - the qualifiers grow our declarations combinatorially 
  // boy this is yucky... I hope Titanium never grows any more array qualifiers...

  // Dense (rectangular) array copy
  native public void copy(tiArray x);
  native public void copy(tiArray local x);
  native public void copy(tiArray local nonshared x);
  native public void copy(tiArray local polyshared x);
  native public local void copy(tiArray x);
  native public local void copy(tiArray local x);
  native public local void copy(tiArray local nonshared x);
  native public local void copy(tiArray local polyshared x);
  native public local nonshared void copy(tiArray x);
  native public local nonshared void copy(tiArray local x);
  native public local nonshared void copy(tiArray local nonshared x);
  native public local nonshared void copy(tiArray local polyshared x);
  native public local polyshared void copy(tiArray x);
  native public local polyshared void copy(tiArray local x);
  native public local polyshared void copy(tiArray local nonshared x);
  native public local polyshared void copy(tiArray local polyshared x);

  native public void copyNBI(tiArray x);
  native public void copyNBI(tiArray local x);
  native public void copyNBI(tiArray local nonshared x);
  native public void copyNBI(tiArray local polyshared x);
  native public local void copyNBI(tiArray x);
  native public local void copyNBI(tiArray local x);
  native public local void copyNBI(tiArray local nonshared x);
  native public local void copyNBI(tiArray local polyshared x);
  native public local nonshared void copyNBI(tiArray x);
  native public local nonshared void copyNBI(tiArray local x);
  native public local nonshared void copyNBI(tiArray local nonshared x);
  native public local nonshared void copyNBI(tiArray local polyshared x);
  native public local polyshared void copyNBI(tiArray x);
  native public local polyshared void copyNBI(tiArray local x);
  native public local polyshared void copyNBI(tiArray local nonshared x);
  native public local polyshared void copyNBI(tiArray local polyshared x);

  native public Handle copyNB(tiArray x);
  native public Handle copyNB(tiArray local x);
  native public Handle copyNB(tiArray local nonshared x);
  native public Handle copyNB(tiArray local polyshared x);
  native public local Handle copyNB(tiArray x);
  native public local Handle copyNB(tiArray local x);
  native public local Handle copyNB(tiArray local nonshared x);
  native public local Handle copyNB(tiArray local polyshared x);
  native public local nonshared Handle copyNB(tiArray x);
  native public local nonshared Handle copyNB(tiArray local x);
  native public local nonshared Handle copyNB(tiArray local nonshared x);
  native public local nonshared Handle copyNB(tiArray local polyshared x);
  native public local polyshared Handle copyNB(tiArray x);
  native public local polyshared Handle copyNB(tiArray local x);
  native public local polyshared Handle copyNB(tiArray local nonshared x);
  native public local polyshared Handle copyNB(tiArray local polyshared x);

  //------------------------------------------------------------------------------------
  // Sparse array copy (over general domain)
  native public void copy(tiArray x, tiDomain d);
  native public void copy(tiArray local x, tiDomain d);
  native public void copy(tiArray local nonshared x, tiDomain d);
  native public void copy(tiArray local polyshared x, tiDomain d);
  native public local void copy(tiArray x, tiDomain d);
  native public local void copy(tiArray local x, tiDomain d);
  native public local void copy(tiArray local nonshared x, tiDomain d);
  native public local void copy(tiArray local polyshared x, tiDomain d);
  native public local nonshared void copy(tiArray x, tiDomain d);
  native public local nonshared void copy(tiArray local x, tiDomain d);
  native public local nonshared void copy(tiArray local nonshared x, tiDomain d);
  native public local nonshared void copy(tiArray local polyshared x, tiDomain d);
  native public local polyshared void copy(tiArray x, tiDomain d);
  native public local polyshared void copy(tiArray local x, tiDomain d);
  native public local polyshared void copy(tiArray local nonshared x, tiDomain d);
  native public local polyshared void copy(tiArray local polyshared x, tiDomain d);

  // Sparse array copy (over rect domain)
  native public void copy(tiArray x, tiRectDomain d);
  native public void copy(tiArray local x, tiRectDomain d);
  native public void copy(tiArray local nonshared x, tiRectDomain d);
  native public void copy(tiArray local polyshared x, tiRectDomain d);
  native public local void copy(tiArray x, tiRectDomain d);
  native public local void copy(tiArray local x, tiRectDomain d);
  native public local void copy(tiArray local nonshared x, tiRectDomain d);
  native public local void copy(tiArray local polyshared x, tiRectDomain d);
  native public local nonshared void copy(tiArray x, tiRectDomain d);
  native public local nonshared void copy(tiArray local x, tiRectDomain d);
  native public local nonshared void copy(tiArray local nonshared x, tiRectDomain d);
  native public local nonshared void copy(tiArray local polyshared x, tiRectDomain d);
  native public local polyshared void copy(tiArray x, tiRectDomain d);
  native public local polyshared void copy(tiArray local x, tiRectDomain d);
  native public local polyshared void copy(tiArray local nonshared x, tiRectDomain d);
  native public local polyshared void copy(tiArray local polyshared x, tiRectDomain d);  

  //------------------------------------------------------------------------------------
  // Sparse array copy (over point list)
  native public /*global*/ /*shared*/ void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public /*global*/ /*shared*/ void copy(tiArray local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void copy(tiArray local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void copy(tiArray local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void copy(tiArray local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void copy(tiArray local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void copy(tiArray local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void copy(tiArray local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void copy(tiArray local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void copy(tiArray local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void copy(tiArray local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void copy(tiArray local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void copy(tiArray local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void copy(tiArray local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void copy(tiArray local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void copy(tiArray local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void copy(tiArray local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void copy(tiArray local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void copy(tiArray local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void copy(tiArray local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void copy(tiArray local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void copy(tiArray local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void copy(tiArray local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void copy(tiArray local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void copy(tiArray local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void copy(tiArray local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void copy(tiArray /*global*/ /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void copy(tiArray local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void copy(tiArray local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void copy(tiArray local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void copy(tiArray local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void copy(tiArray local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void copy(tiArray local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void copy(tiArray local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void copy(tiArray local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void copy(tiArray local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void copy(tiArray local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void copy(tiArray local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void copy(tiArray local      polyshared x, tiPoint[1d] local      polyshared ptArray);

  //------------------------------------------------------------------------------------
  // Gather
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public /*global*/ /*shared*/ void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void gather(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      polyshared ptArray);

  //------------------------------------------------------------------------------------
  // Scatter
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public /*global*/ /*shared*/ void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      /*shared*/ void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      nonshared  void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] /*global*/ /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      /*shared*/ x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      nonshared  x, tiPoint[1d] local      polyshared ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] /*global*/ /*shared*/ ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      /*shared*/ ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      nonshared  ptArray);
  native public local      polyshared void scatter(tiTemplateArgument[1d] local      polyshared x, tiPoint[1d] local      polyshared ptArray);

  private polyshared tiArray() { }
}

