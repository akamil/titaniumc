package ti.io;

public class BulkDataOutputStream extends java.io.DataOutputStream implements BulkDataOutput {

  // constructors

  public BulkDataOutputStream(java.io.OutputStream in) {
    super(in);
    }

  // bulk synchronous write

  public void writeArray(Object primjavaarray, int arrayoffset, int count) 
    throws java.io.IOException, NullPointerException, IndexOutOfBoundsException {
    if (count <= 0) throw new java.io.IOException("element count must be > 0 in BulkDataOutputStream.writeArray()");

    writeArray0(primjavaarray, arrayoffset, count);
    }
  public void writeArray(Object primjavaarray) 
    throws java.io.IOException, NullPointerException, IndexOutOfBoundsException {
    writeArray0(primjavaarray, 0, -1);
    }
  private native void writeArray0(Object primjavaarray, int arrayoffset, int count);

  };
