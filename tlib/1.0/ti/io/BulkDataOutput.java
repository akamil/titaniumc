package ti.io;

public interface BulkDataOutput extends java.io.DataOutput {
  public void writeArray(Object primjavaarray) 
    throws java.io.IOException, NullPointerException, IndexOutOfBoundsException;
  public void writeArray(Object primjavaarray, int arrayoffset, int count) 
    throws java.io.IOException, NullPointerException, IndexOutOfBoundsException;
	}
