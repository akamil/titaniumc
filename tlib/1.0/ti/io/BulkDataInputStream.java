package ti.io;

public class BulkDataInputStream extends java.io.DataInputStream implements BulkDataInput  {

  // constructors

  public BulkDataInputStream(java.io.InputStream in) {
    super(in);
    }

  // bulk synchronous read

  public void readArray(Object primjavaarray, int arrayoffset, int count) 
    throws java.io.IOException, NullPointerException, IndexOutOfBoundsException {
    if (count <= 0) throw new java.io.IOException("element count must be > 0 in BulkDataInputStream.readArray()");

    readArray0(primjavaarray, arrayoffset, count);
    }
  public void readArray(Object primjavaarray) 
    throws java.io.IOException, NullPointerException, IndexOutOfBoundsException {
    readArray0(primjavaarray, 0, -1);
    }
  private native void readArray0(Object primjavaarray, int arrayoffset, int count);

  };
