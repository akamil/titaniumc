package ti.io;

public class BulkRandomAccessFile extends java.io.RandomAccessFile implements BulkDataInput,BulkDataOutput {

  // constructors

  public BulkRandomAccessFile(String name, String mode) throws java.io.IOException {
    super(name, mode);
    }
  public BulkRandomAccessFile(java.io.File file, String mode) throws java.io.IOException {
    super(file, mode);
    }

  // bulk synchronous read

  public void readArray(Object primjavaarray, int arrayoffset, int count) 
    throws java.io.IOException, NullPointerException, IndexOutOfBoundsException {
    if (count <= 0) throw new java.io.IOException("element count must be > 0 in BulkRandomAccessFile.readArray()");

    readArray0(primjavaarray, arrayoffset, count);
    }
  public void readArray(Object primjavaarray) 
    throws java.io.IOException, NullPointerException, IndexOutOfBoundsException {
    readArray0(primjavaarray, 0, -1);
    }
  private native void readArray0(Object primjavaarray, int arrayoffset, int count);

  // bulk synchronous write

  public void writeArray(Object primjavaarray, int arrayoffset, int count) 
    throws java.io.IOException, NullPointerException, IndexOutOfBoundsException {
    if (count <= 0) throw new java.io.IOException("element count must be > 0 in BulkRandomAccessFile.writeArray()");

    writeArray0(primjavaarray, arrayoffset, count);
    }
  public void writeArray(Object primjavaarray) 
    throws java.io.IOException, NullPointerException, IndexOutOfBoundsException {
    writeArray0(primjavaarray, 0, -1);
    }
  private native void writeArray0(Object primjavaarray, int arrayoffset, int count);


  };
