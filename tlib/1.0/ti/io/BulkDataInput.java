package ti.io;

public interface BulkDataInput extends java.io.DataInput {
  public void readArray(Object primjavaarray) 
    throws java.io.IOException, NullPointerException, IndexOutOfBoundsException;
  public void readArray(Object primjavaarray, int arrayoffset, int count) 
    throws java.io.IOException, NullPointerException, IndexOutOfBoundsException;
	}
