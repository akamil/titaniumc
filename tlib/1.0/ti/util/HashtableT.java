/* A simple generic hash table implementation. Does not provide any removal
 * functionality. Uses pointer/immutable equality to compare keys. Clients
 * that require alternate equality comparisons should wrap their keys in an
 * immutable, such as
 *   immutable class HTKey {
 *     Key k;
 *     inline public boolean single op==(HTKey single hk) {
 *       return k.equals(hk.k);
 *     }
 *   }
 * and use the wrapped key instead.
 *
 * Author: Amir Kamil
 * Date: 9/7/05
 */

package ti.util;

template<class K, class T> public class HashtableT {
    private Entry[] table;
    private int size;
    private T none;
    private T[] elements;
    private static final int INITIAL_SIZE = 101;
    private static final float MAX_LOAD = 0.75f;
    /** Construct a new HashtableT. 'none' is the element to be returned by get()
     *  if the given key is not found.
     */
    public HashtableT(T none) {
	table = new (this.regionOf()) Entry[INITIAL_SIZE];
	size = 0;
	this.none = none;
	elements = new (this.regionOf()) T[((int ) (MAX_LOAD * INITIAL_SIZE)) + 2];
    }
    private void rehash() {
	Entry[] tmp = new (this.regionOf()) Entry[table.length * 2 + 1];
	for (int i = 0; i < table.length; i++) {
	    for (Entry e = table[i]; e != null; ) {
		Entry crnt = e;
		e = e.next;
		int index = crnt.hash % tmp.length;
		crnt.next = tmp[index];
		tmp[index] = crnt;
	    }
	}
	table = tmp;
	T[] tmp2 = new (this.regionOf()) T[((int) (MAX_LOAD * table.length)) + 2];
	System.arraycopy(elements, 0, tmp2, 0, size);
	elements = tmp2;
    }
    inline public void put(K key, T item) {
	put(key, key.hashCode(), item);
    }
    inline public void put(K key, int hash, T item) {
	Entry e = getEntry(key, hash);
	if (e != null) {
	    e.item = item;
	    elements[e.index] = item;
	    return;
	}
	if (size > MAX_LOAD * table.length) {
	    rehash();
	}
	e = new (this.regionOf()) Entry();
	e.hash = (0x7FFFFFFF & hash);
	e.key = key;
	e.item = item;
	e.index = size++;
	int index = e.hash % table.length;
	e.next = table[index];
	table[index] = e;
	elements[e.index] = item;
    }
    inline private Entry getEntry(K key, int h) {
	int hash = (0x7FFFFFFF & h);
	int index = hash % table.length;
	for (Entry e = table[index]; e != null; e = e.next) {
	    if (e.hash == hash &&
		key == e.key) { // use pointer/immutable equality
		return e;
	    }
	}
	return null;
    }
    inline public T get(K key) {
	return get(key, key.hashCode());
    }
    inline public T get(K key, int hash) {
	Entry e = getEntry(key, hash);
	return e == null ? none : e.item;
    }
    inline public int size() {
	return size;
    }
    inline public T elementAt(int i) {
	return elements[i];
    }
    private static class Entry {
	private int hash;
	private K key;
	private T item;
	private Entry next;
	private int index;
    }
}
