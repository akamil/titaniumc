/* A simple generic vector implementation. Does not provide any removal
 * functionality.
 *
 * Author: Amir Kamil
 * Date: 9/7/05
 */

package ti.util;

template<class T> public class VectorT {
    private T[] elements;
    private int size;
    public VectorT() {
	this(10);
    }
    public VectorT(int cap) {
	elements = new (this.regionOf()) T[cap];
	size = 0;
    }
    private void expand() {
	T[] tmp = new (this.regionOf()) T[size * 2 + 1];
	System.arraycopy(elements, 0, tmp, 0, size);
	elements = tmp;
    }
    inline public void addElement(T t) {
	if (elements.length == size) {
	    expand();
	}
	elements[size++] = t;
    }
    inline public T elementAt(int i) {
	return elements[i];
    }
    inline public int size() {
	return size;
    }
    inline public void clear() {
	size = 0;
    }
    inline public void copy(VectorT<T> src) {
	size = src.size;
	if (elements.length < size) {
	    elements = new (this.regionOf()) T[size * 2 + 1];
	}
	System.arraycopy(src.elements, 0, elements, 0, size);
    }
}
