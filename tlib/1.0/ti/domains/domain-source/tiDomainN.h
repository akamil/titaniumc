//
// ======
// Domain
// ======
//
// A domain is a group of points.
//

#include "domains.h"

abstract public class tiDomain {

  public static tiDomain local toDomain(tiRectDomain [1d] rda) {
    return new (tiDomains.region) tiMultiRectADomain(rda);
  }
  public static tiDomain local toDomain(Point<ARITY> [1d] pa) {
    return new (tiDomains.region) tiMultiRectADomain(pa);
  }
  public static tiDomain local copy_constructor(Region r, tiDomain d) {
    return new (r) tiMultiRectADomain((tiMultiRectADomain)d);
  }

  // set relations
  public abstract tiDomain op+(tiDomain d);
  public abstract tiDomain op-(tiDomain d);
  public abstract tiDomain op*(tiDomain d);
  public abstract tiDomain op+=(tiDomain d);
  public abstract tiDomain op-=(tiDomain d);
  public abstract tiDomain op*=(tiDomain d);

  public abstract boolean isNull();
  public abstract boolean isNotNull();
  public abstract boolean isEmpty();
  public abstract boolean isNotEmpty();
  public abstract boolean isADR();
  public abstract boolean op<=(tiDomain d); // isSubset
  public abstract boolean op<(tiDomain d);  // isStrictSubset
  public abstract boolean op>=(tiDomain d); // isSuperset
  public abstract boolean op>(tiDomain d);  // isStrictSuperset

  // rect domain relations
  public abstract tiDomain op+(tiRectDomain rd);
  public abstract tiDomain op-(tiRectDomain rd);
  public abstract tiDomain op*(tiRectDomain rd);
  public abstract tiDomain op+=(tiRectDomain d);
  public abstract tiDomain op-=(tiRectDomain d);
  public abstract tiDomain op*=(tiRectDomain d);

  public abstract boolean op<=(tiRectDomain rd); // isSubset
  public abstract boolean op<(tiRectDomain rd);  // isStrictSubset
  public abstract boolean op>=(tiRectDomain rd); // isSuperset
  public abstract boolean op>(tiRectDomain rd);  // isStrictSuperset

  // point relations
  public abstract tiDomain op+(Point<ARITY> p);
  public abstract tiDomain op-(Point<ARITY> p);
  public abstract tiDomain op/(Point<ARITY> p);
  public abstract tiDomain op*(Point<ARITY> p);
  public abstract tiDomain op+=(Point<ARITY> p);
  public abstract tiDomain op-=(Point<ARITY> p);
  public abstract tiDomain op*=(Point<ARITY> p);
  public abstract tiDomain op/=(Point<ARITY> p);

  public abstract tiDomain permute(Point<ARITY> p);

  // shape information
  public static final inline int getArity() { return ARITY; }
  public abstract boolean contains(Point<ARITY> p);
  public abstract boolean isRectangular();
         abstract tiRectDomain[] isRectangularEx();
  public abstract int size();
  public abstract long size64();
  public abstract Point<ARITY> min();
  public abstract Point<ARITY> lwb();
  public abstract Point<ARITY> upb();
  public abstract Point<ARITY> max();
  public abstract tiRectDomain boundingBox();
  public abstract tiRectDomain[] local getRectsJArray();
  public abstract tiRectDomain [1d] RectDomainList();
  public abstract tiRectDomain [1d] local RectDomainListLocal();
  public abstract Point<ARITY> [1d] PointList();
  public abstract Point<ARITY> [1d] local PointListLocal();

  // Object overrides
  public abstract String toString();
  public abstract int hashCode();
  public abstract boolean equals(Object obj);

  // dynamic optimizations
  public abstract tiDomain demote();
  public abstract void optimize();

  // debugging
  private void println() {
    System.out.println(this);
  }

  // Hack for memory allocation - sets region used for all domain allocations and returns
  // old one. Use null for GCed allocation.
  public static Region setRegion(Region r) {
    Region old = tiDomains.region;
    tiDomains.region = r;
    return old;
  }
}
