
#ifndef ARITY
#error ARITY not defined!!!
#endif

#ifndef ARITYM1
#error ARITYM1 not defined!!!
#endif

#ifndef _CONCAT
#define _CONCAT_HELPER(a,b) a ## b
#define _CONCAT(a,b) _CONCAT_HELPER(a,b)
#endif

#ifndef _STRINGIFY
#define _STRINGIFY_HELPER(x) #x
#define _STRINGIFY(x) _STRINGIFY_HELPER(x)
#endif

#define tiDomain                  _CONCAT(tiDomain, ARITY)
#define tiMultiRectADomain        _CONCAT(tiMultiRectADomain, ARITY)
#define tiRectDomain              _CONCAT(tiRectDomain, ARITY)
#define tiRectDomainM1            _CONCAT(tiRectDomai, _CONCAT(n, ARITYM1))

package ti.domains;

#define ZERO_POINT Point<ARITY>.all(0)
#define ONES_POINT Point<ARITY>.all(1)
#define NEGONE_POINT Point<ARITY>.all(-1)

// Disable bounds-checking and internal assertions for the ti.domains.* library code by default
#ifndef TI_DOMAINS_BCHECK
#define TI_DOMAINS_BCHECK 0
#endif
#ifndef TI_DOMAINS_INTERNALCHECK
#define TI_DOMAINS_INTERNALCHECK 0
#endif

// Enable checking for user errors by default, and allow it to be folded away in opt mode
#ifndef TI_DOMAINS_USERCHECK
#define TI_DOMAINS_USERCHECK 1
#endif
#ifndef TI_DOMAINS_USERCHECK_OPT
#define TI_DOMAINS_USERCHECK_OPT 0
#endif

#if !TI_DOMAINS_BCHECK
  #pragma TI nobcheck
#endif

/**Following sections define error-checking macros
 * CHECK     - check for a user-generated error (ie, validate user input), dump error to System.out on failure. Enabled by #define TI_DOMAINS_USERCHECK.
 * CHECKF    - as CHECK, but also perform a programmer-defined failure action. Enabled by #define TI_DOMAINS_USERCHECK.
 * ASSERT    - check for programmer error (ie, internal assertions for representation invariants, etc). Java assert on the condition. enabled by TI_DOMAINS_INTERNALCHECK.
 * ASSERTMSG - as ASSERTMSG, but with a programmer-defined failure message. Enabled by #define TI_DOMAINS_INTERNALCHECK.
 */

#if TI_DOMAINS_USERCHECK
  //define macro for user-code error checking, without and with conditional failure execution
  #if TI_DOMAINS_USERCHECK_OPT
    #define CHECK(assertion,errmsg) \
      if (!(assertion)) System.out.println("Runtime: (Error) "+errmsg);
    #define CHECKF(assertion,errmsg,failureaction) \
      if (!(assertion)) { System.out.println("Runtime: (Error) "+errmsg); failureaction}
  #else
    #define CHECK(assertion,errmsg) \
      if (!CompileSettings.optimize) if (!(assertion)) System.out.println("Runtime: (Error) "+errmsg);
    #define CHECKF(assertion,errmsg,failureaction) \
      if (!CompileSettings.optimize) if (!(assertion)) { System.out.println("Runtime: (Error) "+errmsg); failureaction}
  #endif
#else
  #define CHECK(a,b)
  #define CHECKF(a,b,c)
#endif

#if TI_DOMAINS_INTERNALCHECK
  //define macro for internal error checking
  #define ASSERTMSG(assertion,errmsg) do {assert (assertion) : errmsg; } while (false)
  #define ASSERT(assertion) do { assert (assertion); } while (false)
#else
  #define ASSERT(x)
  #define ASSERTMSG(x,y)
#endif

