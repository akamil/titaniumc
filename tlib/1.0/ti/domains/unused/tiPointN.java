
// Uses fake templates.  Script replaces V_ARITY with specified.
// WARNING: this file is currently not used for anything - DOB

package ti.domains;

//
// =====
// Point
// =====
//
// This represents a point in Z^N space, where N is the "arity".
//

public immutable class tiPointV_ARITY {

  // Constructors
  public tiPointV_ARITY() {
    val = new (tiDomains.region) int[V_ARITY];
  }
  public tiPointV_ARITY(tiPointV_ARITY p) {
    val = new (tiDomains.region) int[V_ARITY];
    for (int i = 0; i < getArity(); i++) {
      val[i] = p.get(i);
    }
  }
  public tiPointV_ARITY(int value) {
    val = new (tiDomains.region) int[V_ARITY];
    for (int i = 0; i < getArity(); i++) {
      val[i] = value;
    }
  }
  public tiPointV_ARITY(int dir, int k) {
    int direction = tiDomainUtils.direction_value(dir);

    val = new (tiDomains.region) int[V_ARITY];
    for (int i = 0; i < getArity(); i++) {
      if (i == direction) {
	val[i] = k*(tiDomainUtils.direction_backward(dir) ? -1 : 1);
      } else {
	val[i] = 0;
      }
    }
  }

  // Information
  public static final inline int getArity() { return V_ARITY; }
  public int get(int index) {
    return val[index];
  }
  public int getPrevious(int index) {
    return val[index-1];
  }

  // Assignment
  public tiPointV_ARITY setThis(int index, int value) {
    val[index] = value;
    return this;
  }
  public tiPointV_ARITY setAllThis(int value) {
    for (int i = 0; i < getArity(); i++) {
      val[i] = value;
    }
    return this;
  }
  public tiPointV_ARITY set(int index, int value) {
    tiPointV_ARITY new_p = new tiPointV_ARITY(this);
    new_p.val[index] = value;
    return new_p;
  }
  public static tiPointV_ARITY setDirection(int dir, int k) {
    return new tiPointV_ARITY(dir, k);
  }
  public static tiPointV_ARITY setDirection(int dir) {
    return new tiPointV_ARITY(dir, 1);
  }
  public static tiPointV_ARITY setAll(int val) {
    return new tiPointV_ARITY(val);
  }

  // Arithmetic relationships
  public tiPointV_ARITY add(tiPointV_ARITY p) {
    tiPointV_ARITY new_p = new tiPointV_ARITY();
    for (int i = 0; i < getArity(); i++) {
      new_p.val[i] = val[i] + p.val[i];
    }
    return new_p;
  }
  public tiPointV_ARITY sub(tiPointV_ARITY p) {
    tiPointV_ARITY new_p = new tiPointV_ARITY();
    for (int i = 0; i < getArity(); i++) {
      new_p.val[i] = val[i] - p.val[i];
    }
    return new_p;
  }
  public tiPointV_ARITY mul(int value) {
    tiPointV_ARITY new_p = new tiPointV_ARITY();
    for (int i = 0; i < getArity(); i++) {
      new_p.val[i] = val[i] * value;
    }
    return new_p;
  }
  public tiPointV_ARITY mul(tiPointV_ARITY p) {
    tiPointV_ARITY new_p = new tiPointV_ARITY();
    for (int i = 0; i < getArity(); i++) {
      new_p.val[i] = val[i] * p.val[i];
    }
    return new_p;
  }
  public tiPointV_ARITY div(int value) {
    return div(new tiPointV_ARITY(value));
  }
  public tiPointV_ARITY div(tiPointV_ARITY p) {
    tiPointV_ARITY new_p = new tiPointV_ARITY();
    for (int i = 0; i < getArity(); i++) {
      if (p.val[i] == 0) {
	System.out.println("Runtime: (Error) division by 0");
	new_p.val[i] = 0;
      } else {
	// divide rounding toward zero from both directions
	if (val[i] < 0) {
	  new_p.val[i] = ((val[i] - (p.val[i]-1)) / p.val[i]);
	} else {
	  new_p.val[i] = val[i] / p.val[i];
	}
      }
    }
    return new_p;
  }
  public int dot(tiPointV_ARITY p) {
    int result = 0;
    for (int i = 0; i < getArity(); i++) {
      result += (val[i] * p.val[i]);
    }
    return result;
  }

  // Boolean relationships
  public boolean isEqual(tiPointV_ARITY p) {
    boolean result = true;
    for (int i = 0; (i < getArity()) && result; i++) {
      if (val[i] != p.val[i]) {
	result = false;
      }
    }
    return result;
  }
  public boolean isNotEqual(tiPointV_ARITY p) {
    boolean result = true;
    for (int i = 0; (i < getArity()) && result; i++) {
      if (val[i] != p.val[i]) {
	return true;
      }
    }
    return false;
  }
  public boolean isLessThan(tiPointV_ARITY p) {
    boolean result = true;
    for (int i = 0; (i < getArity()) && result; i++) {
      if (!(val[i] < p.val[i])) {
	result = false;
      }
    }
    return result;
  }
  public boolean isLessThanEqual(tiPointV_ARITY p) {
    boolean result = true;
    for (int i = 0; (i < getArity()) && result; i++) {
      if (!(val[i] <= p.val[i])) {
	result = false;
      }
    }
    return result;
  }
  public boolean isGreaterThan(tiPointV_ARITY p) {
    boolean result = true;
    for (int i = 0; (i < getArity()) && result; i++) {
      if (!(val[i] > p.val[i])) {
	result = false;
      }
    }
    return result;
  }
  public boolean isGreaterThanEqual(tiPointV_ARITY p) {
    boolean result = true;
    for (int i = 0; (i < getArity()) && result; i++) {
      if (!(val[i] >= p.val[i])) {
	result = false;
      }
    }
    return result;
  }

  // Point-Point relationships
  public tiPointV_ARITY getLowerBound(tiPointV_ARITY p) {
    tiPointV_ARITY new_p = new tiPointV_ARITY();
    for (int i = 0; i < getArity(); i++) {
      new_p.val[i] = (val[i] < p.val[i]) ? val[i] : p.val[i];
    }
    return new_p;
  }
  public tiPointV_ARITY getUpperBound(tiPointV_ARITY p) {
    tiPointV_ARITY new_p = new tiPointV_ARITY();
    for (int i = 0; i < getArity(); i++) {
      new_p.val[i] = (val[i] > p.val[i]) ? val[i] : p.val[i];
    }
    return new_p;
  }
  public tiPointV_ARITY getLcm(tiPointV_ARITY p) {
    tiPointV_ARITY new_p = new tiPointV_ARITY();
    for (int i = 0; i < getArity(); i++) {
      new_p.val[i] = tiDomainUtils.lcm(val[i], p.val[i]);
    }
    return new_p;
  }
  public tiPointV_ARITY permute(tiPointV_ARITY p) {
    tiPointV_ARITY new_p = new tiPointV_ARITY();
    for (int i = 0; i < getArity(); i++) {
      new_p.val[i] = val[p.val[i]];
    }
    return new_p;
  }

  // State
  // private final int val[] = new int[V_ARITY];
  private int val[];
}
