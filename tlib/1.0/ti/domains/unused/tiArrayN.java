
// Uses fake templates.  Script replaces V_ARITY with specified.
// WARNING: this file is currently not used for anything - DOB

package ti.domains;

#pragma TI nobcheck

//
// ===========
// Grid Arrays
// ===========
//
// This is an array mapped onto a RectDomain.  Indicies into the
// array are Points.
//

class tiArrayV_ARITY {

  public tiArrayV_ARITY(tiRectDomainV_ARITY rd) {
    rect = rd;
    data = new Object[rd.size()];
    sideFactors = new tiPointV_ARITY();

    // Calculate and cache the "side factors".  These are used in
    // converting a point to an array index and helping in permuting.
    int sf = 1;
    for (int i = 0; i < rd.getArity(); i++) {
      sideFactors.setThis(i, sf);
      sf = sf * rect.getNumSidePoints(i);
    }
  }
  public tiArrayV_ARITY() { }

  private int convertPointToIndex(tiPointV_ARITY p) {
    return p.sub(rect.min()).div(rect.stride()).dot(sideFactors);
  }

  public tiArrayV_ARITY set(tiPointV_ARITY p, Object obj) {
    // assert rect.contains(p);
    data[convertPointToIndex(p)] = obj;
    return this;
  }

  public Object get(tiPointV_ARITY p) {
    // assert rect.contains(p);
    return data[convertPointToIndex(p)];
  }

  public tiRectDomainV_ARITY getDomain() {
    return rect;
  }

  public static final inline int getArity() {
    return rect.getArity();
  }

  public tiArrayV_ARITY copy(tiArrayV_ARITY ga) {
    if (rect == ga.rect) {
      int num_points = rect.size();
      for (int i = 0; i < num_points; i++) {
	data[i] = ga.data[i];
      }
      sideFactors = ga.sideFactors;
    } else {
      System.out.print("Error: unimplemented.");
      // tiRectDomainV_ARITY id;
      // id = rect.multiply(ga.rect);
      // for each p in id {
      //   set(p, ga.get(p));
      // }
    }

    return this;
  }

  public tiArrayV_ARITY transpose(tiPointV_ARITY p) {
    tiArrayV_ARITY ga = new tiArrayV_ARITY();

    ga.rect = rect.permute(p);
    ga.data = data;
    ga.sideFactors = sideFactors.permute(p);
    return ga;
  }

  public tiArrayV_ARITY translate(tiPointV_ARITY p) {
    tiArrayV_ARITY ga = new tiArrayV_ARITY();

    ga.rect = rect.translate(p);
    ga.data = data;
    ga.sideFactors = sideFactors;
    return ga;
  }

  public tiArrayV_ARITY inject(tiPointV_ARITY p) {
    tiArrayV_ARITY ga = new tiArrayV_ARITY();

    ga.rect = rect.inject(p);
    ga.data = data;
    ga.sideFactors = sideFactors;
    return ga;
  }

  public tiArrayV_ARITY project(tiPointV_ARITY p) {
    tiArrayV_ARITY ga = new tiArrayV_ARITY();

    ga.rect = rect.project(p);
    ga.data = data;
    ga.sideFactors = sideFactors;
    return ga;
  }

  // State
  private tiRectDomainV_ARITY rect;
  private Object data[];
  private tiPointV_ARITY sideFactors;
}
