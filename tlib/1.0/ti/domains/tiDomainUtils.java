package ti.domains;

#pragma TI nobcheck

//
// =====
// Utils
// =====
//

final class tiDomainUtils {
  static inline int gcd(int a, int b) {
    return (b == 0) ? a : gcd(b, a % b);
  }

  static inline int lcm(int a, int b) {
    return ((a == 0) || (b == 0)) ? 0 : (a * (b / gcd(a, b)));
  }

  static inline int max(int a, int b) {
    return (a < b) ? b : a;
  }

  static inline int min(int a, int b) {
    return (a < b) ? a : b;
  }

  static inline int abs(int a) {
    return (a < 0) ? -a : a;
  }

  static inline int align_down(int value, int alignment) {
    return value - (value % alignment);
  }

  static inline int align_up(int value, int alignment) {
    return align_down(value + alignment - 1, alignment);
  }

  static inline int direction_value(int dir) {
    // return (dir < 0) ? -dir : dir;
    return (dir < 0) ? -(dir+1) : dir-1;
  }

  static inline boolean direction_backward(int dir) {
    return (dir < 0);
  }

}
