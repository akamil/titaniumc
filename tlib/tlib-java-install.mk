

install-tlib-java: 
	@mydir=`pwd` ; 									\
	 cd $(top_srcdir)/tlib/$(TLIB_VERSION)/$(tlib_root) || exit $? ;		\
	 for file in $(tlib_entries) ; do						\
	  cd "$$mydir" ; 								\
	  if test -f "$(top_srcdir)/tlib/$(TLIB_VERSION)/$(tlib_root)/$$file" ; then	\
            if test `basename $$file .java` != "$$file" -o				\
                    `basename $$file .ti` != "$$file" -o 	   			\
                    `basename $$file .cti` != "$$file" ; then				\
	       $(mkinstalldirs) "$(DESTDIR)$(datadir)/tlib/$(TLIB_VERSION)/$(tlib_root)" ; \
	       echo "Installing tlib-java file: $(TLIB_VERSION)/$(tlib_root)/$$file" ;	\
	       $(INSTALL_DATA) "$(top_srcdir)/tlib/$(TLIB_VERSION)/$(tlib_root)/$$file"	\
                      "$(DESTDIR)$(datadir)/tlib/$(TLIB_VERSION)/$(tlib_root)/$$file" ;	\
	    fi ;									\
	  elif test -d "$(top_srcdir)/tlib/$(TLIB_VERSION)/$(tlib_root)/$$file" ; then	\
	    files=`/bin/ls $(top_srcdir)/tlib/$(TLIB_VERSION)/$(tlib_root)/$$file` ;	\
	    files=`echo $$files` ; 							\
	    $(MAKE) tlib_entries="$$files" tlib_root="$(tlib_root)/$$file" install-tlib-java ; \
	  else										\
	    echo "INTERNAL ERROR: bad tlib_entry '$$file', tlib_root='$(tlib_root)'" ; 	\
	  fi										\
	done

uninstall-tlib-java:
	rm -Rf "$(DESTDIR)$(datadir)/tlib/$(TLIB_VERSION)/$(tlib_root)"

