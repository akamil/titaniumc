package sun.util ;
import java.util.GregorianCalendar ;
import java.util.TimeZone ;
import java.util.Locale ;
import java.util.Calendar ;
public class BuddhistCalendar extends GregorianCalendar {
  public BuddhistCalendar ( )   {
	throw new UnsupportedOperationException();
  }
  public BuddhistCalendar ( TimeZone zone )   {
	throw new UnsupportedOperationException();
  }
  public BuddhistCalendar ( Locale aLocale )   {
	throw new UnsupportedOperationException();
  }
  public BuddhistCalendar ( TimeZone zone , Locale aLocale )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public int get ( int field )   {
	throw new UnsupportedOperationException();
  }
  public void set ( int field , int value )   {
	throw new UnsupportedOperationException();
  }
  public void add ( int field , int amount )   {
	throw new UnsupportedOperationException();
  }
  public void roll ( int field , int amount )   {
	throw new UnsupportedOperationException();
  }
  static final long serialVersionUID = -8527488697350388578L;

}
