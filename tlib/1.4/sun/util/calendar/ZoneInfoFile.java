package sun.util.calendar ;
import java.util.HashMap ;
public class ZoneInfoFile {
  public static final byte [ ] JAVAZI_LABEL =  {
    (byte)'j', (byte)'a', (byte)'v', (byte)'a', (byte)'z', (byte)'i', (byte)'\0'
};

  public static final byte JAVAZI_VERSION = 0x01 ;
  public static final byte TAG_RawOffset = 1;
  public static final byte TAG_LastDSTSaving = 2 ;
  public static final byte TAG_CRC32 = 3 ;
  public static final byte TAG_Transition = 4 ;
  public static final byte TAG_Offset = 5 ;
  public static final byte TAG_SimpleTimeZone = 6 ;
  public static final byte TAG_GMTOffsetWillChange = 7 ;
  public static final String JAVAZM_FILE_NAME = "ZoneInfoMappings" ;
  public static final byte[]  JAVAZM_LABEL = {
        (byte)'j', (byte)'a', (byte)'v', (byte)'a', (byte)'z', (byte)'m', (byte)'\0'
    };

  public static final byte JAVAZM_VERSION = 0x01 ;
  public static final byte TAG_ZoneIDs = 64 ;
  public static final byte TAG_RawOffsets = 65 ;
  public static final byte TAG_RawOffsetIndices = 66 ;
  public static final byte TAG_ZoneAliases = 67 ;
  public static final byte TAG_TZDataVersion = 68 ;
  public static String getFileName ( String ID )   {
	throw new UnsupportedOperationException();
  }
  public static ZoneInfo getCustomTimeZone ( String originalId , int gmtOffset )   {
	throw new UnsupportedOperationException();
  }
  public synchronized static ZoneInfo getZoneInfo ( String ID )   {
	throw new UnsupportedOperationException();
  }
  static String [ ] getZoneIDs ( )   {
	throw new UnsupportedOperationException();
  }
  static HashMap getZoneAliases ( )   {
	throw new UnsupportedOperationException();
  }
  static byte [ ] getRawOffsetIndices ( )   {
	throw new UnsupportedOperationException();
  }
  static int [ ] getRawOffsets ( )   {
	throw new UnsupportedOperationException();
  }
}
