package sun.util.calendar ;
import java.lang.ref.SoftReference ;
import java.util.ArrayList ;
import java.util.Date ;
import java.util.GregorianCalendar ;
import java.util.HashMap ;
import java.util.SimpleTimeZone ;
import java.util.TimeZone ;
public class ZoneInfo extends TimeZone {
  public ZoneInfo ( )   {
	throw new UnsupportedOperationException();
  }
  public ZoneInfo ( String ID , int rawOffset )   {
	throw new UnsupportedOperationException();
  }
  ZoneInfo ( String ID , int rawOffset , int dstSavings , int checksum , long [ ] transitions , int [ ] offsets , int [ ] simpleTimeZoneParams , boolean willGMTOffsetChange )   {
	throw new UnsupportedOperationException();
  }
  public int getOffset ( long date )   {
	throw new UnsupportedOperationException();
  }
  public int getOffsets ( long utc , int [ ] offsets )   {
	throw new UnsupportedOperationException();
  }
  public int getOffsetsByWall ( long wall , int [ ] offsets )   {
	throw new UnsupportedOperationException();
  }
  public int getOffset ( int era , int year , int month , int day , int dayOfWeek , int milliseconds )   {
	throw new UnsupportedOperationException();
  }
  public synchronized void setRawOffset ( int offsetMillis )   {
	throw new UnsupportedOperationException();
  }
  public int getRawOffset ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean useDaylightTime ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean inDaylightTime ( Date date )   {
	throw new UnsupportedOperationException();
  }
  public int getDSTSavings ( )   {
	throw new UnsupportedOperationException();
  }
  public String toString ( )   {
	throw new UnsupportedOperationException();
  }
  public static String [ ] getAvailableIDs ( )   {
	throw new UnsupportedOperationException();
  }
  public static String [ ] getAvailableIDs ( int rawOffset )   {
	throw new UnsupportedOperationException();
  }
  public static TimeZone getTimeZone ( String ID )   {
	throw new UnsupportedOperationException();
  }
  public SimpleTimeZone getLastRuleInstance ( )   {
	throw new UnsupportedOperationException();
  }
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  public boolean hasSameRules ( TimeZone other )   {
	throw new UnsupportedOperationException();
  }
}
