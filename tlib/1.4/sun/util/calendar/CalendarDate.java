package sun.util.calendar ;
public class CalendarDate {
  public static final int UNKNOWN = Integer.MIN_VALUE ;
  public CalendarDate ( )   {
	throw new UnsupportedOperationException();
  }
  public CalendarDate ( int year , int month , int date )   {
	throw new UnsupportedOperationException();
  }
  public void setYear ( int year )   {
	throw new UnsupportedOperationException();
  }
  public int getYear ( )   {
	throw new UnsupportedOperationException();
  }
  public void setMonth ( int month )   {
	throw new UnsupportedOperationException();
  }
  public int getMonth ( )   {
	throw new UnsupportedOperationException();
  }
  public void setDate ( int date )   {
	throw new UnsupportedOperationException();
  }
  public int getDate ( )   {
	throw new UnsupportedOperationException();
  }
  public void setDayOfWeek ( int dayOfWeek )   {
	throw new UnsupportedOperationException();
  }
  public int getDayOfWeek ( )   {
	throw new UnsupportedOperationException();
  }
  public void setTimeOfDay ( int time )   {
	throw new UnsupportedOperationException();
  }
  public int getTimeOfDay ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public String toString ( )   {
	throw new UnsupportedOperationException();
  }
}
