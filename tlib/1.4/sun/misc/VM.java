package sun.misc ;
import java.util.Properties ;
public final class VM {
  public static boolean threadsSuspended ( )   {
	throw new UnsupportedOperationException();
  }
  public static boolean allowThreadSuspension ( ThreadGroup g , boolean b )   {
	throw new UnsupportedOperationException();
  }
  public static boolean suspendThreads ( )   {
	throw new UnsupportedOperationException();
  }
  public static void unsuspendThreads ( )   {
	throw new UnsupportedOperationException();
  }
  public static void unsuspendSomeThreads ( )   {
	throw new UnsupportedOperationException();
  }
  public static final int STATE_GREEN = 1 ;
  public static final int STATE_YELLOW = 2 ;
  public static final int STATE_RED = 3 ;
  public static final int getState ( )   {
	throw new UnsupportedOperationException();
  }
//  public static void registerVMNotification ( VMNotification n )   { }  //Troublesome
  public static void asChange ( int as_old , int as_new )   { }
  public static void asChange_otherthread ( int as_old , int as_new )   {
	throw new UnsupportedOperationException();
  }
  public static void booted ( )   {
	throw new UnsupportedOperationException();
  }
  public static boolean isBooted ( )   {
	throw new UnsupportedOperationException();
  }
  public static long maxDirectMemory ( )   {
	throw new UnsupportedOperationException();
  }
}
