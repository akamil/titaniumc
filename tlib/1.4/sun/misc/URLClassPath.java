package sun.misc ;
import java.util.Enumeration ;
import java.net.URL ;
import java.io.IOException ;

public class URLClassPath {
  final static String USER_AGENT_JAVA_VERSION = "UNSUPPORTED-URLClassPath";
  final static String JAVA_VERSION = "UNSUPPORTED-URLClassPath";

/*  public URLClassPath ( URL [ ] urls , URLStreamHandlerFactory factory )   {
	throw new UnsupportedOperationException();
  }*/
  public URLClassPath ( URL [ ] urls )   {
	throw new UnsupportedOperationException();
  }
  public void addURL ( URL url )   {
	throw new UnsupportedOperationException();
  }
  public URL [ ] getURLs ( )   {
	throw new UnsupportedOperationException();
  }
  public URL findResource ( String name , boolean check )   {
	throw new UnsupportedOperationException();
  }
  public Resource getResource ( String name , boolean check )   {
	throw new UnsupportedOperationException();
  }
  public Enumeration findResources ( final String name , final boolean check )   {
	throw new UnsupportedOperationException();
  }
  public Resource getResource ( String name )   {
	throw new UnsupportedOperationException();
  }
  public Enumeration getResources ( final String name , final boolean check )   {
	throw new UnsupportedOperationException();
  }
  public Enumeration getResources ( final String name )   {
	throw new UnsupportedOperationException();
  }
  public static URL [ ] pathToURLs ( String path )   {
	throw new UnsupportedOperationException();
  }
  public URL checkURL ( URL url )   {
	throw new UnsupportedOperationException();
  }
  static void check ( URL url ) throws IOException {
	throw new UnsupportedOperationException();
  }
}