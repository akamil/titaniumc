package sun.misc ;
import java.lang.ref.SoftReference ;
import java.lang.ref.ReferenceQueue ;
import java.util.Iterator ;
import java.util.Map ;
import java.util.AbstractMap ;
import java.util.HashMap ;
import java.util.Set ;
import java.util.AbstractSet ;
import java.util.NoSuchElementException ;
public class SoftCache extends AbstractMap implements Map {

	private Map hash;
	public SoftCache(int initialCapacity, float loadFactor) {
		hash = new HashMap(initialCapacity, loadFactor);
	}
	
	public SoftCache(int initialCapacity) {
		hash = new HashMap(initialCapacity);
	}
	
	public SoftCache() {
		hash = new HashMap();
	}

  public int size ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isEmpty ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean containsKey ( Object key )   {
	throw new UnsupportedOperationException();
  }
  protected Object fill ( Object key )   {
	throw new UnsupportedOperationException();
  }
  public Object get ( Object key )   {
	throw new UnsupportedOperationException();
  }
  public Object put ( Object key , Object value )   {
	throw new UnsupportedOperationException();
  }
  public Object remove ( Object key )   {
	throw new UnsupportedOperationException();
  }
  public void clear ( )   {
	throw new UnsupportedOperationException();
  }
  public Set entrySet ( )   {
	throw new UnsupportedOperationException();
  }
}
