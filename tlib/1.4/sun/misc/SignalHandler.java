package sun.misc ;
public interface SignalHandler {
  public static final SignalHandler SIG_DFL = null;
  public static final SignalHandler SIG_IGN = null;
  public void handle ( Signal sig ) ;
}
