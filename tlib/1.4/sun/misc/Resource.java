package sun.misc ;
import java.net.URL ;
import java.io.IOException ;
import java.io.InputStream ;
import java.util.jar.Manifest ;
import java.util.jar.Attributes ;
public abstract class Resource {
  public abstract String getName ( ) ;
  public abstract URL getURL ( ) ;
  public abstract URL getCodeSourceURL ( ) ;
  public abstract InputStream getInputStream ( ) throws IOException ;
  public abstract int getContentLength ( ) throws IOException ;
  public byte [ ] getBytes ( ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public Manifest getManifest ( ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public java.security.cert.Certificate [ ] getCertificates ( )   {
	throw new UnsupportedOperationException();
  }
}
