package sun.misc ;
import java.util.ArrayList ;
public abstract class ClassFileTransformer {
  public static void add ( ClassFileTransformer t )   {
	throw new UnsupportedOperationException();
  }
  public static Object [ ] getTransformers ( )   {
	throw new UnsupportedOperationException();
  }
  public abstract byte [ ] transform ( byte [ ] b , int off , int len ) throws ClassFormatError ;
}
