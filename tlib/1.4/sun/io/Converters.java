package sun.io ;
import java.io.UnsupportedEncodingException ;
import java.lang.ref.SoftReference ;
import java.util.Properties ;
public final class Converters {
  public static final int BYTE_TO_CHAR = 0 ;
  public static final int CHAR_TO_BYTE = 1 ;

  public static boolean isCached ( int type , String encoding )   {
	throw new UnsupportedOperationException();
  }
  public static String getDefaultEncodingName ( )   {
	throw new UnsupportedOperationException();
  }
  public static void resetDefaultEncodingName ( )   {
	throw new UnsupportedOperationException();
  }
  static Object newConverter ( int type , String enc ) throws UnsupportedEncodingException   {
	throw new UnsupportedOperationException();
  }
  static Object newDefaultConverter ( int type )   {
	throw new UnsupportedOperationException();
  }
}
