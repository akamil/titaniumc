package sun.reflect ;
import java.lang.reflect.Field ;
import java.lang.reflect.Method ;
import java.lang.reflect.Constructor ;
import java.lang.reflect.Modifier ;
import java.security.AccessController ;
import java.security.Permission ;
import java.security.PrivilegedAction ;
public class ReflectionFactory {
    private static ReflectionFactory soleInstance = new ReflectionFactory();

	private ReflectionFactory() {
	}
	public static final class GetReflectionFactoryAction implements PrivilegedAction   {
		public Object run() {
			return getReflectionFactory();
		}
	}
	public static ReflectionFactory getReflectionFactory ( )   {
		return soleInstance;
	}
	public void setLangReflectAccess ( LangReflectAccess access )   {
		throw new UnsupportedOperationException();
	}
	public FieldAccessor newFieldAccessor ( Field field )   {
		throw new UnsupportedOperationException();
	}
	public MethodAccessor newMethodAccessor ( Method method )   {
		throw new UnsupportedOperationException();
	}
	public ConstructorAccessor newConstructorAccessor ( Constructor c )   {
		throw new UnsupportedOperationException();
	}
	public Field newField ( Class declaringClass , String name , Class type , int modifiers , int slot )   {
		throw new UnsupportedOperationException();
	}
	public Method newMethod ( Class declaringClass , String name , Class [ ] parameterTypes , Class returnType , Class [ ] checkedExceptions , int modifiers , int slot )   {
		throw new UnsupportedOperationException();
	}
	public Constructor newConstructor ( Class declaringClass , Class [ ] parameterTypes , Class [ ] checkedExceptions , int modifiers , int slot )   {
		throw new UnsupportedOperationException();
	}
	public MethodAccessor getMethodAccessor ( Method m )   {
		throw new UnsupportedOperationException();
	}
	public void setMethodAccessor ( Method m , MethodAccessor accessor )   {
		throw new UnsupportedOperationException();
	}
	public ConstructorAccessor getConstructorAccessor ( Constructor c )   {
		throw new UnsupportedOperationException();
	}
	public void setConstructorAccessor ( Constructor c , ConstructorAccessor accessor )   {
		throw new UnsupportedOperationException();
	}
	public Method copyMethod ( Method arg )   {
		throw new UnsupportedOperationException();
	}
	public Field copyField ( Field arg )   {
		throw new UnsupportedOperationException();
	}
	public Constructor copyConstructor ( Constructor arg )   {
		throw new UnsupportedOperationException();
	}
	public Constructor newConstructorForSerialization ( Class classToInstantiate , Constructor constructorToCall )   {
		throw new UnsupportedOperationException();
	}
	static int inflationThreshold ( )   {
		throw new UnsupportedOperationException();
	}
}
