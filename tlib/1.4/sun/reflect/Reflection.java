package sun.reflect ;
import java.lang.reflect.Modifier ;
public class Reflection {
  public static native Class getCallerClass ( int realFramesToSkip ) ;
  public static boolean quickCheckMemberAccess ( Class memberClass , int modifiers )   {
	throw new UnsupportedOperationException();
  }
  public static void ensureMemberAccess ( Class currentClass , Class memberClass , Object target , int modifiers ) throws IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public static boolean verifyMemberAccess ( Class currentClass , Class memberClass , Object target , int modifiers )   {
	throw new UnsupportedOperationException();
  }
  static boolean isSubclassOf ( Class queryClass , Class ofClass )   {
	throw new UnsupportedOperationException();
  }
}
