package sun.security.util ;
import java.security.BasicPermission;
import java.security.AllPermission;
import java.security.SecurityPermission;
public final class SecurityConstants {

    public static final String FILE_DELETE_ACTION = "delete";
    public static final String FILE_EXECUTE_ACTION = "execute";
    public static final String FILE_READ_ACTION = "read";
    public static final String FILE_WRITE_ACTION = "write";

    public static final String SOCKET_RESOLVE_ACTION = "resolve";
    public static final String SOCKET_CONNECT_ACTION = "connect";
    public static final String SOCKET_LISTEN_ACTION = "listen";
    public static final String SOCKET_ACCEPT_ACTION = "accept";
    public static final String SOCKET_CONNECT_ACCEPT_ACTION = "connect,accept";

    public static final String PROPERTY_RW_ACTION = "read,write";
    public static final String PROPERTY_READ_ACTION = "read";
    public static final String PROPERTY_WRITE_ACTION = "write";

  public static final AllPermission ALL_PERMISSION = new AllPermission ( ) ;
  public static final BasicPermission TOPLEVEL_WINDOW_PERMISSION = null;
  public static final BasicPermission ACCESS_CLIPBOARD_PERMISSION = null;
  public static final BasicPermission CHECK_AWT_EVENTQUEUE_PERMISSION = null;
  public static final BasicPermission READ_DISPLAY_PIXELS_PERMISSION = null;
  public static final BasicPermission CREATE_ROBOT_PERMISSION = null;
  public static final BasicPermission ALL_AWT_EVENTS_PERMISSION = null;
  public static final BasicPermission SPECIFY_HANDLER_PERMISSION = null;
  public static final RuntimePermission CREATE_CLASSLOADER_PERMISSION = new RuntimePermission ("creatClassLoader") ;
  public static final RuntimePermission CHECK_MEMBER_ACCESS_PERMISSION = new RuntimePermission ("accessDeclaredMembers") ;
  public static final RuntimePermission MODIFY_THREAD_PERMISSION = new RuntimePermission ("modifyThread") ;
  public static final RuntimePermission MODIFY_THREADGROUP_PERMISSION = new RuntimePermission ("modifyThreadGroup") ;
  public static final RuntimePermission GET_PD_PERMISSION = new RuntimePermission ("getProtectionDomain") ;
  public static final RuntimePermission GET_CLASSLOADER_PERMISSION = new RuntimePermission ("getClassLoader") ;
  public static final RuntimePermission STOP_THREAD_PERMISSION = new RuntimePermission ("stopThread") ;
  public static final BasicPermission CREATE_ACC_PERMISSION = new SecurityPermission ("createAccessControlContext") ;
  public static final BasicPermission GET_COMBINER_PERMISSION = new SecurityPermission ("getComainCombiner") ;
  public static final BasicPermission GET_POLICY_PERMISSION = new SecurityPermission ("getPolicy") ;
  public static final BasicPermission LOCAL_LISTEN_PERMISSION = null;
  public static final BasicPermission DO_AS_PERMISSION = null;
  public static final BasicPermission DO_AS_PRIVILEGED_PERMISSION = null;
  }
