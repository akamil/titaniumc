package sun.nio.ch ;
import java.io.FileDescriptor ;
import java.nio.channels. * ;

public class FileChannelImpl extends FileChannel {
  public static FileChannel open ( FileDescriptor fd , boolean readable , boolean writable , Object parent )   {
	throw new UnsupportedOperationException();
  }
  public static FileChannel open ( FileDescriptor fd , boolean readable , boolean writable , Object parent , boolean append )   {
	throw new UnsupportedOperationException();
  }
}
