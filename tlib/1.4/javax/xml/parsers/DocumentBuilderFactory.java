package javax.xml.parsers ;
import java.io.InputStream ;
import java.io.IOException ;
import java.io.File ;
import java.io.FileInputStream ;
import java.util.Properties ;
import java.io.BufferedReader ;
import java.io.InputStreamReader ;
public abstract class DocumentBuilderFactory {
  protected DocumentBuilderFactory ( )   {
	throw new UnsupportedOperationException();
  }
  public static DocumentBuilderFactory newInstance ( ) throws FactoryConfigurationError   {
	throw new UnsupportedOperationException();
  }
  public abstract DocumentBuilder newDocumentBuilder ( ) throws ParserConfigurationException ;
  public void setNamespaceAware ( boolean awareness )   {
	throw new UnsupportedOperationException();
  }
  public void setValidating ( boolean validating )   {
	throw new UnsupportedOperationException();
  }
  public void setIgnoringElementContentWhitespace ( boolean whitespace )   {
	throw new UnsupportedOperationException();
  }
  public void setExpandEntityReferences ( boolean expandEntityRef )   {
	throw new UnsupportedOperationException();
  }
  public void setIgnoringComments ( boolean ignoreComments )   {
	throw new UnsupportedOperationException();
  }
  public void setCoalescing ( boolean coalescing )   {
	throw new UnsupportedOperationException();
  }
  public boolean isNamespaceAware ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isValidating ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isIgnoringElementContentWhitespace ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isExpandEntityReferences ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isIgnoringComments ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isCoalescing ( )   {
	throw new UnsupportedOperationException();
  }
  public abstract void setAttribute ( String name , Object value ) throws IllegalArgumentException ;
  public abstract Object getAttribute ( String name ) throws IllegalArgumentException ;
}
