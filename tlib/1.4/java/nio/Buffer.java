package java.nio ;
public abstract class Buffer {
  long address ;
  Buffer ( int mark , int pos , int lim , int cap )   {
	throw new UnsupportedOperationException();
  }
  public final int capacity ( )   {
	throw new UnsupportedOperationException();
  }
  public final int position ( )   {
	throw new UnsupportedOperationException();
  }
  public final Buffer position ( int newPosition )   {
	throw new UnsupportedOperationException();
  }
  public final int limit ( )   {
	throw new UnsupportedOperationException();
  }
  public final Buffer limit ( int newLimit )   {
	throw new UnsupportedOperationException();
  }
  public final Buffer mark ( )   {
	throw new UnsupportedOperationException();
  }
  public final Buffer reset ( )   {
	throw new UnsupportedOperationException();
  }
  public final Buffer clear ( )   {
	throw new UnsupportedOperationException();
  }
  public final Buffer flip ( )   {
	throw new UnsupportedOperationException();
  }
  public final Buffer rewind ( )   {
	throw new UnsupportedOperationException();
  }
  public final int remaining ( )   {
	throw new UnsupportedOperationException();
  }
  public final boolean hasRemaining ( )   {
	throw new UnsupportedOperationException();
  }
  public abstract boolean isReadOnly ( ) ;
  final int nextGetIndex ( )   {
	throw new UnsupportedOperationException();
  }
  final int nextGetIndex ( int nb )   {
	throw new UnsupportedOperationException();
  }
  final int nextPutIndex ( )   {
	throw new UnsupportedOperationException();
  }
  final int nextPutIndex ( int nb )   {
	throw new UnsupportedOperationException();
  }
  final int checkIndex ( int i )   {
	throw new UnsupportedOperationException();
  }
  final int checkIndex ( int i , int nb )   {
	throw new UnsupportedOperationException();
  }
  final int markValue ( )   {
	throw new UnsupportedOperationException();
  }
  static void checkBounds ( int off , int len , int size )   {
	throw new UnsupportedOperationException();
  }
}
