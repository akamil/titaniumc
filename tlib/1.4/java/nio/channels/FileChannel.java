package java.nio.channels ;

public abstract class FileChannel {
  protected FileChannel ( )   {
	throw new UnsupportedOperationException();
  }
  public final FileLock tryLock() throws java.io.IOException {
  	throw new UnsupportedOperationException();
  }
  public final void close() {
  	throw new UnsupportedOperationException();
  }

}
