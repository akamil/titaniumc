package java.nio ;
public abstract class CharBuffer extends Buffer implements Comparable , CharSequence {
  final char [ ] hb = null;
  final int offset = 0;
  boolean isReadOnly = true;
  CharBuffer ( int mark , int pos , int lim , int cap , char [ ] hb , int offset )   {
	super(mark, pos, lim, cap);
  }
  CharBuffer ( int mark , int pos , int lim , int cap )   {
	super(mark, pos, lim, cap);
  }
  public static CharBuffer allocate ( int capacity )   {
	throw new UnsupportedOperationException();
  }
  public static CharBuffer wrap ( char [ ] array , int offset , int length )   {
	throw new UnsupportedOperationException();
  }
  public static CharBuffer wrap ( char [ ] array )   {
	throw new UnsupportedOperationException();
  }
  public static CharBuffer wrap ( CharSequence csq , int start , int end )   {
	throw new UnsupportedOperationException();
  }
  public static CharBuffer wrap ( CharSequence csq )   {
	throw new UnsupportedOperationException();
  }
  public abstract CharBuffer slice ( ) ;
  public abstract CharBuffer duplicate ( ) ;
  public abstract CharBuffer asReadOnlyBuffer ( ) ;
  public abstract char get ( ) ;
  public abstract CharBuffer put ( char c ) ;
  public abstract char get ( int index ) ;
  public abstract CharBuffer put ( int index , char c ) ;
  public CharBuffer get ( char [ ] dst , int offset , int length )   {
	throw new UnsupportedOperationException();
  }
  public CharBuffer get ( char [ ] dst )   {
	throw new UnsupportedOperationException();
  }
  public CharBuffer put ( CharBuffer src )   {
	throw new UnsupportedOperationException();
  }
  public CharBuffer put ( char [ ] src , int offset , int length )   {
	throw new UnsupportedOperationException();
  }
  public final CharBuffer put ( char [ ] src )   {
	throw new UnsupportedOperationException();
  }
  public CharBuffer put ( String src , int start , int end )   {
	throw new UnsupportedOperationException();
  }
  public final CharBuffer put ( String src )   {
	throw new UnsupportedOperationException();
  }
  public final boolean hasArray ( )   {
	throw new UnsupportedOperationException();
  }
  public final char [ ] array ( )   {
	throw new UnsupportedOperationException();
  }
  public final int arrayOffset ( )   {
	throw new UnsupportedOperationException();
  }
  public abstract CharBuffer compact ( ) ;
  public abstract boolean isDirect ( ) ;
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object ob )   {
	throw new UnsupportedOperationException();
  }
  public int compareTo ( Object ob )   {
	throw new UnsupportedOperationException();
  }
  public String toString ( )   {
	throw new UnsupportedOperationException();
  }
  abstract String toString ( int start , int end ) ;
  public final int length ( )   {
	throw new UnsupportedOperationException();
  }
  public final char charAt ( int index )   {
	throw new UnsupportedOperationException();
  }
  public abstract CharSequence subSequence ( int start , int end ) ;
  public abstract ByteOrder order ( ) ;
}
