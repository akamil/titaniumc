package java.nio.charset ;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.Set ;
import java.util.SortedMap ;
import java.util.Locale;

public abstract class Charset implements Comparable {
  public static boolean isSupported ( String charsetName )   {
	throw new UnsupportedOperationException();
  }
  public static Charset forName ( String charsetName )   {
	throw new UnsupportedOperationException();
  }
  public static SortedMap availableCharsets ( )   {
	throw new UnsupportedOperationException();
  }
  protected Charset ( String canonicalName , String [ ] aliases )   {
	throw new UnsupportedOperationException();
  }
  public final String name ( )   {
	throw new UnsupportedOperationException();
  }
  public final Set aliases ( )   {
	throw new UnsupportedOperationException();
  }
  public String displayName ( )   {
	throw new UnsupportedOperationException();
  }
  public final boolean isRegistered ( )   {
	throw new UnsupportedOperationException();
  }
  public String displayName ( Locale locale )   {
	throw new UnsupportedOperationException();
  }
  public abstract boolean contains ( Charset cs ) ;
  public abstract CharsetDecoder newDecoder ( ) ;
  public abstract CharsetEncoder newEncoder ( ) ;
  public boolean canEncode ( )   {
	throw new UnsupportedOperationException();
  }
  public final CharBuffer decode ( ByteBuffer bb )   {
	throw new UnsupportedOperationException();
  }
  public final ByteBuffer encode ( CharBuffer cb )   {
	throw new UnsupportedOperationException();
  }
  public final ByteBuffer encode ( String str )   {
	throw new UnsupportedOperationException();
  }
  public final int compareTo ( Object ob )   {
	throw new UnsupportedOperationException();
  }
  public final int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public final boolean equals ( Object ob )   {
	throw new UnsupportedOperationException();
  }
  public final String toString ( )   {
	throw new UnsupportedOperationException();
  }
}
