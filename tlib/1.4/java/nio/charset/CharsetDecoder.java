package java.nio.charset ;
import java.nio.ByteBuffer ;
import java.nio.CharBuffer ;

public abstract class CharsetDecoder {
  ;
  protected CharsetDecoder ( Charset cs , float averageCharsPerByte , float maxCharsPerByte )   {
	throw new UnsupportedOperationException();
  }
  public final Charset charset ( )   {
	throw new UnsupportedOperationException();
  }
  public final String replacement ( )   {
	throw new UnsupportedOperationException();
  }
  public final CharsetDecoder replaceWith ( String newReplacement )   {
	throw new UnsupportedOperationException();
  }
  protected void implReplaceWith ( String newReplacement )   {
	throw new UnsupportedOperationException();
  }
  public CodingErrorAction malformedInputAction ( )   {
	throw new UnsupportedOperationException();
  }
  public final CharsetDecoder onMalformedInput ( CodingErrorAction newAction )   {
	throw new UnsupportedOperationException();
  }
  protected void implOnMalformedInput ( CodingErrorAction newAction )   {
	throw new UnsupportedOperationException();
  }
  public CodingErrorAction unmappableCharacterAction ( )   {
	throw new UnsupportedOperationException();
  }
  public final CharsetDecoder onUnmappableCharacter ( CodingErrorAction newAction )   {
	throw new UnsupportedOperationException();
  }
  protected void implOnUnmappableCharacter ( CodingErrorAction newAction )   {
	throw new UnsupportedOperationException();
  }
  public final float averageCharsPerByte ( )   {
	throw new UnsupportedOperationException();
  }
  public final float maxCharsPerByte ( )   {
	throw new UnsupportedOperationException();
  }
  public final CoderResult decode ( ByteBuffer in , CharBuffer out , boolean endOfInput )   {
	throw new UnsupportedOperationException();
  }
  public final CoderResult flush ( CharBuffer out )   {
	throw new UnsupportedOperationException();
  }
  protected CoderResult implFlush ( CharBuffer out )   {
	throw new UnsupportedOperationException();
  }
  public final CharsetDecoder reset ( )   {
	throw new UnsupportedOperationException();
  }
  protected void implReset ( )   {
	throw new UnsupportedOperationException();
  }
  protected abstract CoderResult decodeLoop ( ByteBuffer in , CharBuffer out ) ;
  public final CharBuffer decode ( ByteBuffer in ) throws CharacterCodingException   {
	throw new UnsupportedOperationException();
  }
  public boolean isAutoDetecting ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isCharsetDetected ( )   {
	throw new UnsupportedOperationException();
  }
  public Charset detectedCharset ( )   {
	throw new UnsupportedOperationException();
  }
}
