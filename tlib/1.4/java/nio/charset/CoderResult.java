package java.nio.charset ;

public class CoderResult {
  private static final int CR_UNDERFLOW = 0;
  private static final int CR_OVERFLOW = 1;
  
  private CoderResult(int type, int length) { }
  
  public String toString ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isUnderflow ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isOverflow ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isError ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isMalformed ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isUnmappable ( )   {
	throw new UnsupportedOperationException();
  }
  public int length ( )   {
	throw new UnsupportedOperationException();
  }
  public static final CoderResult UNDERFLOW = new CoderResult ( CR_UNDERFLOW , 0 ) ;
  public static final CoderResult OVERFLOW = new CoderResult ( CR_OVERFLOW , 0 ) ;
  ;
  public static CoderResult malformedForLength ( int length )   {
	throw new UnsupportedOperationException();
  }
  ;
  public static CoderResult unmappableForLength ( int length )   {
	throw new UnsupportedOperationException();
  }
  public void throwException ( ) throws CharacterCodingException   {
	throw new UnsupportedOperationException();
  }
}
