package java.nio.charset ;
public final class CodingErrorAction {
	private CodingErrorAction() { }
	public static final CodingErrorAction IGNORE = new CodingErrorAction();
	public static final CodingErrorAction REPLACE = new CodingErrorAction();
	public static final CodingErrorAction REPORT = new CodingErrorAction();
	public String toString ( )   {
		throw new UnsupportedOperationException();
	}
}
