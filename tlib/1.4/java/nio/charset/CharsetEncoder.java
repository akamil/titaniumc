package java.nio.charset ;
/*import java.nio.Buffer ;
import java.nio.ByteBuffer ;
import java.nio.CharBuffer ;
import java.nio.BufferOverflowException ;
import java.nio.BufferUnderflowException ;*/
//import java.lang.ref.WeakReference ;
//import java.nio.charset.CoderMalfunctionError ;
public abstract class CharsetEncoder {
  ;
  protected CharsetEncoder ( Charset cs , float averageBytesPerChar , float maxBytesPerChar , byte [ ] replacement )   {
	throw new UnsupportedOperationException();
  }
  protected CharsetEncoder ( Charset cs , float averageBytesPerChar , float maxBytesPerChar )   {
	throw new UnsupportedOperationException();
  }
  public final Charset charset ( )   {
	throw new UnsupportedOperationException();
  }
  public final byte [ ] replacement ( )   {
	throw new UnsupportedOperationException();
  }
  public final CharsetEncoder replaceWith ( byte [ ] newReplacement )   {
	throw new UnsupportedOperationException();
  }
  protected void implReplaceWith ( byte [ ] newReplacement )   {
	throw new UnsupportedOperationException();
  }
  public boolean isLegalReplacement ( byte [ ] repl )   {
	throw new UnsupportedOperationException();
  }
  public CodingErrorAction malformedInputAction ( )   {
	throw new UnsupportedOperationException();
  }
  public final CharsetEncoder onMalformedInput ( CodingErrorAction newAction )   {
	throw new UnsupportedOperationException();
  }
  protected void implOnMalformedInput ( CodingErrorAction newAction )   {
	throw new UnsupportedOperationException();
  }
  public CodingErrorAction unmappableCharacterAction ( )   {
	throw new UnsupportedOperationException();
  }
  public final CharsetEncoder onUnmappableCharacter ( CodingErrorAction newAction )   {
	throw new UnsupportedOperationException();
  }
  protected void implOnUnmappableCharacter ( CodingErrorAction newAction )   {
	throw new UnsupportedOperationException();
  }
  public final float averageBytesPerChar ( )   {
	throw new UnsupportedOperationException();
  }
  public final float maxBytesPerChar ( )   {
	throw new UnsupportedOperationException();
  }
/*  public final CoderResult encode ( CharBuffer in , ByteBuffer out , boolean endOfInput )   {
	throw new UnsupportedOperationException();
  }
  public final CoderResult flush ( ByteBuffer out )   {
	throw new UnsupportedOperationException();
  }
  protected CoderResult implFlush ( ByteBuffer out )   {
	throw new UnsupportedOperationException();
  }
*/  public final CharsetEncoder reset ( )   {
	throw new UnsupportedOperationException();
  }
  protected void implReset ( )   {
	throw new UnsupportedOperationException();
  }
  /*protected abstract CoderResult encodeLoop ( CharBuffer in , ByteBuffer out ) ;
  public final ByteBuffer encode ( CharBuffer in ) throws CharacterCodingException   {
	throw new UnsupportedOperationException();
  }
*/  public boolean canEncode ( char c )   {
	throw new UnsupportedOperationException();
  }
  public boolean canEncode ( CharSequence cs )   {
	throw new UnsupportedOperationException();
  }
  }
