package java.net ;
import java.io.IOException ;
import java.io.InvalidObjectException ;
import java.io.Serializable ;
import java.nio.ByteBuffer ;
import java.nio.CharBuffer ;
import java.nio.charset.CharsetDecoder ;
import java.nio.charset.CharsetEncoder ;
import java.nio.charset.CoderResult ;
import java.nio.charset.CodingErrorAction ;
import java.nio.charset.CharacterCodingException ;
//import sun.nio.cs.ThreadLocalCoders ;
//import sun.text.Normalizer ;
import java.lang.Character ;
import java.lang.NullPointerException ;
public final class URI implements Comparable , Serializable {
    static final long serialVersionUID = -6052424284110960213L;
  public URI ( String str ) throws URISyntaxException   {
	throw new UnsupportedOperationException();
  }
  public URI ( String scheme , String userInfo , String host , int port , String path , String query , String fragment ) throws URISyntaxException   {
	throw new UnsupportedOperationException();
  }
  public URI ( String scheme , String authority , String path , String query , String fragment ) throws URISyntaxException   {
	throw new UnsupportedOperationException();
  }
  public URI ( String scheme , String host , String path , String fragment ) throws URISyntaxException   {
	throw new UnsupportedOperationException();
  }
  public URI ( String scheme , String ssp , String fragment ) throws URISyntaxException   {
	throw new UnsupportedOperationException();
  }
  public static URI create ( String str )   {
	throw new UnsupportedOperationException();
  }
  public URI parseServerAuthority ( ) throws URISyntaxException   {
	throw new UnsupportedOperationException();
  }
  public URI normalize ( )   {
	throw new UnsupportedOperationException();
  }
  public URI resolve ( URI uri )   {
	throw new UnsupportedOperationException();
  }
  public URI resolve ( String str )   {
	throw new UnsupportedOperationException();
  }
  public URI relativize ( URI uri )   {
	throw new UnsupportedOperationException();
  }
  public URL toURL ( ) throws MalformedURLException   {
	throw new UnsupportedOperationException();
  }
  public String getScheme ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isAbsolute ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isOpaque ( )   {
	throw new UnsupportedOperationException();
  }
  public String getRawSchemeSpecificPart ( )   {
	throw new UnsupportedOperationException();
  }
  public String getSchemeSpecificPart ( )   {
	throw new UnsupportedOperationException();
  }
  public String getRawAuthority ( )   {
	throw new UnsupportedOperationException();
  }
  public String getAuthority ( )   {
	throw new UnsupportedOperationException();
  }
  public String getRawUserInfo ( )   {
	throw new UnsupportedOperationException();
  }
  public String getUserInfo ( )   {
	throw new UnsupportedOperationException();
  }
  public String getHost ( )   {
	throw new UnsupportedOperationException();
  }
  public int getPort ( )   {
	throw new UnsupportedOperationException();
  }
  public String getRawPath ( )   {
	throw new UnsupportedOperationException();
  }
  public String getPath ( )   {
	throw new UnsupportedOperationException();
  }
  public String getRawQuery ( )   {
	throw new UnsupportedOperationException();
  }
  public String getQuery ( )   {
	throw new UnsupportedOperationException();
  }
  public String getRawFragment ( )   {
	throw new UnsupportedOperationException();
  }
  public String getFragment ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object ob )   {
	throw new UnsupportedOperationException();
  }
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public int compareTo ( Object ob )   {
	throw new UnsupportedOperationException();
  }
  public String toString ( )   {
	throw new UnsupportedOperationException();
  }
  public String toASCIIString ( )   {
	throw new UnsupportedOperationException();
  }
}
