package java.net ;
import java.io.IOException ;
import java.io.InputStream ;
import java.io.OutputStream ;
import java.util.Hashtable ;
import java.util.StringTokenizer ;
import sun.security.util.SecurityConstants ;
public final class URL implements java.io.Serializable {
    static final long serialVersionUID = -7627629688361524110L;
  //transient InetAddress hostAddress ;
  //transient URLStreamHandler handler ;
  public URL ( String protocol , String host , int port , String file ) throws MalformedURLException   {
	throw new UnsupportedOperationException();
  }
  public URL ( String protocol , String host , String file ) throws MalformedURLException   {
	throw new UnsupportedOperationException();
  }
/*  public URL ( String protocol , String host , int port , String file , URLStreamHandler handler ) throws MalformedURLException   {
	throw new UnsupportedOperationException();
  } */
  public URL ( String spec ) throws MalformedURLException   {
	throw new UnsupportedOperationException();
  }
  public URL ( URL context , String spec ) throws MalformedURLException   {
	throw new UnsupportedOperationException();
  }
/*  public URL ( URL context , String spec , URLStreamHandler handler ) throws MalformedURLException   {
	throw new UnsupportedOperationException();
  }*/
  protected void set ( String protocol , String host , int port , String file , String ref )   {
	throw new UnsupportedOperationException();
  }
  protected void set ( String protocol , String host , int port , String authority , String userInfo , String path , String query , String ref )   {
	throw new UnsupportedOperationException();
  }
  public String getQuery ( )   {
	throw new UnsupportedOperationException();
  }
  public String getPath ( )   {
	throw new UnsupportedOperationException();
  }
  public String getUserInfo ( )   {
	throw new UnsupportedOperationException();
  }
  public String getAuthority ( )   {
	throw new UnsupportedOperationException();
  }
  public int getPort ( )   {
	throw new UnsupportedOperationException();
  }
  public int getDefaultPort ( )   {
	throw new UnsupportedOperationException();
  }
  public String getProtocol ( )   {
	throw new UnsupportedOperationException();
  }
  public String getHost ( )   {
	throw new UnsupportedOperationException();
  }
  public String getFile ( )   {
	throw new UnsupportedOperationException();
  }
  public String getRef ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  public synchronized int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean sameFile ( URL other )   {
	throw new UnsupportedOperationException();
  }
  public String toString ( )   {
	throw new UnsupportedOperationException();
  }
  public String toExternalForm ( )   {
	throw new UnsupportedOperationException();
  }
/*  public URLConnection openConnection ( ) throws java.io.IOException   {
	throw new UnsupportedOperationException();
  }*/
  public final InputStream openStream ( ) throws java.io.IOException   {
	throw new UnsupportedOperationException();
  }
  public final Object getContent ( ) throws java.io.IOException   {
	throw new UnsupportedOperationException();
  }
  public final Object getContent ( Class [ ] classes ) throws java.io.IOException   {
	throw new UnsupportedOperationException();
  }
/*  static URLStreamHandlerFactory factory ;
  public static void setURLStreamHandlerFactory ( URLStreamHandlerFactory fac )   {
	throw new UnsupportedOperationException();
  }
  static Hashtable handlers = new Hashtable ( ) ;
  static URLStreamHandler getURLStreamHandler ( String protocol )   {
	throw new UnsupportedOperationException();
  }*/
}
