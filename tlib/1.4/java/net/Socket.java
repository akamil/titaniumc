package java.net ;
import java.io.InputStream ;
import java.io.OutputStream ;
import java.io.IOException ;
import java.io.InterruptedIOException ;
//import java.nio.channels.SocketChannel ;
import java.security.AccessController ;
import java.security.PrivilegedExceptionAction ;
public class Socket {
  //SocketImpl impl ;
  public Socket ( )   {
	throw new UnsupportedOperationException();
  }
/*  protected Socket ( SocketImpl impl ) throws SocketException   {
	throw new UnsupportedOperationException();
  }*/
  public Socket ( String host , int port ) throws UnknownHostException , IOException   {
	throw new UnsupportedOperationException();
  }
  public Socket ( InetAddress address , int port ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public Socket ( String host , int port , InetAddress localAddr , int localPort ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public Socket ( InetAddress address , int port , InetAddress localAddr , int localPort ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public Socket ( String host , int port , boolean stream ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public Socket ( InetAddress host , int port , boolean stream ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  void createImpl ( boolean stream ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  void setImpl ( )   {
	throw new UnsupportedOperationException();
  }
/*  SocketImpl getImpl ( ) throws SocketException   {
	throw new UnsupportedOperationException();
  }*/
  public void connect ( SocketAddress endpoint ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public void connect ( SocketAddress endpoint , int timeout ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public void bind ( SocketAddress bindpoint ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  final void postAccept ( )   {
	throw new UnsupportedOperationException();
  }
  void setCreated ( )   {
	throw new UnsupportedOperationException();
  }
  void setBound ( )   {
	throw new UnsupportedOperationException();
  }
  void setConnected ( )   {
	throw new UnsupportedOperationException();
  }
  public InetAddress getInetAddress ( )   {
	throw new UnsupportedOperationException();
  }
  public InetAddress getLocalAddress ( )   {
	throw new UnsupportedOperationException();
  }
  public int getPort ( )   {
	throw new UnsupportedOperationException();
  }
  public int getLocalPort ( )   {
	throw new UnsupportedOperationException();
  }
  public SocketAddress getRemoteSocketAddress ( )   {
	throw new UnsupportedOperationException();
  }
  public SocketAddress getLocalSocketAddress ( )   {
	throw new UnsupportedOperationException();
  }
/*  public SocketChannel getChannel ( )   {
	throw new UnsupportedOperationException();
  }*/
  public InputStream getInputStream ( ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public OutputStream getOutputStream ( ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public void setTcpNoDelay ( boolean on ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public boolean getTcpNoDelay ( ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public void setSoLinger ( boolean on , int linger ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public int getSoLinger ( ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public void sendUrgentData ( int data ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public void setOOBInline ( boolean on ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public boolean getOOBInline ( ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public synchronized void setSoTimeout ( int timeout ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public synchronized int getSoTimeout ( ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public synchronized void setSendBufferSize ( int size ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public synchronized int getSendBufferSize ( ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public synchronized void setReceiveBufferSize ( int size ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public synchronized int getReceiveBufferSize ( ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public void setKeepAlive ( boolean on ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public boolean getKeepAlive ( ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public void setTrafficClass ( int tc ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public int getTrafficClass ( ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public void setReuseAddress ( boolean on ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public boolean getReuseAddress ( ) throws SocketException   {
	throw new UnsupportedOperationException();
  }
  public synchronized void close ( ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public void shutdownInput ( ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public void shutdownOutput ( ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public String toString ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isConnected ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isBound ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isClosed ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isInputShutdown ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isOutputShutdown ( )   {
	throw new UnsupportedOperationException();
  }
/*  public static synchronized void setSocketImplFactory ( SocketImplFactory fac ) throws IOException   {
	throw new UnsupportedOperationException();
  }*/
}
