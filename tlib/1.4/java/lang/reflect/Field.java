package java.lang.reflect ;
import sun.reflect.FieldAccessor ;
public final class Field extends AccessibleObject implements Member {
  Field ( Class declaringClass , String name , Class type , int modifiers , int slot )   {
	throw new UnsupportedOperationException();
  }
  Field copy ( )   {
	throw new UnsupportedOperationException();
  }
  public Class getDeclaringClass ( )   {
	throw new UnsupportedOperationException();
  }
  public String getName ( )   {
	throw new UnsupportedOperationException();
  }
  public int getModifiers ( )   {
	throw new UnsupportedOperationException();
  }
  public Class getType ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public String toString ( )   {
	throw new UnsupportedOperationException();
  }
  public Object get ( Object obj ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public boolean getBoolean ( Object obj ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public byte getByte ( Object obj ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public char getChar ( Object obj ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public short getShort ( Object obj ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public int getInt ( Object obj ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public long getLong ( Object obj ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public float getFloat ( Object obj ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public double getDouble ( Object obj ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public void set ( Object obj , Object value ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public void setBoolean ( Object obj , boolean z ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public void setByte ( Object obj , byte b ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public void setChar ( Object obj , char c ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public void setShort ( Object obj , short s ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public void setInt ( Object obj , int i ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public void setLong ( Object obj , long l ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public void setFloat ( Object obj , float f ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  public void setDouble ( Object obj , double d ) throws IllegalArgumentException , IllegalAccessException   {
	throw new UnsupportedOperationException();
  }
  static String getTypeName ( Class type )   {
	throw new UnsupportedOperationException();
  }
}
