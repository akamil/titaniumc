package java.lang.reflect ;
public class InvocationTargetException extends Exception {
  protected InvocationTargetException ( )   {
	throw new UnsupportedOperationException();
  }
  public InvocationTargetException ( Throwable target )   {
	throw new UnsupportedOperationException();
  }
  public InvocationTargetException ( Throwable target , String s )   {
	throw new UnsupportedOperationException();
  }
  public Throwable getTargetException ( )   {
	throw new UnsupportedOperationException();
  }
  public Throwable getCause ( )   {
	throw new UnsupportedOperationException();
  }
}
