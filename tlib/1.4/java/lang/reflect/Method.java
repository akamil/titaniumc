package java.lang.reflect ;
import sun.reflect.MethodAccessor ;

public final class Method extends AccessibleObject implements Member {
  Method ( Class declaringClass , String name , Class [ ] parameterTypes , Class returnType , Class [ ] checkedExceptions , int modifiers , int slot )   {
	throw new UnsupportedOperationException();
  }
  Method copy ( )   {
	throw new UnsupportedOperationException();
  }
  public Class getDeclaringClass ( )   {
	throw new UnsupportedOperationException();
  }
  public String getName ( )   {
	throw new UnsupportedOperationException();
  }
  public int getModifiers ( )   {
	throw new UnsupportedOperationException();
  }
  public Class getReturnType ( )   {
	throw new UnsupportedOperationException();
  }
  public Class [ ] getParameterTypes ( )   {
	throw new UnsupportedOperationException();
  }
  public Class [ ] getExceptionTypes ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public String toString ( )   {
	throw new UnsupportedOperationException();
  }
  public Object invoke ( Object obj , Object [ ] args ) throws IllegalAccessException , IllegalArgumentException , InvocationTargetException   {
	throw new UnsupportedOperationException();
  }
  MethodAccessor getMethodAccessor ( )   {
	throw new UnsupportedOperationException();
  }
  void setMethodAccessor ( MethodAccessor accessor )   {
	throw new UnsupportedOperationException();
  }
  static Class [ ] copy ( Class [ ] in )   {
	throw new UnsupportedOperationException();
  }
}
