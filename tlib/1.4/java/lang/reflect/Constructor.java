package java.lang.reflect ;

import sun.reflect.ConstructorAccessor ;

public final class Constructor extends AccessibleObject implements Member {
  Constructor ( Class declaringClass , Class [ ] parameterTypes , Class [ ] checkedExceptions , int modifiers , int slot )   {
	throw new UnsupportedOperationException();
  }
  Constructor copy ( )   {
	throw new UnsupportedOperationException();
  }
  public Class getDeclaringClass ( )   {
	throw new UnsupportedOperationException();
  }
  public String getName ( )   {
	throw new UnsupportedOperationException();
  }
  public int getModifiers ( )   {
	throw new UnsupportedOperationException();
  }
  public Class [ ] getParameterTypes ( )   {
	throw new UnsupportedOperationException();
  }
  public Class [ ] getExceptionTypes ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public String toString ( )   {
	throw new UnsupportedOperationException();
  }
  public Object newInstance ( Object [ ] initargs ) throws InstantiationException , IllegalAccessException , IllegalArgumentException , InvocationTargetException   {
	throw new UnsupportedOperationException();
  }
  ConstructorAccessor getConstructorAccessor ( )   {
	throw new UnsupportedOperationException();
  }
  void setConstructorAccessor ( ConstructorAccessor accessor )   {
	throw new UnsupportedOperationException();
  }
  int getSlot ( )   {
	throw new UnsupportedOperationException();
  }
}
