package java.lang.reflect ;
public class UndeclaredThrowableException extends RuntimeException {
  static final long serialVersionUID = 330127114055056639L ;
  public UndeclaredThrowableException ( Throwable undeclaredThrowable )   {
	throw new UnsupportedOperationException();
  }
  public UndeclaredThrowableException ( Throwable undeclaredThrowable , String s )   {
	throw new UnsupportedOperationException();
  }
  public Throwable getUndeclaredThrowable ( )   {
	throw new UnsupportedOperationException();
  }
  public Throwable getCause ( )   {
	throw new UnsupportedOperationException();
  }
}
