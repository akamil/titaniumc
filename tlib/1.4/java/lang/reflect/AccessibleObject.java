package java.lang.reflect ;
import java.security.AccessController ;
import sun.reflect.ReflectionFactory ;
public class AccessibleObject {
  public static void setAccessible ( AccessibleObject [ ] array , boolean flag ) throws SecurityException   {
	throw new UnsupportedOperationException();
  }
  public void setAccessible ( boolean flag ) throws SecurityException   {
	throw new UnsupportedOperationException();
  }
  public boolean isAccessible ( )   {
	throw new UnsupportedOperationException();
  }
  protected AccessibleObject ( )   {
	throw new UnsupportedOperationException();
  }
  volatile Class securityCheckCache ;
  boolean override ;
  static final ReflectionFactory reflectionFactory = ( ReflectionFactory ) AccessController.doPrivileged ( new sun.reflect.ReflectionFactory.GetReflectionFactoryAction ( ) ) ;
}
