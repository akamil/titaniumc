package java.lang.reflect ;

public class Modifier {
	
	public static boolean isPublic ( int mod )   {
		throw new UnsupportedOperationException();
	}
	public static boolean isPrivate ( int mod )   {
		throw new UnsupportedOperationException();
	}
	public static boolean isProtected ( int mod )   {
		throw new UnsupportedOperationException();
	}
	public static boolean isStatic ( int mod )   {
		throw new UnsupportedOperationException();
	}
	public static boolean isFinal ( int mod )   {
		throw new UnsupportedOperationException();
	}
	public static boolean isSynchronized ( int mod )   {
		throw new UnsupportedOperationException();
	}
	public static boolean isVolatile ( int mod )   {
		throw new UnsupportedOperationException();
	}
	public static boolean isTransient ( int mod )   {
		throw new UnsupportedOperationException();
	}
	public static boolean isNative ( int mod )   {
		throw new UnsupportedOperationException();
	}
	public static boolean isInterface ( int mod )   {
		throw new UnsupportedOperationException();
	}
	public static boolean isAbstract ( int mod )   {
		throw new UnsupportedOperationException();
	}
	public static boolean isStrict ( int mod )   {
		throw new UnsupportedOperationException();
	}
	public static String toString ( int mod )   {
		throw new UnsupportedOperationException();
	}
	public static final int PUBLIC = 0x00000001 ;
	public static final int PRIVATE = 0x00000002 ;
	public static final int PROTECTED = 0x00000004 ;
	public static final int STATIC = 0x00000008 ;
	public static final int FINAL = 0x00000010 ;
	public static final int SYNCHRONIZED = 0x00000020 ;
	public static final int VOLATILE = 0x00000040 ;
	public static final int TRANSIENT = 0x00000080 ;
	public static final int NATIVE = 0x00000100 ;
	public static final int INTERFACE = 0x00000200 ;
	public static final int ABSTRACT = 0x00000400 ;
	public static final int STRICT = 0x00000800 ;
}
