package java.lang.reflect ;

public class Proxy implements java.io.Serializable {
  private Proxy() { }
  protected InvocationHandler h ;
  protected Proxy ( InvocationHandler h )   {
	throw new UnsupportedOperationException();
  }
  public static Class getProxyClass ( ClassLoader loader , Class [ ] interfaces ) throws IllegalArgumentException   {
	throw new UnsupportedOperationException();
  }
  public static Object newProxyInstance ( ClassLoader loader , Class [ ] interfaces , InvocationHandler h ) throws IllegalArgumentException   {
	throw new UnsupportedOperationException();
  }
  public static boolean isProxyClass ( Class cl )   {
	throw new UnsupportedOperationException();
  }
  public static InvocationHandler getInvocationHandler ( Object proxy ) throws IllegalArgumentException   {
	throw new UnsupportedOperationException();
  }
}
