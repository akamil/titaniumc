package java.lang.reflect ;
import sun.reflect.MethodAccessor ;
import sun.reflect.ConstructorAccessor ;
class ReflectAccess implements sun.reflect.LangReflectAccess {
  public Field newField ( Class declaringClass , String name , Class type , int modifiers , int slot )   {
	throw new UnsupportedOperationException();
  }
  public Method newMethod ( Class declaringClass , String name , Class [ ] parameterTypes , Class returnType , Class [ ] checkedExceptions , int modifiers , int slot )   {
	throw new UnsupportedOperationException();
  }
  public Constructor newConstructor ( Class declaringClass , Class [ ] parameterTypes , Class [ ] checkedExceptions , int modifiers , int slot )   {
	throw new UnsupportedOperationException();
  }
  public MethodAccessor getMethodAccessor ( Method m )   {
	throw new UnsupportedOperationException();
  }
  public void setMethodAccessor ( Method m , MethodAccessor accessor )   {
	throw new UnsupportedOperationException();
  }
  public ConstructorAccessor getConstructorAccessor ( Constructor c )   {
	throw new UnsupportedOperationException();
  }
  public void setConstructorAccessor ( Constructor c , ConstructorAccessor accessor )   {
	throw new UnsupportedOperationException();
  }
  public int getConstructorSlot ( Constructor c )   {
	throw new UnsupportedOperationException();
  }
  public Method copyMethod ( Method arg )   {
	throw new UnsupportedOperationException();
  }
  public Field copyField ( Field arg )   {
	throw new UnsupportedOperationException();
  }
  public Constructor copyConstructor ( Constructor arg )   {
	throw new UnsupportedOperationException();
  }
}
