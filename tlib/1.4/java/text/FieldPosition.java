package java.text ;
public class FieldPosition {
  int field = 0 ;
  int endIndex = 0 ;
  int beginIndex = 0 ;
  public FieldPosition ( int field )   {
	throw new UnsupportedOperationException();
  }
  public FieldPosition ( Format.Field attribute )   {
	throw new UnsupportedOperationException();
  }
  public FieldPosition ( Format.Field attribute , int fieldID )   {
	throw new UnsupportedOperationException();
  }
  public Format.Field getFieldAttribute ( )   {
	throw new UnsupportedOperationException();
  }
  public int getField ( )   {
	throw new UnsupportedOperationException();
  }
  public int getBeginIndex ( )   {
	throw new UnsupportedOperationException();
  }
  public int getEndIndex ( )   {
	throw new UnsupportedOperationException();
  }
  public void setBeginIndex ( int bi )   {
	throw new UnsupportedOperationException();
  }
  public void setEndIndex ( int ei )   {
	throw new UnsupportedOperationException();
  }
  Format.FieldDelegate getFieldDelegate ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public String toString ( )   {
	throw new UnsupportedOperationException();
  }
}
