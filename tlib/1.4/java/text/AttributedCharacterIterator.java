package java.text ;
import java.io.InvalidObjectException ;
import java.io.Serializable ;
import java.util.HashMap ;
import java.util.Map ;
import java.util.Set ;
public interface AttributedCharacterIterator {
  public static class Attribute implements Serializable   {

  }
  public int getRunStart ( ) ;
  public int getRunStart ( Attribute attribute ) ;
  public int getRunStart ( Set attributes ) ;
  public int getRunLimit ( ) ;
  public int getRunLimit ( Attribute attribute ) ;
  public int getRunLimit ( Set attributes ) ;
  public Map getAttributes ( ) ;
  public Object getAttribute ( Attribute attribute ) ;
  public Set getAllAttributeKeys ( ) ;
}
