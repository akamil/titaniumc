package java.text ;
import java.io.InvalidObjectException ;
import java.io.IOException ;
import java.io.ObjectInputStream ;
//import java.text.DecimalFormat ;
import java.util.ArrayList ;
import java.util.Date ;
import java.util.List ;
import java.util.Locale ;
//import sun.text.Utility ;
public class MessageFormat extends Format {
  public MessageFormat ( String pattern )   {
	throw new UnsupportedOperationException();
  }
  public MessageFormat ( String pattern , Locale locale )   {
	throw new UnsupportedOperationException();
  }
  public void setLocale ( Locale locale )   {
	throw new UnsupportedOperationException();
  }
  public Locale getLocale ( )   {
	throw new UnsupportedOperationException();
  }
  public void applyPattern ( String pattern )   {
	throw new UnsupportedOperationException();
  }
  public String toPattern ( )   {
	throw new UnsupportedOperationException();
  }
  public void setFormatsByArgumentIndex ( Format [ ] newFormats )   {
	throw new UnsupportedOperationException();
  }
  public void setFormats ( Format [ ] newFormats )   {
	throw new UnsupportedOperationException();
  }
  public void setFormatByArgumentIndex ( int argumentIndex , Format newFormat )   {
	throw new UnsupportedOperationException();
  }
  public void setFormat ( int formatElementIndex , Format newFormat )   {
	throw new UnsupportedOperationException();
  }
  public Format [ ] getFormatsByArgumentIndex ( )   {
	throw new UnsupportedOperationException();
  }
  public Format [ ] getFormats ( )   {
	throw new UnsupportedOperationException();
  }
  public final StringBuffer format ( Object [ ] arguments , StringBuffer result , FieldPosition pos )   {
	throw new UnsupportedOperationException();
  }
  public static String format ( String pattern , Object [ ] arguments )   {
	throw new UnsupportedOperationException();
  }
  public final StringBuffer format ( Object arguments , StringBuffer result , FieldPosition pos )   {
	throw new UnsupportedOperationException();
  }
  public AttributedCharacterIterator formatToCharacterIterator ( Object arguments )   {
	throw new UnsupportedOperationException();
  }
  public Object [ ] parse ( String source , ParsePosition pos )   {
	throw new UnsupportedOperationException();
  }
  public Object [ ] parse ( String source ) throws ParseException   {
	throw new UnsupportedOperationException();
  }
  public Object parseObject ( String source , ParsePosition pos )   {
	throw new UnsupportedOperationException();
  }
  public Object clone ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public static class Field extends Format.Field   {
  }
}
