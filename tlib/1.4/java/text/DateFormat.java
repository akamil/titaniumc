package java.text ;
import java.io.InvalidObjectException ;
import java.util.HashMap ;
import java.util.Locale ;
import java.util.Map ;
import java.util.ResourceBundle ;
import java.util.MissingResourceException ;
import java.util.TimeZone ;
import java.util.Calendar ;
import java.util.GregorianCalendar ;
import java.util.Date ;
import sun.text.resources.LocaleData ;
public abstract class DateFormat extends Format {
  protected Calendar calendar ;
  protected NumberFormat numberFormat ;
  public final static int ERA_FIELD = 0 ;
  public final static int YEAR_FIELD = 1 ;
  public final static int MONTH_FIELD = 2 ;
  public final static int DATE_FIELD = 3 ;
  public final static int HOUR_OF_DAY1_FIELD = 4 ;
  public final static int HOUR_OF_DAY0_FIELD = 5 ;
  public final static int MINUTE_FIELD = 6;
  public final static int SECOND_FIELD = 7 ;
  public final static int MILLISECOND_FIELD = 8 ;
  public final static int DAY_OF_WEEK_FIELD = 9 ;
  public final static int DAY_OF_YEAR_FIELD = 10 ;
  public final static int DAY_OF_WEEK_IN_MONTH_FIELD = 11 ;
  public final static int WEEK_OF_YEAR_FIELD = 12 ;
  public final static int WEEK_OF_MONTH_FIELD = 13 ;
  public final static int AM_PM_FIELD = 14 ;
  public final static int HOUR1_FIELD = 15 ;
  public final static int HOUR0_FIELD = 16 ;
  public final static int TIMEZONE_FIELD = 17 ;
  public final StringBuffer format ( Object obj , StringBuffer toAppendTo , FieldPosition fieldPosition )   {
	throw new UnsupportedOperationException();
  }
  public abstract StringBuffer format ( Date date , StringBuffer toAppendTo , FieldPosition fieldPosition ) ;
  public final String format ( Date date )   {
	throw new UnsupportedOperationException();
  }
  public Date parse ( String source ) throws ParseException   {
	throw new UnsupportedOperationException();
  }
  public abstract Date parse ( String source , ParsePosition pos ) ;
  public Object parseObject ( String source , ParsePosition pos )   {
	throw new UnsupportedOperationException();
  }
  public static final int FULL = 0 ;
  public static final int LONG = 1 ;
  public static final int MEDIUM = 2 ;
  public static final int SHORT = 3 ;
  public static final int DEFAULT = MEDIUM ;
  public final static DateFormat getTimeInstance ( )   {
	throw new UnsupportedOperationException();
  }
  public final static DateFormat getTimeInstance ( int style )   {
	throw new UnsupportedOperationException();
  }
  public final static DateFormat getTimeInstance ( int style , Locale aLocale )   {
	throw new UnsupportedOperationException();
  }
  public final static DateFormat getDateInstance ( )   {
	throw new UnsupportedOperationException();
  }
  public final static DateFormat getDateInstance ( int style )   {
	throw new UnsupportedOperationException();
  }
  public final static DateFormat getDateInstance ( int style , Locale aLocale )   {
	throw new UnsupportedOperationException();
  }
  public final static DateFormat getDateTimeInstance ( )   {
	throw new UnsupportedOperationException();
  }
  public final static DateFormat getDateTimeInstance ( int dateStyle , int timeStyle )   {
	throw new UnsupportedOperationException();
  }
  public final static DateFormat getDateTimeInstance ( int dateStyle , int timeStyle , Locale aLocale )   {
	throw new UnsupportedOperationException();
  }
  public final static DateFormat getInstance ( )   {
	throw new UnsupportedOperationException();
  }
  public static Locale [ ] getAvailableLocales ( )   {
	throw new UnsupportedOperationException();
  }
  public void setCalendar ( Calendar newCalendar )   {
	throw new UnsupportedOperationException();
  }
  public Calendar getCalendar ( )   {
	throw new UnsupportedOperationException();
  }
  public void setNumberFormat ( NumberFormat newNumberFormat )   {
	throw new UnsupportedOperationException();
  }
  public NumberFormat getNumberFormat ( )   {
	throw new UnsupportedOperationException();
  }
  public void setTimeZone ( TimeZone zone )   {
	throw new UnsupportedOperationException();
  }
  public TimeZone getTimeZone ( )   {
	throw new UnsupportedOperationException();
  }
  public void setLenient ( boolean lenient )   {
	throw new UnsupportedOperationException();
  }
  public boolean isLenient ( )   {
	throw new UnsupportedOperationException();
  }
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  public Object clone ( )   {
	throw new UnsupportedOperationException();
  }
  protected DateFormat ( )   {
	throw new UnsupportedOperationException();
  }

}
