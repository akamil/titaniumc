package java.text ;
import java.util.TimeZone ;
import java.util.Calendar ;
import java.util.Date ;
import java.util.Locale ;
import java.util.ResourceBundle ;
import java.util.SimpleTimeZone ;
import java.util.GregorianCalendar ;
import java.io.ObjectInputStream ;
import java.io.InvalidObjectException ;
import java.io.IOException ;
import java.lang.ClassNotFoundException ;
import java.util.Hashtable ;
import java.lang.StringIndexOutOfBoundsException ;
import sun.text.resources.LocaleData ;
public class SimpleDateFormat extends DateFormat {
  static final long serialVersionUID = 4774881970558875024L;
  static final int currentSerialVersion = 1 ;
  public SimpleDateFormat ( )   {
	throw new UnsupportedOperationException();
  }
  public SimpleDateFormat ( String pattern )   {
	throw new UnsupportedOperationException();
  }
  public SimpleDateFormat ( String pattern , Locale locale )   {
	throw new UnsupportedOperationException();
  }
  public SimpleDateFormat ( String pattern , DateFormatSymbols formatSymbols )   {
	throw new UnsupportedOperationException();
  }
  SimpleDateFormat ( int timeStyle , int dateStyle , Locale loc )   {
	throw new UnsupportedOperationException();
  }
  public void set2DigitYearStart ( Date startDate )   {
	throw new UnsupportedOperationException();
  }
  public Date get2DigitYearStart ( )   {
	throw new UnsupportedOperationException();
  }
  public StringBuffer format ( Date date , StringBuffer toAppendTo , FieldPosition pos )   {
	throw new UnsupportedOperationException();
  }
  public AttributedCharacterIterator formatToCharacterIterator ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  ;
  ;
  ;
  public Date parse ( String text , ParsePosition pos )   {
	throw new UnsupportedOperationException();
  }
  public String toPattern ( )   {
	throw new UnsupportedOperationException();
  }
  public String toLocalizedPattern ( )   {
	throw new UnsupportedOperationException();
  }
  public void applyPattern ( String pattern )   {
	throw new UnsupportedOperationException();
  }
  public void applyLocalizedPattern ( String pattern )   {
	throw new UnsupportedOperationException();
  }
  public DateFormatSymbols getDateFormatSymbols ( )   {
	throw new UnsupportedOperationException();
  }
  public void setDateFormatSymbols ( DateFormatSymbols newFormatSymbols )   {
	throw new UnsupportedOperationException();
  }
  public Object clone ( )   {
	throw new UnsupportedOperationException();
  }
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object obj )   {
	throw new UnsupportedOperationException();
  }
}
