package java.text ;
import java.io.Serializable ;
public abstract class Format implements Serializable , Cloneable {
  public final String format ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  public abstract StringBuffer format ( Object obj , StringBuffer toAppendTo , FieldPosition pos ) ;
  public AttributedCharacterIterator formatToCharacterIterator ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  public abstract Object parseObject ( String source , ParsePosition pos ) ;
  public Object parseObject ( String source ) throws ParseException   {
	throw new UnsupportedOperationException();
  }
  public Object clone ( )   {
	throw new UnsupportedOperationException();
  }
  AttributedCharacterIterator createAttributedCharacterIterator ( String s )   {
	throw new UnsupportedOperationException();
  }
  AttributedCharacterIterator createAttributedCharacterIterator ( AttributedCharacterIterator [ ] iterators )   {
	throw new UnsupportedOperationException();
  }
  AttributedCharacterIterator createAttributedCharacterIterator ( String string , AttributedCharacterIterator.Attribute key , Object value )   {
	throw new UnsupportedOperationException();
  }
  AttributedCharacterIterator createAttributedCharacterIterator ( AttributedCharacterIterator iterator , AttributedCharacterIterator.Attribute key , Object value )   {
	throw new UnsupportedOperationException();
  }
  public static class Field extends AttributedCharacterIterator.Attribute   {
  }
  interface FieldDelegate   {
  }
}
