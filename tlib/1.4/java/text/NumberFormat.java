package java.text ;
import java.io.InvalidObjectException ;
import java.io.IOException ;
import java.io.ObjectInputStream ;
import java.io.ObjectOutputStream ;
import java.math.BigInteger ;
import java.util.Currency ;
import java.util.HashMap ;
import java.util.Hashtable ;
import java.util.Locale ;
import java.util.Map ;
import java.util.ResourceBundle ;
import sun.text.resources.LocaleData ;
public abstract class NumberFormat extends Format {
  public static final int INTEGER_FIELD = 0 ;
  public static final int FRACTION_FIELD = 1 ;
  public final StringBuffer format ( Object number , StringBuffer toAppendTo , FieldPosition pos )   {
	throw new UnsupportedOperationException();
  }
  public final Object parseObject ( String source , ParsePosition pos )   {
	throw new UnsupportedOperationException();
  }
  public final String format ( double number )   {
	throw new UnsupportedOperationException();
  }
  public final String format ( long number )   {
	throw new UnsupportedOperationException();
  }
  public abstract StringBuffer format ( double number , StringBuffer toAppendTo , FieldPosition pos ) ;
  public abstract StringBuffer format ( long number , StringBuffer toAppendTo , FieldPosition pos ) ;
  public abstract Number parse ( String source , ParsePosition parsePosition ) ;
  public Number parse ( String source ) throws ParseException   {
	throw new UnsupportedOperationException();
  }
  public boolean isParseIntegerOnly ( )   {
	throw new UnsupportedOperationException();
  }
  public void setParseIntegerOnly ( boolean value )   {
	throw new UnsupportedOperationException();
  }
  public final static NumberFormat getInstance ( )   {
	throw new UnsupportedOperationException();
  }
  public static NumberFormat getInstance ( Locale inLocale )   {
	throw new UnsupportedOperationException();
  }
  public final static NumberFormat getNumberInstance ( )   {
	throw new UnsupportedOperationException();
  }
  public static NumberFormat getNumberInstance ( Locale inLocale )   {
	throw new UnsupportedOperationException();
  }
  public final static NumberFormat getIntegerInstance ( )   {
	throw new UnsupportedOperationException();
  }
  public static NumberFormat getIntegerInstance ( Locale inLocale )   {
	throw new UnsupportedOperationException();
  }
  public final static NumberFormat getCurrencyInstance ( )   {
	throw new UnsupportedOperationException();
  }
  public static NumberFormat getCurrencyInstance ( Locale inLocale )   {
	throw new UnsupportedOperationException();
  }
  public final static NumberFormat getPercentInstance ( )   {
	throw new UnsupportedOperationException();
  }
  public static NumberFormat getPercentInstance ( Locale inLocale )   {
	throw new UnsupportedOperationException();
  }
  final static NumberFormat getScientificInstance ( )   {
	throw new UnsupportedOperationException();
  }
  static NumberFormat getScientificInstance ( Locale inLocale )   {
	throw new UnsupportedOperationException();
  }
  public static Locale [ ] getAvailableLocales ( )   {
	throw new UnsupportedOperationException();
  }
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  public Object clone ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean isGroupingUsed ( )   {
	throw new UnsupportedOperationException();
  }
  public void setGroupingUsed ( boolean newValue )   {
	throw new UnsupportedOperationException();
  }
  public int getMaximumIntegerDigits ( )   {
	throw new UnsupportedOperationException();
  }
  public void setMaximumIntegerDigits ( int newValue )   {
	throw new UnsupportedOperationException();
  }
  public int getMinimumIntegerDigits ( )   {
	throw new UnsupportedOperationException();
  }
  public void setMinimumIntegerDigits ( int newValue )   {
	throw new UnsupportedOperationException();
  }
  public int getMaximumFractionDigits ( )   {
	throw new UnsupportedOperationException();
  }
  public void setMaximumFractionDigits ( int newValue )   {
	throw new UnsupportedOperationException();
  }
  public int getMinimumFractionDigits ( )   {
	throw new UnsupportedOperationException();
  }
  public void setMinimumFractionDigits ( int newValue )   {
	throw new UnsupportedOperationException();
  }
  public Currency getCurrency ( )   {
	throw new UnsupportedOperationException();
  }
  public void setCurrency ( Currency currency )   {
	throw new UnsupportedOperationException();
  }
  static final int currentSerialVersion = 1 ;
  static final long serialVersionUID = -2308460125733713944L;
}
