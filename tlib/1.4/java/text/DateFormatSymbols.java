package java.text ;
import java.util.Locale ;
import java.util.ResourceBundle ;
import java.io.Serializable ;
import java.lang.ref.SoftReference ;
import java.util.Vector ;
import java.util.Enumeration ;
//import sun.text.Utility ;
import sun.text.resources.LocaleData ;
import java.util.Hashtable ;
public class DateFormatSymbols implements Serializable , Cloneable {
  public DateFormatSymbols ( )   {
	throw new UnsupportedOperationException();
  }
  public DateFormatSymbols ( Locale locale )   {
	throw new UnsupportedOperationException();
  }
  String eras [ ] = null ;
  String months [ ] = null ;
  String shortMonths [ ] = null ;
  String weekdays [ ] = null ;
  String shortWeekdays [ ] = null ;
  String ampms [ ] = null ;
  String zoneStrings [ ] [ ] = null ;
  static final String  patternChars = "GyMdkHmsSEDFwWahKzZ";
  String localPatternChars = null ;
  static final long serialVersionUID = -5987973545549424702L;
  public String [ ] getEras ( )   {
	throw new UnsupportedOperationException();
  }
  public void setEras ( String [ ] newEras )   {
	throw new UnsupportedOperationException();
  }
  public String [ ] getMonths ( )   {
	throw new UnsupportedOperationException();
  }
  public void setMonths ( String [ ] newMonths )   {
	throw new UnsupportedOperationException();
  }
  public String [ ] getShortMonths ( )   {
	throw new UnsupportedOperationException();
  }
  public void setShortMonths ( String [ ] newShortMonths )   {
	throw new UnsupportedOperationException();
  }
  public String [ ] getWeekdays ( )   {
	throw new UnsupportedOperationException();
  }
  public void setWeekdays ( String [ ] newWeekdays )   {
	throw new UnsupportedOperationException();
  }
  public String [ ] getShortWeekdays ( )   {
	throw new UnsupportedOperationException();
  }
  public void setShortWeekdays ( String [ ] newShortWeekdays )   {
	throw new UnsupportedOperationException();
  }
  public String [ ] getAmPmStrings ( )   {
	throw new UnsupportedOperationException();
  }
  public void setAmPmStrings ( String [ ] newAmpms )   {
	throw new UnsupportedOperationException();
  }
  public String [ ] [ ] getZoneStrings ( )   {
	throw new UnsupportedOperationException();
  }
  public void setZoneStrings ( String [ ] [ ] newZoneStrings )   {
	throw new UnsupportedOperationException();
  }
  public String getLocalPatternChars ( )   {
	throw new UnsupportedOperationException();
  }
  public void setLocalPatternChars ( String newLocalPatternChars )   {
	throw new UnsupportedOperationException();
  }
  public Object clone ( )   {
	throw new UnsupportedOperationException();
  }
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  static final int millisPerHour = 60 * 60 * 1000 ;
  final int getZoneIndex ( String ID )   {
	throw new UnsupportedOperationException();
  }
}
