/*
 * @(#)InputStreamReader.java	1.43 03/01/23
 *
 * Copyright 2003 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package java.io;

import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;


/**
 * An InputStreamReader is a bridge from byte streams to character streams: It
 * reads bytes and decodes them into characters using a specified {@link
 * java.nio.charset.Charset <code>charset</code>}.  The charset that it uses
 * may be specified by name or may be given explicitly, or the platform's
 * default charset may be accepted.
 *
 * <p> Each invocation of one of an InputStreamReader's read() methods may
 * cause one or more bytes to be read from the underlying byte-input stream.
 * To enable the efficient conversion of bytes to characters, more bytes may
 * be read ahead from the underlying stream than are necessary to satisfy the
 * current read operation.
 *
 * <p> For top efficiency, consider wrapping an InputStreamReader within a
 * BufferedReader.  For example:
 *
 * <pre>
 * BufferedReader in
 *   = new BufferedReader(new InputStreamReader(System.in));
 * </pre>
 *
 * @see BufferedReader
 * @see InputStream
 * @see java.nio.charset.Charset
 *
 * @version     1.43, 03/01/23
 * @author      Mark Reinhold
 * @since       JDK1.1
 */

public class InputStreamReader extends Reader {

    /* MODIFICATION: by nam_mai on 07/14/05
     * Added private variables for StreamDecoder workaround.
     * Methods below modified to work without StreamDecoder and
     * are buffered for io efficiency.
     */
    private InputStream in = null;
    private volatile boolean isOpen = true;
    private int DEFAULT_BUFFER_SIZE = 8192;
    private byte bb[] = new byte[DEFAULT_BUFFER_SIZE];
    private char cb[] = new char[DEFAULT_BUFFER_SIZE];
    private int start = -1; /* if start == end == -1 and empty==true => EOF */
    private int end = 0;
    private boolean empty = true; /* if empty==true, start and end hold no info */
	
    /**
     * Create an InputStreamReader that uses the default charset.
     *
     * @param  in   An InputStream
     */
    public InputStreamReader(InputStream in) {
	super(in);
	this.in = in;
    }

    /**
     * Create an InputStreamReader that uses the named charset.
     *
     * @param  in
     *         An InputStream
     *
     * @param  charsetName
     *         The name of a supported
     *         {@link java.nio.charset.Charset </code>charset<code>}
     *
     * @exception  UnsupportedEncodingException
     *             If the named charset is not supported
     */
    public InputStreamReader(InputStream in, String charsetName)
        throws UnsupportedEncodingException
    {
	super(in);
	this.in = in;
    }

    /**
     * Create an InputStreamReader that uses the given charset. </p>
     *
     * @param  in       An InputStream
     * @param  cs       A charset
     *
     * @since 1.4
     * @spec JSR-51
     */
    public InputStreamReader(InputStream in, Charset cs) {
        super(in);
        this.in = in;
    }

    /**
     * Create an InputStreamReader that uses the given charset decoder.  </p>
     *
     * @param  in       An InputStream
     * @param  dec      A charset decoder
     *
     * @since 1.4
     * @spec JSR-51
     */
    public InputStreamReader(InputStream in, CharsetDecoder dec) {
        super(in);
        this.in = in;
    }

    /**
     * Return the name of the character encoding being used by this stream.
     *
     * <p> If the encoding has an historical name then that name is returned;
     * otherwise the encoding's canonical name is returned.
     *
     * <p> If this instance was created with the {@link
     * #InputStreamReader(InputStream, String)} constructor then the returned
     * name, being unique for the encoding, may differ from the name passed to
     * the constructor.  This method may return <code>null</code> if the stream
     * has been closed. </p>
     *
     * @return The historical name of this encoding, or possibly
     *         <code>null</code> if the stream has been closed
     *
     * @see java.nio.charset.Charset
     *
     * @revised 1.4
     * @spec JSR-51
     */
    public String getEncoding() {
    	throw new UnsupportedOperationException();
    }

    /**
     * Read a single character.
     *
     * @return The character read, or -1 if the end of the stream has been
     *         reached
     *
     * @exception  IOException  If an I/O error occurs
     */
    public int read() throws IOException {
        synchronized(lock) {
	    if(!isOpen)
		throw new IOException("Stream Closed");
	    if(empty) { /* Buffer empty */
		fill();
		if(empty)
		    return -1;
	    }
	    empty = ((start+1) == end);
	    return bb[start++] & 0xff;
	}
    }

    /**
     * Read characters into a portion of an array.
     *
     * @param      cbuf     Destination buffer
     * @param      offset   Offset at which to start storing characters
     * @param      length   Maximum number of characters to read
     *
     * @return     The number of characters read, or -1 if the end of the 
     *             stream has been reached
     *
     * @exception  IOException  If an I/O error occurs
     */
    public int read(char cbuf[], int offset, int length) throws IOException {
    	synchronized(lock) {
	    if (cbuf == null) {
		throw new NullPointerException();
	    } else if ((offset < 0) || (offset > cbuf.length) || (length < 0) ||
		       ((offset + length) > cbuf.length) || ((offset + length) < 0)) {
		throw new IndexOutOfBoundsException();
	    } else if (length == 0) {
		return 0;
	    }

	    if(!isOpen)
		throw new IOException("Stream Closed");

	    if(empty && start == -1 && end == -1) {
		return -1;
	    }
	    
	    int validbuf;
	    if(empty)
		validbuf = 0;
	    else
		validbuf = ((start == end) ? DEFAULT_BUFFER_SIZE :
			    end - start);
	    if(length <= validbuf) {
		/* read is entirely contained in buffer */
		syncBuffers(start, start+length);
      		System.arraycopy(cb, start, cbuf, offset, length);
		start += length;
		empty = (start == end);
		return length;
	    }

	    if(length >= DEFAULT_BUFFER_SIZE + validbuf) {
		/* read is larger than buffering */
		if(!empty) {
		    syncBuffers(start, start + validbuf);
		    System.arraycopy(cb, start, cbuf, offset, validbuf);
		}
		byte[] bbuf = new byte[length-validbuf];
		int newoffset = offset + validbuf;
		int numread = in.read(bbuf, 0, length-validbuf);
		for(int i = 0; i < numread; i++) {
		    cbuf[i+newoffset] = (char) bbuf[i];
		}
		start = -1;
		end = 0;
		empty = true;
		return validbuf + numread;
	    }

	    /* read rolls over to next buffering */
	    if(!empty) {
		syncBuffers(start, end);
		System.arraycopy(cb, start, cbuf, offset, validbuf);
	    }
	    fill(); /* fill() resets the variables */
	    if(empty)
		return validbuf;
	    int avail = (length-validbuf < end-start ? length-validbuf :
			 end-start);
	    syncBuffers(start, start + avail);
	    System.arraycopy(cb, start, cbuf, offset+validbuf, avail);
	    start += avail;
	    empty = (start == end);
	    return validbuf + avail;
    	}
	
    }

    /**
     * Tell whether this stream is ready to be read.  An InputStreamReader is
     * ready if its input buffer is not empty, or if bytes are available to be
     * read from the underlying byte stream.
     *
     * @exception  IOException  If an I/O error occurs
     */
    public boolean ready() throws IOException {
	synchronized(lock) {
	    if(!isOpen)
		throw new IOException("Stream closed");
	    return !empty || in.available() > 0;
	}
    }

    /**
     * Close the stream.
     *
     * @exception  IOException  If an I/O error occurs
     */
    public void close() throws IOException {
    	synchronized(lock) {
	    if(!isOpen)
		return;
	    in.close();
	    isOpen = false;
	    start = -1;
	    end = 0;
	    empty = true;
    	}
    }

    /* MODIFICATION: by nam_mai on 07/14/05
     * Added auxilary methods for buffering.
     */
    private void syncBuffers(int start, int end) {
	for(int i = start; i < end; i++)
	    cb[i] = (char) bb[i];
    }
    
    /* fill() assumes that buffer has been emptied or can be thrown away */
    private void fill() {
	try {
	    int avail = in.available();
	    int toRead = ((avail > DEFAULT_BUFFER_SIZE || avail == 0) ? 
			  DEFAULT_BUFFER_SIZE : avail);
	    int numRead = in.read(bb, 0, toRead);
	    if(numRead == -1) { /* EOF */
		start = -1;
		end = -1;
		empty = true;
	    } else if(numRead == 0) { /* read nothing */
		empty = true;
	    } else { /* read something */
		start = 0;
		end = numRead;
		empty = false;
	    }
	    return;
	} catch(IOException e) {
	    start = -1;
	    end = -1;
	    empty = true;
	    return;
	}
    }
	
}
