package java.security;
import java.util.*;
import java.lang.*;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
public abstract class MessageDigest {
    public static MessageDigest getInstance(String algorithm) 
    throws NoSuchAlgorithmException { 
    	throw new UnsupportedOperationException();    
    }
    public byte[] digest(byte[] input) {
    	throw new UnsupportedOperationException();
    }
}
