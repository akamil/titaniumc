package java.security ;

public final class ProtectionDomain {
  public ProtectionDomain ( CodeSource codesource , PermissionCollection permissions )   {
	throw new UnsupportedOperationException();
  }
  public ProtectionDomain ( CodeSource codesource , PermissionCollection permissions , ClassLoader classloader , Principal [ ] principals )   {
	throw new UnsupportedOperationException();
  }
  public final CodeSource getCodeSource ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean implies(Permission permission) {
    return true;
  }

/*  public final ClassLoader getClassLoader ( )   {
	throw new UnsupportedOperationException();
  }
  public final Principal [ ] getPrincipals ( )   {
	throw new UnsupportedOperationException();
  }
  public final PermissionCollection getPermissions ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean implies ( Permission permission )   {
	throw new UnsupportedOperationException();
  }
  public String toString ( )   {
	throw new UnsupportedOperationException();
  }*/
}
