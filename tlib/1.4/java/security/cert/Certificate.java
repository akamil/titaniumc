package java.security.cert ;
import java.util.Arrays ;

public abstract class Certificate implements java.io.Serializable {

  protected Object writeReplace ( ) throws java.io.ObjectStreamException   {
	throw new UnsupportedOperationException();
  }
}
