package java.security ;
import java.util.Enumeration ;
import java.util.Hashtable ;
import java.util.NoSuchElementException ;
import java.util.Map ;
import java.util.HashMap ;
import java.util.List ;
import java.util.ArrayList ;
import java.util.Iterator ;
import java.util.Collections ;
import java.io.Serializable ;
import java.io.ObjectStreamField ;
import java.io.ObjectOutputStream ;
import java.io.ObjectInputStream ;
import java.io.IOException ;
public final class Permissions extends PermissionCollection implements Serializable {
  public Permissions ( )   {
	throw new UnsupportedOperationException();
  }
  public void add ( Permission permission )   {
	throw new UnsupportedOperationException();
  }
  public boolean implies ( Permission permission )   {
	throw new UnsupportedOperationException();
  }
  public Enumeration elements ( )   {
	throw new UnsupportedOperationException();
  }
}
