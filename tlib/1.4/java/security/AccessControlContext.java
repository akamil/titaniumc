package java.security ;
import java.util.Vector ;
import sun.security.util.Debug ;
import sun.security.util.SecurityConstants ;
public final class AccessControlContext {
  static Debug getDebug ( )   {
	throw new UnsupportedOperationException();
  }
  public AccessControlContext ( ProtectionDomain context [ ] )   {
	throw new UnsupportedOperationException();
  }
  public AccessControlContext ( AccessControlContext acc , DomainCombiner combiner )   {
	throw new UnsupportedOperationException();
  }
  AccessControlContext ( ProtectionDomain context [ ] , boolean isPrivileged )   {
	throw new UnsupportedOperationException();
  }
  boolean isPrivileged ( )   {
	throw new UnsupportedOperationException();
  }
  public DomainCombiner getDomainCombiner ( )   {
	throw new UnsupportedOperationException();
  }
  public void checkPermission ( Permission perm ) throws AccessControlException   {
	throw new UnsupportedOperationException();
  }
  AccessControlContext optimize ( )   {
	throw new UnsupportedOperationException();
  }
  public boolean equals ( Object obj )   {
	throw new UnsupportedOperationException();
  }
  public int hashCode ( )   {
	throw new UnsupportedOperationException();
  }
}
