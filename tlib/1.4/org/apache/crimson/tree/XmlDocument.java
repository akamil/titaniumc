package org.apache.crimson.tree ;
import java.io.InputStream ;
import java.io.OutputStreamWriter ;
import java.io.OutputStream ;
import java.io.Writer ;
import java.io.IOException ;
import java.util.Dictionary ;
import java.util.Enumeration ;
import java.util.Hashtable ;
import java.util.Locale ;
import org.w3c.dom. * ;
import org.xml.sax.InputSource ;
import org.xml.sax.SAXException ;
import org.xml.sax.SAXParseException ;

public class XmlDocument implements Document {
  static String eol ;

  //static final MessageCatalog catalog = new Catalog ( ) ;
  int mutationCount ;
  boolean replaceRootElement ;
  public XmlDocument ( )   {
	throw new UnsupportedOperationException();
  }
  public static XmlDocument createXmlDocument ( String documentURI , boolean doValidate ) throws IOException , SAXException   {
	throw new UnsupportedOperationException();
  }
  public static XmlDocument createXmlDocument ( String documentURI ) throws IOException , SAXException   {
	throw new UnsupportedOperationException();
  }
  public static XmlDocument createXmlDocument ( InputStream in , boolean doValidate ) throws IOException , SAXException   {
	throw new UnsupportedOperationException();
  }
  public static XmlDocument createXmlDocument ( InputSource in , boolean doValidate ) throws IOException , SAXException   {
	throw new UnsupportedOperationException();
  }
  public Locale getLocale ( )   {
	throw new UnsupportedOperationException();
  }
  public void setLocale ( Locale locale )   {
	throw new UnsupportedOperationException();
  }
  public Locale chooseLocale ( String languages [ ] )   {
	throw new UnsupportedOperationException();
  }
  public void write ( OutputStream out ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public void write ( Writer out ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  static String java2std ( String encodingName )   {
	throw new UnsupportedOperationException();
  }
  public void write ( Writer out , String encoding ) throws IOException   {
	throw new UnsupportedOperationException();
  }
/*  public XmlWriteContext createWriteContext ( Writer out )   {
	throw new UnsupportedOperationException();
  }
  public XmlWriteContext createWriteContext ( Writer out , int level )   {
	throw new UnsupportedOperationException();
  }
  public void writeXml ( XmlWriteContext context ) throws IOException   {
	throw new UnsupportedOperationException();
  }
  public void writeChildrenXml ( XmlWriteContext context ) throws IOException   {
	throw new UnsupportedOperationException();
  }*/
  void checkChildType ( int type ) throws DOMException   {
	throw new UnsupportedOperationException();
  }
  final public void setSystemId ( String uri )   {
	throw new UnsupportedOperationException();
  }
  final public String getSystemId ( )   {
	throw new UnsupportedOperationException();
  }
  public Node appendChild ( Node n ) throws DOMException   {
	throw new UnsupportedOperationException();
  }
  public Node insertBefore ( Node n , Node refNode ) throws DOMException   {
	throw new UnsupportedOperationException();
  }
  public Node replaceChild ( Node newChild , Node refChild ) throws DOMException   {
	throw new UnsupportedOperationException();
  }
  final public short getNodeType ( )   {
	throw new UnsupportedOperationException();
  }
  final public DocumentType getDoctype ( )   {
	throw new UnsupportedOperationException();
  }
  public DocumentType setDoctype ( String dtdPublicId , String dtdSystemId , String internalSubset )   {
	throw new UnsupportedOperationException();
  }
  public Element getDocumentElement ( )   {
	throw new UnsupportedOperationException();
  }
/*  final public void setElementFactory ( ElementFactory factory )   {
	throw new UnsupportedOperationException();
  }
  final public ElementFactory getElementFactory ( )   {
	throw new UnsupportedOperationException();
  }*/
  public Element createElement ( String tagName ) throws DOMException   {
	throw new UnsupportedOperationException();
  }
  public Element createElementNS ( String namespaceURI , String qualifiedName ) throws DOMException   {
	throw new UnsupportedOperationException();
  }
/*  final public ElementEx createElementEx ( String tagName ) throws DOMException   {
	throw new UnsupportedOperationException();
  }
  final public ElementEx createElementEx ( String uri , String tagName ) throws DOMException   {
	throw new UnsupportedOperationException();
  }
  public Text createTextNode ( String text )   {
	throw new UnsupportedOperationException();
  }
  public CDATASection createCDATASection ( String text )   {
	throw new UnsupportedOperationException();
  }
  TextNode newText ( char buf [ ] , int offset , int len ) throws SAXException   {
	throw new UnsupportedOperationException();
  }
  public ProcessingInstruction createProcessingInstruction ( String target , String instructions ) throws DOMException   {
	throw new UnsupportedOperationException();
  }*/
  public Attr createAttribute ( String name ) throws DOMException   {
	throw new UnsupportedOperationException();
  }
  public Attr createAttributeNS ( String namespaceURI , String qualifiedName ) throws DOMException   {
	throw new UnsupportedOperationException();
  }
/*  public Comment createComment ( String data )   {
	throw new UnsupportedOperationException();
  }*/
  public Document getOwnerDoc ( )   {
	throw new UnsupportedOperationException();
  }
/*  public DOMImplementation getImplementation ( )   {
	throw new UnsupportedOperationException();
  }
  public DocumentFragment createDocumentFragment ( )   {
	throw new UnsupportedOperationException();
  }
  public EntityReference createEntityReference ( String name ) throws DOMException   {
	throw new UnsupportedOperationException();
  }*/
  final public String getNodeName ( )   {
	throw new UnsupportedOperationException();
  }
  public Node cloneNode ( boolean deep )   {
	throw new UnsupportedOperationException();
  }
  final public void changeNodeOwner ( Node node ) throws DOMException   {
	throw new UnsupportedOperationException();
  }
  public Element getElementById ( String elementId )   {
	throw new UnsupportedOperationException();
  }
/*  public ElementEx getElementExById ( String id )   {
	throw new UnsupportedOperationException();
  }*/
  public Node importNode ( Node importedNode , boolean deep ) throws DOMException   {
	throw new UnsupportedOperationException();
  }
  public NodeList getChildNodes() {
  	throw new UnsupportedOperationException();
  }
  public Node getParentNode() {
  	throw new UnsupportedOperationException();
  }
  public Node removeChild(Node oldChild) throws DOMException {
  	throw new UnsupportedOperationException();
  }

}
