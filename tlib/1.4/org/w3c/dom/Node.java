/*
 * Copyright (c) 2000 World Wide Web Consortium,
 * (Massachusetts Institute of Technology, Institut National de
 * Recherche en Informatique et en Automatique, Keio University). All
 * Rights Reserved. This program is distributed under the W3C's Software
 * Intellectual Property License. This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See W3C License http://www.w3.org/Consortium/Legal/ for more details.
 */

package org.w3c.dom;

/**
 * The <code>Node</code> interface is the primary datatype for the entire 
 * Document Object Model. It represents a single node in the document tree. 
 * While all objects implementing the <code>Node</code> interface expose 
 * methods for dealing with children, not all objects implementing the 
 * <code>Node</code> interface may have children. For example, 
 * <code>Text</code> nodes may not have children, and adding children to 
 * such nodes results in a <code>DOMException</code> being raised.
 * <p>The attributes <code>nodeName</code>, <code>nodeValue</code> and 
 * <code>attributes</code> are included as a mechanism to get at node 
 * information without casting down to the specific derived interface. In 
 * cases where there is no obvious mapping of these attributes for a 
 * specific <code>nodeType</code> (e.g., <code>nodeValue</code> for an 
 * <code>Element</code> or <code>attributes</code> for a <code>Comment</code>
 * ), this returns <code>null</code>. Note that the specialized interfaces 
 * may contain additional and more convenient mechanisms to get and set the 
 * relevant information.
 * <p>The values of <code>nodeName</code>, 
 * <code>nodeValue</code>, and <code>attributes</code> vary according to the 
 * node type as follows: 
 * <table border='1' summary="Describes interface, node name, node value, and attributes">
 * <tr>
 * <th>Interface</th>
 * <th>nodeName</th>
 * <th>nodeValue</th>
 * <th>attributes</th>
 * </tr>
 * <tr>
 * <td valign='top'>Attr</td>
 * <td valign='top'>name of 
 * attribute</td>
 * <td valign='top'>value of attribute</td>
 * <td valign='top'>null</td>
 * </tr>
 * <tr>
 * <td valign='top'>CDATASection</td>
 * <td valign='top'><code>"#cdata-section"</code></td>
 * <td valign='top'>
 * content of the CDATA Section</td>
 * <td valign='top'>null</td>
 * </tr>
 * <tr>
 * <td valign='top'>Comment</td>
 * <td valign='top'><code>"#comment"</code></td>
 * <td valign='top'>content of 
 * the comment</td>
 * <td valign='top'>null</td>
 * </tr>
 * <tr>
 * <td valign='top'>Document</td>
 * <td valign='top'><code>"#document"</code></td>
 * <td valign='top'>null</td>
 * <td valign='top'>null</td>
 * </tr>
 * <tr>
 * <td valign='top'>DocumentFragment</td>
 * <td valign='top'>
 * <code>"#document-fragment"</code></td>
 * <td valign='top'>null</td>
 * <td valign='top'>null</td>
 * </tr>
 * <tr>
 * <td valign='top'>DocumentType</td>
 * <td valign='top'>document type name</td>
 * <td valign='top'>
 * null</td>
 * <td valign='top'>null</td>
 * </tr>
 * <tr>
 * <td valign='top'>Element</td>
 * <td valign='top'>tag name</td>
 * <td valign='top'>null</td>
 * <td valign='top'>NamedNodeMap</td>
 * </tr>
 * <tr>
 * <td valign='top'>Entity</td>
 * <td valign='top'>entity name</td>
 * <td valign='top'>null</td>
 * <td valign='top'>null</td>
 * </tr>
 * <tr>
 * <td valign='top'>
 * EntityReference</td>
 * <td valign='top'>name of entity referenced</td>
 * <td valign='top'>null</td>
 * <td valign='top'>null</td>
 * </tr>
 * <tr>
 * <td valign='top'>Notation</td>
 * <td valign='top'>notation name</td>
 * <td valign='top'>null</td>
 * <td valign='top'>
 * null</td>
 * </tr>
 * <tr>
 * <td valign='top'>ProcessingInstruction</td>
 * <td valign='top'>target</td>
 * <td valign='top'>entire content excluding the target</td>
 * <td valign='top'>null</td>
 * </tr>
 * <tr>
 * <td valign='top'>Text</td>
 * <td valign='top'>
 * <code>"#text"</code></td>
 * <td valign='top'>content of the text node</td>
 * <td valign='top'>null</td>
 * </tr>
 * </table> 
 * <p>See also the <a href='http://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113'>Document Object Model (DOM) Level 2 Core Specification</a>.
 */
public interface Node {
    // NodeType
    /**
     * The node is an <code>Element</code>.
     */
    public static final short ELEMENT_NODE              = 1;
    /**
     * The node is an <code>Attr</code>.
     */
    public static final short ATTRIBUTE_NODE            = 2;
    /**
     * The node is a <code>Text</code> node.
     */
    public static final short TEXT_NODE                 = 3;
    /**
     * The node is a <code>CDATASection</code>.
     */
    public static final short CDATA_SECTION_NODE        = 4;
    /**
     * The node is an <code>EntityReference</code>.
     */
    public static final short ENTITY_REFERENCE_NODE     = 5;
    /**
     * The node is an <code>Entity</code>.
     */
    public static final short ENTITY_NODE               = 6;
    /**
     * The node is a <code>ProcessingInstruction</code>.
     */
    public static final short PROCESSING_INSTRUCTION_NODE = 7;
    /**
     * The node is a <code>Comment</code>.
     */
    public static final short COMMENT_NODE              = 8;
    /**
     * The node is a <code>Document</code>.
     */
    public static final short DOCUMENT_NODE             = 9;
    /**
     * The node is a <code>DocumentType</code>.
     */
    public static final short DOCUMENT_TYPE_NODE        = 10;
    /**
     * The node is a <code>DocumentFragment</code>.
     */
    public static final short DOCUMENT_FRAGMENT_NODE    = 11;
    /**
     * The node is a <code>Notation</code>.
     */
    public static final short NOTATION_NODE             = 12;


    public Node appendChild(Node newChild)
    throws DOMException;
    public Node getParentNode();
    public Node removeChild(Node oldChild)
    throws DOMException;
    public NodeList getChildNodes();

}
