#ifndef _INCLUDE_ReachableContext_H_
#define _INCLUDE_ReachableContext_H_


class TreeNode;
class Environ;


class TreeNode::ReachableContext // The static analysis context for reachability
{
public:
  ReachableContext();
  ReachableContext(bool, bool, bool, bool, Environ *); 
  ReachableContext(ReachableContext &);
  ReachableContext(ReachableContext &, Environ *);

  bool reachable; // whether or not the current statement is reachable
  bool broken;    // whether or not a break statement has been encountered
  bool continued; // whether or not a continue statement has been encountered
  bool report;    // whether or not subsequent errors should be reported
  Environ *env;   // environment for labels on statements
};

inline TreeNode::ReachableContext::ReachableContext()
  :  reachable(true),
     broken(false),
     continued(false),
     report(true),
     env(NULL)
{
}

inline TreeNode::ReachableContext::ReachableContext(bool _reachable, 
						    bool _broken, 
						    bool _continued, 
						    bool _report, 
						    Environ *_env)
  :  reachable(_reachable), 
     broken(_broken), 
     continued(_continued), 
     report(_report), 
     env(_env) 
{
}

inline TreeNode::ReachableContext::ReachableContext(ReachableContext &ctx)
  :  reachable(ctx.reachable), 
     broken(ctx.broken), 
     continued(ctx.continued), 
     report(ctx.report), 
     env(ctx.env) 
{
}

inline TreeNode::ReachableContext::ReachableContext(ReachableContext &ctx, 
						    Environ *_env)
  :  reachable(ctx.reachable), 
     broken(ctx.broken), 
     continued(ctx.continued), 
     report(ctx.report), 
     env(_env) 
{
}

#endif
