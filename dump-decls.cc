#define __SINST__		// g++ header bug
#include <iostream>
#include "AST.h"
#include "ClassDecl.h"
#include "FieldDecl.h"
#include "MethodDecl.h"
#include "PrimitiveDecl.h"
#include "decls.h"


void dump( ostream &sink, unsigned, const Decl * value )
{
  value->dump( sink );
}


void Decl::dump( ostream &sink ) const
{
  if (this)
    {
      sink << '<' << thisClassName() << ' ' << this;
      dumpAttributes( sink );
      sink << '>';
    }
  else
    sink << "(nodecl)";
}


void PackageDecl::dumpAttributes( ostream & ) const
{
}


void ClassDecl::dumpAttributes( ostream &sink ) const
{
  sink << ' ' << source();
}


void LocalVarDecl::dumpAttributes( ostream &sink ) const
{
  sink << ' ' << source();
}


void FormalParameterDecl::dumpAttributes( ostream &sink ) const
{
  sink << ' ' << source();
}


void FieldDecl::dumpAttributes( ostream &sink ) const
{
  sink << ' ' << source();
}


void MethodDecl::dumpAttributes( ostream &sink ) const
{
  sink << ' ' << source()
       << ' ' << overrides()
       << " { ";
  
  for (MethodSet::const_iterator sweep = implements().begin();
       sweep != implements().end(); ++sweep)
    sink << *sweep << ' ';
  
  sink << '}';
}


void StmtLblDecl::dumpAttributes( ostream &sink ) const
{
  sink << ' ' << source();
}


void PrimitiveDecl::dumpAttributes( ostream &sink ) const
{
}
