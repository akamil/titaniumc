#ifndef _INCLUDE_TITANIUM_AUX_DEBUG_H_
#define _INCLUDE_TITANIUM_AUX_DEBUG_H_

#include <iostream>
#include <fstream>

class MethodDecl;

void emitStackDebugInformation(ofstream &);
void emitStaticDebugInformation(ofstream &, const MethodDecl &);

#endif // !_INCLUDE_TITANIUM_AUX_DEBUG_H_
