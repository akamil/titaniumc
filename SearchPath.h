#ifndef _INCLUDE_SEARCHPATH_H_
#define _INCLUDE_SEARCHPATH_H_

#include <cstdio>
#include <dirent.h>
#include <string>
#include <cstring>
#include <cstdlib>
#include <vector>
#include "using-std.h"


class SearchPath : public vector< string >
{
public:
    SearchPath( const char *, const string & );
    DIR *opendir( string & ) const;
    FILE *openSource( string & ) const;

    static const char  dirSep;
    static const char pathSep;

protected:
    FILE *tryOpen( const string &, string &, const char [] ) const;

private:
    void addPaths( const string & );
    void addPath(  const string & );
};

FILE *ti_fopen(const char *path, const char *mode);
int ti_fclose(FILE *stream);
string *ti_fname(FILE *stream);

#endif
