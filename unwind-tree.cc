#include "AST.h"


void ConstructorDeclNode::unwindTree( CleanupList &, const TreeNode *target ) const
{
  assert( !target );
}


void MethodDeclNode::unwindTree( CleanupList &, const TreeNode *target ) const
{
  assert( !target );
}


void StatementNode::unwindTree( CleanupList &list, const TreeNode *target ) const
{
  if (this != target)
    TreeNode::unwindTree( list, target );
}


void TreeNode::unwindTree( CleanupList &list, const TreeNode *target ) const
{
  parent()->unwindTree( list, target );
  unwindSelf( list );
}
