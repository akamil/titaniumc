#ifndef _TITANIUM_CODE_UTIL_H_
#define _TITANIUM_CODE_UTIL_H_

#include <string>
#include <cstring>

#include "compiler.h"
#include "code-commonsub.h"

class MethodCallNode;
class TreeNode;
class StatementNode;
class MethodDecl;

void appendWithCommaOperator(string &s, const string &o);
void appendWithSemicolon(string &s, const string &o);

string &unique(void *p);
int &uniqueInt(void *p);
int unique2(void *v, void *w);

Decl * getRectDomainMethodDecl(int arity, const string *name);
Decl * getDomainMethodDecl(int arity, const string *name);
string nameOfEnclosingMethod(const TreeNode *t);
TreeNode *enclosingMethod(const TreeNode *t);
StatementNode *enclosingStmt(const TreeNode *t);
llist<StatementNode *> *all_substmts(TreeNode *t);
TreeNode *enclosingForeach(const TreeNode *t);
TreeNode *enclosingDummy(const TreeNode *t);
static inline bool inDummyNode(const TreeNode *a) { return enclosingDummy(a) != NULL; }
TreeNode *isAncestor(TreeNode *a, TreeNode *b);
TreeNode *isAncestorOfAny(TreeNode *a, llist<TreeNode *> *l);
bool isLHSofAssignNode(TreeNode *t);

// Returns whether s appears in z, a list of n strings.
bool strFind(const char *z[], int n, const string& s);

// Returns whether the given package or type is part of the libraries.
bool isInLibrary(Decl *d);

bool isEmptyStmtNode(const TreeNode *t);
bool isConstantOrCastOfConstant(const TreeNode *t);
bool isNullOrCastOfNull(const TreeNode *t);
bool isTrivialPointOrDomain(const TreeNode *t);
bool isIntLiteralInRange(const TreeNode *t, int lo, int hi);

bool isPureCallThatCannotFail(const TreeNode *t);
bool isSideEffectFreeAndCannotFail(const TreeNode *t);
bool isMethodCallWhereinNoExceptionsCanBeThrown(const MethodCallNode *t);
bool canThrowCheckedException(const MethodDecl *d);

char arithCall(const TreeNode *t);
bool isGridMethod(const TreeNode *t, string methodName = "");
bool isAnyGridMethod(const MethodCallNode *t);
bool isGridMethodMutating(const MethodCallNode *t);

bool isTiOrNumericType(const TreeNode *x);
bool isTiOrNumericType(const TypeNode *t);
bool isPointOrIntegralType(const TypeNode *t);
bool doesNotReadOrWriteNonArgs(const MethodCallNode *t);

bool isInSystemPackage(const Decl *d, const string &n1, const string &n2);
bool isInJavaLang(const Decl *d);
bool isInJavaIO(const Decl *d);

bool isKnownSideEffectFree(const TreeNode *md);
bool isKnownPure(const TreeNode *md);

bool isStaticInitNode(const TreeNode *t);
bool isInstanceInitNode(const TreeNode *t);
bool isFieldDeclNode(const TreeNode *t);
bool isClassDeclNode(const TreeNode *t);
bool isInterfaceDeclNode(const TreeNode *t);
bool isCodeLiteralExprNode(const TreeNode *t);
bool isIIFAN(const TreeNode *t);
bool isOFAN(const TreeNode *t);
bool isCodeLiteralFAN(const TreeNode *t);
bool isTypeFAN(const TreeNode *t);
bool isStaticFAN(const TreeNode *t);
bool isThisFAN(const TreeNode *t);
bool isSFAN(const TreeNode *t);
#define isSuperFieldAccessNode isSFAN
#define isThisFieldAccessNode  isThisFAN
bool isStringConcatNode(const TreeNode *t);
bool isStringConcatAssignNode(const TreeNode *t);
bool isAssertNode(const TreeNode *t);
bool isNameNode(const TreeNode *t);
bool isTypeNameNode(const TreeNode *t);
bool isLabeledStmtNode(const TreeNode *t);
bool isThisNode(const TreeNode *t);
bool isObjectNode(const TreeNode *t);
bool isObjectFieldAccessNode(const TreeNode *t);
bool isIfStmtNode(const TreeNode *t);
bool isForEachStmtNode(const TreeNode *t);
bool isForEachPairNode(const TreeNode *t);
bool isReorderNode(const TreeNode *t);
bool isHasNoOverlapNode(const TreeNode *t);
bool isUpdatePointBeforeStmtNode(const TreeNode *t);
bool isMethodCallNode(const TreeNode *t);
bool isMethodCallAssignNode(const TreeNode *t);
bool isPointNode(const TreeNode *t);
bool isDomainNode(const TreeNode *t);
bool isMethodDeclNode(const TreeNode *t);
bool isTitaniumArrayTypeNode(const TreeNode *t);
bool isConstructorDeclNode(const TreeNode *t);
bool isTreeListNode(const TreeNode *t);
bool isCompileUnitNode(const TreeNode *t);
bool isBlockNode(const TreeNode *t);
bool isOmittedNode(const TreeNode *t);
bool isVarDeclNode(const TreeNode *t);
bool isAssignNode(const TreeNode *t);
bool isSynchronizedNode(const TreeNode *t);
bool isParameterNode (const TreeNode *t);
bool isStringLitNode(const TreeNode *t); 
bool isPrimitiveLitNode(const TreeNode *t);
bool isNullPntrNode(const TreeNode *t);
bool isIBroadcastNode(const TreeNode *t);
bool isAllocateNode(const TreeNode *t);
bool isAllocateSpaceNode(const TreeNode *t);
bool isAllocateArrayNode(const TreeNode *t);
bool isArrayNameNode(const TreeNode *t);
bool isExpressionStmtNode(const TreeNode *t);
bool isSwitchBranchNode(const TreeNode *t);
bool isDummyNode(const TreeNode *t);
bool isGotoNode(const TreeNode *t);
bool isBlockNode(const TreeNode *t);
bool isExprNode(const TreeNode *t);
bool isArrayAccessNode(const TreeNode *t);
bool isJavaArrayAccessNode(const TreeNode *t);
bool isPointArrayAccessNode(const TreeNode *t);
bool isTitaniumArrayAccessNode(const TreeNode *t);
bool isSRArrayAccessNode(const TreeNode *t);
bool isOSRArrayAccessNode(const TreeNode *t);
bool isTryStmtNode(const TreeNode *t);
bool isFinallyNode(const TreeNode *t);
bool isArrayInitNode(const TreeNode *t);
bool isReturnNode(const TreeNode *t);
bool isThisConstructorCallNode(const TreeNode *t);
bool isSuperConstructorCallNode(const TreeNode *t);

bool isPreIncrNode(const TreeNode *t);
bool isPreDecrNode(const TreeNode *t);
bool isPostIncrNode(const TreeNode *t);
bool isPostDecrNode(const TreeNode *t);

bool isForNode(const TreeNode *t);
bool isDoNode(const TreeNode *t);
bool isWhileNode(const TreeNode *t);
bool isBreakNode(const TreeNode *t);
bool isContinueNode(const TreeNode *t);

bool isCastNode(const TreeNode *t);
bool isInstanceOfNode(const TreeNode *t);
bool isDynamicTypeNode(const TreeNode *t);

bool isUnaryPlusNode(const TreeNode *t);
bool isUnaryMinusNode(const TreeNode *t);
bool isUnaryArithNode(const TreeNode *t);

bool isMultNode(const TreeNode *t);
bool isDivNode(const TreeNode *t);
bool isRemNode(const TreeNode *t);
bool isPlusNode(const TreeNode *t);
bool isMinusNode(const TreeNode *t);
bool isBinaryArithNode(const TreeNode *t);

bool isLeftShiftLogNode(const TreeNode *t);
bool isRightShiftLogNode(const TreeNode *t);
bool isRightShiftArithNode(const TreeNode *t);
bool isShiftNode(const TreeNode *t);

bool isEqualityNode(const TreeNode *t);
bool isEQNode(const TreeNode *t);
bool isNENode(const TreeNode *t);
bool isLTNode(const TreeNode *t);
bool isGTNode(const TreeNode *t);
bool isLENode(const TreeNode *t);
bool isGENode(const TreeNode *t);

bool isRelationNode(const TreeNode *t);
bool isBitwiseNode(const TreeNode *t);
bool isLogCondNode(const TreeNode *t);

bool isBitAndNode(const TreeNode *t);
bool isBitOrNode(const TreeNode *t);
bool isBitXorNode(const TreeNode *t);
bool isNotNode(const TreeNode *t);
bool isComplementNode(const TreeNode *t);


bool isFormalParamDecl(const Decl *d);
bool isLocalVarDecl(const Decl *d);
bool isFormalParam(const TreeNode *t);
bool isLocalVar(const TreeNode *t);
bool isConstant(const TreeNode *t);
Decl *getDeclFromLocalOrFormal(const TreeNode *t);

bool isTrue(const TreeNode *t);
bool isFalse(const TreeNode *t);
bool isNOP(const TreeNode *t);

bool isMultAssignNode(const TreeNode *t);
bool isDivAssignNode(const TreeNode *t);
bool isRemAssignNode(const TreeNode *t);
bool isPlusAssignNode(const TreeNode *t);
bool isMinusAssignNode(const TreeNode *t);
bool isLeftShiftLogAssignNode(const TreeNode *t);
bool isRightShiftLogAssignNode(const TreeNode *t);
bool isRightShiftArithAssignNode(const TreeNode *t);
bool isBitAndAssignNode(const TreeNode *t);
bool isBitXorAssignNode(const TreeNode *t);
bool isBitOrAssignNode(const TreeNode *t);

bool isLazyOptimizeNode(const TreeNode *t);

bool isMonitorFetchClassNode(const TreeNode *t);
bool isMonitorFetchInstanceNode(const TreeNode *t);
bool isMonitorLockNode(const TreeNode *t);
bool isMonitorUnlockNode(const TreeNode *t);
bool isMonitorFetchNode(const TreeNode *t);
bool isMonitorUseNode(const TreeNode *t);

bool isCatchNode(const TreeNode *t);

bool isTemplateDeclNode(const TreeNode *t);
bool isTemplateInstanceDeclNode(const TreeNode *t);
bool isTemplateInstanceTypeNode(const TreeNode *t);
bool isTemplateNameNode(const TreeNode *t);
bool isMethodSignatureNode(const TreeNode *t);

int isDomainOrtiDomainType(const TypeNode& t);

// Performs the specified action if the -dumpast user argument matches the
// current filename being worked on and the -dumpphase user argument contains
// the specified string. For example:
//
//   DEBUG_PHASE("opt", currentFilename, t->print(cout,0)) 
//
// will dump the AST if -dumpphase contains "opt" and the filename 
// currently being worked on matches the -dumpast user argument.
//
// If -dumpphase is not given, the action will be performed provided the 
// current filename matches.
// If -dumpast is the string "all", all files will have the action
// performed provided the current phase name matches.
// If -dumpphase is given but -dumpast is not given then -dumpast
// defaults to "all".
//
// So on the tc command line, you can say things like:
// tc -dumpast all (all dumps)
// tc -dumpast all -dumpphase cfg (CFG info for all files)
// tc -dumpphase cfg (CFG info for all files)
// tc -dumpast foo.ti (all dumps for foo.ti)
// tc -dumpast foo.ti -dumpphase "cfg lowered" (dump out CFG and lowered AST)
#define DEBUG_PHASE_ENABLED(phasename, currentFilename)			\
  (dump_ast && ((!::strcmp(dump_ast, "all") ||				\
		 (currentFilename) != NULL &&				\
		 (::strstr((currentFilename), dump_ast))) &&		\
		(!dump_phase || ::strstr(dump_phase, (phasename)))))

#define DEBUG_PHASE(phasename, currentFilename, action)				\
  do if DEBUG_PHASE_ENABLED(phasename, currentFilename) { action; } while (0)

extern void ASTDump(CompileUnitNode *cun, const char *keyname, const char *desc);

string int2alpha(int i);
int gcd(int a, int b);


// output tc compile status info at verbosity level 1..4
extern void compile_status(int verbose_level, string message);

// format a compile time, by default the current compile time
extern string compile_time(struct timeval *tv = NULL);

#endif /* _TITANIUM_CODE_UTIL_H_ */
