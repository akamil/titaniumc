#ifndef INCLUDE_NAMES_QUEUE_H
#define INCLUDE_NAMES_QUEUE_H

#include <list>

class CompileUnitNode;


struct NamesQueue : public list< CompileUnitNode * >
{
  void resolve();
};


extern NamesQueue unresolvedNames;


#endif //INCLUDE_NAMES_QUEUE_H


// Local Variables:
// c-file-style: "gnu"
// End:
