#ifndef _TC_GCSET_H_
#define _TC_GCSET_H_

#include <set>

#define generic template <class T>

generic class gcset : public gc {
 public:
  typedef typename set<T>::size_type size_type;
  typedef typename set<T>::iterator iterator;
  typedef typename set<T>::const_iterator const_iterator;

  set<T> s;

  void insert(const T & x) { s.insert(x); }
  bool contains(const T & x) const { return s.find(x) != s.end(); }
  bool empty() const { return s.empty(); }
  size_type size() const { return s.size(); }
  size_type count(const T & x) const { return s.count(x); }
  iterator find(const T & x) const { return s.find(x); }

  const_iterator begin() const { return s.begin(); }
  iterator begin() { return s.begin(); }
  const_iterator end() const { return s.end(); }
  iterator end() { return s.end(); }

  const_iterator rbegin() const { return s.rbegin(); }
  iterator rbegin() { return s.rbegin(); }
  const_iterator rend() const { return s.rend(); }
  iterator rend() { return s.rend(); }
};

#undef generic

#endif
