#ifndef _include_CfHeader_h_
#define _include_CfHeader_h_

#include <iosfwd>
#include <string>
#include "../IncludeOnce.h"
#include "CfCode.h"


class CfHeader : public CfCode, private IncludeOnce {
public:
  explicit CfHeader( const string & );
  ~CfHeader() { finalize(); }
};


#endif // !_include_CfHeader_h_


// Local Variables:
// c-file-style: "gnu"
// End:
