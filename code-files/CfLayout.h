#ifndef _include_CfLayout_h_
#define _include_CfLayout_h_

#include <string>
#include "../IncludeOnce.h"
#include "CfCode.h"


class CtType;


class CfLayout : public CfFile, private IncludeOnce {
public:
  explicit CfLayout( const CtType & );
  
  static const string deriveName( const CtType & );
  static set<string> tlib_include_files;
};


#endif // !_include_CfLayout_h_


// Local Variables:
// c-file-style: "gnu"
// End:
