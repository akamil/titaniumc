#ifndef _include_CfDepends_h_
#define _include_CfDepends_h_

#include <map>
#include <string>
#include "CfFile.h"
#include "../StringSet.h"


class CfSource;


class CfDepends : public CfFile {
public:
  CfDepends( const CfSource & );

  typedef map< const string, StringSet, less< const string > > Inclusions;
  static Inclusions inclusions;
  
private:
  void collect( StringSet &, const string & ) const;
};


#endif // !_include_CfDepends_h_


// Local Variables:
// c-file-style: "gnu"
// End:
