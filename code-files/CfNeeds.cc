#include "../c-types/CtType.h"
#include "CfCode.h"
#include "CfNeeds.h"


CfNeeds::CfNeeds( const CfCode &code )
  : CfFile( deriveName( code ), 'h' ),
    IncludeOnce( *this, basename )
{
}


const string CfNeeds::deriveName( const CfCode &code )
{
  return "needs!" + code.filename();
}


// Local Variables:
// c-file-style: "gnu"
// End:
