#include "CfDepends.h"
#include "CfSource.h"


CfDepends::Inclusions CfDepends::inclusions;


CfDepends::CfDepends( const CfSource &source )
  : CfFile( source.basename, 'd' )
{
  StringSet seen;
  collect( seen, source.filename() );

  *this << "$(tc_gen)/" << basename << ".o: \\\n";
  
  for (StringSet::const_iterator sweep = seen.begin();
       sweep != seen.end(); ++sweep)
    *this << "\t$(tc_gen)/" << *sweep << "\\\n";
}


void CfDepends::collect( StringSet &seen, const string &file ) const
{
  if (seen.insert( file ).second)
    {
      Inclusions::const_iterator found = inclusions.find( file );
      if (found != inclusions.end())
	for (StringSet::const_iterator subfile = found->second.begin();
	     subfile != found->second.end(); ++subfile)
	  collect( seen, *subfile );
    }
}


// Local Variables:
// c-file-style: "gnu"
// End:
