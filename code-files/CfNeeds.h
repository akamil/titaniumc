#ifndef _include_CfNeeds_h_
#define _include_CfNeeds_h_

#include <string>
#include "../IncludeOnce.h"
#include "CfFile.h"


class CfCode;


class CfNeeds : public CfFile, private IncludeOnce {
public:
  explicit CfNeeds( const CfCode & );
  
  static const string deriveName( const CfCode & );
};


#endif // !_include_CfNeeds_h_


// Local Variables:
// c-file-style: "gnu"
// End:
