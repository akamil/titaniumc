#ifndef _include_CfSource_h_
#define _include_CfSource_h_

#include <iosfwd>
#include <string>
#include "CfCode.h"
#include "../StringLitTable.h"


class CfSource : public CfCode, public StringLitTable {
public:
  explicit CfSource( const string &, string s = "");
  ~CfSource() { finalize(); } 
private:
  void initialize(string s);
};


#endif // !_include_CfSource_h_


// Local Variables:
// c-file-style: "gnu"
// End:
