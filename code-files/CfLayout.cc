#include "../c-types/CtType.h"
#include "CfLayout.h"

set<string> CfLayout::tlib_include_files;

CfLayout::CfLayout( const CtType &type )
  : CfFile( deriveName( type ), 'h' ),
    IncludeOnce( *this, basename )
{
}


const string CfLayout::deriveName( const CtType &type )
{
  return "layout!" + type;
}


// Local Variables:
// c-file-style: "gnu"
// End:
