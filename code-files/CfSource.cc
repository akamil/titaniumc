#include "../c-types/CtType.h"
#include "CfSource.h"

extern bool bounds_checking;
extern bool opt_stoptifu;

CfSource::CfSource( const string &basename, string s )
  : CfCode( basename, 'c' )
{
  initialize(s);
}


void CfSource::initialize(string s)
{
  if (!bounds_checking) {
    // ensure this makes it to the very top of every source file
    *this << "#define BOUNDS_CHECKING 0\n";
  }
  if (opt_stoptifu) {
    *this << "#define USE_STOPTIFU 1\n";
  }
  *this << s << "#include \"titanium.h\"\n";
  includeNeeds();
}


// Local Variables:
// c-file-style: "gnu"
// End:
