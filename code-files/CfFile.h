#ifndef _include_CfFile_h_
#define _include_CfFile_h_

#include <fstream>
#include <string>
#include "../using-std.h"


class CtType;


class CfFile : public ofstream {
public:
  void include( const string & );

  const string basename;
  const string suffix;
  const string filename() const;

protected:
  CfFile( const string &, char );
  CfFile( const string &, char [] );

private:
  const string pathname() const;
};


#endif // !_include_CfFile_h_


// Local Variables:
// c-file-style: "gnu"
// End:
