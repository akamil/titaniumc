#include "../c-types/CtType.h"
#include "CfHeader.h"


CfHeader::CfHeader( const string &basename )
  : CfCode( basename, 'h' ),
    IncludeOnce( *this, basename )
{
  includeNeeds();
}


// Local Variables:
// c-file-style: "gnu"
// End:
