#include <fstream>
#include "../IncludeOnce.h"
#include "../c-types/CtType.h"
#include "../code.h"
#include "CfFile.h"


CfFile::CfFile( const string &basename, char suffix )
  : basename( basename ),
    suffix( 1, suffix )
{
  string path = pathname();
  open( path.c_str() );
}


CfFile::CfFile( const string &basename, char suffix[] )
  : basename( basename ),
    suffix( suffix )
{
  string path = pathname();
  open( path.c_str() );
}


void CfFile::include( const string &headerBase )
{
  // this could be a good bottleneck point
  // for automatic dependency generation
  
  const string header = headerBase + ".h";
  *this << "#include \"" << header << "\"\n";
}


const string CfFile::filename() const
{
  return basename + '.' + suffix;
}


const string CfFile::pathname() const
{
  return codeGen_outdir + filename();
}


// Local Variables:
// c-file-style: "gnu"
// End:
