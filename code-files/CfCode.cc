#include <fstream>
#include "../IncludeOnce.h"
#include "../c-types/CtType.h"
#include "CfCode.h"
#include "CfNeeds.h"


CfCode::CfCode( const string &basename, char suffix )
  : CfFile( basename, suffix )
{
}


void CfCode::finalize()
{
  CfNeeds needs( *this );
  defineAll( needs );
}


void CfCode::includeNeeds()
{
  include( CfNeeds::deriveName( *this ) );
}


// Local Variables:
// c-file-style: "gnu"
// End:
