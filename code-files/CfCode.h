#ifndef _include_CfCode_h_
#define _include_CfCode_h_

#include <string>
#include "../c-types/CtRegistry.h"
#include "CfFile.h"


class CfCode : public CfFile, public CtRegistry {
protected:
  CfCode( const string &, char );
  void finalize();

  void includeNeeds();
};


#endif // !_include_CfCode_h_


// Local Variables:
// c-file-style: "gnu"
// End:
