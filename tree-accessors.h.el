(defun insert-set-get (name type &optional value)
  (unless (string-match ":" (symbol-name name))
    (let ((node-type (to-c++-node-type type)))
      (insert (format "    virtual %s %s () const;\n" node-type name)
	      (format "    virtual void %s (%s);\n" name node-type)))))

(defalias 'defattr*         'insert-set-get)
(defalias 'defattr-default* 'insert-set-get)
(defalias 'defchild*        'insert-set-get)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; The friend stuff allows use of the protected members of TreeNode

(defun insert-friend (name &rest other)
  (insert (format "    friend class %s;\n" name)))

(defalias 'defnode*     'insert-friend)
(defalias 'deflistnode* 'insert-friend)

(defun defbasenode* (name &rest other) 
  (if (not (string-equal name "TreeNode"))
      (insert-friend name)))
