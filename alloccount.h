#ifndef _alloccount_h_
#define _alloccount_h_

#include <string>

// Remove in production.
#define COUNT_ALLOCATION 0

#if COUNT_ALLOCATION
void ALLOCCOUNT(const string &s);
#define CONS(a, b) (ALLOCCOUNT("cons"), cons((a), (b)))
#else
#define ALLOCCOUNT(s)
#define CONS cons
#endif

#endif // _alloccount_h_
