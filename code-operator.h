#ifndef _INCLUDE_CODE_OPERATOR_H_
#define _INCLUDE_CODE_OPERATOR_H_

#include <string>
#include "using-std.h"


const string encodedMethodName( const string &, const string & );


#endif // !_INCLUDE_CODE_OPERATOR_H_
