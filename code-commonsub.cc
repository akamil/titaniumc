/* THIS FILE IS NOT USED ANYMORE */

#include <cassert>
#include <cstdio>
#include <fstream>
#include <iomanip>
#include <iostream>
#include "AST.h"
#include "code-commonsub.h"
#include "code-grid.h"
#include "code-util.h"
#include "code.h"
#include "decls.h"
#include "errors.h"
#include "interface.h"
#include "osstream.h"
#include "utils.h"

extern bool debug_opt;

extern bool apparentlyIdenticalChildren(const TreeNode *u, const TreeNode *t);

/* If this isn't an ExprNode or subclass of ExprNode, then recursively
   check the children but, for simplicity, always return this. */
void *TreeNode::commonSubExpr()
{
  foriter (p, allChildren(), ChildIter)
    (*p)->commonSubExpr();

  return (void *) this;
}

/* If l contains a node that compares equal to t, return it.  If none
   exists, add t to l and return t.  */
static void *search(llist<TreeNode *> *& l, TreeNode *t, bool& found)
{
  if (l == NULL) {
    l = cons(t, l);
    found = false;
    return (void *) t;
  } else if (COMPARE(l->front(), t)) {
    found = true;
    return (void *) l->front();
  } else
    return search(l->tail(), t, found);
}

static void *commonSubExpr(TreeNode *t, string s, bool childrenOnly)
{
  static map_tree_string_to_void memo[2];
  static map_string_to_list memo_search;
  static map_string_to_void memo_noSearch;

  void *& outermemo = memo[childrenOnly ? 1 : 0][t][s];
  if (outermemo != NULL) return outermemo;

  s = s + t->oper_name();
  foriter (p, t->allChildren(), TreeNode::ChildIter) {
    s = s + ' ' + long2hexstring((long) (*p)->commonSubExpr());
  }
  bool found;
  void *ret =
    childrenOnly ? memo_noSearch[s] : search(memo_search[s], t, found);
  if (childrenOnly && !((found = (ret != NULL)))) 
    ret = memo_noSearch[s] = (void *) t;

  if (debug_commonsub) {
    if (found) printf("commonSubExpr() found memo[%s] = %x\n",
		      s.c_str(), ret);
    else printf("commonSubExpr() didn't find memo[%s]\n",
		s.c_str());
  }

  return (outermemo = ret);
}

void *ExprNode::commonSubExpr()
{
  return ::commonSubExpr(this, "", false);
}

void *ArrayAccessNode::commonSubExpr()
{
  return ::commonSubExpr(this, "", true);
}

void *TreeListNode::commonSubExpr()
{
  return ::commonSubExpr(this, "", true);
}

void *NameNode::commonSubExpr()
{
  return ::commonSubExpr(this, *ident() + ' ' + long2hexstring((long) decl()),
			 true);
  // The above is equivalent but faster because it doesn't rely on searching.
  // return ::commonSubExpr(this, "", false);
}

void *StringLitNode::commonSubExpr()
{
  ostringstream s;
  printEscaped(s, text());
  return ::commonSubExpr(this, s.str(), true);
}

/* FieldAccessNodes */

/* IncrDecrNodes */

/* UnaryArithNodes */

/* BinaryArithNodes */

/* BinaryArithAssignNodes */

/* ShiftNodes */

/* ShiftAssignNodes */

/* RelationNodes */

/* EqualityNodes */

/* BitwiseNodes */

/* BitwiseAssignNodes */

/* LogCondNodes */

/* DynamicTypeNodes */

// ArrayInitNode

// NullPntrNode

// ThisNode

// ArrayAccessNode

// ObjectNode

// MethodCallNode

// MethodAssignCallNode

// AllocateNode

// AllocateArrayNode

// ComplementNode

// NotNode

// StringConcatNode

// IfExprNode

// AssignNode

// PointNode

// DomainNode

