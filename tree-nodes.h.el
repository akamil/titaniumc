(defun defnode* (name parents fields methods states)
  (let* ((details (resolve-fields fields))
	 (flds (aref details 2))
	 (kids (aref details 0))
	 (attrs (aref details 1))
	 (states (resolve-states states)))
    (insert (format "class %s : " name))
    (loop for p on parents do
	  (insert (format "public %s%s"
			  (car p)
			  (if (null (cdr p)) "" ", "))))
    (if (null parents)
	(insert "public TreeNode"))
    (insert " {\npublic:\n"
	    (format "    %s (" name))
    (loop for f in flds do
	  (insert (format "%s %s, "
			  (to-c++-type (cadr f))
			  (basic-field-name (car f)))))
    (insert "SourcePosn posn0 = NoSourcePosition);\n")
    (insert "    int arity () const;\n")
    (insert "    const char* oper_name () const;\n")
    (insert (format "    %s* clone () const;\n" name))
    (insert (format "    %s* deepClone () const;\n" name))
    (loop for info in attrs do
	  (let ((name (basic-field-name (car info))))
	    (insert (format "    %s %s () const { return _%s; }\n"
			    (to-c++-node-type (cadr info)) name name)
		    (format "    void %s (%s v) { _%s = v; }\n"
			    name (to-c++-node-type (cadr info)) name))))
    (loop for nchildren from 0 to 100 by 1
	  for info in kids 
	  do (insert (format 
		      "    %s %s () const { return (%s)_children[%d]; }\n"
		      (to-c++-node-type (car (cdr info))) (car info)
		      (to-c++-node-type (car (cdr info))) nchildren)
		     
		     (format 
		      "    void %s (%s v) { child(%d, v); }\n" 
		      (car info) (to-c++-node-type (car (cdr info))) nchildren)))
    (loop for f in methods do
	  (insert "    "
		  (case f
		    ('destructor (format "~%s()" name))
		    (t (cadr (assq f *methods*))))
		  ";\n"))
    (and attrs  (insert "    void print_attrs (ostream&, unsigned = 0) const;\n"))
    (when (or fields
	      (is-subclass-p name 'TypeNode))
      (insert "    void dump  (ostream&, unsigned = 0) const;\n"))
    (insert "    bool compare (const TreeNode *) const;\n")
    (if flds (insert (default-ctor name states)))
    (insert "protected:\n")
    (loop for info in attrs do
	  (insert (format "    %s _%s;\n" 
			  (to-c++-node-type (cadr info))
			  (basic-field-name (car info)))))
    (if kids
	(insert (format "    TreeNode* _children[%d];\n" (length kids))))
    (loop for info in states do
	  (insert (format "    %s %s;\n" (cadr info) (car info))))
    (insert "};\n\n"))
  )

(defun defbasenode* (name parents produce-decls-p methods states)
  (if produce-decls-p
      (let ((states (loop for s in states
		     if (assq s *states*) 
		     collect (assq s *states*) into v else while t
		     finally return v)))
	(insert (format "class %s : " name))
	(loop for p on parents do
	      (insert (format "public %s%s"
			      (car p)
			      (if (null (cdr p)) "" ", "))))
	(if (null parents)
	    (insert "public TreeNode"))
	(insert " {\npublic:\n")
	(insert (format "    %s* clone () const = 0;\n" name))
	(insert (format "    %s* deepClone () const = 0;\n" name))
	(loop for m in methods do
	      (insert (format "    %s;\n" (cadr (assq m *methods*)))))
	(insert (default-ctor name states))
	(loop for info in states do
	      (insert (format "    %s %s;\n" (cadr info) (car info))))
	(insert "};\n\n"))))


(defun default-ctor (name states)
  (format "protected:\n    %s ()%s\n"
	  name
	  (if (need-initializers states) ";" " {}")))

(defun need-initializers (states)
  (and states
       (or (nth 2 (car states))
	   (need-initializers (cdr states)))))

(defun deflistnode* (name methods)
  (let ((element (format "%s*" (list-type name)))
	(name (symbol-name name)))
    (insert "class " name " : public TreeListNode {\n"
	    "public:\n"
	    "    " name "(llist<" element "> *&_children, SourcePosn p = NoSourcePosition)\n"
	    "      : TreeListNode( p )\n"
	    "      { initialize((llist<TreeNode *> *)_children); _children = 0; }\n"
	    "    const char* oper_name() const { return \"" name "\"; }\n"
	    "    " name " *clone() const;\n"
	    "    " name "* deepClone () const;\n"
	    "    " element " child(int i) const {\n"
	    "      return reinterpret_cast<" element ">(TreeListNode::child(i));\n"
	    "    }\n"
	    "    void child(int i, " element " replacement) {\n"
	    "      TreeListNode::child(i, replacement);\n"
	    "    }\n")
    (loop for m in methods do
	  (insert (format "    %s;\n" (cadr (assq m *methods*)))))
    (insert "protected:\n"
	    "    " name " ( SourcePosn posn = NoSourcePosition )\n"
	    "      : TreeListNode( posn )\n"
	    "    {}\n"
	    "};\n\n")))
