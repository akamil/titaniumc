#ifndef _TI_PSEUDOCODE_H_
#define _TI_PSEUDOCODE_H_

#include "osstream.h"

static inline string pseudocode(const TreeNode *t)
{
  if (t == NULL)
    return "NULL";
  ostringstream o;
  t->pseudoprint(o, 0);
  string s = o.str();
  return s;
}

/* If pseudocode(t) is a string containing one '\n' that appears as the
   first character then return the result without that '\n'.  Otherwise
   return pseudocode(t). */
static inline string pseudocode1(const TreeNode *t)
{
  string s = pseudocode(t);
  if (s[0] == '\n') {
    string r = s.substr(1);
    if (r.find('\n') == string::npos)
      return r;
  }
  return s;
}

#endif // _TI_PSEUDOCODE_H_
