(defun initialize-buffer (nodes builder)
  (insert-c++-comment-prefix nodes builder)
  (insert "#include <iostream>\n"
	  "\n"
	  "#include \"AST.h\"\n"
	  "#include \"decls.h\"\n"
	  "\n"
	  "\n"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun defnode* (name parents fields methods states)
  (let* ((details (resolve-fields fields))
	 (flds (aref details 2))
	 (attrs (aref details 1)))
    (if attrs
	(progn (insert
		"\n"
		(format "void %s::print_attrs (ostream& os, unsigned depth) const\n" name)
		"{\n")
	       (loop for f in attrs do
		     (cond ((equal (cadr f) "const string*")
			    (insert (format "    os << \" \\\"\" << *%s() << \"\\\"\";\n" 
					    (car f))))
			   ((or (equal (cadr f) "Common::Modifiers")
				(equal (cadr f) "Modifiers"))
			    (insert (format "    os << \" (\" << stringifyModifiers (%s()) << ')';\n"
					    (car f))))
			   ((and (symbolp (car f)) 
                                 (or (equal "enclosingType" (symbol-name (car f)))
                                     (equal "enclosingBlock" (symbol-name (car f)))
                                     (equal "theClass" (symbol-name (car f)))))
 				(insert "    os << \"\\n\";\n"
                                        "    if ("(symbol-name (car f))"()) "(symbol-name (car f))"()->print (os, depth + 1); else os << \"NULL\";\n"
					"    os << \"\\n\";\n"))
			   ((or (list-type (cadr f))
				(and (symbolp (cadr f)) (string-match "Node" (symbol-name (cadr f)))))
			    (if (eq (car f) 'destination)
				(insert "    if (destination()->absent()) os << \" Label:-*-\";\n"
                                        "    else os << \" Label:\" << destination();\n")
			      (insert "    os << \"\\n\";\n"
				      (format "    printNode (%s(), os, depth + 1);\n" (car f)))))
			   ((and (stringp (cadr f)) (string-match "Decl\*" (cadr f)))
			    (insert "    os << \" \";\n"
				    (format "    printNode (%s(), os);\n" (basic-field-name (car f)))))
			   (t 
			    (insert (format "    os << \" \" << %s();\n" (car f))))))
	       (insert "}\n")))))
