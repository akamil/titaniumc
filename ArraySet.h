#ifndef _TITANIUM_ARRAY_SET_H_
#define _TITANIUM_ARRAY_SET_H_


#include <set>
#include "ArraySetSort.h"

class TitaniumArrayTypeNode;


typedef set< const TitaniumArrayTypeNode *, ArraySetSort > ArraySet;

extern ArraySet arrayTypes;


#endif // !_TITANIUM_ARRAY_SET_H_
