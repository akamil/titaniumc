#include <iomanip>
#include <iostream>
#include <AST.h>
#include "dump.h"
#include "parse.h"


// An overloaded version of dump() must be provided for each
// attribute defined using "defattr" in "nodetypes.def".


void dump( ostream &sink, unsigned, Common::Modifiers value )
{
  sink << "{ " << stringifyModifiers( value ) << " }";
}


void dump( ostream &sink, unsigned, bool value )
{
  sink << value;
}


void dump( ostream &sink, unsigned, const Literal &value )
{
    sink << setiosflags( ios::showpoint ) << value;
}


void dump( ostream &sink, unsigned, const string &value )
{
  sink << '"' << value << '"';
}


void dump( ostream &sink, unsigned, const string16 &value )
{
  sink << value;
}


void dump( ostream &sink, unsigned depth, const string *value )
{
  if (value == NULL) sink << "NULL";
  else dump( sink, depth, *value );
}


void dump( ostream &sink, unsigned, const TreeNode *node )
{
  if (node == NULL) sink << "NULL";
  else {
    sink << node->oper_name() << ' ' << node << ' ';
    node->position().dump( sink );
  }
}


void SourcePosn::dump( ostream &sink ) const
{
  if (file)
    sink << '"' << *file << '"'
	 << ' ' << posnToLineNumber();
  else
    sink << '@';
}
