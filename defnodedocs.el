(require 'cl)

(defmacro defmethod (name defn)
  (` (setq *methods* (cons (list '(, name) '(, defn)) *methods*))))

(defmacro defattr (name type value)
  (` (setq *attributes* (cons (list '(, name) '(, type) '(, value)) *attributes*))))

(defmacro defattr-default (name type value)
  (` (setq *attributes* (cons (list '(, name) '(, type) '(, value)) *attributes*))))

(defmacro defattr (name type value)
  (` (setq *attributes* (cons (list '(, name) '(, type) '(, value)) *attributes*))))

(defmacro defstate (&rest other) nil)

(defmacro defnode (name parents fields comment methods &optional states)
  (` (defnode* '(, name) '(, parents) (, comment) '(, fields) '(, methods))))

(defun defnode* (name parents comment fields methods)
  (let* ((details (resolve-fields fields))
	 (flds (aref details 2)))
    (goto-char (point-max))
    (insert (format "\n\nClass %s:\n" name))
    (if (not (null parents))
	(let ((p parents))
	  (insert "  Superclasses: ")
	  (while (not (null p))
	    (insert (format "%s%s" (car p) (if (null (cdr p)) "" ", ")))
	    (setq p (cdr p)))
	  (insert "\n")))
    (insert (format "  Constructor:\n    new %s (" name))
    (let ((indent (+ (length (symbol-name name)) 10)))
      (loop for f in flds do
	    (insert (format "%s %s,\n" (to-c++-type (cadr f)) (car f)))
	    (indent-to indent)))
    (insert "SourcePosn posn0 = NoSourcePosition);\n")
    (if (not (null methods))
	(progn 
	  (insert "  Methods:  ")
	  (loop for m on methods do
	       (insert (format "%s%s" (car m) (if (null (cdr m)) "" ", "))))
	  (insert "\n")))
    (if (not (null comment))
	(progn
	  (insert "  Description:\n")
	  (let ((start (point)))
	    (insert comment "\n") 
	    (save-excursion
	      (indent-rigidly start (point) 4)))))))

(defmacro defbasenode (name parents produce-decls-p
			    comment methods &optional states)
  (` (defbasenode* '(, name) '(, parents) (, produce-decls-p) (, comment) '(, methods))))

(defun defbasenode* (name parents produce-decls-p comment methods)
  (goto-char (point-max))
  (insert (format "\n\nBase Class %s:\n" name))
  (if (not (null parents))
      (let ((p parents))
	(insert "  Superclasses: ")
	(while (not (null p))
	  (insert (format "%s%s" (car p) (if (null (cdr p)) "" ", ")))
	  (setq p (cdr p)))
	(insert "\n")))
  (if (not (null methods))
      (progn 
	(insert "  Methods:  ")
	(loop for m on methods do
	      (insert (format "%s%s" (car m) (if (null (cdr m)) "" ", "))))
	(insert "\n")))
  (if (not (null comment))
      (progn
	(insert "  Description:\n")
	(let ((start (point)))
	  (insert comment "\n"
		  "This is a base class, and should not be "
		  "instantiated directly.\n" ) 
	  (save-excursion
	    (indent-rigidly start (point) 4))))))
