#include <iostream>
#include "AST.h"		// ick
#include "CodeContext.h"
#include "optimize.h"


const char *TypeNode::fencePreRead() const
{
  return sequential_consistency && sharing() != Nonshared
    ? "FENCE_PRE_READ();" : "";
}


const char *TypeNode::fencePostRead() const
{
  return sequential_consistency && sharing() != Nonshared
    ? "FENCE_POST_READ();" : "";
}


const char *TypeNode::fencePreWrite() const
{
  return sequential_consistency && sharing() != Nonshared
    ? "FENCE_PRE_WRITE();" : "";
}


const char *TypeNode::fencePostWrite() const
{
  return sequential_consistency && sharing() != Nonshared
    ? "FENCE_POST_WRITE();" : "";
}


////////////////////////////////////////////////////////////////////////


static ostream &codeLine( ostream &out, const char *code )
{
  if (*code) out << code << endCline;
  return out;
}


ostream &TypeNode::fencePreRead( ostream &out ) const
{
  return codeLine( out, fencePreRead() );
}


ostream &TypeNode::fencePostRead( ostream &out ) const
{
  return codeLine( out, fencePostRead() );
}


ostream &TypeNode::fencePreWrite( ostream &out ) const
{
  return codeLine( out, fencePreWrite() );
}


ostream &TypeNode::fencePostWrite( ostream &out ) const
{
  return codeLine( out, fencePostWrite() );
}
