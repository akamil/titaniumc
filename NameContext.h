#ifndef _INCLUDE_NameContext_H_
#define _INCLUDE_NameContext_H_


class TreeNode;
class Environ;


class TreeNode::NameContext // The static analysis context for resolveName
{
public:
  NameContext( bool & );
  NameContext( const NameContext &, Environ * );
  
  Environ *env;
  Decl *currentPackage;
  TypeNode *currentClass;
  Modifiers thisModifiers;
  Modifiers constructorSharingLUB;
  TreeNode *inLoop;
  TreeNode *breakOK;
  bool resolveAsObject;
  Environ *partitionEnv;
  bool &postponed;
};


inline TreeNode::NameContext::NameContext( bool &postponed )
  : env(0),
    currentPackage( 0 ),
    currentClass( 0 ),
    thisModifiers( None ),
    constructorSharingLUB( None ),
    inLoop( 0 ),
    breakOK( 0 ),
    resolveAsObject( true ),
    partitionEnv( 0 ),
    postponed( postponed )
{
}


inline TreeNode::NameContext::NameContext( const NameContext &old, Environ *newEnv )
  : env( newEnv ),
    currentPackage( old.currentPackage ),
    currentClass( old.currentClass ),
    thisModifiers( old.thisModifiers ),
    constructorSharingLUB ( old.constructorSharingLUB ),
    inLoop( old.inLoop ),
    breakOK( old.breakOK ),
    resolveAsObject( old.resolveAsObject ),
    partitionEnv( old.partitionEnv ),
    postponed( old.postponed )
{
}


#endif
