#ifndef INCLUDE_ISSTREAM_H
#define INCLUDE_ISSTREAM_H

#include "ti_config.h"
#include "using-std.h"


#ifdef HAVE_SSTREAM
#include <sstream>
#else // no stringstream classes

#include <string>
#include <strstream>

// This is not intended to be a complete ostringstream implementation.
// It has only the minimal functionality required for use in the
// Titanium compiler, and can be removed once real implementations are
// more widely available.

class istringstream : public istrstream {
public:
  istringstream( const string & );
};


inline istringstream::istringstream( const string &source )
  : istrstream( source.data(), source.length() )
{
}

#endif // no stringstream classes


#endif // !INCLUDE_ISSTREAM_H
