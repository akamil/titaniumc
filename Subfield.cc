#include <iostream>
#include "FieldDecl.h"
#include "Subfield.h"


string Subfield::name() const
{
  string result;
  
  if (parent)
    {
      result = parent->name();
      result += '.';
    }

  result += *decl.name();
  return result;
}
