#include "rqueue.h"
#include <utility>

/* countfrom(n) -> {n, n + 1, n + 2, ... }
   countfrom(n, k) -> {n, n + k, n + 2k, ... }

   zero_upto(n) -> {0, 1, ..., n}

   sum(k, {a0, a1, ...}) -> {k + a0, k + a1, ... }
   product(k, {a0, a1, ...}) -> {k * a0, k * a1, ... }
*/

static inline bool sum_(void *& x, queue<int> *q)
{
  pair< rqueue<int> *, int > *p = (pair< rqueue<int> *, int > *) x;
  if (p->first->empty())
    return false;
  q->push(p->first->pop() + p->second);
  return true;
}

static inline rqueue<int> * sum(int k, rqueue<int> *a)
{
  return new rqueue<int>(&sum_, (void *) new pair< rqueue<int> *, int >(a, k));
}

static inline bool zero_upto_(void *& x, queue<int> *q)
{
  pair< int, int > *p = (pair< int, int > *) x;
  if (p->first > p->second)
    return false;
  q->push(p->first++);
  return true;
}

static inline rqueue<int> * zero_upto(int n)
{
  return new rqueue<int>(zero_upto_, (void *) new pair< int, int >(0, n));
}

static inline bool product_(void *& x, queue<int> *q)
{
  pair< rqueue<int> *, int > *p = (pair< rqueue<int> *, int > *) x;
  if (p->first->empty())
    return false;
  q->push(p->first->pop() * p->second);
  return true;
}

static inline rqueue<int> * product(int k, rqueue<int> *a)
{
  return new rqueue<int>(&product_,
			 (void *) new pair< rqueue<int> *, int >(a, k));
}

static inline bool countfrom_(void *& x, queue<int> *q)
{
  int i = (int) x;
  for (int j = 0; j < 10; j++)
    q->push(i++);
  x = (void *) i;
  return true;
}

static inline rqueue<int> * countfrom(int n)
{
  return new rqueue<int>(&countfrom_, (void *) n);
}

static inline rqueue<int> * countfrom(int i, int step)
{
  return sum(i, product(step, countfrom(0)));
}

