#include "Ty.h"
#include <map>
#include <cassert>

typedef map< string, Ty * > map_string_to_Ty;

static Ty *memo_Ty(string s, int n)
{
  static map_string_to_Ty memo;
  Ty *& result = memo[s];
  if (result != NULL)
    return result;
  else
    return (result = new Ty(new string(s), n));
}

Ty *RectDomain_Ty(int n) {
  return memo_Ty(string("RectDomain") + '<' + i2s(n) + '>', n);
}

Ty *Domain_Ty(int n) {
  return memo_Ty(string("Domain") + '<' + i2s(n) + '>', n);
}

static Ty xivsetiTy("ivseti *");
static Ty xsetiTy("seti *");
static Ty xpsetiTy("seti **");
static Ty xppsetiTy("seti ***");
static Ty xpivsetiTy("ivseti **");
static Ty xdoubleTy("double");
static Ty xfloatTy("float");
static Ty xintTy("int");
static Ty xpintTy("int *");
static Ty xppintTy("int **");
static Ty xboolTy("bool");

Ty *Ty::setiTy = &xsetiTy;
Ty *Ty::psetiTy = &xpsetiTy;
Ty *Ty::ppsetiTy = &xppsetiTy;
Ty *Ty::ivsetiTy = &xivsetiTy;
Ty *Ty::pivsetiTy = &xpivsetiTy;
Ty *Ty::doubleTy = &xdoubleTy;
Ty *Ty::floatTy = &xfloatTy;
Ty *Ty::intTy = &xintTy;
Ty *Ty::pintTy = &xpintTy;
Ty *Ty::ppintTy = &xppintTy;
Ty *Ty::boolTy = &xboolTy;

Ty *Ty::rectTy(int n)
{
  static map<int, Ty *> m;
  Ty *& result = m[n];
  if (result == NULL)
    result = new Ty("rect" + i2s(n));
  return result;
}

Ty *Ty::prectTy(int n)
{
  static map<int, Ty *> m;
  Ty *& result = m[n];
  if (result == NULL)
    result = new TyPtr(rectTy(n));
  return result;
}

Ty *indexed_type(const Ty *t, bool strict)
{
  if (t == Ty::psetiTy)
    return Ty::setiTy;

  if (t == Ty::ppsetiTy)
    return Ty::psetiTy;

  if (t == Ty::pintTy)
    return Ty::intTy;

  if (t == Ty::ppintTy)
    return Ty::pintTy;

  if (t == Ty::pivsetiTy)
    return Ty::ivsetiTy;

  if (strict)
    assert(0 && "indexed_type() failed");
  return NULL;
}

bool has_indexed_type(const Ty *t)
{
  return t->indexed_type(false) != NULL;
}
