#ifndef _PIKE_LINALG_H_
#define _PIKE_LINALG_H_

#include "TilingPlane.h"

int positive_gcd(int a, int b);
bool is_linearly_indep(llist<intvector *> *l);
bool is_linearly_indep(llist<TilingPlane *> *l);
bool is_singular(double **a, int n);

double **new_rectangular_matrix(int n, int m);
double **new_square_matrix(int n);
double **copy_square_matrix(double **a, int n);
double **copy_rectangular_matrix(double **a, int n, int m);
void free_rectangular_matrix(double **a, int n, int m);
void free_square_matrix(double **a, int n);

intvector *multiple_with_approximate_length(const intvector *v, int len);
intvector *approximate_vector_with_length(const intvector *v, int len,
					  llist<intvector *> *exclude = NULL);
intvector *find_vector_perp_to_all(llist<TilingPlane *> *l, int exclude = -1);
intvector *find_vector_perp_to_all(llist<intvector *> *l, int exclude = -1);
intvector **multiple_of_inverse(llist<intvector *> *l, bool transpose = false,
				int *k = NULL);

void test_linalg();
void print_matrix(double **a, int n, int m);
void print_matrix(int **a, int n, int m);
void print_matrix0(int **a, int n, int m);

#endif // _PIKE_LINALG_H_

