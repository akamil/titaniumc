#ifndef _PARAMETER_H_
#define _PARAMETER_H_

#define NORMAL 0
#define NONNEG 1
#define BOOL 2

#define STOPTIFU_NO 0
#define STOPTIFU_MAYBE 11
#define STOPTIFU_YES 22

int get_parameter(string p, int kind = NONNEG);
int get_parameter(string p, string desc, int kind = NONNEG, int def = 0, bool write = true);
int get_parameter_in_range(string p, string desc, int lo, int hi,
			   bool write = true);
int get_parameter_in_range(string p, string desc, int lo, int hi, int def,
			   bool write = true);
bool get_bool_parameter(string p, bool d = false);
bool get_bool_parameter(string p, string desc, bool d);
int get_parameter_without_prefix(string p, int kind = NONNEG,
				 bool write = true);
int get_parameter_without_prefix(string p, string desc, int kind = NONNEG,
				 int def = 0, bool write = true);
bool get_bool_parameter_without_prefix(string p, bool d = false);
bool get_bool_parameter_without_prefix(string p, string desc, bool d = false, bool write = true);

string & parameter_description(string p);

void push_parameter_prefix(string s, bool must_be_fresh = false);
string pop_parameter_prefix();
void set_parameter_prefix(string s, bool must_be_fresh = false);
string get_parameter_prefix();

void parameter_warn(string p, string warning);
void parameter_error(string error, bool fatal = true);

void parameter_parse_args(int &argc, char **argv);

#endif // _PARAMETER_H_
