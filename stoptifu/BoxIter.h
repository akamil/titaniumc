#ifndef _BOXITER_H_
#define _BOXITER_H_

#include "intvector.h"
#include <cassert>

class BoxIter {
 public:
  BoxIter(intvector *lo_, intvector *hi_) : lo(lo_), hi(hi_), n(lo_->size()) {
    assert(n == hi->size());
    bool empty = false;
    for (int i = ((int) n) - 1; !empty && i >= 0; i--)
      if ((*lo)[i] > (*hi)[i])
	empty = true;
    c = empty ? NULL : lo->copy();
  }
  intvector const * operator* () const { return c; }
  bool is_done() const { return c == NULL; }
  void next() {
    for (int bump_dim = ((int) n) - 1; bump_dim >= 0; bump_dim--)
      if (++((*c)[bump_dim]) > (*hi)[bump_dim]) 
	(*c)[bump_dim] = (*lo)[bump_dim];
      else
	return;
    c = NULL;
  }
    
 private:
  intvector const *lo;
  intvector const *hi;
  intvector *c;
  size_t const n;
};

#endif // _BOXITER_H_
