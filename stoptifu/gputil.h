#ifndef _GPUTIL_H_
#define _GPUTIL_H_

#include "using-std.h"
#include <map>

static inline int square(int x) { return x * x; }
/* static inline int abs(int a) { return ((a >= 0) ? a : -a); } */

bool is_all_whitespace(const char *s);
bool is_all_digits(const char *s);
char *trim(char *s);

bool snaffle_from_args(char *s, char *&v, int &argc, char **argv,
		       int limit = -1);
void showargs(int argc, char **argv);
void remove_args(int &argc, char **argv, int start, int end);
void remove_arg(int &argc, char **argv, int n);
void remove_arg0(int argc, char **argv);
char *readline(istream &s, int *n = NULL);
char *read(istream &s, char t, int *n = NULL);
int findlast(char c, char *s, int n = -1);

string no_spaces(const string &s);

#define generic template <class T>

generic void bump_all_in_range(map<int, T> &m, int lo, int hi, T bump)
{
  while (lo <= hi)
    m[lo++] += bump;
}

extern void bump_all_in_range(map<int, int> &m, int lo, int hi, int bump = 1);

template <class T, class U> U max_second(map<T, U> const &m)
{
  U result;
  result = U();
  if (! m.empty ()) {
    TYPENAME map<T, U>::const_iterator i = m.begin();
    result = i->second;
    for (i++; i != m.end (); i++) {
      if (i->second > result)
	result = i->second;
    }
  }
  return result;
}

template <class T, class U> T max_first(map<T, U> const &m)
{
  T result;
  bool first = true;
  for (TYPENAME map<T, U>::const_iterator i = m.begin(); i != m.end(); ++i)
    if (first) {
      result = (*i).first;
      first = false;
    } else if ((*i).first > result)
      result = (*i).first;
  return result;
}

template <class T, class U> U min_second(map<T, U> const &m)
{
  U result;
  bool first = true;
  for (TYPENAME map<T, U>::const_iterator i = m.begin(); i != m.end(); ++i)
    if (first) {
      result = (*i).second;
      first = false;
    } else if ((*i).second < result)
      result = (*i).second;
  return result;
}

template <class T, class U> T min_first(map<T, U> const &m)
{
  T result;
  bool first = true;
  for (TYPENAME map<T, U>::const_iterator i = m.begin(); i != m.end(); ++i)
    if (first) {
      result = (*i).first;
      first = false;
    } else if ((*i).first < result)
      result = (*i).first;
  return result;
}

#undef generic

#endif // _GPUTIL_H_

