#ifndef _INT2STRING_H_
#define _INT2STRING_H_

#include <cstdio>
#include <string>
#include "using-std.h"

static inline string i2s(int val)
{
  static char buffer[64];
  sprintf(buffer, "%d", val);
  return string(buffer);
}

static inline string b2s(bool val)
{
  return i2s(val ? 1 : 0);
}

#endif // _INT2STRING_H_
