#ifndef _ACPRINTING_H_
#define _ACPRINTING_H_

static inline void indent2(ostream &o, int n) { while (n-- > 0) o << "  "; }

#endif // _ACPRINTING_H_
