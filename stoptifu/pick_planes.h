#ifndef _PICK_PLANES_H_
#define _PICK_PLANES_H_

#include "AC.h"

bool is_legal(Foreach *f, llist<OrderingConstraint *> *oc, intvector *n);
bool pick_plane(Foreach *f, llist<OrderingConstraint *> *oc,
		int dd, TilingPlane *&p);

#endif /* _PICK_PLANES_H_ */
