#ifndef _userdata_H_
#define _userdata_H_

#include <string>
#include "using-std.h"

extern const string *chars2string(void *u);
extern const string *string2string(void *u);
extern void *userdata_identity(void *u);

#endif /* _userdata_H_ */
