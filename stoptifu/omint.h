#ifndef _OMINT_H_
#define _OMINT_H_

static inline string relation_to_str(Relation R) {
  return string((const char *) R.print_formula_to_string());
}

Relation eval_relation_at_v(Relation r, intvector const *v);
Relation eval_relation_at_zero(Relation r);

void printrel(Relation R, const string &s, bool simp = true);
void show_subset_failure(Relation a, Relation b);

bool dep_violated(Relation const *been_done, Relation const *r);
Relation needed_by(Relation const *been_done, Relation const *r);
Relation ready_wrt_dep(Relation S, Relation D);

Relation *emptyset(int n);
Relation *wholespace(int n);
Relation lexicographically_negative(int n);
Relation *halfspace(intvector const *n, int q = 0);
Relation *antihalfspace(intvector const *n, int q = 0);
Relation *generic_halfspace(intvector const *n);

Relation *halfline(intvector const *n, intvector const *z, int q = 0);
Relation *antihalfline(intvector const *n, intvector const *z, int q = 0);
Relation *wholeline(intvector const *n, intvector const *z);

bool find_nonneg_q_to_cover_halfline(Relation const *r, intvector const *n,
				     intvector const *z, int &q);
bool find_q_in_range_to_cover_halfline(Relation const *r, int lo, int hi,
				       intvector const *n,
				       intvector const *z, int &q);

bool set_contains_intvector(Relation s, intvector const *v);
Relation *intvector_to_singleton_set(intvector const *n);
intvector *singleton_set_to_intvector(Relation r);
string singleton_set_to_string(Relation r, bool force = false);
bool is_singleton_set(Relation s, intvector **v = NULL);

Relation apply_to_generic(Relation r, llist<Free_Var_Decl *> *g);

Relation translate_by_weighted_vectors(Relation s, Free_Var_Decl **a,
				       llist<intvector *> *v);

Relation remove_from_set(Relation s, llist<intvector *> *l);
Relation translate_set(Relation s, intvector const *v);
Relation translate_domain(Relation s, intvector const *v);
Relation translate_range(Relation s, intvector const *v);
Relation translate_relation(Relation s, intvector const *v, int n = -1);
Relation identity_relation(int n);
Relation dreverse(Relation s);
Relation negate_ith_var(Relation s, int i);
Relation preceding(Relation z, bool include_z = false);
Relation halflines(Relation z, bool include_z = false);
Relation last(Relation z);
Relation keep_last_per_bundle(Relation z);

bool set_to_llist(Relation s, llist<intvector *> *&result);
Relation llist_to_set(llist<intvector *> *l, int n = 0);
bool is_finite(Relation s);
int size_of_finite_set(Relation s);
bool same_relation(Relation r, Relation s);
bool exists_range_overlap(Relation r1, Relation r2);

void test_omint();

#endif // _OMINT_H_
