#ifndef _INDIRECT_OMEGA_H_
#define _INDIRECT_OMEGA_H_

#undef foreach
#undef foreach_const
#if 0
/* DOB: don't know what this hack was supposed to accomplish, but it
 * breaks the STL on Cray C++ and possibly other compilers
 */
#define swap OMEGA_swap
#include <include/omega.h>
#undef swap
#else
#include <include/omega.h>
#endif

#undef foreach
#undef foreach_const

#define foreach(P, TYPE, COLLECTION)  \
    for (TYPE::iterator P = (COLLECTION).begin(); \
         P != (COLLECTION).end(); \
         P++) 

#define foreach_const(P, TYPE, COLLECTION) \
    for (TYPE::const_iterator P = (COLLECTION).begin(); \
         P != (COLLECTION).end(); \
         P++) 

#endif
