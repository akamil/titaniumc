#ifndef _GAUSS_JORDAN_H_
#define _GAUSS_JORDAN_H_

#define REAL double

/* Exception that is thrown when gauss-jordan fails. */
class SingularMatrix {};

void gauss_jordan(REAL **a, int n, REAL **b, int m,
		  double *product_of_pivots = 0); 

#endif // _GAUSS_JORDAN_H_
