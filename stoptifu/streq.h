#ifndef _STREQ_H_
#define _STREQ_H_

#include <cstring>

#define streq(a, b) (strcmp((a), (b)) == 0)

#endif // _STREQ_H_
