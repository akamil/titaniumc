#ifndef _TILINGPLANE_H_
#define _TILINGPLANE_H_

#include "intvector.h"

class TilingPlane {
 public:
  TilingPlane(intvector *x) : normal(x) {}
  intvector const *n() const { return normal; }
 private:
  intvector *normal;
};

#endif /* _TILINGPLANE_H_ */
