#ifndef _ACvar_H_
#define _ACvar_H_

#include "Ty.h"
#include "llist.h"

class ACvar
{
public:
  ACvar(const char *s, const Ty *T) :
    t(T), userdata((void *) s), stringify(&chars2string), alternate(NULL) {}
  ACvar(const string *s, const Ty *T) :
    t(T), userdata((void *) s), stringify(&string2string), alternate(NULL) {}
  ACvar(const string s, const Ty *T) : 
    t(T), userdata((void *) new string(s)), stringify(&string2string),
    alternate(NULL) {}
  ACvar(void *userdata, const string * (*stringify)(void *userdata),
	void * (*alternate)(void *userdata), const Ty *T) : 
    t(T), userdata(userdata), stringify(stringify), alternate(alternate) {}

  inline const string *to_pstring() const { return (*stringify)(userdata); }
  inline const string &to_string() const { return *to_pstring(); }
  void *alternate_translation() const {
    return (alternate != NULL) ? (*alternate)(userdata) : NULL;
  }
  void print(ostream &o) const { o << to_string(); }
  inline Ty const *type() const { return t; }
  inline Ty const *indexed_type() const { return t->indexed_type(); }
  inline int arity() const { return t->arity(); }
  ACvar *field(const string s, const Ty *T) const {
    return new ACvar(new string(to_string() + '.' + s), T);
  }
  ACvar *index(const string s, const Ty *T = NULL) const {
    if (T == NULL)
      T = t->indexed_type();
    return new ACvar(new string(to_string() + '[' + s + ']'), T);
  }
  ACvar *index(const ACvar *i, const Ty *T = NULL) const {
    return index(i->to_string(), T);
  }
  ACvar *index(int i, const Ty *T = NULL) const { return index(i2s(i), T); }

private:
  const Ty *t;
  void *userdata;
  const string * (*stringify)(void *userdata);
  void * (*alternate)(void *userdata);
};

#endif /* _ACvar_H_ */
