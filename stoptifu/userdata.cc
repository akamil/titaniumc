#include "userdata.h"

const string *chars2string(void *u)
{
  char *s = (char *) u;
  return new string(s);
}

const string *string2string(void *u)
{
  return (const string *) u;
}

void *userdata_identity(void *u) {
  return u;
}
