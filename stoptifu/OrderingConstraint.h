#ifndef _OrderingConstraint_H_
#define _OrderingConstraint_H_

/* Assumes various other classes have been declared.  Include AC.h first. */

class OrderingConstraint
{
public:
  enum Kind { RAW, WAR, WAW, Unknown, Other };
  Kind k;

  /* "from" must precede "to" */
  int fromseq, fromseqindex;
  AC *from;

  int toseq, toseqindex;
  AC *to;

  void print(ostream &os);

  static void print(Kind k, ostream &os)
  {
    switch (k) {
    case RAW: os << "RAW"; break;
    case WAW: os << "WAW"; break;
    case WAR: os << "WAR"; break;
    case Unknown: os << "Unknown"; break;
    case Other: os << "Other"; break;
    default: fatal("unknown type of OrderingConstraint");
    }
  }

  virtual Relation const * R() { fatal_error(""); return 0; }
  Kind kind() const { return k; }
  virtual void normalize() {}

  // OrderingConstraint(Relation const *R);

  OrderingConstraint(AC *from_, AC *to_, Kind k_ = Unknown);
};

class Dep : public OrderingConstraint
{
 private:
  Relation const *r;

 public:
  Dep(AC *from, AC *to, Relation const *r_, Kind k) :
    OrderingConstraint(from, to, k), r(r_) {}

  Relation const * R() { return r; }
  void normalize();
};

void print(llist<OrderingConstraint *> *l, ostream &os);
llist<OrderingConstraint *> *normalize(llist<OrderingConstraint *> *l);

bool some_oc_violated_by_generic_plane(Foreach *f,
				       llist<OrderingConstraint *> *oc,
				       intvector *n);

Relation needed_by(Relation const *been_done,
		   llist<OrderingConstraint *> *l, int n);

llist<OrderingConstraint *> *
filter_OrderingConstraints(llist<OrderingConstraint *> *oc, AC *f, AC *t);

Relation compute_ready_set(Relation been_done,
			   llist<OrderingConstraint *> *oc);

Relation remove_nodes_that_violate_self_deps(Relation x,
					     llist<OrderingConstraint *> *oc);

bool no_oc_violated(llist<OrderingConstraint *> *oc, Relation been_done, intvector *p);


#endif 

