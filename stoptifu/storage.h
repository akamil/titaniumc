#ifndef _STOPTIFU_STORAGE_H_
#define _STOPTIFU_STORAGE_H_

/* from storage.cc */
extern void storage_opt(llist<Megatile *> *, smap &, rwmap &,
			bool, bool, bool, bool,
			string (*_arrayname_fn)(int) = NULL);

ACvar *backup_by_n(const string &s, size_t n, Ty *t = NULL, Block *b = NULL);
int maxdist_count(const intvector *v);
int maxdist_to_array_dim(const intvector *v);


// (intvector v in tile space, int k, smap_element *e)
class st_source {
 public:
  st_source(intvector *v, int k, smap_element *e) :
    v(v), k(k), e(e) {}

  string to_string() const {
    return '<' + v->to_string() + ", " + i2s(k) + ", " + e->to_string() + '>';
  }

  intvector *v;
  int k;
  smap_element *e;
};

#endif // _STOPTIFU_STORAGE_H_
