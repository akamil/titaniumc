#ifndef _STOPTIFU_L2S_H_
#define _STOPTIFU_L2S_H_

#include "llist.h"

/* list to string */
template <class T> string l2s(llist<T> *l) {
  string result("{");
  bool first = true;
  while (l != NULL) {
    if (first)
      first = false;
    else
      result += ", ";
    result += string(l->front());
    l = l->tail();
  }
  return result + "}";
}

#endif
