#ifndef _INCLUDE_CODE_COMMONSUB_H_
#define _INCLUDE_CODE_COMMONSUB_H_

extern bool debug_commonsub;

#define COMPARE(foo, bar)					\
(								\
  (foo)->compare(bar) ?						\
  ((debug_commonsub ? (						\
		       cout << "\tapparently identical:\n",	\
		       (foo)->print(cout, 12),			\
		       cout << "\n\tand:\n",			\
		       (bar)->print(cout, 12),			\
		       cout << '\n',				\
		       0		      ) : 0),		\
   true) :							\
  ((debug_commonsub ? (						\
		       cout << "\tapparently different:\n",	\
		       (foo)->print(cout, 12),			\
		       cout << "\n\tand:\n",			\
		       (bar)->print(cout, 12),			\
		       cout << '\n',				\
		       0		      ) : 0),		\
   false)							\
)

typedef pair<TreeNode *, TreeNode *> treepair;

bool compareLists(const llist<TreeNode *> *t, const llist<TreeNode *> *u);
bool compareLists(const llist<treepair> *t, const llist<treepair> *u);

#endif /* _INCLUDE_CODE_COMMONSUB_H_ */
