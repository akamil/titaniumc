#ifndef DEBUG_MEMORY_H
#define DEBUG_MEMORY_H

#ifndef DEBUG_MEMORY_USAGE
#define PASS_RETADR
#else

#include <cstdlib>
#include <new>
void *operator new(size_t s);
void *operator new[](size_t s);
void *operator new(size_t s, unsigned long retadr);
void *operator new[](size_t s, unsigned long retadr);

/* For use in new constructors where caller is more interesting:
   write new PASS_RETADR ...
   Note that the use of __builtin_return_address(1) should make this
   unnecessary, but it seems to be somewhat unreliable (weird?)
*/
#define PASS_RETADR (RETADR1())

#include "debug_memory_c.h"
#endif

#endif
