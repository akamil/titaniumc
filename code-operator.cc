#define __SINST__		// g++ header bug
#include <cassert>
#include <map>
#include <string>
#include <ctype.h>
#include "code-operator.h"
#include "code.h"
#include "using-std.h"


typedef pair< const string, string > Pair;

static const Pair Table[] = {
  Pair( "!", "EX" /* logicalComplement */ ),
  Pair( "~", "TI" /* bitwiseComplement */ ),
  Pair( "<", "LE" /* less */ ),
  Pair( ">", "GT" /* greater */ ),
  Pair( "<=", "LEEQ" /* lessEqual */ ),
  Pair( ">=", "GTEQ" /* greaterEqual */ ),
  Pair( "==", "EQEQ" /* equality */ ),
  Pair( "!=", "EXEQ" /* inequality */ ),
  Pair( "+", "PL" /* addition */ ),
  Pair( "-", "MI" /* subtraction */ ),
  Pair( "*", "ST" /* multiplication */ ),
  Pair( "/", "SL" /* division */ ),
  Pair( "&", "AN" /* bitwiseAnd */ ),
  Pair( "|", "BA" /* bitwiseOr */ ),
  Pair( "^", "CA" /* bitwiseXor */ ),
  Pair( "%", "PE" /* remainder */ ),
  Pair( "<<", "LELE" /* leftShift */ ),
  Pair( ">>", "GTGT" /* signedRightShift */ ),
  Pair( ">>>", "GTGTGT" /* unsignedRightShift */ ),
  Pair( "+=", "PLEQ" /* additionAssign */ ),
  Pair( "-=", "MIEQ" /* subtractionAssign */ ),
  Pair( "*=", "STEQ" /* multiplicationAssign */ ),
  Pair( "/=", "SLEQ" /* divisionAssign */ ),
  Pair( "%=", "PEEQ" /* remainderAssign */ ),
  Pair( "<<=", "LELEEQ" /* leftShiftAssign */ ),
  Pair( ">>=", "GTGTEQ" /* signedRightShiftAssign */ ),
  Pair( ">>>=", "GTGTGTEQ" /* unsignedRightShiftAssign */ ),
  Pair( "&=", "ANEQ" /* bitwiseAndAssign */ ),
  Pair( "^=", "CAEQ" /* bitwiseXorAssign */ ),
  Pair( "|=", "BAEQ" /* bitwiseOrAssign */ ),
  Pair( "[]", "OBCB" /* arrayAccess */ ),
  Pair( "[]=", "OBCBEQ" /* arrayAccessAssign */ )
};

enum { TableCount = sizeof( Table ) / sizeof( *Table ) };

typedef map< string, string, less< string > > Map;
static const Map Translation( &Table[ 0 ], &Table[ TableCount ] );


const string encodedMethodName( const string &sourceName,
				const string &nspace )
{
  // It would be simpler to look up *every* name in the translation
  // table, but for speed we want to short-circuit the common case.
  
  if (isalpha( sourceName[ 0 ] )
      || sourceName[ 0 ] == '_'
      || sourceName[ 0 ] == '$')
    return MANGLE_METH_HEAD(+, nspace, sourceName);
  
  else {
    const Map::const_iterator lookup = Translation.find( sourceName );
    assert( lookup != Translation.end() );
    return MANGLE_OP_HEAD(+, (*lookup).second);
  }
}
