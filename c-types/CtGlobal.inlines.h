#ifndef _include_CtGlobal_inlines_h_
#define _include_CtGlobal_inlines_h_


#include "CtGlobal.h"
#include "../code.h"


inline CtGlobal::CtGlobal( const CtType &referent )
  : CtReference( MANGLE_TYPE_GLOBAL_MARKER, referent )
{
}


#endif // !_include_CtGlobal_inlines_h_


// Local Variables:
// c-file-style: "gnu"
// End:
