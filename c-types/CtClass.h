#ifndef _include_CtClass_h_
#define _include_CtClass_h_


#include <string>
#include "CtStruct.h"


class ClassDecl;
class FieldDecl;
class MethodDecl;


class CtClass : public CtStruct {
protected:
  CtClass( const string &, ClassDecl & );

  void dependFields( CtRegistry & ) const;
  bool defineFields( ostream & ) const;

  virtual void dependHeaderFields( CtRegistry & ) const;
  virtual bool defineHeaderFields( ostream & ) const;

  virtual void dependMembers( ClassDecl &, CtRegistry & ) const;
  virtual bool defineMembers( ClassDecl &, ostream & ) const;

  ClassDecl &basis;
};


#endif // !_include_CtClass_h_


// Local Variables:
// c-file-style: "gnu"
// End:
