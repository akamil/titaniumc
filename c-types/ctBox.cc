#include "CtGlobal.h"
#include "CtLocal.h"
#include "ctBox.h"


CtReference &ctBox( const CtType &referent, bool isLocal )
{
  if (isLocal) {
    return *new CtLocal( referent );
  } else {
    return *new CtGlobal( referent );
  }
}


// Local Variables:
// c-file-style: "gnu"
// End:
