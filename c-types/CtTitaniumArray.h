#ifndef _include_CtTitaniumArray_h_
#define _include_CtTitaniumArray_h_


#include "CtArray.h"


class CtInstance;
class CtReference;


class CtTitaniumArray : public CtArray {
public:
  CtTitaniumArray( const CtReference &, unsigned, bool );

  const CtReference &data;
  const CtType &domain;
  
protected:
  void elaborateTo( CfLayout & ) const;

private:
  static const string mangle( const CtReference &, unsigned );
  
  const unsigned arity;
  const bool isLocal;
};


#endif // !_include_CtTitaniumArray_h_


// Local Variables:
// c-file-style: "gnu"
// End:
