#ifndef _include_CtDefined_inlines_h_
#define _include_CtDefined_inlines_h_


#include "CtDefined.h"


inline CtDefined::CtDefined( const string &name )
  : CtType( name )
{
}


#endif // !_include_CtDefined_inlines_h_


// Local Variables:
// c-file-style: "gnu"
// End:
