#include "../code-files/CfCode.h"
#include "../code-files/CfLayout.h"
#include "CtGlobal.h"


void CtGlobal::declare( CfFile &out ) const
{
  out << "#ifdef WIDE_POINTERS\n";
  CtDefined::declare( out );
  out << "#else\n";
  define( out );
  out << "#endif\n";
}


void CtGlobal::elaborateTo( CfLayout &out ) const
{
  CtReference::elaborateTo( out );
  
  out << "#ifdef WIDE_POINTERS\n"
      << "struct " << *this << " {\n"
      << "  Box box;\n"
      << "  " << referent << " *addr;\n"
      << "};\n";
  CtDefined::declare( out );
  out << "#else\n"
      << "typedef " << referent << " *" << *this << ";\n"
      << "#endif\n";
}


const char *CtGlobal::titaniumArrayPrefix() const
{
  return MANGLE_TYPE_GLOBAL_MARKER;
}


// Local Variables:
// c-file-style: "gnu"
// End:
