#include "../code-files/CfLayout.h"
#include "CtReference.h"


CtReference::CtReference( const char prefix[], const CtType &referent )
  : CtDefined( prefix + referent ),
    referent( referent )
{
}


void CtReference::elaborateTo( CfLayout &out ) const
{
  referent.declare( out );
}


// Local Variables:
// c-file-style: "gnu"
// End:
