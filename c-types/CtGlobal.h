#ifndef _include_CtGlobal_h_
#define _include_CtGlobal_h_


#include "CtReference.h"


class CtGlobal : public CtReference {
public:
  explicit CtGlobal( const CtType & );

  void declare( CfFile & ) const;
  void elaborateTo( CfLayout & ) const;
  
  const char *titaniumArrayPrefix() const;
};


#include "CtGlobal.inlines.h"


#endif // !_include_CtGlobal_h_


// Local Variables:
// c-file-style: "gnu"
// End:
