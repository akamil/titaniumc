#include <fstream>
#include "../IncludeOnce.h"
#include "../code-files/CfCode.h"
#include "../code-files/CfLayout.h"
#include "../code.h"
#include "CtDefined.h"
#include "../code-util.h"

void CtDefined::declare( CfFile &out ) const
{
  out << "#ifndef _typedef_" << *this << '\n'
      << "#define _typedef_" << *this << '\n'
      << "typedef struct " << *this << ' ' << *this << ";\n"
      << "#endif\n";
}


void CtDefined::define( CfFile &out ) const
{
  out.include( CfLayout::deriveName( *this ) );
}


void CtDefined::elaborate() const {
  if (!codeGen_libs) { // check whether the header already exists in tlib-include
    const string myname = CfLayout::deriveName( *this ) + ".h";
    if (CfLayout::tlib_include_files.count(myname)) {
      compile_status(3, string("omitting creation of duplicate header: ") + myname); 
      return;
    }
  }
  CfLayout out( *this );
  elaborateTo( out );
}


// Local Variables:
// c-file-style: "gnu"
// End:
