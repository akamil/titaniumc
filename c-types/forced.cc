// Creation of certain C types must be forced.  These are types that
// do not natually arise during code generation, but which are
// required by native code.
//
// The set of forced types should be kept to a minimum to avoid
// needless bloat.  Often, native code can simply call back up to
// Titanium code to get its important work done.  This way, the
// necessary types will simply be produced implicitly.  See
// ti.lang.NativeUtils for some utilities called from native code.
//
// If you must force a type, please clearly identify the code that
// uses it, so that it can be easily checked and removed later should
// it become superfluous.


#include "CtExternal.h"
#include "CtObjectDescriptor.h"
#include "CtGlobal.h"


// This type is not really forced, but is defined here to control the
// order of instantiation relative to globalClassHeader, below.

CtExternal CtObjectDescriptor::classHeader( "class_header" );


// Type used by checkIsArray(), which in turn supports native
// implementation of java.lang.System.arraycopy().

CtGlobal globalClassHeader( CtObjectDescriptor::classHeader );


// Type used by getClass(), which in turn supports several dynamic
// type checking routines.

CtLocal localClassHeader( CtObjectDescriptor::classHeader );
CtGlobal globalLocalClassHeader( localClassHeader );
