#ifndef _include_CtMonitor_h_
#define _include_CtMonitor_h_


#include "CtExternal.h"


class CtMonitor : public CtExternal {
public:
  void define( CfFile & ) const;

  static const CtMonitor singleton;

private:
  CtMonitor();
};


#include "CtMonitor.inlines.h"


#endif // !_include_CtMonitor_h_


// Local Variables:
// c-file-style: "gnu"
// End:
