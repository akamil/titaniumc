#ifndef _include_CtPrimitive_inlines_h_
#define _include_CtPrimitive_inlines_h_


#include "CtPrimitive.h"


inline CtPrimitive::CtPrimitive( const string &name )
  : CtExternal( name )
{
}


#endif // !_include_CtPrimitive_inlines_h_


// Local Variables:
// c-file-style: "gnu"
// End:
