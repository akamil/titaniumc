#include "../code-files/CfLayout.h"
#include "CtJavaArray.h"
#include "../code.h"


CtJavaArray::CtJavaArray( const CtType &elements )
  : CtArray( string(MANGLE_TYPE_MARKER) +
	     MANGLE_JAVA_ARRAY_TYPE_MARKER + elements, elements )
{
}


void CtJavaArray::elaborateTo( CfLayout &out ) const
{
  out << "#include <java_array_header.h>\n";  
  elements.define( out );

  out << "struct " << *this << " {\n"
      << "  java_array_header header;\n"
      << "  " << elements << " data[1];\n"
      << "};\n";

  declare( out );
}


// Local Variables:
// c-file-style: "gnu"
// End:
