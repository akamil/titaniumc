#include "../code-files/CfLayout.h"
#include "CtLocal.h"


void CtLocal::declare( CfFile &out ) const
{
  define( out );
}


void CtLocal::elaborateTo( CfLayout &out ) const
{
  CtReference::elaborateTo( out );
  
  out << "typedef " << referent << " *" << *this << ";\n";
}


const char *CtLocal::titaniumArrayPrefix() const
{
  return MANGLE_TYPE_LOCAL_MARKER;
}


// Local Variables:
// c-file-style: "gnu"
// End:
