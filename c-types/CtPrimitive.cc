#include "../code-files/CfFile.h"
#include "CtPrimitive.h"


void CtPrimitive::define( CfFile &out ) const
{
  out << "#include <primitives.h>\n";
}


// Local Variables:
// c-file-style: "gnu"
// End:
