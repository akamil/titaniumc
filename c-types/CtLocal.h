#ifndef _include_CtLocal_h_
#define _include_CtLocal_h_


#include "CtReference.h"


class CtLocal : public CtReference {
public:
  explicit CtLocal( const CtType & );

  void declare( CfFile & ) const;
  void elaborateTo( CfLayout & ) const;

  const char *titaniumArrayPrefix() const;
};


#include "CtLocal.inlines.h"


#endif // !_include_CtLocal_h_


// Local Variables:
// c-file-style: "gnu"
// End:
