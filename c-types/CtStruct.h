#ifndef _include_CtStruct_h_
#define _include_CtStruct_h_


#include "CtDefined.h"


class CtRegistry;


class CtStruct : public CtDefined {
protected:
  explicit CtStruct( const string & );

  void elaborateTo( CfLayout & ) const;

  virtual void dependFields( CtRegistry & ) const = 0;
  virtual bool defineFields( ostream & ) const = 0;
};


#include "CtStruct.inlines.h"


#endif // !_include_CtStruct_h_


// Local Variables:
// c-file-style: "gnu"
// End:
