#ifndef _include_CtPrimitive_h_
#define _include_CtPrimitive_h_


#include "CtExternal.h"


class CtPrimitive : public CtExternal {
public:
  explicit CtPrimitive( const string & );

  void define( CfFile & ) const;
};


#include "CtPrimitive.inlines.h"


#endif // !_include_CtPrimitive_h_


// Local Variables:
// c-file-style: "gnu"
// End:
