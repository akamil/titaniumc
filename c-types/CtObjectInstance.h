#ifndef _include_CtObjectInstance_h_
#define _include_CtObjectInstance_h_


#include <string>
#include "CtInstance.h"
#include "CtLocal.h"


class ClassDecl;


class CtObjectInstance : public CtInstance {
public:
  explicit CtObjectInstance( ClassDecl & );

protected:
  void dependHeaderFields( CtRegistry & ) const;
  bool defineHeaderFields( ostream & ) const;

private:
  const CtLocal descriptor;
};


#endif // !_include_CtObjectInstance_h_


// Local Variables:
// c-file-style: "gnu"
// End:
