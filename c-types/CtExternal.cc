#include "../code-files/CfCode.h"
#include "CtExternal.h"


void CtExternal::declare( CfFile &out ) const
{
  define( out );
}


void CtExternal::define( CfFile &out ) const
{
  out << "#include <" << *this << ".h>\n";
}


void CtExternal::elaborate() const
{
}


// Local Variables:
// c-file-style: "gnu"
// End:
