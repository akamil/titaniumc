#include "../AST.h"
#include "../ClassDecl.h"
#include "../FieldDecl.h"
#include "../code.h"
#include "../compiler.h"
#include "CtInstance.h"
#include "CtRegistry.h"


CtInstance::CtInstance( ClassDecl &basis )
  : CtClass( mangle( basis ), basis )
{
}


void CtInstance::dependMembers( ClassDecl &ancestor, CtRegistry &depends ) const
{
  // Order doesn't matter here, so the code is structured to
  // facilitate tail recursion optimization of the method.

  foriter (member, ancestor.environ()->allProperDecls( Decl::Field ), EnvironIter)
    if (member->container() == &ancestor && !(member->modifiers() & Common::Static))
      depends.add( static_cast< const FieldDecl & >( *member ).cType() );

  if (&ancestor != ObjectDecl && ancestor.superClass())
    dependMembers( *ancestor.superClass(), depends );
}


bool CtInstance::defineMembers( ClassDecl &ancestor, ostream &out ) const
{
  bool nonempty = false;
  
  // Order matters here, so the tail recursive
  // ordering used above is not suitable.

  if (&ancestor != ObjectDecl && ancestor.superClass())
    nonempty |= defineMembers( *ancestor.superClass(), out );
  
  foriter (member, ancestor.environ()->allProperDecls( Decl::Field ), EnvironIter)
    if (member->container() == &ancestor && !(member->modifiers() & Common::Static))
      {
	const FieldDecl &field = static_cast< const FieldDecl & >( *member );
	out << field.cType() << ' ' << field.cFieldName() << ";\n";
	nonempty = true;
      }

  return nonempty;
}


const string CtInstance::mangle( ClassDecl &basis )
{
  return MANGLE_TYPE_MARKER + basis.mangledFullName();
}


// Local Variables:
// c-file-style: "gnu"
// End:
