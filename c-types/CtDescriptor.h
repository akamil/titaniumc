#ifndef _include_CtDescriptor_h_
#define _include_CtDescriptor_h_


#include <string>
#include "CtClass.h"


class ClassDecl;
class MethodDecl;


class CtDescriptor : public CtClass {
public:
  explicit CtDescriptor( ClassDecl & );
  
protected:
  virtual void dependHeaderFields( CtRegistry & ) const;

  bool defineHeaderFields( ostream & ) const;
  
private:
  static const string mangle( ClassDecl & );
};


#endif // !_include_CtDescriptor_h_


// Local Variables:
// c-file-style: "gnu"
// End:
