#include "../code-files/CfFile.h"
#include "CtMonitor.h"


const CtMonitor CtMonitor::singleton;


void CtMonitor::define( CfFile &out ) const
{
  out << "#include <backend.h>\n";
}


// Local Variables:
// c-file-style: "gnu"
// End:
