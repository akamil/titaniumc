#ifndef _include_CtPointInstance_h_
#define _include_CtPointInstance_h_


#include "CtInstance.h"
#include "CtPrimitive.h"


class ClassDecl;

class CtPointInstance : public CtInstance {
public:
  explicit CtPointInstance( unsigned );

  void declare( CfFile & ) const;
  int isPrimitive() const { return arity == 1; }
  const CtType& componentType() const;

protected:
  void elaborateTo( CfLayout & ) const;
  void dependFields( CtRegistry & ) const;
  bool defineFields( ostream & ) const;

private:
  const unsigned arity; 
};


#endif // !_include_CtPointInstance_h_


// Local Variables:
// c-file-style: "gnu"
// End:
