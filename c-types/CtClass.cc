#include <cassert>
#include "../compiler.h"
#include "../ClassDecl.h"
#include "../MemberDecl.h"
#include "CtClass.h"


CtClass::CtClass( const string &name, ClassDecl &basis )
  : CtStruct( name ),
    basis( basis )
{
  // Internal classes should never make it to code generation.  So if
  // you're asking for C type names relating to an internal class,
  // you're doing something wrong.
  
  assert( basis.container()->fullName() != "ti.internal" );
}


void CtClass::dependFields( CtRegistry &depends ) const
{
  dependHeaderFields( depends );
  dependMembers( basis, depends );
}


bool CtClass::defineFields( ostream &out ) const
{
  bool nonempty = false;
  nonempty |= defineHeaderFields( out );
  nonempty |= defineMembers( basis, out );
  return nonempty;
}


void CtClass::dependHeaderFields( CtRegistry & ) const
{
}


bool CtClass::defineHeaderFields( ostream & ) const
{
  return false;
}


void CtClass::dependMembers( ClassDecl &, CtRegistry & ) const
{
}


bool CtClass::defineMembers( ClassDecl &, ostream & ) const
{
  return false;
}


// Local Variables:
// c-file-style: "gnu"
// End:
