#include "../PrimitiveDecl.h"
#include "../domain-decls.h"
#include "CtPointInstance.h"
#include "CtRegistry.h"
#include "../code-files/CfCode.h"
#include <iostream>
#include <cassert>


CtPointInstance::CtPointInstance( unsigned arity )
  : CtInstance( *PointNDecl[ arity - 1 ] ),
    arity( arity )
{
}

void CtPointInstance::declare( CfFile &out ) const {
  if (isPrimitive()) {
    out << "#include <primitives.h>\n";
    out << "#ifndef _typedef_" << *this << '\n'
        << "#define _typedef_" << *this << '\n'
        << "typedef " << componentType() << ' ' << *this << ";\n"
        << "#endif\n";
  } else CtInstance::declare(out);
}

void CtPointInstance::elaborateTo( CfLayout & out ) const {
  if (isPrimitive()) declare(*(CfFile*)(&out));
  else CtInstance::elaborateTo(out);
}

void CtPointInstance::dependFields( CtRegistry &registry ) const
{
  registry.add( componentType() );
}


bool CtPointInstance::defineFields( ostream &out ) const
{
  assert(arity && !isPrimitive());
  const CtType &fType = componentType();
  for (unsigned dimension = 0; dimension < arity; ++dimension)
    out << fType << ' ' << 'x' << dimension << ";\n";
  return true;
}

const CtType& CtPointInstance::componentType() const
{
#if USE_64BIT_POINTS 
  return PrimitiveDecl::LongDecl.cType();
#else
  return PrimitiveDecl::IntDecl.cType();
#endif
}

// Local Variables:
// c-file-style: "gnu"
// End:
