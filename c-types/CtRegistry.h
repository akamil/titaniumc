#ifndef _include_CtRegistry_h_
#define _include_CtRegistry_h_

#include <set>
#include "CtRegistryPredicate.h"

class CfFile;
class CtType;


class CtRegistry : private set< const CtType *, CtRegistryPredicate > {
public:
  void defineAll( CfFile & ) const;
  void elaborateAll() const;

  void add( const CtType & );
};


#include "CtRegistry.inlines.h"


#endif // !_include_CtRegistry_h_


// Local Variables:
// c-file-style: "gnu"
// End:
