#ifndef _include_CtRegistryPredicate_inlines_h_
#define _include_CtRegistryPredicate_inlines_h_


#include "CtRegistryPredicate.h"
#include "CtType.h"


inline bool CtRegistryPredicate::operator () ( const CtType *first,
					       const CtType *second ) const
{
  return *first < *second;
}


#endif // !_include_CtRegistryPredicate_inlines_h_


// Local Variables:
// c-file-style: "gnu"
// End:
