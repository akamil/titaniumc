#ifndef _include_CtRegistryPredicate_h_
#define _include_CtRegistryPredicate_h_


#include "../SortingPredicate.h"

class CtType;


struct CtRegistryPredicate : public SortingPredicate< const CtType * > {
  bool operator () ( const CtType *, const CtType * ) const;
};


#include "CtRegistryPredicate.inlines.h"


#endif // !_include_CtRegistryPredicate_h_


// Local Variables:
// c-file-style: "gnu"
// End:
