#ifndef _include_CtLocal_inlines_h_
#define _include_CtLocal_inlines_h_


#include "CtLocal.h"
#include "../code.h"


inline CtLocal::CtLocal( const CtType &referent )
  : CtReference( MANGLE_TYPE_LOCAL_MARKER, referent )
{
}


#endif // !_include_CtLocal_inlines_h_


// Local Variables:
// c-file-style: "gnu"
// End:
