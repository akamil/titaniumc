#ifndef _include_CtExternal_h_
#define _include_CtExternal_h_


#include "CtType.h"


class CtExternal : public CtType {
public:
  explicit CtExternal( const string & );

  void declare( CfFile & ) const;
  void define( CfFile & ) const;
  void elaborate() const;
};


#include "CtExternal.inlines.h"


#endif // !_include_CtExternal_h_
    

// Local Variables:
// c-file-style: "gnu"
// End:
