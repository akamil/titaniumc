#ifndef _include_CtType_h_
#define _include_CtType_h_


#include <iosfwd>
#include <string>
#include "../using-std.h"

class CfFile;
class CtRegistry;


class CtType : public string {
public:
  void print( ostream & ) const;

  virtual void declare( CfFile & ) const = 0;
  virtual void define( CfFile & ) const = 0;
  virtual void elaborate() const = 0;
  
  static void elaborateAll();
    
protected:
  explicit CtType( const string & );
  virtual ~CtType() { }

private:
  static CtRegistry *allTypes;
};


#endif // !_include_CtType_h_


// Local Variables:
// c-file-style: "gnu"
// End:
