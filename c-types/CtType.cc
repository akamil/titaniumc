#include "CtRegistry.h"
#include "CtType.h"


// Actual allocation of "allTypes" takes place in CtType constructor.
// If we want to allow static CtType instances, this is the only way
// to ensure that initializations take place in the right order.

CtRegistry *CtType::allTypes;


void CtType::elaborateAll()
{
  allTypes->elaborateAll();
}


CtType::CtType( const string &name )
  : string( name )
{
  if (!allTypes) allTypes = new CtRegistry;
  allTypes->add( *this );
}


// Local Variables:
// c-file-style: "gnu"
// End:
