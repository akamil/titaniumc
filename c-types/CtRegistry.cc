#include "CtRegistry.h"
#include "CtType.h"


void CtRegistry::defineAll( CfFile &out ) const
{
  for( const_iterator sweep = begin();
       sweep != end(); ++sweep )
    (*sweep)->define( out );
}


void CtRegistry::elaborateAll() const
{
  for( const_iterator sweep = begin();
       sweep != end(); ++sweep )
    (*sweep)->elaborate();
}


// Local Variables:
// c-file-style: "gnu"
// End:
