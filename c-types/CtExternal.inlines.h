#ifndef _include_CtExternal_inlines_h_
#define _include_CtExternal_inlines_h_


#include "CtExternal.h"


inline CtExternal::CtExternal( const string &name )
  : CtType( name )
{
}


#endif // !_include_CtExternal_inlines_h_


// Local Variables:
// c-file-style: "gnu"
// End:
