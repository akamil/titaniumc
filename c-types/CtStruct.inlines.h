#ifndef _include_CtStruct_inlines_h_
#define _include_CtStruct_inlines_h_


#include "CtStruct.h"


inline CtStruct::CtStruct( const string &name )
  : CtDefined( name )
{
}


#endif // !_include_CtStruct_inlines_h_


// Local Variables:
// c-file-style: "gnu"
// End:
