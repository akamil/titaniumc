#include "../ClassDecl.h"
#include "../WithDef.h"
#include "../code-files/CfLayout.h"
#include "../code.h"
#include "../domain-decls.h"
#include "../osstream.h"
#include "CtReference.h"
#include "CtTitaniumArray.h"


CtTitaniumArray::CtTitaniumArray( const CtReference &data,
				  unsigned arity,
                                  bool isLocal )
  : CtArray( mangle( data, arity ), data.referent ),
    data( data ),
    domain( RectDomainNDecl[ arity - 1 ]->cType() ),
    arity( arity ),
    isLocal( isLocal )
{
}


void CtTitaniumArray::elaborateTo( CfLayout &out ) const
{
  domain.define( out );
  data.define( out );

  WithDef arrayDef( out, "ti_ARRAY", *this );
  WithDef domainDef( out, "ti_RECTDOMAIN", domain );
  WithDef dataDef( out, "PTR_TO_T", data );
  WithDef arityDef( out, "N", int2string( arity ) );
  WithDef globalDef( out, "GLOBAL_ARRAY", int2string( !isLocal ) );

  out << "#include <ti_array_layout.h>\n";

  declare( out );
}


const string CtTitaniumArray::mangle( const CtReference &data,
				      unsigned arity )
{
  ostringstream assemble;
  assemble << MANGLE_TYPE_MARKER << MANGLE_TI_ARRAY_TYPE_MARKER
	   << data.titaniumArrayPrefix()
	   << arity
	   << data.referent;

  return assemble.str();
}


// Local Variables:
// c-file-style: "gnu"
// End:
