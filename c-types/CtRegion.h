#ifndef _include_CtRegion_h_
#define _include_CtRegion_h_


#include "CtExternal.h"


class CtRegion : public CtExternal {
public:
  void define( CfFile & ) const;

  static const CtRegion singleton;

private:
  CtRegion();
};


#include "CtRegion.inlines.h"


#endif // !_include_CtRegion_h_


// Local Variables:
// c-file-style: "gnu"
// End:
