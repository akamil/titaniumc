#ifndef _include_CtObjectDescriptor_h_
#define _include_CtObjectDescriptor_h_


#include <string>
#include "CtDescriptor.h"
#include "CtLocal.h"


class ClassDecl;
class CtExternal;
class Decl;


class CtObjectDescriptor : public CtDescriptor {
public:
  explicit CtObjectDescriptor( ClassDecl & );

  static CtExternal classHeader;
  static CtExternal interfaceHeader;

protected:
  void dependHeaderFields( CtRegistry & ) const;
  bool defineHeaderFields( ostream & ) const;

  void dependMembers( ClassDecl &, CtRegistry & ) const;
  bool defineMembers( ClassDecl &, ostream & ) const;

private:
  static bool novel( const ClassDecl &, const Decl & );
  static const CtType &superDescriptor( const ClassDecl & );
 
  const ClassDecl &_basis;
  const CtType &super() const;
};


#endif // !_include_CtObjectDescriptor_h_


// Local Variables:
// c-file-style: "gnu"
// End:
