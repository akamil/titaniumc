#include <cassert>
#include "../AST.h"
#include "../ClassDecl.h"
#include "CtDescriptor.h"
#include "CtObjectInstance.h"
#include "CtLocal.h"
#include "CtExternal.h"
#include "CtRegistry.h"


static const CtExternal monitor( "tic_monitor_t" );

CtObjectInstance::CtObjectInstance( ClassDecl &basis )
  : CtInstance( basis ),
    descriptor( basis.cDescriptorType() )
{
  assert( basis.asType()->isReference() );
}


void CtObjectInstance::dependHeaderFields( CtRegistry &depends ) const
{
  depends.add( descriptor );
  //depends.add( monitor );
  
  CtInstance::dependHeaderFields( depends );
}


bool CtObjectInstance::defineHeaderFields( ostream &out ) const
{
  CtInstance::defineHeaderFields( out );
  
  out << descriptor << ' ' << "class_info;\n"
//      << "void (**intf_info)();\n"
      << "#ifdef HAVE_MONITORS\n"
      << monitor << ' ' << "monitor;\n"
      << "#endif\n";

  return true;
}


// Local Variables:
// c-file-style: "gnu"
// End:
