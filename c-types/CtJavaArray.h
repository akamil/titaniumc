#ifndef _include_CtJavaArray_h_
#define _include_CtJavaArray_h_


#include "CtArray.h"


class CtJavaArray : public CtArray {
public:
  explicit CtJavaArray( const CtType & );

protected:
  void elaborateTo( CfLayout & ) const;
};


#endif // !_include_CtJavaArray_h_


// Local Variables:
// c-file-style: "gnu"
// End:
