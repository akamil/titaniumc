#ifndef _include_ctBox_h_
#define _include_ctBox_h_


class CtReference;
class CtType;


CtReference &ctBox( const CtType &, bool isLocal );


#endif // !_include_ctBox_h_


// Local Variables:
// c-file-style: "gnu"
// End:
