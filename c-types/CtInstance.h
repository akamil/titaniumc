#ifndef _include_CtInstance_h_
#define _include_CtInstance_h_


#include <string>
#include "CtClass.h"


class ClassDecl;


class CtInstance : public CtClass {
public:
  explicit CtInstance( ClassDecl & );

protected:
  void dependMembers( ClassDecl &, CtRegistry & ) const;
  bool defineMembers( ClassDecl &, ostream & ) const;

private:
  static const string mangle( ClassDecl & );
};


#endif // !_include_CtInstance_h_


// Local Variables:
// c-file-style: "gnu"
// End:
