#include "../code-files/CfLayout.h"
#include "CtRegistry.h"
#include "CtStruct.h"


void CtStruct::elaborateTo( CfLayout &out ) const
{
  CtRegistry depends;
  dependFields( depends );  
  depends.defineAll( out );
  
  declare( out );

  out << "struct " << *this << " {\n";
  if (!defineFields( out ))
    out << "char dummy;\n";
  out << "};\n";
}


// Local Variables:
// c-file-style: "gnu"
// End:
