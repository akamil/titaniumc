#include "../code-files/CfFile.h"
#include "CtRegion.h"


const CtRegion CtRegion::singleton;


void CtRegion::define( CfFile &out ) const
{
  out << "#include <regions.h>\n";
}


// Local Variables:
// c-file-style: "gnu"
// End:
