#ifndef _include_CtMonitor_inlines_h_
#define _include_CtMonitor_inlines_h_


#include "CtMonitor.h"


inline CtMonitor::CtMonitor()
  : CtExternal( "tic_monitor_t" )
{
}


#endif // !_include_CtMonitor_inlines_h_


// Local Variables:
// c-file-style: "gnu"
// End:
