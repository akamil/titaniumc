#ifndef _include_CtArray_h_
#define _include_CtArray_h_


#include "CtDefined.h"


class CtArray : public CtDefined {
protected:
  CtArray( const string &, const CtType & );

  void elaborateTo( CfLayout & ) const = 0;

  const CtType &elements;
};


#endif // !_include_CtArray_h_


// Local Variables:
// c-file-style: "gnu"
// End:
