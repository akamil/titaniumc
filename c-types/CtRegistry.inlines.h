#ifndef _include_CtRegistry_inlines_h_
#define _include_CtRegistry_inlines_h_

#include "CtRegistry.h"


inline void CtRegistry::add( const CtType &type )
{
  insert( &type );
}


#endif // !_include_CtRegistry_inlines_h_


// Local Variables:
// c-file-style: "gnu"
// End:
