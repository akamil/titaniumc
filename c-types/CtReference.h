#ifndef _include_CtReference_h_
#define _include_CtReference_h_


#include "CtDefined.h"


class CtReference : public CtDefined {
public:
  virtual const char *titaniumArrayPrefix() const = 0;
  
  const CtType &referent;

protected:
  CtReference( const char [], const CtType & );

  void elaborateTo( CfLayout & ) const = 0;
};


#endif // !_include_CtReference_h_


// Local Variables:
// c-file-style: "gnu"
// End:
