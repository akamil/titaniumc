#include "../AST.h"
#include "../ClassDecl.h"
#include "../MethodDecl.h"
#include "../utils.h"
#include "CtDescriptor.h"
#include "CtRegistry.h"
#include "../code.h"
#include "../compiler.h"

CtDescriptor::CtDescriptor( ClassDecl &basis )
  : CtClass( mangle( basis ), basis )
{
}


bool CtDescriptor::defineHeaderFields( ostream &out ) const
{
  out << JavaLangClassDecl->cType() << " class_object;\n"
      << StringDecl->asType()->withModifiers(Common::Local)->cType() << " class_name;\n"
      << "enum TypeCategory category;\n"
      << "enum TypeFlags flags;\n"
      << "jchar index;\n";
  return true;
}

void CtDescriptor::dependHeaderFields( CtRegistry &depends ) const
{
  depends.add( JavaLangClassDecl->cType() );
  depends.add( StringDecl->asType()->withModifiers(Common::Local)->cType() );

  CtClass::dependHeaderFields( depends );
}


const string CtDescriptor::mangle( ClassDecl &basis )
{
  return MANGLE_CLASS_DTYP(+, MANGLE_TYPE_MARKER + basis.mangledFullName());
}


// Local Variables:
// c-file-style: "gnu"
// End:
