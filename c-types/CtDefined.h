#ifndef _include_CtDefined_h_
#define _include_CtDefined_h_

#include <string>
#include "CtType.h"


class CfLayout;


class CtDefined : public CtType {
public:
  void declare( CfFile & ) const;
  void define( CfFile & ) const;
  void elaborate() const;
    
protected:
  explicit CtDefined( const string & );

  virtual void elaborateTo( CfLayout & ) const = 0;

private:
  const string headerBasename() const;
};


#include "CtDefined.inlines.h"


#endif // !_include_CtDefined_h_


// Local Variables:
// c-file-style: "gnu"
// End:
