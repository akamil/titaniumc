#ifndef _MIVEset_H_
#define _MIVEset_H_

#include "llist.h"
#include "code-MIVE.h"

#include <set>

typedef set< MIVE *, MIVESort > MIVEset;

static inline bool contains_pointer(MIVEset *s, MIVE *m)
{
  return (s->count(m) > 0);
}

static inline bool contains(MIVEset *s, MIVE *m)
{
  if (s != NULL)
    for (MIVEset::iterator i = s->begin(); i != s->end(); i++)
      if (m->equals(*i))
	return true;

  return false;
}

static inline MIVEset * adjoin(MIVEset *s, MIVE *m)
{
  if (!contains(s, m))
    s->insert(m);
  return s;
}

/* in code-MIVE.cc */
llist<const MIVE *> * MIVEset_to_list(const MIVEset *s);
llist<const MIVE *> * mightBeMinimum(llist<const MIVE *> *l, int dim); 
llist<const MIVE *> * mightBeMaximum(llist<const MIVE *> *l, int dim); 

#endif /* _MIVEset_H_ */
