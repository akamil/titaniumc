#ifndef _tc_hash_map_h_
#define _tc_hash_map_h_

#if 0 // HAVE_HASH_MAP -- deprecated!
  #include <hash_map>
  #define POINTER_HASH_MAP(keyT, valueT) hash_map< intptr_t, valueT >
  #define HASH_MAP(keyT, valueT) hash_map< keyT, valueT >
  #define FULL_HASH_MAP(keyT, valueT, hashFn, equalFn) hash_map< keyT, valueT, hashFn, equalFn >
#elif defined(HAVE_EXT_HASH_MAP)
  #include <ext/hash_map>
  #if defined(__GNUC__) && ((__GNUC__ == 3 && __GNUC_MINOR__ > 0) || __GNUC__ > 3)
    #define EXT_HASH_MAP_NAMESPACE __gnu_cxx::
  #else
    #define EXT_HASH_MAP_NAMESPACE
  #endif
  #define POINTER_HASH_MAP(keyT, valueT) EXT_HASH_MAP_NAMESPACE hash_map< intptr_t, valueT >
  #define HASH_MAP(keyT, valueT) EXT_HASH_MAP_NAMESPACE hash_map< keyT, valueT >
  #define FULL_HASH_MAP(keyT, valueT, hashFn, equalFn) EXT_HASH_MAP_NAMESPACE hash_map< keyT, valueT, hashFn, equalFn >
#else
  #include <map>
  #define POINTER_HASH_MAP(keyT, valueT) map< intptr_t, valueT >
  #define HASH_MAP(keyT, valueT) map< keyT, valueT >
  #define FULL_HASH_MAP(keyT, valueT, hashFn, equalFn) map< keyT, valueT >
#endif

#endif
