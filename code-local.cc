#include "AST.h"
#include "CodeContext.h"
#include "CtGlobal.h"
#include "CtLocal.h"
#include "code-grid.h"
#include "maps.h"

const string TypeNode::emitGlobalize( CodeContext &context,
				      const string &box,
				      const string &local ) const
{
  assert( isReference() );
  assert( isLocal() );
  
  static map<_CONST_ string, const CtGlobal *> m;
  const CtType &t = decl()->cType();
  const CtGlobal *&global = m[t];

  if (global == NULL)
    global = new CtGlobal( decl()->cType() );

  const string result = ExprNode::declareTemporary( context, *global );

  context << "TO_GLOBALB(" << result << ", "
	  << box << ", " << local << ");" << endCline;

  return result;
}


const string TitaniumArrayTypeNode::emitGlobalize( CodeContext &,
						   const string &box,
						   const string &local ) const
{
  assert( isLocal() );
  
  return
    arrayMethodPrefix(this) + "cast("
    + elementType()->cType() + ", "
    + int2string(tiArity()) + ")(&"
    + local + ", " + box + ')';
}


////////////////////////////////////////////////////////////////////////


const string TypeNode::emitLocalize( CodeContext &context,
				     const string &global ) const
{
  assert( isReference() );
  assert( !isLocal() );

  static map<_CONST_ string, const CtLocal *> m;
  const CtType &t = decl()->cType();
  const CtLocal *&local = m[t];

  if (local == NULL)
    local = new CtLocal( decl()->cType() );

  const string result = ExprNode::declareTemporary( context, *local );

  context << "localize(" << result << ", " << global << ");\n";
  return result;
}


const string TitaniumArrayTypeNode::emitLocalize( CodeContext &,
						  const string &global ) const
{
  assert( !isLocal() );
  
  return arrayMethodPrefix(this) + "cast("
    + elementType()->cType() + ", "
    + int2string(tiArity()) + ")(\""
    + position().asString() + "\", &"
    + global + ')';
}
