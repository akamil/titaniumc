#include "AST.h"
#include "domain-decls.h"
#include "code.h"
#include "code-util.h"
#include "code-operator.h"


bool PointArrayAccessNode::isLocalLvalue()
{
  return true;
}


bool PointArrayAccessNode::isSimpleLvalue()
{
  return true;
}


const string PointArrayAccessNode::emitExpression( CodeContext &context )
{
  const string arrayExpr = array()->simpleVar( context );

  if (isPrimitiveLitNode(index())) {
    int ind = index()->literal().intValue();
    int ptarity = array()->type()->tiArity();
    if (ind >= 1 && ind <= ptarity) {
      return "POINT" + int2string(ptarity) + "_GET(" + arrayExpr + "," + int2string(ind-1) + ")";
    }
    else
      warning("point-bounds") << "Requested non-existent component " << ind << " from a Point<" << ptarity << ">\n";
  }

    const string indexExpr = index()->emitExpression( context );

    return MANGLE_TI_DOMAINS_POINT_RAWDISPATCH(+, 
             int2string(array()->type()->tiArity()), 
             encodedMethodName(string("[]"), string("")),
             MANGLE_INT_ARG(+)) + 
             "(" + arrayExpr + ", " + indexExpr + ')';
}
