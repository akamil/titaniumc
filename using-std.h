#ifndef _INCLUDE_TITANIUM_USING_STD_H_
#define _INCLUDE_TITANIUM_USING_STD_H_


#if (defined(__GNUC__) && __GNUC__ == 2 && __GNUC_MINOR__ == 97) || \
  /* work-around for a buggy g++ release on Tru64 */ \
    defined(__INTEL_COMPILER) /* and a buggy Intel compiler - 8.1 on CITRIS */
  #define STD_REL_OPS_BROKEN 1
#endif

namespace std {
    namespace rel_ops { }
}

using namespace std;
#ifndef STD_REL_OPS_BROKEN
  using namespace std::rel_ops;
#endif

#if (defined(__GNUC__) && ((__GNUC__ == 3 && __GNUC_MINOR__ > 0) || \
                           __GNUC__ > 3)) || \
     defined(__HP_aCC)
  /* g++ 3.1 requires "typename" to appear according to C++ spec,
     anywhere within a template declaration where templatename::membername
     is used as a type (no implicit typenames) */
  #define TYPENAME typename
#else
  #define TYPENAME
#endif

#endif // !_INCLUDE_TITANIUM_USING_STD_H_
