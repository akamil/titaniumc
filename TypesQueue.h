#ifndef INCLUDE_TYPES_QUEUE_H
#define INCLUDE_TYPES_QUEUE_H

#include <list>

class CompileUnitNode;


struct TypesQueue : public list< CompileUnitNode * >
{
  void resolve();
};


extern TypesQueue unresolvedTypes;


#endif //INCLUDE_TYPES_QUEUE_H


// Local Variables:
// c-file-style: "gnu"
// End:
