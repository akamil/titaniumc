#ifndef INCLUDE_CLASSES_QUEUE_H
#define INCLUDE_CLASSES_QUEUE_H

#include <list>

class CompileUnitNode;


struct ClassesQueue : public list< CompileUnitNode * >
{
  void resolve();
};


extern ClassesQueue unresolvedClasses;


#endif //INCLUDE_CLASSES_QUEUE_H


// Local Variables:
// c-file-style: "gnu"
// End:
