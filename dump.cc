#define __SINST__		// g++ header bug
#include <iostream>
#include "AST.h"
#include "decls.h"
#include "dump.h"

/* entry points intended for calls from a debugger */
int ddump(const TreeNode *x)
{
  ostringstream oss;
  x->dump(oss, 0);
  fputs(oss.str().c_str(), stderr);
  fputs("\n", stderr);
  fflush(stderr);
  return 0;
}

int dpseudo(const TreeNode *x)
{
  ostringstream oss;
  x->pseudoprint(oss, 0);
  fputs(oss.str().c_str(), stderr);
  fputs("\n", stderr);
  fflush(stderr);
  return 0;
}

void TreeNode::dumpPrefix( ostream &sink, unsigned depth ) const
{
  sink << '(';
  ::dump( sink, depth, this );
}


void TreeNode::dumpSuffix( ostream &sink, unsigned ) const
{
  sink << ')';
}


void TreeNode::dump( ostream &sink, unsigned depth ) const
{
  dumpPrefix( sink, depth );
  dumpSuffix( sink, depth );
}


void OmittedNode::dump( ostream &sink, unsigned depth ) const
{
  sink << '@';
}


void TreeListNode::dump( ostream &sink, unsigned depth ) const
{
  sink << "[\n";

  foriter (child, allChildren(), ConstChildIter) {
    indent( sink, depth + 1 );
    (*child)->dump( sink, depth + 1 );
    sink << '\n';
  }

  indent( sink, depth );
  sink << ']';
}


#include "dump-nodes.cc"
