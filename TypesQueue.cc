#include "AST.h"
#include "TypesQueue.h"
#include "TypeContext.h"
#include "code-util.h"

TypesQueue unresolvedTypes;


void TypesQueue::resolve()
{
  TypesQueue postponedUnits;
  
  while (!empty())
    {
      CompileUnitNode &unit = *front();
      pop_front();

      compile_status(2,string("type resolution: ") + *(unit.ident()));      
      bool postponed = false;
      TreeNode::TypeContext context( postponed );
      unit.buildTypeEnv( &context );
      unit.resolveTypes( &context );
      
      if (postponed)
	postponedUnits.push_back( &unit );
    }
  
  splice( end(), postponedUnits );
}


// Local Variables:
// c-file-style: "gnu"
// End:
