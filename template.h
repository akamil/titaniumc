#ifndef _TEMPLATE_H_
#define _TEMPLATE_H_

#include "Subst.h"
#include "llist.h"

class ClassDecl;
class Decl;
class TreeNode;
class TypeNode;


// A pre-template template hack, used for Point & co
// Hopefully, it will be somewhat reusable for the real thing

// This remembers the already instantiated members of a 'template'
// (well, it will one day - for now it's just a hack for Point & co)
// essentially, it's a mapping from TreeNodes to Decl's

class TemplateEnv {
public:
  TemplateEnv() { instances = NULL; }

  Decl *lookup(const TypeNode *type);
  void add(const TypeNode *type, Decl *inst);
  void rehash();

  llist<TreeNode *> *allInstances();
  /* Return: a list of all the template instances. Caller must free list
     when done. */

  void resolveTemplates(void);
  void foldFields();
  void foldConstants();
protected:
  struct instance {
    const TypeNode *type;
    Decl *d;
  };
  llist<instance *> *instances;
};

extern TemplateEnv templateEnv;

ClassDecl *instantiateClass(ClassDecl *tmplate,
			    const TypeNode *type,
			    TemplateEnv *templateEnv, 
			    Subst *args, TypeNode*);

#endif
