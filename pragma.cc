#include "AST.h"
#include "optimize.h"
#include "pragma.h"
#include <vector>

static bool contains_pragma(Pragma::Request r, const TreeNode *t)
{
  if (t->isPragma(r))
    return true;
  int a = t->arity();
  for (int i = 0; i < a; i++)
    if (contains_pragma(r, t->child(i)))
      return true;
  return false;
}

treeSet *find_subtrees_not_with_pragma(Pragma::Request r, const TreeNode *t)
{
  treeSet *result = new treeSet;
  vector<bool> v;
  int a = t->arity();
  for (int i = 0; i < a; i++)
    v.push_back(contains_pragma(r, t));
  for (int i = 0; i < a; i++) {
    const TreeNode *child = t->child(i);
    treeSet *x = NULL;
    result = v[i] ?
      destructively_union(result,
			  x = find_subtrees_not_with_pragma(r, child)) :
      adjoin(result, child);
    delete x;
  }
  return result;
}

bool underPragma(Pragma::Request r, const TreeNode *t)
{
  return (t->isPragma(r) ||
	  t->parent() != NULL && underPragma(r, t->parent()));
}

bool TreeNode::isPragma(Pragma::Request r) const
{
  return false;
}

bool PragmaNode::isPragma(Pragma::Request r) const
{
  return (requests() & r) == r;
}

void PragmaNode::emitStatement(CodeContext &c)
{
  stmt()->codeGen(c);
}
