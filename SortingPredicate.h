#ifndef _TITANIUM_SORTING_PREDICATE_H_
#define _TITANIUM_SORTING_PREDICATE_H_

#include <functional>
#include "using-std.h"


template< class T > struct SortingPredicate : public binary_function< T, T, bool > {
};


#endif // !_TITANIUM_SORTING_PREDICATE_H_
