#ifndef INCLUDE_OSSTREAM_H
#define INCLUDE_OSSTREAM_H

#include "ti_config.h"
#include "using-std.h"


#if defined(HAVE_SSTREAM) && !defined(SSTREAM_BROKEN)
#include <sstream>
#else // no stringstream classes, or they're broken

#include <string>
#include <strstream>

// This is not intended to be a complete ostringstream implementation.
// It has only the minimal functionality required for use in the
// Titanium compiler, and can be removed once real implementations are
// more widely available.

class ostringstream : public ostrstream {
public:
  string str() const;

private:
  string extract();
};


inline string ostringstream::str() const
{
  return const_cast< ostringstream * >( this )->extract();
}


inline string ostringstream::extract()
{
  const int wasFrozen = frozen();
  const string result( ostrstream::str(), pcount() );
  
  freeze( wasFrozen );
  return result;
}

#endif // no stringstream classes


#endif // !INCLUDE_OSSTREAM_H
