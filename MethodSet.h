#ifndef _include_MethodSet_h_
#define _include_MethodSet_h_

#include <set>
#include "using-std.h"

class MethodDecl;


typedef set< MethodDecl *, less< MethodDecl * > > MethodSet;


#endif // !_include_MethodSet_h_
