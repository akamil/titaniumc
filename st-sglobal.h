#ifndef _INCLUDE_ST_SGLOBAL_H_
#define _INCLUDE_ST_SGLOBAL_H_

#include "ti_config.h"


extern bool use_sglobal_inference;

void sglobalInference();
// Effects: Perform sglobal qualifier inference on the program


#endif // !_INCLUDE_ST_SGLOBAL_H_
