#define _VERSION_CC 1
#include "version.h"
#undef _VERSION_CC

int tcMajorVersion = TC_VERSION_MAJOR;
int tcMinorVersion = TC_VERSION_MINOR;

char tcMajorVersionStr[] = TC_VERSION_MAJOR_STR;
char tcMinorVersionStr[] = TC_VERSION_MINOR_STR;

char tcWhatString[] = "@(#)" TC_DESCRIPTION_STR " " TC_VERSION_MAJOR_STR "." TC_VERSION_MINOR_STR ;
