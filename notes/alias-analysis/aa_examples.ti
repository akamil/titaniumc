/* Various tests of the new interprocedural analyses. */
class aa_examples {
  
  public static void main(String[] args) { }  

  static aa_examples a;
  aa_examples b;  
  int x;

  /* SEF constructor. */  
  aa_examples(int y)
  {
    x = y;
  }

  /* Constructor with side-effects. */
  aa_examples()
  {
    aa_examples.a = null;
  }

  /* Constructor that leaks the new object. */  
  aa_examples(int y, int x)
  {
    aa_examples.a = this;
  }

  /* Leaks arg1. */
  void leaksArgs(aa_examples arg1, aa_examples arg) {
    aa_examples.a = arg1;
  }

  /* Leaks arg1. */
  void leaksArgs1(aa_examples arg1, aa_examples arg2) {
    leaksArgs(arg1, arg2);
  }

  /* Leaks arg1. (Modifies arg2.) */
  void leaksArgs2(aa_examples arg1, aa_examples arg2) {
    arg2.b = arg1;
  }        

  /* Leaks this. */
  void leaksThis(aa_examples arg1, aa_examples arg) {
    aa_examples.a = this;
  }

  /* Modifies arg1. */
  void modArgs(aa_examples arg1)
  {
    arg1.x = 5;
  }

  /* Modifies this. */  
  void modThis()
  {
    this.x = 5;
  }

  /* Modifies this. */
  void modThis2(aa_examples arg1)
  {
    returnThis(null).modThis();
  }    

  /* Modifies the external value. */  
  void modExt(aa_examples arg1)
  {
    System.out.println("hello");
  }

  /* Returns this. */    
  aa_examples returnThis(aa_examples arg1)
  {
    return this;
  }

  /* Returns the first argument. */
  aa_examples returnArgs1(aa_examples arg1)
  {
    return arg1;
  }

  /* Returns something from first argument (should still be considered
     first argument). */
  aa_examples returnArgs2(aa_examples arg1)
  {
    return arg1.b;
  }

  /* Returns both arguments. */
  aa_examples returnBoth(aa_examples arg1, aa_examples arg2)
  {
    if (true)
      return arg1.returnThis(null);
    else
      return returnArgs1(arg2);
  }    

  /* Returns the external value. */
  aa_examples returnExt(aa_examples arg1)
  {
    return arg1.a; // same as aa_examples.a
  }        

  Object sideEffectFree1(aa_examples arg1)
  {
    aa_examples b = new aa_examples(3);
    b.x = 5;
    if (true)
      return arg1;
    else
      return b;
		  
  }

  Object sideEffectFree2(aa_examples arg1)
  {
    arg1.sideEffectFree1(arg1);
    return arg1;
  }

  Object sideEffectFree3(aa_examples arg1)
  {
    aa_examples b = new aa_examples(3);
    b.modThis();
    return arg1;
  }

  Object sideEffectFree4(aa_examples arg1)
  {
    aa_examples b = new aa_examples(3);
    arg1.modArgs(b);
    b.modThis();
    return b;
  }

  /* Modifies arg1. */
  void badSideEffects1(aa_examples arg1, aa_examples arg2)
  {
    aa_examples o = arg1.returnThis(null);
    o.modThis();
  }

  /* Modifies arg2. */  
  void badSideEffects2(aa_examples arg1, aa_examples arg2)
  {
    aa_examples p = arg1.returnArgs1(arg2);
    p.modThis();
  }

  /* Modifies arg1 and arg2 (and the external). */    
  void badSideEffects3(aa_examples arg1, aa_examples arg2)
  {
    aa_examples p;
    if (true) 
      p = arg1.returnArgs2(arg2);
    else
      p = arg1.returnArgs2(arg1);
    p.modThis();
  }

  /* Modifies arg1 and arg2 (and the external). */    
  void badSideEffects4(aa_examples arg1, aa_examples arg2) {
    aa_examples q = returnBoth(arg1, arg2);
    q.x = 5;
  }

  /* Modifies the external. */    
  void badSideEffects5(aa_examples arg1, aa_examples arg2)
  {
    aa_examples p = new aa_examples(5);
    aa_examples q = p.returnExt(arg1);
    q.modThis();
  }

  /* Modifies arg1 (and the external). */    
  void badSideEffects6(aa_examples arg1, aa_examples arg2)
  {
    leaksArgs1(arg1, null);
    System.out.println();
  }

  /* Modifies the external. */    
  void badSideEffects7(aa_examples arg1, aa_examples arg2)
  {
    arg1 = new aa_examples(); // side-effects on allocate
  }    

  /* abc is modified and also returned. */
  aa_examples internalEffects() {
    aa_examples abc = new aa_examples(3, 4);
    aa_examples o = abc;
    badSideEffects5(null, null);
    return abc;
  }

}

  
  
