/* Example of how to use AM handlers from Titanium native code.

There are four AM handler slots reserved for use by titanium application code.

These are *only* usable on distributed-memory backends where COMM_AM2 is defined (all of them), 
it is not available on pure shared-memory backends, which lack AM functionality.

Applications that need more than four AM handlers should define their slots to be 
"dispatch handlers" that use the arguments to dispatch control to additional functions,
thereby multiplexing arbitrary number of handlers into one slot.

For details on the usage of the AM macros used in this file, see:
 runtime/backend/comm/tic_am2_macros.h

Test code:
class nat {
  public static native void sendAM();
  public static void main (String[] args) {
        sendAM();
  }
}

Place this file in native/T3nat.c and build with:
 tcbuild --cc-flags=-I. nativeam.ti

*/


#ifdef COMM_AM2
/* ----------- AM Request handler ----------------------- */
TI_INLINE(app_handler_req)
void app_handler_req(tic_amtoken_t token, void *addr, size_t nbytes, 
                     tic_handlerarg_t myint, void *myptr, jlong mylong, jdouble mydouble) {
 char str[] = "Goodbye world!";
 assert(strlen(addr) <= nbytes);
 printf("Node %i: Got AM Request! int=%i ptr=%p, long=%llx double=%f payload=%s\n",
        MYBOX, myint, myptr, (unsigned long long)mylong, mydouble, addr); fflush(stdout);

 /* send an AM reply */
 tic_AMReplyI(6,7,(token, TIC_AMIDX(apphandler_2), str, strlen(str)+1,
                          myint+1, TIC_AMSEND_PTR(myptr), TIC_AMSEND_JLONG(mylong+1), TIC_AMSEND_JDOUBLE(mydouble+1)));

}

/* declare handler, and define argument conversion for 32 and 64-bit */
TIC_AMMEDIUM(app_handler_req, 6, 7,
            (token,addr,nbytes, a0, TIC_AMRECV_PTR32(a1),     TIC_AMRECV_JLONG(a2,a3), TIC_AMRECV_JDOUBLE(a4,a5)),
            (token,addr,nbytes, a0, TIC_AMRECV_PTR64(a1, a2), TIC_AMRECV_JLONG(a3,a4), TIC_AMRECV_JDOUBLE(a5,a6)) );

/* register as first app handler */
tic_handler_fn_t tic_app_amhandler1 = TIC_AMWRAPPER_NAME(app_handler_req); 

/* ----------- AM Reply handler ----------------------- */
TI_INLINE(app_handler_rep)
void app_handler_rep(tic_amtoken_t token, void *addr, size_t nbytes, 
                     tic_handlerarg_t myint, void *myptr, jlong mylong, jdouble mydouble) {
 int *pflag = myptr;
 assert(strlen(addr) <= nbytes);
 printf("Node %i: Got AM Reply!   int=%i ptr=%p, long=%llx double=%f payload=%s\n",
        MYBOX, myint, myptr, (unsigned long long)mylong, mydouble, addr);
 *pflag = 1; /* set done flag */
}

/* declare handler, and define argument conversion for 32 and 64-bit */
TIC_AMMEDIUM(app_handler_rep, 6, 7,
            (token,addr,nbytes, a0, TIC_AMRECV_PTR32(a1),     TIC_AMRECV_JLONG(a2,a3), TIC_AMRECV_JDOUBLE(a4,a5)),
            (token,addr,nbytes, a0, TIC_AMRECV_PTR64(a1, a2), TIC_AMRECV_JLONG(a3,a4), TIC_AMRECV_JDOUBLE(a5,a6)) );

/* register as second app handler */
tic_handler_fn_t tic_app_amhandler2 = TIC_AMWRAPPER_NAME(app_handler_rep);

/* ----------- native code entry point ----------------------- */
void m6sendAMmT3nat() {
  if (MYPROC == 0) {
    char str[] = "Hello world!";
    int flag = 0; /* clear a done flag */
    printf("Sending AM...\n");
    tic_AMRequestI(6,7,(BOXES-1, TIC_AMIDX(apphandler_1), str, strlen(str)+1,
                        100, TIC_AMSEND_PTR(&flag), TIC_AMSEND_JLONG(0xFEDCBA9876543210ULL), TIC_AMSEND_JDOUBLE(100.234)));

    tic_poll_until(flag); /* wait for done flag */
    printf("AM transaction complete.\n");
  }
}
#else /* !COMM_AM2 */

void m6sendAMmT3nat() {
  printf("ERROR: no active messages available on this backend (eq sequential, smp) \n");
}

#endif
