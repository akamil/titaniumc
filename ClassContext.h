#ifndef _INCLUDE_ClassContext_H_
#define _INCLUDE_ClassContext_H_


class TreeNode;


class TreeNode::ClassContext // The static analysis context for class resolution
{
public:
  ClassContext(bool &);
  ClassContext(const ClassContext &, bool &);

  Decl *package;
  ClassDecl *cclass;
  Environ *fileEnv;
  bool methodsOnly;
  bool &postponed;
};

inline TreeNode::ClassContext::ClassContext(bool &_postponed)
  : package(0), cclass(0), fileEnv(0), methodsOnly(0), postponed(_postponed) {}

inline TreeNode::ClassContext::ClassContext(const ClassContext &ctx, 
					    bool &_postponed)
  : package(ctx.package), cclass(ctx.cclass), fileEnv(ctx.fileEnv), 
     methodsOnly(ctx.methodsOnly), postponed(_postponed) {}

#endif
