#ifndef _DOMINATOR_H_
#define _DOMINATOR_H_

void freeDomInfo(TreeNode *l);
bool appearsOnEveryIter(const TreeNode *e, const TreeNode *l);
void assertAppearsOnEveryIter(const TreeNode *e, const TreeNode *l);
bool dominated(treeSet *s, TreeNode *t, TreeNode *WRTloop);

#endif // _DOMINATOR_H_
