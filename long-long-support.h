#ifndef _TI_LONG_LONG_SUPPORT_H_
#define _TI_LONG_LONG_SUPPORT_H_

#if SIZEOF_LONG_LONG != SIZEOF_LONG

#include <values.h>
#include <iostream>
#include <assert.h>

template<typename _CharT, typename _Traits> static inline void
put_decimal(std::basic_ostream<_CharT, _Traits> &o, long long l)
{
  if (l <= (long long) MAXINT && l > -1000000)
    o << (int) l;
  else if (l < 0)
    o << '-' << l / -10 << -(l % (long long) 10);
  else {
    char s[100];
    int k = 100;
    s[--k] = '\0';
    do
      s[--k] = '0' + (char) (l % (long long) 10);
    while ((l /= 10) > 0);
    o << (s + k);
  }
}

template<typename _CharT, typename _Traits> static inline void
put_decimal(std::basic_ostream<_CharT, _Traits> &o, unsigned long long l)
{
  if (l <= (unsigned long long) MAXINT)
    o << (int) l;
  else
    o << (long long) (l / (unsigned long long) 10) <<
      (int) (l % (unsigned long long) 10);
}

template<typename _CharT, typename _Traits> static inline void
put_hex(std::basic_ostream<_CharT, _Traits> &o, unsigned long long l)
{
  static char hex[16] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
  char s[100];
  int k = 100;
  s[--k] = '\0';
  do
    s[--k] = hex[l & 0xf];
  while ((l /= 16) > 0);
  o << "0x" << (s + k);
}

template<typename _CharT, typename _Traits> static inline void
put_octal(std::basic_ostream<_CharT, _Traits> &o, unsigned long long l)
{
  char s[100];
  int k = 100;
  s[--k] = '\0';
  do
    s[--k] = '0' + (char) (l & 0x7);
  while ((l /= 8) > 0);
  o << "0" << (s + k);
}

template<typename _CharT, typename _Traits>
std::basic_ostream<_CharT, _Traits>& 
operator<<(std::basic_ostream<_CharT, _Traits> &o, long long __n)
{
  std::basic_ostream<_CharT, _Traits>::sentry __cerb(o);
  if (__cerb) {
    std::ios_base::fmtflags __fmt = o.flags() & std::ios_base::basefield;
    if (__fmt & std::ios_base::oct || __fmt & std::ios_base::hex)
      o << static_cast<unsigned long long>(__n);
    else
      put_decimal(o, __n);
  }
  return o;
}

template<typename _CharT, typename _Traits>
std::basic_ostream<_CharT, _Traits>& 
operator<<(std::basic_ostream<_CharT, _Traits> &o, unsigned long long __n)
{
  std::basic_ostream<_CharT, _Traits>::sentry __cerb(o);
  if (__cerb) {
    std::ios_base::fmtflags __fmt = o.flags() & std::ios_base::basefield;
    if (__fmt & std::ios_base::oct)
      put_octal(o, __n);
    else if (__fmt & std::ios_base::hex)
      put_hex(o, __n);
    else
      put_decimal(o, __n);
  }
  return o;
}

#endif /* SIZEOF_LONG_LONG != SIZEOF_LONG */
#endif /* _TI_LONG_LONG_SUPPORT_H_ */
