#ifndef _polytocode_h_
#define _polytocode_h_

#include <string>
#include "CodeContext.h"
#include "code-MIVE.h"

extern CodeContext * current_context;

string PolyToCode(const Poly *p, MIVEcontext *e, CodeContext &context);
string PolyToCode(const Poly *p, TreeNode *WRTloop, CodeContext &context);
void usedInPoly(const Poly *p, MIVEcontext *e, treeSet *s);
const string product(const string &a, const string &b);
const string quotient(const string &a, const string &b);
const string quotient_with_check(const string &a, const string &b,
				 const string &y);
const string quotient_with_runtime_unitdivisor_opt(const string &a, const string &b);

const string sum(const string &a, const string &b);
const string difference(const string &a, const string &b);


#endif // _polytocode_h_
