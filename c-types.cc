#include "AST.h"
#include "CtGlobal.h"
#include "CtInstance.h"
#include "CtJavaArray.h"
#include "CtLocal.h"
#include "CtObjectInstance.h"
#include "CtPointInstance.h"
#include "CtPrimitive.h"
#include "CtRegion.h"
#include "CtTitaniumArray.h"
#include "PrimitiveDecl.h"
#include "code-grid.h"
#include "compiler.h"
#include "ctBox.h"
#include "domain-decls.h"


const CtType &TypeNode::cTypeCreateBoxed() const
{
  const CtType &unboxed = decl()->cType();

  if (!isReference())
    return unboxed;
  else if (isLocal())
    return *new CtLocal( unboxed );
  else
    return *new CtGlobal( unboxed );
}


const CtType &TypeNode::cTypeCreateUnboxed() const
{
  ClassDecl &classDecl = static_cast< ClassDecl & >(*decl());
  assert( classDecl.category() & (Decl::Class | Decl::Interface) );
  assert( !classDecl.instantiations );

  if (isReference())
    return *new CtObjectInstance( classDecl );
  else
    return *new CtInstance( classDecl );
}


////////////////////////////////////////////////////////////////////////
//
//  creators for types with nonstandard representations
//

const CtType &DomainTypeNode::cTypeCreateUnboxed() const
{
  return *new CtObjectInstance( *DomainNDecl[ tiArity() - 1 ] );
}


const CtType &JavaArrayTypeNode::cTypeCreateUnboxed() const
{
  return *new CtJavaArray( elementType()->cType() );
}


const CtType &PointTypeNode::cTypeCreateUnboxed() const
{
  return *new CtPointInstance( tiArity() );
}


const CtType &PrimitiveTypeNode::cTypeCreateUnboxed() const
{
  return *new CtPrimitive( cPrimitiveTypeName() );
}


const CtType &RectDomainTypeNode::cTypeCreateUnboxed() const
{
  return *new CtInstance( *RectDomainNDecl[ tiArity() - 1 ] );
}


const CtType &TypeNameNode::cTypeCreateUnboxed() const
{
  if (decl() == RegionDecl)
    return CtRegion::singleton;
  else
    return TypeNode::cTypeCreateUnboxed();
}


////////////////////////////////////////////////////////////////////////
//
//  creators for Titanium arrays, which are even more nonstandard


const CtType &TitaniumArrayTypeNode::cTypeCreateUnboxed() const
{
  // only create a CtTitaniumArray once we know how to box the data
  return elementType()->cType();
}


const CtType &TitaniumArrayTypeNode::cTypeCreateBoxed() const
{
  // prepopulate underlying C type information to prevent boundless
  // recursion when inserting (PR #186)  
  elementType()->cType();

  // add to code generation set
  const TitaniumArrayTypeNode *p = this;
  if (arrayTypes.find(p) == arrayTypes.end()) {
    // make sure we get our own private copy so inference can't mess with the
    // modifiers and invalidate the set ordering or uniqueness,
    // and possibly cause some types to vanish entirely from the list (PR 446)
    arrayTypes.insert( p->deepClone() );
  }

  const CtType &unboxed = decl()->cType();
  const CtReference &data = ctBox( unboxed, isLocal() );
  return *new CtTitaniumArray( data, tiArity(), isLocal() );
}


// Local Variables:
// c-file-style: "gnu"
// End:
