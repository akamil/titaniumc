#ifndef INCLUDE_TITANIUM_TC_GC_H
#define INCLUDE_TITANIUM_TC_GC_H
#include "ti_config.h"

# ifdef USE_TC_GC
#  include "runtime/gc/include/gc_cpp.h"
# else  // no tc garbage collection

class gc { };
class gc_cleanup { };

# endif // no tc garbage collection

#endif // !INCLUDE_TITANIUM_TC_GC_H
