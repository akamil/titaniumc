#ifndef _TITANIUM_CODE_GRID_H_
#define _TITANIUM_CODE_GRID_H_

#include <iosfwd>
#include <string>
#include "ArraySet.h"

class TitaniumArrayTypeNode;
class TypeNode;

extern ArraySet arrayTypes;

extern string arrayMethodPrefix(const TypeNode *);
extern void instantiateArrays(void);
extern string arrayInternal(const string &fn, bool global);
extern void neededBy_arrays_c(string filename);

#endif
