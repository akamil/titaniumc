#ifndef _LGMACRO_H_
#define _LGMACRO_H_

#include <string>
#include "using-std.h"

class TypeNode;


// Compose a "_LOCAL" or "_GLOBAL" macro name

string lgMacro( const char base[],  bool local );
string lgMacro( const string &base, bool local );

string lgMacro( const char base[],  const TypeNode & );
string lgMacro( const string &base, const TypeNode & );


#endif // !_LGMACRO_H_
