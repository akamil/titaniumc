#ifndef _INCLUDE_DUMP_H_
#define _INCLUDE_DUMP_H_

#include <iosfwd>
#include <string>
#include "common.h"
#include "string16.h"

class Literal;
class Decl;
class TreeNode;


// An overloaded version of dump() must be provided for each
// attribute defined using "defattr" in "nodetypes.def".

void dump( ostream &, unsigned, Common::Modifiers );
void dump( ostream &, unsigned, bool );
void dump( ostream &, unsigned, const Literal & );
void dump( ostream &, unsigned, const string & );
void dump( ostream &, unsigned, const string * );
void dump( ostream &, unsigned, const string16 & );
void dump( ostream &, unsigned, const Decl * );
void dump( ostream &, unsigned, const TreeNode * );


#endif // !_INCLUDE_DUMP_H_
