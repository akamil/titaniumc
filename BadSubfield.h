#ifndef INCLUDE_BadSubfield_h
#define INCLUDE_BadSubfield_h

#include <string>
#include "using-std.h"

class Subfield;
class TreeNode;


class BadSubfield
{
public:
  BadSubfield( const Subfield & );

  const TreeNode &source;
  const string name;
};


#endif // !INCLUDE_BadSubfield_h
