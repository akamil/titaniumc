#include "AST.h"
#include "CodeContext.h"


void SwitchNode::emitStatement( CodeContext &context )
{
    const string selector = expr()->emitExpression( context );
    context << "switch (" << selector << ')' << endCline;
    
    CodeContext subcontext( context );
    switchBlocks()->codeGen( subcontext );
}


void CaseNode::codeGen( CodeContext &context )
{
    if (expr()->absent())
	context << "default:;" << endCline;
    else
	// This had better not produce any precomputation!
	context << "case " << expr()->emitExpression( context ) << ":;"
		<< endCline;
}
