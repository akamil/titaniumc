#ifndef _INCLUDE_AllocateContext_H_
#define _INCLUDE_AllocateContext_H_
#include "st-field.h"

class TreeNode;
class Decl;
class TypeNode;
class TreeListNode;

class TreeNode::AllocateContext // The static analysis AllocateContext for resolveField
{
public:
  AllocateContext(FieldContext *, TreeNode *);
  FieldContext *fieldContext(); // convert this into a FieldContext
  Decl *currentPackage;
  CompileUnitNode *currentCompileUnit;
  TypeNode *currentClass;
  TreeListNode *methodArgs;
  bool inStatic; // true while in static code
  Modifiers thisModifiers;
  Modifiers constructorSharingLUB;
  TreeNode *allocator;
};

inline TreeNode::AllocateContext::AllocateContext(FieldContext *ctx, TreeNode *_allocator)
  : currentPackage(ctx->currentPackage),
    currentCompileUnit(ctx->currentCompileUnit),
    currentClass(ctx->currentClass),
    methodArgs(ctx->methodArgs),
    inStatic(ctx->inStatic),
    thisModifiers(ctx->thisModifiers),
    constructorSharingLUB(ctx->constructorSharingLUB),
    allocator(_allocator)
{ 
}
  
inline TreeNode::FieldContext *TreeNode::AllocateContext::fieldContext()
{
  FieldContext *ctx = new FieldContext();
  ctx->currentPackage = currentPackage;
  ctx->currentCompileUnit = currentCompileUnit;
  ctx->currentClass = currentClass;
  ctx->methodArgs = methodArgs;
  ctx->inStatic = inStatic;
  ctx->thisModifiers = thisModifiers;
  ctx->constructorSharingLUB = ctx->constructorSharingLUB;
  return ctx;
}

#endif
