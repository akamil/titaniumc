#include <cassert>
#include "ClassDecl.h"
#include "MemberDecl.h"


MemberDecl::MemberDecl( const string *name,
			TypeNode *type,
			ClassDecl *container,
			Modifiers modifiers,
			TreeNode *source )
  : Decl( name ),
    _type( type ),
    _container( container ),
    _modifiers( modifiers ),
    _source( source )
{
}


////////////////////////////////////////////////////////////////////////


bool MemberDecl::hasType() const
{
  return true;
}


TypeNode* MemberDecl::type() const
{
  return _type;
}


void MemberDecl::type( TypeNode *newType )
{
  _type = newType;
}


////////////////////////////////////////////////////////////////////////


bool MemberDecl::hasContainer () const
{
  return true;
}


ClassDecl *MemberDecl::container () const
{
  return _container;
}


void MemberDecl::container (Decl * decl)
{
  assert (decl->category () & (Class | Interface));
  _container = static_cast < ClassDecl * >(decl);
}


////////////////////////////////////////////////////////////////////////


bool MemberDecl::hasModifiers () const
{
  return true;
}


Decl::Modifiers MemberDecl::modifiers () const
{
  return _modifiers;
}


void MemberDecl::modifiers (Modifiers mods)
{
  _modifiers = mods;
}


////////////////////////////////////////////////////////////////////////


bool MemberDecl::hasSource () const
{
  return true;
}


TreeNode *MemberDecl::source () const
{
  return _source;
}


void MemberDecl::source (TreeNode * tree)
{
  _source = tree;
}
