#include "garbageList.h"

vector<char *> garbageVector1, garbageVectorN;

void garbageListClear()
{
  // cout << "Freeing garbage list of size " << garbageListSize() << endl; 
  for (vector<char *>::iterator i = garbageVector1.begin();
       i != garbageVector1.end(); ++i)
    delete *i;
  for (vector<char *>::iterator i = garbageVectorN.begin();
       i != garbageVectorN.end(); ++i)
    delete[] *i;
  garbageVector1.clear();
  garbageVectorN.clear();
}
