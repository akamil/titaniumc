#include "AST.h"
#include "ArrayAccessSet.h"
#include "bitset.h"
#include "code-util.h"
#include "code.h"
#include "dominator.h"
#include "optimize.h"


#define DEBUG_FREE DEBUG_PHASE_ENABLED("free", currentFilename)


void TreeNode::free()
{
  for (int i = arity(); i-- > 0; )
    if (child(i) && !child(i)->absent()) child(i)->free();

  if (DEBUG_FREE)
    cout << "DELETE " << oper_name() << " " << this
	 << " under parent " << parent() << endl;
  TreeNode *parent = this->parent();
  if (parent) {
    for (int i=0; i < parent->arity(); i++) {
      if (parent->child(i) == this) {
	//parent->child(i, TreeNode::omitted); // remove the pointer to me
	// set it to an illegal address to detect dangling references
	parent->children[i] = (TreeNode *)0xDEADBEEF; // must be set explicitly
      }
    }
  }

  delete this;
}


void OmittedNode::free()
{
}


void TypeNode::free()
{
}

void VarDeclNode::free()
{
  // cleanup LocalVarDecl
  delete simpName()->decl();

  TreeNode::free();
}


////////////////////////////////////////////////////////////////////////


ExprNode::~ExprNode()
{
  delete getAbstractValues();
}


ForEachStmtNode::~ForEachStmtNode()
{
  extern void freeInvarInfo(const TreeNode *l);
  
  delete SR;
  delete arrayAccesses;
  freeDomInfo(this);
  freeInvarInfo(this);
}


MethodCallNode::~MethodCallNode()
{
  delete modifiesValues();
}


TreeNode::~TreeNode()
{
  // cleanup def/use chains (a HUGE win)
  free_all(getDefs());
  free_all(getUses());
}


VarDeclNode::~VarDeclNode() 
{
  // ident string
  delete _mangledIdent;
}

ReorderNode::~ReorderNode()
{
  delete stmtsToDelete();
  delete declsToDelete();
  delete remap();
}
