#ifndef _FIELD_DECL_H_
#define _FIELD_DECL_H_

#include <list>
#include <iosfwd>
#include <string>
#include "AST.h"
#include "MemberDecl.h"
#include "BadSubfield.h"

class ClassDecl;
class Subfield;
class TreeNode;
class TypeNode;


class FieldDecl : public MemberDecl {
public:
  FieldDecl (const string* name, TypeNode* type, 
	     ClassDecl* classOrIntf, Modifiers mods, TreeNode* source)
    : MemberDecl(name, type, classOrIntf, mods, source)
    {
    }

  const char* thisClassName() const;
  Category category() const;

  FieldDecl *memberforward(Subst *args);

  void collectEmbeddedLocals( list< BadSubfield > &, const Subfield * ) const;
  bool containsEmbeddedLocals() const;

  string fullName( char = '.' );
  string errorName();

  string cFieldName() const;
  const CtType &cType() const;

protected:
  void dumpAttributes( ostream & ) const;
};


inline const CtType &FieldDecl::cType() const
{
  return type()->cType();
}


#endif // !_FIELD_DECL_H_
