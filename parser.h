/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for GLR parsing with Bison,
   Copyright (C) 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     ABSTRACT = 258,
     ASSERT = 259,
     BOOLEAN = 260,
     BREAK = 261,
     BROADCAST = 262,
     BYTE = 263,
     CASE = 264,
     CATCH = 265,
     CHAR = 266,
     CLASS = 267,
     CONTINUE = 268,
     DEFAULT = 269,
     DO = 270,
     DOUBLE = 271,
     ELSE = 272,
     EXTENDS = 273,
     FINAL = 274,
     FINALLY = 275,
     FLOAT = 276,
     FOR = 277,
     FOREACH = 278,
     IF = 279,
     IMPLEMENTS = 280,
     IMPORT = 281,
     INSTANCEOF = 282,
     INT = 283,
     INTERFACE = 284,
     IMMUTABLE = 285,
     INLINE = 286,
     LOCAL = 287,
     LONG = 288,
     NATIVE = 289,
     NEW = 290,
     NULL_VAL = 291,
     OPERATOR = 292,
     OVERLAP = 293,
     _PACKAGE = 294,
     PRIVATE = 295,
     PROTECTED = 296,
     PUBLIC = 297,
     POLYSHARED = 298,
     NONSHARED = 299,
     PARTITION = 300,
     RETURN = 301,
     SHORT = 302,
     STATIC = 303,
     SUPER = 304,
     SWITCH = 305,
     SYNCHRONIZED = 306,
     STRICTFP = 307,
     SINGLE = 308,
     SGLOBAL = 309,
     TEMPLATE = 310,
     THIS = 311,
     THROW = 312,
     THROWS = 313,
     TRANSIENT = 314,
     TRY = 315,
     VOID = 316,
     VOLATILE = 317,
     WHILE = 318,
     TRUE_LITERAL = 319,
     FALSE_LITERAL = 320,
     IDENTIFIER = 321,
     INT_LITERAL = 322,
     LONG_LITERAL = 323,
     FLOAT_LITERAL = 324,
     DOUBLE_LITERAL = 325,
     CHARACTER_LITERAL = 326,
     STRING_LITERAL = 327,
     CAND = 328,
     COR = 329,
     EQ = 330,
     NE = 331,
     LE = 332,
     GE = 333,
     LSHIFTL = 334,
     ASHIFTR = 335,
     LSHIFTR = 336,
     PLUS_ASG = 337,
     MINUS_ASG = 338,
     MULT_ASG = 339,
     DIV_ASG = 340,
     REM_ASG = 341,
     LSHIFTL_ASG = 342,
     ASHIFTR_ASG = 343,
     LSHIFTR_ASG = 344,
     AND_ASG = 345,
     XOR_ASG = 346,
     OR_ASG = 347,
     PLUSPLUS = 348,
     MINUSMINUS = 349,
     GUARDS = 350
   };
#endif
/* Tokens.  */
#define ABSTRACT 258
#define ASSERT 259
#define BOOLEAN 260
#define BREAK 261
#define BROADCAST 262
#define BYTE 263
#define CASE 264
#define CATCH 265
#define CHAR 266
#define CLASS 267
#define CONTINUE 268
#define DEFAULT 269
#define DO 270
#define DOUBLE 271
#define ELSE 272
#define EXTENDS 273
#define FINAL 274
#define FINALLY 275
#define FLOAT 276
#define FOR 277
#define FOREACH 278
#define IF 279
#define IMPLEMENTS 280
#define IMPORT 281
#define INSTANCEOF 282
#define INT 283
#define INTERFACE 284
#define IMMUTABLE 285
#define INLINE 286
#define LOCAL 287
#define LONG 288
#define NATIVE 289
#define NEW 290
#define NULL_VAL 291
#define OPERATOR 292
#define OVERLAP 293
#define _PACKAGE 294
#define PRIVATE 295
#define PROTECTED 296
#define PUBLIC 297
#define POLYSHARED 298
#define NONSHARED 299
#define PARTITION 300
#define RETURN 301
#define SHORT 302
#define STATIC 303
#define SUPER 304
#define SWITCH 305
#define SYNCHRONIZED 306
#define STRICTFP 307
#define SINGLE 308
#define SGLOBAL 309
#define TEMPLATE 310
#define THIS 311
#define THROW 312
#define THROWS 313
#define TRANSIENT 314
#define TRY 315
#define VOID 316
#define VOLATILE 317
#define WHILE 318
#define TRUE_LITERAL 319
#define FALSE_LITERAL 320
#define IDENTIFIER 321
#define INT_LITERAL 322
#define LONG_LITERAL 323
#define FLOAT_LITERAL 324
#define DOUBLE_LITERAL 325
#define CHARACTER_LITERAL 326
#define STRING_LITERAL 327
#define CAND 328
#define COR 329
#define EQ 330
#define NE 331
#define LE 332
#define GE 333
#define LSHIFTL 334
#define ASHIFTR 335
#define LSHIFTR 336
#define PLUS_ASG 337
#define MINUS_ASG 338
#define MULT_ASG 339
#define DIV_ASG 340
#define REM_ASG 341
#define LSHIFTL_ASG 342
#define ASHIFTR_ASG 343
#define LSHIFTR_ASG 344
#define AND_ASG 345
#define XOR_ASG 346
#define OR_ASG 347
#define PLUSPLUS 348
#define MINUSMINUS 349
#define GUARDS 350




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 190 "parser.yy"
typedef union YYSTYPE {
    SimpTerminalInfo SimpTerminal;  	/* Terminal symbols with only position. */
    TerminalInfo Terminal;		/* Terminal symbols with ASCII string data. */
    StrTerminalInfo StrTerminal;	/* Terminal symbols with Unicode string data. */
    CharTerminalInfo CharTerminal;	/* Terminal symbols with Unicode char data. */
    long Int;				/* Simple integer values */
    bool Bool;
    CompileUnitNode* CompileUnit;
    TreeNode* Tree;
    TypeNode* Type;
    CatchNode* Catch;
    llist<TreeNode*>* TreeList;
    llist<TypeNode*>* TypeList;
    llist<CatchNode*>* CatchList;
    Common::Modifiers Modifiers;
    llist<DeclaratorTuple>* DeclaratorList;
    TryNode *Try;
    TypeDeclNode *TypeDecl;
} YYSTYPE;
/* Line 2319 of glr.c.  */
#line 243 "y.tab.h"
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;

#if ! defined (YYLTYPE) && ! defined (YYLTYPE_IS_DECLARED)
typedef struct YYLTYPE
{

  char yydummy;

} YYLTYPE;
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif



