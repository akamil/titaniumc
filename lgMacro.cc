#include <string>
#include "AST.h"
#include "lgMacro.h"


static const string suffix[] = {
  "_GLOBAL", "_LOCAL"
};


string lgMacro( const char base[], bool local )
{
  return base + suffix[ local ];
}


string lgMacro( const string &base, bool local )
{
  return base + suffix[ local ];
}


string lgMacro( const char base[], const TypeNode &type )
{
  return lgMacro( base, type.isLocal() );
}


string lgMacro( const string &base, const TypeNode &type )
{
  return lgMacro( base, type.isLocal() );
}
