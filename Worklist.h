#ifndef _WORKLIST_H_
#define _WORKLIST_H_

#include "llist.h"

llist<TreeNode *> *add_descendants_to_list(llist<TreeNode *> *l,
					   const TreeNode *t);

class Worklist {

 private:

  llist<TreeNode *> *l;

 public:

  Worklist() : l(NULL) {}
  Worklist(const TreeNode *t) : l(add_descendants_to_list(NULL, t)) {}

  ~Worklist() { free_all(l); }


  llist<TreeNode *> *asList() { return l; }

  bool contains(TreeNode *t) {
    for (llist<TreeNode *> *q = l; q; q = q->tail())
      if (q->front() == t) return true;
    return false;
  }
  
  void add_tree(TreeNode *t) {
    add(t);
    for (int i = t->arity(); --i >= 0; )
      add_tree(t->child(i));
  }

  void add_child_trees(TreeNode *t) {
    for (int i = t->arity(); --i >= 0; )
      add_tree(t->child(i));
  }
  
  void add(TreeNode *t) {
    if (!contains(t))
      l = cons(t, l);
  }

  Worklist *filter(bool f(const TreeNode *)) {
    l = destructive_filter(f, l);
    return this;
  }

  bool empty() { return (l == NULL); }

  TreeNode *pop() {
    TreeNode *r = l->front();
    l = l->free();
    return r;
  }

};

#endif /* _WORKLIST_H_ */
