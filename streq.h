#ifndef INCLUDE_STREQ_H
#define INCLUDE_STREQ_H

#include <cstring>
#include "using-std.h"


inline bool streq( const char *a, const char *b)
{
  return strcmp(a, b) == 0;
}


#endif // !INCLUDE_STREQ_H
