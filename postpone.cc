#include "AST.h"
#include "postpone.h"

bool debug_postpone = false;


void TreeNode::postpone( const char *ttask, const char *reason ) const
{
  if (debug_postpone)
    {
      message() << "node " << this << ": postponed "
		<< ttask << " due to " << reason << endl;
      
      print( cerr, 2 );
      cerr << endl;
    }
}
