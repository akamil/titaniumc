#ifndef _SUBST_H_
#define _SUBST_H_


class Substitution;
template< class T > class llist;

typedef llist< Substitution * > Subst;


#endif // _SUBST_H_
