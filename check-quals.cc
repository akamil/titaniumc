#include <AST.h>


void TypeNode::checkQualifiers() const
{
  if (modifiers() & NonsharedQ && modifiers() & PolysharedQ)
    error() << "type " << unqualifiedTypeName()
	    << " cannot have multiple sharing qualifiers" << endl;
      
  if (hasReference())
    {
      if (sharingEnforcement == Early)
	if (sharing() == Shared)
	  {
	    if (!decl()->allFieldsShared())
	      error() << decl()->errorName()
		      << " has fields that are not shared" << endl;
	  }
	else
	  if (!isLocal())
	    error() << "type " << unqualifiedTypeName()
		    << " cannot be both global and " << sharing() << endl;
    }
  else
    {
      enum { Mask = Local | NonsharedQ | PolysharedQ };
      const Modifiers suspect = (Modifiers) (modifiers() & Mask);

      if (suspect)
	error() << "type " << unqualifiedTypeName() << " cannot be made "
		<< stringifyModifiers( suspect ) << endl;
    }
}


void ArrayTypeNode::checkQualifiers() const
{
  if (sharingEnforcement == Early
      && sharing() == Shared
      && !elementType()->isSharedField())
    error() << "shared array must have shared elements" << endl;

  TypeNode::checkQualifiers();
  elementType()->checkQualifiers();
}
