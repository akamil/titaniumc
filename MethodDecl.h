#ifndef _METHOD_DECL_H_
#define _METHOD_DECL_H_


#include <iosfwd>
#include <string>
#include "MemberDecl.h"
#include "MethodSet.h"

class ClassDecl;
class MethodTypeNode;
class TreeNode;
class TypeNode;


class MethodDecl : public MemberDecl {
public:
  MethodDecl (const string *, TypeNode *, Category,
	      ClassDecl *, Modifiers, TreeNode *);
  
  TypeNode *thisType() const;

  MethodTypeNode* methodType() const;

  bool hasOverrides() const { return true; }
  Decl* overrides() const { return _overrides; }
  void overrides (Decl* decl) { _overrides = decl; }

  bool hasImplements() const { return true; }
  MethodSet &implements() { return _implements; }
  const MethodSet &implements() const { return _implements; }
  void implements( MethodDecl* decl ) { _implements.insert( decl ); }

  bool hasOverriders() const { return true; }
  MethodSet *overriders() { return &_overriders; }
  const MethodSet *overriders() const { return &_overriders; }
  void overriders(MethodDecl* decl) { _overriders.insert( decl ); }

  bool isDead() const { return _isDead; }
  void isDead(bool val) { _isDead = val; }

  typedef set< pair< Decl *, int> > DeclIntSet;
  DeclIntSet *initDepends() { return &_initDependencies; }
  void initDepends(Decl *, int);
  DeclIntSet *dependsOnInit() { return &_dependenciesOnInit; }
  void dependsOnInit(Decl *, int);

  const char* thisClassName() const;
  Category category() const;

  MethodDecl *memberforward(Subst *args);

  // Suitable signature to act as main() method?
  bool isMain();

  string fullName( char = '.' );
  string errorName();

  // Name for a method, resolved statically.
  const string cMethodNameStatic() const;
  const string cMethodNameDynamic() const;
  const string cMethodNameBare() const;

protected:
  void dumpAttributes( ostream & ) const;

  Decl *_overrides;
  MethodSet _implements;
  MethodSet _overriders;
  Category _category;
  bool _isDead;
  DeclIntSet _initDependencies;
  DeclIntSet _dependenciesOnInit;
};

    
#endif // !_METHOD_DECL_H_
