#include "AST.h"
#include "FieldsQueue.h"
#include "code-util.h"

FieldsQueue *unresolvedFields = new FieldsQueue();


void FieldsQueue::resolve()
{
  // Don't want new units to get added to the current queue.
  unresolvedFields = new FieldsQueue();

  while (!empty())
    {
      CompileUnitNode &unit = *front();
      pop_front();

      compile_status(2,string("field resolution: ") + *(unit.ident()));      
      unit.resolveField( NULL, NULL );
    }
  
  delete this;
}


// Local Variables:
// c-file-style: "gnu"
// End:
