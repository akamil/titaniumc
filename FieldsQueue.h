#ifndef INCLUDE_FIELDS_QUEUE_H
#define INCLUDE_FIELDS_QUEUE_H

#include <list>

class CompileUnitNode;


struct FieldsQueue : public list< CompileUnitNode * >
{
  void resolve();
};

extern FieldsQueue *unresolvedFields;


#endif //INCLUDE_FIELDS_QUEUE_H


// Local Variables:
// c-file-style: "gnu"
// End:
