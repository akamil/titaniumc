#ifndef SET_H
#define SET_H

#include "bool.h"
#include "linkage.h"
#include "regions.h"

EXTERN_C_BEGIN

typedef struct Set *set;
typedef struct Set_elt *set_scanner;

/* Fn for comparing set elements.  Should return 0 if e1 = e2, <0 if
   e1 < e2, and >0 if e1 > e2 */
typedef int (*set_cmp_fn)(void *e1, void *e2);

/* Make a new set, allocated in region r.  If bag is FALSE, don't
   store duplicates in set; otherwise allow duplicates. */
set set_new(region r, set_cmp_fn cmp, bool bag);

/* Return a copy of s */
set set_copy(region r, set s);

/* Return TRUE iff s is empty */
bool set_empty(set s);

/* Return TRUE if elt in s */
bool set_member(set s, void *elt);

/* Return the number of elements in s */
int set_size(set s);

/* Add elt to s.  If elt is not in s or s is a bag, return TRUE.
   Otherwise return FALSE. */
bool set_insert(set s, void *elt);

/* Remove all copies of elt from s. Return TRUE if elt was in s. */
bool set_remove(set s, void *elt);

/* Return TRUE if s1 <= s2 */
bool set_subset(set s1, set s2);

/* Return the union of s1 and s2.  Destructive operation */
set set_union(set s1, set s2);

/* Iterate over elements of s */
void set_scan(set s, set_scanner *ss);
void *set_next(set_scanner *ss);

#define scan_set(var, scanner, list) \
  for (set_scan(list, &scanner), *(void**)var = set_next(&scanner); \
       var; *(void**)var = set_next(&scanner))

EXTERN_C_END

#endif
