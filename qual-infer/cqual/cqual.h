#ifndef CQUAL_H
#define CQUAL_H

#include "bool.h"
#include "regions.h"
#include "linkage.h"

EXTERN_C_BEGIN

typedef enum {sev_err, sev_warn, sev_inf} severity;

/* Set to TRUE if any qualifier errors happened (unsatisfiable
   constraints) */
extern bool qerrors;

/* The type of a printf-like function */
typedef int (*printf_func)(const char *fmt, ...);

/* Load in a lattice configuration file */
void load_config_file_quals(const char *);

EXTERN_C_END

#endif
