#ifndef COMPILER_H
#define COMPILER_H

# if defined (__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#  define HAVE_C99
# endif

# if (defined(__GNUC__) || defined(HAVE_C99)) && !defined(__digital__)
#  define HAVE_VARIADIC_MACROS
# endif

# if defined(__GNUC__) || defined(HAVE_C99) || defined(__cplusplus)
#  define HAVE_INLINE
# endif

# if !defined(__GNUC__) && !defined(__attribute__)
#  define __attribute__(attributes)
# endif

#endif /* !COMPILER_H */
