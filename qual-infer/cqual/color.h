#ifndef COLOR_H
#define COLOR_H

#include "linkage.h"

#define pam_color_1          "pam-color-1"
#define pam_color_2          "pam-color-2"
#define pam_color_3          "pam-color-3"
#define pam_color_4          "pam-color-4"
#define pam_color_5          "pam-color-5"
#define pam_color_6          "pam-color-6"
#define pam_color_7	     "pam-color-7"
#define pam_color_8	     "pam-color-8"
#define pam_color_mouse      "pam-color-mouse"
#define pam_color_multiqual  "pam-color-5"
#define pam_color_noqual     "pam-color-1"
#define pam_color_error      "pam-color-8"
#define pam_color_warning    "pam-color-6"
#define pam_color_info       "pam-color-7"

/* Returns color we should use for lub of colors 1 and 2 */
EXTERN_C const char *combine_colors_pam(const char *color1, const char *color2);

#endif
