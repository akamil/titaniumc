#ifndef BOOL_H
#define BOOL_H

#ifndef __cplusplus
typedef unsigned char bool;
#endif

#define TRUE 1
#define FALSE 0

#endif
