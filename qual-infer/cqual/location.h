#ifndef INCLUDE_location_h
#define INCLUDE_location_h


#ifdef __cplusplus
class TreeNode;
#else
typedef struct TreeNode TreeNode;
#endif

typedef const TreeNode *location;


#endif /* !INCLUDE_location_h */
