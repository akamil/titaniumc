#ifndef UTIL_H
#define UTIL_H

#include <assert.h>
#include <stdarg.h>
#include <stdlib.h>
#include "compiler.h"
#include "regions.h"
#include "linkage.h"

EXTERN_C_BEGIN

#ifdef HAVE_VARIADIC_MACROS
#define fail(args...) __fail(__FILE__, __LINE__, __FUNCTION__, args)
#else
void fail(const char *fmt, ...);
#endif

void __fail(const char *file, unsigned int line,
	    const char *func, const char *fmt, ...) __attribute__ ((__noreturn__));


/* insist(action) is like assert(action), but action may have
   side-effects */
#ifdef NDEBUG
# define insist(action)  (action)
#else
# define insist assert
#endif

/* Concatenate 2 strings, allocating space in r for the result */
char *rstrcat(region, const char *, const char *);

/* Concatenate n strings, allocating space in r for the result.  The
   last argument should be a null pointer. */
char *rstrscat(region, ...);

/* Convert an integer to a string, storing the result in r */
const char *inttostr(region r, int);

/* sprintf a string, allocating space in r for the result */
char *rsprintf(region r, const char *fmt, ...);
char *rvsprintf(region r, const char *fmt, va_list args);

/* Convert a pointer to an ascii string with leading 0x.  Re-uses
   internal buffer. */
char *ptr_to_ascii(void *ptr);

#if 0
/* Convert a pointer to an integer */
int ptr_hash(void *ptr);
#endif

/* Return TRUE iff ptr1 == ptr2 */
bool ptr_eq(void *ptr1, void *ptr2);

/* Return TRUE iff s1 == s2 */
bool str_eq(const char *s1, const char *s2);

/* A total ordering on pointers.  Returns 0 if ptr1 = ptr2, a value <0
   if ptr1 < ptr2, or a value >0 if ptr1 > ptr2. */
int ptr_cmp(void *ptr1, void *ptr2);

/* Hash a string */
int string_hash(char *str);

/* Return TRUE iff s1 == s2 */
bool string_eq(char *s1, char *s2);

#ifdef HAVE_INLINE
static inline int min(int a, int b) { if (a < b) return a; else return b; }
static inline int max(int a, int b) { if (a < b) return b; else return a; }
#else
extern int min(int a, int b);
extern int max(int a, int b);
#endif

EXTERN_C_END

#endif
