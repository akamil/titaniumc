#ifndef REGIONS_H
#define REGIONS_H

#include <stdlib.h>
#include "../linkage.h"

EXTERN_C_BEGIN

typedef struct Region **region;

void region_init(void);
region newregion(void);
#define ralloc(r, type) ((type *)int_ralloc((r), sizeof(type)))
#define rarrayalloc(r, n, type) ((type *)int_rarrayalloc((r), (n), sizeof(type)))
void *int_ralloc(region r, size_t size);
void *int_rarrayalloc(region r, size_t n, size_t size);
char *rstralloc(region r, size_t size);
char *rstrdup(region r, const char *s);
/*region regionof(void *ptr);*/
void deleteregion(region r);
void deleteregion_ptr(region *r);

typedef void *(*nomem_handler)(region r, size_t n);
nomem_handler set_nomem_handler(nomem_handler newhandler);

EXTERN_C_END

#endif
