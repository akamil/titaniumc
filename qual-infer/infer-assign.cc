#include <AST.h>


void TreeNode::receiveAssign( const TypeNode &, QualSolver & ) const
{
  undefined( "receiveAssign" );
}


////////////////////////////////////////////////////////////////////////


void ArrayAccessNode::receiveAssign( const TypeNode &donor, QualSolver &solver ) const
{
  const TypeNode &outer = *array()->type();
  const TypeNode &inner = *outer.elementType();

  donor.weaklySubsumedBy( inner, solver );
}


void FieldAccessNode::receiveAssign( const TypeNode &donor, QualSolver &solver ) const
{
  donor.weaklySubsumedBy( *decl()->type(), solver );
}


void ObjectNode::receiveAssign( const TypeNode &donor, QualSolver &solver ) const
{
  donor.weaklySubsumedBy( *decl()->type(), solver );
}


void PointArrayAccessNode::receiveAssign( const TypeNode &, QualSolver & ) const
{
}
