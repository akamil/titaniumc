#include <AST.h>
#include <decls.h>
#include "QualSolver.h"


////////////////////////////////////////////////////////////////////////
//
// Domain operations appear in the AST as classes and methods from
// ti.internal.  But code is actually generated in terms of classes
// and methods from ti.domains.  So this analysis never actually gets
// to see the real methods that are invoked.  Furthermore, "foreach"
// on general domains makes method calls on several ti.domains utility
// classes; these calls are also not reflected in the AST.
//
// The only way to keep the analysis sound is to forbid inference on
// all domain-related types.  How tragic.
//
////////////////////////////////////////////////////////////////////////


void TypeNode::protect( QualSolver &solver ) const
{
  if (needsProtection())
    conservative( solver );
}


void ArrayTypeNode::protect( QualSolver &solver ) const
{
  TypeNode::protect( solver );
  elementType()->protect( solver );
}

////////////////////////////////////////////////////////////////////////


bool TypeNode::needsProtection() const
{
  return false;
}


////////////////////////////////////////////////////////////////////////


bool DomainTypeNode::needsProtection() const
{
  return true;
}


bool TypeNameNode::needsProtection() const
{
  Decl &package = *decl()->container();
  assert( package.category() == Decl::Package );

  return package.fullName() == "ti.domains";
}
