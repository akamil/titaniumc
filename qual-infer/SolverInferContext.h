#ifndef _INCLUDE_TITANIUM_IL_CONTEXT_H_
#define _INCLUDE_TITANIUM_IL_CONTEXT_H_

#include "InferContext.h"


template< class Solver > class SolverInferContext : public InferContext
{
public:
  SolverInferContext( Solver &, StaleBindings & );
  SolverInferContext( const SolverInferContext &, MethodTypeNode & );
  SolverInferContext( const SolverInferContext &, bool );

  Solver &solver;
};
  

////////////////////////////////////////////////////////////////////////


template< class Solver > inline
SolverInferContext< Solver >::SolverInferContext( Solver &solver, StaleBindings &stale )
  : InferContext( solver, stale ),
    solver( solver )
{
}


template< class Solver > inline
SolverInferContext< Solver >::SolverInferContext( const SolverInferContext &parent,
					MethodTypeNode &method )
  : InferContext( parent, method ),
    solver( parent.solver )
{
}
  

template< class Solver > inline
SolverInferContext< Solver >::SolverInferContext( const SolverInferContext &parent,
					bool isExported )
  : InferContext( parent, isExported ),
    solver( parent.solver )
{
}


#endif // !_INCLUDE_TITANIUM_IL_CONTEXT_H_
