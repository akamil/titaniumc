#include <cassert>

#include <AST.h>
#include <decls.h>
#include "EnactContext.h"
#include "code-util.h"

void DataDeclNode::enact( const EnactContext &context )
{
  if (dtype()->enactType( context ))
    reportChange( decl()->errorName(), *dtype() );
}


void FieldDeclNode::enact( const EnactContext &context )
{
  DataDeclNode::enact( context );
  context.enactFromType( *decl() );
}


void MethodNode::enact( const EnactContext &context )
{
  TypeNode &type = *decl()->type();
  const bool thisReference =
    !(decl()->modifiers() & Static) &&
    decl()->container()->asType()->isInferable();

  if (thisReference && type.enactType( context ))
    {
      context.enactFunction( *this );
      reportChange( "dispatch" );
      assert( !(decl()->modifiers() & Native) );
    }
  
  if (type.returnType()->enactType( context ))
    {
      reportChange( "return" );
      assert( !(decl()->modifiers() & Native) );
      if (!type.declaredReturnType()->enactType( context ))
	fatal_error("inference changed actual but not declared return type");
    }
}


void ParameterNode::enact( const EnactContext &context )
{
  if (dtype()->enactType( context ))
    reportChange( decl()->errorName(), *dtype() );
  if (isMethodDeclNode(parent()->parent())) 
    assert( !(parent()->parent()->decl()->modifiers() & Native) );
}
