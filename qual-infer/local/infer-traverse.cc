#include <AST.h>
#include <SolverInferContext.h>
#include <decls.h>
#include <template.h>


void TreeNode::inferLocalChildren( const LocalContext &context ) const
{
  const unsigned numChildren = arity();
  
  for (unsigned sweep = 0; sweep < numChildren; ++sweep)
    child( sweep )->inferLocalTree( context );
}


void TreeNode::inferLocal( const LocalContext &context )
{
  infer( context );
}


////////////////////////////////////////////////////////////////////////


void ConstructorDeclNode::inferLocalChildren( const LocalContext &context ) const
{
  LocalContext subcontext( context, *decl()->methodType() );
  TreeNode::inferLocalChildren( subcontext );
}


void MethodDeclNode::inferLocalChildren( const LocalContext &context ) const
{
  LocalContext subcontext( context, *decl()->methodType() );
  TreeNode::inferLocalChildren( subcontext );
}


void TypeDeclNode::inferLocalChildren( const LocalContext &context ) const
{
  ClassDecl &me = *decl();

  if (decl()->isArrayInstance()) {
      const string *cloneName = intern( "clone" );
      const Decl * const cloneMethod = me.environ()->lookupProper( cloneName, Decl::Method );
      if (cloneMethod)
        {
          assert( cloneMethod->modifiers() & Native );
          cloneMethod->type()->conservative( *(QualSolver*)&context.solver );
        }
      const string *localcloneName = intern( "localClone" );
      const Decl * const localcloneMethod = me.environ()->lookupProper( localcloneName, Decl::Method );
      if (localcloneMethod)
        {
          assert( localcloneMethod->modifiers() & Native );
          localcloneMethod->type()->conservative( *(QualSolver*)&context.solver );
        }                                                                            
  } else {
      TreeNode::inferLocalChildren( context );
  }
}
