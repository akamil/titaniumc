#include <utility>

#include <AST.h>
#include <PhaseTimer.h>
#include <SolverInferContext.h>
#include <StaleBindings.h>
#include <compiler.h>
#include <st-field.h>
#include <template.h>
#include <typecheck.h>
#include <using-std.h>
#include <utils.h>
#include <optimize.h>

#include "enact-context.h"
#include "solver.h"
#include "stats.h"

extern void doSanityTypeCheck();

void inferLocal()
{
    PhaseTimer timer( "local qualification inference" );
    
    llist< TreeNode * > *allTrees = templateEnv.allInstances();
    foreach( unit, llist< CompileUnitNode * >, *allFiles )
      push( allTrees, (*unit) );
    
    foreach( tree, llist< TreeNode * >, *allTrees )
      (*tree)->unshareTypeTree();

    LocalSolver solver;
    StaleBindings stale;
    LocalContext context( solver, stale );
    foreach( tree, llist< TreeNode * >, *allTrees )
      (*tree)->inferLocalTree( context );

    // solver.dumpSolutions();

    foreach( tree, llist< TreeNode * >, *allTrees )
      (*tree)->localEnactTree( solver );

    free_all( allTrees );
    stale.rebindAll();
    templateEnv.rehash();
    
    doSanityTypeCheck(); // sanity checking
    
    if (debug_qual)
      LocalEnactContext::printStats( cerr );
}
