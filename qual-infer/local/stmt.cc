#include <AST.h>
#include <SolverInferContext.h>
#include <decls.h>

#include "solver.h"


void CatchNode::inferLocal( const LocalContext &context )
{
  context.solver.mustBeGlobal( *param()->dtype() );
}
