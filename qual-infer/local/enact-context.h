#ifndef _INCLUDE_TITANIUM_LOCAL_ENACT_CONTEXT_H_
#define _INCLUDE_TITANIUM_LOCAL_ENACT_CONTEXT_H_

#include <iosfwd>

#include <EnactContext.h>
#include "stats-map.h"

class LocalSolver;
class PackageDecl;


class LocalEnactContext : public EnactContext
{
public:
  LocalEnactContext( const LocalSolver & );
  LocalEnactContext( const LocalEnactContext &, const bool & );
  LocalEnactContext( const LocalEnactContext &, PackageDecl * );

  bool enactBits( const TreeNode &, Modifiers &, bool ) const;
  void enactFromType( Decl & ) const;
  Modifiers flagsMask() const;
  
  static void printStats( ostream & );

  const bool * const thisLocal;
  
private:
  const LocalSolver &solver;
  LocalStats &stats;
  
  static void printStat( ostream &, PackageDecl *, const LocalStats & );
  static LocalStatsMap statsMap;
};


////////////////////////////////////////////////////////////////////////


inline LocalEnactContext::LocalEnactContext( const LocalSolver &solver )
  : thisLocal( 0 ),
    solver( solver ),
    stats( statsMap[ 0 ] )
{
}


inline LocalEnactContext::LocalEnactContext( const LocalEnactContext &prior, const bool &thisLocal )
  : thisLocal( &thisLocal ),
    solver( prior.solver ),
    stats( prior.stats )
{
}


inline LocalEnactContext::LocalEnactContext( const LocalEnactContext &prior, PackageDecl *package )
  : thisLocal( prior.thisLocal ),
    solver( prior.solver ),
    stats( statsMap[ package ] )
{
}


#endif // !_INCLUDE_TITANIUM_LOCAL_ENACT_CONTEXT_H_
