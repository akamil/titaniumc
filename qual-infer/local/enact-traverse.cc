#include <AST.h>
#include <code-util.h>
#include <decls.h>
#include "enact-context.h"
#include "stats.h"


void TreeNode::localEnactChildren( const LocalEnactContext &context ) const
{
  const unsigned numChildren = arity();
  for (unsigned sweep = 0; sweep < numChildren; ++sweep)
    child( sweep )->localEnactTree( context );
}


void TreeNode::localEnactTree( const LocalEnactContext &context )
{
  enact( context );
  localEnactChildren( context );
}


void TemplateDeclNode::localEnactTree( const LocalEnactContext & )
{
}


////////////////////////////////////////////////////////////////////////


void CompileUnitNode::localEnactChildren( const LocalEnactContext &context ) const
{
  PackageDecl * const decl = static_cast< PackageDecl * >( thePackage );
  assert( decl->category() == Decl::Package );

  const LocalEnactContext subcontext( context, decl );
  TreeNode::localEnactChildren( subcontext );

  if (DEBUG_PHASE_ENABLED( "infer-local", ident()->c_str() ))
    {
      cout << "after local inference:\n";
      print( cout, 1 );
      cout << '\n' << endl;
    }
}
  

void ConstructorDeclNode::localEnactChildren( const LocalEnactContext &context ) const
{
  const bool thisLocal = !(decl()->container()->modifiers() & Immutable);
  const LocalEnactContext subcontext( context, thisLocal );
  TreeNode::localEnactChildren( subcontext );
}


void FieldDeclNode::localEnactChildren( const LocalEnactContext &context ) const
{
  const bool thisLocal = !(decl()->container()->modifiers() & Immutable);
  const LocalEnactContext subcontext( context, thisLocal );
  TreeNode::localEnactChildren( subcontext );
}


void MethodDeclNode::localEnactChildren( const LocalEnactContext &context ) const
{
  // !! potential problem: disagreement between decl modifiers and
  // !! decl type modifiers
  
  const bool thisLocal = decl()->type()->isLocal();
  const LocalEnactContext subcontext( context, thisLocal );
  TreeNode::localEnactChildren( subcontext );
}


void MethodDeclNode::localEnactTree( const LocalEnactContext &context )
{
  if (!(decl()->modifiers() & Native))
    TreeNode::localEnactTree( context );
}


void TypeDeclNode::localEnactChildren( const LocalEnactContext &context ) const
{
  if (!decl()->isArrayInstance())
    TreeNode::localEnactChildren( context );
}
