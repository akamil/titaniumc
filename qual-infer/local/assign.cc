#include <AST.h>
#include <SolverInferContext.h>
#include <decls.h>

#include "solver.h"


void AssignNode::inferLocal( const LocalContext &context )
{
  opnd0()->localReceiveAssign( *opnd1()->type(), context.solver );
  infer( context );
}

////////////////////////////////////////////////////////////////////////


void TreeNode::localReceiveAssign( const TypeNode &donor, LocalSolver &solver ) const
{
  receiveAssign( donor, solver );
}


////////////////////////////////////////////////////////////////////////


void ArrayAccessNode::localReceiveAssign( const TypeNode &donor, LocalSolver &solver ) const
{
  TreeNode::localReceiveAssign( donor, solver );
  
  const TypeNode &outer = *array()->type();
  const TypeNode &inner = *outer.elementType();
  solver.subsumedBy( outer, inner );
}


void ObjectFieldAccessNode::localReceiveAssign( const TypeNode &donor, LocalSolver &solver ) const
{
  TreeNode::localReceiveAssign( donor, solver );

  const TypeNode &outer = *accessedObjectType();
  const TypeNode &inner = *decl()->type();
  solver.subsumedBy( outer, inner );
}
