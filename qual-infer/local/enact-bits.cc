#include <AST.h>
#include "enact-context.h"
#include "solver.h"


bool LocalEnactContext::enactBits( const TreeNode &node,
				   Modifiers &modifiers,
				   bool interesting ) const
{
  const bool newLocal = solver.solution( node );
  const bool oldLocal = modifiers & Common::Local;
  unsigned *stat = 0;

  if (newLocal)
    if (oldLocal)
      stat = &stats.alreadyLocal;
    else
      {
	(int &) modifiers |= Common::Local | Common::LocalInferred;
	stat = &stats.localized;
      }
  else
    if (oldLocal)
      {
	node.error() << "local inference removed explicit locality qualification" << endl;
	(int &) modifiers &= ~Common::Local;
      }
    else
      stat = &stats.alreadyGlobal;

  if (interesting && stat)
    ++*stat;

  return oldLocal != newLocal;
}
