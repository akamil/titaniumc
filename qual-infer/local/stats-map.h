#ifndef _INCLUDE_TITANIUM_STATS_MAP_H_
#define _INCLUDE_TITANIUM_STATS_MAP_H_

#include <map>
#include "stats.h"

class PackageDecl;


typedef map< PackageDecl *, LocalStats, less< PackageDecl * > >  LocalStatsMap;


#endif // !_INCLUDE_TITANIUM_STATS_MAP_H_
