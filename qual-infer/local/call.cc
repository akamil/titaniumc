#include <AST.h>
#include <SolverInferContext.h>
#include <StaleBindings.h>
#include <decls.h>

#include "solver.h"


void MethodCallNode::inferLocal( const LocalContext &context )
{
  infer( context );
  
  const TypeNode &dispatcher = *method()->accessedObjectType();

  if (dispatcher.isTitaniumArrayType())
    {
      static const string * const copy = intern( "copy" );
      static const string * const scatter = intern( "scatter" );
      static const string * const gather = intern( "gather" );
      static const string * const exchange = intern( "exchange" );
      static const string * const set = intern( "set" );
	  
      const string * const name = decl()->name();
      const TypeNode &element = *dispatcher.elementType();
      
      if (name == copy || name == scatter || name == gather) {
          // ensure that we never copy local elements over a global array pointer
	  context.solver.subsumedBy( *args()->child( 0 )->type(), element );
	  context.solver.subsumedBy( dispatcher, element );
      } else if (name == set) {
          // ensure that we never copy local elements over a global array pointer
	  context.solver.subsumedBy( dispatcher, element );
      } else if (name == exchange) {
	context.solver.mustBeGlobal( element );
        if (element.isImmutable() && !element.isTitaniumArrayType())
          context.solver.mustBeGlobal( *element.decl()->source() );
      }
    }  	  
}
