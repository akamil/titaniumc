#ifndef _INCLUDE_TITANIUM_LOCAL_STATS_H_
#define _INCLUDE_TITANIUM_LOCAL_STATS_H_

#include <iosfwd>
#include <using-std.h>


class LocalStats
{
public:
  LocalStats();
  void print( ostream & ) const;

  unsigned localized;
  unsigned alreadyLocal;
  unsigned alreadyGlobal;

private:
  void print( ostream &, int, int, const char [] ) const;
};


inline ostream & operator << ( ostream &sink, const LocalStats &stats )
{
  stats.print( sink );
  return sink;
}


#endif // !_INCLUDE_TITANIUM_LOCAL_STATS_H_
