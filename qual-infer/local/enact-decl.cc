#include <AST.h>
#include <decls.h>
#include "enact-context.h"


void Decl::localEnactFromType()
{
  if (type()->isLocal())
    modifiers( (Modifiers) (modifiers() | Common::Local) );
  else
    assert( !(modifiers() & Common::Local) );
}


void LocalEnactContext::enactFromType( Decl &decl ) const
{
  decl.localEnactFromType();
}
