#include <AST.h>
#include <SolverInferContext.h>
#include <decls.h>

#include "solver.h"


void BroadcastNode::inferLocal( const LocalContext &context )
{
  const TypeNode &exprType = *expr()->type();
  
  if (type()->isInferable())
    context.solver.mustBeGlobal( *type() );
  
  if (exprType.isImmutable() && !exprType.isTitaniumArrayType())
    context.solver.mustBeGlobal( *exprType.decl()->source() );

  infer( context );
}
