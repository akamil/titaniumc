#include <iostream>
#include "stats.h"


LocalStats::LocalStats()
  : localized( 0 ),
    alreadyLocal( 0 ),
    alreadyGlobal( 0 )
{
}


void LocalStats::print( ostream &sink, int fraction, int total, const char label[] ) const
{
  sink << fraction << " (" << 100 * fraction / total << "%) " << label;
}


void LocalStats::print( ostream &sink ) const
{
  const unsigned total = localized + alreadyLocal + alreadyGlobal;

  if (total)
    {
      print( sink, localized, total, "localized" );
      sink << ", ";
      print( sink, alreadyLocal, total, "declared local" );
      sink << ", ";
      print( sink, alreadyGlobal, total, "remain global" );
    }
  else
    sink << "no declared references";
}
