#include <AST.h>
#include <SolverInferContext.h>
#include "solver.h"


#ifdef IMMUTABLES_HAVE_SUPERCLASSES
void ClassDeclNode::inferLocal( const LocalContext &context )
{
  if (flags() & Immutable)
    {
      const ClassDecl * const extends = decl()->superClass();
      if (extends)
	context.solver.subsumedBy( *this, extends->source() );
    }
}
#endif // IMMUTABLES_HAVE_SUPERCLASSES


void FieldDeclNode::inferLocal( const LocalContext &context )
{
  const Decl &container = *decl()->container();
  const TypeNode &myType = *dtype();
  
  if (container.asType()->isImmutable())
    context.solver.subsumedBy( *container.source(), myType );

  if (myType.isImmutable() && !myType.isTitaniumArrayType())
    context.solver.subsumedBy( myType, *myType.decl()->source() );

  infer( context );
}
