#include <algorithm>
#include <iostream>

#include <decls.h>
#include "enact-context.h"
#include "solver.h"
#include "stats.h"
#include "stats-map.h"


LocalStatsMap LocalEnactContext::statsMap;


Common::Modifiers LocalEnactContext::flagsMask() const
{
  return Local;
}


void LocalEnactContext::printStat( ostream &sink, PackageDecl *package, const LocalStats &stats )
{
  sink << "lqi stats: "
       << (package ? package->errorName() : "no package")
       << ": " << stats << endl;
}


void LocalEnactContext::printStats( ostream &sink )
{
  for (LocalStatsMap::const_iterator sweep = statsMap.begin();
       sweep != statsMap.end(); ++sweep)
    printStat( sink, sweep->first, sweep->second );
}
