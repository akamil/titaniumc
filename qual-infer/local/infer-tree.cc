#include <AST.h>
#include <SolverInferContext.h>
#include <decls.h>


void TreeNode::inferLocalTree( const LocalContext &context )
{
  inferLocalChildren( context );
  inferLocal( context );
}


void TreeNode::inferLocalTreeExport( const LocalContext &context )
{
  const LocalContext subcontext( context, isExported() );
  TreeNode::inferLocalTree( subcontext );
}


////////////////////////////////////////////////////////////////////////


void CompileUnitNode::inferLocalTree( const LocalContext &context )
{
  inferLocalTreeExport( context );
}


void ConstructorDeclNode::inferLocalTree( const LocalContext &context )
{
  inferLocalTreeExport( context );
}


void FieldDeclNode::inferLocalTree( const LocalContext &context )
{
  inferLocalTreeExport( context );
}


void MethodNode::inferLocalTree( const LocalContext &context )
{
  inferLocalTreeExport( context );
}


void TemplateDeclNode::inferLocalTree( const LocalContext & )
{
}


void TypeDeclNode::inferLocalTree( const LocalContext &context )
{
  inferLocalTreeExport( context );
}
