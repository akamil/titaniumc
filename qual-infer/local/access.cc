#include <AST.h>
#include <SolverInferContext.h>
#include <decls.h>
#include "solver.h"


static void handleAccess( QualSolver &solver,
			  const TypeNode *outer,
			  const TypeNode &inner,
			  const TypeNode &result )
{
  inner.weaklySubsumedBy( result, solver );
  if (outer)
    solver.subsumedBy( *outer, result );
}
			       
      

void ArrayAccessNode::inferLocal( const LocalContext &context )
{
  const TypeNode &outer = *array()->type();
  const TypeNode &inner = *outer.elementType();
  const TypeNode &result = *type();

  handleAccess( context.solver, &outer, inner, result );

  // AK (PR427): Handle arrays of immutables with embedded references here;
  //             the immutable class is forced to be global if the result of
  //             this array access is global, in turn forcing all embedded
  //             references to be global
  if (result.isImmutable() && !result.isTitaniumArrayType())
    context.solver.subsumedBy( result, *inner.decl()->source() );
}


void ObjectFieldAccessNode::inferLocal( const LocalContext &context )
{
  const TypeNode * const outer  = (decl()->modifiers() & Common::Static) ? 0 : accessedObjectType();
  const TypeNode &       inner  = *decl()->type();
  const TypeNode &       result = *type();
  
  handleAccess( context.solver, outer, inner, result );

  if (// fieldChainEnd() &&
      // AK (PR427): I have no idea what the code above is supposed to accomplish,
      //             but it seems to me that a FieldAccessNode is a field access iff
      //             its category is Decl::Field (as opposed to Decl::Method)
      decl()->category() == Decl::Field &&
      result.isImmutable() && !result.isTitaniumArrayType())
    context.solver.subsumedBy( result, *decl()->source()->dtype() );
}


void TypeFieldAccessNode::inferLocal( const LocalContext &context )
{
  assert( decl()->modifiers() & Static );

  const TypeNode * const outer = 0;
  const TypeNode &       inner = *decl()->type();

  handleAccess( context.solver, outer, inner, *type() );
}
