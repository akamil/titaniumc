#include <AST.h>
#include <quals.h>
#include "solver.h"


LocalSolver::LocalSolver()
{
  begin_po_qual();
  
  local = add_qual( "local" );
  global = add_qual( "global" );

  add_qual_lt( local, global );

  end_po_qual();
  end_define_pos();
}


bool LocalSolver::solution( const TreeNode &node ) const
{
  const qual var = qvar( node );
  if (var)
    {
      const qual_set lowers = lbc_qual( var );
      qual lower;
      qual_set_scanner scanner;
      
      scan_quals( lower, scanner, lowers )
	if (lower == global)
	  return false;
    }

  return true;
}
