#include <AST.h>
#include <SolverInferContext.h>
#include "solver.h"


void DynamicTypeNode::inferLocal( const LocalContext &context )
{
  opnd0()->type()->localCastTo( *dtype(), context.solver );
  infer( context );
}


////////////////////////////////////////////////////////////////////////


void TypeNode::localCastTo( const TypeNode &target, LocalSolver &solver ) const
{
  if (!target.isLocal())
    solver.subsumedBy( *this, target );
}


////////////////////////////////////////////////////////////////////////


void ArrayTypeNode::localCastTo( const TypeNode &target, LocalSolver &solver ) const
{
  TypeNode::localCastTo( target, solver );
  
  if (target.isArrayType())
    elementType()->subcastTo( *target.elementType(), solver );
  else
    elementType()->conservative( solver );
}


void DomainTypeNode::localCastTo( const TypeNode &target, LocalSolver &solver ) const
{
  if (!target.isRectDomainType())
    TypeNode::localCastTo( target, solver );
}


void TypeNameNode::localCastTo( const TypeNode &target, LocalSolver &solver ) const
{
  TypeNode::localCastTo( target, solver );

  if (target.isArrayType())
    target.elementType()->conservative( solver );
}
