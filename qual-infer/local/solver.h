#ifndef _INCLUDE_TITANIUM_LOCAL_QUAL_H_
#define _INCLUDE_TITANIUM_LOCAL_QUAL_H_

#include <cassert>
#include <QualSolver.h>
#include <quals.h>

class TypeNode;


class LocalSolver : public QualSolver
{
public:
  LocalSolver();
  
  void mustBeGlobal( const TreeNode & );

  bool solution( const TreeNode & ) const;
  
protected:
  qual qconst( const TypeNode & ) const;

private:
  qual local;
  qual global;
};


////////////////////////////////////////////////////////////////////////


inline void LocalSolver::mustBeGlobal( const TreeNode &type )
{
  unify_qual( qvar( type ), global );
}


inline qual LocalSolver::qconst( const TypeNode &type ) const
{
  return type.isLocal() ? local : global;
}


#endif // !_INCLUDE_TITANIUM_LOCAL_QUAL_H_
