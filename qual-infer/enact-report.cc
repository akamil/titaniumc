#include <AST.h>
#include <decls.h>
#include <optimize.h>

void TreeNode::reportChange( const string &context, const TypeNode &type ) const
{
  if (debug_qual)
    message() << context << " : <" << type.typeName() << '>' << endl;
}


////////////////////////////////////////////////////////////////////////


void MethodNode::reportChange( const char *context ) const
{
  if (debug_qual)
    TreeNode::reportChange( decl()->errorName() + ' ' + context, *decl()->type() );
}
