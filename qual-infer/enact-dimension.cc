#include <AST.h>
#include "EnactContext.h"


void AllocateArrayNode::enact( const EnactContext &context )
{
  ExprNode::enact( context );

  bool changed = dtype()->enactType( context );
  changed |= dimExprs()->enactDimension( context );

  if (changed)
    reportChange( "array allocation", *type() );
}


////////////////////////////////////////////////////////////////////////


bool TreeListNode::enactDimension( const EnactContext &context )
{
  bool changed = false;
  foriter (child, allChildren(), ChildIter)
    {
      AllocateArrayDimensionNode &dimension = static_cast< AllocateArrayDimensionNode & >( **child );
      assert( !strcmp( dimension.oper_name(), "AllocateArrayDimensionNode" ) );
      changed |= dimension.enactDimension( context );
    }
  return changed;
}


bool AllocateArrayDimensionNode::enactDimension( const EnactContext &context )
{
  return context.enactBits( *this, _flags, true );
}
