#ifndef INCLUDE_TITANIUM_INFER_CONTEXT_H
#define INCLUDE_TITANIUM_INFER_CONTEXT_H

class MethodTypeNode;
class QualSolver;
class StaleBindings;


class InferContext
{
public:
  InferContext( QualSolver &, StaleBindings & );
  InferContext( const InferContext &, MethodTypeNode & );
  InferContext( const InferContext &, bool );

  QualSolver &solver;
  StaleBindings &stale;
  MethodTypeNode * const method;
  const bool isExported;
};
  

inline InferContext::InferContext( QualSolver &solver, StaleBindings &stale )
  : solver( solver ),
    stale( stale ),
    method( 0 ),
    isExported( true )
{
}


inline InferContext::InferContext( const InferContext &parent,
				   MethodTypeNode &method )
  : solver( parent.solver ),
    stale( parent.stale ),
    method( &method ),
    isExported( parent.isExported )
{
}
  

inline InferContext::InferContext( const InferContext &parent,
				   bool isExported )
  : solver( parent.solver ),
    stale( parent.stale ),
    method( parent.method ),
    isExported( isExported )
{
}
  

#endif // !INCLUDE_TITANIUM_INFER_CONTEXT_H
