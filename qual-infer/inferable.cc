#include "AST.h"


bool TreeNode::isInferable() const
{
  unimplemented( "isInferable" );
  return false;
}


bool AllocateArrayDimensionNode::isInferable() const
{
  return true;
}


bool TypeNode::isInferable() const
{
  return hasReference();
}
