#include <AST.h>
#include <ClassDecl.h>
#include <SolverInferContext.h>
#include <solver.h>


void ClassDeclNode::inferSharingEarly( SharingSolver &solver ) const
{
  ClassDecl &me = *decl();
  if (me.asType()->isInferable())
    {
      const ClassDecl * const extends = me.superClass();

      if (extends)
	solver.propagateShared( *this, *extends->source() );
    }
}


void FieldDeclNode::inferSharingEarly( SharingSolver &solver ) const
{
  const Decl &me = *decl();
  if (!(me.modifiers() & Static))
    {
      TypeNode &fieldType = *dtype();
      if (fieldType.isInferable())
	solver.propagateShared( *me.container()->source(), fieldType );
    }
}


void MethodNode::inferSharingEarly( SharingSolver &solver ) const
{
  const MethodDecl &me = *decl();
  
  if (!(me.modifiers() & (Static | Local)))
    solver.mustBeShared( *me.type() );
}
