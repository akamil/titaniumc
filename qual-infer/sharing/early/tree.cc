#include <AST.h>


void TreeNode::inferSharingEarlyChildren( SharingSolver &solver ) const
{
  const unsigned numChildren = arity();
  
  for (unsigned sweep = 0; sweep < numChildren; ++sweep)
    child( sweep )->inferSharingEarlyTree( solver );
}


void TreeNode::inferSharingEarlyTree( SharingSolver &solver ) const
{
  inferSharingEarlyChildren( solver );
  inferSharingEarly( solver );
}


void TreeNode::inferSharingEarly( SharingSolver & ) const
{
}
