#include <AST.h>
#include <SolverInferContext.h>
#include <solver.h>


void ArrayTypeNode::inferSharingEarly( SharingSolver &solver ) const
{
  const TypeNode &element = *elementType();
  solver.propagateShared( *this, element );

  TypeNode::inferSharingEarly( solver );
}


void TypeNameNode::inferSharingEarly( SharingSolver &solver ) const
{
  solver.propagateShared( *this, *decl()->source() );
  
  TypeNode::inferSharingEarly( solver );
}

void TemplateInstanceTypeNode::inferSharingEarly( SharingSolver &solver ) const
{
  solver.propagateShared( *this, *decl()->source() );
  
  TypeNode::inferSharingEarly( solver );
}


void TypeNode::inferSharingEarly( SharingSolver &solver ) const
{
  if (isInferable() && !isLocal())
    solver.mustBeShared( *this );
}
