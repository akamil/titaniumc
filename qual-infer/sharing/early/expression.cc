#include <AST.h>
#include <solver.h>


void BroadcastNode::inferSharingEarly( SharingSolver &solver ) const
{
  solver.mustBeShared( *expr()->type() );
}
