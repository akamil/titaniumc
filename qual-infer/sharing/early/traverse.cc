#include <AST.h>


void TemplateDeclNode::inferSharingEarlyTree( SharingSolver & ) const
{
}


void TypeDeclNode::inferSharingEarlyChildren( SharingSolver &solver ) const
{
  if (!decl()->isArrayInstance())
    members()->inferSharingEarlyTree( solver );
}


void TypeFieldAccessNode::inferSharingEarlyChildren( SharingSolver & ) const
{
}
