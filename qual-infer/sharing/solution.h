#ifndef INCLUDE_TITANIUM_SHARING_SOLUTION_H
#define INCLUDE_TITANIUM_SHARING_SOLUTION_H

#include <map>

#include <quals.h>
#include <sharing.h>
#include <using-std.h>

class TreeNode;


class SharingSolution : private map< const TreeNode *, Sharing >
{
public:
  void discover( const TreeNode &, Sharing );
  Sharing operator [] ( const TreeNode & ) const;
};


#endif // !INCLUDE_TITANIUM_QUAL_MAP_H
