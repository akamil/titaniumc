#ifndef INCLUDE_TITANIUM_TRACER_H

#include <iostream>


class Tracer
{
public:
  Tracer( const TreeNode &, const char [] );
  ~Tracer();

private:
  const TreeNode &node;
  const char * const message;
};


////////////////////////////////////////////////////////////////////////


inline Tracer::Tracer( const TreeNode &node, const char message[] )
  : node( node ),
    message( message )
{
  node.message() << &node << ": " << message << " begin" << endl;
}


inline Tracer::~Tracer()
{
  node.message() << &node << ": " << message << " end" << endl;
}


#endif // !INCLUDE_TITANIUM_TRACER_H
