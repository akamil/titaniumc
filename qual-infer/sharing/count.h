#ifndef INCLUDE_TITANIUM_SHARING_COUNT_H
#define INCLUDE_TITANIUM_SHARING_COUNT_H

#include <iosfwd>

#include <sharing.h>


class SharingCount
{
public:
  SharingCount();
  void bump( Sharing );
  void print( ostream & ) const;

  unsigned shared;
  unsigned nonshared;
  unsigned polyshared;

private:
  static void print( ostream &, unsigned, unsigned, const char [] );
};


ostream & operator << ( ostream &, const SharingCount & );


#endif // !INCLUDE_TITANIUM_SHARING_COUNT_H
