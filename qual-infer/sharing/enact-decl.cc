#include <AST.h>
#include <decls.h>
#include "enact-context.h"


void Decl::sharingEnactFromType()
{
  modifiers( (Modifiers) (modifiers()
			  & ~(Common::NonsharedQ | Common::PolysharedQ)
			  | sharingToModifiers( type()->sharing() )) );
}


void SharingEnactContext::enactFromType( Decl &decl ) const
{
  decl.sharingEnactFromType();
}


////////////////////////////////////////////////////////////////////////


void ConstructorDeclNode::sharingEnact( const SharingEnactContext &context )
{
  Decl &declaration = *decl();
  TypeNode &type = *declaration.type();
  const bool thisReference = declaration.container()->asType()->isInferable();

  if (thisReference && type.enactType( context ))
    {
      context.enactFunction( *this );
      reportChange( declaration.errorName(), type );
    }
}
