#include <AST.h>
#include <code-util.h>
#include <decls.h>
#include <template.h>
#include "enact-context.h"
#include "stats.h"


void TreeNode::sharingEnact( const SharingEnactContext &context )
{
  enact( context );
}


void TreeNode::sharingEnactChildren( const SharingEnactContext &context ) const
{
  const unsigned numChildren = arity();
  for (unsigned sweep = 0; sweep < numChildren; ++sweep)
    child( sweep )->sharingEnactTree( context );
}


void TreeNode::sharingEnactTree( const SharingEnactContext &context )
{
  sharingEnact( context );
  sharingEnactChildren( context );
}


void TemplateDeclNode::sharingEnactTree( const SharingEnactContext & )
{
}


////////////////////////////////////////////////////////////////////////


void CompileUnitNode::sharingEnactChildren( const SharingEnactContext &context ) const
{
  PackageDecl * const decl = static_cast< PackageDecl * >( thePackage );
  assert( decl->category() == Decl::Package );

  const SharingEnactContext subcontext( context, decl );
  TreeNode::sharingEnactChildren( subcontext );

  if (DEBUG_PHASE_ENABLED( "infer-sharing", ident()->c_str() ))
    {
      cout << "after sharing inference:\n";
      print( cout, 1 );
      cout << '\n' << endl;
    }
}


void TypeDeclNode::sharingEnactChildren( const SharingEnactContext &context ) const
{
  if (!decl()->isArrayInstance())
    TreeNode::sharingEnactChildren( context );
}


void MethodDeclNode::sharingEnactTree( const SharingEnactContext &context )
{
  if (!(decl()->modifiers() & Native))
    TreeNode::sharingEnactTree( context );
}
