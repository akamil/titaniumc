#include <AST.h>
#include "solution.h"


void SharingSolution::discover( const TreeNode &node, Sharing sharing )
{
  // node.message() << &node << " solution: " << sharing << endl;
  const bool novel = insert( value_type( &node, sharing ) ).second;
  assert( novel );
}


Sharing SharingSolution::operator [] ( const TreeNode &node ) const
{
  const_iterator found = find( &node );
  if (found == end())
    return Nonshared;
  else
    return found->second;
}
