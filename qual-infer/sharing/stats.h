#ifndef INCLUDE_TITANIUM_SHARING_STATS_H
#define INCLUDE_TITANIUM_SHARING_STATS_H

#include <iosfwd>
#include "count.h"


class SharingStats
{
public:
  SharingStats();
  void print( ostream &, const string & ) const;

  SharingCount before;
  SharingCount after;
};


#endif // !INCLUDE_TITANIUM_SHARING_STATS_H
