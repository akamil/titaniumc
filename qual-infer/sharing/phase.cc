#include <AST.h>
#include <PhaseTimer.h>
#include <SolverInferContext.h>
#include <StaleBindings.h>
#include <compiler.h>
#include <llist.h>
#include <template.h>
#include <typecheck.h>
#include <optimize.h>

#include "enact-context.h"
#include "solution.h"
#include "solver.h"


extern void doSanityTypeCheck();

void inferSharing()
{
    PhaseTimer timer( "sharing qualification inference" );

    llist< TreeNode * > *allTrees = templateEnv.allInstances();
    foreach( unit, llist< CompileUnitNode * >, *allFiles )
      push( allTrees, (*unit) );
    
    foreach( tree, llist< TreeNode * >, *allTrees )
      (*tree)->unshareTypeTree();

    SharingSolver solver;
    StaleBindings stale;
    
    SharingContext context( solver, stale );
    foreach( tree, llist< TreeNode * >, *allTrees )
      (*tree)->inferSharingLateTree( context );

    if (sharingEnforcement == Early)
      foreach( tree, llist< TreeNode * >, *allTrees )
	(*tree)->inferSharingEarlyTree( solver );

    SharingSolution solution;
    solver.solve( solution );
    
    foreach( tree, llist< TreeNode * >, *allTrees )
      (*tree)->sharingEnactTree( solution );

    free_all( allTrees );
    stale.rebindAll();
    templateEnv.rehash();

    doSanityTypeCheck(); // sanity checking
    
    if (debug_qual)
	SharingEnactContext::printStats( cerr );
}
