#include <iostream>
#include "stats.h"


SharingStats::SharingStats()
{
}


void SharingStats::print( ostream &sink, const string &package ) const
{
  sink << "sharing stats: " << package << " before:" << before << endl;
  sink << "sharing stats: " << package << " after: " << after << endl;
}
