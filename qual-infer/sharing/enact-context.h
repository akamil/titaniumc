#ifndef INCLUDE_TITANIUM_SHARING_ENACT_CONTEXT_H
#define INCLUDE_TITANIUM_SHARING_ENACT_CONTEXT_H

#include <iosfwd>

#include <EnactContext.h>
#include "stats-map.h"

class SharingSolution;
class PackageDecl;


class SharingEnactContext : public EnactContext
{
public:
  SharingEnactContext( const SharingSolution & );
  SharingEnactContext( const SharingEnactContext &, PackageDecl * );

  bool enactBits( const TreeNode &, Modifiers &, bool ) const;
  void enactFromType( Decl & ) const;
  Modifiers flagsMask() const;
  
  static void printStats( ostream & );

private:
  const SharingSolution &solution;
  SharingStats &stats;

  static void printStat( ostream &, PackageDecl *, const SharingStats & );
  static SharingStatsMap statsMap;
};


////////////////////////////////////////////////////////////////////////


inline SharingEnactContext::SharingEnactContext( const SharingSolution &solution )
  : solution( solution ),
    stats( statsMap[ 0 ] )
{
}


inline SharingEnactContext::SharingEnactContext( const SharingEnactContext &prior,
						 PackageDecl *package )
  : solution( prior.solution ),
    stats( statsMap[ package ] )
{
}


#endif // !INCLUDE_TITANIUM_SHARING_ENACT_CONTEXT_H
