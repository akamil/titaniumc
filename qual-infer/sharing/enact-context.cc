#include <algorithm>
#include <iostream>
#include "decls.h"
#include "enact-context.h"
#include "solver.h"
#include "stats.h"
#include "stats-map.h"


SharingStatsMap SharingEnactContext::statsMap;


Common::Modifiers SharingEnactContext::flagsMask() const
{
  return (Modifiers) (NonsharedQ | PolysharedQ);
}


void SharingEnactContext::printStat( ostream &sink, PackageDecl *package, const SharingStats &stats )
{
  stats.print( sink, package ? package->errorName() : "no package" );
}


void SharingEnactContext::printStats( ostream &sink )
{
  for (SharingStatsMap::const_iterator sweep = statsMap.begin();
       sweep != statsMap.end(); ++sweep)
    printStat( sink, sweep->first, sweep->second );
}
