#include <AST.h>
#include "enact-context.h"
#include "solution.h"


bool SharingEnactContext::enactBits( const TreeNode &node,
				     Modifiers &modifiers,
				     bool interesting ) const
{
  const Sharing newSharing = solution[ node ];
  const Sharing oldSharing = modifiersToSharing( modifiers );

  if (newSharing != oldSharing)
    {
      modifiers = (Modifiers) (modifiers & ~(NonsharedQ | PolysharedQ | SharingInferred));
      if (newSharing == Nonshared || newSharing == Polyshared) 
        modifiers = (Modifiers) (modifiers | sharingToModifiers( newSharing ) | SharingInferred);
    }

  if (interesting)
    {
      stats.before.bump( oldSharing );
      stats.after.bump( newSharing );
    }

  return newSharing != oldSharing;
}
