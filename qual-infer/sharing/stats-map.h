#ifndef INCLUDE_TITANIUM_SHARING_STATS_MAP_H
#define INCLUDE_TITANIUM_SHARING_STATS_MAP_H

#include <map>
#include "stats.h"

class PackageDecl;


typedef map< PackageDecl *, SharingStats, less< PackageDecl * > >  SharingStatsMap;


#endif // !INCLUDE_TITANIUM_SHARING_STATS_MAP_H
