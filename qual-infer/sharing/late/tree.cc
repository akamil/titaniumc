#include <AST.h>
#include <SolverInferContext.h>
#include <decls.h>


void TreeNode::inferSharingLateTree( const SharingContext &context )
{
  inferSharingLateChildren( context );
  inferSharingLate( context );
}


void TreeNode::inferSharingTreeExport( const SharingContext &context )
{
  const SharingContext subcontext( context, isExported() );
  TreeNode::inferSharingLateTree( subcontext );
}


////////////////////////////////////////////////////////////////////////


void CompileUnitNode::inferSharingLateTree( const SharingContext &context )
{
  inferSharingTreeExport( context );
}


void ConstructorDeclNode::inferSharingLateTree( const SharingContext &context )
{
  inferSharingTreeExport( context );
}


void FieldDeclNode::inferSharingLateTree( const SharingContext &context )
{
  inferSharingTreeExport( context );
}


void MethodNode::inferSharingLateTree( const SharingContext &context )
{
  inferSharingTreeExport( context );
}


void TemplateDeclNode::inferSharingLateTree( const SharingContext & )
{  
}


void TypeDeclNode::inferSharingLateTree( const SharingContext &context )
{
  inferSharingTreeExport( context );
}
