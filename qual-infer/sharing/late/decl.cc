#include <AST.h>
#include <SolverInferContext.h>
#include <solver.h>


void ConstructorDeclNode::inferSharingLate( const SharingContext &context )
{
  TreeNode::inferSharingLate( context );

  if (context.isExported)
    decl()->type()->conservative( context.solver );
}
