#include <AST.h>
#include <ClassDecl.h>
#include <SolverInferContext.h>
#include <solver.h>


void TreeNode::inferSharingLateChildren( const SharingContext &context ) const
{
  const unsigned numChildren = arity();
  
  for (unsigned sweep = 0; sweep < numChildren; ++sweep)
    child( sweep )->inferSharingLateTree( context );
}


void TreeNode::inferSharingLate( const SharingContext &context )
{
  infer( context );
}


////////////////////////////////////////////////////////////////////////


void ConstructorDeclNode::inferSharingLateChildren( const SharingContext &context ) const
{
  SharingContext subcontext( context, *decl()->methodType() );
  TreeNode::inferSharingLateChildren( subcontext );
}


void MethodDeclNode::inferSharingLateChildren( const SharingContext &context ) const
{
  SharingContext subcontext( context, *decl()->methodType() );
  TreeNode::inferSharingLateChildren( subcontext );
}


void TypeDeclNode::inferSharingLateChildren( const SharingContext &context ) const
{
  ClassDecl &me = *decl();
  
  members()->inferSharingLateTree( context ); // PR655 - be sure to traverse array method signatures

  if (me.isArrayInstance())
    {
      const string *cloneName = intern( "clone" );
      const Decl * const cloneMethod = me.environ()->lookupProper( cloneName, Decl::Method );
      if (cloneMethod)
	{
	  assert( cloneMethod->modifiers() & Native );
	  cloneMethod->type()->conservative( context.solver );
	}
      const string *localcloneName = intern( "localClone" );
      const Decl * const localcloneMethod = me.environ()->lookupProper( localcloneName, Decl::Method );
      if (localcloneMethod)
	{
	  assert( localcloneMethod->modifiers() & Native );
	  localcloneMethod->type()->conservative( context.solver );
	}
    }
}
