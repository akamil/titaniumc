#include <AST.h>
#include <SolverInferContext.h>
#include <solver.h>
#include "code-util.h"


void AssignNode::inferSharingLate( const SharingContext &context )
{
  TreeNode::inferSharingLate( context );
  opnd0()->receiveAssign( *opnd1()->type(), context.solver );
}


void EqualityNode::inferSharingLate( const SharingContext &context )
{
  TreeNode::inferSharingLate( context );

  const TypeNode &type0 = *opnd0()->type();
  const TypeNode &type1 = *opnd1()->type();

  if (type0.isInferable() && type1.isInferable())
    {
      context.solver.subsumedBy( *this, type0 );
      context.solver.subsumedBy( *this, type1 );
    }
}


void IfExprNode::inferSharingLate( const SharingContext &context )
{
  if (type()->isInferable())
    {
      context.solver.eitherAssignable( *thenOpnd()->type(),
				       *elseOpnd()->type() );
      TreeNode::inferSharingLate( context );
    }
}


void ThisNode::inferSharingLate( const SharingContext &context )
{
  if (type()->isInferable())
    {
      if (!context.method) {
      /* PR512: 'this' in field and instance initializers are 
         assigned the sharing LUB of all constructors
       */
        foriter (constructor, 
                 theClass()->decl()->source()->members()->allChildren(), 
                 TreeNode::ChildIter) {
          if (isConstructorDeclNode(*constructor)) {
            context.solver.subsumedBy( *type(), *(*constructor)->type());
          }
        }

      }
      
      TreeNode::inferSharingLate( context );
    }
}
