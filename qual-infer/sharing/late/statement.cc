#include <AST.h>
#include <SolverInferContext.h>
#include <decls.h>
#include <solver.h>


void CatchNode::inferSharingLate( const SharingContext &context )
{
  context.solver.mustBeShared( *param()->dtype() );
}


void MonitorFetchInstanceNode::inferSharingLate( const SharingContext &context )
{
  const TypeNode &mutex = *expr()->type();
  
  if (!mutex.isLocal())
    context.solver.mustBeShared( mutex );
}


void ThrowNode::inferSharingLate( const SharingContext &context )
{
  context.solver.mustBeShared( *expr()->type() );
}
