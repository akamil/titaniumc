#include <AST.h>
#include <SolverInferContext.h>
#include <solver.h>


static void handleAccess( SharingSolver &solver,
			  const TypeNode *outer,
			  const TypeNode &inner,
			  const TypeNode &result )
{
  inner.identicalTo( result, solver );
  
  // !!!: should this not be applied to methods?
  if (outer && outer->isInferable() && !outer->isLocal())
    solver.mustBeShared( *outer );
}


void ArrayAccessNode::inferSharingLate( const SharingContext &context )
{
  const TypeNode &outer = *array()->type();
  const TypeNode &inner = *outer.elementType();

  handleAccess( context.solver, &outer, inner, *type() );
}


void ObjectFieldAccessNode::inferSharingLate( const SharingContext &context )
{
  const TypeNode * const outer = (decl()->modifiers() & Common::Static) ? 0 : accessedObjectType();
  const TypeNode &       inner = *decl()->type();

  handleAccess( context.solver, outer, inner, *type() );
}


void TypeFieldAccessNode::inferSharingLate( const SharingContext &context )
{
  assert( decl()->modifiers() & Static );

  const TypeNode * const outer = 0;
  const TypeNode &       inner = *decl()->type();

  handleAccess( context.solver, outer, inner, *type() );
}
