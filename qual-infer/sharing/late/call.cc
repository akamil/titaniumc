#include <AST.h>
#include <SolverInferContext.h>
#include <solver.h>


void MethodCallNode::inferSharingLate( const SharingContext &context )
{
  TreeNode::inferSharingLate( context );

  const TypeNode &dispatcher = *method()->accessedObjectType();
  if (dispatcher.isTitaniumArrayType())
    {
      // ensure that any grid arguments to grid methods are local and/or shared 
      // (i.e. not global nonshared or global polyshared)
      for (int i=0; i < args()->arity(); i++) {
        const TypeNode *argtype = args()->child( i )->type();
        if (argtype->isTitaniumArrayType() && !argtype->isLocal()) {
          context.solver.mustBeShared( *argtype );
        }
      }
    }
}


void SuperConstructorCallNode::inferSharingLate( const SharingContext &context )
{
  TreeNode::inferSharingLate( context );
  context.solver.subsumedBy( *context.method, *decl()->type() );
}


void ThisConstructorCallNode::inferSharingLate( const SharingContext &context )
{
  TreeNode::inferSharingLate( context );
  context.solver.subsumedBy( *context.method, *decl()->type() );
}
