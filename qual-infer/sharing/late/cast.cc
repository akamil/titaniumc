#include <AST.h>
#include <SolverInferContext.h>
#include <solver.h>


void DynamicTypeNode::inferSharingLate( const SharingContext &context )
{
  opnd0()->type()->sharingCastTo( *dtype(), context.solver );
  TreeNode::inferSharingLate( context );
}


////////////////////////////////////////////////////////////////////////


void TypeNode::sharingCastTo( const TypeNode &target, SharingSolver &solver ) const
{
  if (isInferable() && target.isInferable())
    solver.subsumedBy( *this, target );
}


////////////////////////////////////////////////////////////////////////


void ArrayTypeNode::sharingCastTo( const TypeNode &target, SharingSolver &solver ) const
{
  TypeNode::sharingCastTo( target, solver );
  
  if (target.isArrayType())
    elementType()->subcastTo( *target.elementType(), solver );
  else
    elementType()->conservative( solver );
}


void DomainTypeNode::sharingCastTo( const TypeNode &target, SharingSolver &solver ) const
{
  if (!target.isRectDomainType())
    TypeNode::sharingCastTo( target, solver );
}


void TypeNameNode::sharingCastTo( const TypeNode &target, SharingSolver &solver ) const
{
  TypeNode::sharingCastTo( target, solver );

  if (target.isArrayType())
    target.elementType()->conservative( solver );
}
