#ifndef INCLUDE_TITANIUM_SHARING_SOLVER_H
#define INCLUDE_TITANIUM_SHARING_SOLVER_H

#include <list>

#include <QualSolver.h>
#include <quals.h>
#include <sharing.h>
#include <using-std.h>

class SharingSolution;
class TreeNode;
class TypeNode;


class SharingSolver : public QualSolver
{
public:
  SharingSolver();

  void mustBeShared( const TreeNode & );
  void mustBePolyshared( const TypeNode & );
  void propagateShared( const TreeNode &, const TreeNode & );
  void eitherAssignable( const TreeNode &, const TreeNode & );

  void solve( SharingSolution & );

private:
  qual qconst( Sharing ) const;
  qual qconst( const TypeNode & ) const;

  Sharing sharing( const qual ) const;
  Sharing lub( const qual_set ) const;
  
  typedef list< pair< const TreeNode *, qual > > WorkList;

  qual shared;
  qual nonshared;
  qual polyshared;
};


////////////////////////////////////////////////////////////////////////


inline void SharingSolver::mustBeShared( const TreeNode &node )
{
  unify_qual( qvar( node ), shared );
}


inline void SharingSolver::mustBePolyshared( const TypeNode &type )
{
  assert( type.sharing() == Polyshared );
  unify_qual( qvar( type ), polyshared );
}


inline void SharingSolver::propagateShared( const TreeNode &from,
					    const TreeNode &to )
{
  cond_mkleq_qual( qvar( from ), shared, qvar( to ), shared );
}


inline qual SharingSolver::qconst( const TypeNode &type ) const
{
  return qconst( type.sharing() );
}


#endif // !INCLUDE_TITANIUM_SHARING_SOLVER_H
