#include <cassert>
#include <iostream>
#include "count.h"


SharingCount::SharingCount()
  : shared( 0 ),
    nonshared( 0 ),
    polyshared( 0 )
{
}


void SharingCount::bump( Sharing sharing )
{
  switch (sharing)
    {
    case Shared:
      ++shared;
      break;
    case Nonshared:
      ++nonshared;
      break;
    case Polyshared:
      ++polyshared;
      break;
    default:
      assert( 0 );
    }
}


void SharingCount::print( ostream &sink ) const
{
  const unsigned total = shared + nonshared + polyshared;
  if (total)
    {
      print( sink, shared, total, "shared" );
      sink << ',';
      print( sink, nonshared, total, "nonshared" );
      sink << ',';
      print( sink, polyshared, total, "polyshared" );
    }
  else
    sink << "(no opportunities)";
};


void SharingCount::print( ostream &sink, unsigned fraction,
			  unsigned total, const char label[] )
{
  sink << ' ' << fraction << " (" << 100 * fraction / total << "%) " << label;
}


ostream & operator << ( ostream &sink, const SharingCount &count )
{
  count.print( sink );
  return sink;
}
