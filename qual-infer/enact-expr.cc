#include <AST.h>
#include "EnactContext.h"


void ExprNode::enact( const EnactContext & )
{
  type( 0 );
}


////////////////////////////////////////////////////////////////////////


void AllocateNode::enact( const EnactContext &context )
{
  if (dtype()->enactType( context ))
    reportChange( "object allocation", *dtype() );

  ExprNode::enact( context );
}


void CastNode::enact( const EnactContext &context )
{
  if (dtype()->enactType( context ))
    reportChange( "cast", *dtype() );

  ExprNode::enact( context );
}


void InstanceOfNode::enact( const EnactContext &context )
{
  if (dtype()->enactType( context ))
    reportChange( "instanceof", *dtype() );
}


void ThisNode::enact( const EnactContext &context )
{
  if (type()->isInferable())
    context.enactBits( *type(), _flags, false );
  
  ExprNode::enact( context );
}
