#include <AST.h>
#include <st-field.h>


void MethodCallNode::rebind() const
{
  // This is a really shoddy approximation to resolveField().
  
  FieldContext context;
  context.methodArgs = args();
  
  method()->resolveAMember( false, false, &context );
}
