#include <AST.h>
#include "EnactContext.h"


bool TypeNode::enactType( const EnactContext &context )
{
  bool retval = isInferable() && context.enactBits( *this, _modifiers, true );
  // DOB: respect the TypeNode::modifiers interface to prevent stale cTypes from hanging around (PR 446)
  modifiers(_modifiers);  
  // make sure the cType caches get reset for all types, because many depend on the modifiers for
  // the child types, which may have changed (e.g. Titanium array element types, pointer referent types)
  return retval;
}


////////////////////////////////////////////////////////////////////////


bool ArrayTypeNode::enactType( const EnactContext &context )
{
  bool changed = TypeNode::enactType( context );
  changed |= elementType()->enactType( context );
  return changed;
}


bool TypeListNode::enactType( const EnactContext &context )
{
  bool changed = false;
  foriter (type, allTypes(), TypeIter)
    changed |= (*type)->enactType( context );
  return changed;
}
