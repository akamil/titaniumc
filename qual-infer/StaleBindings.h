#ifndef INCLUDE_TITANIUM_QUAL_INFER_STALE_SET_H
#define INCLUDE_TITANIUM_QUAL_INFER_STALE_SET_H

#include <list>

#include <using-std.h>

class MethodCallNode;


class StaleBindings : public list< MethodCallNode * >
{
public:
  void rebindAll() const;
};


#endif // !INCLUDE_TITANIUM_QUAL_INFER_STALE_SET_H
