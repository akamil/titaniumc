#include <AST.h>

#include "StaleBindings.h"


void StaleBindings::rebindAll() const
{
  foreach_const (call, StaleBindings, *this)
    (*call)->rebind();
}
