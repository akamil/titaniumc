#ifndef INCLUDE_TITANIUM_QUAL_INFER_PHASE_TIMER_H
#define INCLUDE_TITANIUM_QUAL_INFER_PHASE_TIMER_H

#include <sys/time.h>


class PhaseTimer
{
public:
  PhaseTimer( const char [] );
  ~PhaseTimer();
  
private:
  static timeval now();

  const char *message;
  const timeval begin;
};


#endif // !INCLUDE_TITANIUM_QUAL_INFER_PHASE_TIMER_H
