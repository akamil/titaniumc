#include "AST.h"
#include "decls.h"


void TreeNode::unshareTypeTree()
{
  foriter (child, allChildren(), ChildIter)
    (*child)->unshareTypeTree();
  
  unshareType();
}


void TemplateDeclNode::unshareTypeTree()
{
}


////////////////////////////////////////////////////////////////////////


void CastNode::unshareType()
{
  dtype( dtype()->unshareNode( this ) );
  type( dtype() );
}


void ConstructorDeclNode::unshareType()
{
  TypeNode &declType = *decl()->type();

  TypeListNode &paramTypes = *declType.paramTypes();  
  for (unsigned sweep = params()->arity(); sweep--; )
    paramTypes.child( sweep, params()->child( sweep )->dtype() );
}


void DataDeclNode::unshareType()
{
  dtype( dtype()->unshareNode( this ) );
  decl()->type( dtype() );
}


void ExprNode::unshareType()
{
  type( type()->unshareNode( this ) );
}


void InstanceOfNode::unshareType()
{
  ExprNode::unshareType();
  dtype( dtype()->unshareNode( this ) );
}


void MethodNode::unshareType()
{
  TypeNode &declType = *decl()->type();

  returnType( returnType()->unshareNode( this ) );
  declType.returnType( returnType() );
  declaredReturnType( declaredReturnType()->unshareNode( this ) );
  declType.declaredReturnType( declaredReturnType() );

  TypeListNode &paramTypes = *declType.paramTypes();  
  for (unsigned sweep = params()->arity(); sweep--; )
    paramTypes.child( sweep, params()->child( sweep )->dtype() );
}


void ParameterNode::unshareType()
{
  dtype( dtype()->unshareNode( this ) );
  decl()->type( dtype() );
}


void TreeNode::unshareType()
{
}


////////////////////////////////////////////////////////////////////////


TypeNode *TypeNode::unshareNode( const TreeNode *coparent )
{
  return coparent == parent()
    ? this
    : deepClone();
}

TypeNode *ArrayTypeNode::unshareNode( const TreeNode *coparent )
{
  TypeNode *myElement = elementType();
  TypeNode *okElement = myElement->unshareNode( this );
  
  elementType(okElement);

  return TypeNode::unshareNode(coparent);
}

