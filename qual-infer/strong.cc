#include <AST.h>
#include <decls.h>
#include "QualSolver.h"


void TypeNode::stronglySubsumedBy( const TypeNode &greater, QualSolver &solver ) const
{
  if (isInferable())
    solver.identicalTo( *this, greater );
}


////////////////////////////////////////////////////////////////////////


void ArrayTypeNode::stronglySubsumedBy( const TypeNode &greater, QualSolver &solver ) const
{
  TypeNode::identicalTo( greater, solver );
  
  if (greater.isArrayType())
    elementType()->stronglySubsumedBy( *greater.elementType(), solver );
  else
    elementType()->conservative( solver );
}
