#include <AST.h>
#include <template.h>
#include "QualSolver.h"


void TypeNode::conservative( QualSolver &solver ) const
{
  solver.conservative( *this );
}


////////////////////////////////////////////////////////////////////////


void ArrayTypeNode::conservative( QualSolver &solver ) const
{
  TypeNode::conservative( solver );
  elementType()->conservative( solver );
}


void MethodTypeNode::conservative( QualSolver &solver ) const
{
  TypeNode::conservative( solver );
  returnType()->conservative( solver );
  paramTypes()->conservative( solver );
}


void TypeListNode::conservative( QualSolver &solver ) const
{
  foriter (type, allTypes(), ConstTypeIter)
    (*type)->conservative( solver );
}
