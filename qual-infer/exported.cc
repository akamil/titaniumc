#include <AST.h>
#include "code-util.h"
#include "decls.h"

bool TreeNode::isExported() const
{
  return false;
}


bool CompileUnitNode::isExported() const
{
  return !(codeGen_main && selectedForCodeGen( false ));
}


bool ClassDeclNode::isExported() const
{
  return parent()->parent()->isExported();
}


bool InterfaceDeclNode::isExported() const
{
  return parent()->parent()->isExported();
}


bool FieldDeclNode::isExported() const
{
  // DOB: PR643 - when using separate compilation, it's unsafe for LQI to modify
  // any of the field types in the tlib, because it changes the object size & field positioning
  if ((!codeGen_main || !codeGen_libs) && 
      isInLibrary(decl()->container()))
    return true;
  // AK: PR810 - fields of Cloneable classes should not be localized, since
  // clone() nullifies local fields.
  else if (!(decl()->modifiers() & Static) &&
	   !(decl()->container()->modifiers() & Immutable) &&
	   implements(decl()->container(), CloneableDecl))
    return true;
  else
    return false;
}


bool MethodDeclNode::isExported() const
{
  return parent()->parent()->isExported() && 
    (decl()->modifiers() & (Public | Protected));
}


bool MethodSignatureNode::isExported() const
{
  return parent()->parent()->isExported() && 
    (decl()->modifiers() & (Public | Protected));
}


bool ConstructorDeclNode::isExported() const
{
  return parent()->parent()->isExported() && 
    (decl()->modifiers() & (Public | Protected));
}
