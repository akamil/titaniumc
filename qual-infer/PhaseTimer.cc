#include <cassert>
#include <iostream>
#include <using-std.h>
#include <optimize.h>
#include "PhaseTimer.h"

bool debug_qual = false;

PhaseTimer::PhaseTimer( const char message[] )
  : message( message ),
    begin( now() )
{
  if (debug_qual)
    cerr << message << ": begin\n";
}


PhaseTimer::~PhaseTimer()
{
  if (debug_qual)
    {
      const timeval end = now();
      timeval interval;
      interval.tv_sec = end.tv_sec - begin.tv_sec;
      interval.tv_usec = end.tv_usec - begin.tv_usec;
      
      cerr << message << ": end; "
	   << interval.tv_sec + interval.tv_usec / 1000000.
	   << " seconds elapsed" << endl;
    }
}


timeval PhaseTimer::now()
{
  timeval result = { 0, 0 };

  if (debug_qual)
    {
      int error = gettimeofday( &result, 0 );  
      assert( !error );
    }
  
  return result;
}
