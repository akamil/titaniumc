#include <AST.h>
#include "QualSolver.h"


void TypeNode::subcastTo( const TypeNode &target, QualSolver &solver ) const
{
  solver.identicalTo( *this, target );
}


////////////////////////////////////////////////////////////////////////


void ArrayTypeNode::subcastTo( const TypeNode &target, QualSolver &solver ) const
{
  TypeNode::subcastTo( target, solver );

  if (target.isArrayType())
    elementType()->subcastTo( *target.elementType(), solver );
  else
    elementType()->conservative( solver );
}


void TypeNameNode::subcastTo( const TypeNode &target, QualSolver &solver ) const
{
  TypeNode::subcastTo( target, solver );

  if (target.isArrayType())
    target.elementType()->conservative( solver );
}
