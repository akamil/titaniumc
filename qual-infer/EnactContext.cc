#include <AST.h>
#include "EnactContext.h"


void EnactContext::enactFunction( TreeNode &function ) const
{
  Decl &declaration = *function.decl();
  enactFromType( declaration );

  const Modifiers mask = flagsMask();
  function.flags( (Modifiers) (function.flags() & ~mask | (declaration.modifiers() & mask)) );
}
