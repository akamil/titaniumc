#include <AST.h>
#include <InferContext.h>
#include <MethodSet.h>
#include <decls.h>
#include <compiler.h>

#include "QualSolver.h"


void ConstructorDeclNode::infer( const InferContext &context )
{
  if (context.isExported)
    decl()->type()->paramTypes()->conservative( context.solver );
}


void DataDeclNode::infer( const InferContext &context )
{
  assert( decl()->type() == dtype() );
  
  dtype()->protect( context.solver );
  
  if (!initExpr()->absent())
    initExpr()->type()->weaklySubsumedBy( *dtype(), context.solver );
}


void FieldDeclNode::infer( const InferContext &context )
{
  if (context.isExported)
    dtype()->conservative( context.solver );

  /* don't change the implicit .class field or the .TYPE field 
   * on Integer and friends, which are initialized by native code 
   * and need to remain global shared. 
   * Class objects always live on P0 anyhow, so LQI would never 
   * do anything useful with them anyhow, except in very contrived cases
   */
  if (dtype()->decl() == JavaLangClassDecl) 
    dtype()->conservative( context.solver );

  DataDeclNode::infer( context );
}


void MethodNode::infer( const InferContext &context )
{
  const MethodDecl &myDecl = *decl();
  const TypeNode &myType = *myDecl.type();

  if (context.isExported || 
      myDecl.modifiers() & Native || 
      myDecl.container()->container()->fullName() == "ti.domains")
    myType.conservative( context.solver );

  {
    returnType()->protect( context.solver );
    
    const TypeNode * const thisType = myDecl.thisType();    
    if (thisType)
      thisType->protect( context.solver );
  }

  {
    const Decl * const overrides = myDecl.overrides();
    if (overrides)
      myType.identicalTo( *overrides->type(), context.solver );
  }

  {
    declaredReturnType()->identicalTo( *returnType(), context.solver );
  }

  {
    const MethodSet &implements = myDecl.implements();
    for (MethodSet::const_iterator interface = implements.begin();
	 interface != implements.end(); ++interface)
      myType.identicalTo( *(*interface)->type(), context.solver );
  }
}


void MethodDeclNode::infer( const InferContext &context )
{
  const MethodDecl &me = *decl();
  if (me.modifiers() & Native || isMain())
    me.type()->conservative( context.solver );
  
  MethodNode::infer( context );
}


void ParameterNode::infer( const InferContext &context )
{
  dtype()->protect( context.solver );
}
