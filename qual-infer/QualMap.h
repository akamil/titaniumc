#ifndef INCLUDE_TITANIUM_QUAL_MAP_H
#define INCLUDE_TITANIUM_QUAL_MAP_H

#include <map>

#include <quals.h>
#include <using-std.h>

class TreeNode;


typedef map< const TreeNode *, qual > QualMap;


#endif // !INCLUDE_TITANIUM_QUAL_MAP_H
