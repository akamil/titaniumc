#include <AST.h>
#include <decls.h>
#include "QualSolver.h"


void TypeNode::weaklySubsumedBy( const TypeNode &greater, QualSolver &solver ) const
{
  solver.subsumedBy( *this, greater );
}


////////////////////////////////////////////////////////////////////////


void ArrayTypeNode::weaklySubsumedBy( const TypeNode &greater, QualSolver &solver ) const
{
  TypeNode::weaklySubsumedBy( greater, solver );
  
  if (greater.isArrayType())
    elementType()->stronglySubsumedBy( *greater.elementType(), solver );
  else
    elementType()->conservative( solver );
}


void MethodTypeNode::weaklySubsumedBy( const TypeNode &greater, QualSolver &solver ) const
{
  identicalTo( greater, solver );
}
