#include <AST.h>
#include <compiler.h>
#include "QualSolver.h"


void TypeNode::similarTo( const TypeNode &, QualSolver & ) const
{
}


void ArrayTypeNode::similarTo( const TypeNode &other, QualSolver &solver ) const
{
  if (other.isArrayType())
    elementType()->subsimilarTo( *other.elementType(), solver );
}


////////////////////////////////////////////////////////////////////////


void TypeNode::subsimilarTo( const TypeNode &other, QualSolver &solver ) const
{
  solver.identicalTo( *this, other );
}


void JavaArrayTypeNode::subsimilarTo( const TypeNode &other, QualSolver &solver ) const
{
  TypeNode::identicalTo( other, solver );

  if (other.isArrayType())
    elementType()->subsimilarTo( *other.elementType(), solver );
  else
    assert( other.decl() == ObjectDecl );
}


void TitaniumArrayTypeNode::subsimilarTo( const TypeNode &other, QualSolver &solver ) const
{
  TypeNode::identicalTo( other, solver );
  elementType()->subsimilarTo( *other.elementType(), solver );
}
