#include <AST.h>
#include "InferContext.h"


void ReturnNode::infer( const InferContext &context )
{
  if (!expr()->absent())
    expr()->type()->weaklySubsumedBy( *context.method->returnType(), context.solver );
}
