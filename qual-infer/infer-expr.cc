#include <AST.h>
#include "InferContext.h"
#include "QualSolver.h"


void ExprNode::infer( const InferContext & )
{
  assert( !type()->isInferable() );
}


////////////////////////////////////////////////////////////////////////


void AllocateArrayNode::infer( const InferContext &context )
{
  const TypeNode *stack = type();

  // !! Is this order correct, or is it backwards?
  // for (unsigned sweep = dimExprs()->arity(); sweep--; )
  const unsigned numDims = dimExprs()->arity();
  for (unsigned sweep = 0; sweep < numDims; ++sweep)
    {
      context.solver.identicalTo( *stack, *dimExprs()->child( sweep ) );
      stack = stack->elementType();
    }

  dtype()->identicalTo( *stack, context.solver );

  if (!region()->absent())
    region()->type()->conservative( context.solver );
  if (!initExpr()->absent())
    dtype()->identicalTo( *initExpr()->type(), context.solver );
}


void AllocateNode::infer( const InferContext &context )
{
  const TypeNode &constructor = *decl()->type();

  constrainCall( constructor, context.solver );
  dtype()->weaklySubsumedBy( constructor, context.solver );
  dtype()->identicalTo( *type(), context.solver );

  if (!region()->absent())
    region()->type()->conservative( context.solver );
}


void ArrayInitNode::infer( const InferContext &context )
{
  const TypeNode &elemType = *type()->elementType();
  
  foriter (initializer, initializers()->allChildren(), ChildIter)
    (*initializer)->type()->weaklySubsumedBy( elemType, context.solver );
}


void AssignNode::infer( const InferContext &context )
{
  type()->identicalTo( *opnd0()->type(), context.solver );
}


void StringConcatAssignPreLoweringNode::infer( const InferContext &context )
{
  type()->identicalTo( *opnd0()->type(), context.solver );
}


void BroadcastNode::infer( const InferContext &context )
{
  expr()->type()->weaklySubsumedBy( *type(), context.solver );
}


void DynamicTypeNode::infer( const InferContext &context )
{
  dtype()->protect( context.solver );
}


void EqualityNode::infer( const InferContext &context )
{
  opnd0()->type()->similarTo( *opnd1()->type(), context.solver );
}


void IfExprNode::infer( const InferContext &context )
{
  thenOpnd()->type()->weaklySubsumedBy( *type(), context.solver );
  elseOpnd()->type()->weaklySubsumedBy( *type(), context.solver );
}


void NullPntrNode::infer( const InferContext & )
{
}


void ObjectNode::infer( const InferContext &context )
{
  type()->identicalTo( *decl()->type(), context.solver );
}


void StringConcatNode::infer( const InferContext & )
{
}


void StringLitNode::infer( const InferContext &context )
{
  type()->conservative( context.solver );
}


void ThisNode::infer( const InferContext &context )
{
  if (context.method)
    type()->identicalTo( *context.method, context.solver );
}
