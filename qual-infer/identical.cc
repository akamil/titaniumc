#include <AST.h>
#include "QualSolver.h"


void TypeNode::identicalTo( const TypeNode &other, QualSolver &solver ) const
{
  solver.identicalTo( *this, other );
}


////////////////////////////////////////////////////////////////////////


void ArrayTypeNode::identicalTo( const TypeNode &other, QualSolver &solver ) const
{
  TypeNode::identicalTo( other, solver );
  elementType()->identicalTo( *other.elementType(), solver );
}


void MethodTypeNode::identicalTo( const TypeNode &other, QualSolver &solver ) const
{
  TypeNode::identicalTo( other, solver );
  returnType()->identicalTo( *other.returnType(), solver );
  paramTypes()->identicalTo( *other.paramTypes(), solver );
}


void TypeListNode::identicalTo( TypeListNode &other, QualSolver &solver ) const
{
  assert( arity() == other.arity() );
  
  for (unsigned sweep = arity(); sweep--; )
    child( sweep )->identicalTo( *other.child( sweep ), solver );
}
