#ifndef INCLUDE_TITANIUM_ENACT_CONTEXT_H
#define INCLUDE_TITANIUM_ENACT_CONTEXT_H

#include <common.h>

class Decl;
class QualSolver;
class TreeNode;


class EnactContext : public Common
{
public:
  virtual bool enactBits( const TreeNode &, Modifiers &, bool ) const = 0;
  virtual void enactFromType( Decl & ) const = 0;
  
  void enactFunction( TreeNode & ) const;

protected:
  virtual Modifiers flagsMask() const = 0;
};


#endif // !INCLUDE_TITANIUM_ENACT_CONTEXT_H
