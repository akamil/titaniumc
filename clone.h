#ifndef INCLUDE_CLONE_H
#define INCLUDE_CLONE_H


enum UseDefTreatment { Nullify, Duplicate, Share };


bool isLegalToCloneDeep(TreeNode *t);
extern TreeNode *deepCloneWithCrossTreeFixup(TreeNode *orig,
					     bool safety = true);

template< class Node > Node *CloneTree(Node *t, UseDefTreatment treatment = Share)
{
  Node *newNode = t->clone();
  switch (treatment)
    {
    case Nullify:
      newNode->setUses(NULL);
      newNode->setDefs(NULL);
      break;
    case Duplicate:
      newNode->setUses(copylist(t->getUses()));
      newNode->setDefs(copylist(t->getDefs()));
      break;
    case Share:
      break;
    }
  
  const int childCount = newNode->arity();
  for (int sweep = 0; sweep < childCount; ++sweep)
    newNode->child(sweep, CloneTree(newNode->child(sweep), treatment));

  return newNode;
}


#endif // !INCLUDE_CLONE_H
