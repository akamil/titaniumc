#ifndef _INCLUDE_TITANIUM_LEX_STRING_H_
#define _INCLUDE_TITANIUM_LEX_STRING_H_

#include "literal.h"
#include "string16.h"

class SourcePosn;


/*   */ uint16 convertCharacter( char *, size_t, const SourcePosn & );
const string16 convertString(    char *, size_t, const SourcePosn & );


#endif
